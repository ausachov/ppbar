#include "TFile.h"
#include "TTree.h"
#include "TChain.h"
#include "iostream"
#include "TVector3.h"
#include "TRandom.h"


from ROOT import *
from array import array
from math import *
from ROOT import TVector3

homeDir = "/users/LHCb/zhovkovska/scripts/"

def FromB(i): 
  MID = i
  if(i<0): MID = -i 
  if(i%100==0): i=i+1
  
  n2 = int(MID/1000)
  MID=MID-n2*1000
  n1= int(MID/100)
  if(n1==5): return 1
  
  BID=i
  if(i<0): BID = -i 
  n2 = int(BID/10000.)
  BID=BID-n2*10000
  n1 = int(BID/1000.)
  if(n1==5): return 1
  
  return 0

def IP(vtx1 = TVector3(),vtx_jpsi = TVector3(),p_jpsi = TVector3()):
  #from mathworld
  p = p_jpsi.Mag()
  v12 = vtx_jpsi-vtx1
  p12 = v12.Mag()
  dd = -v12.Dot(p_jpsi)/p/p
  return (p12*p12*p*p/p/p-(dd*dd*p*p))**0.5

def AddBr():
  
  t=TChain("DecayTree")
  t.Add("/sps/lhcb/zhovkovska/Pmt2016/Reduced_Pmt_2016_*.root")
  t.Add("/sps/lhcb/zhovkovska/Pmt2015/Reduced_Pmt_2015_*.root")

#   f = TFile("Reduced_Snd_2012.root")
#   t = (TTree*) f.Get("DecayTree")
  
  fn = TFile(homeDir+"Reduced_Mismatch.root","recreate")
  nt = TTree("DecayTree","DecayTree")
#   nt = t.CloneTree(0)
  
  mm, m_scaled = array("d",[0]), array("d",[0])
  pvz,pvze,vtxz,vtxze,pz,pze,truepvz,truevtxz,pe = array("d",[0]), array("d",[0]), array("d",[0]), array("d",[0]), array("d",[0]), array("d",[0]), array("d",[0]), array("d",[0]), array("d",[0])
  vtxx,vtxy,px,py, pt = array("d",[0]), array("d",[0]), array("d",[0]), array("d", [0]), array("d", [0])
  t.SetBranchAddress("Jpsi_OWNPV_Z",pvz)
  t.SetBranchAddress("Jpsi_OWNPV_ZERR",pvze)
  t.SetBranchAddress("Jpsi_ENDVERTEX_Z",vtxz)
  t.SetBranchAddress("Jpsi_ENDVERTEX_Y",vtxy)
  t.SetBranchAddress("Jpsi_ENDVERTEX_X",vtxx)
#   t.SetBranchAddress("Jpsi_TRUEENDVERTEX_Z",truevtxz)
  t.SetBranchAddress("Jpsi_ENDVERTEX_ZERR",vtxze)
  t.SetBranchAddress("Jpsi_PZ",pz)
  t.SetBranchAddress("Jpsi_PY",py)
  t.SetBranchAddress("Jpsi_PX",px)
  t.SetBranchAddress("Jpsi_MM",mm)
  t.SetBranchAddress("Jpsi_m_scaled",m_scaled)
  t.SetBranchAddress("Jpsi_PE",pe)
  t.SetBranchAddress("Jpsi_PT",pt)
  
#   MCPVs=1
#   mcpvz = array("d",100*[0])
#   #if 0:
#   t.SetBranchAddress("MCPVs",MCPVs)
#   t.SetBranchAddress("MCPVZ",mcpvz)   #[MCPVs]
#   t.SetBranchAddress("Jpsi_TRUEPV_Z",truepvz)
#   t.SetBranchAddress("Jpsi_PZERR",pze)
#   #endif
  nPV = array("i",[1])
  opvx = array("d",100*[0])
  opvy = array("d",100*[0])
  opvz = array("d",100*[0])
  t.SetBranchAddress("nPV",nPV)
  t.SetBranchAddress("PVZ",opvz)   #[nPV]
  t.SetBranchAddress("PVX",opvx)   #[nPV]
  t.SetBranchAddress("PVY",opvy)   #[nPV]
  
  #   mother particles
#   Int_t MID,GMID,GGMID
#   t.SetBranchAddress("Jpsi_MC_MOTHER_ID",MID)
#   t.SetBranchAddress("Jpsi_MC_GD_MOTHER_ID",GMID)
#   t.SetBranchAddress("Jpsi_MC_GD_GD_MOTHER_ID",GGMID)
  
#   int fromB=0
#   nt.Branch("fromB",fromB,"fromB/I")
#   int GoodPV=0
#   nt.Branch("GoodPV",GoodPV,"GoodPV/I")
  tz,tze = array("d",100*[0]), array("d",100*[0])
  nt.Branch("tz",tz,"tz/D")
  nt.Branch("tze",tze,"tze/D")
  
#   double mctz=-10000.,otz=-10000.,ntz=-10000.,mcpvtz=-10000.,rtz=-10000,stz=-10000.
  otz, ntz, rtz, stz = array("d",[-1000.]), array("d",[-1000.]), array("d",[-1000.]), array("d",[-1000.])
#   nt.Branch("mctz",mctz,"mctz/D")
#   nt.Branch("mcpvtz",mcpvtz,"mcpvtz/D")
  nt.Branch("otz",otz,"otz/D")
  nt.Branch("rtz",rtz,"rtz/D")
  nt.Branch("ntz",ntz,"ntz/D")
  nt.Branch("stz",stz,"stz/D") 
  nPVZ = array("d",[0])
  nt.Branch("nPVZ",nPVZ,"nPVZ/D")
  Jpsi_Y = array("d",[0])
  nt.Branch("Jpsi_Y",Jpsi_Y,"Jpsi_Y/D")
  Jpsi_PT = array("d",[0])
  nt.Branch("Jpsi_PT",Jpsi_PT,"Jpsi_PT/D")
  totCandidates = array("i",[0])
  t.SetBranchAddress("totCandidates",totCandidates)
  eventNumber = array("i",[0])
  t.SetBranchAddress("eventNumber",eventNumber)
  runNumber = array("i",[0])
  t.SetBranchAddress("runNumber",runNumber)
  
  r = TRandom()
  next_event= array("i",[0])
  next_run= array("i",[0])
  print t.GetEntries()
  #for i in range(t.GetEntries()):
  for i in range(1000000):
    t.GetEntry(i)
    if(i%1000==0): print i
    if (m_scaled[0] < 2800 or m_scaled[0] > 3200): continue
    #if(totCandidates>3) continue
#     GoodPV=0
#     #if 0 #1: true pv is reconstructed, -1: true pv is reconstructed but not equal to associated pv
#     #2: true pv is not reconstructed, and associated pv is another MCPV
#     for(int ii=0ii<nPVii++){
#       if(fabs(truepvz-opvz[ii])<1.) {
#         GoodPV=1
#         if(fabs(truepvz-pvz)>1) GoodPV=-1
#           break
#       }
#     }
#     if(GoodPV==0){
#       for(int j=0j<MCPVsj++){
#         if(fabs(mcpvz[j]-pvz)<1.) GoodPV=-2
#       }
#     }
#     #endif
    Jpsi_PT[0] = pt[0]
    Jpsi_Y[0] = 0.5*log((pe[0]+pz[0])/(pe[0]-pz[0]))
    tz[0] = (vtxz[0]-pvz[0])*mm[0]/pz[0]*10./3.
    tze[0] = sqrt(vtxze[0]*vtxze[0]+pvze[0]*pvze[0]+(vtxz[0]-pvz[0])*(vtxz[0]-pvz[0])*pze[0]*pze[0]/pz[0]/pz[0])*mm[0]/pz[0]*10./3.
#     mcpvtz = (vtxz-truepvz)*mm/pz*10./3.
#     mctz = (truevtxz-pvz)*mm/pz*10./3.
    vJpsi = TVector3(vtxx[0],vtxy[0],vtxz[0])
    pJpsi = TVector3(px[0],py[0],pz[0])
    sd=999999.
#     #if 0
#     #if(MCPVs==1) stz = -10000.
#     stz = -10000.
#     int nearest_index = -1
#     for(int ii=0ii<nPVii++){
#       if(fabs(opvz[ii]-vtxz)<sd){
#         sd = fabs(opvz[ii]-vtxz)
#         nearest_index=ii
#     }
#     }
#     sd=99999.
#     for(int ii=0ii<nPVii++){
#       if(ii==nearest_index) continue
#       if(fabs(opvz[ii]-vtxz)<sd){
#         sd=fabs(opvz[ii]-vtxz)
#         stz = (vtxz-opvz[ii])*mm/pz*10./3.
#     }
#     }
#     #endif
#     fromB=0
#     if(FromB(MID)||FromB(GMID)||FromB(GGMID)) fromB=1
  
  
  
  
    cur_event = eventNumber[0]
    cur_run = runNumber[0]
    jz = vtxz[0]
    jpz = pz[0]
    for npp in range(5,1000):
      index = (npp+i+5)%(t.GetEntries())
      t.GetEntry(index)
      if(cur_run==runNumber[0] and cur_event==eventNumber[0]): continue #same event
      if(next_run[0]==runNumber[0] and next_event[0]==eventNumber[0]):
        continue #same next event as for previous one
      else:
        next_run[0] = runNumber[0]
        next_event[0] = eventNumber[0]
        break
      
    #if 1
    rtz[0] = 10./3.*(jz-pvz[0])*mm[0]/jpz
    #if 1
    otz[0] = -10000.
    ip = 10000. 
    remove = int(r.Uniform(nPV[0]-0.000001))
    for ii in range(nPV[0]):
      if(ii==remove): continue
      vtxpv = TVector3(opvx[ii],opvy[ii],opvz[ii])
      ip1=IP(vtxpv,vJpsi,pJpsi)
      if(ip1<ip):
        ip = ip1
        otz[0] = 10./3.*(jz-opvz[ii])*mm[0]/jpz

    #endif
    #if 1
    stz[0] = -10000.
    sd = 10000 
    for ii in range(nPV[0]):
      pvztmp = r.Gaus(0,60) 
      if(fabs(pvztmp-jz)<sd):
        sd = fabs(jz-pvztmp)
        stz[0] = 10./3.*(jz-pvztmp)*mm[0]/jpz
    #endif
    #if 1
    ntz[0] = -10000.
    sd = 10000 
    remove = int(r.Uniform(nPV[0]-0.000001))
    for ii in range(nPV[0]):
      if(ii==remove): continue
      #if(fabs(pvz-opvz[ii])<0.00001) continue
      if( fabs(jz-opvz[ii])<sd):
        sd = fabs(jz-opvz[ii])
        ntz[0] = 10./3.*(jz-opvz[ii])*mm[0]/jpz
    #endif
    nPVZ[0] = pvz[0]
    t.GetEntry(i)
    #endif
    nt.Fill()
  nt.Write()
  fn.Close()

AddBr()
