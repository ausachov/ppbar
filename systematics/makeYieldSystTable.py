from array import array
from ROOT import *

gROOT.LoadMacro("../libRooRelBreitWigner/RooRelBreitWigner.cxx+")
gROOT.LoadMacro("../BifurcatedCB/BifurcatedCB.cxx+")

charmDict = {
    "Jpsi":     0,
    "Etac":     1,
}


systDictRunI = {
    "rEtaToJpsi"   :  "pT-dependence of $\\sigma_{\\eta_{c}}/\\sigma_{J/\\psi}$",
    "Chebychev3par":  "Comb. bkg. description",
    "eff_pppi0"    :  "$\\epsilon_{p\\bar{p}\\pi^{0}}$",
    'effPP'        :  'effPP',
    'effPB'        :  'effPB',
    'effBP'        :  'effBP',
    'effBB'        :  'effBB',
    "Cross-talk"   :  "Cross-talk",
    "CB"           :  "Mass resolution model",
    "Gamma34"      :  "Variation of  $\\Gamma_{\\eta_{c}}$"
}

def systDiffRunI(nConfs=[0], nPTBins=[0]):

    
    fout = open("RunIFit.txt","w")
    keys = ["Prompt","FromB"]
    
    for iPT in nPTBins:

        f0 = TFile("../Results/MassFit/RunIMethod/Wksp_MassFit_PT%s_CBase.root"%(iPT),"READ")
        w0 = f0.Get("w")
        f0.Close()

        for key in keys:

            fullKey = 'PT'+str(iPT)+'_'+key

            fout.write("\\begin{frame} \n")
            fout.write("\\frametitle{PT Bin "+str(iPT)+" "+key+"} \n")
            fout.write("  \\begin{table}[H] \n")

            fout.write("  \t \\begin{tabular*}{0.95\\linewidth}{@{\\extracolsep{\\fill}}c|c|c|c} \n")
            fout.write("  & $n_{J/\\psi}$ &  $n_{\\eta_c}$ & $N_{\\eta_c}/N_{J/\\psi}$   \\\\ \\hline \n")

            valJpsi     = w0.var("nJpsi_%s"%(fullKey)).getValV()
            errHiJpsi   = w0.var("nJpsi_%s"%(fullKey)).getErrorHi()
            errLowJpsi  = w0.var("nJpsi_%s"%(fullKey)).getErrorLo()
            errJpsiSyst = 0
            if errHiJpsi < 1e-3:
                errJpsi = -errLowJpsi
            elif errLowJpsi > -1e-3:
                errJpsi = errHiJpsi
            else:
                errJpsi = (errHiJpsi-errLowJpsi)/2.
            
            valEtac     = w0.var("nEta_%s"%(fullKey)).getValV()
            errHiEtac   = w0.var("nEta_%s"%(fullKey)).getErrorHi()
            errLowEtac  = w0.var("nEta_%s"%(fullKey)).getErrorLo()
            errEtacSyst = 0
            if errHiEtac < 1e-3:
                errEtac = -errLowEtac
            elif errLowEtac > -1e-3:
                errEtac = errHiEtac
            else:
                errEtac = (errHiEtac-errLowEtac)/2.

            valEtacRel = w0.var("NEtac_%s"%(key)).getValV()
            errEtacRel = w0.var("NEtac_%s"%(key)).getError()
            errEtacRelSyst = 0


            fout.write( "& %5.1f & %5.1f & %1.3f\\\\ \\hline \n  "%(w0.var("nJpsi_%s"%(fullKey)).getValV(),w0.var("nEta_%s"%(fullKey)).getValV(), valEtacRel))
            fout.write( "& %2.3f & %2.3f & %1.3f \\\\ \\hline \n"%(100*errJpsi/valJpsi,100*errEtac/valEtac, 100*errEtacRel/valEtacRel))
 
            for nConf in nConfs:
                diffJpsi    = 0
                diffEtac    = 0
                diffEtacRel = 0
                if nConf!='Cross-talk':       
                    f = TFile("../Results/MassFit/RunIMethod/Wksp_MassFit_PT{}_C{}.root".format(iPT,nConf),"READ")
                    w = f.Get("w")
                    f.Close()

                    diffJpsi = w.var("nJpsi_%s"%(fullKey)).getValV() - w0.var("nJpsi_%s"%(fullKey)).getValV()
                    diffEtac = w.var("nEta_%s"%(fullKey)).getValV()  - w0.var("nEta_%s"%(fullKey)).getValV()
                    diffEtacRel = w.var("NEtac_%s"%(key)).getValV()-w0.var("NEtac_%s"%(key)).getValV()
                    fout.write( "%s &  %2.2f  "%(systDictRunI[nConf],100*diffJpsi/valJpsi))
                    fout.write( "& %2.2f "%( 100*diffEtac/valEtac))
                    fout.write( "& %2.2f \\\\ \n"%( 100*diffEtacRel/valEtacRel))
                else:
                    nConfsCrossTalk = ['effPP','effPB','effBP','effBB']
                    for nConf in nConfsCrossTalk:
                
                        f = TFile("../Results/MassFit/RunIMethod/Wksp_MassFit_PT{}_C{}.root".format(iPT,nConf),"READ")
                        w = f.Get("w")
                        f.Close()

                        diffJpsi += (w.var("nJpsi_%s"%(fullKey)).getValV()-w0.var("nJpsi_%s"%(fullKey)).getValV())**2
                        diffEtac += (w.var("nEta_%s"%(fullKey)).getValV()-w0.var("nEta_%s"%(fullKey)).getValV())**2
                        diffEtacRel += (w.var("NEtac_%s"%(key)).getValV()-w0.var("NEtac_%s"%(key)).getValV())**2

                    diffJpsi    = (diffJpsi)**0.5
                    diffEtac    = (diffEtac)**0.5
                    diffEtacRel = (diffEtacRel)**0.5

                    fout.write( "%s &  %2.2f  "%(systDictRunI['Cross-talk'],100*diffJpsi/valJpsi))
                    fout.write( "& %2.2f "%( 100*diffEtac/valEtac))
                    fout.write( "& %2.2f \\\\ \n"%( 100*diffEtacRel/valEtacRel))

                errJpsiSyst += diffJpsi**2
                errEtacSyst += diffEtac**2
                errEtacRelSyst += diffEtacRel**2


            errJpsiSyst = errJpsiSyst**0.5
            errEtacSyst = errEtacSyst**0.5
            errEtacRelSyst = errEtacRelSyst**0.5
            
            
            fout.write( "\\hline  \n" )
            fout.write( "Tot Syst & %2.1f & %2.1f & %2.1f \\\\ \\hline \n"%(100*errJpsiSyst/valJpsi,100*errEtacSyst/valEtac, 100*errEtacRelSyst/valEtacRel))
 
            fout.write(" \t \\end{tabular*} \n")
            fout.write("  \\end{table} \n")
            fout.write("\\end{frame} \n")        
    fout.close()


nConfs = [
          "rEtaToJpsi",    
          "Chebychev3par",
          "eff_pppi0",
          "Cross-talk",
          "CB",
          "Gamma34"]

nPTBins = [0, 1, 2, 3, 4]
systDiffRunI(nConfs,nPTBins)
