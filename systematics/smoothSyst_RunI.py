from ROOT import *
from ROOT.TMath import Sqrt
from array import array


ptBins = array("d",[6.5, 8.0, 10.0, 12., 14.0])

keysUnCorr = ["pppi0","rEtacJpsi","Bkg","Cross-Talk"]

# vals       = {"Prompt":[1.069, 1.192, 1.232, 1.924],"FromB":[0.273, 0.410, 0.304, 0.346]}
# stats      = {"Prompt":[19.2,  14.2,  16.3,  18.6], "FromB":[14.0,  7.7,   12.8,  13.6]}

# systUnCorr = {"Prompt":[-9, -9,  -9, -9], "FromB":[-9, -9, -9, -9]}
# systCorr   = {"Prompt":[-9, -9,  -9, -9], "FromB":[-9, -9, -9, -9]}

# systsRaw = {
# 	"pppi0":     {"Prompt":[0.1,0.02,0.1,0.04],"FromB":[0.2,1.4,2.1,0.2],"f":"pol1"},
# 	"rEtacJpsi": {"Prompt":[0.6,0.5,0.1,0.5],  "FromB":[4.6,0.1,2.2,0.5],"f":"pol0"},
# 	"Bkg":       {"Prompt":[1.4,2.9,0.3,7.1],  "FromB":[0.2,2.0,6.1,2.4],"f":"pol1"}
# }
# keysCorr = ["CB","Gamma","polarisation"]
# systsCorr = {
# 	"CB": 			{"Prompt":[2.9, 2.9, 2.9, 2.9],
# 					 "FromB" :[3.1, 3.1, 3.1, 3.1]},

# 	"Gamma": 		{"Prompt":[3.1, 3.1, 3.1, 3.1],
# 					 "FromB" :[3.8, 3.8, 3.8, 3.8]},

# 	"polarisation": {"Prompt":[2.0, 1.8, 1.6, 1.5],
# 					 "FromB" :[0.,  0.,   0.,  0.]},
# }

vals       = {"Prompt":[0.984,1.118,1.239,2.194],   
              "FromB" :[0.263,0.395,0.299,0.348]}
stats      = {"Prompt":[22.3 ,16.1 ,15.5 ,19.9 ],   
              "FromB" :[15.4 ,8.2  ,13.2 ,13.5 ]}

systUnCorr = {"Prompt":[-9, -9,  -9, -9], "FromB":[-9, -9, -9, -9]}
systCorr   = {"Prompt":[-9, -9,  -9, -9], "FromB":[-9, -9, -9, -9]}

systsRaw = {
	"pppi0":     {"Prompt":[0.01,0.17,0.06,0.50],  "FromB":[0.05,0.17,0.17,0.80],"f":"pol1"},
	"rEtacJpsi": {"Prompt":[0.24,0.02,1.90,0.40],  "FromB":[0.29,0.07,1.90,4.72],"f":"pol0"},
	"Bkg":       {"Prompt":[2.64,2.68,0.16,9.10],  "FromB":[1.09,4.36,6.68,3.80],"f":"pol1"}
}

systsSmooth = {}

keysCorr = ["CB","Gamma","polarisation"]
systsCorr = {
	"CB": 			{"Prompt":[2.82,2.82,2.82,2.82],
					 "FromB" :[2.96,2.96,2.96,2.96]},

	"Gamma": 		{"Prompt":[4.55,4.55,4.55,4.55],
					 "FromB" :[3.64,3.64,3.64,3.64]},

	"polarisation": {"Prompt":[2.1, 1.8, 1.6, 1.6],
					 "FromB" :[0.,  0.,   0.,  0.]},
}




nameOutCS = "../Results/rel_PT_RunIMethod.txt"
fCS = open(nameOutCS,"w")

def smooth(systName=""):

	pol1Low = TF1("pol1Low","[0]*x-6.5*[0]",6.5,14.0);

	nPT = len(ptBins)-1
	func = systsRaw[systName]["f"]

	hPrompt = TH1D("Prompt","Prompt",nPT,ptBins)
	hFromB = TH1D("FromB","FromB",nPT,ptBins)
	for i in range(nPT):
		hPrompt.SetBinContent(i+1,0.01*systsRaw[systName]["Prompt"][i])
		hFromB.SetBinContent(i+1,0.01*systsRaw[systName]["FromB"][i])
	hPrompt.Fit(func,"WWE")
	hFromB.Fit(func,"WWE")

	hPrompt.SetMinimum(0.)
	hPrompt.SetMaximum(1.4*hPrompt.GetMaximum())
	hPrompt.GetXaxis().SetTitle("p_{T}, GeV/c")
	hPrompt.GetYaxis().SetTitle("rel. syst. uncert.")
	fPrompt = hPrompt.GetFunction(func)
	if fPrompt.Eval(6.5)<0:
		hPrompt.Fit(pol1Low)
		fPrompt = hPrompt.GetFunction("pol1Low")
	fPrompt.SetLineColor(2)

	hFromB.SetMinimum(0.)
	hFromB.SetMaximum(1.4*hFromB.GetMaximum())
	hFromB.GetXaxis().SetTitle("p_{T}, GeV/c")
	hFromB.GetYaxis().SetTitle("rel. syst. uncert.")
	fFromB = hFromB.GetFunction(func)
	fFromB.SetLineColor(2)

	systPrompt_smooth = []
	systFromB_smooth = []
	for i in range(nPT):
		systPrompt_smooth.append(100*fPrompt.Eval(hPrompt.GetBinCenter(i+1)))
		systFromB_smooth.append(100*fFromB.Eval(hFromB.GetBinCenter(i+1)))


	tex = TLatex()
	c=TCanvas("c","c",1400, 600)
	c.Divide(2,1)
	c.cd(1); hPrompt.Draw(); tex.DrawLatex(0.2, 0.8, "prompt")
	c.cd(2); hFromB.Draw();  tex.DrawLatex(0.2, 0.8, "from-b")
	c.SaveAs("%ssmooth.pdf"%(systName))

	systsSmooth[systName] = {}
	systsSmooth[systName]["Prompt"] = systPrompt_smooth
	systsSmooth[systName]["FromB"] = systFromB_smooth


def fillUnSmoothed():
	systsSmooth["Cross-Talk"] = {}
	systsSmooth["Cross-Talk"]["Prompt"] = [1.9, 1.15, 1.24, 1.39]
	systsSmooth["Cross-Talk"]["FromB"]  = [1.4, 1.3,  1.73, 1.01]



def printTable(nPT, keysUnCorr, keysCorr):
    nameOutTxt = "../Results/MassFit/RunIMethod/systTableRunI_PT%s.txt"%(nPT)
    fo = open(nameOutTxt,"w")

    totalUnCorr = {"Prompt":0, "FromB":0}
    totalCorr   = {"Prompt":0, "FromB":0}
    total       = {"Prompt":0, "FromB":0}

    fo.write("\\begin{frame} \n")
    fo.write("\\frametitle{PT Bin "+str(nPT)+"} \n")
    fo.write("  \\begin{table}[H] \n")
    fo.write("  \t \\begin{tabular*}{0.95\\linewidth}{@{\\extracolsep{\\fill}}c|c|c} \n")
    fo.write("& $N^{p}_{\\eta_c}/N^{p}_{J/\\psi}$ & $N^{b}_{\\eta_c}/N^{b}_{J/\\psi}$  \\\\ \\hline \n")
    for key in keysUnCorr:
		systPrompt = systsSmooth[key]["Prompt"][nPT-1]
		systFromB  = systsSmooth[key]["FromB"][nPT-1]
		fo.write("%s & %2.1f & %2.1f \\\\ \\hline  \n"%(key, systPrompt,systFromB))
		totalUnCorr["Prompt"] += systPrompt*systPrompt
		totalUnCorr["FromB"]  += systFromB*systFromB
    totalUnCorr["Prompt"] = Sqrt(totalUnCorr["Prompt"])
    totalUnCorr["FromB"]  = Sqrt(totalUnCorr["FromB"])
    fo.write("\hline \n")
    fo.write("%s & %2.1f & %2.1f \\\\ \\hline  \n"%("total UnCorr",totalUnCorr["Prompt"],totalUnCorr["FromB"]))
    fo.write("\hline \n")
    for key in keysCorr:
    	systPrompt = systsCorr[key]["Prompt"][nPT-1]
    	systFromB  = systsCorr[key]["FromB"][nPT-1]
    	fo.write("%s & %2.1f & %2.1f \\\\ \\hline  \n"%(key, systPrompt,systFromB))
    	totalCorr["Prompt"] += systPrompt*systPrompt
    	totalCorr["FromB"]  += systFromB*systFromB
    totalCorr["Prompt"] = Sqrt(totalCorr["Prompt"])
    totalCorr["FromB"] = Sqrt(totalCorr["FromB"])
    fo.write("\hline \n")
    fo.write("%s & %2.1f & %2.1f \\\\ \\hline  \n"%("total UnCorr",totalCorr["Prompt"],totalCorr["FromB"]))
    fo.write("\hline \n")

    totalPrompt = Sqrt(totalCorr["Prompt"]*totalCorr["Prompt"] + totalUnCorr["Prompt"]*totalUnCorr["Prompt"])
    totalFromB  = Sqrt(totalCorr["FromB"]*totalCorr["FromB"] + totalUnCorr["FromB"]*totalUnCorr["FromB"])
    fo.write("%s & %2.1f & %2.1f \\\\ \\hline  \n"%("total syst",totalPrompt,totalFromB))
    fo.write("\hline \n")
    fo.write("\hline \n")
    fo.write(" \t \\end{tabular*} \n")
    fo.write("  \\end{table} \n")
    fo.write("\\end{frame} \n")
    fo.close()
    fo.close()

    systUnCorr["Prompt"][nPT-1] = totalUnCorr["Prompt"]
    systUnCorr["FromB"][nPT-1]  = totalUnCorr["FromB"]

    systCorr["Prompt"][nPT-1] = totalCorr["Prompt"]
    systCorr["FromB"][nPT-1]  = totalCorr["FromB"]


    valP = vals["Prompt"][nPT-1]
    valB = vals["FromB"][nPT-1]
    fCS.write("0 0 %2.3f %2.3f \n"%(valP, valB))
    fCS.write("0 0 %2.3f %2.3f \n"%(valP*stats["Prompt"][nPT-1]/100., valB*stats["FromB"][nPT-1]/100.))
    fCS.write("0 0 %2.3f %2.3f \n"%(valP*systUnCorr["Prompt"][nPT-1]/100., valB*systUnCorr["FromB"][nPT-1]/100.))
    fCS.write("0 0 %2.3f %2.3f \n"%(valP*systCorr["Prompt"][nPT-1]/100., valB*systCorr["FromB"][nPT-1]/100.))
    
    






gROOT.LoadMacro("../lhcbStyle.C")
smooth("pppi0")
smooth("rEtacJpsi")
smooth("Bkg")
fillUnSmoothed()

# for key in systsSmooth.keys():
# 	print key, systsSmooth[key]["Prompt"]
# 	print key, systsSmooth[key]["FromB"], "\n"

for iPT in [1,2,3,4]:
	printTable(iPT, keysUnCorr, keysCorr)
	fCS.write("\n")
fCS.close()
