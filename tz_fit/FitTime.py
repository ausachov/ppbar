#using namespace RooFit 
from ROOT import *
from ROOT.TMath import *
from ROOT.Fit import *
from array import array
import numpy as np
from chi2 import *
gROOT.LoadMacro("../libRooRelBreitWigner/RooRelBreitWigner.cxx+")
gROOT.LoadMacro("chi2.cxx")

homeDir = "/users/LHCb/zhovkovska/scripts/"

#RooWorkspace *w = RooWorkspace("w",kTRUE)
hTail =  TH1D("hTail","hTail",100,-10.,10.)
kdeTail = TKDE()
fTail = TF1()

# tz bins in general sample
BinsJpsiTot = array("d", [-10., -0.025, 0.0, 0.025, 0.2, 1.0, 1.5, 2.0, 3.0, 4.0, 10])
BinsEtacTot = array("d", [-10., -0.025, 0.0, 0.025, 0.2, 1.0, 1.5, 2.0, 3.0, 4.0, 10])
# tz bins in pT bins
BinsJpsiInPt = array("d", [-10., -0.025, 0.0, 0.025, 0.1, 1.0, 4.0, 10.])
#BinsJpsiInPt = array("d", [-10., -0.025, 0.0, 0.2, 2.0, 10.])
BinsEtacInPt = array("d", [-10., -0.025, 0.0, 0.2, 2.0, 10.])



iparJpsi = [[0,    # mu
             1,    # beta
             2,    # S1
             3,    # rS
             4,    # tauB
             5,    # NpJpsi
             6,    # NbJpsi
             7],     # NtJpsi/Ntot
             [0, 1, 2, 3, 4, 11, 12, 13],
             [0, 1, 2, 3, 4, 17, 18, 19],
             [0, 1, 2, 3, 4, 23, 24, 25]                         
]


iparEtac = [ [0,    # mu
              1,    # beta
              2,    # S1
              3,    # rS
              4,    # tauB
              5,    # NpJpsi
              6,    # NbJpsi
              7,    # NtJpsi/Ntot
              8,    # NpEtac/NpJpsi
              9,    # NbEtac/NbJpsi
              10],    # NtJpsi
              [0, 1, 2, 3, 4, 11, 12, 13, 14, 15, 16], 
              [0, 1, 2, 3, 4, 17, 18, 19, 20, 21, 22], 
              [0, 1, 2, 3, 4, 23, 24, 25, 26, 27, 28]     
]


def evalTail():

#   TChain * nt=TChain("DecayTree")
#   nt.Add("Reduced_Snd_2012.root")
  
  nt = TChain("DecayTree")
  nt.Add(homeDir+"MC/Etac_MC_Tz_Tail.root")
  nt.Add(homeDir+"MC/Jpsi_MC_Tz_Tail.root")
  
  ntCuted = nt.CopyTree("Jpsi_Tz>-100.0")
  
  print ("Tail tree: OK \n")
  
  nEntries = ntCuted.GetEntries()
  Jpsi_Tz = array("d", [0])
  ntCuted.SetBranchAddress("Jpsi_Tz",Jpsi_Tz)
  
  tz = array("d", nEntries*[0])
  
  for iEn in range(nEntries):
      
    ntCuted.GetEntry(iEn)
    tz[iEn] = Jpsi_Tz
  
    
  print ("DataSet fill status: OK \n")
    
  kdeTail = TKDE(nEntries, tz, -10., 10., "kerneltype:gaussian", 4.0)
  
  print ("KeysPdf fill status: OK \n")



def getHist(hJpsi, hEtac, nPTBin, nConfMass):

    BinsJpsi = array("d", 20*[0])
    BinsEtac = array("d", 20*[0])
  
  
    if(nPTBin == 0):
        name = homeDir+"signal/signal_%d.txt"%nConfMass
        BinsJpsi = BinsJpsiTot
        BinsEtac = BinsEtacTot
    else:
        name = homeDir+"signal/signal_PT%d_%d.txt"%(nPTBin, nConfMass)
        BinsJpsi = BinsJpsiInPt
        BinsEtac = BinsEtacInPt

    f = open(name,"r")
    
    nBins = int(f.readline())
        
    #print nBins    
    hJpsi.SetBins(nBins,BinsJpsi)
    hJpsi.SetDirectory(0)
    for iB in range(nBins):
        
        line = f.readline()
        BinPos, BinErr, JpsiData, JpsiDataErr = float(line.split()[0]), float(line.split()[1]), float(line.split()[2]), float(line.split()[3])
        hJpsi.SetBinContent(iB+1,2*JpsiData/(2*BinErr))
        hJpsi.SetBinError(iB+1,JpsiDataErr/BinErr)
        #print JpsiData, JpsiDataErr 
        
    nBins = int(f.readline())
    hEtac.SetBins(nBins,BinsEtac)
    hEtac.SetDirectory(0)
    for iB in range(nBins):

        line = f.readline()
        BinPos, BinErr, EtacData, EtacDataErr = float(line.split()[0]), float(line.split()[1]), float(line.split()[2]), float(line.split()[3])
        hEtac.SetBinContent(iB+1,2*EtacData/(2*BinErr))
        hEtac.SetBinError(iB+1,EtacDataErr/BinErr)
        #print EtacData, EtacDataErr

    f.close()


# def getTail()
# {
#     f = TFile("Jpsi_Cuts/PPDTime_tail.root","READ")
#     hTail = (TH1D*)f.Get("Jpsi_Tz")
#     hTail.SetDirectory(0)
#     hTail.SetName("hTail")
#     hTail.GetXaxis().SetRangeUser(-10.,10.)
#     hTail.Scale(1/hTail.Integral(hTail.FindBin(-10.),hTail.FindBin(10.))/hTail.GetBinWidth(1))
#     hTail.StatOverflows(kFALSE)
#     std.cout<<"Integral = "<<hTail.Integral(hTail.FindBin(-10.),hTail.FindBin(10.))<<std.endl<<std.endl
#     std.cout<<"Integral = "<<hTail.Integral(hTail.FindBin(-40.),hTail.FindBin(40.))<<std.endl<<std.endl
#     std.cout<<hTail.GetBinWidth(1)<<std.endl
#     f.Close()
# 
# }

def getTail():

  f = TFile ("Mismatch_func.root","READ")
  fTail = f.Get("fTail").Clone()
  f.Close()


#ROOT Fit function (prompt + from-b + tail)

def prompt(Tz, par):

    val = par[1]*(TMath.Gaus(Tz,par[0],par[2]*0.01,True))+(1-par[1])*(TMath.Gaus(Tz,par[0],par[2]*par[3]*0.01,True))
    return val



def secondary(Tz, par):

    bias = (Tz-par[0])*par[4]
    sigma1 = par[2]*0.01
    dec1 = TMath.Exp((TMath.Sq(sigma1)-2*bias)/2./TMath.Sq(par[4]))*(1-TMath.Erf((TMath.Sq(sigma1)-bias)/(TMath.Sqrt(2.)*sigma1*par[4])))/(2.*par[4])
    sigma2 = par[2]*par[3]*0.01
    dec2 = TMath.Exp((TMath.Sq(sigma2)-2*bias)/2./TMath.Sq(par[4]))*(1-TMath.Erf((TMath.Sq(sigma2)-bias)/(TMath.Sqrt(2.)*sigma2*par[4])))/(2.*par[4])

    return par[1]*dec1+(1-par[1])*dec2

def missMatch(Tz):


#     Double_t val = kdeTail.GetValue(Tz)
#     Double_t val = fTail.Eval(Tz)/0.928089
    val = fTail.Eval(Tz)/fTail.Integral(fTail.GetXmin(),fTail.GetXmax(),0.5)
    return val

def fitFunctionJpsi(tz, par):

    Tz = tz[0]
    
    p = prompt(Tz,par)
    b = secondary(Tz,par)
    t = missMatch(Tz)
#     t = fTail.Eval(Tz)/fTail.Integral(fTail.GetXmin(),fTail.GetXmax(),0.2)
    
#    return 2*((par[5]-par[6])*p+par[6]*b+par[7]*t)

    return 2*(par[5]*p+par[6]*b+par[7]*(par[5]+par[6])*t)


def fitFunctionEtac(tz, par):

    Tz = tz[0]

    p = prompt(Tz,par)
    b = secondary(Tz,par)
    t = missMatch(Tz)
#     t = fTail.Eval(Tz)/fTail.Integral(fTail.GetXmin(),fTail.GetXmax(),0.2)
    
#    return 2*(par[5]*par[8]*p+par[6]*par[9]*b+par[10]*t)

    return 2*(par[5]*par[8]*p+par[6]*par[9]*b+(par[5]*par[8] + par[6]*par[9])*par[7]*t)



def getConstPars(parF, nPTBin=0, nConf=0, nConfMass=0):
    
    
    fTemp = TFile(homeDir+"Results/MC/TzFit/MC_TzRes_2016_wksp.root","READ") 
#     fTemp = TFile("Results/MC/TzFit/JpsiVsEtac/MC_Tz_2016_wksp.root","READ") 
    wMC = fTemp.Get("w;1")
    fTemp.Close()
    
    parF[0] = 1/wMC.var("mu").getValV() 
    parF[1] = wMC.var("beta").getValV() 
    parF[2] = 1/wMC.var("S1").getValV() 
    parF[3] = wMC.var("rS").getValV() 
    
    fTemp = TFile(homeDir+"Results/MC/TzFit/MC_TRUE_Tz_2016_wksp.root","READ") 
    wMC = fTemp.Get("w;1")
    fTemp.Close()
    
    parF[4] = 1/wMC.var("tauB").getValV() 
    

    pt = array("d", [7250, 9000, 11000, 13000]) #, 16000]) 
    fTemp = TFile(homeDir+"Results/MC/TzFit/parFit.root","READ") 
    fSigma = fTemp.Get("fSigma")
    fTauB  = fTemp.Get("fTauB")
    fTemp.Close()
    parF[2] *= fSigma.Eval(pt[nPTBin-1]) 
    parF[4] *= fTauB.Eval(pt[nPTBin-1]) 
        
    name = homeDir+"Results/TzFit/systUncert/ROOTSimFit_0_0.root"
        
    fTemp = TFile(name,"READ") 
    fEtac = fTemp.Get("fitEtac")      
        
    if nConf == 3:
                parF[0] = 0
                parF[2] *= fEtac.GetParameter(2) 
                parF[4] *= fEtac.GetParameter(4) 
    else:
                parF[0] = fEtac.GetParameter(0)
                parF[2] = fEtac.GetParameter(2) 
                parF[4] = fEtac.GetParameter(4) 
    fTemp.Close()
    
    return parF


def fitSim(intOpt, nPTBin, nConf, nConfMass):

    #gROOT.Reset()
    gStyle.SetOptStat(0)
    gStyle.SetOptFit(0)

    getNorm(nConf)
    #gROOT.ProcessLine("getNorm(nConf)")
    # tz bins in pT bins
    BinsJpsiInPt = array("d", [-10., -0.025, 0.0, 0.025, 0.1, 1.0, 4.0, 10.])
    #BinsJpsiInPt = array("d", [-10., -0.025, 0.0, 0.2, 2.0, 10.])
    BinsEtacInPt = array("d", [-10., -0.025, 0.0, 0.2, 2.0, 10.])

    Npar = 29
    parF = np.array([0.0, 0.952, 4.0, 3.2, 1.35, 6e4, 7e3, 2e-3,1.0,1.0,0,6e4,7e3,2e-3,1.0,1.0,0,6e4,7e3,2e-3,1.0,1.0,0,6e4,7e3,2e-3,1.0,1.0,0])
    
    parF = getConstPars(parF, 0, nConf, nConfMass)


    nPTBins = 4
    nTzJpsi = len(BinsJpsiInPt)
    nTzEtac = len(BinsEtacInPt)
    

    hJpsi = []
    hEtac = []
    
    hJpsiPull = []
    hEtacPull = []
    
    fitJpsi = []
    fitEtac = []
    
    for iPT in range(nPTBins):
        
        hJpsi.append(TH1D())
        hEtac.append(TH1D())
        hJpsi[iPT].SetName("hJpsi_%s"%(iPT+1))
        hEtac[iPT].SetName("hEtac_%s"%(iPT+1))
        getHist(hJpsi[iPT],hEtac[iPT],iPT+1,nConfMass)        

        hJpsiPull.append(TH1D("hJpsiPull_%s"%(iPT+1), "hJpsiPull", nTzJpsi, 0, nTzJpsi))
        hEtacPull.append(TH1D("hEtacPull_%s"%(iPT+1), "hEtacPull", nTzEtac, 0, nTzEtac))
        
        fitJpsi.append(TF1("fitJpsi%s"%(iPT+1),fitFunctionJpsi,-10.,10.,8))
        fitEtac.append(TF1("fitEtac%s"%(iPT+1),fitFunctionEtac,-10.,10.,11))
    

    getTail()
        
    prJpsi = [] 
    secJpsi = [] 
    tJpsi = []

    prEtac = []
    secEtac = []
    tEtac = []
    
    
    #for iPT in range(nPTBins):

        #prJpsi.append(TF1("prJpsi",(double*x, double *p){ return 2*p[5]*prompt(x,p) },-10.,10.,8))
        #secJpsi.append(TF1("secJpsi",(double*x, double *p){ return 2*p[6]*secondary(x,p) },-10.,10.,8))
        #tJpsi.append(TF1("tJpsi",(double*x, double *p){ return 2*p[7]*(p[5]+p[6])*missMatch(*x) },-10.,10.,8))
        
        #prEtac.append(TF1("prEtac",(double*x, double *p){ return 2*p[5]*p[8]*prompt(x,p) },-10.,10.,11))
        #secEtac.append(TF1("secEtac",(double*x, double *p){ return 2*p[6]*p[9]*secondary(x,p) },-10.,10.,11))
        #tEtac.append(TF1("tEtac",(double*x, double *p){ return 2*(p[5]*p[8] + p[6]*p[9])*p[7]*missMatch(*x) },-10.,10.,11))
        
    
    
    
        
    
    opt = Fit.DataOptions()
    if (intOpt):    opt.fIntegral = True

    rangeJpsi = Fit.DataRange()
    rangeJpsi.SetRange(-10.,10.)

    rangeEtac = Fit.DataRange()
    rangeEtac.SetRange(-10.,10.)

    wfJpsi = []
    wfEtac = []

    dataJpsi = []
    dataEtac = []

    chi2_Jpsi = []
    chi2_Etac = []
    

    for iPT in range(nPTBins):
        
        wfJpsi.append(Math.WrappedMultiTF1(fitJpsi[iPT],1))
        wfEtac.append(Math.WrappedMultiTF1(fitEtac[iPT],1))

        dataJpsi.append(Fit.BinData(opt,rangeJpsi))
        Fit.FillData(dataJpsi[iPT], hJpsi[iPT])

        dataEtac.append(Fit.BinData(opt,rangeEtac))
        Fit.FillData(dataEtac[iPT], hEtac[iPT])

        chi2_Jpsi.append(Fit.Chi2Function(dataJpsi[iPT], wfJpsi[iPT]))
        chi2_Etac.append(Fit.Chi2Function(dataEtac[iPT], wfEtac[iPT]))


    #globalChi2 = GlobalChi2(chi2_Jpsi[0], chi2_Etac[0], chi2_Jpsi[1], chi2_Etac[1], chi2_Jpsi[2], chi2_Etac[2], chi2_Jpsi[3], chi2_Etac[3])
    globalChi2 = GlobalChi2(chi2_Jpsi, chi2_Etac)
    
    fitter = Fit.Fitter()

    NtotJpsi = [81994, 26312, 27854, 16177, 6722, 3375]
    NtotEtac = [75082 ,17121, 28816, 13825, 9360, 2557]


    # create before the parameter settings in order to fix or set range on them
    fitter.Config().SetParamsSettings(Npar,parF)
    # set par names
    fitter.Config().ParSettings(0).SetName("mu")
    fitter.Config().ParSettings(1).SetName("beta")
    fitter.Config().ParSettings(2).SetName("S1")
    fitter.Config().ParSettings(3).SetName("rS")
    fitter.Config().ParSettings(4).SetName("tauB")
#    fitter.Config().ParSettings(5).SetName("NJpsi")
    fitter.Config().ParSettings(5).SetName("NpJpsi_1")
    fitter.Config().ParSettings(6).SetName("NbJpsi_1")
    fitter.Config().ParSettings(7).SetName("NtJpsi_1")
#    fitter.Config().ParSettings(8).SetName("NEtac")
    fitter.Config().ParSettings(8).SetName("NpEtac_rel_1")
    fitter.Config().ParSettings(9).SetName("NbEtac_rel_1")
    fitter.Config().ParSettings(10).SetName("NtEtac_1")
    fitter.Config().ParSettings(11).SetName("NpJpsi_2")
    fitter.Config().ParSettings(12).SetName("NbJpsi_2")
    fitter.Config().ParSettings(13).SetName("NtJpsi_2")
    fitter.Config().ParSettings(14).SetName("NpEtac_rel_2")
    fitter.Config().ParSettings(15).SetName("NbEtac_rel_2")
    fitter.Config().ParSettings(16).SetName("NtEtac_2")
    fitter.Config().ParSettings(17).SetName("NpJpsi_3")
    fitter.Config().ParSettings(18).SetName("NbJpsi_3")
    fitter.Config().ParSettings(19).SetName("NtJpsi_3")
    fitter.Config().ParSettings(20).SetName("NpEtac_rel_3")
    fitter.Config().ParSettings(21).SetName("NbEtac_rel_3")
    fitter.Config().ParSettings(22).SetName("NtEtac_3")
    fitter.Config().ParSettings(23).SetName("NpJpsi_4")
    fitter.Config().ParSettings(24).SetName("NbJpsi_4")
    fitter.Config().ParSettings(25).SetName("NtJpsi_4")
    fitter.Config().ParSettings(26).SetName("NpEtac_rel_4")
    fitter.Config().ParSettings(27).SetName("NbEtac_rel_4")
    fitter.Config().ParSettings(28).SetName("NtEtac_4")
    
    # set limits on parameters
    fitter.Config().ParSettings(0).SetLimits(-1,1)
    fitter.Config().ParSettings(1).SetLimits( 0.0, 1.0)
    fitter.Config().ParSettings(2).SetLimits( 0.5, 20.)
    fitter.Config().ParSettings(3).SetLimits( 1.1, 20.)
    fitter.Config().ParSettings(4).SetLimits( 0.5, 3.5)
    fitter.Config().ParSettings(5).SetLimits( 1.e3,2.0*NtotJpsi[0])
    fitter.Config().ParSettings(6).SetLimits( 5.e2,1.e5)
    fitter.Config().ParSettings(7).SetLimits( 0.0,1.0)
    fitter.Config().ParSettings(8).SetLimits( 0.2,10.0*NtotEtac[0]/NtotJpsi[0])
    fitter.Config().ParSettings(9).SetLimits( 0.0,1.e1)
    fitter.Config().ParSettings(10).SetLimits( 0.e1,5.e3)
    fitter.Config().ParSettings(11).SetLimits( 1.e3,2.0*NtotJpsi[0])
    fitter.Config().ParSettings(12).SetLimits( 5.e2,1.e5)
    fitter.Config().ParSettings(13).SetLimits( 0.0,1.0)
    fitter.Config().ParSettings(14).SetLimits( 0.2,10.0*NtotEtac[0]/NtotJpsi[0])
    fitter.Config().ParSettings(15).SetLimits( 0.0,1.e1)
    fitter.Config().ParSettings(16).SetLimits( 0.e1,5.e3)
    fitter.Config().ParSettings(17).SetLimits( 1.e3,2.0*NtotJpsi[0])
    fitter.Config().ParSettings(18).SetLimits( 5.e2,1.e5)
    fitter.Config().ParSettings(19).SetLimits( 0.0,1.0)
    fitter.Config().ParSettings(20).SetLimits( 0.2,10.0*NtotEtac[0]/NtotJpsi[0])
    fitter.Config().ParSettings(21).SetLimits( 0.0,1.e1)
    fitter.Config().ParSettings(22).SetLimits( 0.e1,5.e3)
    fitter.Config().ParSettings(23).SetLimits( 1.e3,2.0*NtotJpsi[0])
    fitter.Config().ParSettings(24).SetLimits( 5.e2,1.e5)
    fitter.Config().ParSettings(25).SetLimits( 0.0,1.0)
    fitter.Config().ParSettings(26).SetLimits( 0.2,10.0*NtotEtac[0]/NtotJpsi[0])
    fitter.Config().ParSettings(27).SetLimits( 0.0,1.e1)
    fitter.Config().ParSettings(28).SetLimits( 0.e1,5.e3)
    # fix parameters
    
    #fitter.Config().ParSettings(0).Fix()
    #fitter.Config().ParSettings(2).Fix()
    #fitter.Config().ParSettings(4).Fix()
    
    fitter.Config().ParSettings(1).Fix()
    fitter.Config().ParSettings(3).Fix()
    fitter.Config().ParSettings(10).Fix()
    fitter.Config().ParSettings(16).Fix()
    fitter.Config().ParSettings(22).Fix()
    fitter.Config().ParSettings(28).Fix()
    
#    fitter.Config().ParSettings(5).SetValue(NtotJpsi[nPTBin] - fitter.Config().ParSettings(6).Value())
#    fitter.Config().ParSettings(8).SetValue((NtotEtac[nPTBin] - fitter.Config().ParSettings(9).Value()*fitter.Config().ParSettings(6).Value())/fitter.Config().ParSettings(5).Value())


    fitter.Config().MinimizerOptions().SetPrintLevel(4)
    fitter.Config().MinimizerOptions().SetMaxIterations(20000)
    fitter.Config().SetMinimizer("Minuit2","Migrad")
#    fitter.Config().SetMinosErrors(True)

    # fit FCN function directly
    # (specify optionally data size and flag to indicate that is a chi2 fit)
    #fitter.FitFCN(Npar,globalChi2,0,dataEtac1.Size()+dataJpsi1.Size()+dataEtac2.Size()+dataJpsi2.Size(),kTRUE)
    size = 0
    for iP in range(nPTBins):
        size += dataEtac[iPT].Size() + dataJpsi[iPT].Size()

    fitter.FitFCN(globalChi2,parF,size,kTRUE)
    #fitter.Config().ParSettings(0).Release()
    #fitter.Config().ParSettings(9).Release()

    #gROOT.ProcessLine("fitter.FitFCN(Npar, globalChi2, 0, size, 1)")

    fitter.CalculateHessErrors()
    fitter.CalculateMinosErrors()
    
    result = fitter.Result()
    result.Print(std.cout)


    namePlot1 = "/users/LHCb/zhovkovska/scripts/Results/TzFit/systUncert/Sim/ROOTSimFit_J_%d_%d_test.pdf"%(nConf,nConfMass)
    namePlot2 = "/users/LHCb/zhovkovska/scripts/Results/TzFit/systUncert/Sim/ROOTSimFit_E_%d_%d_test.pdf"%(nConf,nConfMass)
    nameRtFl = "/users/LHCb/zhovkovska/scripts/Results/TzFit/systUncert/Sim/ROOTSimFit_%d_%d_test.root"%(nConf,nConfMass)
    nameTxt = "/users/LHCb/zhovkovska/scripts/Results/TzFit/systUncert/Sim/signif_%d_%d_test.txt"%(nConf,nConfMass)
    nameRes = "/users/LHCb/zhovkovska/scripts/Results/TzFit/systUncert/Sim/fitPar_%d_%d_test.txt"%(nConf,nConfMass)


    import os, sys 
    save = os.dup( sys.stdout.fileno() ) 
    newout = file(nameRes, 'w' ) 
    os.dup2( newout.fileno(), sys.stdout.fileno() ) 
    result.Print("v") 
    os.dup2( save, sys.stdout.fileno() ) 
    newout.close()
    
    chi2J = []
    chi2E = []
    f = TFile(nameRtFl,"RECREATE")

    fTot = open(nameTxt,"w")

    
    paramsRes = result.GetParams()
   
    
    c1 = TCanvas("Simfit1","Simultaneous fit of two histograms",10,10,1400,1000)
    c2 = TCanvas("Simfit2","Simultaneous fit of two histograms",10,10,1400,1000)
    c1.Divide(2,4)
    c2.Divide(2,4)
    
    gStyle.SetOptFit(1111)
    
    s1 = [1.09494, 1.01542, 0.96146, 0.94688] 
    tau = [1.06198, 1.05187, 0.99203, 0.88069]
    
    
#     BinsJpsiInPt = array("d", [-10., -0.025, 0.0, 0.025, 0.1, 1.0, 4.0, 10.])
    BinsJpsiInPt = array("d", [-10., -0.025, 0.0, 0.2, 2.0, 10.])
    BinsEtacInPt = array("d", [-10., -0.025, 0.0, 0.2, 2.0, 10.])

    evalChi2 = []
    
    for iPT in range(nPTBins):
        evalChi2.append(EvalChi2(chi2_Jpsi[iPT], chi2_Etac[iPT]))    
        
        pad = c1.cd(2*iPT + (iPT+1)%2)
        xl = pad.GetXlowNDC(); xh = xl + pad.GetWNDC()
        yl = pad.GetYlowNDC(); yh = yl + pad.GetHNDC()
        pad.SetPad(xl,yl-0.125,xh,yh)
        
        fitJpsi[iPT].SetFitResult( result, iparJpsi[iPT])
        fitJpsi[iPT].SetParameter( 2, s1[iPT]*fitJpsi[iPT].GetParameter(2))
        fitJpsi[iPT].SetParameter( 4, tau[iPT]*fitJpsi[iPT].GetParameter(4))
        fitJpsi[iPT].SetRange(rangeJpsi().first, rangeJpsi().second)
        fitJpsi[iPT].SetLineColor(kBlue)
        fitJpsi[iPT].SetNpx(10000)
        fitJpsi[iPT].GetXaxis().SetTitle("t_{z} [ps]")
        fitJpsi[iPT].GetYaxis().SetTitle("Entries")
        fitJpsi[iPT].GetYaxis().SetRangeUser(1e-1,5e6)
        #prJpsi[iPT].SetParameters( fitJpsi[iPT].GetParameters())
        #prJpsi[iPT].SetLineColor(kRed)
        #prJpsi[iPT].SetNpx(10000)
        #prJpsi[iPT].GetYaxis().SetRangeUser(1e-1,5e6)
        #secJpsi[iPT].SetParameters( fitJpsi[iPT].GetParameters())
        #secJpsi[iPT].SetLineColor(kGreen)
        #secJpsi[iPT].SetNpx(10000)
        #secJpsi[iPT].GetYaxis().SetRangeUser(1e-1,5e6)
        #tJpsi[iPT].SetParameters( fitJpsi[iPT].GetParameters())
        #tJpsi[iPT].SetLineColor(kBlack)
        #tJpsi[iPT].SetNpx(10000)
        #tJpsi[iPT].GetYaxis().SetRangeUser(1e-1,5e6)
    #    hJpsi[iPT].SetDrawOption("E3")
        hJpsi[iPT].GetListOfFunctions().Add(fitJpsi[iPT])
        hJpsi[iPT].GetListOfFunctions().Add(prJpsi[iPT])
        hJpsi[iPT].GetListOfFunctions().Add(secJpsi[iPT])
        hJpsi[iPT].GetListOfFunctions().Add(tJpsi[iPT])
        hJpsi[iPT].SetMinimum(1e-1)
        hJpsi[iPT].SetMaximum(5e+6)
        hJpsi[iPT].DrawClone()
        hJpsi[iPT].GetXaxis().SetTitle("t_{z} [ps]")
        hJpsi[iPT].GetYaxis().SetTitle("Entries")
        hJpsi[iPT].GetYaxis().SetRangeUser(1e-1,5e6)
    #    chi2J = hJpsi[iPT].Chisquare(fitJpsi[iPT])
        chi2J1 = fitJpsi[iPT].GetChisquare()
        gPad.SetLogy()

        leg1 = TLegend(0.7,0.7,0.89,0.85)
        leg1.SetHeader("PT Bin %s"%(iPT+1))
        leg1.AddEntry(hJpsi[iPT],"J/#psi data","lep")
        leg1.AddEntry(fitJpsi[iPT],"J/#psi t_{z} fit","l")
        leg1.AddEntry(prJpsi[iPT],"J/#psi prompt","l")
        leg1.AddEntry(secJpsi[iPT],"J/#psi from-b","l")
        leg1.AddEntry(tJpsi[iPT],"J/#psi mismatch","l")
        leg1.Draw()
        
        
        for iTz in range(nTzJpsi):
            
            intErr = 0.
            d =  hJpsi[iPT].GetBinContent(iTz+1) - (fitJpsi[iPT].IntegralOneDim(BinsJpsiInPt[iTz],  BinsJpsiInPt[iTz+1], 1.e-2, 1.e-6, intErr))/(BinsJpsiInPt[iTz+1]-BinsJpsiInPt[iTz])
            err =  hJpsi[iPT].GetBinError(iTz+1)
            hJpsiPull[iPT].SetBinContent(iTz+1, d/err)
        
        
        pad = c1.cd(2*iPT + (iPT+1)%2+2)
        xl = pad.GetXlowNDC(); xh = xl + pad.GetWNDC()
        yl = pad.GetYlowNDC(); yh = yl + pad.GetHNDC()
        pad.SetPad(xl,yl,xh,yh-0.125)
        
        
        hJpsiPull[iPT].SetMinimum(-3.)
        hJpsiPull[iPT].SetMaximum(3.)
        hJpsiPull[iPT].SetFillColor(kBlack)
        hJpsiPull[iPT].SetLineColor(kBlack)
        hJpsiPull[iPT].GetXaxis().SetLabelSize(0.12)
        hJpsiPull[iPT].GetYaxis().SetLabelSize(0.12)
        hJpsiPull[iPT].GetXaxis().SetTitleSize(0.12)
        hJpsiPull[iPT].GetYaxis().SetTitleSize(0.12)
        hJpsiPull[iPT].GetXaxis().SetTitle("Number of t_{z} bin")
        hJpsiPull[iPT].GetYaxis().SetTitle("Pull")
        hJpsiPull[iPT].DrawClone()
        
        pad = c2.cd(2*iPT + (iPT+1)%2)
        xl = pad.GetXlowNDC(); xh = xl + pad.GetWNDC()
        yl = pad.GetYlowNDC(); yh = yl + pad.GetHNDC()
        pad.SetPad(xl,yl-0.125,xh,yh)
        
        fitEtac[iPT].SetFitResult( result, iparEtac[iPT])
        fitEtac[iPT].SetParameter( 2, s1[iPT]*fitJpsi[iPT].GetParameter(2))
        fitEtac[iPT].SetParameter( 4, tau[iPT]*fitJpsi[iPT].GetParameter(4))
        fitEtac[iPT].SetRange(rangeEtac().first, rangeEtac().second)
        fitEtac[iPT].SetLineColor(kBlue)
        fitEtac[iPT].SetNpx(10000)
        fitEtac[iPT].GetXaxis().SetTitle("t_{z} [ps]")
        fitEtac[iPT].GetYaxis().SetTitle("Entries")
        fitEtac[iPT].GetYaxis().SetRangeUser(1e-1,5e6)
#         prEtac[iPT].SetFitResult( result, iparEtac[iPT])
        prEtac[iPT].SetParameters( fitEtac[iPT].GetParameters())
#         prEtac[iPT].SetRange(rangeEtac().first, rangeEtac().second)
        prEtac[iPT].SetLineColor(kRed)
        prEtac[iPT].SetNpx(10000)
        prEtac[iPT].GetYaxis().SetRangeUser(1e-1,5e6)
        secEtac[iPT].SetParameters( fitEtac[iPT].GetParameters())
#         secEtac[iPT].SetFitResult( result, iparEtac[iPT])
#         secEtac[iPT].SetRange(rangeEtac().first, rangeEtac().second)
        secEtac[iPT].SetLineColor(kGreen)
        secEtac[iPT].SetNpx(10000)
        secEtac[iPT].GetYaxis().SetRangeUser(1e-1,5e6)
#         tEtac[iPT].SetFitResult( result, iparEtac[iPT])
        tEtac[iPT].SetParameters( fitEtac[iPT].GetParameters())
#         tEtac[iPT].SetRange(rangeEtac().first, rangeEtac().second)
        tEtac[iPT].SetLineColor(kBlack)
        tEtac[iPT].SetNpx(10000)
        tEtac[iPT].GetYaxis().SetRangeUser(1e-1,5e6)
    #    hEtac[iPT].SetDrawOption("E3")
        hEtac[iPT].GetListOfFunctions().Add(fitEtac[iPT])
        hEtac[iPT].GetListOfFunctions().Add(prEtac[iPT])
        hEtac[iPT].GetListOfFunctions().Add(secEtac[iPT])
        hEtac[iPT].GetListOfFunctions().Add(tEtac[iPT])
        hEtac[iPT].SetMinimum(1e-1)
        hEtac[iPT].SetMaximum(5e+6)
        hEtac[iPT].DrawClone()
        hEtac[iPT].GetXaxis().SetTitle("t_{z} [ps]")
        hEtac[iPT].GetYaxis().SetTitle("Entries")
        hEtac[iPT].GetYaxis().SetRangeUser(1e-1,5e6)
    #    chi2E = hEtac[iPT].Chisquare(fitEtac[iPT])
        gPad.SetLogy()

        leg2 = TLegend(0.7,0.7,0.89,0.85)
        leg2.SetHeader(nameLeg)
        leg2.AddEntry(hEtac[iPT],"#eta_{c} data","lep")
        leg2.AddEntry(fitEtac[iPT],"#eta_{c} t_{z} fit","l")
        leg2.AddEntry(prEtac[iPT],"#eta_{c} prompt","l")
        leg2.AddEntry(secEtac[iPT],"#eta_{c} from-b","l")
        leg2.AddEntry(tEtac[iPT],"#eta_{c} mismatch","l")
        leg2.Draw()
        
        
        for iTz in range(nTzEtac):

            intErr = 0.
            d = hEtac[iPT].GetBinContent(iTz+1) - (fitEtac[iPT].IntegralOneDim( BinsEtacInPt[iTz],  BinsEtacInPt[iTz+1], 1.e-2, 1.e-6, intErr))/( BinsEtacInPt[iTz+1]-BinsEtacInPt[iTz])
            err =  hEtac[iPT].GetBinError(iTz+1)
            hEtacPull[iPT].SetBinContent(iTz+1, d/err)
        
#         c2Pull.cd(iPT+1)
        pad = c2.cd(2*iPT + (iPT+1)%2+2)
        xl = pad.GetXlowNDC(); xh = xl + pad.GetWNDC()
        yl = pad.GetYlowNDC(); yh = yl + pad.GetHNDC()
        pad.SetPad(xl,yl,xh,yh-0.125)
        
        hEtacPull[iPT].SetMinimum(-3.)
        hEtacPull[iPT].SetMaximum(3.)
        hEtacPull[iPT].SetFillColor(kBlack)
        hEtacPull[iPT].SetLineColor(kBlack)
#         hEtacPull[iPT].SetFillStyle(3003)
        hEtacPull[iPT].GetXaxis().SetLabelSize(0.12)
        hEtacPull[iPT].GetYaxis().SetLabelSize(0.12)
        hEtacPull[iPT].GetXaxis().SetTitleSize(0.12)
        hEtacPull[iPT].GetYaxis().SetTitleSize(0.12)
        hEtacPull[iPT].GetXaxis().SetTitle("Number of t_{z} bin")
        hEtacPull[iPT].GetYaxis().SetTitle("Pull")
        hEtacPull[iPT].DrawClone()
        
        
        fitEtac[iPT].Write()
        fitJpsi[iPT].Write()
        
        
        
        fTot.write("nPTBin  %s    chi2  = %s \n"%(iPT+1,chi2J1))
        fTot.write("chi2 J/psi = %s    chi2 etac = %s \n"%(evalChi2[iPT].evalChi2Jpsi(iPT, paramsRes), evalChi2[iPT].evalChi2Etac(iPT, paramsRes)))
        
        ind = iPT*6
        
        fTot.write("%15s %10s %10s %10s %10s"%("N",  fitJpsi[iPT].GetParameter(5), fitJpsi[iPT].GetParameter(6), fitEtac[iPT].GetParameter(8), fitEtac[iPT].GetParameter(9), "\n"))
        fTot.write("%15s %10s %10s %10s %10s"%("dN", fitJpsi[iPT].GetParError(5), fitJpsi[iPT].GetParError(6), fitEtac[iPT].GetParError(8), fitEtac[iPT].GetParError(9), "\n"))
        fTot.write("%15s %10s %10s %10s %10s"%("N/dN", fitJpsi[iPT].GetParameter(5)/fitJpsi[iPT].GetParError(5), fitJpsi[iPT].GetParameter(6)/fitJpsi[iPT].GetParError(6), fitJpsi[iPT].GetParameter(8)/fitEtac[iPT].GetParError(8), fitJpsi[iPT].GetParameter(9)/fitEtac[iPT].GetParError(9), "\n"))
        



    c1.SaveAs(namePlot1)
    c2.SaveAs(namePlot2)
    
    f.Write()
    f.Close()
    
    fTot.close()


def FitTime_Int( intOpt):

    for iPT in range(1, 6):
            fitSim(intOpt,iPT,0,0)


fitSim(True,1,0,0)
