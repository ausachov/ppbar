

class Variable:

    #pass
    #mean = 0.
    #statErr = 0.
    #systErr = 0.
    #systCorrErr = 0.
    #totErr = (statErr**2 + systErr**2 + systCorrErr**2)**0.5

    def __init__(self, meanVal=0., stat=0., syst=0., systCorr=0.):
        self.mean = meanVal
        self.statErr = stat
        self.systErr = syst
        self.systErrCorr = systCorr
        #self.totErr = (self.statErr**2 + self.systErr**2 + self.systCorrErr**2)**0.5
        
    def totalErr(self):
        self.totErr = (self.statErr**2 + self.systErr**2 + self.systCorrErr**2)**0.5
        return self.totErr

    def relativeErr(self):
        self.relErr = self.totalErr()/self.mean
        return self.relErr
