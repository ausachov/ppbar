from ROOT import *
from structErr import *
from array import array
homeDir = "/users/LHCb/zhovkovska/scripts/"

ptBins = array("f",[6.5, 8.0, 10.0, 12., 14.0])

systDictMUncorr = {
    1 : "$\\sigma_{\\eta_{c}}$",
    2:  "$\\sigma_{\\eta_{c}}/\\sigma_{J/\\psi}$",
    4:  "Alt. bkg",
    #5:  "Alt. bkg 2",
    #7:  "mass renorm",
    8:  "$\\epsilon_{p\\bar{p}\\pi^{0}}$",
    #9:  "mass const"

}

systDictMCorr = {
    3:  "Crystal ball",
    6:  "$\\Gamma_{\\eta_{c}}$",
}

systDictTz = {
    1 : "$S_{\\eta_{c}}$",
    2:  "$\\tau_{B}$",
    3:  "$\\mu$",
    4:  "$S_{1}/S_{2}$ and $\\beta$",
}

systDictGen = {
    1:  "polarization",
    
}
#class Variable:
    #pass


def processPT0(nTzs=[0], nMasses=[0], nGen=[0]):

    #gROOT.Reset()

    gStyle.SetOptStat(0)
    gStyle.SetOptTitle(0)

    nConfsTz = len(nTzs)
    nConfsMass = len(nMasses)

    massJpsiSystPrompt, massJpsiSystFromB, massSystPrompt, massSystFromB, tzSystPrompt, tzSystFromB = [], [], [], [], [], []
    #fCS = open(homeDir+"Results/rel_PT_sim.txt","w")
    
    
    nameInFile = homeDir+"Results/TzFit/systUncert/ROOTSimFit_0_0.root"
    fIn0 = TFile(nameInFile,"READ")
    
    fJpsi = fIn0.Get("fitJpsi")
    fEtac = fIn0.Get("fitEtac")
    
    NJpsiPr = []
    NEtacPr = []
    NJpsiSec = []
    NEtacSec =  [] 
    
    NJpsiPr.append(Variable(fJpsi.GetParameter(5), fJpsi.GetParError(5))) 
    NJpsiSec.append(Variable(fJpsi.GetParameter(6), fJpsi.GetParError(6))) 
    NEtacPr.append(Variable(fEtac.GetParameter(8), fEtac.GetParError(8))) 
    NEtacSec.append(Variable(fEtac.GetParameter(9), fEtac.GetParError(9))) 
    
    fIn0.Close()
    
    nameOutTxt = homeDir+"Results/TzFit/systTable.txt"
    fo = open(nameOutTxt,"w")
    
    fo.write("\\begin{frame} \n")
    fo.write("\\frametitle{PT Bin Total} \n")
    fo.write("  \\begin{table}[H] \n")
    fo.write("  \t \\begin{tabular*}{0.95\\linewidth}{@{\\extracolsep{\\fill}}c|c|c|c|c} \n")
    fo.write("  & $N^{p}_{J/\\psi}$ & $N^{b}_{J/\\psi}$ & $N^{p}_{\\eta_c}/N^{p}_{J/\\psi}$ & $N^{b}_{\\eta_c}/N^{b}_{J/\\psi}$  \\\\ \\hline \n")


    fo.write(" & %5.2f & %5.2f & %2.3f & %2.3f \\\\ \\hline  \n"%( NJpsiPr[0].mean, NJpsiSec[0].mean, NEtacPr[0].mean, NEtacSec[0].mean))
    fo.write(" & %2.1f & %2.1f & %2.1f & %2.1f \\\\ \\hline  \n"%( 100*NJpsiPr[0].statErr/NJpsiPr[0].mean, 100*NJpsiSec[0].statErr/NJpsiSec[0].mean, 100*NEtacPr[0].statErr/NEtacPr[0].mean, 100*NEtacSec[0].statErr/NEtacSec[0].mean))
    #fo.write(" & %2.1f & %2.1f & %2.1f & %2.1f \\\\ \\hline  \n"%( NJpsiPr[0].statErr, NJpsiSec[0].statErr, NEtacPr[0].statErr, NEtacSec[0].statErr))

        
    for nConfM in nMasses:

        if nConfM==2:
            massSystPrompt.append(0.)
            massSystFromB.append(0.)

            NJpsiPr.append(Variable(NJpsiPr[0].mean, 0)) 
            NJpsiSec.append(Variable(NJpsiPr[0].mean, 0)) 
            NEtacPr.append(Variable(NJpsiPr[0].mean, 0)) 
            NEtacSec.append(Variable(NJpsiPr[0].mean, 0)) 

        else:
            idxM = nMasses.index(nConfM)
            fIn = TFile(homeDir+"Results/TzFit/systUncert/ROOTSimFit_0_%d.root"%(nConfM),"READ")
            fJpsi = fIn.Get("fitJpsi")
            fEtac = fIn.Get("fitEtac")
        
                
            NJpsiPr.append(Variable(fJpsi.GetParameter(5), fJpsi.GetParError(5))) 
            NJpsiSec.append(Variable(fJpsi.GetParameter(6), fJpsi.GetParError(6))) 
            NEtacPr.append(Variable(fEtac.GetParameter(8), fEtac.GetParError(8))) 
            NEtacSec.append(Variable(fEtac.GetParameter(9), fEtac.GetParError(9))) 

            NJpsiPr[0].systErr += (NJpsiPr[idxM + 1].mean-NJpsiPr[0].mean)**2
            NJpsiSec[0].systErr += (NJpsiSec[idxM + 1].mean-NJpsiSec[0].mean)**2

            NEtacPr[0].systErr += (NEtacPr[idxM + 1].mean-NEtacPr[0].mean)**2
            NEtacSec[0].systErr += (NEtacSec[idxM + 1].mean-NEtacSec[0].mean)**2

            massJpsiSystPrompt.append(abs(NJpsiPr[idxM + 1].mean-NJpsiPr[0].mean)/NJpsiPr[0].mean)
            massJpsiSystFromB.append(abs(NJpsiSec[idxM + 1].mean-NJpsiSec[0].mean)/NJpsiSec[0].mean)
            massSystPrompt.append(abs(NEtacPr[idxM + 1].mean-NEtacPr[0].mean)/NEtacPr[0].mean)
            massSystFromB.append(abs(NEtacSec[idxM + 1].mean-NEtacSec[0].mean)/NEtacSec[0].mean)
            
            fIn.Close()
            if nConfM in systDictMUncorr:
                fo.write("%s & %2.1f & %2.1f & %2.1f & %2.1f"%(systDictMUncorr[nConfM], 100*(NJpsiPr[idxM + 1].mean-NJpsiPr[0].mean)/NJpsiPr[0].mean, 100*(NJpsiSec[idxM + 1].mean-NJpsiSec[0].mean)/NJpsiSec[0].mean, 100*(NEtacPr[idxM + 1].mean-NEtacPr[0].mean)/NEtacPr[0].mean, 100*(NEtacSec[idxM + 1].mean-NEtacSec[0].mean)/NEtacSec[0].mean))
            else:
                fo.write("%s & %2.1f & %2.1f & %2.1f & %2.1f"%(systDictMCorr[nConfM], 100*(NJpsiPr[idxM + 1].mean-NJpsiPr[0].mean)/NJpsiPr[0].mean, 100*(NJpsiSec[idxM + 1].mean-NJpsiSec[0].mean)/NJpsiSec[0].mean, 100*(NEtacPr[idxM + 1].mean-NEtacPr[0].mean)/NEtacPr[0].mean, 100*(NEtacSec[idxM + 1].mean-NEtacSec[0].mean)/NEtacSec[0].mean))
                
            fo.write(" \\\\   \n ")

    fo.write(" \\hline   \n ")

    #for nConfTz in nTzs:

        #idxTz = nTzs.index(nConfTz)
        #fIn = TFile(homeDir+"Results/TzFit/systUncert/ROOTSimFit_%d_0.root"%(nConfTz),"READ")
        #fJpsi = fIn.Get("fitJpsi")
        #fEtac = fIn.Get("fitEtac")

        #NJpsiPr.append(Variable(fJpsi.GetParameter(5), fJpsi.GetParError(5))) 
        #NJpsiSec.append(Variable(fJpsi.GetParameter(6), fJpsi.GetParError(6))) 
        #NEtacPr.append(Variable(fEtac.GetParameter(8), fEtac.GetParError(8))) 
        #NEtacSec.append(Variable(fEtac.GetParameter(9), fEtac.GetParError(9))) 

        #NJpsiPr[0].systErr += (NJpsiPr[nConfsMass + idxTz +1].mean-NJpsiPr[0].mean)**2
        #NJpsiSec[0].systErr += (NJpsiSec[nConfsMass + idxTz +1].mean-NJpsiSec[0].mean)**2

        #NEtacPr[0].systErr += (NEtacPr[nConfsMass + idxTz +1].mean-NEtacPr[0].mean)**2
        #NEtacSec[0].systErr += (NEtacSec[nConfsMass + idxTz +1].mean-NEtacSec[0].mean)**2

        #tzSystPrompt.append(abs(NEtacPr[nConfsMass + idxTz + 1].mean-NEtacPr[0].mean)/NEtacPr[0].mean)
        #tzSystFromB.append(abs(NEtacSec[nConfsMass + idxTz + 1].mean-NEtacSec[0].mean)/NEtacSec[0].mean)

        #fIn.Close()

        #fo.write("%s & %2.1f & %2.1f & %2.1f & %2.1f  "%(systDictTz[nConfTz], NJpsiPr[nConfsMass + idxTz +1].mean-NJpsiPr[0].mean, NJpsiSec[nConfsMass + idxTz +1].mean-NJpsiSec[0].mean, NEtacPr[nConfsMass + idxTz +1].mean-NEtacPr[0].mean, NEtacSec[nConfsMass + idxTz +1].mean-NEtacSec[0].mean))
        #fo.write(" \\\\   \n ")

        #fo.write(" \\hline   \n ")

    nGen = [1]
    for nG in nGen:

        idxG = nGen.index(nG)
        fIn = TFile(homeDir+"Results/effPolarization.root","READ")
        hEff = fIn.Get("histEffTot")

        NJpsiPr[0].systErr += abs(hEff.GetBinContent(1)*NJpsiPr[0].mean)**2

        NEtacPr[0].systErr += abs(hEff.GetBinContent(1)*NEtacPr[0].mean)**2


        #fo.write("%s & %2.1f & %2.1f & %2.1f & %2.1f  "%(systDictGen[nG], hEff.GetBinContent(1)*NJpsiPr[0].mean, 0., hEff.GetBinContent(1)*NEtacPr[0].mean, 0))
        fo.write("%s & %2.1f & %2.1f & %2.1f & %2.1f  "%(systDictGen[nG], 100*hEff.GetBinContent(1), 0., 100*hEff.GetBinContent(1), 0))
        fo.write(" \\\\   \n ")

    fIn.Close()


    NJpsiPr[0].systErr = NJpsiPr[0].systErr**0.5
    NJpsiSec[0].systErr = NJpsiSec[0].systErr**0.5
    NEtacPr[0].systErr = NEtacPr[0].systErr**0.5
    NEtacSec[0].systErr = NEtacSec[0].systErr**0.5

        
    fo.write(" \\hline  \n")
    #fo.write("Tot Syst  & %2.1f & %2.1f & %2.1f & %2.1f"%( NJpsiPr[0].systErr, NJpsiSec[0].systErr, NEtacPr[0].systErr, NEtacSec[0].systErr))
    fo.write("Tot Syst  & %2.1f & %2.1f & %2.1f & %2.1f"%( 100*NJpsiPr[0].systErr/NJpsiPr[0].mean, 100*NJpsiSec[0].systErr/NJpsiSec[0].mean, 100*NEtacPr[0].systErr/NEtacPr[0].mean, 100*NEtacSec[0].systErr/NEtacSec[0].mean))
    fo.write(" \\\\   \n")

    fo.write(" \t \\end{tabular*} \n")
    fo.write("  \\end{table} \n")
    fo.write("\\end{frame} \n")
    fo.close()
        
    #fCS.write(" %5.2f  %5.2f  %2.3f  %2.3f  \n"%( NJpsiPr[0].mean, NJpsiSec[0].mean, NEtacPr[0].mean, NEtacSec[0].mean))
    #fCS.write(" %5.2f  %5.2f  %2.3f  %2.3f  \n"%( NJpsiPr[0].statErr, NJpsiSec[0].statErr, NEtacPr[0].statErr, NEtacSec[0].statErr))
    #fCS.write(" %5.2f  %5.2f  %2.3f  %2.3f  \n \n"%( NJpsiPr[0].systErr, NJpsiSec[0].systErr, NEtacPr[0].systErr, NEtacSec[0].systErr))
        
    print "systematic's calculated successfully"
    
    #fCS.close()
    return massJpsiSystPrompt, massJpsiSystFromB, massSystPrompt, massSystFromB, tzSystPrompt, tzSystFromB


def process(nPTs=[0], nTzs=[0], nMasses=[0], nGen=[0]):

    #gROOT.Reset()

    gStyle.SetOptStat(0)
    gStyle.SetOptTitle(0)

    nConfsTz = len(nTzs)
    nConfsMass = len(nMasses)

    fCS = open(homeDir+"Results/rel_PT_sim.txt","w")

    hMassPr = []
    hMassSec = []
    hTzPr = []
    hTzSec = []
    fMassPr = []
    fMassSec = []
    fTzPr = []
    fTzSec = []

    NJpsiPr = []
    NEtacPr = []
    NJpsiSec = []
    NEtacSec =  []

    massJpsiSystPrompt, massJpsiSystFromB, massSystPrompt, massSystFromB, tzSystPrompt, tzSystFromB =  processPT0([0], nMasses, nGen)

    for nConfM in nMasses:
        hMassPr.append(TH1D("hMassPr%s"%(nConfM),"mass syst", 4, ptBins))
        hMassSec.append(TH1D("hMassSec%s"%(nConfM),"mass syst", 4, ptBins))
        fMassPr.append(TF1("fMassPr%s"%(nConfM),"pol0", ptBins[0], ptBins[4]))
        fMassSec.append(TF1("fMassSec%s"%(nConfM),"pol0", ptBins[0], ptBins[4]))
    for nConfTz in nTzs:
        hTzPr.append(TH1D("hTzPr%s"%(nConfTz),"tz syst", 4, ptBins))
        hTzSec.append(TH1D("hTzSec%s"%(nConfTz),"tz syst", 4, ptBins))
        fTzPr.append(TF1("fTzPr%s"%(nConfM),"pol0", ptBins[0], ptBins[4]))
        fTzSec.append(TF1("fTzSec%s"%(nConfM),"pol0", ptBins[0], ptBins[4]))



    for nPT in nPTs:

        nameInFile = homeDir+"Results/TzFit/systUncert/Sim/ROOTSimFit_0_0.root"
        fIn0 = TFile(nameInFile,"READ")
        
        fJpsi = fIn0.Get("fitJpsi%s"%(nPT))
        fEtac = fIn0.Get("fitEtac%s"%(nPT))

        NJpsiPr.append([])
        NEtacPr.append([])
        NJpsiSec.append([])
        NEtacSec.append([]) 

        NJpsiPr[nPT-1].append(Variable(fJpsi.GetParameter(5), fJpsi.GetParError(5))) 
        NJpsiSec[nPT-1].append(Variable(fJpsi.GetParameter(6), fJpsi.GetParError(6))) 
        NEtacPr[nPT-1].append(Variable(fEtac.GetParameter(8), fEtac.GetParError(8))) 
        NEtacSec[nPT-1].append(Variable(fEtac.GetParameter(9), fEtac.GetParError(9))) 

        #NJpsiPr[nPT-1][0].mean = fJpsi0.GetParameter(5)
        #NJpsiPr[nPT-1][0].statErr = fJpsi0.GetParError(5)
        #NJpsiSec[nPT-1][0].mean = fJpsi0.GetParameter(6)
        #NJpsiSec[nPT-1][0].statErr = fJpsi0.GetParError(6)

        #NEtacPr[nPT-1][0].mean = fEtac0.GetParameter(8)
        #NEtacPr[nPT-1][0].statErr = fEtac0.GetParError(8)
        #NEtacSec[nPT-1][0].mean = fEtac0.GetParameter(9)
        #NEtacSec[nPT-1][0].statErr = fEtac0.GetParError(9)

        fIn0.Close()

        #nameOutTxt = homeDir+"Results/TzFit/systTable_PT%s.txt"%(nPT)
        #fo = open(nameOutTxt,"w")

        #fo.write("\\begin{frame} \n")
        #fo.write("\\frametitle{PT Bin "+str(nPT)+"} \n")
        #fo.write("  \\begin{table}[H] \n")
        #fo.write("  \t \\begin{tabular*}{0.95\\linewidth}{@{\\extracolsep{\\fill}}c|c|c|c|c} \n")
        #fo.write("  & $N^{p}_{J/\\psi}$ & $N^{b}_{J/\\psi}$ & $N^{p}_{\\eta_c}/N^{p}_{J/\\psi}$ & $N^{b}_{\\eta_c}/N^{b}_{J/\\psi}$  \\\\ \\hline \n")


        #fo.write(" & %5.2f & %5.2f & %2.3f & %2.3f \\\\ \\hline  \n"%( NJpsiPr[nPT-1][0].mean, NJpsiSec[nPT-1][0].mean, NEtacPr[nPT-1][0].mean, NEtacSec[nPT-1][0].mean))
        #fo.write(" & %5.2f & %5.2f & %2.3f & %2.3f \\\\ \\hline  \n"%( NJpsiPr[nPT-1][0].statErr, NJpsiSec[nPT-1][0].statErr, NEtacPr[nPT-1][0].statErr, NEtacSec[nPT-1][0].statErr))

        for nConfM in nMasses:

            if nConfM in systDictMUncorr:
                idxM = nMasses.index(nConfM)
                fIn = TFile(homeDir+"Results/TzFit/systUncert/Sim/ROOTSimFit_0_%d.root"%(nConfM),"READ")
                fJpsi = fIn.Get("fitJpsi%s"%(nPT))
                fEtac = fIn.Get("fitEtac%s"%(nPT))
        
                
                NJpsiPr[nPT-1].append(Variable(fJpsi.GetParameter(5), fJpsi.GetParError(5))) 
                NJpsiSec[nPT-1].append(Variable(fJpsi.GetParameter(6), fJpsi.GetParError(6))) 
                NEtacPr[nPT-1].append(Variable(fEtac.GetParameter(8), fEtac.GetParError(8))) 
                NEtacSec[nPT-1].append(Variable(fEtac.GetParameter(9), fEtac.GetParError(9))) 

                NJpsiPr[nPT-1][idxM + 1].systErr = (NJpsiPr[nPT-1][idxM + 1].mean-NJpsiPr[nPT-1][0].mean)
                NJpsiSec[nPT-1][idxM + 1].systErr = (NJpsiSec[nPT-1][idxM + 1].mean-NJpsiSec[nPT-1][0].mean)

                NEtacPr[nPT-1][idxM + 1].systErr = (NEtacPr[nPT-1][idxM + 1].mean-NEtacPr[nPT-1][0].mean)
                NEtacSec[nPT-1][idxM + 1].systErr = (NEtacSec[nPT-1][idxM + 1].mean-NEtacSec[nPT-1][0].mean)

                NJpsiPr[nPT-1][0].systErr += (NJpsiPr[nPT-1][idxM + 1].mean-NJpsiPr[nPT-1][0].mean)**2
                NJpsiSec[nPT-1][0].systErr += (NJpsiSec[nPT-1][idxM + 1].mean-NJpsiSec[nPT-1][0].mean)**2

                NEtacPr[nPT-1][0].systErr += (NEtacPr[nPT-1][idxM + 1].mean-NEtacPr[nPT-1][0].mean)**2
                NEtacSec[nPT-1][0].systErr += (NEtacSec[nPT-1][idxM + 1].mean-NEtacSec[nPT-1][0].mean)**2


                fIn.Close()
                
                hMassPr[idxM].SetBinContent(nPT, abs(NEtacPr[nPT-1][idxM + 1].mean-NEtacPr[nPT-1][0].mean)/NEtacPr[nPT-1][0].mean)
                hMassSec[idxM].SetBinContent(nPT, abs(NEtacSec[nPT-1][idxM + 1].mean-NEtacSec[nPT-1][0].mean)/NEtacSec[nPT-1][0].mean)
                fMassPr[idxM].SetParameter(0, massSystPrompt[idxM])
                fMassSec[idxM].SetParameter(0, massSystFromB[idxM])
                
                #fo.write("%s & %5.2f & %5.2f & %2.3f & %2.3f"%(systDictMUncorr[nConfM], NJpsiPr[nPT-1][idxM + 1].mean-NJpsiPr[nPT-1][0].mean, NJpsiSec[nPT-1][idxM + 1].mean-NJpsiSec[nPT-1][0].mean, NEtacPr[nPT-1][idxM + 1].mean-NEtacPr[nPT-1][0].mean, NEtacSec[nPT-1][idxM + 1].mean-NEtacSec[nPT-1][0].mean))
                #fo.write(" \\\\   \n ")
                
            elif nConfM in systDictMCorr:
                idxM = nMasses.index(nConfM)
                NJpsiPr[nPT-1].append(Variable(fJpsi.GetParameter(5), fJpsi.GetParError(5))) 
                NJpsiSec[nPT-1].append(Variable(fJpsi.GetParameter(6), fJpsi.GetParError(6))) 
                NEtacPr[nPT-1].append(Variable(fEtac.GetParameter(8), fEtac.GetParError(8))) 
                NEtacSec[nPT-1].append(Variable(fEtac.GetParameter(9), fEtac.GetParError(9))) 


                NJpsiPr[nPT-1][idxM + 1].systErrCorr = abs(massJpsiSystPrompt[idxM]*NJpsiPr[nPT-1][0].mean)
                NJpsiSec[nPT-1][idxM + 1].systErrCorr = abs(massJpsiSystFromB[idxM]*NJpsiSec[nPT-1][0].mean)

                NEtacPr[nPT-1][idxM + 1].systErrCorr = abs(massSystPrompt[idxM]*NEtacPr[nPT-1][0].mean)
                NEtacSec[nPT-1][idxM + 1].systErrCorr = abs(massSystFromB[idxM]*NEtacSec[nPT-1][0].mean)

                NJpsiPr[nPT-1][0].systErrCorr += abs(massJpsiSystPrompt[idxM]*NJpsiPr[nPT-1][0].mean)**2
                NJpsiSec[nPT-1][0].systErrCorr  += abs(massJpsiSystFromB[idxM]*NJpsiSec[nPT-1][0].mean)**2

                NEtacPr[nPT-1][0].systErrCorr  += abs(massSystPrompt[idxM]*NEtacPr[nPT-1][0].mean)**2
                NEtacSec[nPT-1][0].systErrCorr  += abs(massSystFromB[idxM]*NEtacSec[nPT-1][0].mean)**2

                #fo.write("%s & %5.2f & %5.2f & %2.3f & %2.3f  "%(systDictMCorr[nConfM], massJpsiSystPrompt[idxM]*NJpsiPr[nPT-1][0].mean, massJpsiSystFromB[idxM]*NJpsiSec[nPT-1][0].mean, massSystPrompt[idxM]*NEtacPr[nPT-1][0].mean, massSystFromB[idxM]*NEtacSec[nPT-1][0].mean))
                #fo.write(" \\\\   \n ")
            else:
                NJpsiPr[nPT-1].append(Variable(NJpsiPr[nPT-1][0].mean, 0)) 
                NJpsiSec[nPT-1].append(Variable(NJpsiPr[nPT-1][0].mean, 0)) 
                NEtacPr[nPT-1].append(Variable(NJpsiPr[nPT-1][0].mean, 0)) 
                NEtacSec[nPT-1].append(Variable(NJpsiPr[nPT-1][0].mean, 0)) 

        #fo.write(" \\hline   \n ")

        for nConfTz in nTzs:

            idxTz = nTzs.index(nConfTz)
            fIn = TFile(homeDir+"Results/TzFit/systUncert/Sim/ROOTSimFit_%d_0.root"%(nConfTz),"READ")
            fJpsi = fIn.Get("fitJpsi%s"%(nPT))
            fEtac = fIn.Get("fitEtac%s"%(nPT))

            NJpsiPr[nPT-1].append(Variable(fJpsi.GetParameter(5), fJpsi.GetParError(5))) 
            NJpsiSec[nPT-1].append(Variable(fJpsi.GetParameter(6), fJpsi.GetParError(6))) 
            NEtacPr[nPT-1].append(Variable(fEtac.GetParameter(8), fEtac.GetParError(8))) 
            NEtacSec[nPT-1].append(Variable(fEtac.GetParameter(9), fEtac.GetParError(9))) 

            NJpsiPr[nPT-1][nConfsMass + idxTz + 1].systErr = (NJpsiPr[nPT-1][nConfsMass + idxTz + 1].mean-NJpsiPr[nPT-1][0].mean)
            NJpsiSec[nPT-1][nConfsMass + idxTz + 1].systErr = (NJpsiSec[nPT-1][nConfsMass + idxTz + 1].mean-NJpsiSec[nPT-1][0].mean)

            NEtacPr[nPT-1][nConfsMass + idxTz + 1].systErr = (NEtacPr[nPT-1][nConfsMass + idxTz + 1].mean-NEtacPr[nPT-1][0].mean)
            NEtacSec[nPT-1][nConfsMass + idxTz + 1].systErr = (NEtacSec[nPT-1][nConfsMass + idxTz + 1].mean-NEtacSec[nPT-1][0].mean)


            NJpsiPr[nPT-1][0].systErr += (NJpsiPr[nPT-1][nConfsMass + idxTz + 1].mean-NJpsiPr[nPT-1][0].mean)**2
            NJpsiSec[nPT-1][0].systErr += (NJpsiSec[nPT-1][nConfsMass + idxTz + 1].mean-NJpsiSec[nPT-1][0].mean)**2

            NEtacPr[nPT-1][0].systErr += (NEtacPr[nPT-1][nConfsMass + idxTz + 1].mean-NEtacPr[nPT-1][0].mean)**2
            NEtacSec[nPT-1][0].systErr += (NEtacSec[nPT-1][nConfsMass + idxTz + 1].mean-NEtacSec[nPT-1][0].mean)**2

            fIn.Close()

            hTzPr[idxTz].SetBinContent(nPT, abs(NEtacPr[nPT-1][nConfsMass + idxTz + 1].mean-NEtacPr[nPT-1][0].mean)/NEtacPr[nPT-1][0].mean)
            hTzSec[idxTz].SetBinContent(nPT, abs(NEtacSec[nPT-1][nConfsMass + idxTz + 1].mean-NEtacSec[nPT-1][0].mean)/NEtacSec[nPT-1][0].mean)
            #fTzPr[idxM].SetParameter(0, tzSystPrompt[idxTz])
            #fTzSec[idxM].SetParameter(0, tzSystFromB[idxTz])

            #fo.write("%s & %5.2f & %5.2f & %2.3f & %2.3f  "%(systDictTz[nConfTz], NJpsiPr[nPT-1][nConfsMass + idxTz +1].mean-NJpsiPr[nPT-1][0].mean, NJpsiSec[nPT-1][nConfsMass + idxTz +1].mean-NJpsiSec[nPT-1][0].mean, NEtacPr[nPT-1][nConfsMass + idxTz +1].mean-NEtacPr[nPT-1][0].mean, NEtacSec[nPT-1][nConfsMass + idxTz +1].mean-NEtacSec[nPT-1][0].mean))
            #fo.write(" \\\\   \n ")

        #fo.write(" \\hline   \n ")


        NJpsiPr[nPT-1][0].systErr = NJpsiPr[nPT-1][0].systErr**0.5
        NJpsiSec[nPT-1][0].systErr = NJpsiSec[nPT-1][0].systErr**0.5
        NEtacPr[nPT-1][0].systErr = NEtacPr[nPT-1][0].systErr**0.5
        NEtacSec[nPT-1][0].systErr = NEtacSec[nPT-1][0].systErr**0.5

        #fo.write(" \\hline  \n")
        #fo.write("Tot Syst Uncorr & %5.2f & %5.2f & %2.3f & %2.3f"%( NJpsiPr[nPT-1][0].systErr, NJpsiSec[nPT-1][0].systErr, NEtacPr[nPT-1][0].systErr, NEtacSec[nPT-1][0].systErr))
        #fo.write(" \\\\   \n")
        #fo.write(" \\hline  \n")


        #for nG in nGen:

        fIn = TFile(homeDir+"Results/effPolarization.root","READ")
        hEff = fIn.Get("histEff")

        NJpsiPr[nPT-1].append(Variable(fJpsi.GetParameter(5), fJpsi.GetParError(5))) 
        NJpsiSec[nPT-1].append(Variable(fJpsi.GetParameter(6), fJpsi.GetParError(6))) 
        NEtacPr[nPT-1].append(Variable(fEtac.GetParameter(8), fEtac.GetParError(8))) 
        NEtacSec[nPT-1].append(Variable(fEtac.GetParameter(9), fEtac.GetParError(9))) 

        #NJpsiPr[nPT-1][0].systErr += abs(hEff.GetBinContent(nPT)*NJpsiPr[nPT-1][0].mean)

        #NEtacPr[nPT-1][0].systErr += abs(hEff.GetBinContent(nPT)*NEtacPr[nPT-1][0].mean)

        NJpsiPr[nPT-1][nConfsMass + nConfsTz + 1].systErrCorr = abs(hEff.GetBinContent(nPT)*NJpsiPr[nPT-1][0].mean)
        
        NEtacPr[nPT-1][nConfsMass + nConfsTz + 1].systErrCorr = abs(hEff.GetBinContent(nPT)*NEtacPr[nPT-1][0].mean)
        
        NJpsiPr[nPT-1][0].systErrCorr += abs(hEff.GetBinContent(nPT)*NJpsiPr[nPT-1][0].mean)**2

        NEtacPr[nPT-1][0].systErrCorr += abs(hEff.GetBinContent(nPT)*NEtacPr[nPT-1][0].mean)**2

        #fo.write("%s & %5.2f & %5.2f & %2.3f & %2.3f  "%(systDictGen[1], hEff.GetBinContent(nPT)*NJpsiPr[nPT-1][0].mean, 0., hEff.GetBinContent(nPT)*NEtacPr[nPT-1][0].mean, 0))
        #fo.write(" \\\\   \n ")

        fIn.Close()


        NJpsiPr[nPT-1][0].systErrCorr = NJpsiPr[nPT-1][0].systErrCorr**0.5
        NJpsiSec[nPT-1][0].systErrCorr = NJpsiSec[nPT-1][0].systErrCorr**0.5
        NEtacPr[nPT-1][0].systErrCorr = NEtacPr[nPT-1][0].systErrCorr**0.5
        NEtacSec[nPT-1][0].systErrCorr = NEtacSec[nPT-1][0].systErrCorr**0.5
        
        #fo.write(" \\hline  \n")
        #fo.write("Tot Syst Corr & %5.2f & %5.2f & %2.3f & %2.3f"%( NJpsiPr[nPT-1][0].systErr, NJpsiSec[nPT-1][0].systErr, NEtacPr[nPT-1][0].systErr, NEtacSec[nPT-1][0].systErr))
        #fo.write(" \\\\   \n")

        #fo.write(" \t \\end{tabular*} \n")
        #fo.write("  \\end{table} \n")
        #fo.write("\\end{frame} \n")
        #fo.close()
        
        #fCS.write(" %5.2f  %5.2f  %2.3f  %2.3f  \n"%( NJpsiPr[nPT-1][0].mean, NJpsiSec[nPT-1][0].mean, NEtacPr[nPT-1][0].mean, NEtacSec[nPT-1][0].mean))
        #fCS.write(" %5.2f  %5.2f  %2.3f  %2.3f  \n"%( NJpsiPr[nPT-1][0].statErr, NJpsiSec[nPT-1][0].statErr, NEtacPr[nPT-1][0].statErr, NEtacSec[nPT-1][0].statErr))
        #fCS.write(" %5.2f  %5.2f  %2.3f  %2.3f  \n \n"%( NJpsiPr[nPT-1][0].systErr, NJpsiSec[nPT-1][0].systErr, NEtacPr[nPT-1][0].systErr, NEtacSec[nPT-1][0].systErr))
        #fCS.write(" %5.2f  %5.2f  %2.3f  %2.3f  \n \n"%( NJpsiPr[nPT-1][0].systErrCorr, NJpsiSec[nPT-1][0].systErrCorr, NEtacPr[nPT-1][0].systErrCorr, NEtacSec[nPT-1][0].systErrCorr))
        
        print "systematic's calculated successfully"
    
    #fCS.close()

    tex = TLatex()
    tex.SetNDC()
    
    cMassPr = TCanvas("cMassPr","cMass", 1400, 700)
    cMassSec = TCanvas("cMassSec","cMass", 1400, 700)
    cMassPr.Divide(nConfsMass/2-1,2)
    cMassSec.Divide(nConfsMass/2-1,2)
    ic = 1
    for nConfM in systDictMUncorr:
        
        idxM = nMasses.index(nConfM)
        cMassPr.cd(ic)
        hMassPr[idxM].Fit("pol1","W")
        hMassPr[idxM].Draw()
        hMassPr[idxM].SetMaximum(hMassPr[idxM].GetMaximum()+0.05)
        hMassPr[idxM].SetMinimum(0)
        fMassPr[idxM].Draw("same")
        fMassPr[idxM].SetLineColor(2)
        tex.DrawLatex(0.2, 0.85, systDictMUncorr[nConfM])
        cMassSec.cd(ic)
        hMassSec[idxM].Fit("pol1","W")
        hMassSec[idxM].Draw()
        hMassSec[idxM].SetMaximum(hMassSec[idxM].GetMaximum()+0.05)
        hMassSec[idxM].SetMinimum(0)
        fMassSec[idxM].Draw("same")
        fMassSec[idxM].SetLineColor(2)
        tex.DrawLatex(0.2, 0.85, systDictMUncorr[nConfM])
        ic+=1
        
    cMassPr.SaveAs("massSystPrompt.pdf")
    cMassSec.SaveAs("massSystFromB.pdf")
    cMassPr.SaveAs("massSystPrompt.root")
    cMassSec.SaveAs("massSystFromB.root")

    cTzPr = TCanvas("cTzPr","cTz", 1000, 500)
    cTzSec = TCanvas("cTzSec","cTz", 1000, 500)
    cTzPr.Divide(nConfsTz/2,2)
    cTzSec.Divide(nConfsTz/2,2)
        
    for nConfTz in nTzs:

        idxTz = nTzs.index(nConfTz)
        cTzPr.cd(idxTz+1)
        hTzPr[idxTz].Fit("pol1","W")
        hTzPr[idxTz].Draw()
        #hTzPr[idxTz].SetMaximum(hTzPr[idxTz].GetMaximum()+0.0025)
        hTzPr[idxTz].SetMinimum(0.)
        #fTzPr[idxTz].Draw("same")
        tex.DrawLatex(0.2, 0.8, systDictTz[nConfTz])
        cTzSec.cd(idxTz+1)
        hTzSec[idxTz].Fit("pol1","W")
        hTzSec[idxTz].Draw()
        #hTzSec[idxTz].SetMaximum(hTzSec[idxTz].GetMaximum()+0.0025)
        hTzSec[idxTz].SetMinimum(0.)
        #fTzSec[idxTz].Draw("same")
        tex.DrawLatex(0.2, 0.8, systDictTz[nConfTz])



    cTzPr.SaveAs("tzSystPrompt.pdf")
    cTzSec.SaveAs("tzSystFromB.pdf")
    cTzPr.SaveAs("tzSystPrompt.root")
    cTzSec.SaveAs("tzSystFromB.root")


    for nPT in nPTs:
        
        nameOutTxt = homeDir+"Results/TzFit/systTable_PT%s.txt"%(nPT)
        fo = open(nameOutTxt,"w")

        fo.write("\\begin{frame} \n")
        fo.write("\\frametitle{PT Bin "+str(nPT)+"} \n")
        fo.write("  \\begin{table}[H] \n")
        fo.write("  \t \\begin{tabular*}{0.95\\linewidth}{@{\\extracolsep{\\fill}}c|c|c|c|c} \n")
        fo.write("  & $N^{p}_{J/\\psi}$ & $N^{b}_{J/\\psi}$ & $N^{p}_{\\eta_c}/N^{p}_{J/\\psi}$ & $N^{b}_{\\eta_c}/N^{b}_{J/\\psi}$  \\\\ \\hline \n")


        fo.write(" & %5.2f & %5.2f & %2.3f & %2.3f \\\\ \\hline  \n"%( NJpsiPr[nPT-1][0].mean, NJpsiSec[nPT-1][0].mean, NEtacPr[nPT-1][0].mean, NEtacSec[nPT-1][0].mean))
        fo.write(" & %5.2f & %5.2f & %2.3f & %2.3f \\\\ \\hline  \n"%( NJpsiPr[nPT-1][0].statErr, NJpsiSec[nPT-1][0].statErr, NEtacPr[nPT-1][0].statErr, NEtacSec[nPT-1][0].statErr))


        for nConfM in systDictMUncorr:
            
            idxM = nMasses.index(nConfM)
            fo.write("%s & %5.2f & %5.2f & %2.3f & %2.3f"%(systDictMUncorr[nConfM], NJpsiPr[nPT-1][idxM + 1].systErr, NJpsiSec[nPT-1][idxM + 1].systErr, NEtacPr[nPT-1][idxM + 1].systErr, NEtacSec[nPT-1][idxM + 1].systErr))
            fo.write(" \\\\   \n ")

        fo.write(" \\hline  \n")

        for nConfTz in nTzs:

            idxTz = nTzs.index(nConfTz)
            fo.write("%s & %5.2f & %5.2f & %2.3f & %2.3f"%(systDictTz[nConfTz], NJpsiPr[nPT-1][nConfsMass + idxTz + 1].systErr, NJpsiSec[nPT-1][nConfsMass + idxTz + 1].systErr, NEtacPr[nPT-1][nConfsMass + idxTz + 1].systErr, NEtacSec[nPT-1][nConfsMass + idxTz + 1].systErr))
            fo.write(" \\\\   \n ")

        fo.write(" \\hline  \n")
        fo.write("Tot Syst Uncorr & %5.2f & %5.2f & %2.3f & %2.3f"%( NJpsiPr[nPT-1][0].systErr, NJpsiSec[nPT-1][0].systErr, NEtacPr[nPT-1][0].systErr, NEtacSec[nPT-1][0].systErr))
        fo.write(" \\\\   \n")
        fo.write(" \\hline  \n")

        fo.write("%s & %5.2f & %5.2f & %2.3f & %2.3f  "%(systDictGen[1], NJpsiPr[nPT-1][nConfsMass + nConfsTz + 1].systErrCorr, NJpsiSec[nPT-1][nConfsMass + nConfsTz + 1].systErrCorr, NEtacPr[nPT-1][nConfsMass + nConfsTz + 1].systErrCorr, NEtacSec[nPT-1][nConfsMass + nConfsTz + 1].systErrCorr))
        fo.write(" \\\\   \n ")

        for nConfM in systDictMCorr:
            
            idxM = nMasses.index(nConfM)
            fo.write("%s & %5.2f & %5.2f & %2.3f & %2.3f"%(systDictMCorr[nConfM], NJpsiPr[nPT-1][idxM + 1].systErrCorr, NJpsiSec[nPT-1][idxM + 1].systErrCorr, NEtacPr[nPT-1][idxM + 1].systErrCorr, NEtacSec[nPT-1][idxM + 1].systErrCorr))
            fo.write(" \\\\   \n ")

        fo.write(" \\hline  \n")
        fo.write("Tot Syst Corr & %5.2f & %5.2f & %2.3f & %2.3f"%( NJpsiPr[nPT-1][0].systErrCorr, NJpsiSec[nPT-1][0].systErrCorr, NEtacPr[nPT-1][0].systErrCorr, NEtacSec[nPT-1][0].systErrCorr))
        fo.write(" \\\\   \n")
        fo.write(" \\hline  \n")

        fo.write(" \\hline  \n")
        fo.write("Tot Syst & %5.2f & %5.2f & %2.3f & %2.3f"%( (NJpsiPr[nPT-1][0].systErr**2 + NJpsiPr[nPT-1][0].systErrCorr**2)**0.5, (NJpsiSec[nPT-1][0].systErr**2 + NJpsiSec[nPT-1][0].systErrCorr**2)**0.5, (NEtacPr[nPT-1][0].systErr**2 + NEtacPr[nPT-1][0].systErrCorr**2)**0.5, (NEtacSec[nPT-1][0].systErr**2 + NEtacSec[nPT-1][0].systErrCorr**2)**0.5))
        fo.write(" \\\\   \n")
        fo.write(" \\hline  \n")

        fo.write(" \t \\end{tabular*} \n")
        fo.write("  \\end{table} \n")
        fo.write("\\end{frame} \n")
        fo.close()

        fCS.write(" %5.2f  %5.2f  %2.3f  %2.3f  \n"%( NJpsiPr[nPT-1][0].mean, NJpsiSec[nPT-1][0].mean, NEtacPr[nPT-1][0].mean, NEtacSec[nPT-1][0].mean))
        fCS.write(" %5.2f  %5.2f  %2.3f  %2.3f  \n"%( NJpsiPr[nPT-1][0].statErr, NJpsiSec[nPT-1][0].statErr, NEtacPr[nPT-1][0].statErr, NEtacSec[nPT-1][0].statErr))
        fCS.write(" %5.2f  %5.2f  %2.3f  %2.3f  \n \n"%( NJpsiPr[nPT-1][0].systErr, NJpsiSec[nPT-1][0].systErr, NEtacPr[nPT-1][0].systErr, NEtacSec[nPT-1][0].systErr))
        fCS.write(" %5.2f  %5.2f  %2.3f  %2.3f  \n \n"%( NJpsiPr[nPT-1][0].systErrCorr, NJpsiSec[nPT-1][0].systErrCorr, NEtacPr[nPT-1][0].systErrCorr, NEtacSec[nPT-1][0].systErrCorr))

    fo.close()
    fCS.close()


def processPT(nPTs=[0], nTzs=[0], nMasses=[0], nGen=[0]):

    #gROOT.Reset()

    gStyle.SetOptStat(0)
    gStyle.SetOptTitle(0)

    nConfsTz = len(nTzs)
    nConfsMass = len(nMasses)

    fCS = open(homeDir+"Results/rel_PT_sim.txt","w")

    hMassPr = []
    hMassSec = []
    hTzPr = []
    hTzSec = []
    fMassPr = []
    fMassSec = []
    fTzPr = []
    fTzSec = []

    massJpsiSystPrompt, massJpsiSystFromB, massSystPrompt, massSystFromB, tzSystPrompt, tzSystFromB =  processPT0([0], nMasses, nGen)

    for nConfM in nMasses:
        hMassPr.append(TH1D("hMassPr%s"%(nConfM),"mass syst", 4, ptBins))
        hMassSec.append(TH1D("hMassSec%s"%(nConfM),"mass syst", 4, ptBins))
        fMassPr.append(TF1("fMassPr%s"%(nConfM),"pol0", ptBins[0], ptBins[4]))
        fMassSec.append(TF1("fMassSec%s"%(nConfM),"pol0", ptBins[0], ptBins[4]))
    for nConfTz in nTzs:
        hTzPr.append(TH1D("hTzPr%s"%(nConfTz),"tz syst", 4, ptBins))
        hTzSec.append(TH1D("hTzSec%s"%(nConfTz),"tz syst", 4, ptBins))
        fTzPr.append(TF1("fTzPr%s"%(nConfM),"pol0", ptBins[0], ptBins[4]))
        fTzSec.append(TF1("fTzSec%s"%(nConfM),"pol0", ptBins[0], ptBins[4]))


    NJpsiPr = []
    NEtacPr = []
    NJpsiSec = []
    NEtacSec = []


    for nPT in nPTs:

        nameInFile = homeDir+"Results/TzFit/systUncert/Sim/ROOTSimFit_0_0_test.root"
        fIn0 = TFile(nameInFile,"READ")
        
        fJpsi = fIn0.Get("fitJpsi%s"%(nPT))
        fEtac = fIn0.Get("fitEtac%s"%(nPT))

        NJpsiPr.append([])
        NEtacPr.append([])
        NJpsiSec.append([])
        NEtacSec.append([]) 

        NJpsiPr[nPT-1].append(Variable(fJpsi.GetParameter(6), fJpsi.GetParError(6))) 
        NJpsiSec[nPT-1].append(Variable(fJpsi.GetParameter(7), fJpsi.GetParError(7))) 
        NEtacPr[nPT-1].append(Variable(fEtac.GetParameter(9), fEtac.GetParError(9))) 
        NEtacSec[nPT-1].append(Variable(fEtac.GetParameter(10), fEtac.GetParError(10))) 

        #NJpsiPr[nPT-1][0].mean = fJpsi0.GetParameter(5)
        #NJpsiPr[nPT-1][0].statErr = fJpsi0.GetParError(5)
        #NJpsiSec[nPT-1][0].mean = fJpsi0.GetParameter(6)
        #NJpsiSec[nPT-1][0].statErr = fJpsi0.GetParError(6)

        #NEtacPr[nPT-1][0].mean = fEtac0.GetParameter(8)
        #NEtacPr[nPT-1][0].statErr = fEtac0.GetParError(8)
        #NEtacSec[nPT-1][0].mean = fEtac0.GetParameter(9)
        #NEtacSec[nPT-1][0].statErr = fEtac0.GetParError(9)

        fIn0.Close()

        #nameOutTxt = homeDir+"Results/TzFit/systTable_PT%s.txt"%(nPT)
        #fo = open(nameOutTxt,"w")

        #fo.write("\\begin{frame} \n")
        #fo.write("\\frametitle{PT Bin "+str(nPT)+"} \n")
        #fo.write("  \\begin{table}[H] \n")
        #fo.write("  \t \\begin{tabular*}{0.95\\linewidth}{@{\\extracolsep{\\fill}}c|c|c|c|c} \n")
        #fo.write("  & $N^{p}_{J/\\psi}$ & $N^{b}_{J/\\psi}$ & $N^{p}_{\\eta_c}/N^{p}_{J/\\psi}$ & $N^{b}_{\\eta_c}/N^{b}_{J/\\psi}$  \\\\ \\hline \n")


        #fo.write(" & %5.2f & %5.2f & %2.3f & %2.3f \\\\ \\hline  \n"%( NJpsiPr[nPT-1][0].mean, NJpsiSec[nPT-1][0].mean, NEtacPr[nPT-1][0].mean, NEtacSec[nPT-1][0].mean))
        #fo.write(" & %5.2f & %5.2f & %2.3f & %2.3f \\\\ \\hline  \n"%( NJpsiPr[nPT-1][0].statErr, NJpsiSec[nPT-1][0].statErr, NEtacPr[nPT-1][0].statErr, NEtacSec[nPT-1][0].statErr))

        for nConfM in nMasses:

            if nConfM in systDictMUncorr:
                idxM = nMasses.index(nConfM)
                fIn = TFile(homeDir+"Results/TzFit/systUncert/Sim/ROOTSimFit_0_%d_test.root"%(nConfM),"READ")
                fJpsi = fIn.Get("fitJpsi%s"%(nPT))
                fEtac = fIn.Get("fitEtac%s"%(nPT))
        
                
                NJpsiPr[nPT-1].append(Variable(fJpsi.GetParameter(6), fJpsi.GetParError(6))) 
                NJpsiSec[nPT-1].append(Variable(fJpsi.GetParameter(7), fJpsi.GetParError(7))) 
                NEtacPr[nPT-1].append(Variable(fEtac.GetParameter(9), fEtac.GetParError(9))) 
                NEtacSec[nPT-1].append(Variable(fEtac.GetParameter(10), fEtac.GetParError(10))) 

                NJpsiPr[nPT-1][idxM + 1].systErr = (NJpsiPr[nPT-1][idxM + 1].mean-NJpsiPr[nPT-1][0].mean)
                NJpsiSec[nPT-1][idxM + 1].systErr = (NJpsiSec[nPT-1][idxM + 1].mean-NJpsiSec[nPT-1][0].mean)

                NEtacPr[nPT-1][idxM + 1].systErr = (NEtacPr[nPT-1][idxM + 1].mean-NEtacPr[nPT-1][0].mean)
                NEtacSec[nPT-1][idxM + 1].systErr = (NEtacSec[nPT-1][idxM + 1].mean-NEtacSec[nPT-1][0].mean)

                NJpsiPr[nPT-1][0].systErr += (NJpsiPr[nPT-1][idxM + 1].mean-NJpsiPr[nPT-1][0].mean)**2
                NJpsiSec[nPT-1][0].systErr += (NJpsiSec[nPT-1][idxM + 1].mean-NJpsiSec[nPT-1][0].mean)**2

                NEtacPr[nPT-1][0].systErr += (NEtacPr[nPT-1][idxM + 1].mean-NEtacPr[nPT-1][0].mean)**2
                NEtacSec[nPT-1][0].systErr += (NEtacSec[nPT-1][idxM + 1].mean-NEtacSec[nPT-1][0].mean)**2


                fIn.Close()
                
                hMassPr[idxM].SetBinContent(nPT, abs(NEtacPr[nPT-1][idxM + 1].mean-NEtacPr[nPT-1][0].mean)/NEtacPr[nPT-1][0].mean)
                hMassSec[idxM].SetBinContent(nPT, abs(NEtacSec[nPT-1][idxM + 1].mean-NEtacSec[nPT-1][0].mean)/NEtacSec[nPT-1][0].mean)
                fMassPr[idxM].SetParameter(0, massSystPrompt[idxM])
                fMassSec[idxM].SetParameter(0, massSystFromB[idxM])
                
                #fo.write("%s & %5.2f & %5.2f & %2.3f & %2.3f"%(systDictMUncorr[nConfM], NJpsiPr[nPT-1][idxM + 1].mean-NJpsiPr[nPT-1][0].mean, NJpsiSec[nPT-1][idxM + 1].mean-NJpsiSec[nPT-1][0].mean, NEtacPr[nPT-1][idxM + 1].mean-NEtacPr[nPT-1][0].mean, NEtacSec[nPT-1][idxM + 1].mean-NEtacSec[nPT-1][0].mean))
                #fo.write(" \\\\   \n ")
                
            elif nConfM in systDictMCorr:
                idxM = nMasses.index(nConfM)

                NJpsiPr[nPT-1].append(Variable(fJpsi.GetParameter(6), fJpsi.GetParError(6))) 
                NJpsiSec[nPT-1].append(Variable(fJpsi.GetParameter(7), fJpsi.GetParError(7))) 
                NEtacPr[nPT-1].append(Variable(fEtac.GetParameter(9), fEtac.GetParError(9))) 
                NEtacSec[nPT-1].append(Variable(fEtac.GetParameter(10), fEtac.GetParError(10))) 


                #NJpsiPr[nPT-1][idxM + 1].systErrCorr = abs(massJpsiSystPrompt[idxM]*NJpsiPr[nPT-1][0].mean)
                #NJpsiSec[nPT-1][idxM + 1].systErrCorr = abs(massJpsiSystFromB[idxM]*NJpsiSec[nPT-1][0].mean)

                #NEtacPr[nPT-1][idxM + 1].systErrCorr = abs(massSystPrompt[idxM]*NEtacPr[nPT-1][0].mean)
                #NEtacSec[nPT-1][idxM + 1].systErrCorr = abs(massSystFromB[idxM]*NEtacSec[nPT-1][0].mean)

                NJpsiPr[nPT-1][idxM + 1].systErrCorr = abs(massJpsiSystPrompt[idxM])
                NJpsiSec[nPT-1][idxM + 1].systErrCorr = abs(massJpsiSystFromB[idxM])

                NEtacPr[nPT-1][idxM + 1].systErrCorr = abs(massSystPrompt[idxM])
                NEtacSec[nPT-1][idxM + 1].systErrCorr = abs(massSystFromB[idxM])

                NJpsiPr[nPT-1][0].systErrCorr += (NJpsiPr[nPT-1][idxM + 1].systErrCorr)**2
                NJpsiSec[nPT-1][0].systErrCorr  += (NJpsiSec[nPT-1][idxM + 1].systErrCorr)**2

                NEtacPr[nPT-1][0].systErrCorr  += (NEtacPr[nPT-1][idxM + 1].systErrCorr)**2
                NEtacSec[nPT-1][0].systErrCorr  += (NEtacSec[nPT-1][idxM + 1].systErrCorr)**2

                #fo.write("%s & %5.2f & %5.2f & %2.3f & %2.3f  "%(systDictMCorr[nConfM], massJpsiSystPrompt[idxM]*NJpsiPr[nPT-1][0].mean, massJpsiSystFromB[idxM]*NJpsiSec[nPT-1][0].mean, massSystPrompt[idxM]*NEtacPr[nPT-1][0].mean, massSystFromB[idxM]*NEtacSec[nPT-1][0].mean))
                #fo.write(" \\\\   \n ")
            else:
                NJpsiPr[nPT-1].append(Variable(NJpsiPr[nPT-1][0].mean, 0)) 
                NJpsiSec[nPT-1].append(Variable(NJpsiPr[nPT-1][0].mean, 0)) 
                NEtacPr[nPT-1].append(Variable(NJpsiPr[nPT-1][0].mean, 0)) 
                NEtacSec[nPT-1].append(Variable(NJpsiPr[nPT-1][0].mean, 0)) 

        #fo.write(" \\hline   \n ")

        for nConfTz in nTzs:

            idxTz = nTzs.index(nConfTz)
            fIn = TFile(homeDir+"Results/TzFit/systUncert/Sim/ROOTSimFit_%d_0_test.root"%(nConfTz),"READ")
            fJpsi = fIn.Get("fitJpsi%s"%(nPT))
            fEtac = fIn.Get("fitEtac%s"%(nPT))

            NJpsiPr[nPT-1].append(Variable(fJpsi.GetParameter(6), fJpsi.GetParError(6))) 
            NJpsiSec[nPT-1].append(Variable(fJpsi.GetParameter(7), fJpsi.GetParError(7))) 
            NEtacPr[nPT-1].append(Variable(fEtac.GetParameter(9), fEtac.GetParError(9))) 
            NEtacSec[nPT-1].append(Variable(fEtac.GetParameter(10), fEtac.GetParError(10))) 

            NJpsiPr[nPT-1][nConfsMass + idxTz + 1].systErr = (NJpsiPr[nPT-1][nConfsMass + idxTz + 1].mean-NJpsiPr[nPT-1][0].mean)
            NJpsiSec[nPT-1][nConfsMass + idxTz + 1].systErr = (NJpsiSec[nPT-1][nConfsMass + idxTz + 1].mean-NJpsiSec[nPT-1][0].mean)

            NEtacPr[nPT-1][nConfsMass + idxTz + 1].systErr = (NEtacPr[nPT-1][nConfsMass + idxTz + 1].mean-NEtacPr[nPT-1][0].mean)
            NEtacSec[nPT-1][nConfsMass + idxTz + 1].systErr = (NEtacSec[nPT-1][nConfsMass + idxTz + 1].mean-NEtacSec[nPT-1][0].mean)


            NJpsiPr[nPT-1][0].systErr += (NJpsiPr[nPT-1][nConfsMass + idxTz + 1].mean-NJpsiPr[nPT-1][0].mean)**2
            NJpsiSec[nPT-1][0].systErr += (NJpsiSec[nPT-1][nConfsMass + idxTz + 1].mean-NJpsiSec[nPT-1][0].mean)**2

            NEtacPr[nPT-1][0].systErr += (NEtacPr[nPT-1][nConfsMass + idxTz + 1].mean-NEtacPr[nPT-1][0].mean)**2
            NEtacSec[nPT-1][0].systErr += (NEtacSec[nPT-1][nConfsMass + idxTz + 1].mean-NEtacSec[nPT-1][0].mean)**2

            fIn.Close()

            hTzPr[idxTz].SetBinContent(nPT, abs(NEtacPr[nPT-1][nConfsMass + idxTz + 1].mean-NEtacPr[nPT-1][0].mean)/NEtacPr[nPT-1][0].mean)
            hTzSec[idxTz].SetBinContent(nPT, abs(NEtacSec[nPT-1][nConfsMass + idxTz + 1].mean-NEtacSec[nPT-1][0].mean)/NEtacSec[nPT-1][0].mean)
            #fTzPr[idxM].SetParameter(0, tzSystPrompt[idxTz])
            #fTzSec[idxM].SetParameter(0, tzSystFromB[idxTz])

            #fo.write("%s & %5.2f & %5.2f & %2.3f & %2.3f  "%(systDictTz[nConfTz], NJpsiPr[nPT-1][nConfsMass + idxTz +1].mean-NJpsiPr[nPT-1][0].mean, NJpsiSec[nPT-1][nConfsMass + idxTz +1].mean-NJpsiSec[nPT-1][0].mean, NEtacPr[nPT-1][nConfsMass + idxTz +1].mean-NEtacPr[nPT-1][0].mean, NEtacSec[nPT-1][nConfsMass + idxTz +1].mean-NEtacSec[nPT-1][0].mean))
            #fo.write(" \\\\   \n ")

        #fo.write(" \\hline   \n ")


        NJpsiPr[nPT-1][0].systErr = NJpsiPr[nPT-1][0].systErr**0.5
        NJpsiSec[nPT-1][0].systErr = NJpsiSec[nPT-1][0].systErr**0.5
        NEtacPr[nPT-1][0].systErr = NEtacPr[nPT-1][0].systErr**0.5
        NEtacSec[nPT-1][0].systErr = NEtacSec[nPT-1][0].systErr**0.5

        #fo.write(" \\hline  \n")
        #fo.write("Tot Syst Uncorr & %5.2f & %5.2f & %2.3f & %2.3f"%( NJpsiPr[nPT-1][0].systErr, NJpsiSec[nPT-1][0].systErr, NEtacPr[nPT-1][0].systErr, NEtacSec[nPT-1][0].systErr))
        #fo.write(" \\\\   \n")
        #fo.write(" \\hline  \n")


        #for nG in nGen:

        fIn = TFile(homeDir+"Results/effPolarization.root","READ")
        hEff = fIn.Get("histEff")

        NJpsiPr[nPT-1].append(Variable(fJpsi.GetParameter(6), fJpsi.GetParError(6))) 
        NJpsiSec[nPT-1].append(Variable(fJpsi.GetParameter(7), fJpsi.GetParError(7))) 
        NEtacPr[nPT-1].append(Variable(fEtac.GetParameter(9), fEtac.GetParError(9))) 
        NEtacSec[nPT-1].append(Variable(fEtac.GetParameter(10), fEtac.GetParError(10))) 

        NJpsiPr[nPT-1][nConfsMass + nConfsTz + 1].systErrCorr = abs(hEff.GetBinContent(nPT))
        
        NEtacPr[nPT-1][nConfsMass + nConfsTz + 1].systErrCorr = abs(hEff.GetBinContent(nPT))

        #NJpsiPr[nPT-1][nConfsMass + nConfsTz + 1].systErrCorr = abs(hEff.GetBinContent(nPT)*NJpsiPr[nPT-1][0].mean)
        
        #NEtacPr[nPT-1][nConfsMass + nConfsTz + 1].systErrCorr = abs(hEff.GetBinContent(nPT)*NEtacPr[nPT-1][0].mean)
        
        NJpsiPr[nPT-1][0].systErrCorr += (NJpsiPr[nPT-1][nConfsMass + nConfsTz + 1].systErrCorr)**2

        NEtacPr[nPT-1][0].systErrCorr += (NEtacPr[nPT-1][nConfsMass + nConfsTz + 1].systErrCorr)**2

        #fo.write("%s & %5.2f & %5.2f & %2.3f & %2.3f  "%(systDictGen[1], hEff.GetBinContent(nPT)*NJpsiPr[nPT-1][0].mean, 0., hEff.GetBinContent(nPT)*NEtacPr[nPT-1][0].mean, 0))
        #fo.write(" \\\\   \n ")

        fIn.Close()


        NJpsiPr[nPT-1][0].systErrCorr = NJpsiPr[nPT-1][0].systErrCorr**0.5
        NJpsiSec[nPT-1][0].systErrCorr = NJpsiSec[nPT-1][0].systErrCorr**0.5
        NEtacPr[nPT-1][0].systErrCorr = NEtacPr[nPT-1][0].systErrCorr**0.5
        NEtacSec[nPT-1][0].systErrCorr = NEtacSec[nPT-1][0].systErrCorr**0.5
        
        #fo.write(" \\hline  \n")
        #fo.write("Tot Syst Corr & %5.2f & %5.2f & %2.3f & %2.3f"%( NJpsiPr[nPT-1][0].systErr, NJpsiSec[nPT-1][0].systErr, NEtacPr[nPT-1][0].systErr, NEtacSec[nPT-1][0].systErr))
        #fo.write(" \\\\   \n")

        #fo.write(" \t \\end{tabular*} \n")
        #fo.write("  \\end{table} \n")
        #fo.write("\\end{frame} \n")
        #fo.close()
        
        #fCS.write(" %5.2f  %5.2f  %2.3f  %2.3f  \n"%( NJpsiPr[nPT-1][0].mean, NJpsiSec[nPT-1][0].mean, NEtacPr[nPT-1][0].mean, NEtacSec[nPT-1][0].mean))
        #fCS.write(" %5.2f  %5.2f  %2.3f  %2.3f  \n"%( NJpsiPr[nPT-1][0].statErr, NJpsiSec[nPT-1][0].statErr, NEtacPr[nPT-1][0].statErr, NEtacSec[nPT-1][0].statErr))
        #fCS.write(" %5.2f  %5.2f  %2.3f  %2.3f  \n \n"%( NJpsiPr[nPT-1][0].systErr, NJpsiSec[nPT-1][0].systErr, NEtacPr[nPT-1][0].systErr, NEtacSec[nPT-1][0].systErr))
        #fCS.write(" %5.2f  %5.2f  %2.3f  %2.3f  \n \n"%( NJpsiPr[nPT-1][0].systErrCorr, NJpsiSec[nPT-1][0].systErrCorr, NEtacPr[nPT-1][0].systErrCorr, NEtacSec[nPT-1][0].systErrCorr))
        
        print "systematic's calculated successfully"
    
    #fCS.close()


        #NJpsiPr[nPT-1][0].systErr = 0
        #NJpsiSec[nPT-1][0].systErr = 0
        NEtacPr[nPT-1][0].systErr = 0
        NEtacSec[nPT-1][0].systErr = 0


    tex = TLatex()
    tex.SetNDC()
    
    TGaxis.SetMaxDigits(3)

    cMass = []
    ic = 0
    for nConfM in systDictMUncorr:
        
        idxM = nMasses.index(nConfM)
        cMass.append(TCanvas("cMassPr%s"%ic,"cMass", 1400, 600))
        cMass[ic].Divide(2,1)
        cMass[ic].cd(1)
        gPad.SetLeftMargin(0.15)
        if nConfM!=2:
            hMassPr[idxM].Fit("pol1","W")
            fPr = hMassPr[idxM].GetFunction("pol1")
        else:    
            hMassPr[idxM].Fit("pol0","W")
            fPr = hMassPr[idxM].GetFunction("pol0")
        hMassPr[idxM].Draw()
        hMassPr[idxM].SetMaximum(hMassPr[idxM].GetMaximum()+0.02)
        hMassPr[idxM].SetMinimum(0)
        hMassPr[idxM].GetXaxis().SetTitle("p_{T}, GeV/c")
        hMassPr[idxM].GetYaxis().SetTitle("rel. syst. uncert.")
        #fMassPr[idxM].Draw("same")
        fMassPr[idxM].SetLineColor(2)
        #tex.DrawLatex(0.2, 0.85, systDictMUncorr[nConfM])
        tex.DrawLatex(0.2, 0.8, "prompt")
        cMass[ic].cd(2)
        gPad.SetLeftMargin(0.15)
        if nConfM!=2:
            hMassSec[idxM].Fit("pol1","W")
            fSec = hMassSec[idxM].GetFunction("pol1")
        else:    
            hMassSec[idxM].Fit("pol0","W")
            fSec = hMassSec[idxM].GetFunction("pol0")
        hMassSec[idxM].Draw()
        hMassSec[idxM].SetMaximum(hMassSec[idxM].GetMaximum()+0.02)
        hMassSec[idxM].SetMinimum(0)
        hMassSec[idxM].GetXaxis().SetTitle("p_{T}, GeV/c")
        hMassSec[idxM].GetYaxis().SetTitle("rel. syst. uncert.")
        #fMassSec[idxM].Draw("same")
        fMassSec[idxM].SetLineColor(2)
        #tex.DrawLatex(0.2, 0.85, systDictMUncorr[nConfM])
        tex.DrawLatex(0.2, 0.8, "from-b")
        cMass[ic].SaveAs("massSyst%s.pdf"%ic)
        cMass[ic].SaveAs("massSyst%s.root"%ic)
        ic+=1
        
        for nPT in nPTs:
            #NEtacPr[nPT-1][idxM + 1].systErr = fPr.Eval((ptBins[nPT-1]+ptBins[nPT])/2.)*NEtacPr[nPT-1][0].mean
            NEtacPr[nPT-1][idxM + 1].systErr = fPr.Eval((ptBins[nPT-1]+ptBins[nPT])/2.)
            NEtacPr[nPT-1][0].systErr += NEtacPr[nPT-1][idxM + 1].systErr**2
            #NEtacSec[nPT-1][idxM + 1].systErr = fSec.Eval((ptBins[nPT-1]+ptBins[nPT])/2.)*NEtacSec[nPT-1][0].mean
            NEtacSec[nPT-1][idxM + 1].systErr = fSec.Eval((ptBins[nPT-1]+ptBins[nPT])/2.)
            NEtacSec[nPT-1][0].systErr += NEtacSec[nPT-1][idxM + 1].systErr**2
            print NJpsiPr[nPT-1][0].systErr, NJpsiSec[nPT-1][0].systErr, NEtacPr[nPT-1][0].systErr, NEtacSec[nPT-1][0].systErr


    cTz = []
        
    for nConfTz in nTzs:

        idxTz = nTzs.index(nConfTz)
        cTz.append(TCanvas("cTz%s"%idxTz,"cTz", 1400, 600))
        cTz[idxTz].Divide(2,1)
        cTz[idxTz].cd(1)
        gPad.SetLeftMargin(0.15)
        if nConfTz!=3:
            hTzPr[idxTz].Fit("pol1","W")
            fPr = hTzPr[idxTz].GetFunction("pol1")
        else:    
            hTzPr[idxTz].Fit("pol0","W")
            fPr = hTzPr[idxTz].GetFunction("pol0")
        hTzPr[idxTz].Draw()
        hTzPr[idxTz].SetMaximum(hTzPr[idxTz].GetMaximum()+0.2e-3)
        hTzPr[idxTz].SetMinimum(0.)
        hTzPr[idxTz].GetXaxis().SetTitle("p_{T}, GeV/c")
        hTzPr[idxTz].GetYaxis().SetTitle("rel. syst. uncert.")
        #tex.DrawLatex(0.2, 0.8, systDictTz[nConfTz])
        tex.DrawLatex(0.2, 0.8, "prompt")
        cTz[idxTz].cd(2)
        gPad.SetLeftMargin(0.15)
        if nConfTz!=3:
            hTzSec[idxTz].Fit("pol1","W")
            fSec = hTzSec[idxTz].GetFunction("pol1")
        else:    
            hTzSec[idxTz].Fit("pol0","W")
            fSec = hTzSec[idxTz].GetFunction("pol0")
        hTzSec[idxTz].Draw()
        hTzSec[idxTz].SetMaximum(hTzSec[idxTz].GetMaximum()+0.2e-3)
        hTzSec[idxTz].SetMinimum(0.)
        hTzSec[idxTz].GetXaxis().SetTitle("p_{T}, GeV/c")
        hTzSec[idxTz].GetYaxis().SetTitle("rel. syst. uncert.")
        #tex.DrawLatex(0.2, 0.8, systDictTz[nConfTz])
        tex.DrawLatex(0.2, 0.8, "from-b")
        cTz[idxTz].SaveAs("tzSyst%s.pdf"%idxTz)
        cTz[idxTz].SaveAs("tzSyst%s.root"%idxTz)

        for nPT in nPTs:
            #NEtacPr[nPT-1][nConfsMass + idxTz + 1].systErr = fPr.Eval((ptBins[nPT-1]+ptBins[nPT])/2.)*NEtacPr[nPT-1][0].mean
            NEtacPr[nPT-1][nConfsMass + idxTz + 1].systErr = fPr.Eval((ptBins[nPT-1]+ptBins[nPT])/2.)
            NEtacPr[nPT-1][0].systErr += NEtacPr[nPT-1][nConfsMass + idxTz + 1].systErr**2
            #NEtacSec[nPT-1][nConfsMass + idxTz + 1].systErr = fSec.Eval((ptBins[nPT-1]+ptBins[nPT])/2.)*NEtacSec[nPT-1][0].mean
            NEtacSec[nPT-1][nConfsMass + idxTz + 1].systErr = fSec.Eval((ptBins[nPT-1]+ptBins[nPT])/2.)
            NEtacSec[nPT-1][0].systErr += NEtacSec[nPT-1][nConfsMass + idxTz + 1].systErr**2.



    for nPT in nPTs:
        
        nameOutTxt = homeDir+"Results/TzFit/systTable_PT%s.txt"%(nPT)
        fo = open(nameOutTxt,"w")

        fo.write("\\begin{frame} \n")
        fo.write("\\frametitle{PT Bin "+str(nPT)+"} \n")
        fo.write("  \\begin{table}[H] \n")
        fo.write("  \t \\begin{tabular*}{0.95\\linewidth}{@{\\extracolsep{\\fill}}c|c|c|c|c} \n")
        fo.write("  & $N^{p}_{J/\\psi}$ & $N^{b}_{J/\\psi}$ & $N^{p}_{\\eta_c}/N^{p}_{J/\\psi}$ & $N^{b}_{\\eta_c}/N^{b}_{J/\\psi}$  \\\\ \\hline \n")


        fo.write(" & %5.2f & %5.2f & %2.3f & %2.3f \\\\ \\hline  \n"%( NJpsiPr[nPT-1][0].mean, NJpsiSec[nPT-1][0].mean, NEtacPr[nPT-1][0].mean, NEtacSec[nPT-1][0].mean))
        #fo.write(" & %5.2f & %5.2f & %2.3f & %2.3f \\\\ \\hline  \n"%( NJpsiPr[nPT-1][0].statErr, NJpsiSec[nPT-1][0].statErr, NEtacPr[nPT-1][0].statErr, NEtacSec[nPT-1][0].statErr))
        fo.write(" & %2.1f & %2.1f & %2.1f & %2.1f \\\\ \\hline  \n"%( 100*NJpsiPr[nPT-1][0].statErr/NJpsiPr[nPT-1][0].mean, 100*NJpsiSec[nPT-1][0].statErr/NJpsiSec[nPT-1][0].mean, 100*NEtacPr[nPT-1][0].statErr/NEtacPr[nPT-1][0].mean, 100*NEtacSec[nPT-1][0].statErr/NEtacSec[nPT-1][0].mean))


        for nConfM in systDictMUncorr:
            
            idxM = nMasses.index(nConfM)
            #fo.write("%s & %2.1f & %2.1f & %2.1f & %2.1f"%(systDictMUncorr[nConfM], NJpsiPr[nPT-1][idxM + 1].systErr, NJpsiSec[nPT-1][idxM + 1].systErr, NEtacPr[nPT-1][idxM + 1].systErr, NEtacSec[nPT-1][idxM + 1].systErr))
            fo.write("%s & %2.1f & %2.1f & %2.1f & %2.1f"%(systDictMUncorr[nConfM], 100*NJpsiPr[nPT-1][idxM + 1].systErr/NJpsiPr[nPT-1][0].mean, 100*NJpsiSec[nPT-1][idxM + 1].systErr/NJpsiSec[nPT-1][0].mean, 100*NEtacPr[nPT-1][idxM + 1].systErr, 100*NEtacSec[nPT-1][idxM + 1].systErr))
            fo.write(" \\\\   \n ")

        fo.write(" \\hline  \n")

        for nConfTz in nTzs:

            idxTz = nTzs.index(nConfTz)
            #fo.write("%s & %2.1f & %2.1f & %2.1f & %2.1f"%(systDictTz[nConfTz], NJpsiPr[nPT-1][nConfsMass + idxTz + 1].systErr, NJpsiSec[nPT-1][nConfsMass + idxTz + 1].systErr, NEtacPr[nPT-1][nConfsMass + idxTz + 1].systErr, NEtacSec[nPT-1][nConfsMass + idxTz + 1].systErr))
            fo.write("%s & %2.1f & %2.1f & %2.1f & %2.1f"%(systDictTz[nConfTz], 100*NJpsiPr[nPT-1][nConfsMass + idxTz + 1].systErr/NJpsiPr[nPT-1][0].mean, 100*NJpsiSec[nPT-1][nConfsMass + idxTz + 1].systErr/NJpsiSec[nPT-1][0].mean, 100*NEtacPr[nPT-1][nConfsMass + idxTz + 1].systErr, 100*NEtacSec[nPT-1][nConfsMass + idxTz + 1].systErr))
            fo.write(" \\\\   \n ")


        #NJpsiPr[nPT-1][0].systErr = NJpsiPr[nPT-1][0].systErr**0.5
        #NJpsiSec[nPT-1][0].systErr = NJpsiSec[nPT-1][0].systErr**0.5
        NEtacPr[nPT-1][0].systErr = NEtacPr[nPT-1][0].systErr**0.5
        NEtacSec[nPT-1][0].systErr = NEtacSec[nPT-1][0].systErr**0.5

        fo.write(" \\hline  \n")
        fo.write(" \\hline  \n")
        #fo.write("Tot Syst Uncorr & %2.1f & %2.1f & %2.1f & %2.1f"%( NJpsiPr[nPT-1][0].systErr, NJpsiSec[nPT-1][0].systErr, NEtacPr[nPT-1][0].systErr, NEtacSec[nPT-1][0].systErr))
        fo.write("Tot Syst Uncorr & %2.1f & %2.1f & %2.1f & %2.1f"%( 100*NJpsiPr[nPT-1][0].systErr/NJpsiPr[nPT-1][0].mean, 100*NJpsiSec[nPT-1][0].systErr/NJpsiSec[nPT-1][0].mean, 100*NEtacPr[nPT-1][0].systErr, 100*NEtacSec[nPT-1][0].systErr))
        fo.write(" \\\\   \n")
        fo.write(" \\hline  \n")
        fo.write(" \\hline  \n")

        #fo.write("%s & %2.1f & %2.1f & %2.1f & %2.1f  "%(systDictGen[1], NJpsiPr[nPT-1][nConfsMass + nConfsTz + 1].systErrCorr, NJpsiSec[nPT-1][nConfsMass + nConfsTz + 1].systErrCorr, NEtacPr[nPT-1][nConfsMass + nConfsTz + 1].systErrCorr, NEtacSec[nPT-1][nConfsMass + nConfsTz + 1].systErrCorr))
        fo.write("%s & %2.1f & %2.1f & %2.1f & %2.1f  "%(systDictGen[1], 100*NJpsiPr[nPT-1][nConfsMass + nConfsTz + 1].systErrCorr, 100*NJpsiSec[nPT-1][nConfsMass + nConfsTz + 1].systErrCorr, 100*NEtacPr[nPT-1][nConfsMass + nConfsTz + 1].systErrCorr, 100*NEtacSec[nPT-1][nConfsMass + nConfsTz + 1].systErrCorr))
        fo.write(" \\\\   \n ")

        for nConfM in systDictMCorr:
            
            idxM = nMasses.index(nConfM)
            #fo.write("%s & %2.1f & %2.1f & %2.1f & %2.1f"%(systDictMCorr[nConfM], NJpsiPr[nPT-1][idxM + 1].systErrCorr, NJpsiSec[nPT-1][idxM + 1].systErrCorr, NEtacPr[nPT-1][idxM + 1].systErrCorr, NEtacSec[nPT-1][idxM + 1].systErrCorr))
            fo.write("%s & %2.1f & %2.1f & %2.1f & %2.1f"%(systDictMCorr[nConfM], 100*NJpsiPr[nPT-1][idxM + 1].systErrCorr, 100*NJpsiSec[nPT-1][idxM + 1].systErrCorr, 100*NEtacPr[nPT-1][idxM + 1].systErrCorr, 100*NEtacSec[nPT-1][idxM + 1].systErrCorr))
            fo.write(" \\\\   \n ")

        fo.write(" \\hline  \n")
        fo.write(" \\hline  \n")
        #fo.write("Tot Syst Corr & %2.1f & %2.1f & %2.1f & %2.1f"%( NJpsiPr[nPT-1][0].systErrCorr, NJpsiSec[nPT-1][0].systErrCorr, NEtacPr[nPT-1][0].systErrCorr, NEtacSec[nPT-1][0].systErrCorr))
        fo.write("Tot Syst Corr & %2.1f & %2.1f & %2.1f & %2.1f"%( 100*NJpsiPr[nPT-1][0].systErrCorr, 100*NJpsiSec[nPT-1][0].systErrCorr, 100*NEtacPr[nPT-1][0].systErrCorr, 100*NEtacSec[nPT-1][0].systErrCorr))
        fo.write(" \\\\   \n")
        fo.write(" \\hline  \n")

        fo.write(" \\hline  \n")
        #fo.write("Tot Syst & %2.1f & %2.1f & %2.1f & %2.1f"%( (NJpsiPr[nPT-1][0].systErr**2 + NJpsiPr[nPT-1][0].systErrCorr**2)**0.5, (NJpsiSec[nPT-1][0].systErr**2 + NJpsiSec[nPT-1][0].systErrCorr**2)**0.5, (NEtacPr[nPT-1][0].systErr**2 + NEtacPr[nPT-1][0].systErrCorr**2)**0.5, (NEtacSec[nPT-1][0].systErr**2 + NEtacSec[nPT-1][0].systErrCorr**2)**0.5))
        fo.write("Tot Syst & %2.1f & %2.1f & %2.1f & %2.1f"%( 100*((NJpsiPr[nPT-1][0].systErr/NJpsiPr[nPT-1][0].mean)**2 + NJpsiPr[nPT-1][0].systErrCorr**2)**0.5, 100*((NJpsiSec[nPT-1][0].systErr/NJpsiSec[nPT-1][0].mean)**2 + NJpsiSec[nPT-1][0].systErrCorr**2)**0.5, 100*(NEtacPr[nPT-1][0].systErr**2 + NEtacPr[nPT-1][0].systErrCorr**2)**0.5, 100*(NEtacSec[nPT-1][0].systErr**2 + NEtacSec[nPT-1][0].systErrCorr**2)**0.5))
        fo.write(" \\\\   \n")
        fo.write(" \\hline  \n")

        fo.write(" \t \\end{tabular*} \n")
        fo.write("  \\end{table} \n")
        fo.write("\\end{frame} \n")
        fo.close()



        fCS.write(" %5.2f  %5.2f  %2.3f  %2.3f  \n"%( NJpsiPr[nPT-1][0].mean, NJpsiSec[nPT-1][0].mean, NEtacPr[nPT-1][0].mean, NEtacSec[nPT-1][0].mean))
        fCS.write(" %5.2f  %5.2f  %2.3f  %2.3f  \n"%( NJpsiPr[nPT-1][0].statErr, NJpsiSec[nPT-1][0].statErr, NEtacPr[nPT-1][0].statErr, NEtacSec[nPT-1][0].statErr))
        fCS.write(" %5.2f  %5.2f  %2.3f  %2.3f  \n "%( NJpsiPr[nPT-1][0].systErr, NJpsiSec[nPT-1][0].systErr, NEtacPr[nPT-1][0].mean*NEtacPr[nPT-1][0].systErr, NEtacSec[nPT-1][0].mean*NEtacSec[nPT-1][0].systErr))
        fCS.write(" %5.2f  %5.2f  %2.3f  %2.3f  \n \n"%( NJpsiPr[nPT-1][0].mean*NJpsiPr[nPT-1][0].systErrCorr, NJpsiSec[nPT-1][0].mean*NJpsiSec[nPT-1][0].systErrCorr, NEtacPr[nPT-1][0].mean*NEtacPr[nPT-1][0].systErrCorr, NEtacSec[nPT-1][0].mean*NEtacSec[nPT-1][0].systErrCorr))

    fo.close()
    fCS.close()

def systDiff():
    
    nTzs = [ 1, 2, 3, 4]
    nMs = [ 1, 2, 3, 4, 6, 8]
    nGen = [ 1, 2]

    nPTBins = [1, 2, 3, 4]
    processPT(nPTBins, nTzs, nMs, nGen)
    #process(nPTBins, nTzs, nMs, nGen)

gROOT.LoadMacro("../lhcbStyle.C")
systDiff()