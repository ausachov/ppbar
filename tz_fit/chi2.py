from ROOT import *
#from array import array
import numpy as np

s1 = [1.09494, 1.01542, 0.96146, 0.94688]   
tau = [1.06198, 1.05187, 0.99203, 0.88069]  
rS = [1.0, 1.0, 1.0, 1.0]  
beta = [1.0, 1.0, 1.0, 1.0]  


iparJpsi = [[0, 1, 2, 3, 4,  5,  6,  7],
            [0, 1, 2, 3, 4, 11, 12, 13],
            [0, 1, 2, 3, 4, 17, 18, 19],
            [0, 1, 2, 3, 4, 23, 24, 25]]


iparEtac = [ [0, 1, 2, 3, 4,  5,  6,  7,  8,  9, 10],
             [0, 1, 2, 3, 4, 11, 12, 13, 14, 15, 16], 
             [0, 1, 2, 3, 4, 17, 18, 19, 20, 21, 22], 
             [0, 1, 2, 3, 4, 23, 24, 25, 26, 27, 28]]


def getNorm(nConf=0):
    
    ptBins= [7.250, 9.000, 11.000, 13.000] 
    f0 = TFile("/users/LHCb/zhovkovska/scripts/Results/MC/TzFit/parFit.root","READ") 
    fS = f0.Get("fSigma")
    fSAlt = f0.Get("fSigmaAlt")
    fT  = f0.Get("fTauB")
    f_rS = f0.Get("f_rS")
    fBeta  = f0.Get("fBeta")
    hS = f0.Get("h_sigma_tz")
    hT  = f0.Get("h_tauB")
    f0.Close()
    
    
    fTemp = TFile("/users/LHCb/zhovkovska/scripts/Results/MC/TzFit/MC_TzRes_2016_wksp.root","READ") 
    wMC = fTemp.Get("w;1")
    fTemp.Close()
    
    for iPT in range(4):
        if (nConf!=1):
            s1[iPT] = fS.Eval(ptBins[iPT])/wMC.var("S1").getValV()
        else:
            s1[iPT] = fSAlt.Eval(ptBins[iPT])/wMC.var("S1").getValV()
        #             s1[iPT] = hS.GetBinContent(iPT+1)/wMC.var("S1").getValV()
        if (nConf==3):
            rS[iPT] = f_rS.Eval(ptBins[iPT])/wMC.var("rS").getValV()
            beta[iPT] = fBeta.Eval(ptBins[iPT])/wMC.var("beta").getValV()
    
    fTemp = TFile("/users/LHCb/zhovkovska/scripts/Results/MC/TzFit/MC_TRUE_Tz_2016_wksp.root","READ") 
    wMC = fTemp.Get("w;1")
    fTemp.Close()
    
        
    for iPT in range(4):
        if (nConf!=2):
            tau[iPT] = fT.Eval(ptBins[iPT])/wMC.var("tauB").getValV()
        else:
            tau[iPT] = hT.GetBinContent(iPT+1)/wMC.var("tauB").getValV()


class GlobalChi2:
  
    fChi2_Jpsi = []
    fChi2_Etac = []
    
    def __init__(self, fJpsi, fEtac): 
        
        for iPT in range(len(fJpsi)):
            self.fChi2_Jpsi.append(fJpsi[iPT])
            self.fChi2_Etac.append(fEtac[iPT])


    def __call__(self, par):
        
        #fTotChi2 = ""
        nPTBins = 4
        getNorm(0)
        p1 = np.array(nPTBins*[8*[0]])
        p2 = np.array(nPTBins*[11*[0]])
        for iPT in range(nPTBins):
            
            for i in range(8): p1[iPT][i] = par[iparJpsi[iPT][i]]        
            p1[iPT][1] *= beta[iPT]
            p1[iPT][2] *= s1[iPT]
            p1[iPT][3] *= rS[iPT]
            p1[iPT][4] *= tau[iPT]
            
            for i in range(10): p2[iPT][i] = par[iparEtac[iPT][i]]
            ind = iPT*6
            p2[iPT][10] = (par[ind+5]*par[ind+8] + par[ind+6]*par[ind+9])*par[ind+7]
            p2[iPT][1] *= beta[iPT]
            p2[iPT][2] *= s1[iPT]
            p2[iPT][3] *= rS[iPT]
            p2[iPT][4] *= tau[iPT]
            
            #fTotChi2 += self.fChi2_Jpsi[iPT](p1[iPT]) + self.fChi2_Etac[iPT](p2[iPT])
            #print fTotChi2
        return self.fChi2_Jpsi[0](p1[0]) + self.fChi2_Etac[0](p2[0]) + self.fChi2_Jpsi[1](p1[1]) + self.fChi2_Etac[1](p2[1]) + self.fChi2_Jpsi[2](p1[2]) + self.fChi2_Etac[2](p2[2]) + self.fChi2_Jpsi[3](p1[3]) + self.fChi2_Etac[3](p2[3])
        #return fTotChi2
    
    #fChi2_1 = Math.IMultiGenFunction()
    #fChi2_2 = Math.IMultiGenFunction()
    #fChi2_3 = Math.IMultiGenFunction()
    #fChi2_4 = Math.IMultiGenFunction()
    #fChi2_5 = Math.IMultiGenFunction()
    #fChi2_6 = Math.IMultiGenFunction()
    #fChi2_7 = Math.IMultiGenFunction()
    #fChi2_8 = Math.IMultiGenFunction()
    
