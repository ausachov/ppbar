#include "Math/WrappedMultiTF1.h"
#include "TROOT.h"
#include "Fit/Fitter.h"
#include "Fit/BinData.h"
#include "Fit/Chi2FCN.h"
#include "TH1.h"
#include "TList.h"
#include "Math/WrappedMultiTF1.h"
#include "HFitInterface.h"
#include "TCanvas.h"
#include "TStyle.h"
#include "TKDE.h"
#include "TChain.h"
#include "TFile.h"
#include "TLegend.h"
#include "TProof.h"
#include "RooRealVar.h"
#include "RooWorkspace.h"

using namespace RooFit ;
using namespace ROOT ;
using namespace TMath ;

double s1[4] = {1.09494, 1.01542, 0.96146, 0.94688};  
double tau[4] = {1.06198, 1.05187, 0.99203, 0.88069};  
double rS[4] = {1.0, 1.0, 1.0, 1.0};  
double beta[4] = {1.0, 1.0, 1.0, 1.0};  


Int_t iparJpsi[4][8] = { {0,    // mu
    1,    // beta
    2,    // S1
    3,    // rS
    4,    // tauB
    5,    // NpJpsi
    6,    // NbJpsi
    7},     // NtJpsi/Ntot
    {0, 1, 2, 3, 4, 11, 12, 13},
    {0, 1, 2, 3, 4, 17, 18, 19},
    {0, 1, 2, 3, 4, 23, 24, 25}                         
};


Int_t iparEtac[4][11]  = { {0,    // mu
    1,    // beta
    2,    // S1
    3,    // rS
    4,    // tauB
    5,    // NpJpsi
    6,    // NbJpsi
    7,    // NtJpsi/Ntot
    8,    // NpEtac/NpJpsi
    9,    // NbEtac/NbJpsi
    10},    // NtJpsi
    {0, 1, 2, 3, 4, 11, 12, 13, 14, 15, 16}, 
    {0, 1, 2, 3, 4, 17, 18, 19, 20, 21, 22}, 
    {0, 1, 2, 3, 4, 23, 24, 25, 26, 27, 28} 
    
};

void getNorm(Int_t nConf=0){
    
    Double_t ptBins[4] = {7.250, 9.000, 11.000, 13.000/*, 16000*/}; 
    TFile *f0 = new TFile("/users/LHCb/zhovkovska/scripts/Results/MC/TzFit/parFit.root","READ") ;
    TF1 fS = *(TF1*) f0->Get("fSigma");
    TF1 fSAlt = *(TF1*) f0->Get("fSigmaAlt");
    TF1 fT  = *(TF1*) f0->Get("fTauB");
    TF1 f_rS = *(TF1*) f0->Get("f_rS");
    TF1 fBeta  = *(TF1*) f0->Get("fBeta");
    TH1D hS = *(TH1D*) f0->Get("h_sigma_tz");
    TH1D hT  = *(TH1D*) f0->Get("h_tauB");
    f0->Close();
    
    
    TFile *fTemp = new TFile("/users/LHCb/zhovkovska/scripts/Results/MC/TzFit/MC_TzRes_2016_wksp.root","READ") ;
    RooWorkspace *wMC = (RooWorkspace*) fTemp->Get("w;1");
    fTemp->Close();
    
    for (int iPT=0; iPT<4; iPT++){
        if (nConf!=1)
            s1[iPT] = fS.Eval(ptBins[iPT])/wMC->var("S1")->getValV();
        else
            s1[iPT] = fSAlt.Eval(ptBins[iPT])/wMC->var("S1")->getValV();
        //             s1[iPT] = hS.GetBinContent(iPT+1)/wMC->var("S1")->getValV();
        if (nConf==3){
            rS[iPT] = f_rS.Eval(ptBins[iPT])/wMC->var("rS")->getValV();
            beta[iPT] = fBeta.Eval(ptBins[iPT])/wMC->var("beta")->getValV();
        }
    }
    
    fTemp = new TFile("/users/LHCb/zhovkovska/scripts/Results/MC/TzFit/MC_TRUE_Tz_2016_wksp.root","READ") ;
    wMC = (RooWorkspace*) fTemp->Get("w;1");
    fTemp->Close();
    
    
    for (int iPT=0; iPT<4; iPT++){
        if (nConf!=2)
            tau[iPT] = fT.Eval(ptBins[iPT])/wMC->var("tauB")->getValV();
        else
            tau[iPT] = hT.GetBinContent(iPT+1)/wMC->var("tauB")->getValV();
    }
}

struct EvalChi2 {
    EvalChi2(  Math::IMultiGenFunction & f1,
               Math::IMultiGenFunction & f2  ):
               fChi2_1(&f1), fChi2_2(&f2) {}
               
               double evalChi2Jpsi (int nPT, const double *par) const {
                   
                   double p1[8];
                   for (int i = 0; i < 8; ++i) p1[i] = par[iparJpsi[nPT][i] ];
                   p1[1] *= beta[nPT];
                   p1[2] *= s1[nPT];
                   p1[3] *= rS[nPT];
                   p1[4] *= tau[nPT];
                   
                   return (*fChi2_1)(p1);
               }
               
               double evalChi2Etac (int nPT, const double *par) const {
                   double p2[11];
                   for (int i = 0; i < 10; ++i) p2[i] = par[iparEtac[nPT][i] ];
                   int ind = nPT * 6;   
                   p2[10] = (par[ind+5]*par[ind+8] + par[ind+6]*par[ind+9])*par[ind+7];
                   p2[1] *= beta[nPT];
                   p2[2] *= s1[nPT];
                   p2[3] *= rS[nPT];
                   p2[4] *= tau[nPT];
                   
                   return (*fChi2_2)(p2);
               }
               
               const  Math::IMultiGenFunction * fChi2_1;
               const  Math::IMultiGenFunction * fChi2_2;
};

// struct GlobalChi2 {
//     GlobalChi2(  Math::IMultiGenFunction & f1,
//                  Math::IMultiGenFunction & f2,
//                  Math::IMultiGenFunction & f3,
//                  Math::IMultiGenFunction & f4,
//                  Math::IMultiGenFunction & f5,
//                  Math::IMultiGenFunction & f6,
//                  Math::IMultiGenFunction & f7,
//                  Math::IMultiGenFunction & f8
//     ) :
//     fChi2_1(&f1), fChi2_2(&f2), fChi2_3(&f3), fChi2_4(&f4), fChi2_5(&f5), fChi2_6(&f6), fChi2_7(&f7), fChi2_8(&f8) {}
//     
//     double operator() (const double *par) const {
//         getNorm();
//         int nPTBins = 4;
//         double p1[nPTBins][8];
//         double p2[nPTBins][11];
//         for (int iPT = 0; iPT<nPTBins; iPT++ )
//         {
//             for (int i = 0; i < 8; ++i) p1[iPT][i] = par[iparJpsi[iPT][i]];        
//             p1[iPT][1] *= beta[iPT];
//             p1[iPT][2] *= s1[iPT];
//             p1[iPT][3] *= rS[iPT];
//             p1[iPT][4] *= tau[iPT];
//             //           std::cout<<p1[iPT][2]<<"  "<<p1[iPT][4]<<std::endl;
//             
//             for (int i = 0; i < 10; ++i) p2[iPT][i] = par[iparEtac[iPT][i]];
//             int ind = iPT*6;
//             p2[iPT][10] = (par[ind+5]*par[ind+8] + par[ind+6]*par[ind+9])*par[ind+7];
//             p2[iPT][1] *= beta[iPT];
//             p2[iPT][2] *= s1[iPT];
//             p2[iPT][3] *= rS[iPT];
//             p2[iPT][4] *= tau[iPT];
//             
//         }  
//         
//         return (*fChi2_1)(p1[0]) + (*fChi2_2)(p2[0]) + (*fChi2_3)(p1[1]) + (*fChi2_4)(p2[1]) + (*fChi2_5)(p1[2]) + (*fChi2_6)(p2[2]) + (*fChi2_7)(p1[3]) + (*fChi2_8)(p2[3]);
//     }
//     
//     const  Math::IMultiGenFunction * fChi2_1;
//     const  Math::IMultiGenFunction * fChi2_2;
//     const  Math::IMultiGenFunction * fChi2_3;
//     const  Math::IMultiGenFunction * fChi2_4;
//     const  Math::IMultiGenFunction * fChi2_5;
//     const  Math::IMultiGenFunction * fChi2_6;
//     const  Math::IMultiGenFunction * fChi2_7;
//     const  Math::IMultiGenFunction * fChi2_8;
// };

// getNorm();