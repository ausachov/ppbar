from multiprocessing import Pool
from functools import partial
from contextlib import closing
from ROOT import *

gROOT.LoadMacro("FitTime_rel_tail.C");

def f(list):
    nPTBin = list[0]
    nConf = list[1]
    nConfM = list[2]
    return fitSim(kTRUE,nPTBin, nConf, nConfM);

PT = [1, 2, 3, 4]
#PT = [0]
#Conf = [1, 2, 3, 4, 5, 6]
Conf = [0]
ConfMass = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
#ConfMass = [0]
a = [[PT[iPT], Conf[iConf], ConfMass[iConfM]] for iPT in range(len(PT)) for iConf in range(len(Conf)) for iConfM in range(len(ConfMass))]
Conf = [1, 2, 3, 4, 5, 6]
ConfMass = [0]
b = [[PT[iPT], Conf[iConf], ConfMass[iConfM]] for iPT in range(len(PT)) for iConf in range(len(Conf)) for iConfM in range(len(ConfMass))]
#print(a)

if __name__ == '__main__':
    with closing(Pool(8)) as p:
        (p.map(f, a))
        p.map(f,b)
        p.terminate()
