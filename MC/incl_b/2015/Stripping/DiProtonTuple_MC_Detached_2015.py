from ROOT import gROOT, TChain, TCut, TFile, TTree, TProof

chain = TChain("Jpsi2ppDetachedTuple/DecayTree")

#chain.Add("/afs/cern.ch/work/v/vazhovko/gangadir/workspace/vazhovko/LocalXML/65/output/Tuple.root")
#chain.Add("/afs/cern.ch/work/v/vazhovko/gangadir/workspace/vazhovko/LocalXML/66/output/Tuple.root")

sjMax=55
for i in range(sjMax):
    chain.Add("/eos/lhcb/user/v/vazhovko/gangadir/workspace/vazhovko/LocalXML/58/"+str(i)+"/output/Tuple.root")

sjMax=55
for i in range(sjMax):
    chain.Add("/eos/lhcb/user/v/vazhovko/gangadir/workspace/vazhovko/LocalXML/59/"+str(i)+"/output/Tuple.root")


import sys
sys.path.insert(0, '../../../../')
from makeConfigDictionary import cutsAprioriDetached
totCut = TCut(cutsAprioriDetached)

newfile = TFile("EtacDiProton_MC_2015_Detached_incl_b.root","recreate")

newtree=TTree()
newtree.SetMaxTreeSize(500000000)
newtree = chain.CopyTree(totCut.GetTitle())

newtree.Print()

newtree.GetCurrentFile().Write() 
newtree.GetCurrentFile().Close()
