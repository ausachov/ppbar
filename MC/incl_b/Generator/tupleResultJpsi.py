
from Configurables import (
                           DaVinci,
                           EventSelector,
                           PrintMCTree,
                           MCDecayTreeTuple
                           )
from DecayTreeTuple.Configuration import *




# https://twiki.cern.ch/twiki/bin/view/LHCb/FAQ/LoKiNewDecayFinders
decay = "(J/psi(1S) -> ^p+ ^p~-)"
decay_heads = ["J/psi(1S)"]



branches = {
    "ProtonP"  :  "J/psi(1S) ->^p+ p~-"
    ,"ProtonM"  :  "J/psi(1S) -> p+^p~-"
    ,"Jpsi"     : "^(J/psi(1S) -> p+ p~-)"
}


NSubjobs = 1000
datafile = "/eos/user/a/ausachov/MC/pp/GenLevel/163/0/output/Gauss-24102004-5ev-20170323.xgen"
for sj in range(1,NSubjobs):
    datafile = datafile+",/eos/user/a/ausachov/MC/pp/GenLevel/163/"+str(sj)+"/output/Gauss-24102004-5ev-20170323.xgen"

#datafile = "/eos/user/a/ausachov/MC/pp/GenLevel/160/1/output/Gauss-24102012-5ev-20170322.xgen"
year = 2016

# For a quick and dirty check, you don't need to edit anything below here.
##########################################################################

# Create an MC DTT containing any candidates matching the decay descriptor
mctuple = MCDecayTreeTuple("MCDecayTreeTuple")
mctuple.Decay = decay
mctuple.Branches = branches
mctuple.ToolList = [
                    "MCTupleToolHierarchy",
                    "MCTupleToolAngles"
                    #    "MCTupleToolGeometry",
                    #    "TupleToolPrimaries",
                    #    "TupleToolRecoStats"
                    ]

mctuple.addTupleTool("MCTupleToolKinematic").Verbose = True


# Print the decay tree for any particle in decay_heads
#printMC = PrintMCTree()
#printMC.ParticleNames = decay_heads

# Name of the .xgen file produced by Gauss
EventSelector().Input = ["DATAFILE='{0}' TYP='POOL_ROOTTREE' Opt='READ'".format(datafile)]

# Configure DaVinci
DaVinci().TupleFile = "/eos/user/a/ausachov/MC/pp/GenLevel/163/Jpsi-24102004-Total.root"
DaVinci().Simulation = True
DaVinci().EvtMax = -1                          # Number of events
DaVinci().SkipEvents = 0                       # Events to skip
DaVinci().PrintFreq = 1000
DaVinci().Lumi = False
DaVinci().DataType = str(year)
DaVinci().UserAlgorithms = [mctuple]
#DaVinci().UserAlgorithms = [printMC, mctuple]