
import sys
import os


myApp = GaudiExec()
myApp.directory = '$HOME/cmtuser/GaussDev_v49r8'

#os.environ["DECFILESROOT"] = "/afs/cern.ch/user/a/ausachov/cmtuser/Gauss_v49r7/Gen/DecFiles"
j = Job()
j.application = myApp
j.application.options = ['./Gauss-Job.py','./Gauss-2016.py','./GenStandAlone.py','./Pythia8.py','./28102064.py']
j.name = "GenLevel_28102064_Beam6500GeV-md100-2016-nu1.6_Pythia8"
j.splitter = GaussSplitter(numberOfJobs=2,eventsPerJob=2)
#j.inputfiles = [LocalFile("/eos/user/a/ausachov/MC/pp/GenLevel/incl_b=etac1S,ppbar,NoCut.dec")]
j.outputfiles = [LocalFile("*.xgen"),LocalFile("*.root"),LocalFile("*.xml")]
j.backend = Dirac()
j.parallel_submit = True
j.submit()
