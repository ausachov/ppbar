# RUN this script from ganga

from ROOT import *


gangadir = "/afs/cern.ch/work/a/ausachov/gangadir/workspace/ausachov/LocalXML"

MCDict = {
    "Etac"  :{"2015"   : {"Pr":[852,853],"Sec":[854,855],"SecNoPID":[-1,-1]  ,"Gen":[785,786]},
              "2016"   : {"Pr":[682,683],"Sec":[684,685],"SecNoPID":[905,906],"Gen":[686,687]},
              "2015bis": {"Pr":[856,857],"Sec":[858,859],"SecNoPID":[903,904],"Gen":[817,818]},
              "2016bis": {"Pr":[655,656],"Sec":[658,659],"SecNoPID":[907,908],"Gen":[660,661]}},

    "Jpsi"  :{"2015"   : {"Pr":[835,836],"Sec":[837,838],"SecNoPID":[-1,-1]  ,"Gen":[699,700]},
              "2016"   : {"Pr":[861,806],"Sec":[807,808],"SecNoPID":[899,900],"Gen":[707,708]},
              "2015bis": {"Pr":[825,822],"Sec":[678,679],"SecNoPID":[897,898],"Gen":[796,797]},
              "2016bis": {"Pr":[662,663],"Sec":[664,665],"SecNoPID":[901,902],"Gen":[667,668]}},

    "incl_b":{"2015"   : {"Pr":[773,774],"Sec":[775,776],"SecNoPID":[920,921],"Gen":[713,714]},
              "2016"   : {"Pr":[777,778],"Sec":[779,780],"SecNoPID":[922,923],"Gen":[720,721]}},
              #"2015": {"Pr":[709,710], "Sec":[711,712], 
              #"2016": {"Pr":[716,717], "Sec":[718,719], 

"Jpsi2pppi0":{"2015"   : {"Pr":[841,843],"Sec":[844,845],"SecNoPID":[-1,-1],"Gen":[730,731]},
              "2016"   : {"Pr":[734,735],"Sec":[736,737],"SecNoPID":[-1,-1],"Gen":[742,744]},
              "2015bis": {"Pr":[809,810],"Sec":[728,729],"SecNoPID":[-1,-1],"Gen":[819,820]},
              "2016bis": {"Pr":[738,739],"Sec":[740,741],"SecNoPID":[-1,-1],"Gen":[745,746]}}
}



TupleNameDict = {"Pr"      :"Jpsi2ppTuple/DecayTree",
                 "Sec"     :"Jpsi2ppDetachedTuple/DecayTree",
                 "SecNoPID":"Jpsi2ppDetachedNoPIDTuple/DecayTree",
                 "Gen"     :"MCDecayTreeTuple/MCDecayTree"}

StripKeys = ["Pr", "Sec", "SecNoPID", "Gen"]
# StripKeys = ["Gen"]



# import sys
# sys.path.insert(0, '../../../../')
# from makeConfigDictionary import cutsAprioriDetached
# totCut = TCut(cutsAprioriDetached)

def makeSingleChain(yearDict,stripKey,decTypeKey,yearKey,printList):
  
  jobIDs = yearDict[stripKey]

  if jobIDs[0]>0:

    tupleName = TupleNameDict[stripKey]
    chain = TChain(tupleName)

    filename = "tuples/rawTuples/"+decTypeKey+"_"+stripKey+"_"+yearKey+".root"
    newfile = TFile(filename,"recreate")

    print "\n"
    for ID in jobIDs:
      print "processing job", ID
      for sjID in range(len(jobs(ID).subjobs)):
        chain.Add(gangadir+"/"+str(ID)+"/"+str(sjID)+"/output/Tuple.root")
        


    nTotEvt = chain.GetEntries()
    printList.append([stripKey, decTypeKey, yearKey, nTotEvt])


    newtree = TTree()
    newtree = chain.CopyTree('')

    newfile.Write() 
    newfile.Close()
  
    del chain, newfile, newtree



printList = []

for stripKey in StripKeys:
  for decTypeKey in MCDict.keys():
    decTypeDict = MCDict[decTypeKey]
    yearKeys = decTypeDict.keys()
    for yearKey in yearKeys:
      yearDict = decTypeDict[yearKey]
      makeSingleChain(yearDict,stripKey,decTypeKey,yearKey,printList)

        

with open("MC_yields.txt","w+") as f:
  f.write(" \t stripKey\t decTypeKey\t yearKey\t nTotEvt\n")
  for listMem in printList:
    f.write(str(listMem[0])+"\t ")
    f.write(str(listMem[1])+"\t ")
    f.write(str(listMem[2])+"\t ")
    f.write(str(listMem[3])+"\n ")
f.close()






