void Draw(int mcNumber = 0)
{
    gROOT->Reset();
    TChain * nt=new TChain("DecayTree");

    TString in_name;
    
    switch(mcNumber)
    {
        case 0:
            in_name = "Etac/2016/EtacDiProton_MC_2016_AddBr.root";
            break;
        case 1:
            in_name = "Jpsi/2016/JpsiDiProton_MC_2016_AddBr.root";
            break;
        case 2:
            in_name = "Etac/2015/EtacDiProton_MC_2015_AddBr.root";
            break;
        case 3:
            in_name = "Jpsi/2015/JpsiDiProton_MC_2015_AddBr.root";
            break;
    }
    
    
    nt->Add(in_name);
    
    int bMesID[9] = {5, 511, 521, 531, 541, -511, -521, -531, -541};
    int bHadrID[23] = { 5, 511, 521, 531, 541, -511, -521, -531, -541, 5122, 5112, 5212, 5222, 5132, 5232, 5332, -5122, -5112, -5212, -5222, -5132, -5232, -5332};
    int charmID[14] = { 4, 441, 10441, 100441, 443, 10443, 20443, 100443, 30443, 9000443, 9010443, 9020443, 445, 100445};

    
    
    
    
    Double_t left=2, right = 4.5;
    
    
    TH1F* h1 = new TH1F("indirect","indirect",10,left,right);
    TH1F* h2 = new TH1F("direct","direct",10,left,right);
    
//    nt->Draw("Jpsi_TRUEM-Jpsi_M>>indirect","prompt && Jpsi_MC_MOTHER_ID>5","E");
//    nt->Draw("Jpsi_TRUEM-Jpsi_M>>direct","prompt && Jpsi_MC_MOTHER_ID<5","Esame");
    
//    nt->Draw("Jpsi_PT>>indirect","prompt && Jpsi_MC_MOTHER_ID>5","E");
//    nt->Draw("Jpsi_PT>>direct","prompt && Jpsi_MC_MOTHER_ID<5","Esame");
    
    
    nt->Draw("Jpsi_ETA>>indirect","prompt && Jpsi_MC_MOTHER_ID>5","E");
    nt->Draw("Jpsi_ETA>>direct","prompt && Jpsi_MC_MOTHER_ID<5","Esame");
    
    
    
    h2->SetLineColor(kRed);
    TCanvas* c = new TCanvas("Signal_Fit","Signal Fit",800,500);
    h2->Draw("E");
    h1->Draw("Esame");
    
    TLegend* leg = new TLegend(0.8,0.8,0.99,0.99);
    leg->AddEntry(h1);
    leg->AddEntry(h2);
    leg->Draw();
    

}


