def fillTuple( tuple, myBranches, myTriggerList ):
    
    tuple.Branches = myBranches
    
    tuple.ToolList = [
                      "TupleToolAngles",
                      "TupleToolEventInfo",
                      "TupleToolGeometry",
                      "TupleToolKinematic",
                      "TupleToolPid",
                      "TupleToolPrimaries",
                      "TupleToolRecoStats",
                      "TupleToolTrackInfo",
                      "TupleToolANNPID",
                      "TupleToolPropertime"
                      ]
        

    # RecoStats for filling SpdMult, etc
    from Configurables import TupleToolRecoStats
    tuple.addTool(TupleToolRecoStats, name="TupleToolRecoStats")
    tuple.TupleToolRecoStats.Verbose=True

    from Configurables import TupleToolTISTOS, TupleToolDecay
    tuple.addTool(TupleToolDecay, name = 'Jpsi')


    # TISTOS for Jpsi
    tuple.Jpsi.ToolList+=[ "TupleToolTISTOS" ]
    tuple.Jpsi.addTool(TupleToolTISTOS, name="TupleToolTISTOS" )
    tuple.Jpsi.TupleToolTISTOS.Verbose=True
    tuple.Jpsi.TupleToolTISTOS.TriggerList = myTriggerList


    #LoKi one
    from Configurables import LoKi__Hybrid__TupleTool
    LoKi_Jpsi=LoKi__Hybrid__TupleTool("LoKi_Jpsi")
    LoKi_Jpsi.Variables = {
      "LOKI_FDCHI2"          : "BPVVDCHI2",
      "LOKI_FDS"             : "BPVDLS",
      "LOKI_DIRA"            : "BPVDIRA",
      "LOKI_BPVCORRM"        : "BPVCORRM",
      "DOCA"     : "DOCA(1,2)",
      "m_scaled" : "DTF_FUN ( M , False )",
      "m_pv"     : "DTF_FUN ( M , True )",
      "c2dtf_1"  : "DTF_CHI2NDOF( False )" ,
      "c2dtf_2"  : "DTF_CHI2NDOF( True  )"
     }

    tuple.addTool(TupleToolDecay, name="Jpsi")
    tuple.Jpsi.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_Jpsi"]
    tuple.Jpsi.addTool(LoKi_Jpsi)
    

    from Configurables import TupleToolMCBackgroundInfo, TupleToolMCTruth

    tuple.ToolList += ["TupleToolMCBackgroundInfo",
                       "TupleToolMCTruth"
                       ]

    MCTruth = TupleToolMCTruth()
    MCTruth.ToolList =  ["MCTupleToolAngles"
                         , "MCTupleToolHierarchy"
                         , "MCTupleToolKinematic"
                         , "MCTupleToolReconstructed"
                         , "MCTupleToolPID" 
                         , "MCTupleToolDecayType"
                         ##, "MCTupleToolEventType"
                         ##, "MCTupleToolInteractions"
                         ##, "MCTupleToolP2VV"
                         ##, "MCTupleToolPrimaries"
                         , "MCTupleToolPrompt"
                         ##, "MCTupleToolRedecay"
                         ]
    #tuple.addTool(MCTruth)


    from LoKiMC.functions import MCMOTHER, MCVFASPF, MCPRIMARY

    from Configurables import LoKi__Hybrid__MCTupleTool
    MCTruth.addTool( LoKi__Hybrid__MCTupleTool, name = "MCLoKiHybrid" )
    MCTruth.ToolList += [ "LoKi::Hybrid::MCTupleTool/MCLoKiHybrid" ]
    MCTruth.MCLoKiHybrid.Preambulo = [ "from LoKiCore.functions import switch" ]
    MCTruth.MCLoKiHybrid.Variables = {
        "TRUE_Tz" : "(3.3)*(MCMOTHER(MCVFASPF(MCVZ), 0)-MCVFASPF(MCVZ))*MCM/MCPZ",          
        "TRUEM"   : "MCM",
        "TRUEETA" : "MCETA",
        "TRUEPHI" : "MCPHI",
        "TRUETHETA" : "MCTHETA",
        "SISTERS_1" : "MCMOTHER(MCCHILD(MCID,1), -99999)",
        "SISTERS_2" : "MCMOTHER(MCCHILD(MCID,2), -99999)",
        "SISTERS_3" : "MCMOTHER(MCCHILD(MCID,3), -99999)",
        "SISTERS_4" : "MCMOTHER(MCCHILD(MCID,4), -99999)",
        "SISTERS_5" : "MCMOTHER(MCCHILD(MCID,5), -99999)",
        "MC_ISPRIMARY"      : "MCVFASPF(switch(MCPRIMARY,1,0))",
        "MC_MOTHER_M"       : "MCMOTHER(MCM,   -1)",
        "MC_MOTHER_TRUEP"   : "MCMOTHER(MCP,   -9999)",
        "MC_MOTHER_TRUEETA" : "MCMOTHER(MCETA, -9999)",
        "MC_MOTHER_TRUEPHI" : "MCMOTHER(MCPHI, -9999)",
        "MC_MOTHER_TRUEV_X" : "MCMOTHER(MCVFASPF(MCVX), -9999)",
        "MC_MOTHER_TRUEV_Y" : "MCMOTHER(MCVFASPF(MCVY), -9999)",
        "MC_MOTHER_TRUEV_Z" : "MCMOTHER(MCVFASPF(MCVZ), -9999)",
        "MC_MOTHER_ISPRIMARY"  : "MCMOTHER(MCVFASPF(switch(MCPRIMARY,1,0)),0)",
#        "MC_MOTHER_M"       : "MCMOTHER(MCID,   -9999)",
        #"MC_MOTHER_ISPRIMARY"  : "MCMOTHER(MCSWITCH(MCVFASPF(MCPRIMARY),1,0), 0)",
        "MC_GD_MOTHER_M"       : "MCMOTHER(MCMOTHER(MCM,   -1), -1)",
        "MC_GD_MOTHER_TRUEETA" : "MCMOTHER(MCMOTHER(MCETA, -9999), -9999)",
        "MC_GD_MOTHER_TRUEPHI" : "MCMOTHER(MCMOTHER(MCPHI, -9999), -9999)",
        "MC_GD_MOTHER_TRUEP"   : "MCMOTHER(MCMOTHER(MCP,   -9999), -9999)",
        "MC_GD_MOTHER_TRUEV_X" : "MCMOTHER(MCMOTHER(MCVFASPF(MCVX), -9999), -9999)",
        "MC_GD_MOTHER_TRUEV_Y" : "MCMOTHER(MCMOTHER(MCVFASPF(MCVY), -9999), -9999)",
        "MC_GD_MOTHER_TRUEV_Z" : "MCMOTHER(MCMOTHER(MCVFASPF(MCVZ), -9999), -9999)",
        "MC_GD_MOTHER_ISPRIMARY"  : "MCMOTHER(MCMOTHER(MCVFASPF(switch(MCPRIMARY,1,0)),0),0)",
#        "MC_GD_MOTHER_ID"      : "MCMOTHER(MCMOTHER(MCID, -9999), -9999)",
        "MC_GD_GD_MOTHER_TRUEETA" : "MCMOTHER(MCMOTHER(MCMOTHER(MCETA, -9999), -9999), -9999)",
        "MC_GD_GD_MOTHER_TRUEPHI" : "MCMOTHER(MCMOTHER(MCMOTHER(MCPHI, -9999), -9999), -9999)",
        "MC_GD_GD_MOTHER_TRUEP"   : "MCMOTHER(MCMOTHER(MCMOTHER(MCP,   -9999), -9999), -9999)",
        "MC_GD_GD_MOTHER_TRUEV_X" : "MCMOTHER(MCMOTHER(MCMOTHER(MCVFASPF(MCVX), -9999), -9999), -9999)",
        "MC_GD_GD_MOTHER_TRUEV_Y" : "MCMOTHER(MCMOTHER(MCMOTHER(MCVFASPF(MCVY), -9999), -9999), -9999)",
        "MC_GD_GD_MOTHER_TRUEV_Z" : "MCMOTHER(MCMOTHER(MCMOTHER(MCVFASPF(MCVZ), -9999), -9999), -9999)",
        "MC_GD_GD_MOTHER_ISPRIMARY"  : "MCMOTHER(MCMOTHER(MCMOTHER(MCVFASPF(switch(MCPRIMARY,1,0)),0),0),0)",
        "MC_GD_GD_GD_MOTHER_TRUEETA" : "MCMOTHER(MCMOTHER(MCMOTHER(MCMOTHER(MCETA, -9999), -9999), -9999), -9999)",
        "MC_GD_GD_GD_MOTHER_TRUEPHI" : "MCMOTHER(MCMOTHER(MCMOTHER(MCMOTHER(MCPHI, -9999), -9999), -9999), -9999)",
        "MC_GD_GD_GD_MOTHER_TRUEP"   : "MCMOTHER(MCMOTHER(MCMOTHER(MCMOTHER(MCP,   -9999), -9999), -9999), -9999)",
        "MC_GD_GD_GD_MOTHER_TRUEV_X" : "MCMOTHER(MCMOTHER(MCMOTHER(MCMOTHER(MCVFASPF(MCVX), -9999), -9999), -9999), -9999)",
        "MC_GD_GD_GD_MOTHER_TRUEV_Y" : "MCMOTHER(MCMOTHER(MCMOTHER(MCMOTHER(MCVFASPF(MCVY), -9999), -9999), -9999), -9999)",
        "MC_GD_GD_GD_MOTHER_TRUEV_Z" : "MCMOTHER(MCMOTHER(MCMOTHER(MCMOTHER(MCVFASPF(MCVZ), -9999), -9999), -9999), -9999)",
        "MC_GD_GD_GD_MOTHER_ISPRIMARY"  : "MCMOTHER(MCMOTHER(MCMOTHER(MCMOTHER(MCVFASPF(switch(MCPRIMARY,1,0)),0),0),0),0)",
        }
    tuple.addTool(MCTruth)


    
    #LoKi one
    from Configurables import LoKi__Hybrid__TupleTool
    LoKi_All=LoKi__Hybrid__TupleTool("LoKi_All")
    LoKi_All.Variables = {
        "ETA"                  : "ETA",
        "Y"                    : "Y"  ,
        "LOKI_IPCHI2"          : "BPVIPCHI2()"
    }
    tuple.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_All"]
    tuple.addTool(LoKi_All)


# DecayTreeTuple
from Configurables import DecayTreeTuple
from Configurables import GaudiSequencer, CombineParticles, OfflineVertexFitter

myTriggerList = [
                 # L0
                 "L0ElectronDecision",
                 "L0PhotonDecision",
                 "L0HadronDecision",
                 # L0 Muon
                 "L0MuonDecision",
                 "L0MuonHighDecision",
                 "L0DiMuonDecision",
                 
                 
                 
                 # Hlt1 track
                 "Hlt1B2PhiPhi_LTUNBDecision",
                 "Hlt1TrackMuonDecision",
                 "Hlt1TrackPhotonDecision",
                 
                 "Hlt1TrackMVADecision",
                 "Hlt1TwoTrackMVADecision",
                 "Hlt1TrackAllL0Decision",
                 "Hlt1B2HH_LTUNB_KPiDecision",
                 "Hlt1B2HH_LTUNB_KKDecision",
                 "Hlt1B2HH_LTUNB_PiPiDecision",
                 "Hlt1IncPhiDecision",
                 "Hlt1DiProtonDecision",
                 "Hlt1DiProtonLowMultDecision",
                 "Hlt1LowMultVeloCut_HadronsDecision",
                 "Hlt1LowMultPassThroughDecision",
                 
                 
                 # Hlt2 Topo
                 "Hlt2PhiIncPhiDecision",
                 "Hlt2PhiPromptPhi2EETurboDecision",
                 "Hlt2PhiBs2PhiPhiDecision",
                 "Hlt2B2HH_B2HHDecision",
                 "Hlt2CcDiHadronDiProtonDecision",
                 "Hlt2CcDiHadronDiProtonLowMultDecision",
                 "Hlt2CcDiHadronDiPhiDecision",
                 "Hlt2PhiIncPhiDecision",
                 "Hlt2PhiBs2PhiPhiDecision",
                 "Hlt2DiProtonDecision",
                 "Hlt2DiProtonLowMultDecision",

                 "Hlt2Topo2BodyDecision",
                 "Hlt2Topo3BodyDecision",
                 "Hlt2Topo4BodyDecision"
                 ]


from Configurables import LoKi__Hybrid__Tool as MyFactory
mf = MyFactory("HybridFactory")
mf.Modules.append ( 'LoKiPhysMC.decorators' )





from PhysConf.Selections import AutomaticData
Jpsi2ppLocation = "Phys/Ccbar2PpbarLine/Particles"
ppData = AutomaticData(Jpsi2ppLocation)



MyPreambulo = [
    'from LoKiPhysMC.decorators import *',
    'from LoKiPhysMC.functions import mcMatch' ,
    'from LoKiCore.functions import monitor'
]



from StandardParticles import StdAllNoPIDsProtons as ProtonsForCcbar2Ppbar
from GaudiConfUtils.ConfigurableGenerators import CombineParticles
from PhysConf.Selections import CombineSelection
Jpsi2ppbar = CombineSelection (
    "Jpsi2ppbar"  , ## name
    [ ProtonsForCcbar2Ppbar ] , ## input
    DecayDescriptor = "J/psi(1S) -> p+ p~-" ,
    Preambulo       = MyPreambulo  ,
    DaughtersCuts   = {
    'p+' : "mcMatch ( 'J/psi(1S) => ^p+  p~-' , 2 )",
    'p~-': "mcMatch ( 'J/psi(1S) =>  p+ ^p~-' , 2 )"
    } ,
    CombinationCut = 'AALL'                           ,
    MotherCut = "mcMatch('J/psi(1S) => p+  p~-' , 2 )"
    )

#from PhysConf.Selections import PrintSelection
#Jpsi2ppbar = PrintSelection ( Jpsi2ppbar )

#from PhysConf.Selections import TupleSelection
#PreselJpsi2pp = TupleSelection (
    #'PreselJpsi2pp'              ,  ## the name
    #[ Jpsi2ppbar ]  ,  ## inputs  ,
    #Decay    = "J/psi(1S) -> ^p+ ^p~-" ,
    #Branches = {
    #"ProtonP"     :  "  J/psi(1S) -> ^p+  p~- " ,
    #"ProtonM"     :  "  J/psi(1S) ->  p+ ^p~- " ,
    #"Jpsi"     :  "^(J/psi(1S) ->  p+  p~-)"
    #} , 
    #ToolList = [ "TupleToolKinematic", 
                 #"TupleToolPid" ,
                 #"TupleToolANNPID" ,
                 #"TupleToolTrackInfo" ,
                 #"TupleToolPrimaries" ,
                 #"TupleToolPropertime" ,
                 #"TupleToolEventInfo" ,
                 #"TupleToolTrackInfo" ,
                 #"TupleToolRecoStats",
                 #"TupleToolGeometry",
                 #"TupleToolMCBackgroundInfo"
                 ### "MCTupleToolPrimaries", 
                 ### "MCTupleToolInteractions"
                 #]
    #)

from PhysSelPython.Selections import SelectionSequence 
selseq1 = SelectionSequence('SeqJpsi', Jpsi2ppbar)
seq1 = selseq1.sequence()


#from PhysSelPython.Wrappers import Selection

#_PP = CombineParticles( DecayDescriptor = "J/psi(1S) -> p+ p~-",
                       #DaughtersCuts = {"p+" : "MCMATCH('J/psi(1S) ->^p+ p~-')",
                                        #"p~-": "MCMATCH('J/psi(1S) -> p+^p~-')"},
                       #CombinationCut = "AALL",
                       #MotherCut = "MCMATCH('J/psi(1S) -> p+ p~-')"
                       #)


#PreselJpsi2pp =  Selection("PreselJpsi2pp",
                 #Algorithm = _PP,
                 #RequiredSelections = [ ProtonsForCcbar2Ppbar ]
                 #)


#TUPLE SELECTION
#from PhysConf.Selections import StrippingSelection, TupleSelection
#strip_sel = StrippingSelection("Strip_sel",
#                               "HLT_PASS('StrippingCcbar2PpbarLineDecision')")
#PreselJpsi2pp = TupleSelection("PreselJpsi2pp",
#                               strip_sel,
#                               Decay = 'J/psi(1S) -> p+ p~-')




#from PhysConf.Selections import SelectionSequence
#selseq1    = SelectionSequence ( 'SEQ1' , PreselJpsi2pp )
#seq1 = selseq1.sequence()
#selseq2    = SelectionSequence ( 'SEQ2' , PreselJpsi2ppSecond )
#seq2 = selseq2.sequence()


#SeqPreselJpsi2pp.Members += [ seq1 ]
#SeqPreselJpsi2pp.Members += [ seq2 ]




Jpsi2ppBranches = {
    "ProtonP"  :  "J/psi(1S) ->^p+ p~-"
    ,"ProtonM"  :  "J/psi(1S) -> p+^p~-"
    ,"Jpsi"     : "(J/psi(1S) -> p+ p~-)"
}

Jpsi2ppTuple = DecayTreeTuple("Jpsi2ppTuple")
Jpsi2ppTuple.Decay ="J/psi(1S) -> ^p+ ^p~-"

Jpsi2ppTuple.Inputs = [ selseq1.outputLocation() ]
fillTuple( Jpsi2ppTuple, Jpsi2ppBranches, myTriggerList )

#SeqPreselJpsi2pp.Members += [ Jpsi2ppTuple ]


from Configurables import EventNodeKiller
eventNodeKiller = EventNodeKiller('DAQkiller')
eventNodeKiller.Nodes = ['/Event/DAQ',
                         '/Event/pRec']




"""
    Event-level filters
"""
from PhysConf.Filters import LoKi_Filters
Jpsi2ppFilters = LoKi_Filters (
                               STRIP_Code = "HLT_PASS('StrippingCcbar2PpbarLineDecision')"
                               )

from Configurables import DaVinci, CondDB
#DaVinci().EventPreFilters = Jpsi2ppFilters.filters ('Jpsi2ppFilters')
DaVinci().EvtMax = -1                          # Number of events
DaVinci().SkipEvents = 0                       # Events to skip
DaVinci().PrintFreq = 1000
DaVinci().DataType = "2015"
DaVinci().Simulation    = True
DaVinci().HistogramFile = "DVHistos.root"      # Histogram file
DaVinci().TupleFile = "Tuple.root"             # Ntuple
#DaVinci().UserAlgorithms = [eventNodeKiller,seq1,seq2,Jpsi2ppTuple]# The algorithms
DaVinci().UserAlgorithms = [seq1,Jpsi2ppTuple]# The algorithms
# MDST
DaVinci().InputType = "MDST"
#DaVinci().RootInTES = "/Event/Charm"
DaVinci().RootInTES = "/Event/AllStreams"

# Get Luminosity
DaVinci().Lumi = True


#DaVinci().DDDBtag   = "dddb-20150724"
#DaVinci().CondDBtag = "sim-20161124-2-vc-mu100"
#
#
#
#from Configurables import MessageSvc
#MessageSvc().setWarning = [ 'RFileCnv' ]
#
#
#from GaudiConf import IOHelper
## Use the local input data
#IOHelper().inputFiles([
#                       '00061881_00000190_7.AllStreams.dst'
#                       ], clear=True)
#DaVinci().Input = ['../../../data/00061896_00000009_6.AllStreams.mdst'
                   #]
