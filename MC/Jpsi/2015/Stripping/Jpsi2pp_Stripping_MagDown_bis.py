myJobName = 'MC_Jpsi2pp_R15aS24r1_MagDown_2015_bis'


#myApplication = prepareGaudiExec('DaVinci','v42r6p1', myPath='$HOME/cmtuser/')
myApplication = GaudiExec()
myApplication.directory = "$HOME/cmtuser/DaVinciDev_v42r6p1"
myApplication.options = ['DaVinci_Jpsi2pp_bis.py',
                         'DB_MC_R16S26.py']


data  = BKQuery('MC/2015/Beam6500GeV-2015-MagDown-Nu1.6-25ns-Pythia8/Sim09c/Trig0x411400a2/Reco15a/Turbo02/Stripping24r1NoPrescalingFlagged/24102002/ALLSTREAMS.DST', dqflag=['OK']).getDataset()

#validData = LHCbDataset(files= [file for file in data.files if file.getReplicas() ])
validData = LHCbDataset(files=['LFN:'+lfn for lfn, rep in data.getReplicas().iteritems() if len(rep)])

mySplitter = SplitByFiles( filesPerJob = 10, maxFiles = -1, ignoremissing = False, bulksubmit = False )

myBackend = Dirac()
j = Job (
         name         = myJobName,
         application  = myApplication,
         splitter     = mySplitter,
         outputfiles  = [ LocalFile('Tuple.root'),
                         LocalFile('DVHistos.root')
                         ],
         backend      = myBackend,
         inputdata    = validData,
         do_auto_resubmit = True,
         parallel_submit = True
         )
j.submit(keep_going=True, keep_on_fail=True)

