
import sys
import os

GaussVersion = 'v49r7'
GaussDir = "/afs/cern.ch/lhcb/software/releases/GAUSS/GAUSS_"+GaussVersion

OptnsDir = GaussDir+"/Sim/Gauss/options"
PythiDir = GaussDir+"/Gen/LbPythia8"
CurntDir = os.getcwd()

#if len(sys.argv) is not 2:
#    sys.exit("Please provide an event type file.")

myApplication = Gauss()
myApplication.version = GaussVersion
myApplication.platform='x86_64-slc6-gcc49-opt'
myApplication.user_release_area = "/afs/cern.ch/user/a/ausachov/cmtuser"
myApplication.optsfile = [
                          OptnsDir+"/Gauss-2016.py"
                          , CurntDir+"/24102004.py"
                          , PythiDir+"/options/Pythia8.py"
                          , OptnsDir+"/GenStandAlone.py"
                          , OptnsDir+"/Gauss-Job.py"
                          ]

#os.environ["DECFILESROOT"] = "/afs/cern.ch/user/a/ausachov/cmtuser/Gauss_v49r6/Gen/DecFiles"
j = Job()
j.application = myApplication
j.name = "GenLevel_24102004_Beam6500GeV-md100-2016-nu1.6_Pythia8"
j.splitter = GaussSplitter(numberOfJobs=1000,eventsPerJob=1000)
j.inputfiles = [LocalFile("/eos/user/a/ausachov/MC/pp/GenLevel/incl_Jpsi,pp=NoCut.dec")]
j.outputfiles = [LocalFile("*.xgen"),LocalFile("*.root"),LocalFile("*.xml")]
j.backend = Dirac()
j.parallel_submit = True
j.submit()
