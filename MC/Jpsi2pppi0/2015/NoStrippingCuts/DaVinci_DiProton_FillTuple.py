def fillTuple( tuple, myBranches, myTriggerList ):
    
    tuple.Branches = myBranches
    
    tuple.ToolList = [
                      "TupleToolAngles",
                      "TupleToolEventInfo",
                      "TupleToolGeometry",
                      "TupleToolKinematic",
                      "TupleToolPid",
                      "TupleToolPrimaries",
                      "TupleToolRecoStats",
                      "TupleToolTrackInfo",
                      "TupleToolANNPID",
                      "TupleToolPropertime"
                      ]
        

    # RecoStats for filling SpdMult, etc
    from Configurables import TupleToolRecoStats
    tuple.addTool(TupleToolRecoStats, name="TupleToolRecoStats")
    tuple.TupleToolRecoStats.Verbose=True

    from Configurables import TupleToolTISTOS, TupleToolDecay
    tuple.addTool(TupleToolDecay, name = 'Jpsi')


    # TISTOS for Jpsi
    tuple.Jpsi.ToolList+=[ "TupleToolTISTOS" ]
    tuple.Jpsi.addTool(TupleToolTISTOS, name="TupleToolTISTOS" )
    tuple.Jpsi.TupleToolTISTOS.Verbose=True
    tuple.Jpsi.TupleToolTISTOS.TriggerList = myTriggerList


    #LoKi one
    from Configurables import LoKi__Hybrid__TupleTool
    LoKi_Jpsi=LoKi__Hybrid__TupleTool("LoKi_Jpsi")
    LoKi_Jpsi.Variables = {
      "LOKI_FDCHI2"          : "BPVVDCHI2",
      "LOKI_FDS"             : "BPVDLS",
      "LOKI_DIRA"            : "BPVDIRA",
      "LOKI_BPVCORRM"        : "BPVCORRM",
      "DOCA"     : "DOCA(1,2)",
      "m_scaled" : "DTF_FUN ( M , False )",
      "m_pv"     : "DTF_FUN ( M , True )",
      "c2dtf_1"  : "DTF_CHI2NDOF( False )" ,
      "c2dtf_2"  : "DTF_CHI2NDOF( True  )"
     }

    tuple.addTool(TupleToolDecay, name="Jpsi")
    tuple.Jpsi.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_Jpsi"]
    tuple.Jpsi.addTool(LoKi_Jpsi)
    

    from Configurables import TupleToolMCBackgroundInfo, TupleToolMCTruth

    tuple.ToolList += ["TupleToolMCBackgroundInfo",
                       "TupleToolMCTruth"
                       ]

    MCTruth = TupleToolMCTruth()
    MCTruth.ToolList =  ["MCTupleToolAngles"
                         , "MCTupleToolHierarchy"
                         , "MCTupleToolKinematic"
                         , "MCTupleToolReconstructed"
                         , "MCTupleToolPID" 
                         , "MCTupleToolDecayType"
                         ##, "MCTupleToolEventType"
                         ##, "MCTupleToolInteractions"
                         ##, "MCTupleToolP2VV"
                         ##, "MCTupleToolPrimaries"
                         , "MCTupleToolPrompt"
                         ##, "MCTupleToolRedecay"
                         ]
    #tuple.addTool(MCTruth)


    from LoKiMC.functions import MCMOTHER, MCVFASPF, MCPRIMARY

    from Configurables import LoKi__Hybrid__MCTupleTool
    MCTruth.addTool( LoKi__Hybrid__MCTupleTool, name = "MCLoKiHybrid" )
    MCTruth.ToolList += [ "LoKi::Hybrid::MCTupleTool/MCLoKiHybrid" ]
    MCTruth.MCLoKiHybrid.Preambulo = [ "from LoKiCore.functions import switch" ]
    MCTruth.MCLoKiHybrid.Variables = {
        "TRUE_Tz" : "(3.3)*(MCMOTHER(MCVFASPF(MCVZ), 0)-MCVFASPF(MCVZ))*MCM/MCPZ",          
        "TRUEM"   : "MCM",
        "TRUEETA" : "MCETA",
        "TRUEPHI" : "MCPHI",
        "TRUETHETA" : "MCTHETA",
        "SISTERS_1" : "MCMOTHER(MCCHILD(MCID,1), -99999)",
        "SISTERS_2" : "MCMOTHER(MCCHILD(MCID,2), -99999)",
        "SISTERS_3" : "MCMOTHER(MCCHILD(MCID,3), -99999)",
        "SISTERS_4" : "MCMOTHER(MCCHILD(MCID,4), -99999)",
        "SISTERS_5" : "MCMOTHER(MCCHILD(MCID,5), -99999)",
        "MC_ISPRIMARY"      : "MCVFASPF(switch(MCPRIMARY,1,0))",
        "MC_MOTHER_M"       : "MCMOTHER(MCM,   -1)",
        "MC_MOTHER_TRUEP"   : "MCMOTHER(MCP,   -9999)",
        "MC_MOTHER_TRUEETA" : "MCMOTHER(MCETA, -9999)",
        "MC_MOTHER_TRUEPHI" : "MCMOTHER(MCPHI, -9999)",
        "MC_MOTHER_TRUEV_X" : "MCMOTHER(MCVFASPF(MCVX), -9999)",
        "MC_MOTHER_TRUEV_Y" : "MCMOTHER(MCVFASPF(MCVY), -9999)",
        "MC_MOTHER_TRUEV_Z" : "MCMOTHER(MCVFASPF(MCVZ), -9999)",
        "MC_MOTHER_ISPRIMARY"  : "MCMOTHER(MCVFASPF(switch(MCPRIMARY,1,0)),0)",
        "MC_GD_MOTHER_M"       : "MCMOTHER(MCMOTHER(MCM,   -1), -1)",
        "MC_GD_MOTHER_TRUEETA" : "MCMOTHER(MCMOTHER(MCETA, -9999), -9999)",
        "MC_GD_MOTHER_TRUEPHI" : "MCMOTHER(MCMOTHER(MCPHI, -9999), -9999)",
        "MC_GD_MOTHER_TRUEP"   : "MCMOTHER(MCMOTHER(MCP,   -9999), -9999)",
        "MC_GD_MOTHER_TRUEV_X" : "MCMOTHER(MCMOTHER(MCVFASPF(MCVX), -9999), -9999)",
        "MC_GD_MOTHER_TRUEV_Y" : "MCMOTHER(MCMOTHER(MCVFASPF(MCVY), -9999), -9999)",
        "MC_GD_MOTHER_TRUEV_Z" : "MCMOTHER(MCMOTHER(MCVFASPF(MCVZ), -9999), -9999)",
        "MC_GD_MOTHER_ISPRIMARY"  : "MCMOTHER(MCMOTHER(MCVFASPF(switch(MCPRIMARY,1,0)),0),0)",
        "MC_GD_GD_MOTHER_TRUEETA" : "MCMOTHER(MCMOTHER(MCMOTHER(MCETA, -9999), -9999), -9999)",
        "MC_GD_GD_MOTHER_TRUEPHI" : "MCMOTHER(MCMOTHER(MCMOTHER(MCPHI, -9999), -9999), -9999)",
        "MC_GD_GD_MOTHER_TRUEP"   : "MCMOTHER(MCMOTHER(MCMOTHER(MCP,   -9999), -9999), -9999)",
        "MC_GD_GD_MOTHER_TRUEV_X" : "MCMOTHER(MCMOTHER(MCMOTHER(MCVFASPF(MCVX), -9999), -9999), -9999)",
        "MC_GD_GD_MOTHER_TRUEV_Y" : "MCMOTHER(MCMOTHER(MCMOTHER(MCVFASPF(MCVY), -9999), -9999), -9999)",
        "MC_GD_GD_MOTHER_TRUEV_Z" : "MCMOTHER(MCMOTHER(MCMOTHER(MCVFASPF(MCVZ), -9999), -9999), -9999)",
        "MC_GD_GD_MOTHER_ISPRIMARY"  : "MCMOTHER(MCMOTHER(MCMOTHER(MCVFASPF(switch(MCPRIMARY,1,0)),0),0),0)",
        "MC_GD_GD_GD_MOTHER_TRUEETA" : "MCMOTHER(MCMOTHER(MCMOTHER(MCMOTHER(MCETA, -9999), -9999), -9999), -9999)",
        "MC_GD_GD_GD_MOTHER_TRUEPHI" : "MCMOTHER(MCMOTHER(MCMOTHER(MCMOTHER(MCPHI, -9999), -9999), -9999), -9999)",
        "MC_GD_GD_GD_MOTHER_TRUEP"   : "MCMOTHER(MCMOTHER(MCMOTHER(MCMOTHER(MCP,   -9999), -9999), -9999), -9999)",
        "MC_GD_GD_GD_MOTHER_TRUEV_X" : "MCMOTHER(MCMOTHER(MCMOTHER(MCMOTHER(MCVFASPF(MCVX), -9999), -9999), -9999), -9999)",
        "MC_GD_GD_GD_MOTHER_TRUEV_Y" : "MCMOTHER(MCMOTHER(MCMOTHER(MCMOTHER(MCVFASPF(MCVY), -9999), -9999), -9999), -9999)",
        "MC_GD_GD_GD_MOTHER_TRUEV_Z" : "MCMOTHER(MCMOTHER(MCMOTHER(MCMOTHER(MCVFASPF(MCVZ), -9999), -9999), -9999), -9999)",
        "MC_GD_GD_GD_MOTHER_ISPRIMARY"  : "MCMOTHER(MCMOTHER(MCMOTHER(MCMOTHER(MCVFASPF(switch(MCPRIMARY,1,0)),0),0),0),0)",
        }
    tuple.addTool(MCTruth)


    
    #LoKi one
    from Configurables import LoKi__Hybrid__TupleTool
    LoKi_All=LoKi__Hybrid__TupleTool("LoKi_All")
    LoKi_All.Variables = {
        "ETA"                  : "ETA",
        "Y"                    : "Y"  ,
        "LOKI_IPCHI2"          : "BPVIPCHI2()"
    }
    tuple.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_All"]
    tuple.addTool(LoKi_All)

    MyPreambulo = [
        'from LoKiPhysMC.decorators import *',
        'from LoKiPhysMC.functions import mcMatch' ,
        'from LoKiCore.functions import monitor'
    ]



    from StandardParticles import StdAllNoPIDsProtons as ProtonsForCcbar2Ppbar
    from GaudiConfUtils.ConfigurableGenerators import CombineParticles
    from PhysConf.Selections import CombineSelection
    Jpsi2ppbar = CombineSelection (
        "Jpsi2ppbar"  , ## name
        [ ProtonsForCcbar2Ppbar ] , ## input
        DecayDescriptor = "J/psi(1S) -> p+ p~-" ,
        Preambulo       = MyPreambulo  ,
        DaughtersCuts   = {
        'p+' : "mcMatch ( 'J/psi(1S) => ^p+  p~-' , 2 )",
        'p~-': "mcMatch ( 'J/psi(1S) =>  p+ ^p~-' , 2 )"
        } ,
        CombinationCut = 'AALL'                           ,
        MotherCut = "mcMatch('J/psi(1S) => p+  p~-' , 2 )"
        )


    from PhysSelPython.Selections import SelectionSequence 
    selseq1 = SelectionSequence('SeqJpsi', Jpsi2ppbar)
    seq1 = selseq1.sequence()
    
    from Configurables import DaVinci, CondDB
    
    DaVinci().UserAlgorithms += [seq1]



