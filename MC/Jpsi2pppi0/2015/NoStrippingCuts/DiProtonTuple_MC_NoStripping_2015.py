from ROOT import gROOT, TChain, TCut, TFile, TTree, TProof

chain = TChain("Jpsi2ppTuple/DecayTree")
sjMax=11
for i in range(sjMax):
    chain.Add("/afs/cern.ch/work/v/vazhovko/gangadir/workspace/vazhovko/LocalXML/103/"+str(i)+"/output/Tuple.root")

sjMax=53
for i in range(sjMax):
    chain.Add("/afs/cern.ch/work/v/vazhovko/gangadir/workspace/vazhovko/LocalXML/104/"+str(i)+"/output/Tuple.root")


cutProtonP = TCut("ProtonP_P>12.5e3 && ProtonM_P>12.5e3")
cutProtonPt = TCut("ProtonP_PT>2000 && ProtonM_PT>2000")
cutProtonPt2P = TCut("ProtonP_PT/ProtonP_P>0.0366 && ProtonM_PT/ProtonM_P>0.0366")
cutID = TCut("ProtonP_PIDp>20 && ProtonM_PIDp>20")
cutID += TCut("(ProtonP_PIDp-ProtonP_PIDK)>15 && (ProtonM_PIDp-ProtonM_PIDK)>15")
cutProtonTrack = TCut("ProtonP_TRACK_CHI2NDOF<4 && ProtonM_TRACK_CHI2NDOF<4")
cutJpsiVx = TCut("Jpsi_ENDVERTEX_CHI2<9")
cutJpsiY = TCut("Jpsi_Y>2 && Jpsi_Y<4.5")
cutJpsiPt = TCut("Jpsi_PT>6000")
cutJpsiDOCA = TCut("Jpsi_DOCA<0.1")
cutProtonClone = TCut("ProtonP_TRACK_CloneDist<0 && ProtonM_TRACK_CloneDist<0")
cutProtonGhost = TCut("ProtonP_TRACK_GhostProb<0.2 && ProtonP_TRACK_GhostProb<0.2")
cutSpdMult = TCut("nSPDHits < 300")
cutL0 = TCut("(Jpsi_L0HadronDecision_TOS) && Jpsi_Hlt1DiProtonDecision_TOS && Jpsi_Hlt2DiProtonDecision_TOS")




totCut = TCut(cutProtonP +
             cutProtonPt +
             cutProtonPt2P +
             cutID +
             cutProtonTrack +
             cutJpsiVx +
             cutJpsiY +
             cutJpsiPt +
             cutJpsiDOCA +
             cutProtonClone +
             cutProtonGhost +
             cutSpdMult +
             cutL0)

#totCut = TCut("")

newfile = TFile("JpsiDiProtonPi0_MC_NoStripping_2015.root","recreate")

newtree=TTree()
#newtree.SetMaxTreeSize(500000000)
newtree = chain.CopyTree(totCut.GetTitle())

newtree.Print()

newtree.GetCurrentFile().Write() 
newtree.GetCurrentFile().Close()
