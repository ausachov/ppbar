from Gaudi.Configuration import *
from LHCbKernel.Configuration import *
from DaVinci.Configuration import *
from DecayTreeTuple.Configuration import *
from GaudiConf import IOHelper
from Configurables import DaVinci, DecayTreeTuple, SubstitutePID
from PhysSelPython.Wrappers import Selection, SelectionSequence, DataOnDemand

from Configurables import GaudiSequencer, DataOnDemandSvc,CondDB
from Configurables import DstConf, CaloDstUnPackConf, LoKi__Hybrid__TupleTool
from Configurables import EventNodeKiller, ProcStatusCheck
from Configurables import StrippingReport, TimingAuditor, SequencerTimerTool

from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStreams
from StrippingArchive import strippingArchive


##### MC DECAYTREETUPLE ####
def addNtupleToDaVinci(name, isSim, isDST):

  if isSim:
    from Configurables import MCDecayTreeTuple
    MCTuple = MCDecayTreeTuple("MCDecayTreeTuple")

     
    MCTuple.Decay = "J/psi(1S) ==> ^p+ ^p~-"
    MCTuple.Branches = {
        "ProtonP"  :  "J/psi(1S) ==>^p+ p~-"
       ,"ProtonM"  :  "J/psi(1S) ==> p+^p~-"
       ,"Jpsi"     : "(J/psi(1S) ==> p+ p~-)"
    }


    from Configurables import TupleToolMCBackgroundInfo, TupleToolMCTruth, TupleToolGeneration


    MCTuple.ToolList += [ "MCTupleToolHierarchy" ]
    MCTuple.ToolList += [ "MCTupleToolKinematic" ]
   # MCTuple.ToolList += [ "MCTupleToolGeometry" ]
    MCTuple.ToolList += [ "MCTupleToolPID" ]
    MCTuple.ToolList += ["MCTupleToolAngles"
                         #, "MCTupleToolReconstructed"
                         , "MCTupleToolDecayType"
                         , "MCTupleToolEventType"
                         #, "MCTupleToolInteractions"
                         #, "MCTupleToolP2VV"
                         , "MCTupleToolPrimaries"
                         , "MCTupleToolPrompt"
                         , "MCTupleToolRedecay"
                         ]



    #MCTuple.Inputs = ['/Event/Leptonic.Strip/Phys/'+line+'/Particles']
    ######################################################################
    #from Configurables import TupleToolDecay
    #MCTuple.addTool( TupleToolDecay, name = "Lb" )
    ######################################################################
    #MCTuple.Lb.ToolList += [ "MCTupleToolP2VV" ]
    ############
  ######################################################################

    from LoKiMC.functions import MCMOTHER, MCVFASPF
    
    from Configurables import LoKi__Hybrid__MCTupleTool
    MCTuple.addTool( LoKi__Hybrid__MCTupleTool, name = "MCLoKiHybrid" )
    MCTuple.ToolList += [ "LoKi::Hybrid::MCTupleTool/MCLoKiHybrid" ]
    MCTuple.MCLoKiHybrid.Preambulo = [ "from LoKiCore.functions import switch" ]
    MCTuple.MCLoKiHybrid.Variables = {
        "TRUEM"   : "MCM",
        "TRUEETA" : "MCETA",
        "TRUEPHI" : "MCPHI",
        "TRUETHETA" : "MCTHETA",
        "SISTERS_1" : "MCMOTHER(MCCHILD(MCID,1), -99999)",
        "SISTERS_2" : "MCMOTHER(MCCHILD(MCID,2), -99999)",
        "SISTERS_3" : "MCMOTHER(MCCHILD(MCID,3), -99999)",
        "SISTERS_4" : "MCMOTHER(MCCHILD(MCID,4), -99999)",
        "SISTERS_5" : "MCMOTHER(MCCHILD(MCID,5), -99999)",
        "MC_ISPRIMARY"      : "MCVFASPF(switch(MCPRIMARY,1,0))",
        #"MC_ISSIGNAL"       : "MCSSWITCH(MCVFASPF(MCPRIMARY),1,0)",
        "MC_MOTHER_M"       : "MCMOTHER(MCM,   -1)",
        "MC_MOTHER_TRUEP"   : "MCMOTHER(MCP,   -9999)",
        "MC_MOTHER_TRUEETA" : "MCMOTHER(MCETA, -9999)",
        "MC_MOTHER_TRUEPHI" : "MCMOTHER(MCPHI, -9999)",
        "MC_MOTHER_TRUEV_X" : "MCMOTHER(MCVFASPF(MCVX), -9999)",
        "MC_MOTHER_TRUEV_Y" : "MCMOTHER(MCVFASPF(MCVY), -9999)",
        "MC_MOTHER_TRUEV_Z" : "MCMOTHER(MCVFASPF(MCVZ), -9999)",
        "MC_MOTHER_ISPRIMARY"  : "MCMOTHER(MCVFASPF(switch(MCPRIMARY,1,0)),0)",
#        "MC_MOTHER_M"       : "MCMOTHER(MCID,   -9999)",
        #"MC_MOTHER_ISPRIMARY"  : "MCMOTHER(MCSWITCH(MCVFASPF(MCPRIMARY),1,0), 0)",
        "MC_GD_MOTHER_M"       : "MCMOTHER(MCMOTHER(MCM,   -1), -1)",
        "MC_GD_MOTHER_TRUEETA" : "MCMOTHER(MCMOTHER(MCETA, -9999), -9999)",
        "MC_GD_MOTHER_TRUEPHI" : "MCMOTHER(MCMOTHER(MCPHI, -9999), -9999)",
        "MC_GD_MOTHER_TRUEP"   : "MCMOTHER(MCMOTHER(MCP,   -9999), -9999)",
        "MC_GD_MOTHER_TRUEV_X" : "MCMOTHER(MCMOTHER(MCVFASPF(MCVX), -9999), -9999)",
        "MC_GD_MOTHER_TRUEV_Y" : "MCMOTHER(MCMOTHER(MCVFASPF(MCVY), -9999), -9999)",
        "MC_GD_MOTHER_TRUEV_Z" : "MCMOTHER(MCMOTHER(MCVFASPF(MCVZ), -9999), -9999)",
        "MC_GD_MOTHER_ISPRIMARY"  : "MCMOTHER(MCMOTHER(MCVFASPF(switch(MCPRIMARY,1,0)),0),0)",
#        "MC_GD_MOTHER_ID"      : "MCMOTHER(MCMOTHER(MCID, -9999), -9999)",
        "MC_GD_GD_MOTHER_TRUEETA" : "MCMOTHER(MCMOTHER(MCMOTHER(MCETA, -9999), -9999), -9999)",
        "MC_GD_GD_MOTHER_TRUEPHI" : "MCMOTHER(MCMOTHER(MCMOTHER(MCPHI, -9999), -9999), -9999)",
        "MC_GD_GD_MOTHER_TRUEP"   : "MCMOTHER(MCMOTHER(MCMOTHER(MCP,   -9999), -9999), -9999)",
        "MC_GD_GD_MOTHER_TRUEV_X" : "MCMOTHER(MCMOTHER(MCMOTHER(MCVFASPF(MCVX), -9999), -9999), -9999)",
        "MC_GD_GD_MOTHER_TRUEV_Y" : "MCMOTHER(MCMOTHER(MCMOTHER(MCVFASPF(MCVY), -9999), -9999), -9999)",
        "MC_GD_GD_MOTHER_TRUEV_Z" : "MCMOTHER(MCMOTHER(MCMOTHER(MCVFASPF(MCVZ), -9999), -9999), -9999)",
        "MC_GD_GD_MOTHER_ISPRIMARY"  : "MCMOTHER(MCMOTHER(MCMOTHER(MCVFASPF(switch(MCPRIMARY,1,0)),0),0),0)",
        "MC_GD_GD_GD_MOTHER_TRUEETA" : "MCMOTHER(MCMOTHER(MCMOTHER(MCMOTHER(MCETA, -9999), -9999), -9999), -9999)",
        "MC_GD_GD_GD_MOTHER_TRUEPHI" : "MCMOTHER(MCMOTHER(MCMOTHER(MCMOTHER(MCPHI, -9999), -9999), -9999), -9999)",
        "MC_GD_GD_GD_MOTHER_TRUEP"   : "MCMOTHER(MCMOTHER(MCMOTHER(MCMOTHER(MCP,   -9999), -9999), -9999), -9999)",
        "MC_GD_GD_GD_MOTHER_TRUEV_X" : "MCMOTHER(MCMOTHER(MCMOTHER(MCMOTHER(MCVFASPF(MCVX), -9999), -9999), -9999), -9999)",
        "MC_GD_GD_GD_MOTHER_TRUEV_Y" : "MCMOTHER(MCMOTHER(MCMOTHER(MCMOTHER(MCVFASPF(MCVY), -9999), -9999), -9999), -9999)",
        "MC_GD_GD_GD_MOTHER_TRUEV_Z" : "MCMOTHER(MCMOTHER(MCMOTHER(MCMOTHER(MCVFASPF(MCVZ), -9999), -9999), -9999), -9999)",
        "MC_GD_GD_GD_MOTHER_ISPRIMARY"  : "MCMOTHER(MCMOTHER(MCMOTHER(MCMOTHER(MCVFASPF(switch(MCPRIMARY,1,0)),0),0),0),0)",
        }
    ######################################################################
    from Configurables import LoKi__Hybrid__EvtTupleTool
    MCTuple.addTool( LoKi__Hybrid__EvtTupleTool, name = "ELoKiHybrid" )
    MCTuple.ToolList += [ "LoKi::Hybrid::EvtTupleTool/ELoKiHybrid" ]
    MCTuple.ELoKiHybrid.Preambulo = [ "from LoKiCore.basic import LHCb" ]
    MCTuple.ELoKiHybrid.VOID_Variables = {
        "nSPDHits" : "RECSUMMARY( LHCb.RecSummary.nSPDhits, -1, '/Event/Rec/Summary', False )"
    }
  

    DaVinci().UserAlgorithms += [MCTuple]


def getDBInfo(sim, magnet, lyr):
  if sim:
    if lyr == "2011" or lyr == "2012":
      TagDDDB = "dddb-20150928"
      TagCondDB = "sim-20160321"#-2-vc-md100
      #TagDDDB   = "dddb-20150522"# New version for calo reconstruction
      #TagCondDB = "sim-20150522"    
    if lyr == "2011":
      TagDDDB   += "-1"
      TagCondDB += "-1"
      TagCondDB += "-vc"
    if lyr == "2012":
      #TagDDDB   += "-2"
      TagCondDB += "-2"
      TagCondDB += "-vc"

    if lyr == "2015":
      TagDDDB   = "dddb-20150724"
      TagCondDB = "sim-20161124-vc"
    if lyr == "2016":
      TagDDDB   = "dddb-20150724"
      TagCondDB = "sim-20161124-2-vc"
      
    if magnet == "MD":
      TagCondDB += "-md100"
    else:
      TagCondDB += "-mu100"
  else: #for data - default tags are the best
    if lyr == "2011": 
      TagDDDB  = ""#"dddb-20150522-1"
      TagCondDB= ""#"cond-20150409"
    elif lyr == "2012" : 
      TagDDDB  = ""#"dddb-20150928"
      TagCondDB= ""#"cond-20150409-1"
    elif lyr == "2015" :
      TagDDDB  = ""#"dddb-20150724"
      TagCondDB= ""#"cond-20151016"
    elif lyr == "2016" : 
      TagDDDB  = ""#"dddb-20150724"
      TagCondDB= ""#"cond-20160517"
  return TagDDDB, TagCondDB

##############################################################
#        You should make changes in the lines below          #   
##############################################################

# 1) SELECT THE YEAR #########################################
#Year = "2011"
#Year = "2012"
Year = "2015"
#Year = "2016"

# 2) SELECT MAGNET POLARITY ###################################
MagnetPolarity= "MU"
#MagnetPolarity= "MD"

# 3) SELECT DATA OR MC ########################################
DaVinci().Simulation = True
#DaVinci().Simulation = False

if DaVinci().Simulation: #if MC
  
  # 3a) IF YOUR MC IS DST BUT NOT DST PLEASE CHANGE THIS ####################
  #DaVinci().InputType = "DST"
  DaVinci().InputType = "DST" #==> please select this line if running on MC which is DST
  #calo rerunning for Run1 MC
  if Year == "2011" or Year == "2012":
    importOptions("$APPCONFIGOPTS/DaVinci/DV-RedoCaloPID-Stripping21.py")
  DaVinci().Lumi = False
else: #if data
  DaVinci().InputType = "DST"
  DaVinci().Lumi = True

#gets DB tags as described above
tDDDB, tConDB = getDBInfo(DaVinci().Simulation, MagnetPolarity, Year)
DaVinci().DDDBtag   = tDDDB
DaVinci().CondDBtag = tConDB

DaVinci().RootInTES = '/Event/Charm'

# 4) PICK HERE IF YOU RUN ELECTRONS OR MUONS ################################
#addNtupleToDaVinci("tuple_eeLine2", DaVinci().Simulation, DaVinci().InputType)
addNtupleToDaVinci("tuple_Jpsi", DaVinci().Simulation, DaVinci().InputType)

# for 2016 MC
DaVinci().DataType = Year
# for 2015 MC
#DaVinci().DataType = "2016"

DaVinci().EvtMax = -1
DaVinci().PrintFreq = 1000
DaVinci().TupleFile = "Tuple.root"
MessageSvc().Format = "% F%60W%S%7W%R%T %0W%M"

from PhysConf.Filters import LoKi_Filters
fltrs = LoKi_Filters (
    VOID_Code = """
    EXISTS( '/Event/Charm/pMC/Particles' )
    """
    )
#DaVinci().EventPreFilters = fltrs.filters('MC')

#MessageSvc().OutputLevel = VERBOSE #to debug



#DaVinci().Input = ['~/private/MC/data/00052391_00000003_3.AllStreams.DST'
                   #]

#from GaudiConf import IOHelper
## Use the local input data
#IOHelper().inputFiles([
#                      './00072116_00000002_6.AllStreams.dst'
#                      ], clear=True)


