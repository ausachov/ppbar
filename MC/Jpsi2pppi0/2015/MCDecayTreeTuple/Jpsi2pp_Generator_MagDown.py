myJobName = 'MC_Jpsi2pppi0_R15aS24_Generator_MagDown_2015'

myApplication = GaudiExec()
myApplication.directory = "$HOME/cmtuser/DaVinciDev_v42r6p1"
myApplication.options = ['MCDecayTreeTuple.py']


data  = BKQuery('MC/2015/Beam6500GeV-2015-MagDown-Nu1.6-25ns-Pythia8/Sim09b/Trig0x411400a2/Reco15a/Turbo02/Stripping24NoPrescalingFlagged/24102402/ALLSTREAMS.MDST', dqflag=['OK']).getDataset()

#validData = LHCbDataset(files= [file for file in data.files if file.getReplicas() ])
validData = LHCbDataset(files=['LFN:'+lfn for lfn, rep in data.getReplicas().iteritems() if len(rep)])

mySplitter = SplitByFiles( filesPerJob = 1, maxFiles = -1, ignoremissing = False, bulksubmit = False )

myBackend = Dirac()
j = Job (
         name         = myJobName,
         application  = myApplication,
         splitter     = mySplitter,
         outputfiles  = [ LocalFile('Tuple.root'),
                         LocalFile('DVHistos.root')
                         ],
         backend      = myBackend,
         inputdata    = validData,
         do_auto_resubmit = True,
         parallel_submit = True
         )
j.submit(keep_going=True, keep_on_fail=True)

