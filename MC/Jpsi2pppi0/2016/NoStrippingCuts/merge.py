from ROOT import *

f = TFile('DVntuple_JpsiDiProton_NoStrippingCuts_2016.root', 'recreate')
chain = TChain("Jpsi2ppTuple/DecayTree")

for i in range(24):
    chainName = '/afs/cern.ch/work/v/vazhovko/gangadir/workspace/vazhovko/LocalXML/4/%s/output/Tuple.root'%i
    chain.Add(chainName)

for i in range(26):
    chainName = '/afs/cern.ch/work/v/vazhovko/gangadir/workspace/vazhovko/LocalXML/5/%s/output/Tuple.root'%i
    chain.Add(chainName)

tree = TTree()
tree = chain.CopyTree("")
tree.Write()
f.Close()