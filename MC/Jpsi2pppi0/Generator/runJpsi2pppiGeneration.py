
import sys
import os

GaussVersion = 'v49r6'
GaussDir = "/afs/cern.ch/lhcb/software/releases/GAUSS/GAUSS_"+GaussVersion

OptnsDir = "/afs/cern.ch/lhcb/software/releases/GAUSS/GAUSS_v49r7/Sim/Gauss/options"
PythiDir = "/afs/cern.ch/lhcb/software/releases/GAUSS/GAUSS_v49r7/Gen/LbPythia8"
#CurntDir = os.getcwd()

#if len(sys.argv) is not 2:
#    sys.exit("Please provide an event type file.")

myApplication = GaudiExec()
myApplication.directory = "/afs/cern.ch/user/a/ausachov/cmtuser/GaussDev_v49r7"
#myApplication.platform='x86_64-slc6-gcc49-opt'
#myApplication.user_release_area = "/afs/cern.ch/user/a/ausachov/cmtuser"
myApplication.options = [
                          OptnsDir+"/Gauss-2016.py"
                          , "/eos/user/a/ausachov/MC/pp/GenLevel/24102403.py"
                          , PythiDir+"/options/Pythia8.py"
                          , OptnsDir+"/GenStandAlone.py"
                          , OptnsDir+"/Gauss-Job.py"
                          ]

#os.environ["DECFILESROOT"] = "/afs/cern.ch/user/a/ausachov/cmtuser/Gauss_v49r6/Gen/DecFiles"
j = Job()
j.application = myApplication
j.name = "GenLevel_24102403_Beam6500GeV-md100-2016-nu1.6_Pythia8"
j.splitter = GaussSplitter(numberOfJobs=1000,eventsPerJob=1000)
j.inputfiles = [LocalFile("/eos/user/a/ausachov/MC/pp/GenLevel/incl_Jpsi,pppi0=NoCut.dec")]
j.outputfiles = [DiracFile("*.xgen"),DiracFile("*.root"),DiracFile("*.xml")]
j.backend = Dirac()
j.parallel_submit = True
j.submit()
