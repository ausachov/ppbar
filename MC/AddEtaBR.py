from ROOT import *
from array import *

chain  = TChain("MCDecayTreeTuple/MCDecayTree")


chain.Add("../MC/Etac/Generator/Etac-24102012-Total.root")
newfile = TFile("../MC/Etac/Generator/Etac_GenNoCut.root","recreate")

#chain.Add("../MC/Jpsi/Generator/Jpsi-24102004-Total.root")
#newfile = TFile("../MC/Jpsi/Generator/Jpsi_GenNoCut.root","recreate")

newtree = chain.CopyTree("")

Jpsi_PX    = array('d',[0])
Jpsi_PY    = array('d',[0])
Jpsi_PZ    = array('d',[0])
Jpsi_PE    = array('d',[0])
Jpsi_ETA   = array('d',[0])
ProtonP_TRUECosTheta  = array('d',[0])
ProtonM_TRUECosTheta  = array('d',[0])
ProtonP_CosTheta  = array('d',[0])
ProtonM_CosTheta  = array('d',[0])
    
newtree.SetBranchAddress("Jpsi_TRUEP_X",Jpsi_PX)
newtree.SetBranchAddress("Jpsi_TRUEP_Y",Jpsi_PY)
newtree.SetBranchAddress("Jpsi_TRUEP_Z",Jpsi_PZ)
newtree.SetBranchAddress("Jpsi_TRUEP_E",Jpsi_PE)
newtree.SetBranchAddress("ProtonP_TRUECosTheta",ProtonP_TRUECosTheta)
newtree.SetBranchAddress("ProtonM_TRUECosTheta",ProtonM_TRUECosTheta)



Eta_Branch = newtree.Branch("Jpsi_ETA", Jpsi_ETA, "Jpsi_ETA/D")
ProtonP_CosTheta_Branch = newtree.Branch("ProtonP_CosTheta",ProtonP_CosTheta,"ProtonP_CosTheta/D")
ProtonM_CosTheta_Branch = newtree.Branch("ProtonM_CosTheta",ProtonM_CosTheta,"ProtonM_CosTheta/D")

nEn = newtree.GetEntries()
for i in range(nEn):
    newtree.GetEntry(i)
    vect = TLorentzVector(Jpsi_PX[0],Jpsi_PY[0],Jpsi_PZ[0],Jpsi_PE[0])
    Jpsi_ETA[0] = vect.PseudoRapidity()
    ProtonP_CosTheta[0] = ProtonP_TRUECosTheta[0]
    ProtonM_CosTheta[0] = ProtonM_TRUECosTheta[0]
    Eta_Branch.Fill()
    ProtonP_CosTheta_Branch.Fill()
    ProtonM_CosTheta_Branch.Fill()

newtree.Print()
newfile.Write()
newfile.Close()
