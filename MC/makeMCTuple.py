import sys
print sys.argv

import argparse

#parser = argparse.ArgumentParser(description="make MC tuples for ppbar analysis")
#parser.add_argument('year', choices=['2015','2015bis','2016','2016bis'],
#                    help='Year of data-taking to run over')
#parser.add_argument('polarity', choices=['Up', 'Down'],
#                    help='Polarity of data-taking to run over')
#parser.add_argument('evtType', choices=['Jpsi', 'Etac', 'Jpsi2pppi0','incl_b'],
#                    help='event types to run over')
#parser.add_argument('--test', action='store_true',
#                    help='Run over one file locally')
#args = parser.parse_args()

#year      = args.year
#polarity  = args.polarity
#evtType   = args.evtType
#test      = args.test



#year     = '2016bis'
#polarity = 'Down'
#evtType  = 'Jpsi' 



def makeSingleTuple(year,polarity,evtType):

   myJobName = evtType+'_Mag'+polarity+'_'+year

   myApplication = GaudiExec()
   myApplication.directory = "$HOME/cmtuser/DaVinci_v42r6p1_NoPID4ppbar/DaVinciDev_v42r6p1"


   evtTypeNumbers = {
    "Etac"       : "24102010",
    "Jpsi"       : "24102002",
    "incl_b"     : "28102061",
    "Jpsi2pppi0" : "24102402"
   }

   evtTypeNumber = evtTypeNumbers[evtType]
   dataType = 'DST'

   if evtType=='incl_b':
      dataType = 'MDST'



   if year=='2015':
      reco       = 'Reco15a'
      TCK        = 'Trig0x411400a2'
      stripping  = 'Turbo02/Stripping24NoPrescalingFlagged'
      simVersion = 'Sim09b'
   elif year=='2015bis':
      reco       = 'Reco15a'
      TCK        = 'Trig0x411400a2'
      stripping  = 'Turbo02/Stripping24r1NoPrescalingFlagged'
      simVersion = 'Sim09c'
      year       = '2015'
   elif year=='2016':
      reco       = 'Reco16'
      TCK        = 'Trig0x6138160F'
      stripping  = 'Turbo03/Stripping26NoPrescalingFlagged'

      if evtType=='incl_b':
       	 stripping  = 'Turbo03/Stripping28NoPrescalingFlagged'

      simVersion = 'Sim09b'
   elif year=='2016bis':
      reco       = 'Reco16'
      TCK        = 'Trig0x6138160F'
      stripping  = 'Turbo03/Stripping28r1NoPrescalingFlagged'
      simVersion = 'Sim09c'
      year       = '2016'

   if evtType == 'incl_b':
      myApplication.options = ['DaVinci_Etac2pp_Detached_'+year+'.py']
   else:
      myApplication.options = ['DaVinci_Jpsi2pp_Detached_'+year+'.py']



   data  = BKQuery('/MC/'                + year          + \
                '/Beam6500GeV-'          + year          + '-Mag' + polarity + \
                '-Nu1.6-25ns-Pythia8/'   + simVersion    + \
                '/'+ TCK +'/' + reco +'/'+ stripping     +'/' + \
                                           evtTypeNumber + \
                '/ALLSTREAMS.' + dataType, dqflag=['OK']).getDataset()


   validData = LHCbDataset(files=['LFN:'+lfn for lfn, rep in data.getReplicas().iteritems() if len(rep)])

   mySplitter = SplitByFiles( filesPerJob = 10, maxFiles = -1, ignoremissing = False, bulksubmit = False )

   myBackend = Dirac()
   j = Job (
         name         = myJobName,
         application  = myApplication,
         splitter     = mySplitter,
         outputfiles  = [ LocalFile('Tuple.root'),
                          LocalFile('DVHistos.root')],
         backend      = myBackend,
         inputdata    = validData,
         do_auto_resubmit = True,
         parallel_submit = True
         )
   j.submit()
   print '\n\n\n'



#years      = ['2015bis','2016','2016bis']
years      = ['2016']
polarities = ['Down','Up']
#evtTypes   = ['Jpsi','Etac','Jpsi2pppi0'] #'Jpsi','Etac','incl_b','Jpsi2pppi0']
evtTypes = ['incl_b']

for evtType in evtTypes:
   for year in years:
      for polarity in polarities:
         print 'MC', evtType, year, polarity, ' submitting below! \n'
         makeSingleTuple(year=year,polarity=polarity,evtType=evtType)
