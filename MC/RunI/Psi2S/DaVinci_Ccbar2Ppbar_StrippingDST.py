from Gaudi.Configuration import *
from Configurables import SelDSTWriter, DaVinci
from StrippingConf.Configuration import StrippingConf
from StrippingConf.StrippingStream import StrippingStream

MessageSvc().Format = "% F%60W%S%7W%R%T %0W%M"

#
# Stripping job configuration
#

from StrippingSelections.StrippingCcbar2Ppbar import StrippingCcbar2PpbarConf

stream = StrippingStream("DiProton")
stream.appendLines( [ StrippingCcbar2PpbarConf().Nominal_Line() ] )
stream.appendLines( [ StrippingCcbar2PpbarConf().Loose_Line() ] )

sc = StrippingConf( Streams = [ stream ] )

dstWriter = SelDSTWriter("MyDSTWriter",
                         SelectionSequences = sc.activeStreams(),
                         OutputPrefix = 'Strip',
                         OutputFileSuffix = 'MC2010'
                         )

from StrippingSelections.StartupOptions import veloNZSKiller, redoPV
dvinit = GaudiSequencer("DaVinciInitSeq")
dvinit.Members.insert(0, redoPV() )
dvinit.Members.insert(0, veloNZSKiller() )

DaVinci().EvtMax = -1          # Number of events
DaVinci().PrintFreq = 1000
DaVinci().DataType = "2010"
DaVinci().Simulation = True
DaVinci().UserAlgorithms = [ dstWriter.sequence() ]

"""
from Configurables import CondDB
CondDB(UseOracle=True)

DaVinci().DDDBtag   = "head-20100407"
DaVinci().CondDBtag = "sim-20100412-vc-md100"
importOptions("$APPCONFIGOPTS/DisableLFC.py")
"""


