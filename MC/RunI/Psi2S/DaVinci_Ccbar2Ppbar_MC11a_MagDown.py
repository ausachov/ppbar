def fillTuple( tuple, branches, myTriggerList ):
    
    from Configurables import DecayTreeTuple, TupleToolTrigger, TupleToolTISTOS, TupleToolDecay, TupleToolRecoStats, TupleToolEventInfo
    
    tuple.Branches = branches
    
    # Trigger
    tuple.addTool(TupleToolTrigger, name="TupleToolTrigger" )
    tuple.TupleToolTrigger.Verbose=True
    tuple.TupleToolTrigger.TriggerList  = myTriggerList
 
    # TISTOS
    tuple.addTool(TupleToolTISTOS, name="TupleToolTISTOS" )
    tuple.TupleToolTISTOS.Verbose=True
    tuple.TupleToolTISTOS.TriggerList = myTriggerList
    
    tuple.ToolList = [
        "TupleToolAngles",
        "TupleToolEventInfo",
        "TupleToolGeometry",
        "TupleToolKinematic",
        "TupleToolPid",
        "TupleToolPrimaries",
        "TupleToolRecoStats",
        "TupleToolTrackInfo",
        # trigger      
        "TupleToolTISTOS"
        ]

    # RecoStats for filling SpdMult, etc
    tuple.addTool(TupleToolRecoStats, name="TupleToolRecoStats")
    tuple.TupleToolRecoStats.Verbose=True


    #LoKi one
    from Configurables import LoKi__Hybrid__TupleTool
    LoKi_Jpsi=LoKi__Hybrid__TupleTool("LoKi_Jpsi")
    LoKi_Jpsi.Variables = {
        "ARMENTEROS":"ARMENTEROS"
        ,"LV01" : "LV01"
        ,"ETA"  : "ETA"
        ,"Y"    : "Y"  
        ,"KKMass": "WM('K+','K-')"
        }
    
    tuple.addTool(TupleToolDecay, name="Jpsi")
    tuple.Jpsi.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_Jpsi"]
    tuple.Jpsi.addTool(LoKi_Jpsi)

    """
    MC Information 
    """
    from Configurables import TupleToolMCTruth
    tuple.ToolList += [
        "TupleToolMCBackgroundInfo",
        "TupleToolMCTruth"
        ]
    
    MCTruth = TupleToolMCTruth()
    MCTruth.ToolList =  [
        "MCTupleToolAngles"
        , "MCTupleToolHierarchy"
        , "MCTupleToolKinematic"
        , "MCTupleToolReconstructed"
        ]
    tuple.addTool(MCTruth)
    

from os import environ
from GaudiKernel.SystemOfUnits import *
from Gaudi.Configuration import *
from Configurables import GaudiSequencer, CombineParticles
# DecayTreeTuple
from Configurables import DecayTreeTuple, EventTuple
from Configurables import TupleToolTrigger, TupleToolTriggerRecoStats

Jpsi2ppBranches = {
     "ProtonP"  :  "psi(2S) -> ^p+ p~-"
    ,"ProtonM"  :  "psi(2S) ->  p+^p~-"
    ,"Jpsi" :  "psi(2S): psi(2S) ->  p+ p~-"
    }

myTriggerList = [
    # L0
    "L0ElectronDecision",
    "L0PhotonDecision",
    "L0HadronDecision",
    # L0 Muon
    "L0MuonDecision",
    "L0MuonHighDecision",
    "L0DiMuonDecision",
    
    # Hlt1
    "Hlt1DiProtonLowMultDecision",
    "Hlt1DiProtonDecision",
    # Hlt1 track 
    "Hlt1TrackAllL0Decision",
    "Hlt1TrackMuonDecision",
    "Hlt1TrackPhotonDecision",

    # Hlt2
    "Hlt2DiProtonDecision",
    "Hlt2DiProtonTFDecision",
    "Hlt2DiProtonLowMultDecision",
    "Hlt2DiProtonLowMultTFDecision",
    
    # Hlt2 Topo lines
    "Hlt2Topo2BodyBBDTDecision",
    "Hlt2Topo3BodyBBDTDecision",
    "Hlt2Topo4BodyBBDTDecision"
    ]


# Make Jpsi
from Configurables import GaudiSequencer, CombineParticles, DecayTreeTuple, OfflineVertexFitter



PreselJpsi2pp = CombineParticles( "PreselJpsi2pp" )
PreselJpsi2pp.DecayDescriptor = "psi(2S) -> p+ p~-"
PreselJpsi2pp.Inputs = [ 'Phys/StdTightProtons/Particles' ]
PreselJpsi2pp.DaughtersCuts = { "p+" : "(PT>0.5*GeV) & (TRCHI2DOF<4.0) & (PIDp>5) & ((PIDp-PIDK)>0)" }
PreselJpsi2pp.MotherCut = "(VFASPF(VCHI2/VDOF)<9)"

Jpsi2ppLocation = "Phys/PreselJpsi2pp/Particles"

Jpsi2ppTuple = DecayTreeTuple("Jpsi2ppTuple")
Jpsi2ppTuple.Decay = "psi(2S) -> ^p+ ^p~-"
Jpsi2ppTuple.Inputs = [ Jpsi2ppLocation ]

fillTuple( Jpsi2ppTuple, Jpsi2ppBranches, myTriggerList )


SeqPreselJpsi2pp = GaudiSequencer("SeqPreselJpsi2pp")
SeqPreselJpsi2pp.Members += [ PreselJpsi2pp,
                              Jpsi2ppTuple
                              ]

from Configurables import RawBankToSTClusterAlg
createITClusters = RawBankToSTClusterAlg("CreateITClusters")
createITClusters.DetType     = "IT"

from Configurables import DaVinci
DaVinci().EvtMax = -1                          # Number of events
DaVinci().SkipEvents = 0                       # Events to skip
DaVinci().PrintFreq = 1000
DaVinci().DataType = "2011"
DaVinci().Simulation    = True
DaVinci().HistogramFile = "DVHistos.root"      # Histogram file
DaVinci().TupleFile = "Tuple.root"             # Ntuple
DaVinci().UserAlgorithms = [ createITClusters,
                             SeqPreselJpsi2pp                              
                             ]        # The algorithms

DaVinci().DDDBtag = 'MC11-20111102'
DaVinci().CondDBtag  = "sim-20111111-vc-md100"

from Configurables import MessageSvc
MessageSvc().setWarning = [ 'RFileCnv' ]

