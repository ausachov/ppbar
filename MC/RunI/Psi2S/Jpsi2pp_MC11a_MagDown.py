DaVinciVersion = 'v29r1'
myJobName = 'Psi2S2pp_MC11a_MagDown'
myApplication = DaVinci()
myApplication.version = DaVinciVersion
myApplication.user_release_area = '/exp/LHCb/usachov/cmtuser'
myApplication.platform='x86_64-slc5-gcc43-opt'
myApplication.optsfile = ['DaVinci_Ccbar2Ppbar_MC11a_MagDown.py', 'add-XMLSummary.py']

#data = readDiracLFNList("/users/LHCb/he/diracDir/Jobs/Jpsi2ppPi0/1/lfn.db")
data = BKQuery('/MC/2011/Beam3500GeV-2011-MagDown-Nu2-EmNoCuts/Sim05c/Trig0x40760037Flagged/Reco12a/Stripping17NoPrescalingFlagged/28102002/ALLSTREAMS.DST', dqflag=['All']).getDataset()

mySplitter = SplitByFiles( filesPerJob = 1, maxFiles = -1, ignoremissing = True )

myBackend = Dirac()
j = Job (
    name         = myJobName,
    application  = myApplication,
    splitter     = mySplitter,
    outputfiles  = [ SandboxFile('Tuple.root'),
                     SandboxFile('DVHistos.root')
                     ],
    inputdata    = data,
    backend	 = myBackend,
    do_auto_resubmit = True
    )
j.submit(keep_going=True, keep_on_fail=True)
