DaVinciVersion = 'v29r1'
myJobName = 'Jpsi2ppPi0_OwnSel_MC11a_MagDown'
myApplication = DaVinci()
myApplication.version = DaVinciVersion
myApplication.user_release_area = '/exp/LHCb/he/cmtuser'
myApplication.platform='x86_64-slc5-gcc43-opt'
myApplication.optsfile = ['DaVinci_Ccbar2Ppbar_MC11a_MagDown.py', 'add-XMLSummary.py']

data = readDiracLFNList("/users/LHCb/he/diracDir/Jobs/Jpsi2ppPi0/1/lfn.db")

mySplitter = DiracSplitter( filesPerJob = 20, maxFiles = -1, ignoremissing = True )

myInputsandbox = []
myOutputsandbox = ['Tuple.root', 'DVHistos.root', 'summary.xml']

myOutputData = ['*.dst']

myBackend = Dirac()
j = Job (
    name         = myJobName,
    application  = myApplication,
    splitter     = mySplitter,
    inputsandbox = myInputsandbox,
    outputsandbox= myOutputsandbox,
    backend      = myBackend,
    inputdata    = data,
    outputdata   = myOutputData
    )
j.submit()

