
from ROOT import *

import sys
import os
sys.path.insert(0, '../')
from makeConfigDictionary import *


def mergeMCDecayTreeTuples(eventType="Etac"):
  
  chain2015    = TChain("MCDecayTree")
  chain2015bis = TChain("MCDecayTree")
  chain2016    = TChain("MCDecayTree")
  chain2016bis = TChain("MCDecayTree")

  if eventType=="incl_b":
    chain2015.Add("tuples/AddBr/{}_Gen_2015_AddBr.root".format(eventType))
    chain2016.Add("tuples/AddBr/{}_Gen_2016_AddBr.root".format(eventType))

    tListUnsel = TList()
    tListUnsel.Add(chain2015.CopyTree(""))
    tListUnsel.Add(chain2016.CopyTree(""))
  else:
    chain2015bis.Add("tuples/AddBr/{}_Gen_2015bis_AddBr.root".format(eventType))
    chain2016.Add("tuples/AddBr/{}_Gen_2016_AddBr.root".format(eventType))
    chain2016bis.Add("tuples/AddBr/{}_Gen_2016bis_AddBr.root".format(eventType))

    tListUnsel = TList()
    tListUnsel.Add(chain2015bis.CopyTree(""))
    tListUnsel.Add(chain2016.CopyTree(""))
    tListUnsel.Add(chain2016bis.CopyTree(""))

  fOutUnsel = TFile("tuples/selected/{}_allMCDecayTree_AddBr.root".format(eventType),"RECREATE");
  selectedTreeUnsel = TTree()
  selectedTreeUnsel = TTree.MergeTrees(tListUnsel)
  evtsselectedUnsel = selectedTreeUnsel.GetEntries("")
  selectedTreeUnsel.GetCurrentFile().Write()
  selectedTreeUnsel.GetCurrentFile().Close()






def makeSelected(eventType="Etac", stripKey = "Pr", cutType="all_l0TOS"):
  
  if stripKey=="Sec":
    cut2015 = cutsAprioriDetached
    cut2016 = cutsAprioriDetached
  elif stripKey== "SecNoPID":
    cut2015 = cutsAprioriDetachedNoPID
    cut2016 = cutsAprioriDetachedNoPID
  elif stripKey=="Pr":
    cut2015 = cutApriori2015_Dict["DiProton"]
    cut2016 = cutApriori2016_Dict["DiProton"]
  else:
    print "cutType ",cutType," is not recognised! Aborting..."
    return


  cutSel = cutsDict[cutType]

  chain2015     = TChain("DecayTree")
  chain2015bis  = TChain("DecayTree")
  chain2016     = TChain("DecayTree")
  chain2016bis  = TChain("DecayTree")


  if eventType=="incl_b":
    chain2015.Add("tuples/AddBr/{}_{}_2015_AddBr.root".format(eventType,stripKey))
    chain2016.Add("tuples/AddBr/{}_{}_2016_AddBr.root".format(eventType,stripKey))

    tList = TList()
    tList.Add(chain2015.CopyTree(cut2015+" && "+cutSel))
    tList.Add(chain2016.CopyTree(cut2016+" && "+cutSel))
  else:
    chain2015bis.Add("tuples/AddBr/{}_{}_2015bis_AddBr.root".format(eventType,stripKey))
    chain2016.Add("tuples/AddBr/{}_{}_2016_AddBr.root".format(eventType,stripKey))
    chain2016bis.Add("tuples/AddBr/{}_{}_2016bis_AddBr.root".format(eventType,stripKey))

    tList = TList()
    tList.Add(chain2015bis.CopyTree(cut2015+" && "+cutSel))
    tList.Add(chain2016.CopyTree(cut2016+" && "+cutSel))
    tList.Add(chain2016bis.CopyTree(cut2016+" && "+cutSel))



  try:
    os.system('mkdir tuples/selected/'+cutType)
  except:
    pass

  fOut = TFile("tuples/selected/"+cutType+"/{}_{}_{}_allSelected_AddBr.root".format(eventType,stripKey,cutType),"RECREATE");
  selectedTree = TTree()
  selectedTree = TTree.MergeTrees(tList)
  evtsselected = selectedTree.GetEntries()
  selectedTree.Write()
  fOut.Close()

  if eventType=="incl_b":
    tListUnsel = TList()
    tListUnsel.Add(chain2015.CopyTree(""))
    tListUnsel.Add(chain2016.CopyTree(""))
  else:
    tListUnsel = TList()
    tListUnsel.Add(chain2015bis.CopyTree(""))
    tListUnsel.Add(chain2016.CopyTree(""))
    tListUnsel.Add(chain2016bis.CopyTree(""))

  fOutUnsel = TFile("tuples/selected/{}_{}_allUnsel_AddBr.root".format(eventType,stripKey),"RECREATE");
  selectedTreeUnsel = TTree()
  selectedTreeUnsel = TTree.MergeTrees(tListUnsel)
  evtsselectedUnsel = selectedTreeUnsel.GetEntries()
  selectedTreeUnsel.GetCurrentFile().Write()
  selectedTreeUnsel.GetCurrentFile().Close()

  eff = float(evtsselected)/float(evtsselectedUnsel)

  return eff


print "Etac       secondary_l0TOS eff :  ", makeSelected(eventType="Etac",stripKey="Pr", cutType="secondary_l0TOS")
print "incl_b     secondary_l0TOS eff :  ", makeSelected(eventType="incl_b",stripKey="Pr",cutType="secondary_l0TOS")
print "Jpsi       secondary_l0TOS eff :  ", makeSelected(eventType="Jpsi",stripKey="Pr", cutType="secondary_l0TOS")
print "Jpsi2pppi0 secondary_l0TOS eff :  ", makeSelected(eventType="Jpsi2pppi0",stripKey="Pr",cutType="secondary_l0TOS")

print "Etac       prompt_l0TOS eff :  ", makeSelected(eventType="Etac",stripKey="Pr", cutType="prompt_l0TOS")
# print "incl_b     prompt_l0TOS eff :  ", makeSelected(eventType="incl_b",stripKey="Pr",cutType="prompt_l0TOS")
print "Jpsi       prompt_l0TOS eff :  ", makeSelected(eventType="Jpsi",stripKey="Pr", cutType="prompt_l0TOS")
print "Jpsi2pppi0 prompt_l0TOS eff :  ", makeSelected(eventType="Jpsi2pppi0",stripKey="Pr",cutType="prompt_l0TOS")


print "Etac       prompt selection eff :  ", makeSelected(eventType="Etac",stripKey="Pr", cutType="all_l0TOS")
print "incl_b     prompt selection eff :  ", makeSelected(eventType="incl_b",stripKey="Pr",cutType="all_l0TOS")
print "Jpsi       prompt selection eff :  ", makeSelected(eventType="Jpsi",stripKey="Pr", cutType="all_l0TOS")
print "Jpsi2pppi0 prompt selection eff :  ", makeSelected(eventType="Jpsi2pppi0",stripKey="Pr",cutType="all_l0TOS")

print "Etac       detached selection eff :  ", makeSelected(eventType="Etac",stripKey="Sec",cutType="Detached")
print "incl_b     detached selection eff :  ", makeSelected(eventType="incl_b",stripKey="Sec",cutType="Detached")
print "Jpsi       detached selection eff :  ", makeSelected(eventType="Jpsi",stripKey="Sec",cutType="Detached")
print "Jpsi2pppi0       detached selection eff :  ", makeSelected(eventType="Jpsi2pppi0",stripKey="Sec",cutType="Detached")


print "Etac    detached NoPID eff :  ", makeSelected(eventType="Etac",stripKey="SecNoPID",cutType="DetachedNoPID")
print "incl_b  detached NoPID eff :  ", makeSelected(eventType="incl_b",stripKey="SecNoPID",cutType="DetachedNoPID")
print "Jpsi    detached NoPID eff :  ", makeSelected(eventType="Jpsi",stripKey="SecNoPID",cutType="DetachedNoPID")
# print "Jpsi2pppi0 detached NoPID eff :  ", makeSelected(eventType="Jpsi2pppi0",stripKey="SecNoPID",cutType="DetachedNoPID")


print "Etac       secondaryTopoOpt eff :  ", makeSelected(eventType="Etac",stripKey="Sec",cutType="secondaryTopoOpt")
print "incl_b     secondaryTopoOpt eff :  ", makeSelected(eventType="incl_b",stripKey="Sec",cutType="secondaryTopoOpt")
print "Jpsi       secondaryTopoOpt eff :  ", makeSelected(eventType="Jpsi",stripKey="Sec",cutType="secondaryTopoOpt")
print "Jpsi2pppi0 secondaryTopoOpt eff :  ", makeSelected(eventType="Jpsi2pppi0",stripKey="Sec",cutType="secondaryTopoOpt")




mergeMCDecayTreeTuples("Etac")
mergeMCDecayTreeTuples("incl_b")
mergeMCDecayTreeTuples("Jpsi")
mergeMCDecayTreeTuples("Jpsi2pppi0")

