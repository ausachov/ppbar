from ROOT import *
from array import array

spsDir = "/sps/lhcb/zhovkovska/etacToPpbar/"
homeDir = "/users/LHCb/zhovkovska/"
def select( charm, year, add):

    #gROOT.Reset()
    nt = TChain("DecayTree")
#   nt.Add("CharmSelect.root")
#   TChain * nt=new TChain("tuple/DecayTree")
#   nt.Add("Tuple*.root")
#    nt.Add("Reduced_Snd_2012_cuted.root")
#    nt.Add("MC/Etac/2016/EtacDiProton_MC_2016.root")
#    nt.Add("MC/Jpsi/2015/JpsiDiProton_MC_2015.root")    
    
    if (add == ''):
        nt.Add(spsDir+"MC/{}DiProton_MC_{}{}.root".format(charm, year, add))
    elif (add == "incl_b"):
        nt.Add(spsDir+"MC/{}DiProton_MC_{}_{}.root".format(charm, year, add))
    else:
        nt.Add(spsDir+"MC/{}DiProtonPi0_MC_{}.root".format(charm, year))
        
    bMesID = [5, 511, 521, 531, 541, -511, -521, -531, -541]
    bHadrID = [ 511, 521, 531, 541, -511, -521, -531, -541, 5122, 5112, 5212, 5222, 5132, 5232, 5332, -5122, -5112, -5212, -5222, -5132, -5232, -5332]
    charmID = [ 441, 10441, 100441, 443, 10443, 20443, 100443, 30443, 9000443, 9010443, 9020443, 445, 100445]
    dirCgarmID = [0, 4, -4]

#   TTree *tree = new TTree ("tree","DecayTree")
    #new branches
    prompt = array( 'l', [False])
    sec = array( 'l', [False])
    #gROOT.ProcessLine()
    Jpsi_M_res = array( 'd', [0])
    Jpsi_TRUEM = array( 'd', [0])
    Jpsi_Tz = array( 'd', [0])
    Jpsi_Tz_res = array( 'd', [0])
    Jpsi_Dist_CHI2 = array( 'd', [0])
    Jpsi_TRUEPV_X = array( 'd', [0])
    Jpsi_TRUEPV_Y = array( 'd', [0])
    Jpsi_TRUEPV_Z = array( 'd', [0])
    Jpsi_TRUETz = array( 'd', [0])
                
        
    #mother parameters    
    Jpsi_MC_MOTHER_TRUEV_X  = array( 'd', [0])
    Jpsi_MC_MOTHER_TRUEV_Y = array( 'd', [0])
    Jpsi_MC_MOTHER_TRUEV_Z = array( 'd', [0])
    Jpsi_MC_GD_MOTHER_TRUEV_X  = array( 'd', [0])
    Jpsi_MC_GD_MOTHER_TRUEV_Y = array( 'd', [0])
    Jpsi_MC_GD_MOTHER_TRUEV_Z = array( 'd', [0])
    Jpsi_MC_GD_GD_MOTHER_TRUEV_X  = array( 'd', [0])
    Jpsi_MC_GD_GD_MOTHER_TRUEV_Y  = array( 'd', [0])
    Jpsi_MC_GD_GD_MOTHER_TRUEV_Z  = array( 'd', [0])
    Jpsi_MC_GD_GD_GD_MOTHER_TRUEV_X  = array( 'd', [0])
    Jpsi_MC_GD_GD_GD_MOTHER_TRUEV_Y  = array( 'd', [0])
    Jpsi_MC_GD_GD_GD_MOTHER_TRUEV_Z = array( 'd', [0])
    Jpsi_MC_MOTHER_ID = array( 'i', [0])
    Jpsi_MC_GD_MOTHER_ID = array( 'i', [0])
    Jpsi_MC_GD_GD_MOTHER_ID = array( 'i', [0])
    Jpsi_MC_GD_GD_GD_MOTHER_ID = array( 'i', [0])
    
        
    Jpsi_ENDVERTEX_Z = array( 'd', [0])
    Jpsi_OWNPV_Z = array( 'd', [0])
    Jpsi_MM = array( 'd', [0])
    Jpsi_PZ = array( 'd', [0])
    Jpsi_M  = array( 'd', [0])
    PVX  = array( 'f', [0])
    PVY  = array( 'f', [0])
    PVZ  = array( 'f', [0])
    PVXERR  = array( 'f', [0])
    PVYERR  = array( 'f', [0])
    PVZERR  = array( 'f', [0])
    
    #true variables
    Jpsi_TRUEP_E = array( 'd', [0])
    Jpsi_TRUEP_X = array( 'd', [0])
    Jpsi_TRUEP_Y = array( 'd', [0])
    Jpsi_TRUEP_Z = array( 'd', [0])
    Jpsi_TRUE_Tz = array( 'd', [0])
    Jpsi_TRUEENDVERTEX_X = array( 'd', [0])
    Jpsi_TRUEENDVERTEX_Y = array( 'd', [0])
    Jpsi_TRUEENDVERTEX_Z = array( 'd', [0])

    nt.SetBranchAddress("Jpsi_ENDVERTEX_Z",Jpsi_ENDVERTEX_Z)
    nt.SetBranchAddress("Jpsi_OWNPV_Z",Jpsi_OWNPV_Z)
    nt.SetBranchAddress("Jpsi_MM",Jpsi_MM)
    nt.SetBranchAddress("Jpsi_PZ",Jpsi_PZ)
    
    nt.SetBranchAddress("Jpsi_TRUEENDVERTEX_X",Jpsi_TRUEENDVERTEX_X)
    nt.SetBranchAddress("Jpsi_TRUEENDVERTEX_Y",Jpsi_TRUEENDVERTEX_Y)
    nt.SetBranchAddress("Jpsi_TRUEENDVERTEX_Z",Jpsi_TRUEENDVERTEX_Z)
    nt.SetBranchAddress("Jpsi_MC_MOTHER_ID",Jpsi_MC_MOTHER_ID)
    nt.SetBranchAddress("Jpsi_MC_MOTHER_TRUEV_X",Jpsi_MC_MOTHER_TRUEV_X)
    nt.SetBranchAddress("Jpsi_MC_MOTHER_TRUEV_Y",Jpsi_MC_MOTHER_TRUEV_Y)
    nt.SetBranchAddress("Jpsi_MC_MOTHER_TRUEV_Z",Jpsi_MC_MOTHER_TRUEV_Z)
    nt.SetBranchAddress("Jpsi_MC_GD_MOTHER_ID",Jpsi_MC_GD_MOTHER_ID)
    nt.SetBranchAddress("Jpsi_MC_GD_MOTHER_TRUEV_X",Jpsi_MC_GD_MOTHER_TRUEV_X)
    nt.SetBranchAddress("Jpsi_MC_GD_MOTHER_TRUEV_Y",Jpsi_MC_GD_MOTHER_TRUEV_Y)
    nt.SetBranchAddress("Jpsi_MC_GD_MOTHER_TRUEV_Z",Jpsi_MC_GD_MOTHER_TRUEV_Z)
    nt.SetBranchAddress("Jpsi_MC_GD_GD_MOTHER_ID",Jpsi_MC_GD_GD_MOTHER_ID)    
    nt.SetBranchAddress("Jpsi_MC_GD_GD_MOTHER_TRUEV_X",Jpsi_MC_GD_GD_MOTHER_TRUEV_X)    
    nt.SetBranchAddress("Jpsi_MC_GD_GD_MOTHER_TRUEV_Y",Jpsi_MC_GD_GD_MOTHER_TRUEV_Y)    
    nt.SetBranchAddress("Jpsi_MC_GD_GD_MOTHER_TRUEV_Z",Jpsi_MC_GD_GD_MOTHER_TRUEV_Z)    
    #nt.SetBranchAddress("Jpsi_MC_GD_GD_GD_MOTHER_ID",Jpsi_MC_GD_GD_GD_MOTHER_ID)    
    #nt.SetBranchAddress("Jpsi_MC_GD_GD_GD_MOTHER_TRUEV_X",Jpsi_MC_GD_GD_GD_MOTHER_TRUEV_X)    
    #nt.SetBranchAddress("Jpsi_MC_GD_GD_GD_MOTHER_TRUEV_Y",Jpsi_MC_GD_GD_GD_MOTHER_TRUEV_Y)    
    #nt.SetBranchAddress("Jpsi_MC_GD_GD_GD_MOTHER_TRUEV_Z",Jpsi_MC_GD_GD_GD_MOTHER_TRUEV_Z)    



    nt.SetBranchAddress("Jpsi_TRUEP_E",Jpsi_TRUEP_E)
    nt.SetBranchAddress("Jpsi_TRUEP_X",Jpsi_TRUEP_X)
    nt.SetBranchAddress("Jpsi_TRUEP_Y",Jpsi_TRUEP_Y)
    nt.SetBranchAddress("Jpsi_TRUEP_Z",Jpsi_TRUEP_Z)
    nt.SetBranchAddress("Jpsi_TRUE_Tz",Jpsi_TRUE_Tz)
    nt.SetBranchAddress("Jpsi_M",Jpsi_M)
    
    nt.SetBranchAddress("PVX",PVX)
    nt.SetBranchAddress("PVY",PVY)
    nt.SetBranchAddress("PVZ",PVZ)
    nt.SetBranchAddress("PVXERR",PVXERR)
    nt.SetBranchAddress("PVYERR",PVYERR)
    nt.SetBranchAddress("PVZERR",PVZERR)
    
#    TFile *f = new TFile("MC/Etac/2016/EtacDiProton_MC_2016_AddBr.root", "RECREATE")
#    TFile *f = new TFile("MC/Jpsi/2015/JpsiDiProton_MC_2015_AddBr.root", "RECREATE")
    if (add == ''):
        f = TFile(homeDir+'scripts/MC/newMC/{}/{}DiProton_MC_{}_AddBr.root'.format(charm, charm, year), "RECREATE")
    elif (add == "incl_b"):
        f = TFile(homeDir+'scripts/MC/newMC/{}/{}DiProton_MC_{}_{}_AddBr.root'.format(add, charm, year, add), "RECREATE")
    else:
        f = TFile(homeDir+'scripts/MC/newMC/{}/{}DiProtonPi0_MC_{}_AddBr.root'.format(charm+"2pppi0", charm, year, add), "RECREATE")
    
    tree = nt.CloneTree(0)
    tree.Branch("Jpsi_prompt",prompt,"prompt/O")
    tree.Branch("Jpsi_sec",sec,"sec/O")
    tree.Branch("Jpsi_M_res",Jpsi_M_res,"Jpsi_M_res/D")
    tree.Branch("Jpsi_TRUEM",Jpsi_TRUEM,"Jpsi_TRUEM/D")
    tree.Branch("Jpsi_Tz",Jpsi_Tz,"Jpsi_Tz/D")
    tree.Branch("Jpsi_Tz_res",Jpsi_Tz_res,"Jpsi_Tz_res/D")
    tree.Branch("Jpsi_Dist_CHI2",Jpsi_Dist_CHI2,"Jpsi_Dist_CHI2/D")
    
    tree.Branch("Jpsi_TRUEPV_X",Jpsi_TRUEPV_X,"Jpsi_TRUEPV_X/D")
    tree.Branch("Jpsi_TRUEPV_Y",Jpsi_TRUEPV_Y,"Jpsi_TRUEPV_Y/D")
    tree.Branch("Jpsi_TRUEPV_Z",Jpsi_TRUEPV_Z,"Jpsi_TRUEPV_Z/D")
    tree.Branch("Jpsi_TRUETz",Jpsi_TRUETz,"Jpsi_TRUETz/D")

    nEntries = nt.GetEntries()

    for iEn in range(nEntries):
        
        nt.GetEntry(iEn)
        Jpsi_Tz[0] = (3.3* (Jpsi_ENDVERTEX_Z[0] - Jpsi_OWNPV_Z[0])*Jpsi_MM[0]/Jpsi_PZ[0])
        Jpsi_TRUEM[0] = TMath.Sqrt(Jpsi_TRUEP_E[0]*Jpsi_TRUEP_E[0] - (Jpsi_TRUEP_X[0]*Jpsi_TRUEP_X[0] + Jpsi_TRUEP_Y[0]*Jpsi_TRUEP_Y[0] + Jpsi_TRUEP_Z[0]*Jpsi_TRUEP_Z[0]))
        Jpsi_M_res[0] = Jpsi_M[0] - Jpsi_TRUEM[0]
        
        
#        std.cout<<Jpsi_MC_MOTHER_ID<<"   "<<Jpsi_MC_GD_MOTHER_ID<<"   "<<Jpsi_MC_GD_GD_MOTHER_ID<<"   "<<std.endl 
        if (Jpsi_MC_MOTHER_ID[0] in dirCgarmID ):

            prompt[0] = True
            sec[0] = False
            Jpsi_TRUEPV_X[0] = Jpsi_TRUEENDVERTEX_X[0]
            Jpsi_TRUEPV_Y[0] = Jpsi_TRUEENDVERTEX_Y[0]
            Jpsi_TRUEPV_Z[0] = Jpsi_TRUEENDVERTEX_Z[0]

        elif (Jpsi_MC_MOTHER_ID[0] in charmID):
        
            if (((Jpsi_MC_GD_MOTHER_ID[0]) in dirCgarmID)  or ((Jpsi_MC_GD_MOTHER_ID[0] in charmID) and ((Jpsi_MC_GD_GD_MOTHER_ID[0] in dirCgarmID) or (Jpsi_MC_GD_GD_MOTHER_ID[0] in charmID)))):
        
                prompt[0] = True
                sec[0] = False
                Jpsi_TRUEPV_X[0] = Jpsi_MC_MOTHER_TRUEV_X[0]
                Jpsi_TRUEPV_Y[0] = Jpsi_MC_MOTHER_TRUEV_Y[0]
                Jpsi_TRUEPV_Z[0] = Jpsi_MC_MOTHER_TRUEV_Z[0]

            #elif ( Jpsi_MC_GD_MOTHER_ID[0] in charmID and Jpsi_MC_GD_GD_MOTHER_ID[0] in charmID and Jpsi_MC_GD_GD_GD_MOTHER_ID[0] in bHadrID ):

                #prompt[0] = False
                #sec[0] = True
                #Jpsi_TRUEPV_X[0] = Jpsi_MC_GD_GD_GD_MOTHER_TRUEV_X[0]
                #Jpsi_TRUEPV_Y[0] = Jpsi_MC_GD_GD_GD_MOTHER_TRUEV_Y[0]
                #Jpsi_TRUEPV_Z[0] = Jpsi_MC_GD_GD_GD_MOTHER_TRUEV_Z[0]

            elif ( Jpsi_MC_GD_MOTHER_ID[0] in charmID and Jpsi_MC_GD_GD_MOTHER_ID[0] in bHadrID ):

                prompt[0] = False
                sec[0] = True
                Jpsi_TRUEPV_X[0] = Jpsi_MC_GD_GD_GD_MOTHER_TRUEV_X[0]
                Jpsi_TRUEPV_Y[0] = Jpsi_MC_GD_GD_GD_MOTHER_TRUEV_Y[0]
                Jpsi_TRUEPV_Z[0] = Jpsi_MC_GD_GD_GD_MOTHER_TRUEV_Z[0]

            else:
                if( Jpsi_MC_GD_MOTHER_ID[0] in bHadrID):
                    prompt[0] = False
                    sec[0] = True
                    Jpsi_TRUEPV_X[0] = Jpsi_MC_GD_GD_MOTHER_TRUEV_X[0]
                    Jpsi_TRUEPV_Y[0] = Jpsi_MC_GD_GD_MOTHER_TRUEV_Y[0]
                    Jpsi_TRUEPV_Z[0] = Jpsi_MC_GD_GD_MOTHER_TRUEV_Z[0]
                else:
                    print iEn
                    print Jpsi_MC_MOTHER_ID[0], Jpsi_MC_GD_MOTHER_ID[0], Jpsi_MC_GD_GD_MOTHER_ID[0]

        else:

            if( Jpsi_MC_MOTHER_ID[0] in bHadrID):

                prompt[0] = False
                sec[0] = True
                Jpsi_TRUEPV_X[0] = Jpsi_MC_GD_MOTHER_TRUEV_X[0]
                Jpsi_TRUEPV_Y[0] = Jpsi_MC_GD_MOTHER_TRUEV_Y[0]
                Jpsi_TRUEPV_Z[0] = Jpsi_MC_GD_MOTHER_TRUEV_Z[0]
            else:
                print iEn
                print Jpsi_MC_MOTHER_ID[0], Jpsi_MC_GD_MOTHER_ID[0], Jpsi_MC_GD_GD_MOTHER_ID[0]
                
        Dist_CHI2 = ( TMath.Abs(float(Jpsi_TRUEPV_X[0])-PVX[0])/(PVXERR[0]) + TMath.Abs(float(Jpsi_TRUEPV_Y[0])-PVY[0])/(PVYERR[0]) + TMath.Abs(float(Jpsi_TRUEPV_Z[0])-PVZ[0])/(PVZERR[0]))
        Jpsi_Dist_CHI2[0] = Dist_CHI2
        Jpsi_TRUETz[0] = (3.3* (Jpsi_TRUEENDVERTEX_Z[0] - Jpsi_TRUEPV_Z[0])*Jpsi_TRUEM[0]/Jpsi_TRUEP_Z[0])
        Jpsi_TRUE_Tz[0] = Jpsi_TRUETz[0]
        Jpsi_Tz_res[0] = Jpsi_Tz[0] - Jpsi_TRUETz[0]
        tree.Fill()

        #print Jpsi_Dist_CHI2[0], PVXERR[0], PVYERR[0], PVZERR[0]


    c = TCanvas("Signal_Fit","Signal Fit",800,500)
    gPad.SetLeftMargin(0.15) 

    tree.Draw("Jpsi_Dist_CHI2")
    #c.SaveAs("%s_Dist_CHI2_%s_%s.png"%(charm,year,add))
    tree.Write()
    
    f.Close()


#    cut_tzPT()


def selectGen( charm, year, add):

    #gROOT.Reset()
    nt = TChain("MCDecayTree")
#   nt.Add("CharmSelect.root")
#   TChain * nt=new TChain("tuple/DecayTree")
#   nt.Add("Tuple*.root")
#    nt.Add("Reduced_Snd_2012_cuted.root")
#    nt.Add("MC/Etac/2016/EtacDiProton_MC_2016.root")
#    nt.Add("MC/Jpsi/2015/JpsiDiProton_MC_2015.root")    
    
    if (add == ''):
        nt.Add(spsDir+"MC/{}DiProton_MC_Generator_{}{}.root".format(charm, year, add))
    elif (add == "incl_b"):
        nt.Add(spsDir+"MC/{}DiProton_MC_Generator_{}_{}.root".format(charm, year, add))
    else:
        nt.Add(spsDir+"MC/{}DiProtonPi0_MC_{}.root".format(charm, year))
        
    bMesID = [5, 511, 521, 531, 541, -511, -521, -531, -541]
    bHadrID = [ 511, 521, 531, 541, -511, -521, -531, -541, 5122, 5112, 5212, 5222, 5132, 5232, 5332, -5122, -5112, -5212, -5222, -5132, -5232, -5332]
    charmID = [ 441, 10441, 100441, 443, 10443, 20443, 100443, 30443, 9000443, 9010443, 9020443, 445, 100445]
    dirCgarmID = [0, 4, -4]

#   TTree *tree = new TTree ("tree","DecayTree")
    #new branches
    prompt = array( 'l', [False])
    sec = array( 'l', [False])
    #gROOT.ProcessLine()
    Jpsi_M_res = array( 'd', [0])
    Jpsi_TRUEM = array( 'd', [0])
    #Jpsi_Tz = array( 'd', [0])
    #Jpsi_Tz_res = array( 'd', [0])
    Jpsi_Dist_CHI2 = array( 'd', [0])
    Jpsi_TRUEPV_X = array( 'd', [0])
    Jpsi_TRUEPV_Y = array( 'd', [0])
    Jpsi_TRUEPV_Z = array( 'd', [0])
    Jpsi_TRUE_Tz = array( 'd', [0])
                
        
    #mother parameters    
    Jpsi_MC_MOTHER_TRUEV_X  = array( 'd', [0])
    Jpsi_MC_MOTHER_TRUEV_Y = array( 'd', [0])
    Jpsi_MC_MOTHER_TRUEV_Z = array( 'd', [0])
    Jpsi_MC_GD_MOTHER_TRUEV_X  = array( 'd', [0])
    Jpsi_MC_GD_MOTHER_TRUEV_Y = array( 'd', [0])
    Jpsi_MC_GD_MOTHER_TRUEV_Z = array( 'd', [0])
    Jpsi_MC_GD_GD_MOTHER_TRUEV_X  = array( 'd', [0])
    Jpsi_MC_GD_GD_MOTHER_TRUEV_Y  = array( 'd', [0])
    Jpsi_MC_GD_GD_MOTHER_TRUEV_Z  = array( 'd', [0])
    Jpsi_MC_GD_GD_GD_MOTHER_TRUEV_X  = array( 'd', [0])
    Jpsi_MC_GD_GD_GD_MOTHER_TRUEV_Y  = array( 'd', [0])
    Jpsi_MC_GD_GD_GD_MOTHER_TRUEV_Z = array( 'd', [0])
    Jpsi_MC_MOTHER_ID = array( 'i', [0])
    Jpsi_MC_GD_MOTHER_ID = array( 'i', [0])
    Jpsi_MC_GD_GD_MOTHER_ID = array( 'i', [0])
    Jpsi_MC_GD_GD_GD_MOTHER_ID = array( 'i', [0])
    
        
    #Jpsi_ENDVERTEX_Z = array( 'd', [0])
    #Jpsi_OWNPV_Z = array( 'd', [0])
    #Jpsi_MM = array( 'd', [0])
    #Jpsi_TRUEP_Z = array( 'd', [0])
    Jpsi_TRUEM  = array( 'd', [0])
    #PVX  = array( 'f', [0])
    #PVY  = array( 'f', [0])
    #PVZ  = array( 'f', [0])
    #PVXERR  = array( 'f', [0])
    #PVYERR  = array( 'f', [0])
    #PVZERR  = array( 'f', [0])
    
    #true variables

    ProtonM_TRUEPT = array( 'd', [0])
    ProtonM_TRUEP_X = array( 'd', [0])
    ProtonM_TRUEP_Y = array( 'd', [0])
    ProtonM_TRUEP_Z = array( 'd', [0])
    ProtonP_TRUEPT = array( 'd', [0])
    ProtonP_TRUEP_X = array( 'd', [0])
    ProtonP_TRUEP_Y = array( 'd', [0])
    ProtonP_TRUEP_Z = array( 'd', [0])
    Jpsi_TRUEETA = array( 'd', [0])

    Jpsi_TRUEP_E = array( 'd', [0])
    Jpsi_TRUEP_X = array( 'd', [0])
    Jpsi_TRUEP_Y = array( 'd', [0])
    Jpsi_TRUEP_Z = array( 'd', [0])
    Jpsi_TRUE_Tz = array( 'd', [0])
    Jpsi_TRUEENDVERTEX_X = array( 'd', [0])
    Jpsi_TRUEENDVERTEX_Y = array( 'd', [0])
    Jpsi_TRUEENDVERTEX_Z = array( 'd', [0])

    #nt.SetBranchAddress("Jpsi_ENDVERTEX_Z",Jpsi_ENDVERTEX_Z)
    #nt.SetBranchAddress("Jpsi_OWNPV_Z",Jpsi_OWNPV_Z)
    #nt.SetBranchAddress("Jpsi_MM",Jpsi_MM)
    #nt.SetBranchAddress("Jpsi_PZ",Jpsi_PZ)
    
    nt.SetBranchAddress("Jpsi_TRUEENDVERTEX_X",Jpsi_TRUEENDVERTEX_X)
    nt.SetBranchAddress("Jpsi_TRUEENDVERTEX_Y",Jpsi_TRUEENDVERTEX_Y)
    nt.SetBranchAddress("Jpsi_TRUEENDVERTEX_Z",Jpsi_TRUEENDVERTEX_Z)
    nt.SetBranchAddress("Jpsi_MC_MOTHER_ID",Jpsi_MC_MOTHER_ID)
    nt.SetBranchAddress("Jpsi_MC_MOTHER_TRUEV_X",Jpsi_MC_MOTHER_TRUEV_X)
    nt.SetBranchAddress("Jpsi_MC_MOTHER_TRUEV_Y",Jpsi_MC_MOTHER_TRUEV_Y)
    nt.SetBranchAddress("Jpsi_MC_MOTHER_TRUEV_Z",Jpsi_MC_MOTHER_TRUEV_Z)
    nt.SetBranchAddress("Jpsi_MC_GD_MOTHER_ID",Jpsi_MC_GD_MOTHER_ID)
    nt.SetBranchAddress("Jpsi_MC_GD_MOTHER_TRUEV_X",Jpsi_MC_GD_MOTHER_TRUEV_X)
    nt.SetBranchAddress("Jpsi_MC_GD_MOTHER_TRUEV_Y",Jpsi_MC_GD_MOTHER_TRUEV_Y)
    nt.SetBranchAddress("Jpsi_MC_GD_MOTHER_TRUEV_Z",Jpsi_MC_GD_MOTHER_TRUEV_Z)
    nt.SetBranchAddress("Jpsi_MC_GD_GD_MOTHER_ID",Jpsi_MC_GD_GD_MOTHER_ID)    
    nt.SetBranchAddress("Jpsi_MC_GD_GD_MOTHER_TRUEV_X",Jpsi_MC_GD_GD_MOTHER_TRUEV_X)    
    nt.SetBranchAddress("Jpsi_MC_GD_GD_MOTHER_TRUEV_Y",Jpsi_MC_GD_GD_MOTHER_TRUEV_Y)    
    nt.SetBranchAddress("Jpsi_MC_GD_GD_MOTHER_TRUEV_Z",Jpsi_MC_GD_GD_MOTHER_TRUEV_Z)    
    #nt.SetBranchAddress("Jpsi_MC_GD_GD_GD_MOTHER_ID",Jpsi_MC_GD_GD_GD_MOTHER_ID)    
    #nt.SetBranchAddress("Jpsi_MC_GD_GD_GD_MOTHER_TRUEV_X",Jpsi_MC_GD_GD_GD_MOTHER_TRUEV_X)    
    #nt.SetBranchAddress("Jpsi_MC_GD_GD_GD_MOTHER_TRUEV_Y",Jpsi_MC_GD_GD_GD_MOTHER_TRUEV_Y)    
    #nt.SetBranchAddress("Jpsi_MC_GD_GD_GD_MOTHER_TRUEV_Z",Jpsi_MC_GD_GD_GD_MOTHER_TRUEV_Z)    



    nt.SetBranchAddress("ProtonM_TRUEPT",ProtonM_TRUEPT)
    nt.SetBranchAddress("ProtonM_TRUEP_X",ProtonM_TRUEP_X)
    nt.SetBranchAddress("ProtonM_TRUEP_Y",ProtonM_TRUEP_Y)
    nt.SetBranchAddress("ProtonM_TRUEP_Z",ProtonM_TRUEP_Z)
    nt.SetBranchAddress("ProtonP_TRUEPT",ProtonP_TRUEPT)
    nt.SetBranchAddress("ProtonP_TRUEP_X",ProtonP_TRUEP_X)
    nt.SetBranchAddress("ProtonP_TRUEP_Y",ProtonP_TRUEP_Y)
    nt.SetBranchAddress("ProtonP_TRUEP_Z",ProtonP_TRUEP_Z)

    nt.SetBranchAddress("Jpsi_TRUEETA",Jpsi_TRUEETA)
    nt.SetBranchAddress("Jpsi_TRUEP_E",Jpsi_TRUEP_E)
    nt.SetBranchAddress("Jpsi_TRUEP_X",Jpsi_TRUEP_X)
    nt.SetBranchAddress("Jpsi_TRUEP_Y",Jpsi_TRUEP_Y)
    nt.SetBranchAddress("Jpsi_TRUEP_Z",Jpsi_TRUEP_Z)
    #nt.SetBranchAddress("Jpsi_TRUE_Tz",Jpsi_TRUE_Tz)
    nt.SetBranchAddress("Jpsi_TRUEM",Jpsi_TRUEM)
    
    #nt.SetBranchAddress("PVX",PVX)
    #nt.SetBranchAddress("PVY",PVY)
    #nt.SetBranchAddress("PVZ",PVZ)
    #nt.SetBranchAddress("PVXERR",PVXERR)
    #nt.SetBranchAddress("PVYERR",PVYERR)
    #nt.SetBranchAddress("PVZERR",PVZERR)
    
#    TFile *f = new TFile("MC/Etac/2016/EtacDiProton_MC_2016_AddBr.root", "RECREATE")
#    TFile *f = new TFile("MC/Jpsi/2015/JpsiDiProton_MC_2015_AddBr.root", "RECREATE")
    if (add == ''):
        f = TFile(homeDir+'scripts/MC/newMC/{}/{}DiProton_MC_Generator_{}_AddBr.root'.format(charm, charm, year), "RECREATE")
    elif (add == "incl_b"):
        f = TFile(homeDir+'scripts/MC/newMC/{}/{}DiProton_MC_Generator_{}_{}_AddBr.root'.format(add, charm, year, add), "RECREATE")
    else:
        f = TFile(homeDir+'scripts/MC/newMC/{}/{}DiProtonPi0_MC_{}_AddBr.root'.format(charm+"2pppi0", charm, year, add), "RECREATE")
    
    cutApriori = "       TMath::Sqrt(ProtonP_TRUEP_X*ProtonP_TRUEP_X+ProtonP_TRUEP_Y*ProtonP_TRUEP_Y+ProtonP_TRUEP_Z*ProtonP_TRUEP_Z)>12.5e3 && TMath::Sqrt(ProtonM_TRUEP_X*ProtonM_TRUEP_X+ProtonM_TRUEP_Y*ProtonM_TRUEP_Y+ProtonM_TRUEP_Z*ProtonM_TRUEP_Z)>12.5e3 && \
                         ProtonP_TRUEPT>2000 && ProtonM_TRUEPT>2000 && \
                         ProtonP_TRUEPT/TMath::Sqrt(ProtonP_TRUEP_X*ProtonP_TRUEP_X+ProtonP_TRUEP_Y*ProtonP_TRUEP_Y+ProtonP_TRUEP_Z*ProtonP_TRUEP_Z)>0.0366 && ProtonM_TRUEPT/TMath::Sqrt(ProtonM_TRUEP_X*ProtonM_TRUEP_X+ProtonM_TRUEP_Y*ProtonM_TRUEP_Y+ProtonM_TRUEP_Z*ProtonM_TRUEP_Z)>0.0366 && \
                         Jpsi_TRUEETA>2 && Jpsi_TRUEETA<4.5"

    
    tree = nt.CloneTree(0)
    tree.Branch("Jpsi_prompt",prompt,"prompt/O")
    tree.Branch("Jpsi_sec",sec,"sec/O")
    #tree.Branch("Jpsi_M_res",Jpsi_M_res,"Jpsi_M_res/D")
    #tree.Branch("Jpsi_TRUEM",Jpsi_TRUEM,"Jpsi_TRUEM/D")
    #tree.Branch("Jpsi_Tz",Jpsi_Tz,"Jpsi_Tz/D")
    #tree.Branch("Jpsi_Tz_res",Jpsi_Tz_res,"Jpsi_Tz_res/D")
    tree.Branch("Jpsi_Dist_CHI2",Jpsi_Dist_CHI2,"Jpsi_Dist_CHI2/D")
    
    tree.Branch("Jpsi_TRUEPV_X",Jpsi_TRUEPV_X,"Jpsi_TRUEPV_X/D")
    tree.Branch("Jpsi_TRUEPV_Y",Jpsi_TRUEPV_Y,"Jpsi_TRUEPV_Y/D")
    tree.Branch("Jpsi_TRUEPV_Z",Jpsi_TRUEPV_Z,"Jpsi_TRUEPV_Z/D")
    tree.Branch("Jpsi_TRUE_Tz",Jpsi_TRUE_Tz,"Jpsi_TRUE_Tz/D")

    
    
    nEntries = nt.GetEntries()

    for iEn in range(nEntries):
        
        nt.GetEntry(iEn)
        #Jpsi_Tz[0] = (3.3* (Jpsi_ENDVERTEX_Z[0] - Jpsi_OWNPV_Z[0])*Jpsi_MM[0]/Jpsi_PZ[0])
        #Jpsi_TRUEM[0] = TMath.Sqrt(Jpsi_TRUEP_E[0]*Jpsi_TRUEP_E[0] - (Jpsi_TRUEP_X[0]*Jpsi_TRUEP_X[0] + Jpsi_TRUEP_Y[0]*Jpsi_TRUEP_Y[0] + Jpsi_TRUEP_Z[0]*Jpsi_TRUEP_Z[0]))
        #Jpsi_M_res[0] = Jpsi_M[0] - Jpsi_TRUEM[0]
        
        
#        std.cout<<Jpsi_MC_MOTHER_ID<<"   "<<Jpsi_MC_GD_MOTHER_ID<<"   "<<Jpsi_MC_GD_GD_MOTHER_ID<<"   "<<std.endl 
        if (Jpsi_MC_MOTHER_ID[0] in dirCgarmID ):

            prompt[0] = True
            sec[0] = False
            Jpsi_TRUEPV_X[0] = Jpsi_TRUEENDVERTEX_X[0]
            Jpsi_TRUEPV_Y[0] = Jpsi_TRUEENDVERTEX_Y[0]
            Jpsi_TRUEPV_Z[0] = Jpsi_TRUEENDVERTEX_Z[0]

        elif (Jpsi_MC_MOTHER_ID[0] in charmID):
        
            if (((Jpsi_MC_GD_MOTHER_ID[0]) in dirCgarmID)  or ((Jpsi_MC_GD_MOTHER_ID[0] in charmID) and ((Jpsi_MC_GD_GD_MOTHER_ID[0] in dirCgarmID) or (Jpsi_MC_GD_GD_MOTHER_ID[0] in charmID)))):
        
                prompt[0] = True
                sec[0] = False
                Jpsi_TRUEPV_X[0] = Jpsi_MC_MOTHER_TRUEV_X[0]
                Jpsi_TRUEPV_Y[0] = Jpsi_MC_MOTHER_TRUEV_Y[0]
                Jpsi_TRUEPV_Z[0] = Jpsi_MC_MOTHER_TRUEV_Z[0]

            #elif ( Jpsi_MC_GD_MOTHER_ID[0] in charmID and Jpsi_MC_GD_GD_MOTHER_ID[0] in charmID and Jpsi_MC_GD_GD_GD_MOTHER_ID[0] in bHadrID ):

                #prompt[0] = False
                #sec[0] = True
                #Jpsi_TRUEPV_X[0] = Jpsi_MC_GD_GD_GD_MOTHER_TRUEV_X[0]
                #Jpsi_TRUEPV_Y[0] = Jpsi_MC_GD_GD_GD_MOTHER_TRUEV_Y[0]
                #Jpsi_TRUEPV_Z[0] = Jpsi_MC_GD_GD_GD_MOTHER_TRUEV_Z[0]

            elif ( Jpsi_MC_GD_MOTHER_ID[0] in charmID and Jpsi_MC_GD_GD_MOTHER_ID[0] in bHadrID ):

                prompt[0] = False
                sec[0] = True
                Jpsi_TRUEPV_X[0] = Jpsi_MC_GD_GD_GD_MOTHER_TRUEV_X[0]
                Jpsi_TRUEPV_Y[0] = Jpsi_MC_GD_GD_GD_MOTHER_TRUEV_Y[0]
                Jpsi_TRUEPV_Z[0] = Jpsi_MC_GD_GD_GD_MOTHER_TRUEV_Z[0]

            else:
                if( Jpsi_MC_GD_MOTHER_ID[0] in bHadrID):
                    prompt[0] = False
                    sec[0] = True
                    Jpsi_TRUEPV_X[0] = Jpsi_MC_GD_GD_MOTHER_TRUEV_X[0]
                    Jpsi_TRUEPV_Y[0] = Jpsi_MC_GD_GD_MOTHER_TRUEV_Y[0]
                    Jpsi_TRUEPV_Z[0] = Jpsi_MC_GD_GD_MOTHER_TRUEV_Z[0]
                else:
                    print iEn
                    print Jpsi_MC_MOTHER_ID[0], Jpsi_MC_GD_MOTHER_ID[0], Jpsi_MC_GD_GD_MOTHER_ID[0]

        else:

            if( Jpsi_MC_MOTHER_ID[0] in bHadrID):

                prompt[0] = False
                sec[0] = True
                Jpsi_TRUEPV_X[0] = Jpsi_MC_GD_MOTHER_TRUEV_X[0]
                Jpsi_TRUEPV_Y[0] = Jpsi_MC_GD_MOTHER_TRUEV_Y[0]
                Jpsi_TRUEPV_Z[0] = Jpsi_MC_GD_MOTHER_TRUEV_Z[0]
            else:
                print iEn
                print Jpsi_MC_MOTHER_ID[0], Jpsi_MC_GD_MOTHER_ID[0], Jpsi_MC_GD_GD_MOTHER_ID[0]
                
        #Dist_CHI2 = ( TMath.Abs(float(Jpsi_TRUEPV_X[0])-PVX[0])/(PVXERR[0]) + TMath.Abs(float(Jpsi_TRUEPV_Y[0])-PVY[0])/(PVYERR[0]) + TMath.Abs(float(Jpsi_TRUEPV_Z[0])-PVZ[0])/(PVZERR[0]))
        #Jpsi_Dist_CHI2[0] = Dist_CHI2
        Jpsi_TRUE_Tz[0] = (3.3* (Jpsi_TRUEENDVERTEX_Z[0] - Jpsi_TRUEPV_Z[0])*Jpsi_TRUEM[0]/Jpsi_TRUEP_Z[0])
        #Jpsi_TRUE_Tz[0] = Jpsi_TRUE_Tz[0]
        #Jpsi_Tz_res[0] = Jpsi_Tz[0] - Jpsi_TRUE_Tz[0]
        if (( ProtonP_TRUEP_X[0]**2+ProtonP_TRUEP_Y[0]**2+ProtonP_TRUEP_Z[0]**2)**0.5>12.5e3  and (ProtonM_TRUEP_X[0]**2+ProtonM_TRUEP_Y[0]**2+ProtonM_TRUEP_Z[0]**2)**0.5>12.5e3 and (ProtonP_TRUEPT[0]>2000) and (ProtonM_TRUEPT[0]>2000) and (ProtonP_TRUEPT[0]/(ProtonP_TRUEP_X[0]**2+ProtonP_TRUEP_Y[0]**2+ProtonP_TRUEP_Z[0]**2)**0.5>0.0366  and (ProtonM_TRUEPT[0]/(ProtonM_TRUEP_X[0]**2+ProtonM_TRUEP_Y[0]**2+ProtonM_TRUEP_Z[0]**2)**0.5>0.0366)  and (Jpsi_TRUEETA[0]>2 and Jpsi_TRUEETA[0]<4.5))):
            tree.Fill()

        #print Jpsi_Dist_CHI2[0], PVXERR[0], PVYERR[0], PVZERR[0]


    c = TCanvas("Signal_Fit","Signal Fit",800,500)
    gPad.SetLeftMargin(0.15) 

    tree.Draw("Jpsi_Dist_CHI2")
    #c.SaveAs("%s_Dist_CHI2_%s_%s.png"%(charm,year,add))
    tree.Write()
    
    f.Close()


charm = ['Etac','Jpsi']
#charm = 'Etac'
year = ['2015','2016']
#year = '2016'
add = ''

for c in charm:
    for y in year:
        selectGen(c, y, add)

add = 'incl_b'

for y in year:
    selectGen(charm[0], y, add)

#add = '2pppi0'

#for y in year:
    #select(charm[1], y, add)
#select(charm, year, add)
