
input_tree_name = 'Jpsi2ppTuple/DecayTree'
merged_filepath_prefix = '/eos/user/a/ausachov/DataRunII_ppbar/'

import os, sys
import ROOT

sys.path.insert(0, './')
from makeConfigDictionary import *

def merge_root_output(job, input_tree_name, merged_filepath_prefix, cutType):
    # Treat a job with subjobs the same as a job with no subjobs
    JOBS = job.subjobs
    print "treating job ", job.id
    if len(JOBS) == 0:
        JOBS = [job]

    access_urls = []
    for j in JOBS:
        if j.status != 'completed':
            print 'Skipping #{0}'.format(j.id)
            continue
        # THIS WAS FOR NORMAL JOBS ACCORDING SECOND ANALYSIS STEPS?????
        #for df in j.outputfiles.get(DiracFile):
        #    access_urls.append(df.accessURL())
        location = j.outputfiles[0].localDir+'/Tuple.root'
        access_urls.append(location)

    tchain = ROOT.TChain(input_tree_name)
    for url in access_urls:
        tchain.Add(url)
#        print tchain.GetEntries()
    merged_filepath = merged_filepath_prefix + str(job.id)
    try:
        os.system('mkdir '+merged_filepath)
    except:
        pass
#    tchain.Merge(merged_filepath+"/mergedTuple.root")
#    tchain.Add(merged_filepath+"/mergedTuple.root")

    newfile = ROOT.TFile(merged_filepath+"/mergedTuple.root","recreate")


    newtree = ROOT.TTree()
    newtree.SetMaxTreeSize(500000000)
    if cutType=="Prompt16":
       cut = cutApriori2016_Dict["DiProton"]
    elif cutType=="Prompt15":
       cut = cutApriori2015_Dict["DiProton"]
    else:
       cut = ""

    newtree = tchain.CopyTree(cut)

    newtree.Print()

    newtree.GetCurrentFile().Write() 
    newtree.GetCurrentFile().Close()


    del tchain, newtree

merge_root_output(job,input_tree_name, merged_filepath_prefix, "default")


