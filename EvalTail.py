from ROOT import *
from array import array

homeDir = "/users/LHCb/zhovkovska/scripts/"
expDir = "/exp/LHCb/zhovkovska/"
#def EvalTail()
#{

#    RooRealVar Jpsi_Tz("Jpsi_Tz","Jpsi_Tz",-10.,10.) 
#    Jpsi_Tz.setBins(100)


#    nt=TChain("DecayTree")
#    nt.Add("Reduced_Snd_2012.root")



#    RooDataSet ds_tail("ds_tail","ds_tail",RooArgSet(Jpsi_Tz),StoreError(RooArgSet(Jpsi_Tz)))
#    const Int_t nEntries = nt.GetEntries()
#    Jpsi_ENDVERTEX_Z
#    Jpsi_OWNPV_Z
#    Jpsi_MM
#    Jpsi_PZ
#    nt.SetBranchAddress("Jpsi_ENDVERTEX_Z",&Jpsi_ENDVERTEX_Z)
#    nt.SetBranchAddress("Jpsi_OWNPV_Z",&Jpsi_OWNPV_Z)
#    nt.SetBranchAddress("Jpsi_MM",&Jpsi_MM)
#    nt.SetBranchAddress("Jpsi_PZ",&Jpsi_PZ)

#    for(Int_t iEn=1 iEn<=nEntries iEn++)
#    {

#        nt.GetEntry((iEn+1)%nEntries)
#        Jpsi_OWNPV_Z_next = Jpsi_OWNPV_Z
#        nt.GetEntry(iEn)
#        if(TMath::Abs(3.3* (Jpsi_ENDVERTEX_Z - Jpsi_OWNPV_Z_next)*Jpsi_MM/Jpsi_PZ) <= 10.)
#        {    Jpsi_Tz = (3.3* (Jpsi_ENDVERTEX_Z - Jpsi_OWNPV_Z_next)*Jpsi_MM/Jpsi_PZ)
#            Jpsi_Tz.setRange(-10.,10.)
#            ds_tail.add(RooArgSet(Jpsi_Tz)) 
#        }
#    }



##    TFile f("test.root","RECREATE")
##    tree.Write()
##    f.Close()
#    ds_tail.Print()


#    RooKeysPdf kest("kest","kestPdf",Jpsi_Tz,ds_tail,RooKeysPdf::MirrorBoth)

#    RooPlot *frame = Jpsi_Tz.frame(Title("KeyPdf")) 
#    ds_tail.plotOn(frame)
#    kest.plotOn(frame,LineColor(2))

#     c = TCanvas("Signal_Fit","Signal Fit",800,500)
#     gPad.SetLeftMargin(0.15)  frame.GetXaxis().SetTitle("t_{z} [ps]")  frame.Draw()
##     c.SetLogy()
##     frame.SetMinimum(1.)

#     Jpsi_Tz = 5.
#     print kest.getValV(/*RooArgSet(Jpsi_Tz)*/)<<std::endl


#}



def EvalTail():

    gStyle.SetOptStat(0)

    nt = TChain("DecayTree")
#    nt.Add("CharmSelect_BMes.root")
    nt.Add(homeDir+"Reduced_Snd_2012.root")


    nEntries = nt.GetEntries()
    Jpsi_ENDVERTEX_Z = array("d",[0])
    Jpsi_OWNPV_Z = array("d",[0])
    Jpsi_MM = array("d",[0])
    Jpsi_PZ = array("d",[0])
#    Bool_t prompt
#    Bool_t sec
    nt.SetBranchAddress("Jpsi_ENDVERTEX_Z",Jpsi_ENDVERTEX_Z)
    nt.SetBranchAddress("Jpsi_OWNPV_Z",Jpsi_OWNPV_Z)
    nt.SetBranchAddress("Jpsi_MM",Jpsi_MM)
    nt.SetBranchAddress("Jpsi_PZ",Jpsi_PZ)
#    nt.SetBranchAddress("Jpsi_prompt",prompt)
#    nt.SetBranchAddress("Jpsi_sec",sec)

    tz = array("d",nEntries*[0])
    Jpsi_OWNPV_Z_next = array("d",[0])
    hTail = TH1D("hTail","hTail",100,-10.,10.)
    hTailPr = TH1D("hTailPr","hTailPr",100,-10.,10.)
    for iEn in range(nEntries):

        nt.GetEntry((iEn+1)/nEntries)
        Jpsi_OWNPV_Z_next = Jpsi_OWNPV_Z
        nt.GetEntry(iEn)
        tz[iEn] = (3.3* (Jpsi_ENDVERTEX_Z - Jpsi_OWNPV_Z_next)*Jpsi_MM/Jpsi_PZ)
        hTail.Fill(tz[iEn])


#    f  = TFile("evalTail.root","RECREATE")

    kdeTail = TKDE(nEntries, tz[0], -10., 10., "kerneltype:gaussian", 3.0)
    f = kdeTail.GetFunction(1000)

#    hTail.Scale(1/hTail.Integral(1,100)/hTail.GetBinWidth(1))
#    hTail.Scale(1/hTail.GetEntries()/hTail.GetBinWidth(1))
    hTail.Scale(1/hTail.Integral(hTail.FindBin(-10.0),hTail.FindBin(10.0))/hTail.GetBinWidth(1))
#    hTailPr.Scale(1/hTailPr.Integral(hTailPr.FindBin(-10.),hTailPr.FindBin(10.))/hTailPr.GetBinWidth(1))
#    hTailPr.Fit(f)

    c = TCanvas("TailEval","Tail Evaluation",800,500)
#    c.cd()

    hTail.Draw()
    hTail.GetXaxis().SetTitle("t_{z} [ps]")
    f.Draw("same")
#    hTailPr.SetLineColor(kRed)
#    hTailPr.DrawClone("SAME")

#    print "nSec"<<hTailPr.Integral(hTailPr.FindBin(-10.),hTailPr.FindBin(10.))<<std::endl
#    kdeTail.Draw("SAME")
#    kdeTail.SaveAs("evalTail.root")
#    f.Write("kdeTail")
#    f.Close()
    leg = TLegend(0.7, 0.65, 0.85, 0.85)
    leg.AddEntry(hTail,"MC Mismatch","lep")
    leg.AddEntry(f,"Mismatch evaluation","l")
    leg.SetBorderSize(0)
    leg.Draw()


def ReadEval():

    f  = TFile(homeDir+"evalTail.root","READ")
#    kdeTail = (TKDE*)((TKDE*)f.Get("kdeTail")).Clone()
    kdeTail = TKDE()
    kdeTail.Read("")
    kdeTail.DrawClone()
    f.Close()

def evalTail(nPT=0):

  gStyle.SetOptTitle(0)
  gStyle.SetOptFit(0)
  gStyle.SetOptStat(0)

  ptBins = [6500., 8000., 10000., 12000., 14000.]  
  nt=TChain("DecayTree")
#   nt.Add("MC/Etac_MC_Tz_Tail.root")
#   nt.Add("MC/Jpsi_MC_Tz_Tail.root")

  #nt.Add(expDir+"Reduced_Mismatch.root")
  nt.Add(homeDir+"Reduced_Mismatch.root")
  
  if nPT == 0:
    ntCuted = nt.CopyTree("otz>=-10.0 && otz<=10.0")
  else:
    ptName = "%s < p_{T} < %s GeV/c"%(ptBins[nPT-1]/1000.,ptBins[nPT]/1000.)  
    print "otz>=-10.0 && otz<=10.0 && Jpsi_PT>%s && Jpsi_PT<%s"%(ptBins[nPT-1]/1000.,ptBins[nPT]/1000.)  
    ntCuted = nt.CopyTree("otz>=-10.0 && otz<=10.0 && Jpsi_PT>%s && Jpsi_PT<%s"%(ptBins[nPT-1],ptBins[nPT]))
      
  print "Tail tree: OK \n"
  
  nEntries = ntCuted.GetEntries()
  nEntriesUsed = nEntries
  Jpsi_Tz = array("d",[0])
  ntCuted.SetBranchAddress("otz",Jpsi_Tz)
  
  tz = array("d",nEntriesUsed*[0])
  hTail = TH1D("hTail","h",100, -10.0, 10.0)

  for iEn in range(nEntriesUsed):

    ntCuted.GetEntry(iEn)
    tz[iEn] = Jpsi_Tz[0]
    hTail.Fill(tz[iEn])

  
  
  print "DataSet fill status: OK \n"

  
  kdeTail = TKDE(nEntriesUsed, tz, -10., 10., "kerneltype:gaussian", 3.0)
#   fTail = kdeTail.GetFunction(1000, -10., 10.)
  fTail  = TF1("fTail",kdeTail,-10.0,10.0,0)
  
  print "KeysPdf fill status: OK\n"
  
  c = TCanvas("Masses_Fit","Masses Fit",800,500)
  hTail.Scale(1/hTail.Integral(hTail.FindBin(-10.0),hTail.FindBin(10.0))/hTail.GetBinWidth(1)*fTail.Integral(-10.0,10.0))
#   hTail.Scale(1/hTail.Integral(1,100)/hTail.GetBinWidth(1))
#   hTail.Scale(1/hTail.GetEntries()/hTail.GetBinWidth(1))

  print hTail.Integral(hTail.FindBin(-10.0),hTail.FindBin(10.0)), "\n"
  print fTail.Integral(-10.0,10.0), "\n"
  print fTail.GetXmin(), "\n"
  print fTail.GetXmax(), "\n"
  print fTail.Integral(fTail.GetXmin(),fTail.GetXmax()), "\n"
  
  texMC = TLatex()
  texMC.SetNDC()
  
  hTail.Draw()
  hTail.GetXaxis().SetTitle("t_{z}, ps")
  hTail.SetMinimum(0.)
  hTail.SetMaximum(0.125)
  fTail.Draw("same")
  fTail.SetLineColor(2)
  fTail.SetNpx(5000)
  texMC.DrawLatex(0.18, 0.85, "LHCb simulation")
  texMC.DrawLatex(0.18, 0.79, "#sqrt{s} = 13 TeV")
  if nPT!=0:
    texMC.DrawLatex(0.18, 0.73, ptName)
      
  c.SaveAs("Mismatch_PT%s.pdf"%(nPT))
  
  fOut = TFile(homeDir+"Mismatch_func_PT%s.root"%(nPT),"RECREATE")
  fTail.Write("fTail")
  fOut.Close()
  
gROOT.LoadMacro("lhcbStyle.C")  
evalTail(1)  
