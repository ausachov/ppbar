myJobName = 'Jpsi2ppDetached_R17S29_MagUp_2017'

# myApplication = prepareGaudiExec('DaVinci','v42r6p1', myPath='$HOME/cmtuser/')
myApplication = GaudiExec()
myApplication.directory = "$HOME/cmtuser/DaVinciDev_v42r6p1"
myApplication.platform='x86_64-slc6-gcc49-opt'
myApplication.options = [ 'DaVinci_Jpsi2pp_Detached.py',
                          'DB_R17S29.py'
                          ]

data  = BKQuery('/LHCb/Collision17/Beam6500GeV-VeloClosed-MagUp/Real Data/Reco17/Stripping29/90000000/CHARMCOMPLETEEVENT.DST', dqflag=['OK']).getDataset()

validData = LHCbDataset(files=['LFN:'+lfn for lfn, rep in data.getReplicas().iteritems() if len(rep)])

mySplitter = SplitByFiles( filesPerJob = 10, maxFiles = -1, ignoremissing = True, bulksubmit = False )

myBackend = Dirac()
j = Job (
         name         = myJobName,
         application  = myApplication,
         splitter     = mySplitter,
         outputfiles  = [ DiracFile('Tuple.root'),
                         DiracFile('DVHistos.root')
                         ],
         backend      = myBackend,
         inputdata    = validData,
         do_auto_resubmit = True,
         parallel_submit = True
         )
j.submit(keep_going=True, keep_on_fail=True)

