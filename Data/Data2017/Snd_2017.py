from ROOT import gROOT, TChain, TCut, TFile, TTree, TProof

chain = TChain("Jpsi2ppTuple/DecayTree")

sjMax=504
for i in range(sjMax):
    chain.Add("/eos/user/a/ausachov/Data2017_ppbar/478/"+str(i)+"/Tuple.root")

for i in range(sjMax):
    chain.Add("/eos/user/a/ausachov/Data2017_ppbar/479/"+str(i)+"/Tuple.root")

for i in range(sjMax):
    chain.Add("/eos/user/a/ausachov/Data2017_ppbar/480/"+str(i)+"/Tuple.root")

for i in range(sjMax):
    chain.Add("/eos/user/a/ausachov/Data2017_ppbar/482/"+str(i)+"/Tuple.root")

import sys
sys.path.insert(0, '../')
from makeConfigDictionary import cutsAprioriDetached
totCut = TCut(cutsAprioriDetached)

newfile = TFile("/eos/user/a/ausachov/Data2017_ppbar/Snd/Reduced_Snd_2017.root","recreate")

newtree=TTree()
newtree.SetMaxTreeSize(500000000)
newtree = chain.CopyTree(totCut.GetTitle())

newtree.Print()

newtree.GetCurrentFile().Write() 
newtree.GetCurrentFile().Close()
