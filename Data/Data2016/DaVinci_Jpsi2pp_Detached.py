def fillTuple( tuple, myTriggerList ):
        
    tuple.ToolList = ["TupleToolAngles",
                      "TupleToolEventInfo",
                      "TupleToolGeometry",
                      "TupleToolKinematic",
                      "TupleToolPid",
                      "TupleToolPrimaries",
                      "TupleToolTrackInfo",
                      "TupleToolRecoStats"
                     ]
    tuple.TupleToolRecoStats.Verbose=True

    from Configurables import TupleToolTISTOS, TupleToolDecay
    tuple.addTupleTool(TupleToolDecay, name = 'Jpsi')

    tuple.Jpsi.ToolList+=[ "TupleToolTISTOS/TupleToolTISTOSForJpsi" ]
    tuple.Jpsi.addTool(TupleToolTISTOS, name="TupleToolTISTOSForJpsi" )
    tuple.Jpsi.TupleToolTISTOSForJpsi.Verbose=True
    tuple.Jpsi.TupleToolTISTOSForJpsi.TriggerList = myTriggerList

#    from Configurables import TupleToolDecayTreeFitter
#    tuple.Jpsi.ToolList +=  ["TupleToolDecayTreeFitter/PVFit"]        # fit with both PV and mass constraint
#    tuple.Jpsi.addTool(TupleToolDecayTreeFitter("PVFit"))
#    tuple.Jpsi.PVFit.Verbose = True
#    tuple.Jpsi.PVFit.constrainToOriginVertex = True
#    tuple.Jpsi.PVFit.daughtersToConstrain = []


    #LoKi one
    from Configurables import LoKi__Hybrid__TupleTool
    LoKi_Jpsi=LoKi__Hybrid__TupleTool("LoKi_Jpsi")
    LoKi_Jpsi.Variables = {
      "LOKI_FDCHI2"          : "BPVVDCHI2",
      "LOKI_FDS"             : "BPVDLS",
      "LOKI_DIRA"            : "BPVDIRA",
      "LOKI_BPVCORRM"        : "BPVCORRM",
      "m_scaled"             : "DTF_FUN ( M , False )",
      "DOCA"                 : "DOCA(1,2)",
      "m_pv"     : "DTF_FUN ( M , True )",
      "c2dtf_1"  : "DTF_CHI2NDOF( False )" ,
      "c2dtf_2"  : "DTF_CHI2NDOF( True  )"
    }
    tuple.Jpsi.addTupleTool(LoKi_Jpsi)


    #LoKi one
    from Configurables import LoKi__Hybrid__TupleTool
    LoKi_All=LoKi__Hybrid__TupleTool("LoKi_All")
    LoKi_All.Variables = {
        "ETA"                  : "ETA",
        "Y"                    : "Y"  ,
        "LOKI_IPCHI2"          : "BPVIPCHI2()"
        }
    tuple.addTool(LoKi_All)
    
myTriggerList = [
                 # L0
                 "L0ElectronDecision",
                 "L0PhotonDecision",
                 "L0HadronDecision",
                 # L0 Muon
                 "L0MuonDecision",
                 "L0MuonHighDecision",
                 "L0DiMuonDecision",
                 
                 
                 
                 # Hlt1 track
                 "Hlt1B2PhiPhi_LTUNBDecision",
                 "Hlt1TrackMuonDecision",
                 "Hlt1TrackPhotonDecision",
                 
                 "Hlt1TrackAllL0Decision",
                 "Hlt1TrackMVADecision",
                 "Hlt1TrackMVALooseDecision",
                 "Hlt1TwoTrackMVADecision",
                 "Hlt1TwoTrackMVALooseDecision"
                 "Hlt1B2HH_LTUNB_KPiDecision",
                 "Hlt1B2HH_LTUNB_KKDecision",
                 "Hlt1B2HH_LTUNB_PiPiDecision",
                 "Hlt1IncPhiDecision",
                 "Hlt1DiProtonDecision",
                 "Hlt1DiProtonLowMultDecision",
                 "Hlt1LowMultVeloCut_HadronsDecision",
                 "Hlt1LowMultPassThroughDecision",
                 
                 
                 
                 # Hlt2 Topo
                 "Hlt2PhiIncPhiDecision",
                 "Hlt2PhiPromptPhi2EETurboDecision",
                 "Hlt2PhiBs2PhiPhiDecision",
                 "Hlt2B2HH_B2HHDecision",
                 "Hlt2CcDiHadronDiProtonDecision",
                 "Hlt2DiProtonDecision",
                 "Hlt2CcDiHadronDiProtonLowMultDecision",
                 "Hlt2CcDiHadronDiPhiDecision",
                 "Hlt2PhiIncPhiDecision",
                 "Hlt2PhiBs2PhiPhiDecision",
                 
                 "Hlt2Topo2BodyDecision",
                 "Hlt2Topo3BodyDecision",
                 "Hlt2Topo4BodyDecision"
                 ]







year = "2016"
Jpsi2ppDecay = "J/psi(1S) -> ^p+ ^p~-"
Jpsi2ppBranches = {
     "ProtonP"  :  "J/psi(1S) ->^p+ p~-"
    ,"ProtonM"  :  "J/psi(1S) -> p+^p~-"
    ,"Jpsi"     : "(J/psi(1S) -> p+ p~-)"
}

Jpsi2ppLocation = "/Event/CharmCompleteEvent/Phys/Ccbar2PpbarDetachedLine/Particles"
from PhysConf.Selections import AutomaticData, MomentumScaling, TupleSelection
inputData = AutomaticData(Jpsi2ppLocation) 
inputData = MomentumScaling(inputData, Year = year)

Jpsi2ppTuple = TupleSelection("Jpsi2ppTuple", inputData, Decay = Jpsi2ppDecay, Branches = Jpsi2ppBranches)
fillTuple( Jpsi2ppTuple, myTriggerList )

# from Configurables import EventNodeKiller
# eventNodeKiller = EventNodeKiller('DAQkiller')
# eventNodeKiller.Nodes = ['/Event/DAQ',
#                          '/Event/pRec']


"""
Event-level filters
"""
from PhysConf.Filters import LoKi_Filters
Jpsi2ppFilters = LoKi_Filters (
    STRIP_Code = "HLT_PASS('StrippingCcbar2PpbarDetachedLineDecision')"
    )



from Configurables import DaVinci, CondDB
CondDB ( LatestGlobalTagByDataType = year )
DaVinci().EventPreFilters = Jpsi2ppFilters.filters('Jpsi2ppFilters')
DaVinci().EvtMax = -1                         # Number of events
DaVinci().SkipEvents = 0                       # Events to skip
DaVinci().PrintFreq = 1000
DaVinci().DataType = year
DaVinci().Simulation    = False
DaVinci().HistogramFile = "DVHistos.root"      # Histogram file
DaVinci().TupleFile = "Tuple.root"             # Ntuple
DaVinci().UserAlgorithms = [Jpsi2ppTuple]        # The algorithms
# MDST
DaVinci().InputType = "DST"

DaVinci().Lumi = True

DaVinci().DDDBtag   = "dddb-20150724"
#DaVinci().CondDBtag = "cond-20161004"


from Configurables import MessageSvc
MessageSvc().setWarning = [ 'RFileCnv' ]

#from GaudiConf import IOHelper
## Use the local input data
#IOHelper().inputFiles([
#                       '00069595_00000016_1.charm.mdst'
#                       ], clear=True)



