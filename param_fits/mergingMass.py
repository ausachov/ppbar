from ROOT import *
from array import array
gROOT.LoadMacro("../libRooRelBreitWigner/RooRelBreitWigner.cxx+");
homeDir = "/users/LHCb/zhovkovska/scripts/"


def process(int iPTBin):

    gROOT.Reset();

#     const Int_t nConf = 9;
#     const Int_t nTzBins = 5;
    const Int_t nConf = 10;
    const Int_t nTzBins = 5;
    

    char nameInFile[50];
    char nameOutFile[50];
    char nameInTxt[50];
    char nameOutTxt[50];
    char nameInWsp[50];
    char nameOutWsp[50];

    if (iPTBin==0):

        sprintf(nameOutFile,"TotFitResult/Jpsi_Fit.root");
        sprintf(nameOutTxt,"TotFitResult/systTableMass.txt");
        sprintf(nameOutWsp,"TotFitResult/Wksp.root");

    else:
#         sprintf(nameOutFile,"JpsiFitResults/Jpsi_Fit_PT%d.root", iPTBin);
#         sprintf(nameOutTxt, "JpsiFitResults/systTableMass_PT%d.txt", iPTBin);
#         sprintf(nameOutWsp, "JpsiFitResults/Wksp_PT%d.root", iPTBin);
        sprintf(nameOutFile,"EtacFitResults/Jpsi_Fit_PT%d.root", iPTBin);
        sprintf(nameOutTxt, "EtacFitResults/systTableMass_PT%d.txt", iPTBin);
        sprintf(nameOutWsp, "EtacFitResults/Wksp_PT%d.root", iPTBin);



    std::fstream fi;
    std::fstream fo;

    fo = open(nameOutTxt,"w");
    TFile *fOut = new TFile(nameOutFile,"RECREATE");
    TFile *fOutWsp = new TFile(nameOutWsp,"RECREATE");


    TCanvas *c;
    RooWorkspace *w;

    TDirectory *cdtof = fOut.mkdir("TzBins");
    cdtof.cd();

      # create a new subdirectory for each plane
    char dirname[50];
    TDirectory *cdplane[nTzBins];
    for(Int_t iTz=1; iTz<=nTzBins; iTz++)
    {
        sprintf(dirname,"TzBin%d",iTz);
        cdplane[iTz] = cdtof.mkdir(dirname);
        cdplane[iTz].cd();

        for (Int_t iConf=0; iConf<nConf; iConf++ )
        {
            if (iPTBin == 0)
            {
                sprintf(nameInFile,"TotFitResult/systUncert/Jpsi_Fit_dT_%d_%d.root",iTz, iConf);
            }
            else
            {
#                 sprintf(nameInFile,"JpsiFitResults/systUncert/Jpsi_Fit_PT%d_%d_%d.root",iPTBin, iTz, iConf);
                sprintf(nameInFile,"EtacFitResults/systUncert/Jpsi_Fit_PT%d_%d_%d.root",iPTBin, iTz, iConf);
            }
            TFile *fIn = new TFile(nameInFile,"READ");
            c = (TCanvas*)fIn.Get("Masses_Fit");
#            TFile *fInWsp = new TFile(nameInWsp,"READ");
#            w = (RooWorkspace*) fInWsp.Get("w");
            cdplane[iTz].cd();
            c.Write();
#            w.Write();
            fOut.Write();
            fIn.Close();
#            fInWsp.Close();
        }
    }


#    fOut.Write();
    delete c;

    fOut.Close();
    std::cout<<"OK1"<<std::endl;



    string s;
    Double_t chi2 = 0;
    Double_t chi2Jpsi = 0;
    Double_t chi2Etac = 0;

    Double_t NJpsi[nConf], NEtac[nConf], NBkg[nConf];
    Double_t NJpsiErr[nConf], NEtacErr[nConf], NBkgErr[nConf];

    cdtof = fOutWsp.mkdir("TzBins");
    cdtof.cd();


    for(Int_t iTz=1; iTz<=nTzBins; iTz++)
    {
        sprintf(dirname,"TzBin%d",iTz);
        cdplane[iTz] = cdtof.mkdir(dirname);
        cdplane[iTz].cd();
        Double_t NJpsiErrSyst = 0, NEtacErrSyst = 0, NBkgErrSyst = 0;

        std::cout<<"OK1."<<iTz<<std::endl;

        fo<<"\\begin{frame}"<<std::endl;
        fo<<"\\frametitle{Tz Bin "<<iTz<<"}"<<std::endl<<std::endl;

        fo<<"  \\begin{table}[H]"<<std::endl;
        fo<<"  \t \\begin{tabular*}{0.9\\linewidth}{@{\\extracolsep{\\fill}}c|c|c|c}"<<std::endl;
        fo<<"  & $N_{J/\\psi}$ & $N_{\\eta_c}$ & $N_{bkg}$  \\\\ \\hline"<<std::endl;

        for (Int_t iConf=0; iConf<nConf; iConf++ )
        {
            if (iPTBin == 0)
                sprintf(nameInWsp,"TotFitResult/systUncert/Reduced_Pmt_2016_wksp_dT_%d_%d.root",iTz, iConf);
            else
#                 sprintf(nameInWsp,"JpsiFitResults/systUncert/Reduced_Pmt_2016_wksp_PT%d_%d_%d.root",iPTBin, iTz, iConf);
                sprintf(nameInWsp,"EtacFitResults/systUncert/Reduced_Pmt_2016_wksp_PT%d_%d_%d.root",iPTBin, iTz, iConf);
            
            TFile *fInWsp = new TFile(nameInWsp,"READ");
            w = (RooWorkspace*) fInWsp.Get("w");
            cdplane[iTz].cd();
            w.Write();
            fOutWsp.Write();
            fInWsp.Close();


            NJpsi[iConf] = w.var("nJpsi").getValV();
            NJpsiErr[iConf] = w.var("nJpsi").getError();
            NEtac[iConf] = w.var("nEta").getValV();
            NEtacErr[iConf] = w.var("nEta").getError();
            NBkg[iConf] = w.var("nBckgr").getValV();
            NBkgErr[iConf] = w.var("nBckgr").getError();


            if(iConf == 0)
            {
                fo<<" & "<<NJpsi[iConf]<<" & "<<NEtac[iConf]<<" & "<<NBkg[iConf];
                fo<<" \\\\ \\hline"<<std::endl;
                fo<<"Stat uncert & "<<NJpsiErr[iConf]<<" & "<<NEtacErr[iConf]<<" & "<<NBkgErr[iConf];
                fo<<" \\\\ \\hline"<<std::endl;

            }
            else
            {
                fo<<" & "<<NJpsi[iConf]-NJpsi[0]<<" & "<<NEtac[iConf]-NEtac[0]<<" & "<<NBkg[iConf]-NBkg[0];
                fo<<" \\\\ "<<std::endl;
                if (iConf != 4 && iConf != 8)
                {
                    NJpsiErrSyst+=TMath::Sq(NJpsi[iConf]-NJpsi[0]);
                    NEtacErrSyst+=TMath::Sq(NEtac[iConf]-NEtac[0]);
                    NBkgErrSyst+=TMath::Sq(NBkg[iConf]-NBkg[0]);

                }
            }
            fi.close();

        }
        fo<<" \\hline"<<std::endl;
        fo<<"Tot Syst &"<< TMath::Sqrt(NJpsiErrSyst)<<" & "<< TMath::Sqrt(NEtacErrSyst)<<" & "<<TMath::Sqrt(NBkgErrSyst);
        fo<<" \\\\"<<std::endl;
        fo<<" \t \\end{tabular*}"<<std::endl;
        fo<<"  \\end{table}"<<std::endl;
        fo<<"\\end{frame}"<<std::endl;
    }


    fo.close();
    std::cout<<"OK2"<<std::endl;
    fOutWsp.Close();

    delete fOut;
    delete fOutWsp;
    delete w;

}


void mergingMass()
{
#     process(0);
    Int_t nPTBins = 5;
    for (int iPTBin=1; iPTBin<nPTBins+1; iPTBin++)
    {
        process(iPTBin);
    }
}
