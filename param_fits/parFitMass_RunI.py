from ROOT import *
from array import array
import numpy as np

gROOT.LoadMacro("../libRooRelBreitWigner/RooRelBreitWigner.cxx+");
gROOT.LoadMacro("../BifurcatedCB/BifurcatedCB.cxx+");

def mass_pars(gauss=True):
  
    gROOT.ProcessLine('gStyle->SetOptStat(000000000)')
    gROOT.ProcessLine('gStyle->SetOptTitle(0)')

    nPTBins = 4
        
    if gauss: add = ""
    else: add = "_CB"
    
    pt = array( 'f',[6.5, 8.0, 10.0, 12.0, 14.])
    ptMax = 14.0
    ptMin = 6.5


    f = TFile("../resolutions/resoTzCut_log/MC_MassResolution%s_Integr_wksp.root"%(add),"READ") 
    wMC = f.Get("w")
    f.Close()
    
    sigma = wMC.var("sigma_eta_1").getValV()
    relEtac2Jpsi = wMC.var("rEtaToJpsi").getValV()
    relG12G2 = wMC.var("rG1toG2").getValV()
    relN2W = wMC.var("rNarToW").getValV()

    f_sigma_mean = TF1("f_sigma_mean","pol0", ptMin, ptMax)
    f_sigma_mean.SetParameter(0, sigma)
    f_sigma_mean.SetLineColor(2)
    f_rel_mean = TF1("f_rel_mean","pol0", ptMin, ptMax)
    f_rel_mean.SetParameter(0, relEtac2Jpsi)
    f_rel_mean.SetLineColor(2)
    f_relRes_mean = TF1("f_relRes_mean","pol0", ptMin, ptMax)
    f_relRes_mean.SetParameter(0, relN2W)
    f_relRes_mean.SetLineColor(2)
    f_relYield_mean = TF1("f_relYield_mean","pol0", ptMin, ptMax)
    f_relYield_mean.SetParameter(0, relG12G2)
    f_relYield_mean.SetLineColor(2)

    
    # h_sigma_mass = TH1D("h_sigma_mass","#sigma_{mass #eta_{c}} DATA",nPTBins, pt)
    
    h_sigma_mass_MC = TH1D("h_sigma_mass_MC","#sigma_{mass #eta_{c}} MC",nPTBins,pt)
    # h_sigma_mass_MC_fromB = TH1D("h_sigma_mass_MC_fromB","#sigma_{mass #eta_{c}} MC from-b",nPTBins,pt)
    h_rEtacToJpsi = TH1D("h_rEtacToJpsi","#sigma_{#eta_{c}}/#sigma_{J/#psi} MC",nPTBins,pt)
    h_rNarToW = TH1D("h_rNarToW","#sigma_{Narr}/#sigma_{W} MC",nPTBins,pt)
    h_rG1toG2 = TH1D("h_rG1toG2","N_{Narr}/N_{W} MC",nPTBins,pt)
      
  
    for iPT in range(1, nPTBins+1):

        f = TFile("../resolutions/resoTzCut_log/MC_MassResolution{}_wksp_PT{}.root".format(add,iPT),"READ") 
        wMC = f.Get("w")
        f.Close()
        
        h_sigma_mass_MC.SetBinContent(iPT,wMC.var("sigma_eta_1").getValV())
        h_sigma_mass_MC.SetBinError(iPT,wMC.var("sigma_eta_1").getError())
        
        h_rEtacToJpsi.SetBinContent(iPT,wMC.var("rEtaToJpsi").getValV())
        h_rNarToW.SetBinContent(iPT,wMC.var("rNarToW").getValV())
        h_rG1toG2.SetBinContent(iPT,wMC.var("rG1toG2").getValV())
        h_rEtacToJpsi.SetBinError(iPT,wMC.var("rEtaToJpsi").getError())
        h_rNarToW.SetBinError(iPT,wMC.var("rNarToW").getError())
        h_rG1toG2.SetBinError(iPT,wMC.var("rG1toG2").getError())

        print wMC.var("rNarToW").getValV(), wMC.var("rG1toG2").getValV()
        
        # f = TFile("../Results/MassFit/fromB/Jpsi_PT/Wksp_MassFit_PT{}_Tz0_C0.root".format(iPT),"READ") 
        # wMC = f.Get("w")
        # f.Close()
        
        # h_sigma_mass.SetBinContent(iPT,wMC.var("sigma_eta_1").getValV())
        # h_sigma_mass.SetBinError(iPT,wMC.var("sigma_eta_1").getError())
    

        # f = TFile("../Results/MC/MassFit/MC_MassResolution_fromB_cuted_2016_wksp_PT{}.root".format(iPT),"READ") 
        # wMC = f.Get("w")
        # f.Close()

        # h_sigma_mass_MC_fromB.SetBinContent(iPT,wMC.var("sigma_eta_1").getValV())
        # h_sigma_mass_MC_fromB.SetBinError(iPT,wMC.var("sigma_eta_1").getError())
        
  
    
    fSigmaMC = TF1("fSigmaMC","pol1", ptMin, ptMax)
    fRel = TF1("fRel","pol1", ptMin, ptMax)
    fRelRes = TF1("fRelRes","pol1", ptMin, ptMax)
    fRelYield = TF1("fRelYield","pol1", ptMin, ptMax)
    
    fSigmaMC.SetLineColor(2)
    fRel.SetLineColor(2)

    h_sigma_mass_MC.Fit(fSigmaMC)
    h_rNarToW.Fit(fRelRes)
    h_rG1toG2.Fit(fRelYield)
    h_rEtacToJpsi.Fit(fRel)

    
    leg = TLegend(0.5,0.25,0.88,0.45)
    leg.AddEntry(0, "LHCb preliminary", "")
    leg.AddEntry(0, "#sqrt{s}=13 TeV", "")
    leg.SetLineWidth(0)
    
    c = TCanvas("Masses_Fit MC","Masses Fit MC",1200,900)
    c.Divide(2,2)

    c.cd(1)
    gPad.SetLeftMargin(0.15)
    gPad.SetBottomMargin(0.15)
    h_sigma_mass_MC.GetXaxis().SetTitle("p_{T}, GeV/c")
    h_sigma_mass_MC.GetYaxis().SetTitle("#sigma_{#eta_{c}}, MeV/c^{2}")
    h_sigma_mass_MC.SetLineWidth(2)
    h_sigma_mass_MC.SetLineColor(1)
    h_sigma_mass_MC.SetMarkerColor(2)
    h_sigma_mass_MC.SetMarkerStyle(kFullCircle)
    h_sigma_mass_MC.SetMinimum(6.95)
    h_sigma_mass_MC.SetMaximum(8.05)
    h_sigma_mass_MC.GetXaxis().SetNdivisions(510)
    h_sigma_mass_MC.DrawCopy("0E1")
    # texJpsiPiMC1.DrawLatex(0.25, 0.80, "LHCb simulation")
    # texJpsiPiMC1.DrawLatex(0.25, 0.75, "#sqrt{s}=13 TeV")

    c.cd(2)
    gPad.SetLeftMargin(0.15)
    gPad.SetBottomMargin(0.15)
    h_rEtacToJpsi.GetXaxis().SetTitle("p_{T}, GeV/c")
    h_rEtacToJpsi.GetYaxis().SetTitle("#sigma_{#eta_{c}}/#sigma_{J/#psi}")
    h_rEtacToJpsi.SetLineWidth(2)
    h_rEtacToJpsi.SetLineColor(1)
    h_rEtacToJpsi.SetMarkerColor(2)
    h_rEtacToJpsi.SetMinimum(0.87)
    h_rEtacToJpsi.SetMaximum(1.0)
    h_rEtacToJpsi.SetMarkerStyle(kFullCircle)
    h_rEtacToJpsi.DrawCopy("E1  0")
    #fRel.Draw("same")
    # texJpsiPiMC1.DrawLatex(0.25, 0.80, "LHCb simulation")
    # texJpsiPiMC1.DrawLatex(0.25, 0.75, "#sqrt{s}=13 TeV")
    f_rel_mean.Draw("same")
    
    c.cd(3)
    gPad.SetLeftMargin(0.15)
    gPad.SetBottomMargin(0.15)
    h_rNarToW.GetXaxis().SetTitle("p_{T}, GeV/c")
    h_rNarToW.GetYaxis().SetTitle("#sigma_{n}/#sigma_{w}")
    h_rNarToW.SetLineWidth(2)
    h_rNarToW.SetLineColor(1)
    h_rNarToW.SetMarkerColor(2)
    h_rNarToW.SetMinimum(0.125)
    h_rNarToW.SetMaximum(0.26)
    h_rNarToW.SetMarkerStyle(kFullCircle)
    h_rNarToW.DrawCopy("E1 0")
    #fRelRes.Draw("same")
    # texJpsiPiMC1.DrawLatex(0.25, 0.35, "LHCb simulation")
    # texJpsiPiMC1.DrawLatex(0.25, 0.3, "#sqrt{s}=13 TeV")
    f_relRes_mean.Draw("same")

    c.cd(4)
    gPad.SetLeftMargin(0.15)
    gPad.SetBottomMargin(0.15)
    h_rG1toG2.GetXaxis().SetTitle("p_{T}, GeV/c")
    h_rG1toG2.GetYaxis().SetTitle("f_{n}")
    h_rG1toG2.SetLineWidth(2)
    h_rG1toG2.SetLineColor(1)
    h_rG1toG2.SetMarkerColor(2)
    h_rG1toG2.SetMinimum(0.915)
    h_rG1toG2.SetMaximum(0.97)
    h_rG1toG2.SetMarkerStyle(kFullCircle)
    h_rG1toG2.DrawCopy("E1")
    # texJpsiPiMC1.DrawLatex(0.25, 0.35, "LHCb simulation")
    # texJpsiPiMC1.DrawLatex(0.25, 0.3, "#sqrt{s}=13 TeV")
    f_relYield_mean.Draw("same")

    fout = TFile("../Results/MC/MassFit/parFit%s_GeV.root"%(add), "recreate")
    h_rEtacToJpsi.Write()
    h_rNarToW.Write()
    h_rG1toG2.Write()
    fSigmaMC.Write()
    fRel.Write()
    c.Write()
    fout.Close()
    
    c.SaveAs("PT_dist_mass%s.pdf"%(add))

    
gROOT.LoadMacro("../lhcbStyle.C")
mass_pars(gauss=True)
#mass_pars(False)
# mass_parsCB()
