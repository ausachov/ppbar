from ROOT import *
from array import array
import numpy as np

gROOT.LoadMacro("../libRooRelBreitWigner/RooRelBreitWigner.cxx+");
gROOT.LoadMacro("../BifurcatedCB/BifurcatedCB.cxx+");
homeDir = "/users/LHCb/zhovkovska/scripts/"


def massTzInPT(gauss=True):
    
    gStyle.SetOptTitle(0)
    gStyle.SetOptStat(0)
    nTzBins = 5
    nPTBins = 4

    if gauss: add=""
    else: add="_CB"
    
    
    #tz = array( 'f',[-10., -0.025, 0.0, 0.025, 0.2, 1.0, 1.5, 2.0, 3.0, 4.0, 10.])
    #tz = array( 'f',[-10., -0.025, 0.,        0.200,       ., 10.])
    #tz = np.array([-10., -0.025, 0.,        0.200,   2., 10.])
    tz = np.array([0., 1., 2., 3., 4., 5., 6., 7.])
    tzPT = np.array([0., 0.8, 1.8, 2.8, 3.8, 4.8, 5.8, 6.8])
    nTzBins = len(tz)-1
    tzMin, tzMax = tz[0], tz[nTzBins]

    Jpsi_TRUE_M = 3096.9
    etac_TRUE_M = 2093.4
    
    h_mass_jpsi_MC = []
    h_mass_etac_MC = []

    Jpsi_M_as, Jpsi_M_as_err, etac_M_as, etac_M_as_err = [], [], [], []

    for iPT in range(nPTBins):
        h_mass_jpsi_MC.append(TH1D("h_mass_jpsi_MC_PT%i"%iPT,"(M-TRUE_M)_{J/#psi}",nTzBins , tzPT+0.1*iPT))
        h_mass_etac_MC.append(TH1D("h_mass_etac_MC_PT%i"%iPT,"(M-TRUE_M)_{#eta_c}",nTzBins , tz))      
        
        f = TFile(homeDir+"Results/MC/MassFit/Tz{}Bins/MC_MassResolution{}_shiftFix_2016_wksp_PT{}_Tz{}.root".format(nTzBins,add,iPT+1,nTzBins),"READ") 
        w = f.Get("w")
        f.Close()

        Jpsi_M_as.append(w.var("mean_Jpsi").getValV())
        Jpsi_M_as_err.append(w.var("mean_Jpsi").getError())
        #etac_M_as.append(w.var("mean_Etac").getValV())
        #etac_M_as_err.append(w.var("mean_Etac").getError())


        for iTz in range(1, nTzBins+1):

            f = TFile(homeDir+"Results/MC/MassFit/Tz{}Bins/MC_MassResolution{}_shiftFix_2016_wksp_PT{}_Tz{}.root".format(nTzBins,add,iPT+1,iTz),"READ") 
            w = f.Get("w")
            f.Close()
            
            h_mass_jpsi_MC[iPT].SetBinContent(iTz,(w.var("mean_Jpsi").getValV()-Jpsi_M_as[iPT]))
            if iTz!=nTzBins:
                h_mass_jpsi_MC[iPT].SetBinError(iTz,(w.var("mean_Jpsi").getError())+Jpsi_M_as_err[iPT])
            else:
                h_mass_jpsi_MC[iPT].SetBinError(iTz,(w.var("mean_Jpsi").getError()))
            #h_mass_jpsi_MC[iPT].SetBinContent(iTz,(w.var("mean_Jpsi").getValV()-Jpsi_M_as[iPT])/(Jpsi_M_as[iPT]+Jpsi_TRUE_M))
            #h_mass_jpsi_MC[iPT].SetBinError(iTz,(w.var("mean_Jpsi").getError())/(Jpsi_M_as[iPT]+Jpsi_TRUE_M))
            #h_mass_etac_MC[iPT].SetBinContent(iTz,(w.var("mean_Etac").getValV()-etac_M_as)/(etac_M_as+etac_TRUE_M))
            #h_mass_etac_MC[iPT].SetBinError(iTz,(w.var("mean_Etac").getError()-etac_M_as)/(etac_M_as+etac_TRUE_M))

            #print w.var("mean_Jpsi").getValV(), w.var("mean_Jpsi").getError(), h_mass_jpsi_MC[iPT].GetBinContent(iTz), h_mass_jpsi_MC[iPT].GetBinError(iTz)
            
            
    f = TFile(homeDir+"Results/MC/MassFit/Tz{}Bins/MC_MassResolution{}_shiftFix_2016_wksp_Tz{}.root".format(nTzBins,add,nTzBins),"READ") 
    w = f.Get("w")
    f.Close()
    Jpsi_M_as0 = w.var("mean_Jpsi").getValV()       
    Jpsi_M_as0_err = w.var("mean_Jpsi").getError()       
    h_mass_jpsi = TH1D("h_mass_jpsi","(M_{rel})_{J/#psi}",nTzBins , tz)
    #f = TFile(homeDir+"Results/MassFit/prompt/Sim/Etac_Wksp_MassFit_PT0_C0_total_sigma.root","READ") 
    #w = f.Get("w")
    #f.Close()
    
    #for iTz in range(1, 6):
        

        #h_mass_jpsi.SetBinContent(iTz,(w.var("rNormJpsi_Tz%s"%(iTz)).getValV()-1.)*w.var("mass_Jpsi").getValV())
        #h_mass_jpsi.SetBinError(iTz,(w.var("rNormJpsi_Tz%s"%(iTz)).getError())*w.var("mass_Jpsi").getValV())
        ##h_mass_jpsi.SetBinContent(iTz,(w.var("rNormJpsi_Tz%s"%(iTz)).getValV()-1.))
        ##h_mass_jpsi.SetBinError(iTz,(w.var("rNormJpsi_Tz%s"%(iTz)).getError()))
        

    for iTz in range(1, nTzBins+1):
        

        f = TFile(homeDir+"Results/MC/MassFit/Tz{}Bins/MC_MassResolution{}_shiftFix_2016_wksp_Tz{}.root".format(nTzBins,add,iTz),"READ") 
        w = f.Get("w")
        f.Close()
            
        h_mass_jpsi.SetBinContent(iTz,(w.var("mean_Jpsi").getValV()-Jpsi_M_as0))
        h_mass_jpsi.SetBinError(iTz,(w.var("mean_Jpsi").getError()))
        if iTz!=nTzBins:
            h_mass_jpsi.SetBinError(iTz,(w.var("mean_Jpsi").getError()+Jpsi_M_as0_err))
        else:
            h_mass_jpsi.SetBinError(iTz,(w.var("mean_Jpsi").getError()))
        #h_mass_jpsi.SetBinError(iTz,(w.var("mean_Jpsi").getError()))
            
    
    #h_mass_jpsi.SetBinContent(5, 0.)
    #h_mass_jpsi.SetBinError(5,1e-5)

        
    fMass = TF1("fMass","pol1", tzMin, tzMax )
    
    #h_mass.Fit(fMass, "I")
    
    leg = TLegend(0.18,0.65, 0.35, 0.88)
    #leg.AddEntry(h_mass_jpsi, "Data","lep")
    #leg.AddEntry(h_mass_jpsi, "Data","fp")
    leg.AddEntry(h_mass_jpsi, "MC PT-inegr","fp")

    c0 = TCanvas("Masses_Fit","Masses Fit",1000,600)
    #c0.Divide(1, 2)    

    c0.cd(1)
    gPad.SetLeftMargin(0.15)
    gPad.SetBottomMargin(0.15)
    gPad.SetTopMargin(0.10)

    #h_mass_jpsi.GetXaxis().SetTitle("t_{z}, ps")
    h_mass_jpsi.GetXaxis().SetTitle("Number of t_{z} bin")
    h_mass_jpsi.GetYaxis().SetTitle("#Deltam(t_{z}), MeV/c ")
    h_mass_jpsi.GetYaxis().SetRangeUser(-7.5,7.5)
    h_mass_jpsi.SetLineWidth(2)
    h_mass_jpsi.SetLineColor(1)
    h_mass_jpsi.SetFillColor(2)
    #h_mass_jpsi.SetFillStyle(3003)
    h_mass_jpsi.SetMarkerColor(2)
    h_mass_jpsi.SetMarkerStyle(kFullCircle)
    #h_mass_jpsi.SetMinimum(-0.0051)
    #h_mass_jpsi.SetMaximum(0.0051)
    h_mass_jpsi.SetMinimum(-15.5)
    h_mass_jpsi.SetMaximum(6.5)
    h_mass_jpsi.DrawCopy("E2")
    h_mass_jpsi.GetYaxis().SetRangeUser(-7.5,7.5)
    ##fMass.Draw("same")

    for iPT in range(nPTBins):
        #leg.AddEntry(h_mass_jpsi_MC[iPT], "MC PT%s"%(iPT+1),"f")
        leg.AddEntry(h_mass_jpsi_MC[iPT], "MC PT%s"%(iPT+1),"lep")
        h_mass_jpsi_MC[iPT].GetXaxis().SetTitle("t_{z}, ps")
        h_mass_jpsi_MC[iPT].GetYaxis().SetTitle("#Deltam(t_{z}), MeV/c ")
        h_mass_jpsi_MC[iPT].SetLineWidth(2)
        h_mass_jpsi_MC[iPT].SetLineColor(1)
        h_mass_jpsi_MC[iPT].SetMarkerColor(iPT+6)
        h_mass_jpsi_MC[iPT].SetMarkerStyle(kFullCircle)
        #h_mass_jpsi_MC[iPT].SetMarkerStyle(1)
        #h_mass_jpsi_MC[iPT].SetFillColor(iPT+6)
        #h_mass_jpsi_MC[iPT].SetFillStyle(3002)
        #h_mass_jpsi_MC[iPT].SetMinimum(-0.0051)
        #h_mass_jpsi_MC[iPT].SetMaximum(0.0051)
        h_mass_jpsi_MC[iPT].SetMinimum(-7.5)
        h_mass_jpsi_MC[iPT].SetMaximum(7.5)
        h_mass_jpsi_MC[iPT].DrawCopy("same E1")
    #fMass.Draw("same")
    leg.Draw()
        

    c0.SaveAs("Tz_dist_inPT.pdf")
    #c0.SaveAs("Tz_dist_massErr_5Bins_MC.pdf")
    


def massErrTz(gauss=True):
    
    gStyle.SetOptTitle(0)
    gStyle.SetOptStat(0)
    nTzBins = 11

    if gauss: add=""
    else: add="_CB"
    
    mass = nTzBins*[0]
        
    mass_err = nTzBins *[0]
    
    #tz = array( 'f',[-10., -0.15, -0.025, 0.0, 0.025, 0.2, 1.0, 1.5, 2.0, 3.0, 4.0, 10.])
    tz = array( 'f',[-10.0, -0.125, -0.025, 0.,        0.200,   2., 4., 10.])
    nTzBins = len(tz)-1
    tzMin, tzMax = tz[0], tz[nTzBins]
    
    h_mass_jpsi = TH1D("h_mass_jpsi_Data","(M_{rel})_{J/#psi}",nTzBins , tz)
    h_mass_jpsi_sc = TH1D("h_mass_jpsi_sc","(M_{rel})_{J/#psi}",nTzBins , tz)
    h_mass_jpsi_MC = TH1D("h_mass_jpsi","(M-TRUE_M)_{J/#psi}",nTzBins , tz)
    h_mass_etac_MC = TH1D("h_mass_etac_MC","(M-TRUE_M)_{#eta_c}",nTzBins , tz)
    h_sigma_mass = TH1D("h_sigma_mass","#sigma_{#eta_c}",nTzBins , tz)
    h_sigma_mass_MC = TH1D("h_sigma_mass_MC","#sigma_{#eta_c}",nTzBins , tz)
      
    Jpsi_TRUE_M = 3096.9
    etac_TRUE_M = 2093.4

    #------getting as. val. of mass from last two bins of MC-----------------------------------------------------------------------------------------
    f = TFile(homeDir+"Results/MC/MassFit/Tz{}Bins/MC_MassResolution{}_shiftFix_2016_wksp_Tz{}.root".format(nTzBins,add,nTzBins-1),"READ") 
    w = f.Get("w")
    f.Close()

    Jpsi_M_as = w.var("mean_Jpsi").getValV()
    Jpsi_M_as_err = w.var("mean_Jpsi").getError()
    
    f = TFile(homeDir+"Results/MC/MassFit/Tz{}Bins/MC_MassResolution{}_shiftFix_2016_wksp_Tz{}.root".format(nTzBins,add,nTzBins),"READ") 
    w = f.Get("w")
    f.Close()

    Jpsi_M_as += w.var("mean_Jpsi").getValV()
    Jpsi_M_as_err += w.var("mean_Jpsi").getError()
    #etac_M_as = w.var("mean_Etac").getValV()
    #etac_M_as_err = w.var("mean_Etac").getError()

    #f = TFile(homeDir+"Results/MC/MassFit/MC_MassResolution_2016_wksp.root","READ") 
    #w = f.Get("w")
    #f.Close()
    sigmaNormMC = w.var("sigma_eta_1").getValV()

    Jpsi_M_as/=2.
    Jpsi_M_as_err/=2.

    #------getting as. val. of mass from last bin of Data-----------------------------------------------------------------------------------------
    f = TFile(homeDir+"Results/MassFit/prompt/Sim/m_scaled/Etac_Wksp_MassFit_C0_total_sigma.root","READ") 
    w = f.Get("w")
    f.Close()
    sigmaNormData = w.var("rNormSigma_Tz%s"%(nTzBins)).getValV()

    for iTz in range(1, nTzBins+1):

        f = TFile(homeDir+"Results/MC/MassFit/Tz{}Bins/MC_MassResolution{}_shiftFix_2016_wksp_Tz{}.root".format(nTzBins,add,iTz),"READ") 
        w = f.Get("w")
        f.Close()
        
        #h_mass_jpsi_MC.SetBinContent(iTz,(w.var("mean_Jpsi").getValV()-Jpsi_M_as))
        #h_mass_jpsi_MC.SetBinError(iTz,(w.var("mean_Jpsi").getError()))
        #h_mass_etac_MC.SetBinContent(iTz,w.var("mean_Etac").getValV()/etac_TRUE_M)
        #h_mass_etac_MC.SetBinError(iTz,w.var("mean_Etac").getError()/etac_TRUE_M)
        h_mass_jpsi_MC.SetBinContent(iTz,(w.var("mean_Jpsi").getValV()-Jpsi_M_as)/(Jpsi_M_as+Jpsi_TRUE_M))
        h_mass_jpsi_MC.SetBinError(iTz,(w.var("mean_Jpsi").getError())/(Jpsi_M_as+Jpsi_TRUE_M))
        print 1+h_mass_jpsi_MC.GetBinContent(iTz)
        #h_mass_etac_MC.SetBinContent(iTz,(w.var("mean_Etac").getValV()-etac_M_as)/(etac_M_as+etac_TRUE_M))
        #h_mass_etac_MC.SetBinError(iTz,(w.var("mean_Etac").getError()-etac_M_as)/(etac_M_as+etac_TRUE_M))

                
        #h_sigma_mass_MC.SetBinContent(iTz,w.var("sigma_Jpsi_1").getValV())
        #h_sigma_mass_MC.SetBinError(iTz,w.var("sigma_Jpsi_1").getError())
        h_sigma_mass_MC.SetBinContent(iTz,w.var("sigma_eta_1").getValV()/sigmaNormMC)
        h_sigma_mass_MC.SetBinError(iTz,w.var("sigma_eta_1").getError()/sigmaNormMC)
        
        ##f = TFile(homeDir+"Results/MassFit/prompt/Sim/Etac_Wksp_MassFit_PT0_C0_total_sigma.root","READ") 
        f = TFile(homeDir+"Results/MassFit/prompt/Sim/m_scaled/Etac_Wksp_MassFit_C0_total_sigma.root","READ") 
        w = f.Get("w")
        f.Close()

        #h_mass_jpsi.SetBinContent(iTz,(w.var("rNormJpsi_Tz%s"%(iTz)).getValV()-1.)*w.var("mass_Jpsi").getValV())
        #h_mass_jpsi.SetBinError(iTz,(w.var("rNormJpsi_Tz%s"%(iTz)).getError())/w.var("rNormJpsi_Tz%s"%(iTz)).getValV()*w.var("mass_Jpsi").getValV() )
        h_mass_jpsi.SetBinContent(iTz,(w.var("rNormJpsi_Tz%s"%(iTz)).getValV()-1.))
        h_mass_jpsi.SetBinError(iTz,(w.var("rNormJpsi_Tz%s"%(iTz)).getError()))
        
        h_sigma_mass.SetBinContent(iTz,(w.var("rNormSigma_Tz%s"%(iTz)).getValV())/sigmaNormData)
        h_sigma_mass.SetBinError(iTz,(w.var("rNormSigma_Tz%s"%(iTz)).getError())/sigmaNormData)

        #f = TFile(homeDir+"Results/MassFit/prompt/Sim/Etac_Wksp_MassFit_PT0_C0_total_sigma.root","READ") 
        #f = TFile(homeDir+"Results/MassFit/prompt/Sim/m_scaled/Etac_Wksp_MassFit_C0_total_sigma.root","READ") 
        #w = f.Get("w")
        #f.Close()

        #h_mass_jpsi_sc.SetBinContent(iTz,(w.var("rNormJpsi_Tz%s"%(iTz)).getValV()-1.)*w.var("mass_Jpsi").getValV())
        #print "%s"%(w.var("rNormJpsi_Tz%s"%(iTz)).getError()/w.var("rNormJpsi_Tz%s"%(iTz)).getValV()), h_mass_jpsi_sc.GetBinContent(iTz) 
        #h_mass_jpsi_sc.SetBinError(iTz,(w.var("rNormJpsi_Tz%s"%(iTz)).getError())/w.var("rNormJpsi_Tz%s"%(iTz)).getValV()*w.var("mass_Jpsi").getValV() )
        #h_mass_jpsi_sc.SetBinContent(iTz,(w.var("rNormJpsi_Tz%s"%(iTz)).getValV()-1.))
        #print("%s"%(w.var("rNormJpsi_Tz%s"%(iTz)).getValV()-1.))
        #h_mass_jpsi_sc.SetBinError(iTz,(w.var("rNormJpsi_Tz%s"%(iTz)).getError()))

    
    #h_mass_jpsi.SetBinContent(nTzBins, 0.)
    h_mass_jpsi.SetBinError(nTzBins,1e-5)
    h_mass_jpsi_sc.SetBinContent(nTzBins, 0.)
    h_mass_jpsi_sc.SetBinError(nTzBins,1e-5)
        
    fMass = TF1("fMass","pol1", tzMin, tzMax )
    fSigmaTz = TF1("fSigmaTz","pol1", tzMin, tzMax )
    
    #h_mass.Fit(fMass, "I")
    h_sigma_mass_MC.Fit(fSigmaTz,"I")
    
    leg = TLegend(0.20,0.70, 0.40, 0.88)
    leg.AddEntry(h_mass_jpsi, "Data","lep")
    #leg.AddEntry(h_mass_jpsi_sc, "Data mom. sc.","lep")
    #leg.AddEntry(h_mass_jpsi_MC, "MC w/o mom. sc.","f")
    leg.AddEntry(h_mass_jpsi_MC, "MC","f")

    c0 = TCanvas("Masses_Fit","Masses Fit",900,600)
    #c0.Divide(1, 2)    

    c0.cd(1)
    gPad.SetLeftMargin(0.15)
    gPad.SetBottomMargin(0.15)
    gPad.SetTopMargin(0.10)
    h_mass_jpsi_MC.GetXaxis().SetTitle("t_{z}, ps")
    h_mass_jpsi_MC.GetYaxis().SetTitle("#Deltam(t_{z}), MeV/c ")
    h_mass_jpsi_MC.SetLineWidth(2)
    h_mass_jpsi_MC.SetLineColor(1)
    h_mass_jpsi_MC.SetMarkerColor(2)
    #h_mass_jpsi_MC.SetMarkerStyle(kFullCircle)
    h_mass_jpsi_MC.SetMarkerStyle(1)
    h_mass_jpsi_MC.SetFillColor(2)
    h_mass_jpsi_MC.SetFillStyle(3003)
    h_mass_jpsi_MC.SetMinimum(-0.006)
    h_mass_jpsi_MC.SetMaximum(0.006)
    #h_mass_jpsi_MC.SetMinimum(-14.)
    #h_mass_jpsi_MC.SetMaximum(7.)
    h_mass_jpsi_MC.DrawCopy("E2")
    #h_mass_jpsi_MC.DrawCopy("E1")
    ##fMass.Draw("same")
    
    h_mass_jpsi.GetXaxis().SetTitle("t_{z}, ps")
    h_mass_jpsi.GetYaxis().SetTitle("#Deltam(t_{z}), MeV/c ")
    h_mass_jpsi.SetLineWidth(2)
    h_mass_jpsi.SetLineColor(1)
    #h_mass_jpsi.SetFillColor(2)
    #h_mass_jpsi.SetFillStyle(3003)
    h_mass_jpsi.SetMarkerColor(2)
    h_mass_jpsi.SetMarkerStyle(kFullCircle)
    h_mass_jpsi.DrawCopy("same E1")
    ##fMass.Draw("same")

    
    h_mass_jpsi_sc.SetLineWidth(2)
    h_mass_jpsi_sc.SetLineColor(1)
    h_mass_jpsi_sc.SetMarkerColor(4)
    h_mass_jpsi_sc.SetMarkerStyle(kFullCircle)
    #h_mass_jpsi_sc.DrawCopy("same E1")
    leg.Draw()

    ##gPad.SetLeftMargin(0.15)
    #gPad.SetBottomMargin(0.15)
    #gPad.SetTopMargin(0.15)
    #h_mass_etac_MC.GetXaxis().SetTitle("t_{z}, ps")
    ##h_mass_res.GetYaxis().SetTitle("mass_{J/#psi} - mass_{#eta_c}/ [MeV/c^{2}]")
    #h_mass_etac_MC.GetYaxis().SetTitle("(M-M^{TRUE})_{#eta_{c}}/ [MeV/c^{2}]")
    #h_mass_etac_MC.SetLineWidth(2)
    #h_mass_etac_MC.SetLineColor(1)
    #h_mass_etac_MC.SetMarkerColor(2)
    #h_mass_etac_MC.SetMarkerStyle(kFullCircle)
    ##h_mass_etac_MC.GetXaxis().SetTitleOffset(1.50)
    ##h_mass_etac_MC.GetYaxis().SetTitleOffset(1.50)
    ##h_mass_etac_MC.SetMinimum(98.2)
    ##h_mass_etac_MC.SetMaximum(122.)
    #h_mass_etac_MC.SetMinimum(-0.0035)
    #h_mass_etac_MC.SetMaximum(0.0035)
    #h_mass_etac_MC.DrawCopy("E1")
    TGaxis.SetMaxDigits(3)
    ###fMass.Draw("same")
    
    c0.SaveAs("Tz_dist_massErr%s_%sBins.pdf"%(add,nTzBins))
    #c0.SaveAs("Tz_dist_massErr_5Bins_MC.pdf")
    
    c1 = TCanvas("Sigma_Fit","Masses Fit",900,600)
    #c0.Divide(1, 2)    

    c1.cd(1)
    gPad.SetLeftMargin(0.15)
    gPad.SetBottomMargin(0.15)
    gPad.SetTopMargin(0.10)
    h_sigma_mass_MC.GetXaxis().SetTitle("t_{z}, ps")
    #h_sigma_mass_MC.GetYaxis().SetTitle("#sigma^{MC}_{J/#psi}, MeV/c^{2}")
    h_sigma_mass_MC.GetYaxis().SetTitle("#alpha(t_{z})")
    h_sigma_mass_MC.SetLineWidth(2)
    h_sigma_mass_MC.SetLineColor(1)
    h_sigma_mass_MC.SetMarkerColor(2)
    h_sigma_mass_MC.SetMarkerStyle(kFullCircle)
    h_sigma_mass_MC.GetXaxis().SetTitleOffset(0.90)
    h_sigma_mass_MC.GetYaxis().SetTitleOffset(0.90)
    #h_sigma_mass_MC.SetMinimum(6.)
    #h_sigma_mass_MC.SetMaximum(8.5)
    h_sigma_mass_MC.SetMinimum(0.50)
    h_sigma_mass_MC.SetMaximum(2.0)
    h_sigma_mass_MC.SetMarkerSize(0)
    h_sigma_mass_MC.SetFillColor(2)
    h_sigma_mass_MC.SetFillStyle(3002)
    h_sigma_mass_MC.DrawCopy("E2")
    h_sigma_mass.SetMarkerColor(2)
    h_sigma_mass.DrawCopy("SAME E1")
    #fMass.Draw("same")
    leg.Draw()

    c1.SaveAs("Tz_dist_sigma%s_%sBins.pdf"%(add,nTzBins))

    fout = TFile("errMassTzDist%s_s_m_scaled_%sBins.root"%(add,nTzBins), "recreate")
    h_mass_jpsi_MC.Write()
    h_mass_jpsi.Write()
    h_mass_etac_MC.Write()
    h_sigma_mass_MC.Write()
    #fMass.Write()
    fSigmaTz.Write()
    c0.Write()
    fout.Close()


gROOT.LoadMacro("../lhcbStyle.C")
#massTzInPT()
massErrTz()
