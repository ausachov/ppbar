from ROOT import *
from array import *

# gROOT.LoadMacro("../libRooRelBreitWigner/RooRelBreitWigner.cxx+")
gROOT.LoadMacro("BifurcatedCB.cxx+")

# nTzBins = 8
# tz = (['-10.', '-0.025'], 
#       ['-0.025', '0.'],
#       ['0.',   '0.1'],
#       ['0.1',  '0.2'],
#       ['0.2',  '0.3'],
#       ['0.4',  '1.0'],
#       ['1.0',  '2.0'],
#       ['2.0',  '10.0'])

# nTzBins = 5
# var = "Jpsi_Tz"
# tz = (['-0.1',   '-0.025'],
#       ['-0.025', '0.'],
#       ['0',      '0.025'],
#       ['0.025',  '0.1'],
#       ['0.1',    '0.2'],
#       ['0.2',    '0.3'],
#       ['0.3',    '4.0'])
# JpsiTzBinning   = array("d",[-0.1,-0.025,0.,0.025,0.1,0.2,0.3,1.,2.,4.,10.])

nTzBins = 8
var = "(3.3)*(Jpsi_ENDVERTEX_Z-Jpsi_OWNPV_Z)*Jpsi_M/Jpsi_PZ"
tz = (['-0.1',   '-0.025'],
      ['-0.025', '0.'],
      ['0',      '0.025'],
      ['0.025',  '0.1'],
      ['0.1',    '0.2'],
      ['0.2',    '0.3'],
      ['0.3',    '4.0'],
      ['4.0',    '10.0'])
JpsiTzBinning   = array("d",[-0.1,-0.025,0.,0.025,0.1,0.2,0.3,4.,10.])


# nTzBins = 6
# var = "Jpsi_TRUE_Tz"
# tz = (['0',      '0.01'],
#       ['0.01',   '0.025'],
#       ['0.025',  '0.1'],
#       ['0.1',    '0.2'],
#       ['0.2',    '0.3'],
#       ['0.3',    '4.0'])
# JpsiTzBinning   = array("d",[0.,0.01,0.025,0.1,0.2,0.3,1.,2.,4.,10.])


# nTzBins = 6
# var = "Jpsi_PZ"
# tz = (['10000.',   '40000.'],
#       ['40000.',   '60000.'],
#       ['60000.',   '80000.'],
#       ['80000.',   '100000.'],
#       ['100000.',  '120000.'],
#       ['120000.',  '180000.'],)
# JpsiTzBinning = array("d",[10000.,40000.,60000.,80000.,90000.,100000.,120000.,180000])


MCLoc = '/users/LHCb/zhovkovska/scripts/MC/newMC/Jpsi/JpsiDiProton_MC_2016_AddBr.root'



def getData_d(w, iTz=0):
    
    ntJpsi_Prompt = TChain('DecayTree')
    ntJpsi_Prompt.Add(MCLoc)
    treeJpsi_Prompt = TTree()
    
    if(iTz == 0):
        treeJpsi_Prompt = ntJpsi_Prompt.CopyTree("")
    elif ( iTz <= nTzBins ):
        cutJpsiTzL = '('+var+' > %s)'%(tz[iTz-1][0])
        cutJpsiTzR = '('+var+' < %s)'%(tz[iTz-1][1])
        treeJpsi_Prompt = ntJpsi_Prompt.CopyTree( 'Jpsi_ENDVERTEX_CHI2<3.5 && '+ \
                                                  cutJpsiTzL + '&&' + \
                                                  cutJpsiTzR)
    else:
        print ('Incorrect number of Tz bin %s'%(iTz))
    
    
    Jpsi_M_res = RooRealVar ('Jpsi_M','Jpsi_M',3096.-100.0,3096.+100.0) 
    Jpsi_M_res.setBins(100)
    dsJpsi_Prompt = RooDataSet('dsJpsi_Prompt','dsJpsi_Prompt',treeJpsi_Prompt,RooArgSet(Jpsi_M_res))
    getattr(w,'import')(dsJpsi_Prompt)
    


def fillRelWorkspace(w):
    
    Jpsi_M_res = w.var('Jpsi_M')
    
    ratioNtoW = 0.50
    ratioEtaToJpsi = 0.88
    ratioArea = 0.70
    #gammaEtac = 31.8
    gammaEtac = 29.7
    
    
    
    rNarToW = RooRealVar('rNarToW','rNarToW',ratioNtoW, 0.01, 1.0)
    rG1toG2 = RooRealVar('rG1toG2','rG1toG2',ratioArea, 0.01, 1.0)
    nJpsi_Prompt = RooRealVar('nJpsi_Prompt','num of J/Psi Prompt', 2e3, 0, 5.e4)
    nJpsi_Prompt_1 = RooFormulaVar('nJpsi_Prompt_1','num of J/Psi','@0*@1',RooArgList(nJpsi_Prompt,rG1toG2))
    nJpsi_Prompt_2 = RooFormulaVar('nJpsi_Prompt_2','num of J/Psi','@0-@1',RooArgList(nJpsi_Prompt,nJpsi_Prompt_1))
    
    
    mean_Jpsi = RooRealVar('mean_Jpsi','mean of gaussian', 3096, -50.0, 4000)   
    
    sigma_Jpsi_1 = RooRealVar('sigma_Jpsi_1','width of gaussian',8.,0.,40.)
    sigma_Jpsi_2 = RooFormulaVar('sigma_Jpsi_2','width of gaussian','@0/@1',RooArgList(sigma_Jpsi_1,rNarToW))    

    
    #Fit J/psi
    gauss_1 = RooGaussian('gauss_1','gaussian PDF',Jpsi_M_res, mean_Jpsi, sigma_Jpsi_1) 
    gauss_2 = RooGaussian('gauss_2','gaussian PDF',Jpsi_M_res, mean_Jpsi, sigma_Jpsi_2) 


    aL = RooRealVar('aL','aL', 2,0,1000)
    aR = RooRealVar('aR','aR', 1)
    nL = RooRealVar('nL','nL', 4,0,100)
    nR = RooRealVar('nR','nR', 1)

    # CB1 =  BifurcatedCB('CB1','CB1',Jpsi_M_res, mean_Jpsi, sigma_Jpsi_1,aL,nL,aR,nR) 
    # CB2 =  BifurcatedCB('CB2','CB2',Jpsi_M_res, mean_Jpsi, sigma_Jpsi_2,aL,nL,aR,nR) 

    CB1 =  RooCBShape('CB1','CB1',Jpsi_M_res, mean_Jpsi, sigma_Jpsi_1,aL,nL) 
    CB2 =  RooCBShape('CB2','CB2',Jpsi_M_res, mean_Jpsi, sigma_Jpsi_2,aL,nL) 
    
    modelJpsi_Prompt = RooAddPdf('modelJpsi_Prompt','Jpsi signal', RooArgList(gauss_1, gauss_2), RooArgList(nJpsi_Prompt_1, nJpsi_Prompt_2))
    modelJpsi_PromptCB = RooAddPdf('modelJpsi_PromptCB','Jpsi signalCB', RooArgList(CB1, CB2), RooArgList(nJpsi_Prompt_1, nJpsi_Prompt_2))
    
    
    getattr(w,'import')(modelJpsi_Prompt)
    getattr(w,'import')(modelJpsi_PromptCB,RooFit.RecycleConflictNodes())
    




def fitData(iTz):

    gROOT.Reset()
    w = RooWorkspace('w',True)   
    
    getData_d(w, iTz)
    
    fillRelWorkspace(w)
    
    Jpsi_M_res = w.var('Jpsi_M')
        
    modelJpsi_Prompt = w.pdf('modelJpsi_Prompt')    
    dataJpsi_Prompt = w.data('dsJpsi_Prompt')
    
    
    sigma = w.var('sigma_Jpsi_1')
    mean_Jpsi = w.var('mean_Jpsi')
    
    
    
    

    r = modelJpsi_Prompt.fitTo(dataJpsi_Prompt,RooFit.Save(True)) 
    r = modelJpsi_Prompt.fitTo(dataJpsi_Prompt,RooFit.Minos(True),RooFit.Save(True)) 
    r = modelJpsi_Prompt.fitTo(dataJpsi_Prompt,RooFit.Extended(True),RooFit.Minos(True),RooFit.Save(True)) 
    
    
    
    gROOT.ProcessLine('gStyle->SetOptStat(000000000)')
    gROOT.ProcessLine('gStyle->SetOptTitle(0)')

    frame2 = Jpsi_M_res.frame(RooFit.Title('J/#psi to p #bar{p} prompt'))     
    dataJpsi_Prompt.plotOn(frame2)    
    modelJpsi_Prompt.plotOn(frame2,RooFit.Normalization(1.0,RooAbsReal.RelativeExpected))
    chi2Jpsi_Prompt = frame2.chiSquare()
    #modelJpsi_Prompt.plotOn(frame2,RooFit.Components(RooArgSet(gauss_1,gauss_2)),RooFit.FillStyle(3005),RooFit.FillColor(kMagenta),RooFit.DrawOption('F'), RooFit.Normalization(1.0,RooAbsReal.RelativeExpected))
    print 'chi2Jpsi_Prompt = ', chi2Jpsi_Prompt
    
    c = TCanvas('Masses_Fit','Masses Fit',1200,800)
    frame2.Draw()
    
    if(iTz == 0):
        namePic = 'MC_MassResolution_empty.pdf'    
    else:
        namePic = 'MC_MassResolution_Tz%s.pdf'%(iTz)    
    c.SaveAs(namePic)

    return mean_Jpsi.getVal(), mean_Jpsi.getError(), sigma.getVal(), sigma.getError()
    

def fitWholeData(iTz):

    gROOT.Reset()
    w = RooWorkspace('w',True)   
    
    getData_d(w, iTz)
    
    fillRelWorkspace(w)
    
    Jpsi_M_res = w.var('Jpsi_M')
        
    modelJpsi_Prompt = w.pdf('modelJpsi_Prompt')    
    modelJpsi_PromptCB = w.pdf('modelJpsi_PromptCB')  
      
      

    dataJpsi_Prompt = w.data('dsJpsi_Prompt')
    
    
    sigma = w.var('sigma_Jpsi_1')
    mean_Jpsi = w.var('mean_Jpsi')
    
    rNarToW = w.var('rNarToW')
    rG1toG2 = w.var('rG1toG2')
    nJpsi_Prompt = w.var('nJpsi_Prompt')
    
    
    r = modelJpsi_Prompt.fitTo(dataJpsi_Prompt,RooFit.Save(True)) 
    r = modelJpsi_Prompt.fitTo(dataJpsi_Prompt,RooFit.Minos(True),RooFit.Save(True)) 
    r = modelJpsi_Prompt.fitTo(dataJpsi_Prompt,RooFit.Extended(True),RooFit.Minos(True),RooFit.Save(True)) 

    j = modelJpsi_PromptCB.fitTo(dataJpsi_Prompt,RooFit.Extended(True),RooFit.Minos(True),RooFit.Save(True))
    j = modelJpsi_PromptCB.fitTo(dataJpsi_Prompt,RooFit.Extended(True),RooFit.Minos(True),RooFit.Save(True))
    j = modelJpsi_PromptCB.fitTo(dataJpsi_Prompt,RooFit.Extended(True),RooFit.Minos(True),RooFit.Save(True)) 


    
    frame = Jpsi_M_res.frame(RooFit.Title('J/#psi to p #bar{p}'))     
    dataJpsi_Prompt.plotOn(frame)    
    modelJpsi_Prompt.plotOn(frame,RooFit.Normalization(1.0,RooAbsReal.RelativeExpected))
    modelJpsi_PromptCB.plotOn(frame,RooFit.Normalization(1.0,RooAbsReal.RelativeExpected),RooFit.LineColor(2))
    c = TCanvas("c")
    frame.Draw()
    c.SaveAs("tryCB.pdf")






def MC_Resolution_Fit():
    
    JpsiMasses   = array("d",[])

    fitData(0)
    histMeans = TH1F("histMeans","histMeans",nTzBins,JpsiTzBinning)
    histReso  = TH1F("histReso","histReso",nTzBins,JpsiTzBinning)
    for iTz in range(1,nTzBins+1):
        valMeanJpsi, errMeanJpsi, valReso, errReso = fitData(iTz) 
        histMeans.SetBinContent(iTz,valMeanJpsi) 
        histMeans.SetBinError(iTz,errMeanJpsi) 
        histReso.SetBinContent(iTz,valReso) 
        histReso.SetBinError(iTz,errReso) 
        
    cc = TCanvas("means","means")
    histMeans.Draw()
    cc.SaveAs("JpsiMeans_vs_Tz.pdf")  

    c1 = TCanvas("reso","reso")
    histReso.Draw()
    c1.SaveAs("JpsiReso_vs_Tz.pdf")  

# MC_Resolution_Fit()
# fitData(0)
fitWholeData(0)




