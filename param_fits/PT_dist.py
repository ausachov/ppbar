from ROOT import *
from array import array
import numpy as np

gROOT.LoadMacro("../libRooRelBreitWigner/RooRelBreitWigner.cxx+");
gROOT.LoadMacro("../BifurcatedCB/BifurcatedCB.cxx+");
homeDir = "/users/LHCb/zhovkovska/scripts/"
    
def promptImp():
    
    nPTBins = 5
    pt = (['6500', '8000'], ['8000', '10000'], ['10000', '12000'], ['12000', '14000'], ['14000', '18000'])


    NEtac_Prompt_Data = 78400.*1.225
    NJpsi_Prompt_Data = 78400.
    NEtac_FromB_Data= 27587.*0.366
    NJpsi_FromB_Data= 27587.
    
    ntEtac_Prompt = TChain('DecayTree')
    ntJpsi_Prompt = TChain('DecayTree')
    ntEtac_FromB = TChain('DecayTree')
    ntJpsi_FromB = TChain('DecayTree')
    
    ntEtac_Prompt.Add(homeDir+'MC/newMC/Etac/EtacDiProton_MC_2016_AddBr.root')
    ntEtac_Prompt.Add(homeDir+'MC/newMC/Etac/EtacDiProton_MC_2015_AddBr.root')
    ntJpsi_Prompt.Add(homeDir+'MC/newMC/Jpsi/JpsiDiProton_MC_2016_AddBr.root')
    ntJpsi_Prompt.Add(homeDir+'MC/newMC/Jpsi/JpsiDiProton_MC_2015_AddBr.root')
    ntEtac_FromB.Add(homeDir+'MC/newMC/incl_b/EtacDiProton_MC_2015_incl_b_AddBr.root')
    ntEtac_FromB.Add(homeDir+'MC/newMC/incl_b/EtacDiProton_MC_2016_incl_b_AddBr.root')
    ntJpsi_FromB.Add(homeDir+'MC/newMC/Jpsi/JpsiDiProton_MC_2016_AddBr.root')
    ntJpsi_FromB.Add(homeDir+'MC/newMC/Jpsi/JpsiDiProton_MC_2015_AddBr.root')
    
    treeEtac_Prompt = TTree()
    treeJpsi_Prompt = TTree()
    treeEtac_FromB = TTree()
    treeJpsi_FromB = TTree()
    
    treeEtac_Prompt = ntEtac_Prompt.CopyTree("prompt")
    treeJpsi_Prompt = ntJpsi_Prompt.CopyTree("prompt")
    treeEtac_FromB = ntEtac_FromB.CopyTree("sec")
    treeJpsi_FromB = ntJpsi_FromB.CopyTree("sec")
    
    NEtac_Prompt = treeEtac_Prompt.GetEntries()
    NJpsi_Prompt = treeJpsi_Prompt.GetEntries()
    NEtac_FromB = treeEtac_FromB.GetEntries()
    NJpsi_FromB = treeJpsi_FromB.GetEntries()

    print '%4.1f, %4.1f, %4.1f, %4.1f'%(NEtac_Prompt, NJpsi_Prompt, NEtac_FromB, NJpsi_FromB)

    cutJpsi_Tz_High = 'Jpsi_Tz > 0.08'
    cutJpsi_IP_CHI2 = '((ProtonP_IPCHI2_OWNPV > 16) && (ProtonM_IPCHI2_OWNPV > 16))'
    
    treeEtac_Prompt = ntEtac_Prompt.CopyTree('prompt' + '&&' + cutJpsi_IP_CHI2 + '&&' + cutJpsi_Tz_High)
    treeJpsi_Prompt = ntJpsi_Prompt.CopyTree('prompt' + '&&' + cutJpsi_IP_CHI2 + '&&' + cutJpsi_Tz_High)
    treeEtac_FromB = ntEtac_FromB.CopyTree('sec' + '&&' + cutJpsi_IP_CHI2 + '&&' + cutJpsi_Tz_High)
    treeJpsi_FromB = ntJpsi_FromB.CopyTree('sec' + '&&' + cutJpsi_IP_CHI2 + '&&' + cutJpsi_Tz_High)

    NEtac_Prompt_Cuted = treeEtac_Prompt.GetEntries()
    NJpsi_Prompt_Cuted = treeJpsi_Prompt.GetEntries()
    NEtac_FromB_Cuted = treeEtac_FromB.GetEntries()
    NJpsi_FromB_Cuted = treeJpsi_FromB.GetEntries()

    print '%4.1f, %4.1f, %4.1f, %4.1f'%(NEtac_Prompt_Cuted, NJpsi_Prompt_Cuted, NEtac_FromB_Cuted, NJpsi_FromB_Cuted)
    
    prImpEtac = (NEtac_Prompt_Data/NEtac_FromB_Data) * float(NEtac_Prompt_Cuted/float(NEtac_Prompt)) * float(NEtac_FromB/float(NEtac_FromB_Cuted))
    prImpJpsi = (NJpsi_Prompt_Data/NJpsi_FromB_Data) * float(NJpsi_Prompt_Cuted/float(NJpsi_Prompt)) * (NJpsi_FromB/float(NJpsi_FromB_Cuted))
    
    effPS_Jpsi = float(NJpsi_Prompt_Cuted/float(NJpsi_Prompt))
    effPS_Etac = float(NEtac_Prompt_Cuted/float(NEtac_Prompt))
    effSS_Jpsi = float(NJpsi_FromB_Cuted/float(NJpsi_FromB))

    print 'eta_c = %2.2e, Jpsi = %2.2e'%(prImpEtac, prImpJpsi)
    print effPS_Etac, effPS_Jpsi, effSS_Jpsi
    
    cutJpsi_Tz_Low = 'Jpsi_Tz < 0.08'

    treeEtac_Prompt = ntEtac_Prompt.CopyTree('prompt' + '&&' + cutJpsi_Tz_Low)
    treeJpsi_Prompt = ntJpsi_Prompt.CopyTree('prompt' + '&&' + cutJpsi_Tz_Low)
    treeEtac_FromB = ntEtac_FromB.CopyTree('sec' + '&&' + cutJpsi_Tz_Low)
    treeJpsi_FromB = ntJpsi_FromB.CopyTree('sec' + '&&' + cutJpsi_Tz_Low)

    NEtac_Prompt_Cuted = treeEtac_Prompt.GetEntries()
    NJpsi_Prompt_Cuted = treeJpsi_Prompt.GetEntries()
    NEtac_FromB_Cuted = treeEtac_FromB.GetEntries()
    NJpsi_FromB_Cuted = treeJpsi_FromB.GetEntries()

    print '%4.1f, %4.1f, %4.1f, %4.1f'%(NEtac_Prompt_Cuted, NJpsi_Prompt_Cuted, NEtac_FromB_Cuted, NJpsi_FromB_Cuted)

    secImpEtac = (NEtac_FromB_Data/NEtac_Prompt_Data) * float(NEtac_Prompt/float(NEtac_Prompt_Cuted)) * float(NEtac_FromB_Cuted/float(NEtac_FromB))
    secImpJpsi = (NJpsi_FromB_Data/NJpsi_Prompt_Data) * float(NJpsi_Prompt/float(NJpsi_Prompt_Cuted)) * float(NJpsi_FromB_Cuted/float(NJpsi_FromB))
    
    effPP_Jpsi = float(NJpsi_Prompt_Cuted/float(NJpsi_Prompt))
    effPP_Etac = float(NEtac_Prompt_Cuted/float(NEtac_Prompt))
    effSP_Jpsi = float(NJpsi_FromB_Cuted/float(NJpsi_FromB))

    print 'eta_c = %2.2e, Jpsi = %2.2e'%(secImpEtac, secImpJpsi)
    print effPP_Etac, effPP_Jpsi, effSP_Jpsi

spsDir = "/sps/lhcb/zhovkovska/etacToPpbar/"

def pppi0Reco():
    
    ntJpsi_pp       = TChain('DecayTree')
    ntJpsi_pppi0    = TChain('DecayTree')
    
    ntJpsiGen_pp       = TChain('MCDecayTree')
    ntJpsiGen_pppi0    = TChain('MCDecayTree')


    ntJpsi_pp.Add(spsDir+'MC/JpsiDiProton_MC_2015.root')
    ntJpsi_pp.Add(spsDir+'MC/JpsiDiProton_MC_2016.root')
    ntJpsi_pppi0.Add(spsDir+'MC/JpsiDiProtonPi0_MC_2015.root')
    ntJpsi_pppi0.Add(spsDir+'MC/JpsiDiProtonPi0_MC_2016.root')
    ntJpsiGen_pp.Add(spsDir+'MC/JpsiDiProton_MC_Geneartor_2015.root')
    ntJpsiGen_pp.Add(spsDir+'MC/JpsiDiProton_MC_Geneartor_2016.root')
    ntJpsiGen_pppi0.Add(spsDir+'MC/JpsiDiProtonPi0_MC_Geneartor_2015.root')
    ntJpsiGen_pppi0.Add(spsDir+'MC/JpsiDiProtonPi0_MC_Geneartor_2016.root')
    
    treeJpsi_pp = TTree()
    treeJpsi_pppi0 = TTree()
    treeJpsiGen_pp = TTree()
    treeJpsiGen_pppi0 = TTree()
    
    NJpsi_pp = ntJpsi_pp.GetEntries()
    NJpsi_pppi0 = ntJpsi_pppi0.GetEntries()
    NJpsiGen_pp = ntJpsiGen_pp.GetEntries()
    NJpsiGen_pppi0 = ntJpsiGen_pppi0.GetEntries()

    print NJpsi_pp, NJpsi_pppi0, NJpsiGen_pp, NJpsiGen_pppi0
    print NJpsi_pp/float(NJpsiGen_pp), NJpsi_pppi0/float(NJpsiGen_pppi0)


def distChi2():
    
    ntEtac_Prompt = TChain('DecayTree')
    ntEtac_FromB = TChain('DecayTree')
    ntJpsi= TChain('DecayTree')
    #ntEtac = TChain('DecayTree')
    
    ntEtac_Prompt.Add(homeDir+'MC/newMC/Etac/EtacDiProton_MC_2016_AddBr.root')
    ntEtac_Prompt.Add(homeDir+'MC/newMC/Etac/EtacDiProton_MC_2015_AddBr.root')
    #ntEtac = ntEtac_Prompt.CopyTree("prompt")
    ntEtac_FromB.Add(homeDir+'MC/newMC/incl_b/EtacDiProton_MC_2015_incl_b_AddBr.root')
    ntEtac_FromB.Add(homeDir+'MC/newMC/incl_b/EtacDiProton_MC_2016_incl_b_AddBr.root')
    ntJpsi.Add(homeDir+'MC/newMC/Jpsi/JpsiDiProton_MC_2016_AddBr.root')
    ntJpsi.Add(homeDir+'MC/newMC/Jpsi/JpsiDiProton_MC_2015_AddBr.root')
    
    treeJpsi = TTree()
    treeEtac = TTree()
        
    
    treeEtac_Prompt = ntEtac_Prompt.CopyTree("prompt")
    treeEtac_FromB = ntEtac_FromB.CopyTree("sec")
    treeJpsi = ntJpsi.CopyTree("")
    list0 = TList()
    list0.Add(treeEtac_Prompt) 
    list0.Add(treeEtac_FromB) 
    list0.Add(treeJpsi)
    #treeEtac = TTree.MergeTrees(list0)

    tree = TTree.MergeTrees(list0)
    nEntrAll = tree.GetEntries()
    nEntrCut = tree.GetEntries("Jpsi_Dist_CHI2 < 7")
    print nEntrCut/float(nEntrAll)

    hist = TH1D("Dist_Chi2","Dist_Chi2",500, 0., 25)
    c1 = TCanvas("Chi2","Chi2",900,600)
    pad = c1.cd()
    tree.Draw("Jpsi_Dist_CHI2>>hist", "Jpsi_Dist_CHI2 < 25")
    hist = gDirectory.Get("hist");
    hist.SetFillStyle(3002)
    hist.SetFillColor(2)
    hist.GetXaxis().SetTitle("Distance #chi^{2}")
    hist.GetYaxis().SetTitle("Entries")
    #hist.Draw()
    pad.SetLogy()
    c1.SaveAs("Chi2Dist.pdf")



gROOT.LoadMacro("../lhcbStyle.C")
#promptImp()
#pppi0Reco()
#distChi2()

