from ROOT import *
from array import array
import numpy as np

gROOT.LoadMacro("../libRooRelBreitWigner/RooRelBreitWigner.cxx+");
gROOT.LoadMacro("../BifurcatedCB/BifurcatedCB.cxx+");
homeDir = "/users/LHCb/zhovkovska/scripts/"

def tz_pars():


    gStyle.SetOptTitle(0)
    gStyle.SetOptStat(0)
    nPTBins = 4
    
    add = "_newMC"
    
    pt = array( 'f',[6.5, 8., 10., 12., 14.])
    #pt = array( 'f',[6.5, 8., 10., 12., 14., 16., 20., 30])
    ptMax = pt[nPTBins]
    
    h_sigma_tz = TH1D("h_sigma_tz","#sigma_{t_{z}}",nPTBins,pt)
    h_mu = TH1D("h_mu","#mu(p_{T})",nPTBins,pt)
    h_mu_mean = TH1D("h_mu_mean","#mu total",1,6.5, ptMax)
    h_mu_mean_MC = TH1D("h_mu_mean_MC","#mu MC total",1,6.5, ptMax)
    h_tauB = TH1D("h_tauB","#tau_{B}",nPTBins,pt)
    h_beta = TH1D("h_beta","#beta",nPTBins,pt)
    h_rS = TH1D("h_rS","r_{S}",nPTBins,pt)
    

    f = TFile(homeDir+"Results/MC/TzFit/MC_TzRes"+add+"_2016_wksp.root","READ") 
    wMC = f.Get("w")
    f.Close()

    sigma_tz = wMC.var("S1").getValV()
    mu = wMC.var("mu").getValV()
    beta = wMC.var("beta").getValV()
    rs = wMC.var("rS").getValV()
    
    f = TFile(homeDir+"Results/MC/TzFit/MC_TRUE_Tz"+add+"_2016_wksp.root","READ") 
    wMC = f.Get("w")
    f.Close()

    tauB = wMC.var("tauB").getValV()


    f_sigma_tz =  TF1("f_sigma_tz","pol0",6.5,ptMax)
    f_mu = TF1("f_mu","pol0",6.5,ptMax)
    f_beta = TF1("f_beta","pol0",6.5,ptMax)
    f_tauB = TF1("f_tauB","pol0",6.5,ptMax)
    f_rs = TF1("f_rs","pol0", 6.5, ptMax)

    f_sigma_tz.SetParameter(0,sigma_tz)
    f_mu.SetParameter(0,mu)
    f_beta.SetParameter(0,beta)
    f_tauB.SetParameter(0,tauB)
    f_rs.SetParameter(0,rs)
    f_sigma_tz.SetLineColor(2)
    f_mu.SetLineColor(2)
    f_beta.SetLineColor(2)
    f_tauB.SetLineColor(2)
    f_rs.SetLineColor(2)


    #f = TFile(homeDir+"Results/MC/TzFit/MC_TzRes_2016_wksp.root","READ") 
    #wMC = f.Get("w")
    #f.Close()
    #h_mu_mean_MC.SetBinContent(1,wMC.var("mu").getValV())
    #h_mu_mean_MC.SetBinError(1,wMC.var("mu").getError())
         
    h_mu_mean.SetBinContent(1,1.3105e-04)
    h_mu_mean.SetBinError(1,4.0874e-04)
    
    for iPT in range(1, nPTBins+1): 
    
        f = TFile(homeDir+"Results/MC/TzFit/MC_TzRes"+add+"_2016_wksp_PT{}.root".format(iPT),"READ") 
        wMC = f.Get("w")
        f.Close()
        
        h_sigma_tz.SetBinContent(iPT,wMC.var("S1").getValV())
        h_sigma_tz.SetBinError(iPT,wMC.var("S1").getError())
        h_mu.SetBinContent(iPT,wMC.var("mu").getValV())
        h_mu.SetBinError(iPT,wMC.var("mu").getError())
        h_beta.SetBinContent(iPT,wMC.var("beta").getValV())
        h_beta.SetBinError(iPT,wMC.var("beta").getError())
        h_rS.SetBinContent(iPT,wMC.var("rS").getValV())
        h_rS.SetBinError(iPT,wMC.var("rS").getError())
    
    
        f = TFile(homeDir+"Results/MC/TzFit/MC_TRUE_Tz"+add+"_2016_wksp_PT{}.root".format(iPT),"READ") 
        wMC = f.Get("w")
        f.Close()

        h_tauB.SetBinContent(iPT,wMC.var("tauB").getValV())
        h_tauB.SetBinError(iPT,wMC.var("tauB").getError())
    
    #kdeSigma = TKDE(nPTBins, sigma_tz[0], 6.5., ptMax, "kerneltype:gaussian", 1.0)
    #kdeTauB = TKDE(nPTBins, tauB[0], 6.5., ptMax, "kerneltype:gaussian", 1.0)

    #  TF1 *fSigma = kdeSigma.GetFunction(10000)
    #  TF1 *fTauB = kdeTauB.GetFunction(10000)
    
    fSigmaAlt = TF1("fSigmaAlt","exp([0]-[1]*x)+exp([2]-[3]*x)",6.5,ptMax)
    fSigma = TF1("fSigma","exp([0]-[1]*x)+[2]",6.5,ptMax)
    fSigma.SetParameters(1.5, 0.025, 0.0003)
    #fSigma.SetLineColor(kBLACK)
    fTauB = TF1("fTauB","-exp([0]-[1]*x)+[2]",6.5,ptMax)
    fBeta = TF1("fBeta","pol1",6.5,ptMax)
    f_rS = TF1("f_rS","pol1",6.5,ptMax)
    
    h_sigma_tz.Fit(fSigma,"IR")
    h_sigma_tz.Fit(fSigma,"IR")
    #h_sigma_tz.Fit(fSigmaAlt,"IR")
    h_tauB.Fit(fTauB,"IR")
    #h_beta.Fit(fBeta,"IR")
    #h_rS.Fit(f_rS,"IR")

    legend = TLegend(0.1,0.7,0.35,0.9)
    legend.SetHeader("bias of t_{z}","C") #option "C" allows to center the header
    legend.AddEntry(h_mu,"#mu_{MC} in p_{T} bins","lep")
    legend.AddEntry(h_mu_mean_MC,"#mu_{MC}","f")
    legend.AddEntry(h_mu_mean,"#mu_{data}","f")

    texJpsiPiMC1 = TLatex()
    texJpsiPiMC1.SetNDC()
    
    c = TCanvas("Masses_Fit","Masses Fit",1200,900)
    c.Divide(2,2)
    
    c.cd(1)
    gPad.SetTopMargin(0.07)
    h_sigma_tz.GetXaxis().SetTitle("p_{T}, GeV/c")
    h_sigma_tz.GetYaxis().SetTitle("S_{#eta_{c}},S_{J/#psi}")
    h_sigma_tz.SetLineWidth(2)
    h_sigma_tz.SetLineColor(1)
    h_sigma_tz.SetMarkerColor(2)
    #h_sigma_tz.SetMinimum(3.5)
    h_sigma_tz.SetMaximum(5.0)
    h_sigma_tz.SetMarkerStyle(kFullCircle)
    h_sigma_tz.DrawCopy("E1 0 PMC")
    fSigma.Draw("same")
    fSigmaAlt.Draw("same")
    fSigmaAlt.SetLineColor(2)
    texJpsiPiMC1.DrawLatex(0.55, 0.75, "LHCb simulation")
    texJpsiPiMC1.DrawLatex(0.55, 0.7, "#sqrt{s}=13 TeV")
    f_sigma_tz.Draw("same")

    c.cd(2)
    
    #h_mu_mean.GetYaxis().SetRangeUser(-0.004, 0.004)
    #h_mu_mean.SetFillStyle(3002)
    #h_mu_mean.SetFillColor(15)
    #h_mu_mean.DrawCopy("E2")
    
    #h_mu_mean_MC.SetFillStyle(3003)
    #h_mu_mean_MC.SetFillColor(4)
    #h_mu_mean_MC.GetXaxis().SetTitleSize(0.07)
    #h_mu_mean_MC.GetYaxis().SetTitleSize(0.07)
    #h_mu_mean_MC.GetXaxis().SetTitleOffset(0.90)
    #h_mu_mean_MC.GetYaxis().SetTitleOffset(0.90)
    #h_mu_mean_MC.GetXaxis().SetTitleFont(12)
    #h_mu_mean_MC.GetYaxis().SetTitleFont(12)
    #h_mu_mean_MC.GetXaxis().SetLabelSize(0.05)
    #h_mu_mean_MC.GetYaxis().SetLabelSize(0.05)
    #h_mu_mean_MC.GetXaxis().SetLabelFont(62)
    #h_mu_mean_MC.GetYaxis().SetLabelFont(62)
    #h_mu_mean_MC.DrawCopy("E2")
    
    gPad.SetTopMargin(0.07)
    h_mu.GetXaxis().SetTitle("p_{T}, GeV/c")
    h_mu.GetYaxis().SetTitle("#mu, ps")
    h_mu.SetLineWidth(2)
    h_mu.SetLineColor(1)
    h_mu.SetMarkerColor(2)
    h_mu.SetMinimum(-0.003)
    h_mu.SetMaximum(0.003)
    h_mu.SetMarkerStyle(kFullCircle)
    TGaxis.SetMaxDigits(3)
    h_mu.DrawCopy("E1")
    texJpsiPiMC1.DrawLatex(0.25, 0.80, "LHCb simulation")
    texJpsiPiMC1.DrawLatex(0.25, 0.75, "#sqrt{s}=13 TeV")
    f_mu.Draw("same")

    #legend.Draw()
    
    c.cd(3)
    gPad.SetTopMargin(0.07)
    h_rS.GetXaxis().SetTitle("p_{T}, GeV/c")
    h_rS.GetYaxis().SetTitle("S_{w}/S_{n}")
    h_rS.SetLineWidth(2)
    h_rS.SetLineColor(1)
    h_rS.SetMarkerColor(2)
    h_rS.SetMinimum(2.7)
    h_rS.SetMarkerStyle(kFullCircle)
    h_rS.DrawCopy("E1 PMC")
    #f_rS.Draw("same")
    texJpsiPiMC1.DrawLatex(0.25, 0.80, "LHCb simulation")
    texJpsiPiMC1.DrawLatex(0.25, 0.75, "#sqrt{s}=13 TeV")
    f_rs.Draw("same")

    c.cd(4)
    gPad.SetTopMargin(0.07)
    h_beta.GetXaxis().SetTitle("p_{T}, GeV/c")
    h_beta.GetYaxis().SetTitle("#beta")
    h_beta.SetLineWidth(2)
    h_beta.SetLineColor(1)
    h_beta.SetMarkerColor(2)
    h_beta.SetMinimum(0.94)
    h_beta.SetMaximum(1.0)
    h_beta.SetMarkerStyle(kFullCircle)
    h_beta.DrawCopy("E1 PMC")
    #fBeta.Draw("same")
    texJpsiPiMC1.DrawLatex(0.25, 0.80, "LHCb simulation")
    texJpsiPiMC1.DrawLatex(0.25, 0.75, "#sqrt{s}=13 TeV")
    f_beta.Draw("same")


    c.SaveAs("PT_dist_tz"+add+".pdf")
    
    c0 = TCanvas("tau_Fit","tau Fit",800,600)
    
    c0.cd()
    gPad.SetLeftMargin(0.20)
    gPad.SetBottomMargin(0.15)

    h_tauB.GetXaxis().SetTitle("p_{T}, GeV/c")
    h_tauB.GetYaxis().SetTitle("#tau_{B}, ps")
    h_tauB.SetLineWidth(2)
    h_tauB.SetLineColor(1)
    h_tauB.SetMarkerColor(2)
    h_tauB.SetMarkerStyle(kFullCircle)
    h_tauB.DrawCopy("E1 PMC")
    fTauB.Draw("same")
    texJpsiPiMC1.DrawLatex(0.25, 0.35, "LHCb simulation")
    texJpsiPiMC1.DrawLatex(0.25, 0.3, "#sqrt{s}=13 TeV")
    #f_tauB.Draw("same")
    
    c0.SaveAs("PT_dist_tau_tz"+add+".pdf")
    
    fout = TFile (homeDir+"Results/MC/TzFit/parFit"+add+".root", "recreate")
    h_sigma_tz.Write()
    h_tauB.Write()
    fSigma.Write()
    fSigmaAlt.Write()
    fTauB.Write()
    f_rS.Write()
    fBeta.Write()
    h_mu.Write()
    h_mu_mean.Write()
    c.Write()
    fout.Close()
        
  
def tauFit():


    gStyle.SetOptTitle(0)
    gStyle.SetOptStat(0)
    nPTBins = 7
    
    add = "_newMC"
    
    #pt = array( 'f',[0., 4, 7.,10., 13.,16., 20., 30])
    #pt = array( 'f',[6.4, 7., 8., 9., 10., 12., 14., 16., 19., 30])
    pt = array( 'f',[6.4, 8, 10., 12., 14., 16., 19., 25])
    nPTBins = len(pt)-1
    ptMin = pt[0]
    ptMax = pt[nPTBins]
    
    h_tauB = TH1D("h_tauB","#tau_{B}",nPTBins,pt)
    

    f = TFile(homeDir+"Results/MC/TzFit/MC_TRUE_Tz"+add+"_2016_wksp.root","READ") 
    wMC = f.Get("w")
    f.Close()

    tauB = wMC.var("tauB").getValV()
    f_tauB = TF1("f_tauB","pol0",ptMin,ptMax)
    f_tauB.SetParameter(0,tauB)
    f_tauB.SetLineColor(2)

    
    for iPT in range(1, nPTBins+1): 
    
        f = TFile(homeDir+"Results/MC/TzFit/MC_TRUE_Tz"+add+"_2016_wksp_PT{}.root".format(iPT),"READ") 
        wMC = f.Get("w")
        f.Close()

        h_tauB.SetBinContent(iPT,wMC.var("tauB").getValV())
        h_tauB.SetBinError(iPT,wMC.var("tauB").getError())
    
    fTauB = TF1("fTauB","pol1",ptMin,ptMax)
    fTauB_alt = TF1("fTauB_alt","pol1",6.5,18.)
    
    h_tauB.Fit(fTauB,"I")
    h_tauB.Fit(fTauB_alt,"IR")

    texJpsiPiMC1 = TLatex()
    texJpsiPiMC1.SetNDC()
    
    c0 = TCanvas("tau_Fit","tau Fit",800,600)
    
    c0.cd()
    gPad.SetLeftMargin(0.20)
    gPad.SetBottomMargin(0.15)

    h_tauB.GetXaxis().SetTitle("p_{T}, GeV/c")
    h_tauB.GetYaxis().SetTitle("#tau_{B}, ps")
    h_tauB.SetLineWidth(2)
    h_tauB.SetLineColor(1)
    h_tauB.SetMarkerColor(2)
    h_tauB.SetMarkerStyle(kFullCircle)
    h_tauB.DrawCopy("E1 PMC")
    #h_tauB.SetMinimum(0.)
    fTauB.Draw("same")
    fTauB_alt.SetLineColor(4)
    fTauB_alt.Draw("same")
    texJpsiPiMC1.DrawLatex(0.63, 0.77, "LHCb simulation")
    texJpsiPiMC1.DrawLatex(0.63, 0.71, "#sqrt{s}=13 TeV")
    f_tauB.Draw("same")
    
    c0.SaveAs("PT_dist_tau_tz"+add+".pdf")
    
    fout = TFile (homeDir+"Results/MC/TzFit/tauFit"+add+".root", "recreate")
    h_tauB.Write()
    fTauB.Write()
    fTauB_alt.Write()
    c0.Write()
    fout.Close()
        
gROOT.LoadMacro("../lhcbStyle.C")
#tz_pars()
tauFit()