from ROOT import *
from array import array
import numpy as np

gROOT.LoadMacro("../libRooRelBreitWigner/RooRelBreitWigner.cxx+");
gROOT.LoadMacro("../BifurcatedCB/BifurcatedCB.cxx+");
homeDir = "/users/LHCb/zhovkovska/scripts/"

def mass_pars(gauss=True):
  
  
    gROOT.ProcessLine('gStyle->SetOptStat(000000000)')
    gROOT.ProcessLine('gStyle->SetOptTitle(0)')

    nPTBins = 4
        
    if gauss: add = ""
    else: add = "_CB"
    
    pt = array( 'f',[6.5, 8.0, 10.0, 12.0, 14.])
    ptMax = 14.0
    ptMin = 6.5


    f = TFile(homeDir+"Results/MC/MassFit/MC_MassResolution%s_2016_wksp.root"%(add),"READ") 
    wMC = f.Get("w")
    f.Close()
    
    sigma = wMC.var("sigma_eta_1").getValV()
    relEtac2Jpsi = wMC.var("rEtaToJpsi").getValV()
    relG12G2 = wMC.var("rG1toG2").getValV()
    relN2W = wMC.var("rNarToW").getValV()

    f_sigma_mean = TF1("f_sigma_mean","pol0", ptMin, ptMax)
    f_sigma_mean.SetParameter(0, sigma)
    f_sigma_mean.SetLineColor(2)
    f_rel_mean = TF1("f_rel_mean","pol0", ptMin, ptMax)
    f_rel_mean.SetParameter(0, relEtac2Jpsi)
    f_rel_mean.SetLineColor(2)
    f_relRes_mean = TF1("f_relRes_mean","pol0", ptMin, ptMax)
    f_relRes_mean.SetParameter(0, relN2W)
    f_relRes_mean.SetLineColor(2)
    f_relYield_mean = TF1("f_relYield_mean","pol0", ptMin, ptMax)
    f_relYield_mean.SetParameter(0, relG12G2)
    f_relYield_mean.SetLineColor(2)

    
    h_sigma_mass = TH1D("h_sigma_mass","#sigma_{mass #eta_{c}} DATA",nPTBins, pt)
    
    h_sigma_mass_MC = TH1D("h_sigma_mass_MC","#sigma_{mass #eta_{c}} MC",nPTBins,pt)
    h_sigma_mass_MC_fromB = TH1D("h_sigma_mass_MC_fromB","#sigma_{mass #eta_{c}} MC from-b",nPTBins,pt)
    h_rEtacToJpsi = TH1D("h_rEtacToJpsi","#sigma_{#eta_{c}}/#sigma_{J/#psi} MC",nPTBins,pt)
    h_rNarToW = TH1D("h_rNarToW","#sigma_{Narr}/#sigma_{W} MC",nPTBins,pt)
    h_rG1toG2 = TH1D("h_rG1toG2","N_{Narr}/N_{W} MC",nPTBins,pt)
      
  
    for iPT in range(1, nPTBins+1):

        f = TFile(homeDir+"Results/MC/MassFit/MC_MassResolution{}_2016_wksp_PT{}.root".format(add,iPT),"READ") 
        wMC = f.Get("w")
        f.Close()
        
        h_sigma_mass_MC.SetBinContent(iPT,wMC.var("sigma_eta_1").getValV())
        h_sigma_mass_MC.SetBinError(iPT,wMC.var("sigma_eta_1").getError())
        
        h_rEtacToJpsi.SetBinContent(iPT,wMC.var("rEtaToJpsi").getValV())
        h_rNarToW.SetBinContent(iPT,wMC.var("rNarToW").getValV())
        h_rG1toG2.SetBinContent(iPT,wMC.var("rG1toG2").getValV())
        h_rEtacToJpsi.SetBinError(iPT,wMC.var("rEtaToJpsi").getError())
        h_rNarToW.SetBinError(iPT,wMC.var("rNarToW").getError())
        h_rG1toG2.SetBinError(iPT,wMC.var("rG1toG2").getError())
        
        f = TFile(homeDir+"Results/MassFit/fromB/Jpsi_PT/Wksp_MassFit_PT{}_Tz0_C0.root".format(iPT),"READ") 
        wMC = f.Get("w")
        f.Close()
        
        h_sigma_mass.SetBinContent(iPT,wMC.var("sigma_eta_1").getValV())
        h_sigma_mass.SetBinError(iPT,wMC.var("sigma_eta_1").getError())
    

        f = TFile(homeDir+"Results/MC/MassFit/MC_MassResolution_fromB_cuted_2016_wksp_PT{}.root".format(iPT),"READ") 
        wMC = f.Get("w")
        f.Close()

        h_sigma_mass_MC_fromB.SetBinContent(iPT,wMC.var("sigma_eta_1").getValV())
        h_sigma_mass_MC_fromB.SetBinError(iPT,wMC.var("sigma_eta_1").getError())
        
  
    fSigmaDATA = TF1("fSigmaDATA","pol1", ptMin, ptMax)
    fSigmaMC = TF1("fSigmaMC","pol1", ptMin, ptMax)
    fRel = TF1("fRel","pol1", ptMin, ptMax)
    fRelRes = TF1("fRelRes","pol1", ptMin, ptMax)
    fRelYield = TF1("fRelYield","pol1", ptMin, ptMax)
    
    fSigmaMC.SetLineColor(2)
    fRel.SetLineColor(2)

    h_sigma_mass.Fit(fSigmaDATA, "I")
    h_sigma_mass_MC.Fit(fSigmaMC, "I")
    #h_rEtacToJpsi.Fit(fRel, "I")
    h_rNarToW.Fit(fRelRes, "I")
    h_rG1toG2.Fit(fRelYield, "I")
    
    leg = TLegend(0.5,0.25,0.88,0.45)
    leg.AddEntry(0, "LHCb preliminary", "")
    leg.AddEntry(0, "#sqrt{s}=13 TeV", "")
    leg.SetLineWidth(0)

    c0 = TCanvas("Masses_Fit","Masses Fit",1200,900)
    
    c0.cd(1)
    gPad.SetLeftMargin(0.15)
    gPad.SetBottomMargin(0.15)
    h_sigma_mass.GetXaxis().SetTitle("p_{T}, GeV/c")
    h_sigma_mass.GetYaxis().SetTitle("#sigma_{#eta_{c}} / [GeV/c^{2}]")
    h_sigma_mass.SetLineWidth(2)
    h_sigma_mass.SetLineColor(1)
    h_sigma_mass.SetMarkerColor(2)
    h_sigma_mass.SetMarkerStyle(kFullCircle)
    h_sigma_mass.GetXaxis().SetTitleSize(0.07)
    h_sigma_mass.GetYaxis().SetTitleSize(0.07)
    h_sigma_mass.GetXaxis().SetTitleOffset(0.90)
    h_sigma_mass.GetYaxis().SetTitleOffset(0.90)
    h_sigma_mass.GetXaxis().SetTitleFont(12)
    h_sigma_mass.GetYaxis().SetTitleFont(12)
    h_sigma_mass.GetXaxis().SetLabelSize(0.05)
    h_sigma_mass.GetYaxis().SetLabelSize(0.05)
    h_sigma_mass.GetXaxis().SetLabelFont(62)
    h_sigma_mass.GetYaxis().SetLabelFont(62)
    h_sigma_mass.GetXaxis().SetNdivisions(510)
    h_sigma_mass.DrawCopy("E1")
    
    TGaxis.SetMaxDigits(3)
    fSigmaDATA.Draw("same")
    fSigmaDATA.SetLineColor(2)
    leg.Draw()

    texJpsiPiMC1 = TLatex()
    texJpsiPiMC1.SetNDC()

    c0.SaveAs("PT_dist_mass_DATA.pdf")
    
    c = TCanvas("Masses_Fit MC","Masses Fit MC",1200,900)
    c.Divide(2,2)
    #  c.cd(1).SetPad(.005, .405, .995, .995)
    #  gPad.SetLeftMargin(0.15)  frame.GetXaxis().SetTitle("J/#psi mass / [MeV/c^{2]")  frame.Draw()
    #  c.cd(2).SetPad(.005, .005, .995, .395)
    #  gPad.SetLeftMargin(0.15)  frame_res.GetXaxis().SetTitle("J/#psi mass / [MeV/c^{2]")  frame_res.Draw()

    c.cd(1)
    #h_sigma_mass_MC.GetXaxis().SetTitle("p_{T}, MeV/c")
    #h_sigma_mass_MC.GetYaxis().SetTitle("#sigma_{#eta_{c}}, MeV/c^{2}")
    #h_sigma_mass_MC_fromB.SetLineWidth(2)
    #h_sigma_mass_MC_fromB.SetLineColor(1)
    #h_sigma_mass_MC_fromB.SetMarkerColor(4)
    #h_sigma_mass_MC_fromB.SetMarkerStyle(kFullCircle)
    #h_sigma_mass_MC_fromB.DrawCopy("E1")
    #fSigmaMC.Draw("same")
    gPad.SetLeftMargin(0.15)
    gPad.SetBottomMargin(0.15)
    h_sigma_mass_MC.GetXaxis().SetTitle("p_{T}, GeV/c")
    h_sigma_mass_MC.GetYaxis().SetTitle("#sigma_{#eta_{c}}, MeV/c^{2}")
    h_sigma_mass_MC.SetLineWidth(2)
    h_sigma_mass_MC.SetLineColor(1)
    h_sigma_mass_MC.SetMarkerColor(2)
    h_sigma_mass_MC.SetMarkerStyle(kFullCircle)
    h_sigma_mass_MC.SetMinimum(6.95)
    h_sigma_mass_MC.SetMaximum(8.05)
    h_sigma_mass_MC.GetXaxis().SetNdivisions(510)
    h_sigma_mass_MC.DrawCopy("0E1")
    texJpsiPiMC1.DrawLatex(0.25, 0.80, "LHCb simulation")
    texJpsiPiMC1.DrawLatex(0.25, 0.75, "#sqrt{s}=13 TeV")
    #f_sigma_mean.Draw("same")

    c.cd(2)
    gPad.SetLeftMargin(0.15)
    gPad.SetBottomMargin(0.15)
    h_rEtacToJpsi.GetXaxis().SetTitle("p_{T}, GeV/c")
    h_rEtacToJpsi.GetYaxis().SetTitle("#sigma_{#eta_{c}}/#sigma_{J/#psi}")
    h_rEtacToJpsi.SetLineWidth(2)
    h_rEtacToJpsi.SetLineColor(1)
    h_rEtacToJpsi.SetMarkerColor(2)
    h_rEtacToJpsi.SetMinimum(0.87)
    h_rEtacToJpsi.SetMaximum(1.0)
    h_rEtacToJpsi.SetMarkerStyle(kFullCircle)
    h_rEtacToJpsi.DrawCopy("E1  0")
    #fRel.Draw("same")
    texJpsiPiMC1.DrawLatex(0.25, 0.80, "LHCb simulation")
    texJpsiPiMC1.DrawLatex(0.25, 0.75, "#sqrt{s}=13 TeV")
    f_rel_mean.Draw("same")
    
    c.cd(3)
    gPad.SetLeftMargin(0.15)
    gPad.SetBottomMargin(0.15)
    h_rNarToW.GetXaxis().SetTitle("p_{T}, GeV/c")
    h_rNarToW.GetYaxis().SetTitle("#sigma_{n}/#sigma_{w}")
    h_rNarToW.SetLineWidth(2)
    h_rNarToW.SetLineColor(1)
    h_rNarToW.SetMarkerColor(2)
    h_rNarToW.SetMinimum(0.125)
    h_rNarToW.SetMaximum(0.26)
    h_rNarToW.SetMarkerStyle(kFullCircle)
    h_rNarToW.DrawCopy("E1 0")
    #fRelRes.Draw("same")
    texJpsiPiMC1.DrawLatex(0.25, 0.35, "LHCb simulation")
    texJpsiPiMC1.DrawLatex(0.25, 0.3, "#sqrt{s}=13 TeV")
    f_relRes_mean.Draw("same")

    c.cd(4)
    gPad.SetLeftMargin(0.15)
    gPad.SetBottomMargin(0.15)
    h_rG1toG2.GetXaxis().SetTitle("p_{T}, GeV/c")
    h_rG1toG2.GetYaxis().SetTitle("f_{n}")
    h_rG1toG2.SetLineWidth(2)
    h_rG1toG2.SetLineColor(1)
    h_rG1toG2.SetMarkerColor(2)
    h_rG1toG2.SetMinimum(0.915)
    h_rG1toG2.SetMaximum(0.97)
    h_rG1toG2.SetMarkerStyle(kFullCircle)
    h_rG1toG2.DrawCopy("E1")
    #fRelYield.Draw("same")
    texJpsiPiMC1.DrawLatex(0.25, 0.35, "LHCb simulation")
    texJpsiPiMC1.DrawLatex(0.25, 0.3, "#sqrt{s}=13 TeV")
    f_relYield_mean.Draw("same")

    fout = TFile(homeDir+"Results/MC/MassFit/parFit%s_GeV.root"%(add), "recreate")
    h_sigma_mass.Write()
    h_rEtacToJpsi.Write()
    h_rNarToW.Write()
    h_rG1toG2.Write()
    fSigmaDATA.Write()
    fSigmaMC.Write()
    fRel.Write()
    #fRelRes.Write()
    #fRelYield.Write()
    c.Write()
    fout.Close()
    
    c.SaveAs("PT_dist_mass%s.pdf"%(add))

def mass_parsCB():
  
  
    gROOT.ProcessLine('gStyle->SetOptStat(000000000)')
    gROOT.ProcessLine('gStyle->SetOptTitle(0)')

    nPTBins = 4
        
    add = "_CB"
    
    pt = array( 'f',[6.5, 8.0, 10.0, 12.0, 14.])
    ptMax = 14.0
    ptMin = 6.5


    f = TFile(homeDir+"Results/MC/MassFit/MC_MassResolution%s_2016_wksp.root"%(add),"READ") 
    wMC = f.Get("w")
    f.Close()
    
    sigma = wMC.var("sigma_eta_1").getValV()
    relEtac2Jpsi = wMC.var("rEtaToJpsi").getValV()
    #relG12G2 = wMC.var("rG1toG2").getValV()
    alpha_1 = wMC.var("alpha_eta_1").getValV()
    n_1 = wMC.var("n_eta_1").getValV()

    f_sigma_mean = TF1("f_sigma_mean","pol0", ptMin, ptMax)
    f_sigma_mean.SetParameter(0, sigma)
    f_sigma_mean.SetLineColor(2)
    f_rel_mean = TF1("f_rel_mean","pol0", ptMin, ptMax)
    f_rel_mean.SetParameter(0, relEtac2Jpsi)
    f_rel_mean.SetLineColor(2)
    #f_relYield_mean = TF1("f_relYield_mean","pol0", ptMin, ptMax)
    #f_relYield_mean.SetParameter(0, relG12G2)
    #f_relYield_mean.SetLineColor(2)
    f_alpha_mean = TF1("f_alpha_mean","pol0", ptMin, ptMax)
    f_alpha_mean.SetParameter(0, alpha_1)
    f_alpha_mean.SetLineColor(2)
    f_n_mean = TF1("f_n_mean","pol0", ptMin, ptMax)
    f_n_mean.SetParameter(0, n_1)
    f_n_mean.SetLineColor(2)

    
    h_sigma_mass = TH1D("h_sigma_mass","#sigma_{mass #eta_{c}} DATA",nPTBins, pt)
    
    h_sigma_mass_MC = TH1D("h_sigma_mass_MC","#sigma_{mass #eta_{c}} MC",nPTBins,pt)
    h_sigma_mass_MC_fromB = TH1D("h_sigma_mass_MC_fromB","#sigma_{mass #eta_{c}} MC from-b",nPTBins,pt)
    h_rEtacToJpsi = TH1D("h_rEtacToJpsi","#sigma_{#eta_{c}}/#sigma_{J/#psi} MC",nPTBins,pt)
    #h_rG1toG2 = TH1D("h_rG1toG2","N_{Narr}/N_{W} MC",nPTBins,pt)
    h_alpha = TH1D("h_alpha","#alpha",nPTBins,pt)
    h_n = TH1D("h_n","n",nPTBins,pt)
      
  
    for iPT in range(1, nPTBins+1):

        f = TFile(homeDir+"Results/MC/MassFit/MC_MassResolution{}_2016_wksp_PT{}.root".format(add,iPT),"READ") 
        wMC = f.Get("w")
        f.Close()
        
        h_sigma_mass_MC.SetBinContent(iPT,wMC.var("sigma_eta_1").getValV())
        h_sigma_mass_MC.SetBinError(iPT,wMC.var("sigma_eta_1").getError())
        
        h_rEtacToJpsi.SetBinContent(iPT,wMC.var("rEtaToJpsi").getValV())
        #h_rG1toG2.SetBinContent(iPT,wMC.var("rG1toG2").getValV())
        h_alpha.SetBinContent(iPT,wMC.var("alpha_eta_1").getValV())
        h_n.SetBinContent(iPT,wMC.var("n_eta_1").getValV())
        h_rEtacToJpsi.SetBinError(iPT,wMC.var("rEtaToJpsi").getError())
        #h_rG1toG2.SetBinError(iPT,wMC.var("rG1toG2").getError())
        h_alpha.SetBinError(iPT,wMC.var("alpha_eta_1").getError())
        h_n.SetBinError(iPT,wMC.var("n_eta_1").getError())
        
        #f = TFile(homeDir+"Results/MassFit/fromB/Jpsi_PT/Wksp_MassFit_PT{}_Tz0_C0.root".format(iPT),"READ") 
        #wMC = f.Get("w")
        #f.Close()
        
        #h_sigma_mass.SetBinContent(iPT,wMC.var("sigma_eta_1").getValV())
        #h_sigma_mass.SetBinError(iPT,wMC.var("sigma_eta_1").getError())
    

        
  
    fSigmaDATA = TF1("fSigmaDATA","pol1", ptMin, ptMax)
    fSigmaMC = TF1("fSigmaMC","pol1", ptMin, ptMax)
    fRel = TF1("fRel","pol1", ptMin, ptMax)
    fRelRes = TF1("fRelRes","pol1", ptMin, ptMax)
    fRelYield = TF1("fRelYield","pol1", ptMin, ptMax)
    
    fSigmaMC.SetLineColor(2)
    fRel.SetLineColor(2)

    #h_sigma_mass.Fit(fSigmaDATA, "I")
    h_sigma_mass_MC.Fit(fSigmaMC, "I")
    #h_rEtacToJpsi.Fit(fRel, "I")
    #h_rG1toG2.Fit(fRelYield, "I")
    #h_alpha.Fit(fRelRes, "I")
    #h_n.Fit(fRelRes, "I")
    
    leg = TLegend(0.5,0.25,0.88,0.45)
    leg.AddEntry(0, "LHCb preliminary", "")
    leg.AddEntry(0, "#sqrt{s}=13 TeV", "")
    leg.SetLineWidth(0)

    #c0 = TCanvas("Masses_Fit","Masses Fit",1200,900)
    
    #c0.cd(1)
    #gPad.SetLeftMargin(0.15)
    #gPad.SetBottomMargin(0.15)
    #h_sigma_mass.GetXaxis().SetTitle("p_{T}, GeV/c")
    #h_sigma_mass.GetYaxis().SetTitle("#sigma_{mass #eta_{c}} / [GeV/c^{2}]")
    #h_sigma_mass.SetLineWidth(2)
    #h_sigma_mass.SetLineColor(1)
    #h_sigma_mass.SetMarkerColor(2)
    #h_sigma_mass.SetMarkerStyle(kFullCircle)
    #h_sigma_mass.GetXaxis().SetTitleSize(0.07)
    #h_sigma_mass.GetYaxis().SetTitleSize(0.07)
    #h_sigma_mass.GetXaxis().SetTitleOffset(0.90)
    #h_sigma_mass.GetYaxis().SetTitleOffset(0.90)
    #h_sigma_mass.GetXaxis().SetTitleFont(12)
    #h_sigma_mass.GetYaxis().SetTitleFont(12)
    #h_sigma_mass.GetXaxis().SetLabelSize(0.05)
    #h_sigma_mass.GetYaxis().SetLabelSize(0.05)
    #h_sigma_mass.GetXaxis().SetLabelFont(62)
    #h_sigma_mass.GetYaxis().SetLabelFont(62)
    #h_sigma_mass.GetXaxis().SetNdivisions(510)
    #h_sigma_mass.DrawCopy("E1")
    
    #TGaxis.SetMaxDigits(3)
    #fSigmaDATA.Draw("same")
    #leg.Draw()

    texJpsiPiMC1 = TLatex()
    texJpsiPiMC1.SetNDC()

    #c0.SaveAs("PT_dist_mass_DATA.pdf")
    
    c = TCanvas("Masses_Fit MC","Masses Fit MC",1200,900)
    c.Divide(2,2)
    #  c.cd(1).SetPad(.005, .405, .995, .995)
    #  gPad.SetLeftMargin(0.15)  frame.GetXaxis().SetTitle("J/#psi mass / [MeV/c^{2]")  frame.Draw()
    #  c.cd(2).SetPad(.005, .005, .995, .395)
    #  gPad.SetLeftMargin(0.15)  frame_res.GetXaxis().SetTitle("J/#psi mass / [MeV/c^{2]")  frame_res.Draw()

    c.cd(1)
    #h_sigma_mass_MC.GetXaxis().SetTitle("p_{T}, MeV/c")
    #h_sigma_mass_MC.GetYaxis().SetTitle("#sigma_{#eta_{c}}, MeV/c^{2}")
    #h_sigma_mass_MC_fromB.SetLineWidth(2)
    #h_sigma_mass_MC_fromB.SetLineColor(1)
    #h_sigma_mass_MC_fromB.SetMarkerColor(4)
    #h_sigma_mass_MC_fromB.SetMarkerStyle(kFullCircle)
    #h_sigma_mass_MC_fromB.DrawCopy("E1")
    #fSigmaMC.Draw("same")
    gPad.SetLeftMargin(0.15)
    gPad.SetBottomMargin(0.15)
    h_sigma_mass_MC.GetXaxis().SetTitle("p_{T}, GeV/c")
    h_sigma_mass_MC.GetYaxis().SetTitle("#sigma_{#eta_{c}}, MeV/c^{2}")
    h_sigma_mass_MC.SetLineWidth(2)
    h_sigma_mass_MC.SetLineColor(1)
    h_sigma_mass_MC.SetMarkerColor(2)
    h_sigma_mass_MC.SetMarkerStyle(kFullCircle)
    #h_sigma_mass_MC.SetMinimum(6.5)
    #h_sigma_mass_MC.SetMaximum(8.5)
    h_sigma_mass_MC.GetXaxis().SetNdivisions(510)
    h_sigma_mass_MC.DrawCopy("0E1")
    texJpsiPiMC1.DrawLatex(0.25, 0.80, "LHCb simulation")
    texJpsiPiMC1.DrawLatex(0.25, 0.75, "#sqrt{s}=13 TeV")
    f_sigma_mean.Draw("same")

    c.cd(2)
    gPad.SetLeftMargin(0.15)
    gPad.SetBottomMargin(0.15)
    h_rEtacToJpsi.GetXaxis().SetTitle("p_{T}, GeV/c")
    h_rEtacToJpsi.GetYaxis().SetTitle("#sigma_{#eta_{c}}/#sigma_{J/#psi}")
    h_rEtacToJpsi.SetLineWidth(2)
    h_rEtacToJpsi.SetLineColor(1)
    h_rEtacToJpsi.SetMarkerColor(2)
    #h_rEtacToJpsi.SetMinimum(0.85)
    h_rEtacToJpsi.SetMarkerStyle(kFullCircle)
    h_rEtacToJpsi.DrawCopy("E1  0")
    #fRel.Draw("same")
    texJpsiPiMC1.DrawLatex(0.25, 0.80, "LHCb simulation")
    texJpsiPiMC1.DrawLatex(0.25, 0.75, "#sqrt{s}=13 TeV")
    f_rel_mean.Draw("same")
    

    #c.cd(3)
    #gPad.SetLeftMargin(0.15)
    #gPad.SetBottomMargin(0.15)
    #h_rG1toG2.GetXaxis().SetTitle("p_{T}, GeV/c")
    #h_rG1toG2.GetYaxis().SetTitle("f_{n}")
    #h_rG1toG2.SetLineWidth(2)
    #h_rG1toG2.SetLineColor(1)
    #h_rG1toG2.SetMarkerColor(2)
    #h_rG1toG2.SetMinimum(0.85)
    ##h_rG1toG2.SetMaximum(0.98)
    #h_rG1toG2.SetMarkerStyle(kFullCircle)
    #h_rG1toG2.DrawCopy("E1")
    ##fRelYield.Draw("same")
    #texJpsiPiMC1.DrawLatex(0.25, 0.35, "LHCb simulation")
    #texJpsiPiMC1.DrawLatex(0.25, 0.3, "#sqrt{s}=13 TeV")
    #f_relYield_mean.Draw("same")

    c.cd(3)
    gPad.SetLeftMargin(0.15)
    gPad.SetBottomMargin(0.15)
    h_alpha.GetXaxis().SetTitle("p_{T}, GeV/c")
    h_alpha.GetYaxis().SetTitle("#alpha")
    h_alpha.SetLineWidth(2)
    h_alpha.SetLineColor(1)
    h_alpha.SetMarkerColor(2)
    h_alpha.SetMinimum(0.08)
    h_alpha.SetMarkerStyle(kFullCircle)
    h_alpha.DrawCopy("E1 0")
    #fRelRes.Draw("same")
    texJpsiPiMC1.DrawLatex(0.25, 0.35, "LHCb simulation")
    texJpsiPiMC1.DrawLatex(0.25, 0.3, "#sqrt{s}=13 TeV")
    f_alpha_mean.Draw("same")


    c.cd(4)
    gPad.SetLeftMargin(0.15)
    gPad.SetBottomMargin(0.15)
    h_n.GetXaxis().SetTitle("p_{T}, GeV/c")
    h_n.GetYaxis().SetTitle("n")
    h_n.SetLineWidth(2)
    h_n.SetLineColor(1)
    h_n.SetMarkerColor(2)
    h_n.SetMinimum(0.08)
    h_n.SetMarkerStyle(kFullCircle)
    h_n.DrawCopy("E1 0")
    #fRelRes.Draw("same")
    texJpsiPiMC1.DrawLatex(0.25, 0.35, "LHCb simulation")
    texJpsiPiMC1.DrawLatex(0.25, 0.3, "#sqrt{s}=13 TeV")
    f_n_mean.Draw("same")


    fout = TFile(homeDir+"Results/MC/MassFit/parFit%s_GeV.root"%(add), "recreate")
    h_sigma_mass.Write()
    h_rEtacToJpsi.Write()
    h_alpha.Write()
    h_n.Write()
    #h_rG1toG2.Write()
    #fSigmaDATA.Write()
    fSigmaMC.Write()
    fRel.Write()
    #fRelRes.Write()
    #fRelYield.Write()
    c.Write()
    fout.Close()
    
    c.SaveAs("PT_dist_mass%s.pdf"%(add))

def gamma_pars():

    nPTBins = 5
    
    sigma_mass = nPTBins*[0]
    gamma = nPTBins*[0]
    
    sigma_mass_err = nPTBins*[0]
    gamma_err = nPTBins*[0]
    
#    ptMass = [6.5,7.0, 8.0,9.0, 10.0, 11.0, 12.0, 13.0, 14.0, 16.0, 18.]
#    pt = [6.5, 8.0, 10.0, 12.0, 14.0, 18.]
    pt = [2.0, 3.0, 4.0, 5.0, 6.5, 18.]
    ptMax = 18.
    ptMin = 6.5
    
    h_sigma_mass = TH1D("h_sigma_mass","#sigma_{mass J/#psi}",nPTBins,pt)
    h_gamma = TH1D("h_gamma","#Gamma_{#eta_{c}}",nPTBins,pt)
    
    
    for iPT in range(1, nPTBins+1):

#        sprintf(nameWksp, "TotFitResult/massEtac/PT_distributions/Reduced_Pmt_2016_wksp_gamma_PT%d.root",iPT)
        f = TFile("TotFitResult/massEtac/ProtonPT_distributions/Reduced_Pmt_2016_wksp_gamma_proton_PT{}.root".format(iPT),"READ") 
        wMC = f.Get("w1")
        f.Close()
               
        h_gamma.SetBinContent(iPT,wMC.var("gamma_eta").getValV())
        h_gamma.SetBinError(iPT,wMC.var("gamma_eta").getError())
        gamma[iPT-1] = wMC.var("gamma_eta").getValV()
        gamma_err[iPT-1] = wMC.var("gamma_eta").getError()    
        
#        sprintf(nameWksp, "TotFitResult/massEtac/PT_distributions/Reduced_Pmt_2016_wksp_sep_PT%d.root",iPT)        
        f = TFile("TotFitResult/massEtac/ProtonPT_distributions/Reduced_Pmt_2016_wksp_sep_proton_PT{}.root".format(iPT),"READ") 
        wMC = f.Get("w1")
        f.Close()
        
        h_sigma_mass.SetBinContent(iPT,wMC.var("sigma_Jpsi_1").getValV())
        h_sigma_mass.SetBinError(iPT,wMC.var("sigma_Jpsi_1").getError())
        sigma_mass[iPT-1] = wMC.var("sigma_Jpsi_1").getValV()
        sigma_mass_err[iPT-1] = wMC.var("sigma_Jpsi_1").getError()    
        
        
    
    fSigma = TF1("fSigma","pol1",ptMin,ptMax)
    fGamma = TF1("fRel","pol1",ptMin,ptMax)
    
    h_sigma_mass.Fit(fSigma, "I")
    h_gamma.Fit(fGamma, "I")
    
    c = TCanvas("Masses_Fit","Masses Fit",600,800)
    c.Divide(1,2)
    #  c.cd(1).SetPad(.005, .405, .995, .995)
    #  gPad.SetLeftMargin(0.15)  frame.GetXaxis().SetTitle("J/#psi mass / [MeV/c^{2]")  frame.Draw()
    #  c.cd(2).SetPad(.005, .005, .995, .395)
    #  gPad.SetLeftMargin(0.15)  frame_res.GetXaxis().SetTitle("J/#psi mass / [MeV/c^{2]")  frame_res.Draw()
    
    c.cd(1)
    h_sigma_mass.GetXaxis().SetTitle("Proton p_{T}, MeV/c")
    h_sigma_mass.GetYaxis().SetTitle("#sigma_{mass J/#psi}, MeV/c^{2}")
    h_sigma_mass.SetLineWidth(2)
    h_sigma_mass.SetLineColor(1)
    h_sigma_mass.SetMarkerColor(2)
    h_sigma_mass.SetMarkerStyle(kFullCircle)
    h_sigma_mass.DrawCopy("E1")
    fSigma.Draw("same")
    gPad.SetLogx() 
    
    
    c.cd(2)
    h_gamma.GetXaxis().SetTitle("Proton p_{T}, MeV/c")
    h_gamma.GetYaxis().SetTitle("#Gamma_{#eta_{c}}, MeV")
    h_gamma.SetLineWidth(2)
    h_gamma.SetLineColor(1)
    h_gamma.SetMarkerColor(2)
    h_gamma.SetMarkerStyle(kFullCircle)
    h_gamma.DrawCopy("E1")
    fGamma.Draw("same")
    gPad.SetLogx() 
    
    fout = TFile (homeDir+"Results/Data/parFit_gamma_protonPT.root", "recreate")
    h_sigma_mass.Write()
    h_gamma.Write()
    fSigma.Write()
    fGamma.Write()
    c.Write()
    fout.Close()
    
    c.SaveAs("PT_dist_gamma_protonPT.pdf")



def nRel_gamma():

    nGamma = 10
    
    NRel = nGamma*[0]
    NRel_err = nGamma*[0]
    
    gamma = [15.0, 18.0, 21.0, 24.0, 27.0, 30.0, 33.0, 36.0, 39.0, 42.0, 45.0]
    gammaMax = 45.0
    
    h_NRel = TH1D("h_NRel","N_{#eta_{c/N_{J/#psi",nGamma,gamma)
    h_NRel_mean = TH1D("h_NRel_mean","#mu total",1,15.0, gammaMax)
#    TH1Dh_mu_mean_MC("h_mu_mean_MC","#mu MC total",1,6.5, ptMax)
    
    
#    sprintf(nameWksp, homeDir+"Results/MC/TzFit/JpsiVsEtac/MC_2016_wksp.root")
#    f = TFile(nameWksp,"READ") 
#    wMC =  f.Get("w1")
#    f.Close()
#        
#    h_mu_mean.SetBinContent(1,-0.00154759)
#    h_mu_mean.SetBinError(1,0.00167596)
    
    for iGamma in range(1, nGamma+1):
        
        f = TFile("TotFitResult/massEtac/Reduced_Pmt_2016_wksp_gamma_{}.root".format(iGamma),"READ") 
        wMC =  f.Get("w1")
        f.Close()
        
        h_NRel.SetBinContent(iGamma,wMC.var("nEtacRel").getValV())
        h_NRel.SetBinError(iGamma,wMC.var("nEtacRel").getError())
        
        NRel[iGamma-1] = wMC.var("nEtacRel").getValV()
        NRel_err[iGamma-1] = wMC.var("nEtacRel").getError()    
    
    
    kdeSigma = TKDE(nGamma, NRel[0], 15.0, gammaMax, "kerneltype:gaussian", 1.0)
    
    #  fSigma = kdeSigma.GetFunction(10000)
    #  fTauB = kdeTauB.GetFunction(10000)
    
    fYield = TF1("fYield","pol1",15.0,gammaMax)
    
    h_NRel.Fit(fYield,"I")
    
    legend = TLegend(0.1,0.7,0.48,0.9)
    legend.SetHeader("relative yield of #eta_{c","C") #option "C" allows to center the header
    legend.AddEntry(h_NRel,"N_{#eta_{c/N_{J/#psi","lep")
#    legend.AddEntry(h_NRel_mean,"#mu_{data","f")
#    legend.AddEntry(h_mu_mean_MC,"#mu_{MC","f")
    
    c = TCanvas("Masses_Fit","Masses Fit",800,500)
    
    c.cd()
#    h_NRel_mean.GetYaxis().SetRangeUser(-0.004, 0.004)
#    h_NRel_mean.SetFillStyle(3002)
#    h_NRel_mean.SetFillColor(15)
#    h_NRel_mean.DrawCopy("E2")
#    
#    h_NRel_mean_MC.SetFillStyle(3003)
#    h_NRel_mean_MC.SetFillColor(4)
#    h_NRel_mean_MC.DrawCopy("E2 SAME")
    
    h_NRel.GetXaxis().SetTitle("gamma_{#eta_{c}}, MeV/c")
    h_NRel.SetLineWidth(2)
    h_NRel.SetLineColor(1)
    h_NRel.SetMarkerColor(2)
    h_NRel.SetMarkerStyle(kOpenCircle)
    h_NRel.DrawCopy("")
    
#    legend.Draw()
    
    fout = TFile ("TotFitResult/massEtac/yieldFit.root", "recreate")
    h_NRel.Write()
#    fYield.Write()
#    h_NRelmean.Write()
    c.Write()
    fout.Close()
    
    
    c.SaveAs("gamma_dist_yields.pdf")
    
gROOT.LoadMacro("../lhcbStyle.C")
#mass_pars()
#mass_pars(False)
mass_parsCB()
