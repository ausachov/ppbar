from ROOT import *
from ROOT.RooFit import *
import os


histosDir = "/users/LHCb/zhovkovska/scripts/Histos/m_scaled/"
tuples2015 = "/sps/lhcb/zhovkovska/Pmt2015_scaled/Reduced_Pmt_2015*.root"
tuples2016 = "/sps/lhcb/zhovkovska/Pmt2016_scaled/Reduced_Pmt_2016*.root"


cutApriori2015_Dict = {"DiProton":"Jpsi_Hlt1DiProtonDecision_TOS && Jpsi_Hlt2DiProtonDecision_TOS && \
                                  ProtonP_P>12.5e3 && ProtonM_P>12.5e3 && \
                                  ProtonP_PT>2000 && ProtonM_PT>2000 && \
                                  ProtonP_PT/ProtonP_P>0.0366 && ProtonM_PT/ProtonM_P>0.0366 && \
                                  ProtonP_PIDp>20 && ProtonM_PIDp>20 && \
                                  (ProtonP_PIDp-ProtonP_PIDK)>15 && (ProtonM_PIDp-ProtonM_PIDK)>15 && \
                                  ProtonP_TRACK_CHI2NDOF<2.5 && ProtonM_TRACK_CHI2NDOF<2.5 && \
                                  Jpsi_ENDVERTEX_CHI2<3.5 && \
                                  Jpsi_Y>2 && Jpsi_Y<4.5 && \
                                  Jpsi_PT>6500 && \
                                  ProtonP_TRACK_GhostProb<0.2 && ProtonP_TRACK_GhostProb<0.2 && \
                                  ProtonP_TRACK_CloneDist<0 && ProtonM_TRACK_CloneDist<0",
                       "Topo":    "Jpsi_L0HadronDecision_TOS && \
                                  (Jpsi_Hlt1TrackMVADecision_TOS || Jpsi_Hlt1TwoTrackMVADecision_TOS) && \
                                  (Jpsi_Hlt2Topo2BodyDecision_TOS || Jpsi_Hlt2Topo3BodyDecision_TOS || Jpsi_Hlt2Topo4BodyDecision_TOS) && \
                                  ProtonP_P>-2e3 && ProtonM_P>-2e3 && \
                                  ProtonP_PT>1000 && ProtonM_PT>1000 && \
                                  ProtonP_PIDp>15 && ProtonM_PIDp>15 && \
                                  (ProtonP_PIDp-ProtonP_PIDK)>10 && (ProtonM_PIDp-ProtonM_PIDK)>10 && \
                                  ProtonP_TRACK_CHI2NDOF<5 && ProtonM_TRACK_CHI2NDOF<5 && \
                                  ProtonP_IPCHI2_OWNPV>9 && ProtonM_IPCHI2_OWNPV>9 && \
                                  Jpsi_ENDVERTEX_CHI2<9 && \
                                  Jpsi_Y>2 && Jpsi_Y<4.5 && \
                                  Jpsi_FDCHI2_OWNPV>25 && \
                                  nSPDHits<600"
                      }

cutApriori2016_Dict = {"DiProton":"Jpsi_Hlt1DiProtonDecision_TOS && Jpsi_Hlt2CcDiHadronDiProtonDecision_TOS && \
                                  ProtonP_P>12.5e3 && ProtonM_P>12.5e3 && \
                                  ProtonP_PT>2000 && ProtonM_PT>2000 && \
                                  ProtonP_PT/ProtonP_P>0.0366 && ProtonM_PT/ProtonM_P>0.0366 && \
                                  ProtonP_PIDp>20 && ProtonM_PIDp>20 && \
                                  (ProtonP_PIDp-ProtonP_PIDK)>15 && (ProtonM_PIDp-ProtonM_PIDK)>15 && \
                                  ProtonP_TRACK_CHI2NDOF<2.5 && ProtonM_TRACK_CHI2NDOF<2.5 && \
                                  Jpsi_ENDVERTEX_CHI2<3.5 && \
                                  Jpsi_Y>2 && Jpsi_Y<4.5 && \
                                  Jpsi_PT>6500 && \
                                  ProtonP_TRACK_GhostProb<0.2 && ProtonP_TRACK_GhostProb<0.2 && \
                                  ProtonP_TRACK_CloneDist<0 && ProtonM_TRACK_CloneDist<0",
                       "Topo":    "Jpsi_L0HadronDecision_TOS && \
                                  (Jpsi_Hlt1TrackMVADecision_TOS || Jpsi_Hlt1TwoTrackMVADecision_TOS) && \
                                  (Jpsi_Hlt2Topo2BodyDecision_TOS || Jpsi_Hlt2Topo3BodyDecision_TOS || Jpsi_Hlt2Topo4BodyDecision_TOS) && \
                                  ProtonP_P>-2e3 && ProtonM_P>-2e3 && \
                                  ProtonP_PT>1000 && ProtonM_PT>1000 && \
                                  ProtonP_PIDp>15 && ProtonM_PIDp>15 && \
                                  (ProtonP_PIDp-ProtonP_PIDK)>10 && (ProtonM_PIDp-ProtonM_PIDK)>10 && \
                                  ProtonP_TRACK_CHI2NDOF<5 && ProtonM_TRACK_CHI2NDOF<5 && \
                                  ProtonP_IPCHI2_OWNPV>9 && ProtonM_IPCHI2_OWNPV>9 && \
                                  Jpsi_ENDVERTEX_CHI2<9 && \
                                  Jpsi_Y>2 && Jpsi_Y<4.5 && \
                                  Jpsi_FDCHI2_OWNPV>25 && \
                                  nSPDHits<600"
                      }


tzVar = "(3.3)*(Jpsi_ENDVERTEX_Z-Jpsi_OWNPV_Z)*Jpsi_MM/Jpsi_PZ"

cutsDict = {
    "all"       : "1",
    "all_l0TOS" : "Jpsi_L0HadronDecision_TOS",
    "all_l0TIS" : "Jpsi_L0HadronDecision_TIS",
    "secondary" : "("+tzVar + ">0.08) && (ProtonP_IPCHI2_OWNPV > 16) && (ProtonM_IPCHI2_OWNPV > 16)",
    "secondary_l0TOS" : "("+tzVar+">0.08) && (Jpsi_L0HadronDecision_TOS) && (ProtonP_IPCHI2_OWNPV > 16) && (ProtonM_IPCHI2_OWNPV > 16)",
    "secondary_tight_l0TOS" : "("+tzVar+">2.00) && (Jpsi_L0HadronDecision_TOS) && (ProtonP_IPCHI2_OWNPV > 16) && (ProtonM_IPCHI2_OWNPV > 16)",
    "secondary_middle_l0TOS" : "("+tzVar+">1.00) && (Jpsi_L0HadronDecision_TOS) && (ProtonP_IPCHI2_OWNPV > 16) && (ProtonM_IPCHI2_OWNPV > 16)",
    "secondary_soft_l0TOS" : "("+tzVar+">2.00) && (Jpsi_L0HadronDecision_TOS)",
    # "secondary_l0TIS" : "("+tzVar+">0.08) && (Jpsi_L0HadronDecision_TIS)",
    "prompt"    : tzVar + "<0.08",
    "prompt_l0TOS" : "("+tzVar+"<0.08) && (Jpsi_L0HadronDecision_TOS)",
    # "prompt_l0TIS" : "("+tzVar+"<0.08) && (Jpsi_L0HadronDecision_TIS)",
    # "IPCHI2_9"  : "1",
    # "IPCHI2_16" : "1",
    # "FD_25"  : "1",
    # "FD_100" : "1",
    # "secondary_RunI" : "("+tzVar + ">0.08)",
    # "secondary_RunI_l0TOS" : "("+tzVar+">0.08) && (Jpsi_L0HadronDecision_TOS)",
    # "secondary_RunI_l0TIS" : "("+tzVar+">0.08) && (Jpsi_L0HadronDecision_TIS)",
    # "prompt_RunI"    : tzVar + "<0.08",
    # "prompt_RunI_l0TOS" : "("+tzVar+"<0.08) && (Jpsi_L0HadronDecision_TOS)",
    # "prompt_RunI_l0TIS" : "("+tzVar+"<0.08) && (Jpsi_L0HadronDecision_TIS)"
    }


binningDict = {
    "Jpsi_PT":                      [6500, 8000, 10000, 12000, 14000, 18000],
    "Jpsi_ETA":                     [2.0, 2.45, 2.8, 3.15, 4.5],
    "TMath::Abs(ProtonM_CosTheta)": [0.0, 0.15, 0.3, 0.50, 1.0],
    "nSPDHits":                     [0, 100, 150, 200, 250, 300],
}

charmDict = {
    "Jpsi":     0,
    "Etac":     1,
}

tzTotalBinning =  [-10., -0.15, -0.025, 0.0, 0.025, 0.2, 1.0, 1.5, 2.0, 3.0, 4.0, 10]
#tzTotalBinning = [-10.0, -0.125, -0.025, 0.,        0.200,  2., 4., 10.]

#tzInBinsBinning = [[-1., -0.025, 0., 0.025, 0.1, 1., 4., 10.],
                   #[-1., -0.025, 0.,        0.200,   2., 10.]]

tzInBinsBinning = [[-10.0, -0.125, -0.025, 0.,        0.200,  2., 4., 10.],
                   [-10.0, -0.125, -0.025, 0.,        0.200,  2., 4., 10.]]


tzInBinsBinningAlt = [[-10.0, -0.15, -0.05, 0.,        0.100,   4., 10.],
                      [-10.0, -0.15, -0.05, 0.,        0.100,   4., 10.]]



minMass = 2850.
maxMass = 3250.
#binWidth = 1.
binWidth = 0.4


bold = "\033[1m"
reset = "\033[0;0m"
def makeHistos(nt, year="2016", trigger="DiProton", inCutKey="secondary", var="Jpsi_PT", doBinning=False, doTz=False, charm='Etac'):
    # gROOT.Reset()
    inCut = cutsDict[inCutKey]

    nMassBins = int((maxMass-minMass)/binWidth)
    Jpsi_M = TH1F("Jpsi_M","Jpsi_M" , nMassBins, minMass, maxMass)
    Jpsi_M.SetLineColor(4)
    Jpsi_M.GetXaxis().SetTitle("J/#psi_Mass / MeV ")
    Jpsi_M.GetYaxis().SetTitle("Entries")
    Jpsi_M.SetMinimum(0.0)

    cutMass = "(Jpsi_m_scaled>" + str(minMass) + ") && (Jpsi_m_scaled<" + str(maxMass) + ")"
    #cutMass = "(Jpsi_M>" + str(minMass) + ") && (Jpsi_M<" + str(maxMass) + ")"                   #!!!!!!!!!!!!!!!!!!!!!

    cutDoubleCount = "totCandidates<2"

    isEtac = charmDict[charm]

    if year=="2015": cutApriori = cutApriori2015_Dict[trigger]
    if year=="2016": cutApriori = cutApriori2016_Dict[trigger]

    if doBinning:
        binningFile = "binning.txt"
# !!!!!!!!
        if doTz:
            nTzBins = len(tzInBinsBinning[isEtac])-1
            for iTz in range(nTzBins):
                print bold+"  I\'m doing", charm, " Tz binning. Bin No", (iTz+1), reset
                tzLowEdge = tzInBinsBinning[isEtac][iTz]
                tzHighEdge = tzInBinsBinning[isEtac][iTz+1]
                cutTz = "("+tzVar+">"+str(tzLowEdge)+") && ("+tzVar+"<"+str(tzHighEdge)+")"

                varBinning = binningDict[var]
                nVarBins =  len(varBinning)-1
                for iVar in range(nVarBins):
                    print bold+"        Now I\'m doing", var, "binning. Bin No", (iVar+1), reset
                    varLowEdge = varBinning[iVar]
                    varHighEdge = varBinning[iVar+1]
                    cutVar = "("+var+">"+str(varLowEdge)+") && ("+var+"<"+str(varHighEdge)+")"

                    sumCut = cutMass    +" && "+ \
                             cutApriori +" && "+ \
                             inCut      +" && "+ \
                             cutTz      +" && "+ \
                             cutVar     +" && "+ \
                             cutDoubleCount

                    dirName = histosDir+year+"/"+trigger+"/"+inCutKey+"/"+var+"/"+charm  #+ "/CutedBins" #!!!!!!!!!!!!!!!!!!!!!!!!
                    if not os.path.exists(dirName):
                        os.makedirs(dirName)
                    fileName = dirName + "/bin"+str(iVar+1)+"_Tz"+str(iTz+1)+".root"
                    nt.Draw("Jpsi_m_scaled>>Jpsi_M", sumCut)
                    Jpsi_M.SaveAs(fileName)
        else:
            varBinning = binningDict[var]
            nVarBins =  len(varBinning)-1
            print bold+"I DON\'T do Tz binning",reset
            for i in range(nVarBins):
                print bold+"    Now I\'m doing", var, "binning. Bin No", (i+1), ", Tz integrated", reset
                varLowEdge = varBinning[i]
                varHighEdge = varBinning[i+1]
                cutVar = "("+var+">"+str(varLowEdge)+") && ("+var+"<"+str(varHighEdge)+")"

                sumCut = cutMass    +" && "+ \
                         cutApriori +" && "+ \
                         inCut      +" && "+ \
                         cutVar

                dirName = histosDir+year+"/"+trigger+"/"+inCutKey+"/"+var
                if not os.path.exists(dirName):
                    os.makedirs(dirName)
                fileName = dirName + "/bin"+str(i+1)+"_Tz0.root"
                nt.Draw("Jpsi_m_scaled>>Jpsi_M", sumCut)
                Jpsi_M.SaveAs(fileName)
    else:
        print bold+"I\'m doing Total histos"+reset
        if doTz:
            nTzBins = len(tzTotalBinning)-1
            for i in range(nTzBins):
                print bold+"     I\'m doing Tz binning for Total histos. Bin No", (i+1), reset
                tzLowEdge = tzTotalBinning[i]
                tzHighEdge = tzTotalBinning[i+1]
                cutTz = "("+tzVar+">"+str(tzLowEdge)+") && ("+tzVar+"<"+str(tzHighEdge)+")"
                cutPTHigh = "(Jpsi_PT < 14000)"
                
                sumCut = cutMass    +" && "+ \
                         cutApriori +" && "+ \
                         inCut      +" && "+ \
                         cutPTHigh  +" && "+ \
                         cutTz

                dirName = histosDir + year + "/" + trigger + "/" + inCutKey + "/Total"
                if not os.path.exists(dirName):
                    os.makedirs(dirName)
                fileName = dirName + "/Tz" + str(i+1) + ".root"  #!!!!!!!!!!!!!!!!!!!!!
                nt.Draw("Jpsi_m_scaled>>Jpsi_M", sumCut)
                #nt.Draw("Jpsi_M>>Jpsi_M", sumCut)           #!!!!!!!!!!!!!!!!!!!!!
                Jpsi_M.SaveAs(fileName)
        else:
            print bold+"     I DON\'T do Tz binning for Total histos"+reset
            sumCut = cutMass    +" && "+ \
                     cutApriori +" && "+ \
                     inCut

            cut = TCut(sumCut)

            dirName = histosDir + year + "/" + trigger + "/" + inCutKey + "/Total"
            if not os.path.exists(dirName):
                os.makedirs(dirName)
            fileName = dirName + "/Tz0.root"   
            nt.Draw("Jpsi_m_scaled>>Jpsi_M", cut)
            Jpsi_M.SaveAs(fileName)
    del Jpsi_M



from multiprocessing import Pool
from functools import partial
from contextlib import closing

def makeAllHistos():

    inCutKey= "all_l0TOS"
    #inCutKey= "secondary_l0TOS"
    var     = "Jpsi_PT"
    year    = "2016"
    trigger = "DiProton"


    years = ["2016"]
    #variables = ["Jpsi_PT","Jpsi_ETA","TMath::Abs(ProtonM_CosTheta)","nSPDHits"]
    variables = ["Jpsi_PT"]
    cutKeys = ["all_l0TOS", "secondary_l0TOS"]
    
    #nt = TChain("DecayTree")
    #if (year=="2016"): 
        #nt.Add(tuples2016)
    #if (year=="2015"): 
        #nt.Add(tuples2015)
    #makeHistos(nt,year,trigger,inCutKey,var,False,False,'Etac')
    #makeHistos(nt,year,trigger,inCutKey,var,True,True,'Etac')
            
    for var in variables:
        for year in years:
            #for inCutKey in cutKeys:
                #print "Setup Cuts: ", bold+inCutKey, reset
                #print "      year: ", bold+year, reset,"\n"

                nt = TChain("DecayTree")
                if (year=="2016"): 
                    nt.Add(tuples2016)
                if (year=="2015"): 
                    nt.Add(tuples2015)

                #makeHistos(nt,year,trigger,inCutKey,var,False,False)
                #makeHistos(nt,year,trigger,inCutKey,var,True,False)
                makeHistos(nt,year,trigger,inCutKey,var,True,True)
                #makeHistos(nt,year,trigger,inCutKey,var,False,True)
                #makeHistos(nt,year,trigger,inCutKey,var,True,True,'Jpsi')




def multHistos(list):
    year = list[0]
    #trigger = list[1]
    trigger = "DiProton"
    #trigger = "Topo"
    inCutKey = list[1]
    var = list[2]
    isPT = list[3]
    isTz = list[4]
    charm = list[5]
    nt = TChain("DecayTree")
    if (year=="2016"): 
        nt.Add(tuples2016)
    if (year=="2015"): 
        nt.Add(tuples2015)
        print nt.GetEntries()

    return  makeHistos(nt,year,trigger,inCutKey,var,isPT,isTz,charm)


def proc():

    years = ["2015","2016"]
    #variables = ["Jpsi_PT","Jpsi_ETA","TMath::Abs(ProtonM_CosTheta)","nSPDHits"]
    variables = ["Jpsi_PT"]
    #cutKeys = [ "all_l0TOS","secondary_l0TOS"]
    cutKeys = [ "all_l0TOS"]
    doPT = [True,False]
    #doPT = [True]

    a = [[year, inCutKey, var, isDoPT, True, "Etac"] for year in years for inCutKey in cutKeys for var in variables for isDoPT in doPT]
#print(a);

    if __name__ == '__main__':
        with closing(Pool(8)) as p:
            (p.map(multHistos, a))
            p.terminate()


#makeAllHistos()
proc()