from ROOT import *
from ROOT.TMath import Sqrt, Sq

import sys, os

sys.path.insert(0, '../')
from makeConfigDictionary import *

from array import array
PTBins  = array("d", [6500,8000,10000,12000,14000])
nPTBins = 4

# PTBins  = array("d", [6500,7000,8000,9000,10000,11000,12000,13000,14000])
# nPTBins = 8

JpsiPmt_genLevelEff   = 0.272881/2.
JpsiFromB_genLevelEff = 0.281591/2.
JpsiAll_genLevelEff   = 0.274709/2.
#Jpsi b/prompt genEff = 1.022

EtacPmt_genLevelEff   = 0.260471/2.
EtacFromB_genLevelEff = 0.274638/2.
EtacAll_genLevelEff   = 0.263341/2.
#Etac b/prompt genEff = 1.011

# JpsiAll_genLevelTab   = 0.14072
# EtacAll_genLevelTab   = 0.13557

effPmtJpsi2EtacGenLevel =  JpsiPmt_genLevelEff/EtacPmt_genLevelEff

#allJpsi2Etac Gen 1.0432
#allJpsi2Etac Tab 1.0280



noCutTreeName = "MCDecayTree"
# genTreeName = "DecayTree"
genTreeName   = "MCDecayTree"
selTreeName = "DecayTree"


cutMass = "(Jpsi_M>" + str(minMass) + ") && (Jpsi_M<" + str(maxMass) + ")"
inCut   = cutsDict["all_l0TOS"]
cutGenLevel = "TMath::ATan2(ProtonP_TRUEPT,ProtonP_TRUEP_Z)<0.40 && TMath::ATan2(ProtonP_TRUEPT,ProtonP_TRUEP_Z)>0.01 && TMath::ATan2(ProtonM_TRUEPT,ProtonM_TRUEP_Z)<0.40 && TMath::ATan2(ProtonM_TRUEPT,ProtonM_TRUEP_Z)>0.01 && ProtonP_TRUEPT>900 && ProtonP_TRUEPT>900 && ProtonM_TRUEPT>900"

cutPIDp  = "20"
cutPIDpK = "15"
additionalCut = "ProtonP_PIDp>{0} && ProtonM_PIDp>{0} && (ProtonP_PIDp-ProtonP_PIDK)>{1} && (ProtonM_PIDp-ProtonM_PIDK)>{1}".format(cutPIDp,cutPIDpK)
cutMass = cutMass+" && "+additionalCut

def getTrees(PmtSelNames, FromBSelNames, PmtGenNames, FromBGenNames, NoCutPmtNames, NoCutFromBNames):

    ntSel_Prompt= TChain(selTreeName) 
    ntSel_FromB = TChain(selTreeName)

    ntGen_Prompt = TChain(genTreeName)
    ntGen_FromB  = TChain(genTreeName)

    ntNoCut_Prompt = TChain(noCutTreeName)
    ntNoCut_FromB  = TChain(noCutTreeName)

    for PmtSelName in PmtSelNames:
        ntSel_Prompt.Add('../MC/tuples/selected/all_l0TOS/'+PmtSelName)
    for FromBSelName in FromBSelNames:
        ntSel_FromB.Add('../MC/tuples/selected/all_l0TOS/'+FromBSelName)
    for PmtGenName in PmtGenNames:
        ntGen_Prompt.Add('../MC/tuples/selected/'+PmtGenName)
    for FromBGenName in FromBGenNames:
        ntGen_FromB.Add('../MC/tuples/selected/'+FromBGenName)
    for NoCutPmtName in NoCutPmtNames:
        ntNoCut_Prompt.Add('../MC/'+NoCutPmtName)
    for NoCutFromBName in NoCutFromBNames:
        ntNoCut_FromB.Add('../MC/'+NoCutFromBName)

    treeSel_Prompt, treeSel_FromB, treeNoCut_Prompt = TTree(), TTree(), TTree()
    treeGen_Prompt, treeGen_FromB, treeNoCut_FromB  = TTree(), TTree(), TTree()

    treeSel_Prompt = ntSel_Prompt.CopyTree("Jpsi_prompt")
    treeSel_FromB  = ntSel_FromB.CopyTree("Jpsi_sec")

    treeGen_Prompt = ntGen_Prompt.CopyTree("Jpsi_prompt")
    treeGen_FromB  = ntGen_FromB.CopyTree("Jpsi_sec")

    treeNoCut_Prompt = ntNoCut_Prompt.CopyTree("Jpsi_prompt")
    treeNoCut_FromB  = ntNoCut_FromB.CopyTree("Jpsi_sec")

    return treeSel_Prompt,treeSel_FromB,treeGen_Prompt,treeGen_FromB, treeNoCut_Prompt, treeNoCut_FromB



def calculateBinEff(treeSel,treeGen,treeNoCut,cutPT):

    nSel    = float(treeSel.GetEntries(inCut+" && "+cutMass+" && "+cutPT))
    nGen    = float(treeGen.GetEntries(cutPT))
    nNoCut  = float(treeNoCut.GetEntries(cutPT))
    nGenCut = float(treeNoCut.GetEntries(cutGenLevel+" && "+cutPT))

    effSel      = nSel/nGen
    effGenLevel = nGenCut/nNoCut/2.

    # eff = effSel*effGenLevel
    # err = eff*Sqrt(1./nSel+1./nGen+1./nNoCut+1./nGenCut)

    eff = effSel
    err = eff*Sqrt(1./nSel+1./nGen)

    return eff,err


def plot():

    gStyle.SetOptFit(0)
    gStyle.SetOptStat(0)
    gStyle.SetOptTitle(0)

    treeJpsiSel_Prompt,treeJpsiSel_FromB,\
    treeJpsiGen_Prompt,treeJpsiGen_FromB,\
    treeJpsiNoCut_Prompt,treeJpsiNoCut_FromB = \
    getTrees(['Jpsi_Pr_all_l0TOS_allSelected_AddBr.root'],
             ['Jpsi_Pr_all_l0TOS_allSelected_AddBr.root'],
             ['Jpsi_allMCDecayTree_AddBr.root'],
             ['Jpsi_allMCDecayTree_AddBr.root'],
             ['Jpsi/Generator/Jpsi_GenNoCut_AddBr.root'],
             ['Jpsi/Generator/Jpsi_GenNoCut_AddBr.root'])

    treeEtacSel_Prompt,treeEtacSel_FromB,\
    treeEtacGen_Prompt,treeEtacGen_FromB,\
    treeEtacNoCut_Prompt,treeEtacNoCut_FromB = \
    getTrees(['Etac_Pr_all_l0TOS_allSelected_AddBr.root'],
        ['Etac_Pr_all_l0TOS_allSelected_AddBr.root'],
        ['Etac_allMCDecayTree_AddBr.root'],
        ['Etac_allMCDecayTree_AddBr.root'],
        ['Etac/Generator/Etac_GenNoCut_AddBr.root'],
        ['Etac/Generator/Etac_GenNoCut_AddBr.root'])



    hJpsiPmtEffMC = TH1F("hJpsiPmtEffMC","hJpsiPmtEffMC",nPTBins,PTBins)
    hJpsiFromBEffMC = TH1F("hJpsiFromBEffMC","hJpsiFromBEffMC",nPTBins,PTBins)

    hEtacPmtEffMC = TH1F("hEtacPmtEffMC","hEtacPmtEffMC",nPTBins,PTBins)
    hEtacFromBEffMC = TH1F("hEtacFromBEffMC","hEtacFromBEffMC",nPTBins,PTBins)

    hEffJpsi2EtacMCPmt = TH1F("hEffJpsi2EtacMCPmt","hEffJpsi2EtacMCPmt",nPTBins,PTBins)
    hEffJpsi2EtacMCFromB = TH1F("hEffJpsi2EtacMCFromB","hEffJpsi2EtacMCFromB",nPTBins,PTBins)
    
    effJpsiPmtMCtot,         errJpsiPmtMCtot         = 0, 0 
    effJpsiFromBMCtot,       errJpsiFromBMCtot       = 0, 0 
    effEtacPmtMCtot,         errEtacPmtMCtot         = 0, 0
    effEtacFromBMCtot,       errEtacFromBMCtot       = 0, 0
    effJpsi2EtacRelPmttot,   errJpsi2EtacRelPmttot   = 0, 0
    effJpsi2EtacRelFromBtot, errJpsi2EtacRelFromBtot = 0, 0

    for i in range(nPTBins+1):
        if i==0:
            cutPT = "Jpsi_TRUEPT>"+str(PTBins[0])+" && "+"Jpsi_TRUEPT<"+str(PTBins[nPTBins])
        else:
            cutPT = "Jpsi_TRUEPT>"+str(PTBins[i-1])+" && "+"Jpsi_TRUEPT<"+str(PTBins[i])

        effJpsiPmtMC, errJpsiPmtMC = calculateBinEff(treeJpsiSel_Prompt,
                                                     treeJpsiGen_Prompt,
                                                     treeJpsiNoCut_Prompt,
                                                     cutPT)
        effJpsiFromBMC, errJpsiFromBMC = calculateBinEff(treeJpsiSel_FromB,
                                                         treeJpsiGen_FromB,
                                                         treeJpsiNoCut_FromB,
                                                         cutPT)
        effEtacPmtMC, errEtacPmtMC = calculateBinEff(treeEtacSel_Prompt,
                                                     treeEtacGen_Prompt,
                                                     treeEtacNoCut_Prompt,
                                                     cutPT)
        effEtacFromBMC, errEtacFromBMC = calculateBinEff(treeEtacSel_FromB,
                                                         treeEtacGen_FromB,
                                                         treeEtacNoCut_FromB,
                                                         cutPT)

        effJpsi2EtacRelPmt = effJpsiPmtMC/effEtacPmtMC
        errJpsi2EtacRelPmt = effJpsi2EtacRelPmt*Sqrt(Sq(errJpsiPmtMC/effJpsiPmtMC)+Sq(errEtacPmtMC/effEtacPmtMC))

        effJpsi2EtacRelFromB = effJpsiFromBMC/effEtacFromBMC
        errJpsi2EtacRelFromB = effJpsi2EtacRelFromB*Sqrt(Sq(errJpsiFromBMC/effJpsiFromBMC)+Sq(errEtacFromBMC/effEtacFromBMC))

        if i==0:
            effJpsiPmtMCtot = effJpsiPmtMC
            errJpsiPmtMCtot = errJpsiPmtMC
            effJpsiFromBMCtot = effJpsiFromBMC
            errJpsiFromBMCtot = errJpsiFromBMC
            effEtacPmtMCtot = effEtacPmtMC
            errEtacPmtMCtot = errEtacPmtMC
            effEtacFromBMCtot = effEtacFromBMC
            errEtacFromBMCtot = errEtacFromBMC
            effJpsi2EtacRelPmttot = effJpsi2EtacRelPmt
            errJpsi2EtacRelPmttot = errJpsi2EtacRelPmt
            effJpsi2EtacRelFromBtot = effJpsi2EtacRelFromB
            errJpsi2EtacRelFromBtot = errJpsi2EtacRelFromB
        else:
            hJpsiPmtEffMC.SetBinContent(i,effJpsiPmtMC)
            hJpsiPmtEffMC.SetBinError(i,errJpsiPmtMC)

            hJpsiFromBEffMC.SetBinContent(i,effJpsiFromBMC)
            hJpsiFromBEffMC.SetBinError(i,errJpsiFromBMC)

            hEtacPmtEffMC.SetBinContent(i,effEtacPmtMC)
            hEtacPmtEffMC.SetBinError(i,errEtacPmtMC)

            hEtacFromBEffMC.SetBinContent(i,effEtacFromBMC)
            hEtacFromBEffMC.SetBinError(i,errEtacFromBMC)

            hEffJpsi2EtacMCPmt.SetBinContent(i,effJpsi2EtacRelPmt)
            hEffJpsi2EtacMCPmt.SetBinError(i,errJpsi2EtacRelPmt)

            hEffJpsi2EtacMCFromB.SetBinContent(i,effJpsi2EtacRelFromB)
            hEffJpsi2EtacMCFromB.SetBinError(i,errJpsi2EtacRelFromB)

    cRelEffPmt = TCanvas("cRelEffPmt")
    cRelEffPmt.cd()
    gPad.SetLeftMargin(0.15)

    # hEffJpsi2EtacMCPmt.Fit("pol1","EIM")

    horizUp   = TLine(PTBins[0],1.02,PTBins[4],1.02)
    horizDown = TLine(PTBins[0],0.98,PTBins[4],0.98)
    horizUp.SetLineColor(2);   horizUp.SetLineStyle(2)
    horizDown.SetLineColor(2); horizDown.SetLineStyle(2)

    hEffJpsi2EtacMCPmt.GetXaxis().SetTitle("p_{T}, [MeV/c]")
    hEffJpsi2EtacMCPmt.GetYaxis().SetTitle("#epsilon_{J/#psi}/#epsilon_{#eta_{c}}")
    hEffJpsi2EtacMCPmt.GetYaxis().SetTitleOffset(0.9)
    hEffJpsi2EtacMCPmt.GetXaxis().SetTitleOffset(1.1)

    hEffJpsi2EtacMCtotPmt = TH1F("hEffJpsi2EtacMCtotPmt","hEffJpsi2EtacMCtotPmt",1,PTBins[0],PTBins[nPTBins])
    hEffJpsi2EtacMCtotPmt.SetBinContent(1,effJpsi2EtacRelPmttot)
    hEffJpsi2EtacMCtotPmt.SetBinError(1,errJpsi2EtacRelPmttot)

    hEffJpsi2EtacMCPmt.Draw("E1")
    hEffJpsi2EtacMCPmt.GetYaxis().SetRangeUser(0.93,1.06)
    hEffJpsi2EtacMCtotPmt.SetFillColor(2)
    hEffJpsi2EtacMCtotPmt.SetFillStyle(3002)
    hEffJpsi2EtacMCtotPmt.SetMarkerSize(0.)
    hEffJpsi2EtacMCtotPmt.Draw("E2 same")
    hEffJpsi2EtacMCPmt.Draw("E1 same")
    horizUp.Draw("same"); horizDown.Draw("same")

    texCS = TLatex()
    texCS.SetNDC()
    # texCS.SetTextSize(0.025)
    texCS.DrawLatex(0.2, 0.85, "LHCb simulation")
    texCS.DrawLatex(0.2, 0.8, "prompt")
    cRelEffPmt.SaveAs("cRelEffPmt.pdf")





    cRelEffFromB = TCanvas("cRelEffFromB")
    cRelEffFromB.cd()
    gPad.SetLeftMargin(0.15)

    # hEffJpsi2EtacMCFromB.Fit("pol1","EIM")

    hEffJpsi2EtacMCFromB.GetXaxis().SetTitle("p_{T}, [MeV/c]")
    hEffJpsi2EtacMCFromB.GetYaxis().SetTitle("#epsilon_{J/#psi}/#epsilon_{#eta_{c}}")
    hEffJpsi2EtacMCFromB.GetYaxis().SetTitleOffset(0.9)
    hEffJpsi2EtacMCFromB.GetXaxis().SetTitleOffset(1.1)


    hEffJpsi2EtacMCtotFromB = TH1F("hEffJpsi2EtacMCtotFromB","hEffJpsi2EtacMCtotFromB",1,PTBins[0],PTBins[nPTBins])
    hEffJpsi2EtacMCtotFromB.SetBinContent(1,effJpsi2EtacRelFromBtot)
    hEffJpsi2EtacMCtotFromB.SetBinError(1,errJpsi2EtacRelFromBtot)

    hEffJpsi2EtacMCFromB.Draw("E1")
    hEffJpsi2EtacMCFromB.GetYaxis().SetRangeUser(0.93,1.06)
    hEffJpsi2EtacMCtotFromB.SetFillColor(2)
    hEffJpsi2EtacMCtotFromB.SetFillStyle(3002)
    hEffJpsi2EtacMCtotFromB.SetMarkerSize(0.)
    hEffJpsi2EtacMCtotFromB.Draw("E2 same")
    hEffJpsi2EtacMCFromB.Draw("E1 same")
    horizUp.Draw("same"); horizDown.Draw("same")
    texCS.DrawLatex(0.2, 0.85, "LHCb simulation")
    texCS.DrawLatex(0.2, 0.8, "from b")
    cRelEffFromB.SaveAs("cRelEffFromB.pdf")

    print "tot Pmt   = ",effJpsi2EtacRelPmttot,  " +- ",errJpsi2EtacRelPmttot
    print "tot FromB = ",effJpsi2EtacRelFromBtot," +- ",errJpsi2EtacRelFromBtot
    averageEff = 0.5*(effJpsi2EtacRelPmttot+effJpsi2EtacRelFromBtot)
    diff = effJpsi2EtacRelPmttot-effJpsi2EtacRelFromBtot
    averageErr = Sqrt(Sq(errJpsi2EtacRelPmttot)+Sq(diff))
    print "tot       = ",averageEff," +- ",averageErr
gROOT.LoadMacro("../lhcbStyle.C")
plot()



