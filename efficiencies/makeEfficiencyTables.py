import sys
sys.path.insert(0, '../')
from makeConfigDictionary import *

from ROOT import *
from array import array

#MC Productions to in BKK and get prodID:
#                     |   2015    |  2016
# etac     {24102010} | 61893(94) | 61882(83)    0.13557 ± 0.00048 
#                     | 61886(87) | 61878(79)    0.13570 ± 0.00036
#                                                0.13508 ± 0.00036
#
# jpsi     {24102002} | 61895(96) | 61884(85)    0.14112 ± 0.00050
#                     | 61888(89) | 61880(71)    0.14072 ± 0.00046
#                                                0.14104 ± 0.00036
#
# b->etacX {24102010} | 65133(34) | 65129(30)    0.60513 ± 0.00018
#                     | 65131(32) | 65127(28)    0.60520 ± 0.00018
#                                                0.60505 ± 0.00012
#                                                0.60504 ± 0.00012
#
# pppi0 {24102402}    | 58381(82) | 62822(23)
#                     | 58379(80) | 62820(21)






# mcDir = "/sps/lhcb/zhovkovska/etacToPpbar/MC/"
# MCTuples = {
#     "etac"   :{ "Reco"        :[mcDir+"EtacDiProton_MC_NoStripping_2015.root ",
#                                 mcDir+"EtacDiProton_MC_NoStripping_2016.root "],
#                 "promptSel"   :[mcDir+"EtacDiProton_MC_2015.root ",
#                                 mcDir+"EtacDiProton_MC_2016.root "],
#                 "secondarySel": "../MC/Etac/*",
#                 "MCDecayTree" : "../MC/Etac/*",
#                 "NoGenCut"    : "../MC/Etac/*",
#                 "GenLevelEff" : 0 }
#     "jpsi"   :{ "Reco"        :[mcDir+"JpsiDiProton_NoStripping_MC_2015.root ",
#                                 mcDir+"JpsiDiProton_NoStripping_MC_2016.root "],
#                 "promptSel"   :[mcDir+"JpsiDiProton_MC_2015.root ",
#                                 mcDir+"JpsiDiProton_MC_2016.root "],
#                 "secondarySel": "",
#                 "MCDecayTree" : "",
#                 "NoGenCut"    : "",
#                 "GenLevelEff" : 0 }
#     "b2etacX":{ "Reco"        :[mcDir+"EtacDiProton_MC_NoStripping_2015_incl_b.root ",
#                                 mcDir+"EtacDiProton_MC_NoStripping_2016_incl_b.root "],
#                 "promptSel"   :[mcDir+"EtacDiProton_MC_2015_incl_b.root ",
#                                 mcDir+"EtacDiProton_MC_2016_incl_b.root "],
#                 "secondarySel": "",
#                 "MCDecayTree" : "",
#                 "NoGenCut"    : "",
#                 "GenLevelEff" : 0 }
# }


def calculateEff(eventType,mcKey,cutKey):
    if mcKey == "MCDecayTree": 
        chain = TChain("MCDecayTree")
    else:
        chain = TChain("DecayTree")

    for file in MCTuples[eventType][mcKey]:
        chain.Add(file)

    return chain.GetEntries()


a = calculateEff("jpsi","Reco","")
print a,"\n"


print "\
                 |  etac prompt   |  etac from b  |  jpsi prompt  | jpsi from b  \n\
    GenLevel     |                |               |               |              \n\
    Generated    |                |               |               |              \n\
    Reco         |                |               |               |              \n\
    Triggered    |                |               |               |              \n\
    Selected     |                |               |               |              \n\
    HLT1-like    |                |               |               |              \n\
    postselection|                                                               \n\
    Mass         |                |               |               |               "






# def mergeTuple():
#     chain = TChain("Jpsi2ppTuple/DecayTree")

#     sjMax=26
#     for i in range(sjMax):
#         chain.Add("/afs/cern.ch/work/a/ausachov/gangadir/workspace/ausachov/LocalXML/473/"+str(i)+"/output/Tuple.root")
#     for i in range(sjMax):
#         chain.Add("/afs/cern.ch/work/a/ausachov/gangadir/workspace/ausachov/LocalXML/474/"+str(i)+"/output/Tuple.root")

#     cutProtonP = TCut("ProtonP_P>12.5e3 && ProtonM_P>12.5e3")
#     cutProtonPt = TCut("ProtonP_PT>2000 && ProtonM_PT>2000")
# #    cutProtonPt = TCut("ProtonP_PT>1000 && ProtonM_PT>1000")
#     cutProtonTheta = TCut("(ProtonP_PT/ProtonP_P)>0.0366 && (ProtonM_PT/ProtonM_P)>0.0366")
# #    cutID = TCut("ProtonP_PIDp>20 && ProtonM_PIDp>20")
# #    cutID += TCut("(ProtonP_PIDp-ProtonP_PIDK)>10 && (ProtonM_PIDp-ProtonM_PIDK)>10")
#     cutID = TCut("ProtonP_ProbNNp>0.6 && ProtonM_ProbNNp>0.6")
#     cutProtonTrack = TCut("ProtonP_TRACK_CHI2NDOF<2.5 && ProtonM_TRACK_CHI2NDOF<2.5")
#     cutJpsiVx = TCut("Jpsi_ENDVERTEX_CHI2<9")
#     cutJpsiY = TCut("Jpsi_Y>2 && Jpsi_Y<4.5")
#     cutJpsiPt = TCut("Jpsi_PT>6500")
#     cutProtonClone = TCut("ProtonP_TRACK_CloneDist<0 && ProtonM_TRACK_CloneDist<0")
#     cutSpdMult = TCut("nSPDHits < 300")
#     cutL0 = TCut("Jpsi_L0HadronDecision_TOS")

#     selectionCut = TCut(cutProtonP +
#                         cutProtonPt +
#                         cutProtonTheta +
#                         cutID +
#                         cutProtonTrack +
#                         cutJpsiVx +
#                         cutJpsiY +
# #                        cutJpsiPt +
#                         cutProtonClone +
#                         cutSpdMult +
#                         cutL0
#                         )

#     newtree = chain.CopyTree(selectionCut.GetTitle())
# #    newfile = TFile
# ("noPTcut.root","recreate")
# #    newfile = TFile("noPIDcut.root","recreate")
# #    newfile = TFile("noPTcut_soft.root","recreate")
# #    newfile = TFile("noPTcut_soft_ID06.root","recreate")
#     newfile = TFile("noPTcut_ID06.root","recreate")
#     newtree.Write()


# newtree = TChain("DecayTree")
# signal = array('d')
# signalErr = array('d')
# cutValsErr = array('d',[0,0,0,0,0,0,0,0,0])


# def plotEff():
# #   mergeTuple()

#     nPoints = 8
#     newtree.Add("noPTcut.root")
#     #newtree.Add("noPTcut_soft_ID06.root")
#     #newtree.Add("noPTcut_ID06.root")
#     varName = "Jpsi_PT"
#     cutVals = array('d',[2,3,4,5,6,6.5,7,8])

#     #nPoints = 8
#     #newtree.Add("noPIDcut.root")
#     #varName = "ProtonP_ProbNNp"
#     #cutVals = array('d',[0., 0.1, 0.2, 0.4, 0.6, 0.8, 0.9, 0.95])

#     for i in range(nPoints):
#         cut = varName+">"+str(cutVals[i]*1000)
#     #    cut = "ProtonP_ProbNNp>"+str(cutVals[i])+" && "+"ProtonM_ProbNNp>"+str(cutVals[i])
#         nSig = newtree.GetEntries(cut)
#         signal.append(nSig)
#         signalErr.append(TMath.Sqrt(nSig))

#     graph = TGraphErrors(nPoints,cutVals,signal,cutValsErr,signalErr)

#     c = TCanvas()
#     graph.Draw("APC")
#     c.SaveAs("eff_"+varName+"_cut.pdf")


# def plotOptimization():
    
#     newtree.Add("noPTcut.root")
    
#     nPoints = 5
#     cutVals = array('d',[2,3,4,5,6.5])
    
#     HLT2RatesLowMass = array('d',[0.11, 0.11, 0.09, 0.06, 0.03])
#     errHLT2RatesLowMass = array('d',[0.01, 0.01, 0.01, 0.01, 0.01])
    
#     significance = array('d')
#     significanceErr = array('d')

#     for i in range(nPoints):
#         cut = "Jpsi_PT>"+str(cutVals[i]*1000)
#         nSig = newtree.GetEntries(cut)
#         nSigErr = TMath.Sqrt(nSig)
#         HLT2Rate = HLT2RatesLowMass[i]
#         errHLT2Rate = errHLT2RatesLowMass[i]
#         significanceVal = nSig/TMath.Sqrt(HLT2RatesLowMass[i])
#         significanceErrRelSq = (nSigErr/nSig)**2 + (errHLT2Rate/HLT2Rate/2)**2
#         significanceErrVal = significanceVal*TMath.Sqrt(significanceErrRelSq)

#         significance.append(significanceVal)
#         significanceErr.append(significanceErrVal)

#     graph = TGraphErrors(nPoints,cutVals,significance,cutValsErr,significanceErr)
#     graph.GetXaxis().SetTitle("Jpsi_PT")
#     graph.GetYaxis().SetTitle("~ significance")
    
#     c = TCanvas()
#     graph.Draw("APC")
#     c.SaveAs("optimize_JpsiPT.pdf")



# def plotOptimizationRSim():
    
#     newtree.Add("../MC/Jpsi/RapidSim/Jpsipp_tree.root")
    
# #    nPoints = 5
# #    cutVals = array('d',[2,3,4,5,6.5])
# #    
# #    HLT2RatesLowMass = array('d',[0.11, 0.11, 0.09, 0.06, 0.03])
# #    errHLT2RatesLowMass = array('d',[0.01, 0.01, 0.01, 0.01, 0.01])
# #    
# #    significance = array('d')
# #    significanceErr = array('d')
# #    
# #    for i in range(nPoints):
# #        cut = "Jpsi_PT>"+str(cutVals[i]*1000)
# #        nSig = newtree.GetEntries(cut)
# #        nSigErr = TMath.Sqrt(nSig)
# #        HLT2Rate = HLT2RatesLowMass[i]
# #        errHLT2Rate = errHLT2RatesLowMass[i]
# #        significanceVal = nSig/TMath.Sqrt(HLT2RatesLowMass[i])
# #        significanceErrRelSq = (nSigErr/nSig)**2 + (errHLT2Rate/HLT2Rate/2)**2
# #        significanceErrVal = significanceVal*TMath.Sqrt(significanceErrRelSq)
# #        
# #        significance.append(significanceVal)
# #        significanceErr.append(significanceErrVal)
# #    
# #    graph = TGraphErrors(nPoints,cutVals,significance,cutValsErr,significanceErr)
# #    graph.GetXaxis().SetTitle("Jpsi_PT")
# #    graph.GetYaxis().SetTitle("~ significance")
# #    
# #    c = TCanvas()
# #    graph.Draw("APC")
# #    c.SaveAs("optimize_JpsiPT.pdf")

# plotOptimization()
# #plotOptimizationRSim()


