from ROOT import *
from ROOT.TMath import Sqrt, Sq

import sys, os

sys.path.insert(0, '../')
from makeConfigDictionary import *

from array import array

PTBins  = array("d", [6500,8000,10000,12000,14000])
nPTBins = 4

#PTBins  = array("d", [6500,7000,8000,9000,10000,11000,12000,13000,14000])
#nPTBins = 8

cutsApriori = cutsAprioriVeryLooseNoPID
PIDCut  = "ProtonP_PIDp>20 && (ProtonP_PIDp-ProtonP_PIDK)>15 && ProtonP_PT>1000 && (ProtonP_PT/ProtonP_P)>0.0366"
PIDCutM = "ProtonM_PIDp>20 && (ProtonM_PIDp-ProtonM_PIDK)>15 && ProtonM_PT>1000 && (ProtonM_PT/ProtonM_P)>0.0366"

cutMass = "(Jpsi_M>" + str(minMass) + ") && (Jpsi_M<" + str(maxMass) + ")"


f = TFile("/afs/cern.ch/work/a/ausachov/UraniaDev_v7r0/effPID.root")
PIDCalHist = f.Get("effPID")


selTreeName = "DecayTree"
JpsiNoPIDTupleName  = "../MC/tuples/selected/Jpsi_SecNoPID_allUnsel_AddBr.root"
EtacNoPIDTupleName  = "../MC/tuples/selected/Etac_SecNoPID_allUnsel_AddBr.root"
InclBNoPIDTupleName = "../MC/tuples/selected/incl_b_SecNoPID_allUnsel_AddBr.root"

EtacNoPIDTuple = TChain(selTreeName); 
EtacNoPIDTuple.Add(EtacNoPIDTupleName)
#EtacNoPIDTuple.Add(InclBNoPIDTupleName)
JpsiNoPIDTuple = TChain(selTreeName); JpsiNoPIDTuple.Add(JpsiNoPIDTupleName)

def calculateMCEff(treeNoPID, cutPT, isEtac):

    defCut = "Jpsi_sec"
    if isEtac:
       defCut = "Jpsi_sec && Jpsi_M<3050"

    nNoPID  = float(treeNoPID.GetEntries(defCut+" && "+cutsApriori+" && "+cutMass+" && "+cutPT))
    nPID_MC = float(treeNoPID.GetEntries(defCut+" && "+cutsApriori+" && "+PIDCut+" && "+cutMass+" && "+cutPT))

    nPIDM_MC = float(treeNoPID.GetEntries(defCut+" && "+cutsApriori+" && "+PIDCutM+" && "+cutMass+" && "+cutPT))



    effPID_MC = (nPID_MC+nPIDM_MC)/nNoPID/2.
    errPID_MC = effPID_MC*Sqrt(1./nNoPID + 1./(nPID_MC+nPIDM_MC))

    return effPID_MC,errPID_MC

def calculatePIDCalibEff(treeNoPID, cutPT, isEtac):

    defCut = "Jpsi_sec"
    if isEtac:
       defCut = "Jpsi_sec && Jpsi_M<3050"


    treeBin  = treeNoPID.CopyTree(defCut+" && "+cutsApriori+" && "+cutMass+" && "+cutPT)
    nEntries = treeBin.GetEntries()

    P_P   = array( 'd', [0])
    P_ETA = array( 'd', [0])
    M_P   = array( 'd', [0])
    M_ETA = array( 'd', [0])

    treeBin.SetBranchAddress("ProtonP_P",P_P)
    treeBin.SetBranchAddress("ProtonM_P",M_P)
    treeBin.SetBranchAddress("ProtonP_ETA",P_ETA)
    treeBin.SetBranchAddress("ProtonM_ETA",M_ETA)

    eff, effSum = 0., 0.
    err, errSum = 0., 0.
    for i in range(nEntries):

        treeBin.GetEntry(i)

        bin_Px = PIDCalHist.GetXaxis().FindBin(P_ETA[0])
        bin_Py = PIDCalHist.GetYaxis().FindBin(P_P[0])

        bin_Mx = PIDCalHist.GetXaxis().FindBin(M_ETA[0])
        bin_My = PIDCalHist.GetYaxis().FindBin(M_P[0])

        eff_P = PIDCalHist.GetBinContent(bin_Px,bin_Py)
        eff_M = PIDCalHist.GetBinContent(bin_Mx,bin_My)

        err_P = PIDCalHist.GetBinError(bin_Px,bin_Py)
        err_M = PIDCalHist.GetBinError(bin_Mx,bin_My)

        effEv = eff_P+eff_M
        errEv = Sq(err_P)+Sq(err_M)
        
        effSum  += effEv
        errSum  += errEv

    errSum = Sqrt(errSum)

    eff = effSum/(2.*float(nEntries))
    err = eff*Sqrt(Sq(errSum/effSum) + 0./float(nEntries))
    return eff, err


def plot():

    gStyle.SetOptFit(0)
    gStyle.SetOptStat(0)
    gStyle.SetOptTitle(0)




    rJpsi2EtacMC     = TH1F("rJpsi2EtacMC","rJpsi2EtacMC",nPTBins,PTBins)
    rJpsi2EtacPIDCal = TH1F("rJpsi2EtacPIDCal","rJpsi2EtacPIDCal",nPTBins,PTBins)
    rJpsi2EtacMCTot, errJpsi2EtacMCTot         = 0, 0 
    rJpsi2EtacPIDCalTot, errJpsi2EtacPIDCalTot = 0, 0 

    for i in range(nPTBins+1):
        if i==0:
            cutPT = "Jpsi_TRUEPT>"+str(PTBins[0])+" && "+"Jpsi_TRUEPT<"+str(PTBins[nPTBins])
        else:
            cutPT = "Jpsi_TRUEPT>"+str(PTBins[i-1])+" && "+"Jpsi_TRUEPT<"+str(PTBins[i])

        effJpsiMC, errJpsiMC = calculateMCEff(JpsiNoPIDTuple,cutPT,False)
        effEtacMC, errEtacMC = calculateMCEff(EtacNoPIDTuple,cutPT,True)

        effJpsiPIDCal, errJpsiPIDCal = calculatePIDCalibEff(JpsiNoPIDTuple,cutPT,False)
        effEtacPIDCal, errEtacPIDCal = calculatePIDCalibEff(EtacNoPIDTuple,cutPT,True)

        effRMC = effJpsiMC/effEtacMC
        errRMC = effRMC*Sqrt(Sq(errJpsiMC/effJpsiMC)+Sq(errEtacMC/effEtacMC))

        effRPIDCal = effJpsiPIDCal/effEtacPIDCal
        errRPIDCal = effRPIDCal*Sqrt(Sq(errJpsiPIDCal/effJpsiPIDCal)+Sq(errEtacPIDCal/effEtacPIDCal))

        print "bin ",i,":"
       	print "effRMC     = ",effRMC,     " +- ", errRMC
       	print "effRPIDCal = ",effRPIDCal, " +- ", errRPIDCal, "\n"

        if i==0:
            rJpsi2EtacMCTot   = effRMC
            errJpsi2EtacMCTot = errRMC

            rJpsi2EtacPIDCalTot   = effRPIDCal
            errJpsi2EtacPIDCalTot = errRPIDCal
        else:
            rJpsi2EtacMC.SetBinContent(i,effRMC)
            rJpsi2EtacMC.SetBinError(i,errRMC)

            rJpsi2EtacPIDCal.SetBinContent(i,effRPIDCal)
            rJpsi2EtacPIDCal.SetBinError(i,errRPIDCal)



    cPIDEff = TCanvas("cPIDEff")
    cPIDEff.cd()
    gPad.SetLeftMargin(0.15)

    rJpsi2EtacMC.GetXaxis().SetTitle("p_{T}, [MeV/c]")
    rJpsi2EtacMC.GetYaxis().SetTitle("#epsilon_{J/#psi}^{MC}/#epsilon_{#eta_{c}}^{MC}")
    rJpsi2EtacMC.GetYaxis().SetTitleOffset(0.9)
    rJpsi2EtacMC.GetXaxis().SetTitleOffset(1.1)

    rJpsi2EtacPIDCal.SetLineColor(2)

    rJpsi2EtacMC.Draw("E1")
#    rJpsi2EtacPIDCal.SetFillColor(2)
    rJpsi2EtacPIDCal.SetMarkerSize(0.)
#    rJpsi2EtacPIDCal.SetFillStyle(3002)
    rJpsi2EtacPIDCal.Draw("same")






    hRJpsi2EtacMCTot = TH1F("hRJpsi2EtacMCTot","hRJpsi2EtacMCTot",1,PTBins[0],PTBins[nPTBins])
    hRJpsi2EtacMCTot.SetBinContent(1,rJpsi2EtacMCTot)
    hRJpsi2EtacMCTot.SetBinError(1,errJpsi2EtacMCTot)

    hRJpsi2EtacMCTot.SetFillColor(2)
    hRJpsi2EtacMCTot.SetFillStyle(3002)
    hRJpsi2EtacMCTot.SetMarkerSize(0.)
#    hRJpsi2EtacMCTot.Draw("E2 same")


    hRJpsi2EtacPIDCalTot = TH1F("hRJpsi2EtacPIDCalTot","hRJpsi2EtacPIDCalTot",1,PTBins[0],PTBins[nPTBins])
    hRJpsi2EtacPIDCalTot.SetBinContent(1,rJpsi2EtacPIDCalTot)
#    hRJpsi2EtacPIDCalTot.SetBinError(1,errJpsi2EtacPIDCalTot)
    hRJpsi2EtacPIDCalTot.SetLineColor(2)
#    hRJpsi2EtacPIDCalTot.SetFillColor(2)
#    hRJpsi2EtacPIDCalTot.SetFillStyle(3002)
    hRJpsi2EtacPIDCalTot.SetMarkerSize(0.)
#    hRJpsi2EtacPIDCalTot.Draw("same")



    leg = TLegend(0.2,0.75,0.4,0.92)
    leg.AddEntry(rJpsi2EtacMC,"MC")
    leg.AddEntry(rJpsi2EtacPIDCal,"PIDCalib")
    leg.Draw()

    texCS = TLatex()
    texCS.SetNDC()
    texCS.DrawLatex(0.15, 0.25, "LHCb simulation")
    cPIDEff.SaveAs("rPIDEff.pdf")

gROOT.LoadMacro("../lhcbStyle.C")
plot()



