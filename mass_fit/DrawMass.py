from ROOT import *
from DrawMass import *   
gROOT.LoadMacro("../libRooRelBreitWigner/RooRelBreitWigner.cxx+")
gROOT.LoadMacro("../BifurcatedCB/BifurcatedCB.cxx+")



histosDir = "Histos/"
homeDir = "/users/LHCb/zhovkovska/scripts/"

binningDict = {
    "Jpsi_PT":                      [6500, 8000, 10000, 12000, 14000, 18000],
    "Jpsi_ETA":                     [2.0, 2.45, 2.8, 3.15, 4.5],
    "TMath::Abs(ProtonM_CosTheta)": [0.0, 0.15, 0.3, 0.50, 1.0],
    "nSPDHits":                     [0, 100, 150, 200, 250, 300],
    "Jpsi_Tz":                      [-10., -0.125, -0.025, 0.0, 0.20, 2.0, 4.0, 10.0],
    #"Jpsi_Tz":                      [-10., -0.15, -0.025, 0.0, 0.025, 0.2, 1.0, 1.5, 2.0, 3.0, 4.0, 10], #[-10., -0.15, -0.025, 0.0, 0.20, 2.0, 10.0],
    "Jpsi_Tz_Alt":                  [-10.0, -0.15, -0.05, 0.,        0.100,   4., 10.],
    "Jpsi_Tz_old":                  [-10., -0.025, 0.0, 0.20, 2.0, 10.0],
    "Jpsi_Tz_Tot":                  [-10., -0.025, 0.0, 0.025, 0.2, 1.0, 1.5, 2.0, 3.0, 4.0, 10],
}


minM = 2850
maxM = 3250
nBins = 1000





def Draw(w, combData, sample, simPdf, nPTs, nTzs, nConf, keyPrompt=True, charm="Etac"):
    
    Jpsi_M = w.var("Jpsi_M")
    
    nPTbins = len(nPTs)
    nTzbins = len(nTzs)

    #sample = w.cat("sample")
    #simPdf = w.pdf("simPdf")
    #combData = w.data("combData")

    plot_binWidth = 10.
    plot_binN = int((maxM-minM)/plot_binWidth)

    binning = RooFit.Binning(plot_binN,minM,maxM)
    mrkSize = RooFit.MarkerStyle(7)
    lineWidth1 = RooFit.LineWidth(1)
    lineWidth2 = RooFit.LineWidth(2)
    lineStyle = RooFit.LineStyle(2)
    lineColor1 = RooFit.LineColor(6)
    lineColor2 = RooFit.LineColor(8)
    name = RooFit.Name
            
            
    frames = []
    pulls = []
    resids = []
    Jpsi_M.setBins(nBins)
    for nPT in nPTs:
        frames.append([])
        pulls.append([])
        resids.append([])
        idx = nPTs.index(nPT)
        for nTz in nTzs:
            idxTz = nTzs.index(nTz)
            frame = Jpsi_M.frame(RooFit.Title("PT bin %s, Tz bin %s"%(nPT,nTz)))
            combData.plotOn(frame, RooFit.Cut("sample==sample::PT%s_Tz%s"%(nPT,nTz)), binning, mrkSize, name("dataPT%sTz%s"%(nPT,nTz)))
            print nPT, nTz
            simPdf.plotOn(frame,RooFit.Slice(sample,"PT%s_Tz%s"%(nPT,nTz)),RooFit.ProjWData(RooArgSet(sample),combData,True),name("pdfPT%sTz%s"%(nPT,nTz)))
            bkg = w.pdf("bkg_PT%s_Tz%s"%(nPT,nTz))
            bkg.plotOn(frame, lineColor2, lineWidth1, lineStyle, name("pdf_bkg_PT%sTz%s"%(nPT,nTz)), RooFit.Normalization(w.var("nBckgr_PT%s_Tz%s"%(nPT,nTz)).getValV(), RooAbsReal.NumEvent))
            #modelBkg = w.pdf("modelBkg_PT%s_Tz%s"%(nPT,nTz))
            #modelBkg.plotOn(frame, lineColor2, lineWidth1, lineStyle, name("pdf_bg_PT%sTz%s"%(nPT,nTz)), RooFit.Normalization(1.0,RooAbsReal.RelativeExpected))
            modelSignal = w.pdf("modelSignal_PT%s_Tz%s"%(nPT,nTz))
            modelSignal.plotOn(frame, lineColor1, lineWidth2, name("pdf_signal_PT%sTz%s"%(nPT,nTz)), RooFit.Normalization(1.0,RooAbsReal.RelativeExpected))
            frame.GetXaxis().SetTitle("M_{p#bar{p}}, MeV/c^{2}")  
            frame.GetXaxis().SetTitleSize(0.06)
            frame.GetYaxis().SetTitleSize(0.06)
            frame.GetXaxis().SetTitleOffset(0.9)
            #frame.GetYaxis().SetTitleOffset(0.90)

            
            hresid = frame.residHist("dataPT%sTz%s"%(nPT,nTz),"pdf_bkg_PT%sTz%s"%(nPT,nTz))
            hresid.SetMarkerSize(0.5)
            #hresid = frame.residHist()
            frame_res = Jpsi_M.frame(RooFit.Bins(100)) 
            frame_res.addPlotable(hresid,"P") 
            pppi0 = w.pdf("pppi0_PT%s_Tz%s"%(nPT,nTz))
            nPPPi0 = w.function("nPPPi0_PT%s_Tz%s"%(nPT,nTz)).getValV() 
            pppi0.plotOn(frame_res, lineColor2, lineWidth2, RooFit.Normalization(nPPPi0, RooAbsReal.NumEvent))
            modelSignal.plotOn(frame_res, lineColor1, lineWidth2, RooFit.Normalization(1.0,RooAbsReal.RelativeExpected))
            frame_res.GetXaxis().SetTitle("M_{p#bar{p}}, MeV/c^{2}")  
            frame_res.GetXaxis().SetTitleSize(0.06)
            frame_res.GetYaxis().SetTitleSize(0.00)
            frame_res.GetXaxis().SetLabelSize(0.06)
            frame_res.GetYaxis().SetLabelSize(0.06)
            frame_res.GetYaxis().SetNdivisions(505)
            
            hpull = frame.pullHist("dataPT%sTz%s"%(nPT,nTz),"pdfPT%sTz%s"%(nPT,nTz),True)
            frame_pull = Jpsi_M.frame()
            for ii in range(hpull.GetN()):
                hpull.SetPointEYlow(ii,0)
                hpull.SetPointEYhigh(ii,0)
            frame_pull.addPlotable(hpull,"B")
            frame_pull.SetMinimum(-4.0)
            frame_pull.SetMaximum(4.0)
            frame_pull.GetXaxis().SetTitleSize(0.0)
            frame_pull.GetXaxis().SetLabelSize(0.06)
            frame_pull.GetYaxis().SetLabelSize(0.06)
            
            frames[idx].append(frame)
            pulls[idx].append(frame_pull)
            resids[idx].append(frame_res)



    if keyPrompt: inCutKey = "prompt"
    else:         inCutKey = "fromB"
    
    #if nPT!=0:    binPT = "Jpsi_PT/"
    #else:         binPT = "Total/"

    binPT = "Sim/"
    #binTz = "Tz%s.root"%(nTz)
    
    dirName = homeDir+"Results/MassFit/" + inCutKey + "/" + binPT + "m_scaled/"
    
    #if (nPT!=0 and nTz!=0):
        #dirName = dirName + charm + "/"
    if nPTs == [0]:
        ptNum = "_total_sigma"
    else:
        ptNum = ""
    
    gStyle.SetOptTitle(0)
    texData = TLatex()        
    texData.SetNDC()
    texData.SetTextSize(0.06)
    TGaxis.SetMaxDigits(4)

    nameTxt  = dirName + charm + "_fitRes_C%d%s.txt"%(nConf, ptNum)
    nameRoot = dirName + charm + "_Jpsi_MassFit_C%d%s.root"%(nConf, ptNum)
    nameWksp = dirName + charm + "_Wksp_MassFit_C%d%s.root"%(nConf, ptNum)
    
    fFit = TFile (nameRoot,"RECREATE")
    c = []
    for nPT in range(nPTbins):
        c.append(TCanvas("c%s"%nPT,"c",1600,800))
        c[nPT].Divide(nTzbins,3, 0.001, 0.001)
        for i in range(nTzbins):
            pad = c[nPT].cd(i+1)
            xl = pad.GetXlowNDC(); xh = xl + pad.GetWNDC()
            yl = pad.GetYlowNDC(); yh = yl + pad.GetHNDC()
            pad.SetPad(xl,yl-0.2,xh,yh)
            pad.SetBottomMargin(0.15)
            pad.SetLeftMargin(0.15)
            frames[nPT][i].Draw()

            tzL = binningDict["Jpsi_Tz"][i]
            tzR = binningDict["Jpsi_Tz"][i+1]
            texData.DrawLatex(0.3, 0.95, "%s<t_{z}<%s"%(tzL,tzR))
            #ptL = binningDict["Jpsi_PT"][nPT]
            #ptR = binningDict["Jpsi_PT"][nPT+1]
            #texData.DrawLatex(0.3, 0.95, "%s<p_{T}<%s"%(ptL,ptR))

            pad = c[nPT].cd(nTzbins+i+1)
            xl = pad.GetXlowNDC(); xh = xl + pad.GetWNDC()
            yl = pad.GetYlowNDC(); yh = yl + pad.GetHNDC()
            pad.SetPad(xl,yl-0.1,xh,yh-0.2)
            pad.SetBottomMargin(0.15)
            pad.SetLeftMargin(0.15)
            #pad.SetTopMargin(0)
            #pad.SetTickx(2)
            #pad.SetTicky(2)
            resids[nPT][i].Draw()


            pad = c[nPT].cd(2*nTzbins+i+1)
            xl = pad.GetXlowNDC(); xh = xl + pad.GetWNDC()
            yl = pad.GetYlowNDC(); yh = yl + pad.GetHNDC()
            #pad.SetTickx(2)
            #pad.SetTicky(2)
            pad.SetPad(xl,yl,xh,yh-0.1)
            pad.SetLeftMargin(0.15)
            pulls[nPT][i].Draw()
        c[nPT].SaveAs(dirName + charm + "_MassFit_nPT%d_C%d%s.pdf"%(nPT+1,nConf, ptNum))    
        c[nPT].Write("")

    fFit.Close()

    #import os, sys 
    #save = os.dup( sys.stdout.fileno() ) 
    #newout = file(nameTxt, 'w' ) 
    #os.dup2( newout.fileno(), sys.stdout.fileno() ) 
    #r.Print("v") 
    ##print w.var("rNormJpsi_Tz1").getValV(), "\n"
    ##print w.var("rNormJpsi_Tz2").getValV(), "\n"
    ##print w.var("rNormJpsi_Tz3").getValV(), "\n"
    ##print w.var("rNormJpsi_Tz4").getValV(), "\n"
    ##print w.var("rNormJpsi_Tz5").getValV(), "\n"
    #r.correlationMatrix().Print()
    #os.dup2( save, sys.stdout.fileno() ) 
    #newout.close()
        
    w.writeToFile(nameWksp)
    
    #w.Print("v")
