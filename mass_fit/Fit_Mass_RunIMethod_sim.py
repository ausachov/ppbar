
from ROOT import *
gROOT.LoadMacro("../libRooRelBreitWigner/RooRelBreitWigner.cxx+")
gROOT.LoadMacro("../BifurcatedCB/BifurcatedCB.cxx+")



histosDir = "/users/LHCb/usachov/zhovkovska2018/scripts/Histos/"
homeDir = "/users/LHCb/usachov/zhovkovska2018/scripts/"

binningDict = {
    "Jpsi_PT":                      [6500, 8000, 10000, 12000, 14000, 18000],
    "Jpsi_ETA":                     [2.0, 2.45, 2.8, 3.15, 4.5],
    "TMath::Abs(ProtonM_CosTheta)": [0.0, 0.15, 0.3, 0.50, 1.0],
    "nSPDHits":                     [0, 100, 150, 200, 250, 300],
}


minM = 2850
maxM = 3250
nBins = 1000

from setupPlot import *
from setupFitModel_RunI import *

def promptImp():
    

    #ntEtac_Prompt = TChain('DecayTree')
    #ntEtac_FromB = TChain('DecayTree')
    ntJpsi_Prompt = TChain('DecayTree')
    ntJpsi_FromB = TChain('DecayTree')
    
    #ntEtac_Prompt.Add(homeDir+'MC/newMC/Etac/EtacDiProton_MC_2016_AddBr.root')
    #ntEtac_Prompt.Add(homeDir+'MC/newMC/Etac/EtacDiProton_MC_2015_AddBr.root')
    #ntEtac_FromB.Add(homeDir+'MC/newMC/incl_b/EtacDiProton_MC_2015_incl_b_AddBr.root')
    #ntEtac_FromB.Add(homeDir+'MC/newMC/incl_b/EtacDiProton_MC_2016_incl_b_AddBr.root')
    ntJpsi_Prompt.Add(homeDir+'MC/newMC/Jpsi/JpsiDiProton_MC_2016_AddBr.root')
    ntJpsi_Prompt.Add(homeDir+'MC/newMC/Jpsi/JpsiDiProton_MC_2015_AddBr.root')
    ntJpsi_FromB.Add(homeDir+'MC/newMC/Jpsi/JpsiDiProton_MC_2016_AddBr.root')
    ntJpsi_FromB.Add(homeDir+'MC/newMC/Jpsi/JpsiDiProton_MC_2015_AddBr.root')
    
    #treeEtac_Prompt = TTree()
    #treeEtac_FromB = TTree()
    treeJpsi_Prompt = TTree()
    treeJpsi_FromB = TTree()
    
    #treeEtac_Prompt = ntEtac_Prompt.CopyTree("prompt")
    #treeEtac_FromB = ntEtac_FromB.CopyTree("sec")
    treeJpsi_Prompt = ntJpsi_Prompt.CopyTree("prompt")
    treeJpsi_FromB = ntJpsi_FromB.CopyTree("sec")
    
    #NEtac_Prompt = treeEtac_Prompt.GetEntries()
    #NEtac_FromB = treeEtac_FromB.GetEntries()
    NJpsi_Prompt = treeJpsi_Prompt.GetEntries()
    NJpsi_FromB = treeJpsi_FromB.GetEntries()

    cutJpsi_Tz_High = 'Jpsi_Tz > 0.08'
    cutJpsi_IP_CHI2 = '((ProtonP_IPCHI2_OWNPV > 16) && (ProtonM_IPCHI2_OWNPV > 16))'
    
    #treeEtac_Prompt = ntEtac_Prompt.CopyTree('prompt' + '&&' + cutJpsi_IP_CHI2 + '&&' + cutJpsi_Tz_High)
    #treeEtac_FromB = ntEtac_FromB.CopyTree('sec' + '&&' + cutJpsi_IP_CHI2 + '&&' + cutJpsi_Tz_High)
    treeJpsi_Prompt = ntJpsi_Prompt.CopyTree('prompt' + '&&' + cutJpsi_IP_CHI2 + '&&' + cutJpsi_Tz_High)
    treeJpsi_FromB = ntJpsi_FromB.CopyTree('sec' + '&&' + cutJpsi_IP_CHI2 + '&&' + cutJpsi_Tz_High)

    #NEtac_Prompt_Cuted = treeEtac_Prompt.GetEntries()
    #NEtac_FromB_Cuted = treeEtac_FromB.GetEntries()
    NJpsi_Prompt_Cuted = treeJpsi_Prompt.GetEntries()
    NJpsi_FromB_Cuted = treeJpsi_FromB.GetEntries()

    effPS = float(NJpsi_Prompt_Cuted/float(NJpsi_Prompt))
    effSS = float(NJpsi_FromB_Cuted/float(NJpsi_FromB))
       
    #print    NJpsi_Prompt_Cuted, NJpsi_Prompt
    effPS_err = effPS*(1/float(NJpsi_Prompt_Cuted)+1/float(NJpsi_Prompt))**0.5
    effSS_err = effSS*(1/float(NJpsi_FromB_Cuted)+1/float(NJpsi_FromB))**0.5


    cutJpsi_Tz_Low = 'Jpsi_Tz < 0.08'

    #treeEtac_Prompt = ntEtac_Prompt.CopyTree('prompt' + '&&' + cutJpsi_Tz_Low)
    #treeEtac_FromB = ntEtac_FromB.CopyTree('sec' + '&&' + cutJpsi_Tz_Low)
    treeJpsi_Prompt = ntJpsi_Prompt.CopyTree('prompt' + '&&' + cutJpsi_Tz_Low)
    treeJpsi_FromB = ntJpsi_FromB.CopyTree('sec' + '&&' + cutJpsi_Tz_Low)

    #NEtac_Prompt_Cuted = treeEtac_Prompt.GetEntries()
    #NEtac_FromB_Cuted = treeEtac_FromB.GetEntries()
    NJpsi_Prompt_Cuted = treeJpsi_Prompt.GetEntries()
    NJpsi_FromB_Cuted = treeJpsi_FromB.GetEntries()
    
    effPP = float(NJpsi_Prompt_Cuted/float(NJpsi_Prompt))
    effSP = float(NJpsi_FromB_Cuted/float(NJpsi_FromB))

    effPP_err = effPP*(1/float(NJpsi_Prompt_Cuted)+1/float(NJpsi_Prompt))**0.5
    effSP_err = effSP*(1/float(NJpsi_FromB_Cuted)+1/float(NJpsi_FromB))**0.5
    
    return effPP, effPS, effSP, effSS, effPP_err, effPS_err, effSP_err, effSS_err


def correctedYield(w, nPT, nConf):
    
    nConfsEff = [8, 9, 10, 11]

    effPP, effPS, effSP, effSS, effPP_err, effPS_err, effSP_err, effSS_err  = promptImp()
    
    if nConf==nConfsEff[0]:
            effPP += effPP_err
    elif nConf==nConfsEff[1]:
            effPS += effPS_err
    elif nConf==nConfsEff[2]:
            effSP += effSP_err
    elif nConf==nConfsEff[3]:
            effSS += effSS_err            
        
    
    nJpsi_Pr = w.var("nJpsi_Prompt").getValV()
    nJpsi_FrB = w.var("nJpsi_FromB").getValV()
    nEtac_Pr = w.var("nEta_Prompt").getValV()
    nEtac_FrB = w.var("nEta_FromB").getValV()

    nJpsi_Pr_Err  = w.var("nJpsi_Prompt").getError()
    nJpsi_FrB_Err = w.var("nJpsi_FromB").getError()
    nEtac_Pr_Err  = w.var("nEta_Prompt").getError()
    nEtac_FrB_Err = w.var("nEta_FromB").getError()
        
    NRel_Pr  = (nEtac_Pr*effSS - nEtac_FrB*effSP) / (nJpsi_Pr*effSS - nJpsi_FrB*effSP)
    NRel_FrB = (nEtac_FrB*effPP - nEtac_Pr*effPS) / (nJpsi_FrB*effPP - nJpsi_Pr*effPS)

    
    dNRel_Pr  = NRel_Pr * ( ((nEtac_Pr_Err*effSS)**2 + (nEtac_FrB_Err*effSP)**2)/(nEtac_Pr*effSS - nEtac_FrB*effSP)**2 + ((nJpsi_Pr_Err*effSS)**2 + (nJpsi_FrB_Err*effSP)**2)/(nJpsi_Pr*effSS - nJpsi_FrB*effSP)**2 )**0.5
    dNRel_FrB = NRel_FrB * ( ((nEtac_FrB_Err*effPP)**2 + (nEtac_Pr_Err*effPS)**2)/(nEtac_FrB*effPP - nEtac_Pr*effPS)**2 + ((nJpsi_FrB_Err*effPP)**2 + (nJpsi_Pr_Err*effPS)**2)/(nJpsi_FrB*effPP - nJpsi_Pr*effPS)**2 )**0.5

    nameTxt = homeDir+"Results/MassFit/RunIMethod/correctedYield_PT%s_C%s.txt"%(nPT,nConf)
    
    fo = open(nameTxt,'w')

    fo.write("nEtac_Prompt Relative = %2.4f +/- %2.4f, %3.2f \n"   %( NRel_Pr, dNRel_Pr, NRel_Pr/dNRel_Pr ))
    fo.write("nEtac_FromB Relative = %2.4f +/- %2.4f, %3.2f \n"   %( NRel_FrB, dNRel_FrB, NRel_FrB/dNRel_FrB ))

    NEtac_Prompt = RooRealVar("NEtac_Prompt","num of Etac", NRel_Pr); NEtac_Prompt.setError(dNRel_Pr)
    NEtac_FromB = RooRealVar("NEtac_FromB","num of Etac", NRel_FrB); NEtac_FromB.setError(dNRel_FrB)

    getattr(w,'import')(NEtac_Prompt)
    getattr(w,'import')(NEtac_FromB)

    fo.close()


def getData_h(w, keyPrompt, nPT=0, nTz=0, shift=False):
       
    
    if keyPrompt: 
        inCutKey = "prompt_l0TOS"
        histName = "dh_PT%s_Prompt"%(nPT)
    else:         
        inCutKey = "secondary_l0TOS"
        histName = "dh_PT%s_FromB"%(nPT)
    
    if nPT!=0:    binPT = "Jpsi_PT/bin%s_"%(nPT) 
    else:         binPT = "Total/"

    binTz = "Tz%s.root"%(nTz)
    
    outDirName_2015 = homeDir+"Histos/2015/DiProton/" + inCutKey + "/"
    outDirName_2016 = homeDir+"Histos/2016/DiProton/" + inCutKey + "/"
        
    nameFile = outDirName_2015 + binPT + binTz
    f_2015 = TFile(nameFile,"read")
    print nameFile
    nameFile = outDirName_2016 + binPT + binTz
    f_2016 = TFile(nameFile,"read")
    print nameFile
    
    
    hh = TH1D()
    hh = f_2015.Get("Jpsi_M")
    hh.Add(f_2016.Get("Jpsi_M"))
    
    #Jpsi_M = RooRealVar("Jpsi_M","Jpsi_M",2850,3250) 
    Jpsi_M = w.var("Jpsi_M") 

    if(shift):
        hh.SetBins(276,2705,3395)
        Jpsi_M.setRange(2855,3245) 

    dh = RooDataHist(histName,histName, RooArgList(Jpsi_M) ,hh)
    getattr(w,'import')(dh)
    f_2015.Close()
    f_2016.Close()
    
    print "DATA\'S READ SUCCESSFULLY"



def fitData(nPTs, nConf):
    w = RooWorkspace('w',True)   


    models = {}
    hists = {}

    sample = RooCategory('sample','sample') 
    for nPT in nPTs:
        promptKey = 'PT%s_Prompt'%(nPT)
        fromBKey  = 'PT%s_FromB'%(nPT)

        sample.defineType(promptKey) 
        sample.defineType(fromBKey)

        fillRelWorkspace(w, nConf, nPT, promptKey)
        fillRelWorkspace(w, nConf, nPT, fromBKey)

        getData_h(w, True,  nPT, 0)
        getData_h(w, False, nPT, 0)

        model_Prompt = w.pdf("model_%s"%(promptKey))
        model_FromB  = w.pdf("model_%s"%(fromBKey))
        models[promptKey] = model_Prompt
        models[fromBKey] = model_FromB

        hist_Prompt = w.data('dh_%s'%(promptKey))
        hist_FromB = w.data('dh_%s'%(fromBKey))
        hists[promptKey] = hist_Prompt
        hists[fromBKey] = hist_FromB

    sample.defineType("Prompt")
    sample.defineType("FromB")

    Jpsi_M = w.var('Jpsi_M')
    
    
    
    # # Construct combined dataset in (Jpsi_M,sample)
    combDataFromB = RooDataHist("combData_FromB", "combined data", RooArgList(Jpsi_M), RooFit.Index(sample), 
        RooFit.Import('PT1_FromB', hists['PT1_FromB']), 
        RooFit.Import('PT2_FromB', hists['PT2_FromB']),
        RooFit.Import('PT3_FromB', hists['PT3_FromB']),
        RooFit.Import('PT4_FromB', hists['PT4_FromB']))
    combDataPrompt = RooDataHist("combData_Prompt", "combined data", RooArgList(Jpsi_M), RooFit.Index(sample), 
        RooFit.Import('PT1_Prompt', hists['PT1_Prompt']), 
        RooFit.Import('PT2_Prompt', hists['PT2_Prompt']),
        RooFit.Import('PT3_Prompt', hists['PT3_Prompt']),
        RooFit.Import('PT4_Prompt', hists['PT4_Prompt']))


    
    combData = RooDataHist("combData","combData",RooArgList(Jpsi_M,sample),RooFit.Index(sample),RooFit.Import("Prompt",combDataPrompt),RooFit.Import("FromB",combDataFromB))

    
    
    # Associate model with the physics state and model_ctl with the control state
    simPdf = RooSimultaneous('simPdf','simultaneous signal pdf',sample)
    for key in models.keys():
        simPdf.addPdf(models[key],key) 



    

    # sigma = w.var('sigma_eta_1')
    mass_Jpsi = w.var('mass_Jpsi')
    mass_res = w.var('mass_res')
    gamma_eta = w.var('gamma_eta')
    
    

    mass_Jpsi.setConstant(True)
    mass_res.setConstant(True)

    # w.var("a2_FromB").setVal(0.)
    # w.var("a2_FromB").setConstant(True)


    mass_Jpsi.setConstant(False)
    mass_res.setConstant(False)



    massJpsi_tot = 3097.0; massJpsi_totErr = 0.11; 
    massres_tot  = 112.60; massres_totErr =  0.86; 

    fconstrJpsi = RooGaussian("fconstrEtac","fconstrEtac",mass_Jpsi, RooFit.RooConst(massJpsi_tot), RooFit.RooConst(massJpsi_totErr))  #PDG 
    fconstrEtac = RooGaussian("fconstrJpsi","fconstrJpsi",mass_res, RooFit.RooConst(massres_tot),RooFit.RooConst(massres_totErr))
    constraints = RooFit.ExternalConstraints(RooArgSet(fconstrEtac,fconstrJpsi))




    simPdf.fitTo(combData, RooFit.Extended(True), RooFit.Offset(True), RooFit.Save(True))
    simPdf.fitTo(combData, RooFit.Extended(True), RooFit.Offset(True), RooFit.Save(True), RooFit.NumCPU(48)) 
    simPdf.fitTo(combData, RooFit.Extended(True), RooFit.Offset(True), RooFit.Save(True))
    simPdf.fitTo(combData, RooFit.Extended(True), RooFit.Offset(True), RooFit.Save(True), RooFit.NumCPU(48))
    simPdf.fitTo(combData, RooFit.Extended(True), RooFit.Offset(True), RooFit.Save(True))
    simPdf.fitTo(combData, RooFit.Extended(True), RooFit.Offset(True), RooFit.Save(True), RooFit.NumCPU(48))
    # correctedYield(w, nPT, nConf)
    


    # frames = []; pulls = []; resids = []
    # if nConf!=3:
    #     signal = RooArgSet(w.pdf("gauss_1"), w.pdf("gauss_2"),w.pdf("bwxg_1"), w.pdf("bwxg_2")) 
    # else:
    #     signal = RooArgSet(w.pdf("cb_Jpsi_1"),w.pdf("bwxg_1"))
    # fullBkg_Prompt =  RooArgSet(w.pdf("bkg_Prompt"),w.pdf("pppi0"))  
    # fullBkg_FromB =  RooArgSet(w.pdf("bkg_FromB"),w.pdf("pppi0"))       

    # frames0, resid0, pull0, chi2_Prompt = setupFrame(Jpsi_M, 'Prompt', hist_Prompt,model_Prompt,signal, fullBkg_Prompt)
    # frames1, resid1, pull1, chi2_FromB = setupFrame(Jpsi_M, 'FromB', hist_FromB, model_FromB,signal,fullBkg_FromB)
    
    # frames.append(frames0); frames.append(frames1)
    # resids.append(resid0); resids.append(resid1)
    # pulls.append(pull0);  pulls.append(pull1)
 
    
    # inCutKey = "RunIMethod"
    
    # #binPT = "RunIMethod/"
    # #binTz = "Tz%s.root"%(nTz)
    
    # outDirName = homeDir+"Results/MassFit/" + inCutKey + "/" 
            
    # nameTxt     = outDirName + "fitRes_PT%s_C%d.txt"%(nPT,nConf)
    # nameRoot    = outDirName + "Jpsi_MassFit_PT%s_C%d.root"%(nPT,nConf)
    # nameWksp    = outDirName + "Wksp_MassFit_PT%s_C%d.root"%(nPT,nConf)

    # names = ["prompt sample", "b-decays sample"]
    # c = setupCanvas(names, frames, resids, pulls) 
    # namePic  = outDirName + "Jpsi_MassFit_PT%s_C%d.pdf"%(nPT,nConf)   
    # c.SaveAs(namePic)
    
    # import os, sys 
    # save = os.dup( sys.stdout.fileno() ) 
    # newout = file(nameTxt, 'w' ) 
    # os.dup2( newout.fileno(), sys.stdout.fileno() ) 
    # r.Print("v") 
    # r.correlationMatrix().Print()
    # os.dup2( save, sys.stdout.fileno() ) 
    # newout.close()

    # fo = open(nameTxt,'a')
    # fo.write('$chi^2 prompt$ %6.4f \n'  %(chi2_Prompt))
    # fo.write('$chi^2 from-b$ %6.4f \n'  %(chi2_FromB))
    # fo.close()

    
    # w.writeToFile(nameWksp)

    # fFit = TFile (nameRoot,'RECREATE')
    # fFit.Write()
    # fFit.Close()
    
    # r.correlationMatrix().Print('v')
    # r.globalCorr().Print('v')


#fitData(1, 0)
#print promptImp()

# nConfs = [0,2,3,4,5,6,7]     
fitData(nPTs=[1,2,3,4], nConf=4)
