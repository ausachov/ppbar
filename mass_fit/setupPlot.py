from ROOT import *
gROOT.LoadMacro("../libRooRelBreitWigner/RooRelBreitWigner.cxx+")
gROOT.LoadMacro("../BifurcatedCB/BifurcatedCB.cxx+")



histosDir = "/users/LHCb/usachov/zhovkovska2018/scripts/Histos/"
homeDir = "/users/LHCb/usachov/zhovkovska2018/scripts/"

binningDict = {
    "Jpsi_PT":                      [6500, 8000, 10000, 12000, 14000, 18000],
    "Jpsi_ETA":                     [2.0, 2.45, 2.8, 3.15, 4.5],
    "TMath::Abs(ProtonM_CosTheta)": [0.0, 0.15, 0.3, 0.50, 1.0],
    "nSPDHits":                     [0, 100, 150, 200, 250, 300],
}


minM = 2850.
maxM = 3250.
nBins = 1000

def setupFrame(Jpsi_M, namePostFix,hist,model,signal,fullBkg):

    nameHist = "hist_"+namePostFix
    ## Construct plot frame and draw
    Title = RooFit.Title
    Components = RooFit.Components
    LineColor = RooFit.LineColor
    Normalization = RooFit.Normalization  
    Normalization1 = RooFit.Normalization(1.0,RooAbsReal.RelativeExpected)
    Normalization0 = RooFit.Normalization(0.0,RooAbsReal.RelativeExpected)


    plot_binWidth = 10.
    plot_binN = int((maxM-minM)/plot_binWidth)
    binning = RooFit.Binning(plot_binN,minM,maxM)
    mrkStyle = RooFit.MarkerStyle(7)
    lineWidth1 = RooFit.LineWidth(1)
    lineWidth2 = RooFit.LineWidth(2)
    lineWidth0 = RooFit.LineWidth(0)
    lineColor1 = RooFit.LineColor(6)
    lineColor2 = RooFit.LineColor(8)
    lineStyle  = RooFit.LineStyle(2)
    name = RooFit.Name

     
    frame = Jpsi_M.frame(Title(" "))
    frame.GetXaxis().SetTitle("M_{p#bar{p}}, MeV/c^{2}")
    frame.GetXaxis().SetTitleSize(0.06)
    frame.GetYaxis().SetTitleSize(0.06)
    frame.GetXaxis().SetTitleOffset(0.9) 

    hist.plotOn(frame, name(nameHist), binning,mrkStyle)
    model.plotOn(frame,Normalization1,lineWidth2, name("pdf_"+namePostFix))
    model.plotOn(frame,Components(signal),lineColor1, lineWidth2, Normalization1)
    model.plotOn(frame,Components(fullBkg),lineColor2, lineWidth1, lineStyle, Normalization1)
    model.plotOn(frame,Components("bkg_"+namePostFix), lineWidth0, Normalization1, name("pdfCombBkg_"+namePostFix))
    hist.plotOn(frame, name(nameHist), binning,mrkStyle)
    chi2 = frame.chiSquare()
    print 'chi2 = ', chi2


    
    hResid = frame.residHist("hist_"+namePostFix,"pdfCombBkg_"+namePostFix)
    hResid.SetMarkerSize(0.5)
    resid = Jpsi_M.frame(Title(" ")) 
    resid.GetXaxis().SetTitle("M_{p#bar{p}}, MeV/c^{2}") 
    resid.GetXaxis().SetTitleSize(0.06)
    resid.GetYaxis().SetTitleSize(0.00)
    resid.GetXaxis().SetLabelSize(0.06)
    resid.GetYaxis().SetLabelSize(0.06)
    resid.GetYaxis().SetNdivisions(505)
    resid.addPlotable(hResid,"P") 
    model.plotOn(resid, Components("pppi0"),lineColor2, lineWidth2,Normalization1)
    model.plotOn(resid, Components(signal),lineColor1, lineWidth2,Normalization1)
    resid.addPlotable(hResid,"P") 
    

    hPull = frame.pullHist("hist_"+namePostFix,"pdf_"+namePostFix,True)
    hPull.SetFillColor(1)
    pull = Jpsi_M.frame(Title(" ")) 
    for ii in range(hPull.GetN()):
        hPull.SetPointEYlow(ii,0)
        hPull.SetPointEYhigh(ii,0)
    pull.SetMinimum(-4.0)
    pull.SetMaximum(4.0)
    pull.GetXaxis().SetTitleSize(0.0)
    pull.GetXaxis().SetLabelSize(0.06)
    pull.GetYaxis().SetLabelSize(0.06)
    pull.addPlotable(hPull,"B")

    return frame, resid, pull, chi2



def setupCanvas(names, frames, resids, pulls):

    nPads = len(names)

    texData = TLatex()
    texData.SetNDC()
    texData.SetTextSize(0.06)
    TGaxis.SetMaxDigits(4)

    c = TCanvas("c1","c1", 1400, 700)
    c.Divide(nPads, 3, 0.001, 0.001)
    for i in range(nPads):
            pad = c.cd(i+1)
            xl = pad.GetXlowNDC(); xh = xl + pad.GetWNDC()
            yl = pad.GetYlowNDC(); yh = yl + pad.GetHNDC()
            pad.SetPad(xl,yl-0.2,xh,yh)
            pad.SetBottomMargin(0.15)
            pad.SetLeftMargin(0.15)
            frames[i].Draw()

            texData.DrawLatex(0.3, 0.95, names[i])

            #ptL = binningDict["Jpsi_PT"]
            #ptR = binningDict["Jpsi_PT"][nPT+1]
            #texData.DrawLatex(0.3, 0.95, "%s<p_{T}<%s"%(ptL,ptR))

            pad = c.cd(2+i+1)
            xl = pad.GetXlowNDC(); xh = xl + pad.GetWNDC()
            yl = pad.GetYlowNDC(); yh = yl + pad.GetHNDC()
            pad.SetPad(xl,yl-0.1,xh,yh-0.2)
            pad.SetBottomMargin(0.15)
            pad.SetLeftMargin(0.15)
            #pad.SetTopMargin(0)
            #pad.SetTickx(2)
            #pad.SetTicky(2)
            resids[i].Draw()


            pad = c.cd(2*2+i+1)
            xl = pad.GetXlowNDC(); xh = xl + pad.GetWNDC()
            yl = pad.GetYlowNDC(); yh = yl + pad.GetHNDC()
            #pad.SetTickx(2)
            #pad.SetTicky(2)
            pad.SetPad(xl,yl,xh,yh-0.1)
            pad.SetLeftMargin(0.15)
            pulls[i].Draw()
    return c
