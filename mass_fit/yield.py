from array import array
from ROOT import *
gROOT.LoadMacro("../libRooRelBreitWigner/RooRelBreitWigner.cxx+")
gROOT.LoadMacro("../BifurcatedCB/BifurcatedCB.cxx+")

charmDict = {
    "Jpsi":     0,
    "Etac":     1,
}

homeDir = "/users/LHCb/zhovkovska/scripts/"

def yieldCharm(nPT, nConf, keyPrompt=True):
    #gROOT.Reset()

    #nBinsEtac = 5
    #nBinsJpsi = 7
    nBinsEtac = 10
    nBinsJpsi = 10

    yJpsi = array("d", 15*[0])
    yEtac = array("d", 15*[0])
    
    yJpsiErr = array("d", 15*[0])
    yEtacErr = array("d", 15*[0])

    tzTotalBinning =  array("d",[-10., -0.025, 0.0, 0.025, 0.2, 1.0, 1.5, 2.0, 3.0, 4.0, 10])
    tzEtacBinning = array("d", [-10., -0.025, 0.,         0.200,  2., 10.])
    tzJpsiBinning = array("d", [-10., -0.025, 0.,         0.200,  2., 10.])
    #tzJpsiBinning = array("d", [-10., -0.025, 0., 0.025, 0.1, 1., 4., 10.])
        
    if keyPrompt: inCutKey = "prompt"
    else:         inCutKey = "fromB"
    
    if nPT!=0:    binPT = "Jpsi_PT/"
    else:         binPT = "Total/"
    
    dirName = homeDir+"Results/MassFit/" + inCutKey + "/" + binPT        
    
    
    #nameTxt  = dirName + "/fitRes_PT%s_Tz%d_C%d.txt"%(nPT,iTz,nConf)
    #nameRoot = dirName + "/Jpsi_MassFit_PT%s_Tz%d_C%d.root"%(nPT,iTz,nConf)
    #namePic  = dirName + "/Jpsi_MassFit_PT%s_Tz%d_C%d.pdf"%(nPT,iTz,nConf)
    w = RooWorkspace("w", True)
    
    hhJpsi = TH1D()
    hhEtac = TH1D()
    hhJpsi.SetTitle("yield")
    hhEtac.SetTitle("yield")

    
    if nPT == 0:
        fo = open(homeDir+"signal/signal_%s.txt"%nConf, "w")
        fo_sign = open(homeDir+"signal/signif/signif_Mass_%s.txt"%nConf, "w")

        hhJpsi.SetBins(len(tzTotalBinning)-1, tzTotalBinning)
        hhEtac.SetBins(len(tzTotalBinning)-1, tzTotalBinning)

        fo.write( str(len(tzTotalBinning)-1) )
        fo.write( "\n" )
        fo_sign.write( str(len(tzTotalBinning)-1) )
        fo_sign.write( "\n" )
        for iTz in range(len(tzTotalBinning)-1):

            nameWksp = dirName + "Wksp_MassFit_PT%s_Tz%d_C%d.root"%(nPT,iTz+1,nConf)
            f1 = TFile(nameWksp,"READ")
            w = f1.Get("w") 
            
            fo.write("%s %s   %6.2f    %6.2f \n"%( (tzTotalBinning[iTz]+tzTotalBinning[iTz+1])/2., (tzTotalBinning[iTz+1]-tzTotalBinning[iTz])/2., w.var("nJpsi").getValV(),w.var("nJpsi").getError()))
            fo_sign.write("%6.2f   %6.2f  %3.3f \n"%( w.var("nJpsi").getValV(), w.var("nJpsi").getError(), w.var("nJpsi").getValV()/w.var("nJpsi").getError()))
            hhJpsi.SetBinContent(iTz+1, w.var("nJpsi").getValV())
            hhJpsi.SetBinError(iTz+1, w.var("nJpsi").getError())
            #print ((tzTotalBinning[iTz]+tzTotalBinning[iTz+1])/2.0, (tzTotalBinning[iTz+1]-tzTotalBinning[iTz])/2.0)
            f1.Close()

        fo.write( str(len(tzTotalBinning)-1) )
        fo.write( "\n" )
        fo_sign.write( str(len(tzTotalBinning)-1) )
        fo_sign.write( "\n" )
        for iTz in range(len(tzTotalBinning)-1):

            nameWksp = dirName + "Wksp_MassFit_PT%s_Tz%d_C%d.root"%(nPT,iTz+1,nConf)
            f1 = TFile(nameWksp,"READ")
            w = f1.Get("w") 
            
            fo.write("%s %s   %6.2f    %6.2f\n"%( (tzTotalBinning[iTz]+tzTotalBinning[iTz+1])/2., (tzTotalBinning[iTz+1]- tzTotalBinning[iTz])/2., w.var("nEta").getValV(),w.var("nEta").getError()))
            fo_sign.write("%6.2f   %6.2f   %3.3f \n"%(  w.var("nEta").getValV(),w.var("nEta").getError(), w.var("nEta").getValV()/w.var("nEta").getError() ))
            hhEtac.SetBinContent(iTz+1, w.var("nEta").getValV())
            hhEtac.SetBinError(iTz+1, w.var("nEta").getError())
            #print ((tzTotalBinning[iTz]+tzTotalBinning[iTz+1])/2.0, (tzTotalBinning[iTz+1]-tzTotalBinning[iTz])/2.0)

            f1.Close()

        fo.close()
        fo_sign.close()    

    else:
        fo = open(homeDir+"signal/signal_PT%s_%s_Jpsi5Bins.txt"%(nPT,nConf), "w")
        if nPT == 1:    fo_sign = open(homeDir+"signal/signif/signif_Mass_PTBins_%s_Jpsi5Bins.txt"%nConf, "w")
        else:           fo_sign = open(homeDir+"signal/signif/signif_Mass_PTBins_%s_Jpsi5Bins.txt"%nConf, "a")

        hhJpsi.SetBins(len(tzEtacBinning)-1, tzJpsiBinning)
        hhEtac.SetBins(len(tzEtacBinning)-1, tzEtacBinning)

        fo.write( str(len(tzJpsiBinning)-1) )
        fo.write( "\n" )
        fo_sign.write( "PTBin " + str(nPT) + "\n" )
        fo_sign.write( str(len(tzJpsiBinning)-1) + "\n" )
        for iTz in range(len(tzJpsiBinning)-1):

            nameWksp = dirName + "Etac/Wksp_MassFit_PT%s_Tz%d_C%d.root"%(nPT,iTz+1,nConf)
            #nameWksp = dirName + "Jpsi/Wksp_MassFit_PT%s_Tz%d_C%d.root"%(nPT,iTz+1,nConf)
            f1 = TFile(nameWksp,"READ")
            w = f1.Get("w") 
            
            #fo.write("%s %s   %6.2f    %6.2f\n"%( (tzJpsiBinning[iTz+1]+tzJpsiBinning[iTz])/2., (tzJpsiBinning[iTz+1]-tzJpsiBinning[iTz])/2., w.var("nJpsi").getValV(),w.var("nJpsi").getError()))
            fo.write("%s %s   %6.2f    %6.2f\n"%( (tzJpsiBinning[iTz+1]+tzJpsiBinning[iTz])/2., (tzJpsiBinning[iTz+1]-tzJpsiBinning[iTz])/2., w.var("nJpsi").getValV(), (w.var("nJpsi").getErrorHi()-w.var("nJpsi").getErrorLo())/2.))
                     
            fo_sign.write("%6.2f   %6.2f  %6.2f   %6.2f  %3.3f \n"%( w.var("nJpsi").getValV(), w.var("nJpsi").getError(), w.var("nJpsi").getErrorLo(), w.var("nJpsi").getErrorHi(), w.var("nJpsi").getValV()/w.var("nJpsi").getError()))
            hhJpsi.SetBinContent(iTz+1, w.var("nJpsi").getValV())
            hhJpsi.SetBinError(iTz+1, w.var("nJpsi").getError())

            f1.Close()
            
        fo.write( str(len(tzEtacBinning)-1) )
        fo.write( "\n" )
        fo_sign.write( "PTBin " + str(nPT) + "\n" )
        fo_sign.write( str(len(tzEtacBinning)-1) + "\n" )
        for iTz in range(len(tzEtacBinning)-1):

            nameWksp = dirName + "Etac/Wksp_MassFit_PT%s_Tz%d_C%d.root"%(nPT,iTz+1,nConf)
            f1 = TFile(nameWksp,"READ")
            w = f1.Get("w") 
            
            #fo.write("%s %s   %6.2f    %6.2f\n"%( (tzEtacBinning[iTz]+tzEtacBinning[iTz+1])/2., (tzEtacBinning[iTz+1]-tzEtacBinning[iTz])/2., w.var("nEta").getValV(),w.var("nEta").getError()))
            fo.write("%s %s   %6.2f    %6.2f\n"%( (tzEtacBinning[iTz]+tzEtacBinning[iTz+1])/2., (tzEtacBinning[iTz+1]-tzEtacBinning[iTz])/2., w.var("nEta").getValV(), (w.var("nEta").getErrorHi() - w.var("nEta").getErrorLo())/2.))
            fo_sign.write("%6.2f   %6.2f   %6.2f   %6.2f   %3.3f \n"%(  w.var("nEta").getValV(),w.var("nEta").getError(), w.var("nEta").getErrorLo(), w.var("nEta").getErrorHi(), w.var("nEta").getValV()/w.var("nEta").getError() ))
            hhEtac.SetBinContent(iTz+1, w.var("nEta").getValV())
            hhEtac.SetBinError(iTz+1, w.var("nEta").getError())

            f1.Close()

        fo_sign.write("\n")
        fo.close()
        fo_sign.close()    

    canv_1 = TCanvas("canv_1","J/Psi_Mass",55,55,800,800)
    canv_1.Divide(1,2)
    rangeL = -10.0
    rangeR = 10.0

    canv_1.cd(1)
    canv_1.cd(1).SetLogy()

    hhJpsi.Draw("AP")
    hhJpsi.SetTitle("J/#psi")
    hhJpsi.SetLineWidth(2)
    hhJpsi.SetLineColor(4)
    hhJpsi.SetMinimum(1.0)
    hhJpsi.SetMaximum(7.e5)
    hhJpsi.GetXaxis().SetTitle("t_{z} [ps]")
    hhJpsi.GetYaxis().SetTitle("Entries per ps")
    hhJpsi.GetXaxis().SetRangeUser(rangeL,rangeR)
    
    canv_1.cd(2)


    canv_1.cd(2).SetLogy()

    hhEtac.Draw("AP")
    hhEtac.SetTitle("#eta_{c}")
    hhEtac.SetLineWidth(2)
    hhEtac.SetLineColor(4)
    hhEtac.SetMinimum(1.0)
    hhEtac.SetMaximum(7.e5)
    hhEtac.GetXaxis().SetRangeUser(rangeL,rangeR)
    hhEtac.GetXaxis().SetTitle("t_{z} [ps]")
    hhEtac.GetYaxis().SetTitle("Entries per ps")


def yieldSim(nPTBins=[0], nConf=0, keyPrompt=True):

    yJpsi = array("d", 15*[0])
    yEtac = array("d", 15*[0])
    
    yJpsiErr = array("d", 15*[0])
    yEtacErr = array("d", 15*[0])

    #tzTotalBinning =  array("d",[-10., -1., -0.025, 0.0, 0.025, 0.2, 1.0, 1.5, 2.0, 3.0, 4.0, 10])
    tzTotalBinning =  array("d",[-10., -0.025, 0.0, 0.025, 0.2, 1.0, 1.5, 2.0, 3.0, 4.0, 10])
    #tzEtacBinning  =  array("d",[-10., -0.025, 0.0, 0.025, 0.2, 1.0, 1.5, 2.0, 3.0, 4.0, 10])
    #tzJpsiBinning =  array("d",[-10., -0.025, 0.0, 0.025, 0.2, 1.0, 1.5, 2.0, 3.0, 4.0, 10])
    tzEtacBinning = array("d", [-10., -0.125, -0.025, 0.,    0.200,  2., 4., 10.])
    tzJpsiBinning = array("d", [-10., -0.125, -0.025, 0.,    0.200,  2., 4., 10.])
    #tzJpsiBinning = array("d", [-10., -0.025, 0., 0.025, 0.1, 1., 4., 10.])

    nBinsEtac = len(tzEtacBinning)
    nBinsJpsi = len(tzJpsiBinning)
        
    if keyPrompt: inCutKey = "prompt"
    else:         inCutKey = "fromB"
    
    binPT = "Sim/"
    
    dirName = homeDir+"Results/MassFit/" + inCutKey + "/" + binPT + "m_scaled/"       
    
    
    w = RooWorkspace("w", True)
    
    hhJpsi = TH1D()
    hhEtac = TH1D()
    hhJpsi.SetTitle("yield")
    hhEtac.SetTitle("yield")

    
    #if nPT == 0:
        #fo = open(homeDir+"signal/signal_%s_minos.txt"%nConf, "w")
        #fo_sign = open(homeDir+"signal/signif/signif_Mass_%s_minos.txt"%nConf, "w")

        #hhJpsi.SetBins(len(tzTotalBinning)-1, tzTotalBinning)
        #hhEtac.SetBins(len(tzTotalBinning)-1, tzTotalBinning)

        #fo.write( str(len(tzTotalBinning)-1) )
        #fo.write( "\n" )
        #fo_sign.write( str(len(tzTotalBinning)-1) )
        #fo_sign.write( "\n" )
        #for iTz in range(len(tzTotalBinning)-1):

            #nameWksp = dirName + "Wksp_MassFit_PT%s_Tz%d_C%d.root"%(nPT,iTz+1,nConf)
            #f1 = TFile(nameWksp,"READ")
            #w = f1.Get("w") 
            
            #fo.write("%s %s   %6.2f    %6.2f \n"%( (tzTotalBinning[iTz]+tzTotalBinning[iTz+1])/2., (tzTotalBinning[iTz+1]-tzTotalBinning[iTz])/2., w.var("nJpsi").getValV(),w.var("nJpsi").getError()))
            #fo_sign.write("%6.2f   %6.2f  %3.3f \n"%( w.var("nJpsi").getValV(), w.var("nJpsi").getError(), w.var("nJpsi").getValV()/w.var("nJpsi").getError()))
            #hhJpsi.SetBinContent(iTz+1, w.var("nJpsi").getValV())
            #hhJpsi.SetBinError(iTz+1, w.var("nJpsi").getError())
            ##print ((tzTotalBinning[iTz]+tzTotalBinning[iTz+1])/2.0, (tzTotalBinning[iTz+1]-tzTotalBinning[iTz])/2.0)
            #f1.Close()

        #fo.write( str(len(tzTotalBinning)-1) )
        #fo.write( "\n" )
        #fo_sign.write( str(len(tzTotalBinning)-1) )
        #fo_sign.write( "\n" )
        #for iTz in range(len(tzTotalBinning)-1):

            #nameWksp = dirName + "Wksp_MassFit_PT%s_Tz%d_C%d.root"%(nPT,iTz+1,nConf)
            #f1 = TFile(nameWksp,"READ")
            #w = f1.Get("w") 
            
            #fo.write("%s %s   %6.2f    %6.2f\n"%( (tzTotalBinning[iTz]+tzTotalBinning[iTz+1])/2., (tzTotalBinning[iTz+1]- tzTotalBinning[iTz])/2., w.var("nEta").getValV(),w.var("nEta").getError()))
            #fo_sign.write("%6.2f   %6.2f   %3.3f \n"%(  w.var("nEta").getValV(),w.var("nEta").getError(), w.var("nEta").getValV()/w.var("nEta").getError() ))
            #hhEtac.SetBinContent(iTz+1, w.var("nEta").getValV())
            #hhEtac.SetBinError(iTz+1, w.var("nEta").getError())
            ##print ((tzTotalBinning[iTz]+tzTotalBinning[iTz+1])/2.0, (tzTotalBinning[iTz+1]-tzTotalBinning[iTz])/2.0)

            #f1.Close()

        #fo.close()
        #fo_sign.close()    
    if len(nPTBins)==1 and nPTBins[0]==0:
        nPT = 0
        fo = open(homeDir+"signal/signal_%s_scaled_10Bins.txt"%(nConf), "w")
        fo_sign = open(homeDir+"signal/signif/signif_Mass_%s_scaled_10Bins.txt"%nConf, "w")

        hhJpsi.SetBins(len(tzEtacBinning)-1, tzJpsiBinning)
        hhEtac.SetBins(len(tzEtacBinning)-1, tzEtacBinning)

        fo.write( str(nBinsJpsi-1) )
        fo.write( "\n" )
        fo_sign.write( str(nBinsJpsi-1) + "\n" )
        for nTz in range(1, nBinsJpsi):

            nameWksp = dirName + "Etac_Wksp_MassFit_C%d_total_test_10Bins.root"%(nConf)
            #nameWksp = dirName + "Jpsi_Wksp_MassFit_C%d.root"%(nConf)
            f1 = TFile(nameWksp,"READ")
            w = f1.Get("w") 
            val = w.var("nJpsi_PT0_Tz%s"%(nTz)).getValV()
            errHi = w.var("nJpsi_PT0_Tz%s"%(nTz)).getErrorHi()
            errLow = w.var("nJpsi_PT0_Tz%s"%(nTz)).getErrorLo()
            if errHi < 1e-3:
                err = -errLow
            elif errLow > -1e-3:
                err = errHi
            else:
                err = (errHi-errLow)/2.
            fo.write("%s %s   %6.2f    %6.2f\n"%( (tzJpsiBinning[nTz]+tzJpsiBinning[nTz-1])/2., (tzJpsiBinning[nTz]-tzJpsiBinning[nTz-1])/2., val, err))                     
            fo_sign.write("%6.2f   %6.2f  %6.2f   %6.2f  %3.3f \n"%( w.var("nJpsi_PT0_Tz%s"%(nTz)).getValV(), w.var("nJpsi_PT0_Tz%s"%(nTz)).getError(), w.var("nJpsi_PT0_Tz%s"%(nTz)).getErrorLo(), w.var("nJpsi_PT0_Tz%s"%(nTz)).getErrorHi(), val/err))
            hhJpsi.SetBinContent(nTz, w.var("nJpsi_PT0_Tz%s"%(nTz)).getValV())
            hhJpsi.SetBinError(nTz, w.var("nJpsi_PT0_Tz%s"%(nTz)).getError())

            f1.Close()
            
        fo.write( str(nBinsEtac-1) )
        fo.write( "\n" )
        fo_sign.write( "PTBin " + str(nPT) + "\n" )
        fo_sign.write( str(nBinsEtac-1) + "\n" )
        for nTz in range(1, nBinsEtac):

            nameWksp = dirName + "Etac_Wksp_MassFit_C%d_total_test_10Bins.root"%(nConf)
            f1 = TFile(nameWksp,"READ")
            w = f1.Get("w") 

            val = w.var("nEta_PT0_Tz%s"%(nTz)).getValV()
            errHi = w.var("nEta_PT0_Tz%s"%(nTz)).getErrorHi()
            errLow = w.var("nEta_PT0_Tz%s"%(nTz)).getErrorLo()
            if errHi < 1e-3:
                err = -errLow
            elif errLow > -1e-3:
                err = errHi
            else:
                err = (errHi-errLow)/2.
            
            fo.write("%s %s   %6.2f    %6.2f\n"%( (tzEtacBinning[nTz-1]+tzEtacBinning[nTz])/2., (tzEtacBinning[nTz]-tzEtacBinning[nTz-1])/2., val, err))
            fo_sign.write("%6.2f   %6.2f   %6.2f   %6.2f   %3.3f \n"%(  w.var("nEta_PT0_Tz%s"%(nTz)).getValV(),w.var("nEta_PT0_Tz%s"%(nTz)).getError(), w.var("nEta_PT0_Tz%s"%(nTz)).getErrorLo(), w.var("nEta_PT0_Tz%s"%(nTz)).getErrorHi(), val/err))
            hhEtac.SetBinContent(nTz, w.var("nEta_PT0_Tz%s"%(nTz)).getValV())
            hhEtac.SetBinError(nTz, w.var("nEta_PT0_Tz%s"%(nTz)).getError())

            f1.Close()
    else:
        for nPT in nPTBins:
            fo = open(homeDir+"signal/signal_PT%s_%s_newMC.txt"%(nPT,nConf), "w")
            if nPT == 1:    fo_sign = open(homeDir+"signal/signif/signif_Mass_PTBins_%s_newMC.txt"%nConf, "w")
            else:           fo_sign = open(homeDir+"signal/signif/signif_Mass_PTBins_%s_newMC.txt"%nConf, "a")

            hhJpsi.SetBins(len(tzEtacBinning)-1, tzJpsiBinning)
            hhEtac.SetBins(len(tzEtacBinning)-1, tzEtacBinning)

            fo.write( str(nBinsJpsi-1) )
            fo.write( "\n" )
            fo_sign.write( "PTBin " + str(nPT) + "\n" )
            fo_sign.write( str(nBinsJpsi-1) + "\n" )
            for nTz in range(1, nBinsJpsi):

                if nConf==0:
                    nameWksp = dirName + "Etac_Wksp_MassFit_C%d_newMC.root"%(nConf)
                else:
                    nameWksp = dirName + "Etac_Wksp_MassFit_C%d_newMC.root"%(nConf)
                #nameWksp = dirName + "Jpsi_Wksp_MassFit_C%d.root"%(nConf)
                f1 = TFile(nameWksp,"READ")
                w = f1.Get("w") 
                
                val = w.var("nJpsi_PT%s_Tz%s"%(nPT,nTz)).getValV()
                errHi = w.var("nJpsi_PT%s_Tz%s"%(nPT,nTz)).getErrorHi()
                errLow = w.var("nJpsi_PT%s_Tz%s"%(nPT,nTz)).getErrorLo()
                if errHi < 1e-3:
                    err = -errLow
                elif errLow > -1e-3:
                    err = errHi
                else:
                    err = (errHi-errLow)/2.
                fo.write("%s %s   %6.2f    %6.2f\n"%( (tzJpsiBinning[nTz]+tzJpsiBinning[nTz-1])/2., (tzJpsiBinning[nTz]-tzJpsiBinning[nTz-1])/2., val, err))                     
                fo_sign.write("%6.2f   %6.2f  %6.2f   %6.2f  %3.3f \n"%( w.var("nJpsi_PT%s_Tz%s"%(nPT,nTz)).getValV(), w.var("nJpsi_PT%s_Tz%s"%(nPT,nTz)).getError(), w.var("nJpsi_PT%s_Tz%s"%(nPT,nTz)).getErrorLo(), w.var("nJpsi_PT%s_Tz%s"%(nPT,nTz)).getErrorHi(), w.var("nJpsi_PT%s_Tz%s"%(nPT,nTz)).getValV()/w.var("nJpsi_PT%s_Tz%s"%(nPT,nTz)).getError()))
                hhJpsi.SetBinContent(nTz, w.var("nJpsi_PT%s_Tz%s"%(nPT,nTz)).getValV())
                hhJpsi.SetBinError(nTz, w.var("nJpsi_PT%s_Tz%s"%(nPT,nTz)).getError())

                f1.Close()
                
            fo.write( str(nBinsEtac-1) )
            fo.write( "\n" )
            fo_sign.write( "PTBin " + str(nPT) + "\n" )
            fo_sign.write( str(nBinsEtac-1) + "\n" )
            for nTz in range(1, nBinsEtac):

                if nConf==0:
                    nameWksp = dirName + "Etac_Wksp_MassFit_C%d_newMC.root"%(nConf)
                else:
                    nameWksp = dirName + "Etac_Wksp_MassFit_C%d_newMC.root"%(nConf)
                f1 = TFile(nameWksp,"READ")
                w = f1.Get("w") 

                val = w.var("nEta_PT%s_Tz%s"%(nPT,nTz)).getValV()
                errHi = w.var("nEta_PT%s_Tz%s"%(nPT,nTz)).getErrorHi()
                errLow = w.var("nEta_PT%s_Tz%s"%(nPT,nTz)).getErrorLo()
                if errHi < 1e-3:
                    err = -errLow
                elif errLow > -1e-3:
                    err = errHi
                else:
                    err = (errHi-errLow)/2.
                
                fo.write("%s %s   %6.2f    %6.2f\n"%( (tzEtacBinning[nTz-1]+tzEtacBinning[nTz])/2., (tzEtacBinning[nTz]-tzEtacBinning[nTz-1])/2., val, err))
                fo_sign.write("%6.2f   %6.2f   %6.2f   %6.2f   %3.3f \n"%(  w.var("nEta_PT%s_Tz%s"%(nPT,nTz)).getValV(),w.var("nEta_PT%s_Tz%s"%(nPT,nTz)).getError(), w.var("nEta_PT%s_Tz%s"%(nPT,nTz)).getErrorLo(), w.var("nEta_PT%s_Tz%s"%(nPT,nTz)).getErrorHi(), w.var("nEta_PT%s_Tz%s"%(nPT,nTz)).getValV()/w.var("nEta_PT%s_Tz%s"%(nPT,nTz)).getError() ))
                hhEtac.SetBinContent(nTz, w.var("nEta_PT%s_Tz%s"%(nPT,nTz)).getValV())
                hhEtac.SetBinError(nTz, w.var("nEta_PT%s_Tz%s"%(nPT,nTz)).getError())

                f1.Close()

        fo_sign.write("\n")
        fo.close()
        fo_sign.close()    

 
systDictRunI = {
    1 : "$\\sigma_{\\eta_{c}}$",
    2:  "$\\sigma_{\\eta_{c}}/\\sigma_{J/\\psi}$",
    3:  "Crystal ball",
    4:  "Alt. bkg",
    5:  "Alt. bkg 2",
    6:  "$\\Gamma_{\\eta_{c}}$",
    7:  "$\\epsilon_{p\\bar{p}\\pi^{0}}$",
    8:  "$\\epsilon_{P,S\\rightarrow S,P}$",

}
 
def systDiffRunI(nConfs=[0], nPTBins=[0]):
    
    #nPTBins=4
    nTzBins=5
    
    fout = open("RunIFit.txt","w")

    keys = ["Prompt","FromB"]
    
    for iPT in nPTBins:

        f0 = TFile(homeDir+"Results/MassFit/RunIMethod/Wksp_MassFit_PT%s_C0.root"%(iPT),"READ")
        w0 = f0.Get("w")
        f0.Close()

        #fout.write("PT bin "+str(iPT)+"\n \n")
        for key in keys:

            fout.write("\\begin{frame} \n")
            fout.write("\\frametitle{PT Bin "+str(iPT)+" "+key+"} \n")
            fout.write("  \\begin{table}[H] \n")
            fout.write("  \t \\begin{tabular*}{0.75\\linewidth}{@{\\extracolsep{\\fill}}c|c|c|c} \n")
            fout.write("  & $n_{J/\\psi}$ &  $n_{\\eta_c}$ & $N_{\\eta_c}/N_{J/\\psi}$   \\\\ \\hline \n")

            #fout.write("Tz bin "+str(iTz)+"\n")

            valJpsi = w0.var("nJpsi_%s"%(key)).getValV()
            errHiJpsi = w0.var("nJpsi_%s"%(key)).getErrorHi()
            errLowJpsi = w0.var("nJpsi_%s"%(key)).getErrorLo()
            errJpsiSyst = 0
            if errHiJpsi < 1e-3:
                errJpsi = -errLowJpsi
            elif errLowJpsi > -1e-3:
                errJpsi = errHiJpsi
            else:
                errJpsi = (errHiJpsi-errLowJpsi)/2.
            
            valEtac = w0.var("nEta_%s"%(key)).getValV()
            errHiEtac = w0.var("nEta_%s"%(key)).getErrorHi()
            errLowEtac = w0.var("nEta_%s"%(key)).getErrorLo()
            errEtacSyst = 0
            if errHiEtac < 1e-3:
                errEtac = -errLowEtac
            elif errLowEtac > -1e-3:
                errEtac = errHiEtac
            else:
                errEtac = (errHiEtac-errLowEtac)/2.

            valEtacRel = w0.var("NEtac_%s"%(key)).getValV()
            errEtacRel = w0.var("NEtac_%s"%(key)).getError()
            errEtacRelSyst = 0
            #fout.write( "%5.1f +/- %5.1f   "%(w0.var("nJpsi_PT%s_Tz%s"%(iPT,iTz)).getValV(),w0.var("nJpsi_PT%s_Tz%s"%(iPT,iTz)).getError()))
            #fout.write( "%5.1f +/- %5.1f \n"%(w0.var("nEta_PT%s_Tz%s"%(iPT,iTz)).getValV(),w0.var("nEta_PT%s_Tz%s"%(iPT,iTz)).getError()))
            fout.write( "& %5.1f & %5.1f & %1.3f\\\\ \\hline \n  "%(w0.var("nJpsi_%s"%(key)).getValV(),w0.var("nEta_%s"%(key)).getValV(), valEtacRel))
            fout.write( "& %2.3f & %2.3f & %1.3f \\\\ \\hline \n"%(100*errJpsi/valJpsi,100*errEtac/valEtac, 100*errEtacRel/valEtacRel))
 
            for nConf in nConfs:
        
                f = TFile(homeDir+"Results/MassFit/RunIMethod/Wksp_MassFit_PT{}_C{}.root".format(iPT,nConf),"READ")
                w = f.Get("w")
                f.Close()
                #fout.write("Config "+str(nConf)+"\n")
                diffJpsi = w.var("nJpsi_%s"%(key)).getValV()-w0.var("nJpsi_%s"%(key)).getValV()
                diffEtac = w.var("nEta_%s"%(key)).getValV()-w0.var("nEta_%s"%(key)).getValV()
                diffEtacRel = w.var("NEtac_%s"%(key)).getValV()-w0.var("NEtac_%s"%(key)).getValV()
                fout.write( "%s &  %2.2f  "%(systDictRunI[nConf],100*diffJpsi/valJpsi))
                fout.write( "& %2.2f "%( 100*diffEtac/valEtac))
                fout.write( "& %2.2f \\\\ \n"%( 100*diffEtacRel/valEtacRel))
                errJpsiSyst += diffJpsi**2
                errEtacSyst += diffEtac**2
                errEtacRelSyst += diffEtacRel**2

            diffJpsi = 0
            diffEtac = 0
            diffEtacRel = 0
            nConfsEff = [8, 9, 10, 11]
            for nConf in nConfsEff:
        
                f = TFile(homeDir+"Results/MassFit/RunIMethod/Wksp_MassFit_PT{}_C{}.root".format(iPT,nConf),"READ")
                w = f.Get("w")
                f.Close()
                #fout.write("Config "+str(nConf)+"\n")
                diffJpsi += (w.var("nJpsi_%s"%(key)).getValV()-w0.var("nJpsi_%s"%(key)).getValV())**2
                diffEtac += (w.var("nEta_%s"%(key)).getValV()-w0.var("nEta_%s"%(key)).getValV())**2
                diffEtacRel += (w.var("NEtac_%s"%(key)).getValV()-w0.var("NEtac_%s"%(key)).getValV())**2

            diffJpsi = (diffJpsi/4)**0.5
            diffEtac = (diffEtac/4)**0.5
            diffEtacRel = (diffEtacRel/4)**0.5

            fout.write( "%s &  %2.2f  "%(systDictRunI[8],100*diffJpsi/valJpsi))
            fout.write( "& %2.2f "%( 100*diffEtac/valEtac))
            fout.write( "& %2.2f \\\\ \n"%( 100*diffEtacRel/valEtacRel))

            errJpsiSyst += diffJpsi**2
            errEtacSyst += diffEtac**2
            errEtacRelSyst += diffEtacRel**2


            errJpsiSyst = errJpsiSyst**0.5
            errEtacSyst = errEtacSyst**0.5
            errEtacRelSyst = errEtacRelSyst**0.5
            
            
            fout.write( "\\hline  \n" )
            fout.write( "Tot Syst & %2.1f & %2.1f & %2.1f \\\\ \\hline \n"%(100*errJpsiSyst/valJpsi,100*errEtacSyst/valEtac, 100*errEtacRelSyst/valEtacRel))
 
            fout.write(" \t \\end{tabular*} \n")
            fout.write("  \\end{table} \n")
            fout.write("\\end{frame} \n")
            #fout.close()
            #fout.write("\n")        
        #fout.write("\n")        
        
    fout.close()
 
systDict = {
    1 : "$\\sigma_{\\eta_{c}}$",
    2:  "$\\sigma_{\\eta_{c}}/\\sigma_{J/\\psi}$",
    3:  "Crystal ball",
    4:  "Alt. bkg",
    5:  "Alt. bkg 2",
    6:  "$\\Gamma_{\\eta_{c}}$",
    7:  "mass renorm",
    8:  "$\\epsilon_{p\\bar{p}\\pi^{0}}$",
    9:  "mass const"

}
 
def systDiff(nConfs=[0]):
    
    nPTBins=4
    nTzBins=5
    
    f0 = TFile(homeDir+"Results/MassFit/prompt/Sim/Etac_Wksp_MassFit_C0.root","READ")
    w0 = f0.Get("w")
    f0.Close()

    fout = open("simFit.txt","w")

    for iPT in range(1, nPTBins+1):
        #fout.write("PT bin "+str(iPT)+"\n \n")
        for iTz in range(1, nTzBins+1):

            fout.write("\\begin{frame} \n")
            fout.write("\\frametitle{PT Bin "+str(iPT)+" Tz Bin "+str(iTz)+"} \n")
            fout.write("  \\begin{table}[H] \n")
            fout.write("  \t \\begin{tabular*}{0.75\\linewidth}{@{\\extracolsep{\\fill}}c|c|c} \n")
            fout.write("  & $N_{J/\\psi}$ &  $N_{\\eta_c}$   \\\\ \\hline \n")

            #fout.write("Tz bin "+str(iTz)+"\n")

            valJpsi = w0.var("nJpsi_PT%s_Tz%s"%(iPT,iTz)).getValV()
            errHiJpsi = w0.var("nJpsi_PT%s_Tz%s"%(iPT,iTz)).getErrorHi()
            errLowJpsi = w0.var("nJpsi_PT%s_Tz%s"%(iPT,iTz)).getErrorLo()
            if errHiJpsi < 1e-3:
                errJpsi = -errLowJpsi
            elif errLowJpsi > -1e-3:
                errJpsi = errHiJpsi
            else:
                errJpsi = (errHiJpsi-errLowJpsi)/2.
            
            valEtac = w0.var("nEta_PT%s_Tz%s"%(iPT,iTz)).getValV()
            errHiEtac = w0.var("nEta_PT%s_Tz%s"%(iPT,iTz)).getErrorHi()
            errLowEtac = w0.var("nEta_PT%s_Tz%s"%(iPT,iTz)).getErrorLo()
            if errHiEtac < 1e-3:
                errEtac = -errLowEtac
            elif errLowEtac > -1e-3:
                errEtac = errHiEtac
            else:
                errEtac = (errHiEtac-errLowEtac)/2.
            #fout.write( "%5.1f +/- %5.1f   "%(w0.var("nJpsi_PT%s_Tz%s"%(iPT,iTz)).getValV(),w0.var("nJpsi_PT%s_Tz%s"%(iPT,iTz)).getError()))
            #fout.write( "%5.1f +/- %5.1f \n"%(w0.var("nEta_PT%s_Tz%s"%(iPT,iTz)).getValV(),w0.var("nEta_PT%s_Tz%s"%(iPT,iTz)).getError()))
            fout.write( "& %5.1f & %5.1f \\\\ \\hline \n  "%(w0.var("nJpsi_PT%s_Tz%s"%(iPT,iTz)).getValV(),w0.var("nEta_PT%s_Tz%s"%(iPT,iTz)).getValV()))
            fout.write( "& %2.3f & %2.3f \\\\ \\hline \n"%(100*errJpsi/valJpsi,100*errEtac/valEtac))
 
            for nConf in nConfs:
        
                f = TFile(homeDir+"Results/MassFit/prompt/Sim/Etac_Wksp_MassFit_C{}.root".format(nConf),"READ")
                w = f.Get("w")
                f.Close()
                #fout.write("Config "+str(nConf)+"\n")
                diffJpsi = w.var("nJpsi_PT%s_Tz%s"%(iPT,iTz)).getValV()-w0.var("nJpsi_PT%s_Tz%s"%(iPT,iTz)).getValV()
                diffEtac = w.var("nEta_PT%s_Tz%s"%(iPT,iTz)).getValV()-w0.var("nEta_PT%s_Tz%s"%(iPT,iTz)).getValV()
                fout.write( "%s &  %2.2f  & "%(systDict[nConf],100*diffJpsi/valJpsi))
                fout.write( "%2.2f \\\\ \n"%( 100*diffEtac/valEtac))

            fout.write(" \t \\end{tabular*} \n")
            fout.write("  \\end{table} \n")
            fout.write("\\end{frame} \n")
            #fout.close()
            #fout.write("\n")        
        #fout.write("\n")        
        
    fout.close()

#yieldCharm(0, 0, True)
#for iPT in range(1, 6):
    #yieldCharm(iPT, 0, True)

#nConfs = [ 1, 2, 3, 4, 6, 8, 9]
#nConfs = [1, 3, 4, 6, 8]
nConf = [0]
#nPTBins = [0]
nPTBins = [1, 2, 3, 4]
yieldSim(nPTBins, 0, True)
#for iC in nConfs:
    #yieldSim(nPTBins, iC, True)

#systDiff(nConfs)

#nConfs = [ 2, 3, 4, 6, 7]
#systDiffRunI(nConfs,nPTBins)

