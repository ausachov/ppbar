
from ROOT import *
from DrawMass import *
gROOT.LoadMacro("../libRooRelBreitWigner/RooRelBreitWigner.cxx+")
gROOT.LoadMacro("../BifurcatedCB/BifurcatedCB.cxx+")
#from setupFitModel import *


histosDir = "Histos/"
homeDir = "/users/LHCb/zhovkovska/scripts/"

binningDict = {
    "Jpsi_PT":                      [6500, 8000, 10000, 12000, 14000, 18000],
    "Jpsi_ETA":                     [2.0, 2.45, 2.8, 3.15, 4.5],
    "TMath::Abs(ProtonM_CosTheta)": [0.0, 0.15, 0.3, 0.50, 1.0],
    "nSPDHits":                     [0, 100, 150, 200, 250, 300],
    "Jpsi_Tz":                      [-10., -0.15, -0.025, 0.0, 0.025, 0.2, 1.0, 1.5, 2.0, 3.0, 4.0, 10], #[-10., -0.15, -0.025, 0.0, 0.20, 2.0, 10.0],
    "Jpsi_Tz_Alt":                  [-10.0, -0.15, -0.05, 0.,        0.100,   4., 10.],
    "Jpsi_Tz_old":                  [-10., -0.025, 0.0, 0.20, 2.0, 10.0],
    "Jpsi_Tz_Tot":                  [-10., -0.025, 0.0, 0.025, 0.2, 1.0, 1.5, 2.0, 3.0, 4.0, 10],
}


minM = 2850
maxM = 3250
nBins = 1000


def getData_h(w, keyPrompt=True, nPT=0, nTz=0, charm='Etac', shift=False):
       
    
    if keyPrompt: inCutKey = "all_l0TOS"
    else:         inCutKey = "secondary_l0TOS"
    
    if nPT!=0:    
        if nTz!=0: binPT = "Jpsi_PT/"+charm+"/bin%s_"%(nPT)
        else:      binPT = "Jpsi_PT/bin%s_"%(nPT) 
        binTz = "Tz%s.root"%(nTz)
    else:        
        binPT = "Total/"
        binTz = "Tz%s_7Bins.root"%(nTz)

    #binTz = "Tz%s.root"%(nTz)

    
    dirName_2015 = homeDir+"Histos/m_scaled/2015/DiProton/" + inCutKey + "/"
    dirName_2016 = homeDir+"Histos/m_scaled/2016/DiProton/" + inCutKey + "/"
        
    nameFile = dirName_2015 + binPT + binTz
    f_2015 = TFile(nameFile,"read")
    print nameFile
    nameFile = dirName_2016 + binPT + binTz
    f_2016 = TFile(nameFile,"read")
    print nameFile
    
    
    hh = TH1D("hh","hh", nBins, minM, maxM)
    hh = f_2015.Get("Jpsi_M")
    hh.Add(f_2016.Get("Jpsi_M"))
    
    Jpsi_M = w.var("Jpsi_M")
    #Jpsi_M = RooRealVar("Jpsi_M","Jpsi_M",2850,3250) 

    if(shift):
        hh.SetBins(nBins, minM+(maxM-minM)/(2.*float(nBins)),maxM+(maxM-minM)/(2.*float(nBins)))
        Jpsi_M.setRange(minM+(maxM-minM)/(2.*float(nBins)), maxM+(maxM-minM)/(2.*float(nBins))) 

    dh = RooDataHist("dh_PT%s_Tz%s"%(nPT,nTz),"dh_PT%s_Tz%s"%(nPT,nTz), RooArgList(Jpsi_M) ,hh)
    getattr(w,'import')(dh)
    f_2015.Close()
    f_2016.Close()
    
    #print "DATA\'S READ SUCCESSFULLY"




def getConstPars(nConf, nPT=0, nTz=0):
    
    if nConf==3: add="_CB"
    else:        add=""
    
    f = TFile(homeDir+"Results/MC/MassFit/MC_MassResolution_2016_wksp.root","READ") 
    wMC = f.Get("w")
    f.Close()
    
    rNtoW = wMC.var("rNarToW").getValV()
    rEtaToJpsi = wMC.var("rEtaToJpsi").getValV()
    rArea = wMC.var("rG1toG2").getValV()
    sigma = wMC.var("sigma_eta_1").getValV()
    bkgOpt = 0
    gamma = 31.8
    #effic = 0.035
    #gamma = 21.3
    effic = 0.057*1.19/2.12
    #ratioNtoW = 0.5
    #ratioEtaToJpsi = 0.88
    #ratioArea = 0.9
    
    if nPT !=0 : 
        f = TFile(homeDir+"Results/MC/MassFit/parFit%s_GeV.root"%(add),"READ")
        fSigma = f.Get("fSigmaMC")
        f.Close()
        ptL = binningDict["Jpsi_PT"][nPT-1]
        ptR = binningDict["Jpsi_PT"][nPT]
        rScSigma = fSigma.Eval((ptR+ptL)/2000.)/sigma
    else:
        rScSigma = 1
    
    #f = TFile("/users/LHCb/zhovkovska/ppbar/param_fits/errMassTzDist_shiftFix_MC.root","READ")
    f = TFile("/users/LHCb/zhovkovska/ppbar/param_fits/errMassTzDist_s_m_scaled_7Bins.root","READ")
    hJpsi = f.Get("h_mass_jpsi")
    #hEtac = f.Get("h_mass_etac")
    hSigma = f.Get("h_sigma_mass_MC")
    #rScEtac = (1+hEtac.GetBinContent(nTz))
    rScJpsi = (1+hJpsi.GetBinContent(nTz))    #!!!!!!!!!!!!!!!
    rScSigma *= hSigma.GetBinContent(nTz)     #!!!!!!!!!!!!!!!
    f.Close()
    
    
    if (nConf == 1):

        if nPT !=0 : 
            f = TFile(homeDir+"Results/MC/MassFit/parFit%s_GeV.root"%(add),"READ")
            fSigma = f.Get("fSigmaMC")
            f.Close()
            ptL = binningDict["Jpsi_PT"][nPT-1]
            ptR = binningDict["Jpsi_PT"][nPT]
            rScSigma = fSigma.Eval((ptR+ptL)/2000.)/sigma
        else:
            rScSigma = 1
        f = TFile("/users/LHCb/zhovkovska/ppbar/param_fits/errMassTzDist_shiftFix_MC.root","READ")
        fSigmaTz = f.Get("fSigmaTz")
        tzL = binningDict["Jpsi_Tz"][nTz-1]
        tzR = binningDict["Jpsi_Tz"][nTz]
        rScSigma *= fSigmaTz.Eval((tzL+tzR)/2.)
        f.Close()
        print ("SETUP 1")
    elif (nConf == 2):
        f = TFile(homeDir+"Results/MC/MassFit/parFit_GeV.root","READ") 
        func = f.Get("fRel")
        f.Close()
        ptL = binningDict["Jpsi_PT"][nPT-1]
        ptR = binningDict["Jpsi_PT"][nPT]
        rEtaToJpsi = func.Eval((ptR+ptL)/2000.)
        #rEtaToJpsi = wMC.var("rEtaToJpsi").getValV()+wMC.var("rEtaToJpsi").getError()
        print ("SETUP 2")      
    elif (nConf == 3):
        rNtoW = 1.0
        rArea = 1.0
        print ("SETUP 3")
    elif (nConf == 4):
        bkgOpt = 1
        print ("SETUP 4")
    elif (nConf == 5):
        bkgOpt = 2
        print ("SETUP 5")
    elif (nConf == 6):
        gamma = 34.0
        print ("SETUP 6")
    elif (nConf == 7):
        f = TFile(homeDir+"Results/MassFit/prompt/Sim/Etac_Wksp_MassFit_PT0_C0_total.root","READ") 
        w = f.Get("w")
        f.Close()
        rScJpsi = w.var("rNormJpsi_Tz%s"%(nTz)).getValV()
        print ("SETUP 7")
    elif (nConf == 8):
        #effic = 0.032
        effic = 0.057*1.19/2.12*(1+0.09)
        print ("SETUP 8")
    elif (nConf == 9):
        #effic = 0.032
        rScJpsi = 1
        print ("SETUP 9")
    elif (nConf == 10):
        #gamma = 19.1
        gamma = 31.8
        print ("SETUP 6")

    #if (nPT != 0):
        #nameFile = "results/MC/MassFit/MC_Mass_2016_wksp_PT%d.root"%(nPT)
        #f = TFile(nameFile,"READ") 
        #wMC = f.Get("w;1")
        #f.Close()
        #if (nConf == 1):
            #ratioEtaToJpsi = wMC.var("rEtaToJpsi").getValV()+wMC.var("rEtaToJpsi").getError()
        #else:
            #ratioEtaToJpsi = wMC.var("rEtaToJpsi").getValV()   
    
    return rNtoW,rEtaToJpsi,rArea, bkgOpt, gamma, effic, rScJpsi, rScSigma
    
    
    
        

def fillRelWorkspace(w, nConf, nPT, nTz):
    
    Jpsi_M = RooRealVar("Jpsi_M","Jpsi_M",2850,3250) 
    Jpsi_M.setBins(2000,"cache")
    Jpsi_M.setRange("SBLeft", 2850, 2900)
    Jpsi_M.setRange("SBCentral", 3035, 3060)
    Jpsi_M.setRange("SBRight", 3150, 3250)
    Jpsi_M.setRange("SignalEtac", 2900, 3035)
    Jpsi_M.setRange("SignalJpsi", 3060, 3150)
    Jpsi_M.setRange("Total", 2850, 3250)
    
    mDiffEtac = 113.501
    meanJpsi = 3096.9
    meanEtac = 3083.4
    
    print type(nConf), type(nPT), type(nTz)
    ratioNtoW,ratioEtaToJpsi, ratioArea, bkgType, gammaEtac, eff, rScJpsi, rScSigma = getConstPars(nConf, nPT, nTz)
    
    
    if (nConf==10):
        Jpsi_M.setRange(2855,3245)
    
    
        
    rEtaToJpsi = RooRealVar("rEtaToJpsi","rEtaToJpsi", ratioEtaToJpsi)
    #rNarToW = RooRealVar("rNarToW","rNarToW",ratioNtoW)
    #rG1toG2 = RooRealVar("rG1toG2","rG1toG2",ratioArea)
    #eff_pppi0 = RooRealVar("eff_pppi0","eff_pppi0",eff)
    #rEtaToJpsi = RooRealVar("rEtaToJpsi","rEtaToJpsi", ratioEtaToJpsi)
    rNarToW = RooRealVar("rNarToW","rNarToW",ratioNtoW)
    rG1toG2 = RooRealVar("rG1toG2","rG1toG2",ratioArea)
    eff_pppi0 = RooRealVar("eff_pppi0","eff_pppi0",eff)
    nEta = RooRealVar("nEta_PT%s_Tz%s"%(nPT,nTz),"num of Etac", -5e2, 1.e7)
    nJpsi = RooRealVar("nJpsi_PT%s_Tz%s"%(nPT,nTz),"num of J/Psi", -5e2, 1.e7)
    nEtacRel = RooRealVar("nEtacRel_PT%s_Tz%s"%(nPT,nTz),"num of Etac", 0.0, 3.0)
    #nEtacRel = RooRealVar("nEtacRel_PT%s_Tz%s"%(nPT,nTz),"nEtacRel", "@0/@1",RooArgList(nEta,nJpsi))
    
    #nEta_1 = RooFormulaVar("nEta_1_PT%s_Tz%s"%(nPT,nTz),"num of Etac","@0*@1*@2",RooArgList(nEtacRel,nJpsi,rG1toG2))
    #nEta_2 = RooFormulaVar("nEta_2_PT%s_Tz%s"%(nPT,nTz),"num of Etac","@0*@1-@2",RooArgList(nEtacRel,nJpsi,nEta_1))
    nEta_1 = RooFormulaVar("nEta_1_PT%s_Tz%s"%(nPT,nTz),"num of Etac","@0-@1",RooArgList(nEta,rG1toG2))
    nEta_2 = RooFormulaVar("nEta_2_PT%s_Tz%s"%(nPT,nTz),"num of Etac","@0-@1",RooArgList(nEta,nEta_1))
    nJpsi_1 = RooFormulaVar("nJpsi_1_PT%s_Tz%s"%(nPT,nTz),"num of J/Psi","@0*@1",RooArgList(nJpsi,rG1toG2))
    nJpsi_2 = RooFormulaVar("nJpsi_2_PT%s_Tz%s"%(nPT,nTz),"num of J/Psi","@0-@1",RooArgList(nJpsi,nJpsi_1))
    nBckgr = RooRealVar("nBckgr_PT%s_Tz%s"%(nPT,nTz),"num of backgr",7e7,10,1.e+9)
    
    rNormJpsi = RooRealVar("rNormJpsi_Tz%s"%(nTz), "rNormJpsi", rScJpsi, 0.99, 1.01); rNormJpsi.setConstant(True)
    #rNormEtac = RooRealVar("rNormEtac_Tz%s"%(nTz), "rNormEtac", rScEtac); rNormEtac.setConstant()
    rNormSigma = RooRealVar("rNormSigma_Tz%s"%(nTz), "rNormSigma", rScSigma, 0.7, 1.3); rNormSigma.setConstant(True)
    #rNormSigma = RooRealVar("rNormSigma_Tz%s"%(nTz), "rNormSigma", 1.0); rNormSigma.setConstant(True)
         
    
    mass_Jpsi = RooRealVar("mass_Jpsi","mean of gaussian",meanJpsi, 3030, 3150) 
    mass_res = RooRealVar("mass_res","mean of gaussian",mDiffEtac, 100, 130) 
    

    mass_eta = RooFormulaVar("mass_eta","mean of gaussian","(@0-@1)",RooArgList(mass_Jpsi,mass_res)) 

    mass_Jpsi_sc = RooFormulaVar("mass_Jpsi_PT%s_Tz%s"%(nPT,nTz),"mean of gaussian", "@0*@1", RooArgList(mass_Jpsi,rNormJpsi)) 
    #mass_res_sc = RooFormulaVar("mass_res_PT%s_Tz%s"%(nPT,nTz),"mean of gaussian", "@0*@1-@2*@3", RooArgList(mass_Jpsi,rNormJpsi, mass_res, rNormEtac)) 
    #mass_eta_sc = RooFormulaVar("mass_eta_PT%s_Tz%s"%(nPT,nTz),"mean of gaussian","(@0-@1)*@2",RooArgList(mass_Jpsi,mass_res, rNormEtac)) 
    mass_eta_sc = RooFormulaVar("mass_eta_PT%s_Tz%s"%(nPT,nTz),"mean of gaussian","(@0-@1)",RooArgList(mass_Jpsi_sc,mass_res)) 

    gamma_eta = RooRealVar("gamma_eta","width of Br-W", gammaEtac, 10., 50.)
    #if nConf!=6:
    gamma_eta.setConstant()
    spin_eta = RooRealVar("spin_eta","spin_eta", 0. )
    radius_eta = RooRealVar("radius_eta","radius", 1.)
    proton_m = RooRealVar("proton_m","proton mass", 938.3 )
    
    sigma_eta = RooRealVar("sigma_eta_1","width of gaussian", 9., 5., 50.) 
    sigma_eta_1 = RooFormulaVar("sigma_eta_1_PT%s_Tz%s"%(nPT,nTz),"width of gaussian","@0*@1",RooArgList(sigma_eta,rNormSigma))
    sigma_eta_2 = RooFormulaVar("sigma_eta_2_PT%s_Tz%s"%(nPT,nTz),"width of gaussian","@0/@1",RooArgList(sigma_eta_1,rNarToW))
    
    sigma_Jpsi_1 = RooFormulaVar("sigma_Jpsi_1_PT%s_Tz%s"%(nPT,nTz),"width of gaussian","@0/@1",RooArgList(sigma_eta_1,rEtaToJpsi))
    sigma_Jpsi_2 = RooFormulaVar("sigma_Jpsi_2_PT%s_Tz%s"%(nPT,nTz),"width of gaussian","@0/@1",RooArgList(sigma_Jpsi_1,rNarToW))
    
    
    # Fit eta
    gaussEta_1 = RooGaussian("gaussEta_1_PT%s_Tz%s"%(nPT,nTz),"gaussEta_1 PDF", Jpsi_M, RooFit.RooConst(0),  sigma_eta_1)
    gaussEta_2 = RooGaussian("gaussEta_2_PT%s_Tz%s"%(nPT,nTz),"gaussEta_2 PDF", Jpsi_M, RooFit.RooConst(0),  sigma_eta_2)
        
    br_wigner = RooRelBreitWigner("br_wigner_PT%s_Tz%s"%(nPT,nTz), "br_wigner",Jpsi_M, mass_eta_sc, gamma_eta, spin_eta,radius_eta,proton_m,proton_m)
    
    bwxg_1 = RooFFTConvPdf("bwxg_1_PT%s_Tz%s"%(nPT,nTz),"breit-wigner (X) gauss", Jpsi_M, br_wigner, gaussEta_1) 
    bwxg_2 = RooFFTConvPdf("bwxg_2_PT%s_Tz%s"%(nPT,nTz),"breit-wigner (X) gauss", Jpsi_M, br_wigner, gaussEta_2) 
        
    # Fit J/psi
    gauss_1 = RooGaussian("gauss_1_PT%s_Tz%s"%(nPT,nTz),"gaussian PDF",Jpsi_M,mass_Jpsi_sc,sigma_Jpsi_1) 
    gauss_2 = RooGaussian("gauss_2_PT%s_Tz%s"%(nPT,nTz),"gaussian PDF",Jpsi_M,mass_Jpsi_sc,sigma_Jpsi_2) 
        

    #Connection between parameters
    #    RooFormulaVar f_1("f_1","f_1","@0*9.0",RooArgList(nEta_2))
    
    #Create constraints
    #    RooGaussian constrNEta("constrNEta","constraint Etac",nEta_1,f_1,RooConst(0.0)) 
    fconstraint = RooGaussian("fconstraint","fconstraint",mass_res, RooFit.RooConst(113.5), RooFit.RooConst(0.5))  #PDG 
    fconstrJpsi = RooGaussian("fconstrJpsi","fconstraint",mass_Jpsi, RooFit.RooConst(3096.9),RooFit.RooConst(0.5)) 
    
#    bkg = RooChebychev()
    a0 = RooRealVar("a0_PT%s_Tz%s"%(nPT,nTz),"a0",0.4,-1,2) 
    a1 = RooRealVar("a1_PT%s_Tz%s"%(nPT,nTz),"a1",0.05,-1.5,1.5) 
    a2 = RooRealVar("a2_PT%s_Tz%s"%(nPT,nTz),"a2",-0.005,-1.5,1.5) 
    a3 = RooRealVar("a3_PT%s_Tz%s"%(nPT,nTz),"a3",0.005,-0.1,0.1) 
    a4 = RooRealVar("a4_PT%s_Tz%s"%(nPT,nTz),"a4",0.005,-0.1,0.1) 
    
    if (bkgType == 0):
        #a0.setRange(0.0, 5.e2)
        a1.setVal(0.04)
        a2.setVal(0.04)
        #if nTz!=1:
            #bkg = RooGenericPdf("bkg_PT%s_Tz%s"%(nPT,nTz),"background","TMath::Exp(-(@0-3050.)/200.*@1)*(1.+@2*(@0-3050.)/200.+@3*(@0-3050.)*(@0-3050.)/200./200.)",RooArgList(Jpsi_M,a0,a1,a2)) 
        #else:
            #bkg = RooGenericPdf("bkg_PT%s_Tz%s"%(nPT,nTz),"background","TMath::Exp(-(@0-3050.)/200.*@1)*(1.+@2*(@0-3050.)/200)",RooArgList(Jpsi_M,a0,a1)) 
        bkg = RooGenericPdf("bkg_PT%s_Tz%s"%(nPT,nTz),"background","TMath::Exp(-(@0-3050.)/200.*@1)*(1.+@2*(@0-3050.)/200.+@3*(@0-3050.)*(@0-3050.)/200./200.)",RooArgList(Jpsi_M,a0,a1,a2)) 
        
        #bkg = RooGenericPdf("bkg","background","TMath::Exp(-(@0-3050.)/200.*@1)*(1.+@2*@0.+@3*(2*@0*@0-1))",RooArgList(Jpsi_M,a0,a1,a2)) 
    elif (bkgType == 1):
        bkg = RooChebychev ("bkg_PT%s_Tz%s"%(nPT,nTz),"Background",Jpsi_M,RooArgList(a0,a1,a2)) 
    elif (bkgType == 2):
        bkg = RooChebychev("bkg_PT%s_Tz%s"%(nPT,nTz),"Background",Jpsi_M,RooArgList(a0,a1,a2,a3))
        
    
    pppi0 = RooGenericPdf("pppi0_PT%s_Tz%s"%(nPT,nTz),"Jpsi.pppi0","@0<(2961.92) ? TMath::Sqrt(3096.900-134.977-@0) : 0",RooArgList(Jpsi_M))
    nPPPi0 = RooFormulaVar("nPPPi0_PT%s_Tz%s"%(nPT,nTz),"nPPPi0","@0*@1",RooArgList(nJpsi,eff_pppi0))
    
    if (nConf!=3):

        modelBkg = RooAddPdf("modelBkg_PT%s_Tz%s"%(nPT,nTz),"background", RooArgList(bkg,pppi0), RooArgList(nBckgr,nPPPi0))
        modelSignal = RooAddPdf("modelSignal_PT%s_Tz%s"%(nPT,nTz),"signal", RooArgList(bwxg_1, bwxg_2,gauss_1,gauss_2), RooArgList(nEta_1,nEta_2,nJpsi_1,nJpsi_2))
        model = RooAddPdf("model_PT%s_Tz%s"%(nPT,nTz),"signal+bkg", RooArgList(bwxg_1, bwxg_2,gauss_1,gauss_2, bkg,pppi0), RooArgList(nEta_1,nEta_2,nJpsi_1,nJpsi_2, nBckgr,nPPPi0))
                
        #modelC = RooProdPdf("modelC", "model with constraints",RooArgList(model,fconstraint,fconstrJpsi))
        modelC = RooProdPdf("modelC_PT%s_Tz%s"%(nPT,nTz), "model with constraints",RooArgList(model,fconstraint))
        
    else:
        
        f = TFile(homeDir+"Results/MC/MassFit/MC_MassResolution_CB_2016_wksp.root","READ") 
        wMC = f.Get("w")
        f.Close()
        
        alpha_eta_1 = RooRealVar('alpha_eta_1','alpha of CB', wMC.var('alpha_eta_1').getValV(), 0.0, 10.) 
        alpha_eta_1.setConstant(True)
        n_eta_1 = RooRealVar('n_eta_1','n of CB', wMC.var('n_eta_1').getValV(), 0.0, 100.) 
        n_eta_1.setConstant(True)    

        # Fit eta
        cb_etac_1 = BifurcatedCB("cb_etac_1_PT%s_Tz%s"%(nPT,nTz), "Cystal Ball Function", Jpsi_M, RooFit.RooConst(0), sigma_eta_1, alpha_eta_1, n_eta_1, alpha_eta_1, n_eta_1)
        br_wigner = RooRelBreitWigner("br_wigner_Tz%s"%(nTz), "br_wigner",Jpsi_M, mass_eta_sc, gamma_eta, spin_eta,radius_eta,proton_m,proton_m)
            
        bwxg_1 = RooFFTConvPdf("bwxg_1_PT%s_Tz%s"%(nPT,nTz),"breit-wigner (X) gauss", Jpsi_M, br_wigner, cb_etac_1) 
        
        # Fit J/psi
        cb_Jpsi_1 = BifurcatedCB("cb_Jpsi_1_PT%s_Tz%s"%(nPT,nTz), "Cystal Ball Function", Jpsi_M, mass_Jpsi_sc, sigma_Jpsi_1, alpha_eta_1, n_eta_1, alpha_eta_1, n_eta_1)

        modelBkg = RooAddPdf("modelBkg_PT%s_Tz%s"%(nPT,nTz),"background", RooArgList(bkg,pppi0), RooArgList(nBckgr,nPPPi0))
        modelSignal = RooAddPdf("modelSignal_PT%s_Tz%s"%(nPT,nTz),"signal", RooArgList(bwxg_1, cb_Jpsi_1), RooArgList(nEta,nJpsi))
        model = RooAddPdf("model_PT%s_Tz%s"%(nPT,nTz),"signal+bkg", RooArgList(bwxg_1, cb_Jpsi_1, bkg,pppi0), RooArgList(nEta, nJpsi, nBckgr, nPPPi0))
        
        #modelC = RooProdPdf("modelC", "model with constraints",RooArgList(model,fconstraint,fconstrJpsi))
        modelC = RooProdPdf("modelC_PT%s_Tz%s"%(nPT,nTz), "model with constraints",RooArgList(model,fconstraint))
    

    getattr(w,'import')(model, RooFit.RecycleConflictNodes())
    getattr(w,'import')(modelC, RooFit.RecycleConflictNodes())
    getattr(w,'import')(modelBkg, RooFit.RecycleConflictNodes())
    getattr(w,'import')(modelSignal, RooFit.RecycleConflictNodes())


def fitData( nPT=0, nTzs=[0], nConf=0, keyPrompt=True, charm="Etac"):

    #gROOT.Reset()
    
    w = RooWorkspace("w",kTRUE)   
    
    #if (nConf!=10):
        #getData_h(w,keyPrompt, nPT, nTz, charm)
    #else:
        #getData_h(w,keyPrompt, nPT, nTz, charm, True)
    
    
    Jpsi_M = w.var("Jpsi_M") 
    
    hists = []    
    #nPTbins = len(nPTs)
    nTzbins = len(nTzs)


    fillRelWorkspace(w,nConf,0, 1)
    #if nConf!=3:
        #model = fillRelWorkspace(w,nConf,1, 1)
    #else:
        #model = fillRelWorkspaceCB(w,nConf,1, 1)


    Jpsi_M = w.var("Jpsi_M")
    Jpsi_M.setBins(nBins)

    for nTz in nTzs:
        print nTz
        fillRelWorkspace(w,nConf,nPT, nTz)
        print w.var("rNormJpsi_Tz%s"%nTz).getValV()
        #if nConf!=3:
            #model = fillRelWorkspace(w,nConf,nPT, nTz)
        #else:
            #model = fillRelWorkspaceCB(w,nConf,nPT, nTz)
        getData_h(w, keyPrompt, nPT, nTz, charm)

        histo = w.data("dh_PT%s_Tz%s"%(nPT,nTz))
        hists.append(histo)

    
    sample = RooCategory("sample","sample") 

    model_pts = []
    for nTz in nTzs:
        sample.defineType("PT%s_Tz%s"%(nPT,nTz)) 
        model_pts.append(w.pdf("model_PT%s_Tz%s"%(nPT,nTz)))

    #import ROOT.std as std
    #MapStrRootPtr = std.map(std.string, "RooDataHist") 
    #StrHist = std.pair(std.string, "RooDataHist") 
    #input_hists = MapStrRootPtr() 
    #for nTz in nTzs:
        #idxTz = nTzs.index(nTz)
        #input_hists.insert(StrHist("PT%s_Tz%s"%(nPT,nTz), hists[idxTz])) 
    
    #combData = RooDataHist("combData","combData",RooArgList(Jpsi_M,sample),RooFit.Index(sample), input_hists)
   
    #if nPT==0:
        #combData = RooDataHist("combData","combData",RooArgList(Jpsi_M,sample),RooFit.Index(sample),RooFit.Import("Tz1",hists[0]),RooFit.Import("Tz2",hists[1]),RooFit.Import("Tz3",hists[2]),RooFit.Import("Tz4",hists[3]),RooFit.Import("Tz5",hists[4]),RooFit.Import("Tz6",hists[5]),RooFit.Import("Tz7",hists[6]),RooFit.Import("Tz8",hists[7]),RooFit.Import("Tz9",hists[8]),RooFit.Import("Tz10",hists[9]))
    #else:
        #combData = RooDataHist("combData","combData",RooArgList(Jpsi_M,sample),RooFit.Index(sample),RooFit.Import("Tz1",hists[0]),RooFit.Import("Tz2",hists[1]),RooFit.Import("Tz3",hists[2]),RooFit.Import("Tz4",hists[3]),RooFit.Import("Tz5",hists[4]))
    #combData = RooDataHist("combData","combData",RooArgList(Jpsi_M,sample),RooFit.Index(sample),RooFit.Import("PT0_Tz2",hists[0]),RooFit.Import("PT0_Tz3",hists[1]),RooFit.Import("PT0_Tz4",hists[2]),RooFit.Import("PT0_Tz5",hists[3]),RooFit.Import("PT0_Tz6",hists[4]))
    
    
    #---------------11 Tz bins----------------------------------------------------------
    combData = RooDataHist("combData","combData",RooArgList(Jpsi_M,sample),RooFit.Index(sample),RooFit.Import("PT0_Tz1",hists[0]),RooFit.Import("PT0_Tz2",hists[1]),RooFit.Import("PT0_Tz3",hists[2]),RooFit.Import("PT0_Tz4",hists[3]),RooFit.Import("PT0_Tz5",hists[4]), RooFit.Import("PT0_Tz6",hists[5]), RooFit.Import("PT0_Tz7",hists[6]))


    #---------------11 Tz bins----------------------------------------------------------
    #sample.defineType("Part1")
    #sample.defineType("Part2")
    
    #combDataPart1 = RooDataHist("combDataPart1","combDataPart1",RooArgList(Jpsi_M,sample),RooFit.Index(sample),RooFit.Import("PT0_Tz1",hists[0]),RooFit.Import("PT0_Tz2",hists[1]),RooFit.Import("PT0_Tz3",hists[2]),RooFit.Import("PT0_Tz4",hists[3]),RooFit.Import("PT0_Tz5",hists[4]))

    #combDataPart2 = RooDataHist("combDataPart2","combDataPart2",RooArgList(Jpsi_M,sample),RooFit.Index(sample), RooFit.Import("PT0_Tz6",hists[5]), RooFit.Import("PT0_Tz7",hists[6]), RooFit.Import("PT0_Tz8",hists[7]), RooFit.Import("PT0_Tz9",hists[8]), RooFit.Import("PT0_Tz10",hists[9]), RooFit.Import("PT0_Tz11",hists[10]))
    
    #combData = RooDataHist("combData","combData",RooArgList(Jpsi_M,sample),RooFit.Index(sample), RooFit.Import("Part1",combDataPart1), RooFit.Import("Part2",combDataPart2))
    
    
    #sigma = w.var("sigma_eta_1")
    mass_Jpsi = w.var("mass_Jpsi")
    mass_res = w.var("mass_res")
    gamma_eta = w.var("gamma_eta")

    if keyPrompt:

        f = TFile(homeDir+"Results/MassFit/fromB/Total/Wksp_MassFit_PT0_Tz0_C0.root","READ") 
        wSec = f.Get("w")
        f.Close()
        w.var("mass_Jpsi").setVal(wSec.var("mass_Jpsi").getValV())
        #w.var("mass_Jpsi").setConstant(True)
        w.var("mass_res").setVal(wSec.var("mass_res").getValV())        
        #w.var("mass_res").setVal(110.2)  #etac2S2pp
        #w.var("mass_res").setVal(113.5)  #PDG
        #w.var("mass_res").setConstant(True)
        w.var("sigma_eta_1").setVal(wSec.var("sigma_eta_1").getValV())
        #w.var("sigma_eta_1").setConstant(True)
        
        #for nTz in nTzs:
            #pt = [7250, 9000, 11000, 13000 , 16000] 
            #f= TFile(homeDir+"Results/MC/MassFit/parFit.root","READ")
            #fSigma = f.Get("fSigmaDATA")
            #f.Close()
            #w.var("sigma_eta_1_PT%s_Tz%s"%(nPT,nTz)).setVal(fSigma.Eval(pt[nPT-1]))
            #w.var("sigma_eta_1_PT%s_Tz%s"%(nPT,nTz)).setConstant(True)

    #w.var("rNormJpsi_Tz1").setVal(w.var("rNormJpsi_Tz2").getValV())
    #w.var("rNormSigma_Tz1").setVal(w.var("rNormSigma_Tz2").getValV())
    #w.var("rNormJpsi_Tz1").setConstant(False)
    #w.var("rNormJpsi_Tz7").setVal(1.0)
    #w.var("rNormJpsi_Tz7").setConstant(True)
    #w.var("rNormSigma_Tz7").setVal(1.0)
    #w.var("rNormSigma_Tz7").setConstant(True)
    #w.var("a2_PT%s_Tz1"%(nPT)).setVal(0)
    #w.var("a2_PT%s_Tz1"%(nPT)).setConstant(True)
    #w.var("nBckgr_PT%s_Tz1"%(nPT)).setVal(3.1e4)


    simPdf = RooSimultaneous("smodel","",sample)
    for nTz in nTzs:
        idx = nTzs.index(nTz)
        simPdf.addPdf(model_pts[idx],"PT%s_Tz%s"%(nPT,nTz))

        #w.var("rNormJpsi_PT%s_Tz%s"%(nPT,nTz)).setVal(1.0)
        if nTz!=7:
            w.var("rNormJpsi_Tz%s"%(nTz)).setConstant(False)
            w.var("rNormSigma_Tz%s"%(nTz)).setConstant(False)
        model_pts[idx].fitTo(hists[idx], RooFit.Extended(True), RooFit.Offset(True), RooFit.Save(True))
        model_pts[idx].fitTo(hists[idx], RooFit.Extended(True), RooFit.Offset(True), RooFit.Minos(True), RooFit.Save(True))
        #model_pts[idx].fitTo(hists[idx], RooFit.Extended(True), RooFit.Offset(True), RooFit.Minos(True), RooFit.Save(True))
        bgVal = w.var("nBckgr_PT%s_Tz%s"%(nPT,nTz)).getValV()
        bgErr = w.var("nBckgr_PT%s_Tz%s"%(nPT,nTz)).getError()
        w.var("nBckgr_PT%s_Tz%s"%(nPT,nTz)).setRange(bgVal-15*bgErr, bgVal+15*bgErr)
        a0Val = w.var("a0_PT%s_Tz%s"%(nPT,nTz)).getValV()
        a0Err = w.var("a0_PT%s_Tz%s"%(nPT,nTz)).getError()
        #w.var("a0_PT%s_Tz%s"%(nPT,nTz)).setRange(a0Val-15*a0Err, a0Val+15*a0Err)
        a1Val = w.var("a1_PT%s_Tz%s"%(nPT,nTz)).getValV()
        a1Err = w.var("a1_PT%s_Tz%s"%(nPT,nTz)).getError()
        #w.var("a1_PT%s_Tz%s"%(nPT,nTz)).setRange(a1Val-15*a1Err, a1Val+15*a1Err)
        #a2Val = w.var("a2_PT%s_Tz%s"%(nPT,nTz)).getValV()
        #a2Err = w.var("a2_PT%s_Tz%s"%(nPT,nTz)).getError()
        #w.var("a2_PT%s_Tz%s"%(nPT,nTz)).setRange(a2Val-15*a2Err, a2Val+15*a2Err)


    #w.var("nBckgr_PT%s_Tz1"%(nPT)).setVal(3.1e4)
    #w.var("nBckgr_PT%s_Tz1"%(nPT)).setRange(-100, 4e5)
    
    
    #NLL with extended term, constrains, and likelihood offset
    nll = simPdf.createNLL(combData, RooFit.Extended(True), RooFit.Offset(True))

    # Manual minimization
    minimizer = RooMinimizer(nll)
    minimizer.setVerbose(False)
    minimizer.setMinimizerType("Minuit2")
    minimizer.optimizeConst(True)
    minimizer.setEps(1e-8)
    minimizer.setStrategy(2)
    minimizer.setPrintEvalErrors(1)
    minimizer.setOffsetting(True)
    minimizer.setMaxIterations(1000)
    minimizer.migrad()
    minimizer.minos()

    #ROOT.Math.MinimizerOptions.SetMaxIterations(1000)
    
    r = simPdf.fitTo(combData,RooFit.Strategy(2))
    r = simPdf.fitTo(combData, RooFit.Extended(True), RooFit.Offset(True), RooFit.Save(True))
    r = simPdf.fitTo(combData, RooFit.Extended(True),RooFit.Minos(True),RooFit.Save(True), RooFit.NumCPU(48))
    r = simPdf.fitTo(combData, RooFit.Extended(True),RooFit.Minos(True),RooFit.Save(True), RooFit.Offset(True),RooFit.NumCPU(48))
    #r = simPdf.fitTo(combData, RooFit.Extended(True),RooFit.Minos(True),RooFit.Save(True), RooFit.Offset(True),RooFit.NumCPU(48))
        
    #getattr(w,'import')(combData)
    #getattr(w,'import')(sample, RooFit.RecycleConflictNodes())
    #getattr(w,'import')(simPdf, RooFit.RecycleConflictNodes())
    Draw(w, combData, sample, simPdf, [nPT], nTzs, nConf, keyPrompt, charm)


    if keyPrompt: inCutKey = "prompt"
    else:         inCutKey = "fromB"
    
    #if nPT!=0:    binPT = "Jpsi_PT/"
    #else:         binPT = "Total/"

    binPT = "Sim/"
    #binTz = "Tz%s.root"%(nTz)
    
    dirName = homeDir+"Results/MassFit/" + inCutKey + "/" + binPT + "m_scaled/"
    
    #if (nPT!=0 and nTz!=0):
        #dirName = dirName + charm + "/"
        
    nameTxt  = dirName + charm + "_fitRes_PT%s_C%d_total_sigma_7BinsNorm.txt"%(nPT,nConf)
    #nameRoot = dirName + charm + "_Jpsi_MassFit_PT%s_C%d_total_test.root"%(nPT,nConf)
    #nameWksp = dirName + charm + "_Wksp_MassFit_PT%s_C%d_total_test.root"%(nPT,nConf)
    #namePic  = dirName + charm + "_MassFit_PT%s_C%d_total_test.pdf"%(nPT,nConf)
    

    import os, sys 
    save = os.dup( sys.stdout.fileno() ) 
    newout = file(nameTxt, 'w' ) 
    os.dup2( newout.fileno(), sys.stdout.fileno() ) 
    r.Print("v") 
    r.correlationMatrix().Print()
    os.dup2( save, sys.stdout.fileno() ) 
    newout.close()
    
    #w.writeToFile(nameWksp)
    
    #fFit = TFile (nameRoot,"RECREATE")
    #w.Print("v")
    #c.Write("")
    #fFit.Close()

    #c.SaveAs(namePic)
    ##r.correlationMatrix().Print()



def Fit_Mass():

    nPT = 5
    nTzTot = 10
    nTzEtac = 5
    nTzJpsi = 7
    nConf = 11
    iConf = 0
    pr = True
    #    Bool_t pr = False
    #    for (Int_t iConf=0 iConf<10 iConf++)
    #    {
    #        fitData(iConf,0,pr)
    #    }
    #   Int_t iPT=0
    #fitData(0, 0, 0, False, 'Etac')
    #for iPT in range (1, nPT+1):        
            #fitData(iPT, 0, iConf, False, 'Etac')
    #for iTz in range (1, nTzTot+1):        
            #fitData(0, iTz, iConf, pr, 'Etac')
    #for iPT in range (1, nPT+1):        
            #fitData(iPT, 0, iConf, pr, 'Etac')
            
    #for iPT in range(1, nPT+1):
        #for iTz in range (1, nTzJpsi+1):        
            #fitData(iPT, iTz, iConf, pr, 'Jpsi')
    #for iPT in range(1, nPT+1):
        #for iTz in range (1, nTzEtac+1):        
            #fitData(iPT, iTz, iConf, pr, 'Etac')

#w = RooWorkspace("w",kTRUE)   
#fillRelWorkspace(w,0,0)
#getData_h(w, False, 1, 0,"Etac")

#fitData( 0, 0, 4, False)
#fitData( 0, 0, 4, True)
nTzs = [1, 2, 3, 4, 5, 6, 7]
#nConfs = [0, 1, 3, 4, 6, 7, 8, 9]
nConfs = [0]
for iC in nConfs:
    fitData( 0, nTzs, iC, True, "Etac")
#fitData( 0, nTzs, 0, True,"Etac")
#fitData( 1, 2, 0, True)
            
#Fit_Mass()
