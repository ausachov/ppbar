
from ROOT import * 
gROOT.LoadMacro("../libRooRelBreitWigner/RooRelBreitWigner.cxx+") 
gROOT.LoadMacro("../BifurcatedCB/BifurcatedCB.cxx+")


from setupPlot import *
from setupFitModel_RunI import *

def promptImp(nPT):

    ntJpsi_Prompt = TChain('DecayTree')
    ntJpsi_FromB = TChain('DecayTree')
    
    ntJpsi_Prompt.Add('../MC/tuples/selected/all_l0TOS/Jpsi_Pr_all_l0TOS_allSelected_AddBr.root')
    ntJpsi_FromB.Add('../MC/tuples/selected/all_l0TOS/Jpsi_Pr_all_l0TOS_allSelected_AddBr.root')
    
    treeJpsi_Prompt = TTree()
    treeJpsi_FromB = TTree()

    treeJpsi_Prompt = ntJpsi_Prompt.CopyTree("Jpsi_prompt")
    treeJpsi_FromB = ntJpsi_FromB.CopyTree("Jpsi_sec")
    
    NJpsi_Prompt = treeJpsi_Prompt.GetEntries()
    NJpsi_FromB = treeJpsi_FromB.GetEntries()

    cutJpsi_Tz_Sec = 'Jpsi_Tz > 0.08'
    cutJpsi_Tz_Prompt = 'Jpsi_Tz < 0.08'
    cutJpsi_IP_CHI2 = '((ProtonP_IPCHI2_OWNPV > 16) && (ProtonM_IPCHI2_OWNPV > 16))'
    
    if nPT==0:
       cutPT = "Jpsi_PT>6500 && Jpsi_PT<14000"
    elif nPT==1:
       cutPT = "Jpsi_PT>6500 && Jpsi_PT<8000"
    elif nPT==2:
       cutPT = "Jpsi_PT>8000 && Jpsi_PT<10000"
    elif nPT==3:
       cutPT = "Jpsi_PT>10000 && Jpsi_PT<12000"
    elif nPT==4:
       cutPT = "Jpsi_PT>12000 && Jpsi_PT<14000"       

    NJpsi_PP = treeJpsi_Prompt.GetEntries(cutPT + " && " + cutJpsi_Tz_Prompt)
    NJpsi_PB = treeJpsi_Prompt.GetEntries(cutPT + " && " + cutJpsi_Tz_Sec + "&&" + cutJpsi_IP_CHI2)
    NJpsi_BB = treeJpsi_FromB.GetEntries(cutPT + " && " + cutJpsi_Tz_Sec + "&&" + cutJpsi_IP_CHI2)
    NJpsi_BP = treeJpsi_FromB.GetEntries(cutPT + " && " + cutJpsi_Tz_Prompt)


    effPP = float(NJpsi_PP/float(NJpsi_Prompt))
    effBB = float(NJpsi_BB/float(NJpsi_FromB))
    effPB = float(NJpsi_PB/float(NJpsi_Prompt))
    effBP = float(NJpsi_BP/float(NJpsi_FromB))


    effPP_err = effPP*(1/float(NJpsi_PP)+1/float(NJpsi_Prompt))**0.5
    if NJpsi_PB!=0:
       effPB_err = effPB*(1/float(NJpsi_PB)+1/float(NJpsi_Prompt))**0.5
    else: 
       effPB_err = effPB*2
    effBB_err = effBB*(1/float(NJpsi_BB)+1/float(NJpsi_FromB))**0.5
    effBP_err = effBP*(1/float(NJpsi_BP)+1/float(NJpsi_FromB))**0.5

    return effPP, effPB, effBP, effBB, effPP_err, effPB_err, effBP_err, effBB_err


def correctedYield(w, nPT, nConf):
    
    effPP, effPB, effBP, effBB, effPP_err, effPB_err, effBP_err, effBB_err  = promptImp(nPT)
    
    if nConf=='effPP':
            effPP += effPP_err
    elif nConf=='effPB':
            effPB += effPB_err
    elif nConf=='effBP':
            effBP += effBP_err
    elif nConf=='effBB':
            effBB += effBB_err            
        
    
    nJpsi_Pr = w.var("nJpsi_PT%s_Prompt"%(nPT)).getValV()
    nJpsi_FrB = w.var("nJpsi_PT%s_FromB"%(nPT)).getValV()
    nEtac_Pr = w.var("nEta_PT%s_Prompt"%(nPT)).getValV()
    nEtac_FrB = w.var("nEta_PT%s_FromB"%(nPT)).getValV()

    nJpsi_Pr_Err  = w.var("nJpsi_PT%s_Prompt"%(nPT)).getError()
    nJpsi_FrB_Err = w.var("nJpsi_PT%s_FromB"%(nPT)).getError()
    nEtac_Pr_Err  = w.var("nEta_PT%s_Prompt"%(nPT)).getError()
    nEtac_FrB_Err = w.var("nEta_PT%s_FromB"%(nPT)).getError()
        
    NRel_Pr  = (nEtac_Pr*effBB - nEtac_FrB*effBP) / (nJpsi_Pr*effBB - nJpsi_FrB*effBP)
    NRel_FrB = (nEtac_FrB*effPP - nEtac_Pr*effPB) / (nJpsi_FrB*effPP - nJpsi_Pr*effPB)

    
    dNRel_Pr  = NRel_Pr * ( ((nEtac_Pr_Err*effBB)**2 + (nEtac_FrB_Err*effBP)**2)/(nEtac_Pr*effBB - nEtac_FrB*effBP)**2 + ((nJpsi_Pr_Err*effBB)**2 + (nJpsi_FrB_Err*effBP)**2)/(nJpsi_Pr*effBB - nJpsi_FrB*effBP)**2 )**0.5
    dNRel_FrB = NRel_FrB * ( ((nEtac_FrB_Err*effPP)**2 + (nEtac_Pr_Err*effPB)**2)/(nEtac_FrB*effPP - nEtac_Pr*effPB)**2 + ((nJpsi_FrB_Err*effPP)**2 + (nJpsi_Pr_Err*effPB)**2)/(nJpsi_FrB*effPP - nJpsi_Pr*effPB)**2 )**0.5

    nameTxt = "../Results/MassFit/RunIMethod/correctedYield_PT%s_C%s.txt"%(nPT,nConf)
    
    fo = open(nameTxt,'w')

    fo.write("nEtac_Prompt Relative = %2.4f +/- %2.4f, %3.2f \n"   %( NRel_Pr, dNRel_Pr, NRel_Pr/dNRel_Pr ))
    fo.write("nEtac_FromB Relative = %2.4f +/- %2.4f, %3.2f \n"   %( NRel_FrB, dNRel_FrB, NRel_FrB/dNRel_FrB ))

    NEtac_Prompt = RooRealVar("NEtac_Prompt","num of Etac", NRel_Pr); NEtac_Prompt.setError(dNRel_Pr)
    NEtac_FromB = RooRealVar("NEtac_FromB","num of Etac", NRel_FrB); NEtac_FromB.setError(dNRel_FrB)

    getattr(w,'import')(NEtac_Prompt)
    getattr(w,'import')(NEtac_FromB)

    fo.close()


def getData_h(w, keyPrompt, nPT=0, nTz=0, shift=False):
       
    
    if keyPrompt: 
        inCutKey = "prompt_l0TOS"
        histName = "dh_PT%s_Prompt"%(nPT)
    else:         
        inCutKey = "secondary_l0TOS"
        histName = "dh_PT%s_FromB"%(nPT)
    
    if nPT!=0:    binPT = "Jpsi_PT/bin%s_"%(nPT) 
    else:         binPT = "Total/"

    binTz = "Tz%s.root"%(nTz)
    
    outDirName_2015 = "../Histos/2015/DiProton/" + inCutKey + "/"
    outDirName_2016 = "../Histos/2016/DiProton/" + inCutKey + "/"
        
    nameFile = outDirName_2015 + binPT + binTz
    f_2015 = TFile(nameFile,"read")
    print nameFile
    nameFile = outDirName_2016 + binPT + binTz
    f_2016 = TFile(nameFile,"read")
    print nameFile
    
    
    hh = TH1D()
    hh = f_2015.Get("Jpsi_M")
    hh.Add(f_2016.Get("Jpsi_M"))
    
    #Jpsi_M = RooRealVar("Jpsi_M","Jpsi_M",2850,3250) 
    Jpsi_M = w.var("Jpsi_M") 

    if(shift):
        hh.SetBins(276,2705,3395)
        Jpsi_M.setRange(2855,3245) 

    dh = RooDataHist(histName,histName, RooArgList(Jpsi_M) ,hh)
    getattr(w,'import')(dh)
    f_2015.Close()
    f_2016.Close()
    
    print "DATA\'S READ SUCCESSFULLY"



def fitData(nPT, nConf):
    w = RooWorkspace('w',True)  

    keyPrompt =  "PT%s_Prompt"%(nPT)
    keyFromB = "PT%s_FromB"%(nPT)

    fillRelWorkspace(w, nConf, nPT, keyPrompt)
    fillRelWorkspace(w, nConf, nPT, keyFromB)

    getData_h(w, True,  nPT, 0)
    getData_h(w, False, nPT, 0)
        
    model_Prompt = w.pdf("model_%s"%(keyPrompt))
    model_FromB  = w.pdf("model_%s"%(keyFromB))
    


    Jpsi_M = w.var('Jpsi_M')
    
    sample = RooCategory('sample','sample') 
    sample.defineType(keyPrompt) 
    sample.defineType(keyFromB) 
    
    hist_Prompt = w.data('dh_%s'%(keyPrompt))
    hist_FromB = w.data('dh_%s'%(keyFromB))
    
    # combData = RooDataHist("combData", "combined data", RooArgList(Jpsi_M), RooFit.Index(sample), RooFit.Import(keyFromB,hist_FromB))
    combData = RooDataHist("combData", "combined data", RooArgList(Jpsi_M), RooFit.Index(sample), RooFit.Import(keyPrompt,hist_Prompt), RooFit.Import(keyFromB,hist_FromB))
    
    
    simPdf = RooSimultaneous('simPdf','simultaneous signal pdf',sample) 
    simPdf.addPdf(model_Prompt,keyPrompt) 
    simPdf.addPdf(model_FromB,keyFromB) 

    sigma = w.var("sigma_eta_PT%s"%(nPT))
    mass_Jpsi = w.var('mass_Jpsi')
    mass_res = w.var('mass_res')
    gamma_eta = w.var('gamma_eta')
    

    mass_Jpsi.setConstant(True)
    mass_res.setConstant(True)

    w.var("a2_%s"%(keyFromB)).setVal(0.)
#    w.var("a2_%s"%(keyFromB)).setConstant(True)


    simPdf.fitTo(combData, RooFit.Extended(True), RooFit.Offset(True), RooFit.Save(True), RooFit.NumCPU(48)) 
    simPdf.fitTo(combData, RooFit.Extended(True), RooFit.Offset(True), RooFit.Save(True), RooFit.NumCPU(48)) 
    mass_Jpsi.setConstant(False)
    mass_res.setConstant(False)



    massJpsi_tot = 3096.57; massJpsi_totErr = 0.106; 
    massres_tot  = 112.62; massres_totErr =  0.83; 
    fconstrJpsi = RooGaussian("fconstrEtac","fconstrEtac",mass_Jpsi, RooFit.RooConst(massJpsi_tot), RooFit.RooConst(massJpsi_totErr))  #PDG 
    fconstrEtac = RooGaussian("fconstrJpsi","fconstrJpsi",mass_res, RooFit.RooConst(massres_tot),RooFit.RooConst(massres_totErr))
    constraints = RooFit.ExternalConstraints(RooArgSet(fconstrEtac,fconstrJpsi))




    if(nPT!=0):
        simPdf.fitTo(combData, RooFit.Extended(True), RooFit.Offset(True), RooFit.Save(True),constraints)
        simPdf.fitTo(combData, RooFit.Extended(True), RooFit.Offset(True), RooFit.Save(True), RooFit.NumCPU(48),constraints) 
        simPdf.fitTo(combData, RooFit.Extended(True), RooFit.Offset(True), RooFit.Minos(True), RooFit.Save(True), RooFit.Strategy(1), RooFit.NumCPU(48),constraints)
        simPdf.fitTo(combData, RooFit.Extended(True), RooFit.Offset(True), RooFit.Minos(True), RooFit.Save(True), RooFit.Strategy(1), RooFit.NumCPU(48),constraints)
        # simPdf.fitTo(combData, RooFit.Extended(True), RooFit.Offset(True), RooFit.Minos(True), RooFit.Save(True), RooFit.Strategy(1), RooFit.NumCPU(48),constraints)
        r = simPdf.fitTo(combData, RooFit.Extended(True), RooFit.Offset(True), RooFit.Minos(True), RooFit.Save(True), RooFit.Strategy(1), RooFit.NumCPU(48),constraints)

# # BEGIN 1: to check masses from individual pt-fits::
#         simPdf.fitTo(combData, RooFit.Extended(True), RooFit.Offset(True), RooFit.Save(True))
#         simPdf.fitTo(combData, RooFit.Extended(True), RooFit.Offset(True), RooFit.Save(True), RooFit.NumCPU(48)) 
#         simPdf.fitTo(combData, RooFit.Extended(True), RooFit.Offset(True), RooFit.Save(True), RooFit.Strategy(1), RooFit.NumCPU(48))
#         simPdf.fitTo(combData, RooFit.Extended(True), RooFit.Offset(True), RooFit.Save(True), RooFit.Strategy(1), RooFit.NumCPU(48)) 
#         simPdf.fitTo(combData, RooFit.Extended(True), RooFit.Offset(True), RooFit.Minos(False), RooFit.Save(True), RooFit.Strategy(1), RooFit.NumCPU(48)) 
#         r = simPdf.fitTo(combData, RooFit.Extended(True), RooFit.Offset(True), RooFit.Minos(False), RooFit.Save(True), RooFit.Strategy(1), RooFit.NumCPU(48))  
# # END 1 
    else:
        simPdf.fitTo(combData, RooFit.Extended(True), RooFit.Offset(True))
        simPdf.fitTo(combData, RooFit.Extended(True), RooFit.Offset(True), RooFit.NumCPU(48)) 
#        simPdf.fitTo(combData, RooFit.Extended(True), RooFit.Offset(True), RooFit.Minos(True), RooFit.Strategy(1), RooFit.NumCPU(48))
#        simPdf.fitTo(combData, RooFit.Extended(True), RooFit.Offset(True), RooFit.Minos(True), RooFit.Strategy(1), RooFit.NumCPU(48))
#        simPdf.fitTo(combData, RooFit.Extended(True), RooFit.Offset(True), RooFit.Minos(True), RooFit.Strategy(1), RooFit.NumCPU(48))
#        simPdf.fitTo(combData, RooFit.Extended(True), RooFit.Offset(True), RooFit.Minos(True), RooFit.Strategy(2), RooFit.NumCPU(48))
        simPdf.fitTo(combData, RooFit.Extended(True), RooFit.Offset(True), RooFit.Minos(True), RooFit.Strategy(2), RooFit.NumCPU(48))
        r = simPdf.fitTo(combData, RooFit.Minos(True), RooFit.Extended(True), RooFit.Offset(True), RooFit.Save(True), RooFit.Strategy(1), RooFit.NumCPU(48)) 
         

    correctedYield(w, nPT, nConf)
    


    frames = []; pulls = []; resids = []


    if nConf=='CB':
        signal = RooArgSet(w.pdf("cb_Jpsi_1_PT%s"%(nPT)),w.pdf("bwxg_1_PT%s"%(nPT)))
    else:
        signal = RooArgSet(w.pdf("gauss_1_PT%s"%(nPT)), 
                           w.pdf("gauss_2_PT%s"%(nPT)),
                           w.pdf("bwxg_1_PT%s"%(nPT)), 
                           w.pdf("bwxg_2_PT%s"%(nPT))) 
        

    fullBkg_Prompt =  RooArgSet(w.pdf("bkg_%s"%(keyPrompt)),w.pdf("pppi0"))  
    fullBkg_FromB  =  RooArgSet(w.pdf("bkg_%s"%(keyFromB)),w.pdf("pppi0"))       

    frames0, resid0, pull0, chi2_Prompt = setupFrame(Jpsi_M, keyPrompt, hist_Prompt,model_Prompt,signal, fullBkg_Prompt)
    frames1, resid1, pull1, chi2_FromB = setupFrame(Jpsi_M, keyFromB, hist_FromB, model_FromB,signal,fullBkg_FromB)
    
    frames.append(frames0); frames.append(frames1)
    resids.append(resid0); resids.append(resid1)
    pulls.append(pull0);  pulls.append(pull1)
 
    
    
    inCutKey = "RunIMethod"
    outDirName = "../Results/MassFit/" + inCutKey + "/" 
            
    nameTxt     = outDirName + "fitRes_PT%s_C%s.txt"%(nPT,nConf)
    nameRoot    = outDirName + "Jpsi_MassFit_PT%s_C%s.root"%(nPT,nConf)
    nameWksp    = outDirName + "Wksp_MassFit_PT%s_C%s.root"%(nPT,nConf)

    names = ["prompt sample", "b-decays sample"]
    c = setupCanvas(names, frames, resids, pulls) 
    namePic  = outDirName + "Jpsi_MassFit_PT%s_C%s.pdf"%(nPT,nConf)   
    c.SaveAs(namePic)

    
    import os, sys 
    save = os.dup( sys.stdout.fileno() ) 
    newout = file(nameTxt, 'w' ) 
    os.dup2( newout.fileno(), sys.stdout.fileno() ) 
    r.Print("v") 
    r.correlationMatrix().Print()
    os.dup2( save, sys.stdout.fileno() ) 
    newout.close()

    fo = open(nameTxt,'a')
    fo.write('$chi^2 prompt$ %6.4f \n'%(chi2_Prompt))
    fo.write('$chi^2 from-b$ %6.4f \n'%(chi2_FromB))
    fo.close()

    
    w.writeToFile(nameWksp)

    fFit = TFile(nameRoot,'RECREATE')
    fFit.Write()
    fFit.Close()
    
    r.correlationMatrix().Print('v')
    r.globalCorr().Print('v')
    
    return 0 

# nConfs = ['Gamma34','effPP','effPB','effBB','effBP']

if __name__ == "__main__":
    nConfs = ['Base','rEtaToJpsi','CB','Chebychev3par','eff_pppi0','Gamma34','effPP','effPB','effBB','effBP']
    nConfs = ['effPP','effPB','effBB','effBP']
    nConfs = ['Base']
    for iPT in range (4,5):        
        for iConf in nConfs:        
            fitData(iPT, iConf)
            # fitData(iPT, 3)


    # effPP, effPB, effBP, effBB, effPP_err, effPB_err, effBP_err, effBB_err = promptImp(0)
    # print "effPP = ",effPP," +- ",effPP_err
    # print "effBB = ",effBB," +- ",effBB_err
    # print "effPB = ",effPB," +- ",effPB_err
    # print "effBP = ",effBP," +- ",effBP_err
