from ROOT import *
gROOT.LoadMacro("../libRooRelBreitWigner/RooRelBreitWigner.cxx+")
gROOT.LoadMacro("../BifurcatedCB/BifurcatedCB.cxx+")

histosDir = "/users/LHCb/zhovkovska/scripts/Histos/"
homeDir = "/users/LHCb/zhovkovska/scripts/"

binningDict = {
    "Jpsi_PT":                      [6500, 8000, 10000, 12000, 14000, 18000],
    "Jpsi_ETA":                     [2.0, 2.45, 2.8, 3.15, 4.5],
    "TMath::Abs(ProtonM_CosTheta)": [0.0, 0.15, 0.3, 0.50, 1.0],
    "nSPDHits":                     [0, 100, 150, 200, 250, 300],
}


minM = 2850
maxM = 3250
nBins = 1000


def getConstPars(nConf, nPT=0, nTz=0):
    
    if nConf==3: add="_CB"
    else:        add=""
    
    f = TFile(homeDir+"Results/MC/MassFit/MC_MassResolution_2016_wksp.root","READ") 
    wMC = f.Get("w")
    f.Close()
    
    rNtoW = wMC.var("rNarToW").getValV()
    rEtaToJpsi = wMC.var("rEtaToJpsi").getValV()
    rArea = wMC.var("rG1toG2").getValV()
    sigma = wMC.var("sigma_eta_1").getValV()
    bkgOpt = 0
    gamma = 31.8
    #effic = 0.035
    #gamma = 21.3
    effic = 0.057*1.19/2.12
    #ratioNtoW = 0.5
    #ratioEtaToJpsi = 0.88
    #ratioArea = 0.9
    
    if nPT !=0 : 
        f = TFile(homeDir+"Results/MC/MassFit/parFit%s_GeV.root"%(add),"READ")
        fSigma = f.Get("fSigmaMC")
        f.Close()
        ptL = binningDict["Jpsi_PT"][nPT-1]
        ptR = binningDict["Jpsi_PT"][nPT]
        rScSigma = fSigma.Eval((ptR+ptL)/2000.)/sigma
    else:
        rScSigma = 1
    
    f = TFile("/users/LHCb/zhovkovska/ppbar/param_fits/errMassTzDist_shiftFix_MC.root","READ")
    hJpsi = f.Get("h_mass_jpsi")
    #hEtac = f.Get("h_mass_etac")
    hSigma = f.Get("h_sigma_mass_MC")
    #rScEtac = (1+hEtac.GetBinContent(nTz))
    rScJpsi = (1+hJpsi.GetBinContent(nTz))
    rScSigma *= hSigma.GetBinContent(nTz)
    f.Close()
    
    
    if (nConf == 1):
        f = TFile(homeDir+"Results/MC/MassFit/parFit_GeV.root","READ")
        fSigma = f.Get("fSigmaMC")
        f.Close()
        ptL = binningDict["Jpsi_PT"][nPT-1]
        ptR = binningDict["Jpsi_PT"][nPT]
        rScSigma = fSigma.Eval((ptR+ptL)/2000.)/sigma
        
        f = TFile("/users/LHCb/zhovkovska/ppbar/param_fits/errMassTzDist_shiftFix_MC.root","READ")
        fSigmaTz = f.Get("fSigmaTz")
        tzL = binningDict["Jpsi_Tz"][nTz-1]
        tzR = binningDict["Jpsi_Tz"][nTz]
        rScSigma *= fSigmaTz.Eval((tzL+tzR)/2.)
        f.Close()
        print ("SETUP 1")
    elif (nConf == 2):
        f = TFile(homeDir+"Results/MC/MassFit/parFit_GeV.root","READ") 
        func = f.Get("fRel")
        f.Close()
        ptL = binningDict["Jpsi_PT"][nPT-1]
        ptR = binningDict["Jpsi_PT"][nPT]
        rEtaToJpsi = func.Eval((ptR+ptL)/2000.)
        #rEtaToJpsi = wMC.var("rEtaToJpsi").getValV()+wMC.var("rEtaToJpsi").getError()
        print ("SETUP 2")      
    elif (nConf == 3):
        rNtoW = 1.0
        rArea = 1.0
        print ("SETUP 3")
    elif (nConf == 4):
        bkgOpt = 1
        print ("SETUP 4")
    elif (nConf == 5):
        bkgOpt = 2
        print ("SETUP 5")
    elif (nConf == 6):
        gamma = 34.0
        print ("SETUP 6")
    elif (nConf == 7):
        f = TFile(homeDir+"Results/MassFit/prompt/Sim/Etac_Wksp_MassFit_PT0_C0_total.root","READ") 
        w = f.Get("w")
        f.Close()
        rScJpsi = w.var("rNormJpsi_Tz%s"%(nTz)).getValV()
        print ("SETUP 7")
    elif (nConf == 8):
        #effic = 0.032
        effic = 0.057*1.19/2.12*(1+0.09)
        print ("SETUP 8")
    elif (nConf == 9):
        #effic = 0.032
        rScJpsi = 1
        print ("SETUP 9")
    elif (nConf == 10):
        #gamma = 19.1
        gamma = 31.8
        print ("SETUP 6")

    #if (nPT != 0):
        #nameFile = "results/MC/MassFit/MC_Mass_2016_wksp_PT%d.root"%(nPT)
        #f = TFile(nameFile,"READ") 
        #wMC = f.Get("w;1")
        #f.Close()
        #if (nConf == 1):
            #ratioEtaToJpsi = wMC.var("rEtaToJpsi").getValV()+wMC.var("rEtaToJpsi").getError()
        #else:
            #ratioEtaToJpsi = wMC.var("rEtaToJpsi").getValV()   
    
    return rNtoW, rEtaToJpsi, rArea, bkgOpt, gamma, effic, rScJpsi, rScSigma


def fillRelWorkspace(w, nConf, nPT, nTz):
    
    Jpsi_M = RooRealVar("Jpsi_M","Jpsi_M",2850,3250) 
    Jpsi_M.setBins(2000,"cache")
    Jpsi_M.setRange("SBLeft", 2850, 2900)
    Jpsi_M.setRange("SBCentral", 3035, 3060)
    Jpsi_M.setRange("SBRight", 3150, 3250)
    Jpsi_M.setRange("SignalEtac", 2900, 3035)
    Jpsi_M.setRange("SignalJpsi", 3060, 3150)
    Jpsi_M.setRange("Total", 2850, 3250)
    
    mDiffEtac = 113.501
    meanJpsi = 3096.9
    meanEtac = 3083.4
    
    print type(nConf), type(nPT), type(nTz)
    ratioNtoW,ratioEtaToJpsi, ratioArea, bkgType, gammaEtac, eff, rScJpsi, rScSigma = getConstPars(nConf, nPT, nTz)
    
    
    if (nConf==10):
        Jpsi_M.setRange(2855,3245)
    
    
        
    rEtaToJpsi = RooRealVar("rEtaToJpsi","rEtaToJpsi", ratioEtaToJpsi)
    #rNarToW = RooRealVar("rNarToW","rNarToW",ratioNtoW)
    #rG1toG2 = RooRealVar("rG1toG2","rG1toG2",ratioArea)
    #eff_pppi0 = RooRealVar("eff_pppi0","eff_pppi0",eff)
    #rEtaToJpsi = RooRealVar("rEtaToJpsi","rEtaToJpsi", ratioEtaToJpsi)
    rNarToW = RooRealVar("rNarToW","rNarToW",ratioNtoW)
    rG1toG2 = RooRealVar("rG1toG2","rG1toG2",ratioArea)
    eff_pppi0 = RooRealVar("eff_pppi0","eff_pppi0",eff)
    nEta = RooRealVar("nEta_PT%s_Tz%s"%(nPT,nTz),"num of Etac", -5e2, 1.e7)
    nJpsi = RooRealVar("nJpsi_PT%s_Tz%s"%(nPT,nTz),"num of J/Psi", -5e2, 1.e7)
    nEtacRel = RooRealVar("nEtacRel_PT%s_Tz%s"%(nPT,nTz),"num of Etac", 0.0, 3.0)
    #nEtacRel = RooRealVar("nEtacRel_PT%s_Tz%s"%(nPT,nTz),"nEtacRel", "@0/@1",RooArgList(nEta,nJpsi))
    
    #nEta_1 = RooFormulaVar("nEta_1_PT%s_Tz%s"%(nPT,nTz),"num of Etac","@0*@1*@2",RooArgList(nEtacRel,nJpsi,rG1toG2))
    #nEta_2 = RooFormulaVar("nEta_2_PT%s_Tz%s"%(nPT,nTz),"num of Etac","@0*@1-@2",RooArgList(nEtacRel,nJpsi,nEta_1))
    nEta_1 = RooFormulaVar("nEta_1_PT%s_Tz%s"%(nPT,nTz),"num of Etac","@0-@1",RooArgList(nEta,rG1toG2))
    nEta_2 = RooFormulaVar("nEta_2_PT%s_Tz%s"%(nPT,nTz),"num of Etac","@0-@1",RooArgList(nEta,nEta_1))
    nJpsi_1 = RooFormulaVar("nJpsi_1_PT%s_Tz%s"%(nPT,nTz),"num of J/Psi","@0*@1",RooArgList(nJpsi,rG1toG2))
    nJpsi_2 = RooFormulaVar("nJpsi_2_PT%s_Tz%s"%(nPT,nTz),"num of J/Psi","@0-@1",RooArgList(nJpsi,nJpsi_1))
    nBckgr = RooRealVar("nBckgr_PT%s_Tz%s"%(nPT,nTz),"num of backgr",7e7,10,1.e+9)
    
    rNormJpsi = RooRealVar("rNormJpsi_PT%s_Tz%s"%(nPT,nTz), "rNormJpsi", rScJpsi, 0.99, 1.01); rNormJpsi.setConstant(True)
    #rNormEtac = RooRealVar("rNormEtac_PT%s_Tz%s"%(nPT,nTz), "rNormEtac", rScEtac); rNormEtac.setConstant()
    rNormSigma = RooRealVar("rNormSigma_PT%s_Tz%s"%(nPT,nTz), "rNormSigma", rScSigma, 0.7, 1.3); rNormSigma.setConstant(True)
    #rNormSigma = RooRealVar("rNormSigma_PT%s_Tz%s"%(nPT,nTz), "rNormSigma", 1.0); rNormSigma.setConstant(True)
         
    
    mass_Jpsi = RooRealVar("mass_Jpsi","mean of gaussian",meanJpsi, 3030, 3150) 
    mass_res = RooRealVar("mass_res","mean of gaussian",mDiffEtac, 100, 130) 
    

    mass_eta = RooFormulaVar("mass_eta","mean of gaussian","(@0-@1)",RooArgList(mass_Jpsi,mass_res)) 

    mass_Jpsi_sc = RooFormulaVar("mass_Jpsi_PT%s_Tz%s"%(nPT,nTz),"mean of gaussian", "@0*@1", RooArgList(mass_Jpsi,rNormJpsi)) 
    #mass_res_sc = RooFormulaVar("mass_res_PT%s_Tz%s"%(nPT,nTz),"mean of gaussian", "@0*@1-@2*@3", RooArgList(mass_Jpsi,rNormJpsi, mass_res, rNormEtac)) 
    #mass_eta_sc = RooFormulaVar("mass_eta_PT%s_Tz%s"%(nPT,nTz),"mean of gaussian","(@0-@1)*@2",RooArgList(mass_Jpsi,mass_res, rNormEtac)) 
    mass_eta_sc = RooFormulaVar("mass_eta_PT%s_Tz%s"%(nPT,nTz),"mean of gaussian","(@0-@1)",RooArgList(mass_Jpsi_sc,mass_res)) 

    gamma_eta = RooRealVar("gamma_eta","width of Br-W", gammaEtac, 10., 50.)
    #if nConf!=6:
    gamma_eta.setConstant()
    spin_eta = RooRealVar("spin_eta","spin_eta", 0. )
    radius_eta = RooRealVar("radius_eta","radius", 1.)
    proton_m = RooRealVar("proton_m","proton mass", 938.3 )
    
    sigma_eta = RooRealVar("sigma_eta_1","width of gaussian", 9., 5., 50.) 
    sigma_eta_1 = RooFormulaVar("sigma_eta_1_PT%s_Tz%s"%(nPT,nTz),"width of gaussian","@0*@1",RooArgList(sigma_eta,rNormSigma))
    sigma_eta_2 = RooFormulaVar("sigma_eta_2_PT%s_Tz%s"%(nPT,nTz),"width of gaussian","@0/@1",RooArgList(sigma_eta_1,rNarToW))
    
    sigma_Jpsi_1 = RooFormulaVar("sigma_Jpsi_1_PT%s_Tz%s"%(nPT,nTz),"width of gaussian","@0/@1",RooArgList(sigma_eta_1,rEtaToJpsi))
    sigma_Jpsi_2 = RooFormulaVar("sigma_Jpsi_2_PT%s_Tz%s"%(nPT,nTz),"width of gaussian","@0/@1",RooArgList(sigma_Jpsi_1,rNarToW))
    
    
    # Fit eta
    gaussEta_1 = RooGaussian("gaussEta_1_PT%s_Tz%s"%(nPT,nTz),"gaussEta_1 PDF", Jpsi_M, RooFit.RooConst(0),  sigma_eta_1)
    gaussEta_2 = RooGaussian("gaussEta_2_PT%s_Tz%s"%(nPT,nTz),"gaussEta_2 PDF", Jpsi_M, RooFit.RooConst(0),  sigma_eta_2)
        
    br_wigner = RooRelBreitWigner("br_wigner_PT%s_Tz%s"%(nPT,nTz), "br_wigner",Jpsi_M, mass_eta_sc, gamma_eta, spin_eta,radius_eta,proton_m,proton_m)
    
    bwxg_1 = RooFFTConvPdf("bwxg_1_PT%s_Tz%s"%(nPT,nTz),"breit-wigner (X) gauss", Jpsi_M, br_wigner, gaussEta_1) 
    bwxg_2 = RooFFTConvPdf("bwxg_2_PT%s_Tz%s"%(nPT,nTz),"breit-wigner (X) gauss", Jpsi_M, br_wigner, gaussEta_2) 
        
    # Fit J/psi
    gauss_1 = RooGaussian("gauss_1_PT%s_Tz%s"%(nPT,nTz),"gaussian PDF",Jpsi_M,mass_Jpsi_sc,sigma_Jpsi_1) 
    gauss_2 = RooGaussian("gauss_2_PT%s_Tz%s"%(nPT,nTz),"gaussian PDF",Jpsi_M,mass_Jpsi_sc,sigma_Jpsi_2) 
        

    #Connection between parameters
    #    RooFormulaVar f_1("f_1","f_1","@0*9.0",RooArgList(nEta_2))
    
    #Create constraints
    #    RooGaussian constrNEta("constrNEta","constraint Etac",nEta_1,f_1,RooConst(0.0)) 
    fconstraint = RooGaussian("fconstraint","fconstraint",mass_res, RooFit.RooConst(113.5), RooFit.RooConst(0.5))  #PDG 
    fconstrJpsi = RooGaussian("fconstrJpsi","fconstraint",mass_Jpsi, RooFit.RooConst(3096.9),RooFit.RooConst(0.5)) 
    
#    bkg = RooChebychev()
    a0 = RooRealVar("a0_PT%s_Tz%s"%(nPT,nTz),"a0",0.4,-1,2) 
    a1 = RooRealVar("a1_PT%s_Tz%s"%(nPT,nTz),"a1",0.05,-1.5,1.5) 
    a2 = RooRealVar("a2_PT%s_Tz%s"%(nPT,nTz),"a2",-0.005,-1.5,1.5) 
    a3 = RooRealVar("a3_PT%s_Tz%s"%(nPT,nTz),"a3",0.005,-0.1,0.1) 
    a4 = RooRealVar("a4_PT%s_Tz%s"%(nPT,nTz),"a4",0.005,-0.1,0.1) 
    
    if (bkgType == 0):
        #a0.setRange(0.0, 5.e2)
        a1.setVal(0.04)
        a2.setVal(0.04)
        if nTz!=1:
            bkg = RooGenericPdf("bkg_PT%s_Tz%s"%(nPT,nTz),"background","TMath::Exp(-(@0-3050.)/200.*@1)*(1.+@2*(@0-3050.)/200.+@3*(@0-3050.)*(@0-3050.)/200./200.)",RooArgList(Jpsi_M,a0,a1,a2)) 
        else:
            bkg = RooGenericPdf("bkg_PT%s_Tz%s"%(nPT,nTz),"background","TMath::Exp(-(@0-3050.)/200.*@1)*(1.+@2*(@0-3050.)/200)",RooArgList(Jpsi_M,a0,a1)) 
        
        #bkg = RooGenericPdf("bkg","background","TMath::Exp(-(@0-3050.)/200.*@1)*(1.+@2*@0.+@3*(2*@0*@0-1))",RooArgList(Jpsi_M,a0,a1,a2)) 
    elif (bkgType == 1):
        bkg = RooChebychev ("bkg_PT%s_Tz%s"%(nPT,nTz),"Background",Jpsi_M,RooArgList(a0,a1,a2)) 
    elif (bkgType == 2):
        bkg = RooChebychev("bkg_PT%s_Tz%s"%(nPT,nTz),"Background",Jpsi_M,RooArgList(a0,a1,a2,a3))
        
    
    pppi0 = RooGenericPdf("pppi0_PT%s_Tz%s"%(nPT,nTz),"Jpsi.pppi0","@0<(2961.92) ? TMath::Sqrt(3096.900-134.977-@0) : 0",RooArgList(Jpsi_M))
    nPPPi0 = RooFormulaVar("nPPPi0_PT%s_Tz%s"%(nPT,nTz),"nPPPi0","@0*@1",RooArgList(nJpsi,eff_pppi0))
    
    if (nConf!=3):

        modelBkg = RooAddPdf("modelBkg_PT%s_Tz%s"%(nPT,nTz),"background", RooArgList(bkg,pppi0), RooArgList(nBckgr,nPPPi0))
        modelSignal = RooAddPdf("modelSignal_PT%s_Tz%s"%(nPT,nTz),"signal", RooArgList(bwxg_1, bwxg_2,gauss_1,gauss_2), RooArgList(nEta_1,nEta_2,nJpsi_1,nJpsi_2))
        model = RooAddPdf("model_PT%s_Tz%s"%(nPT,nTz),"signal+bkg", RooArgList(bwxg_1, bwxg_2,gauss_1,gauss_2, bkg,pppi0), RooArgList(nEta_1,nEta_2,nJpsi_1,nJpsi_2, nBckgr,nPPPi0))
                
        #modelC = RooProdPdf("modelC", "model with constraints",RooArgList(model,fconstraint,fconstrJpsi))
        modelC = RooProdPdf("modelC_PT%s_Tz%s"%(nPT,nTz), "model with constraints",RooArgList(model,fconstraint))
        
    else:
        
        f = TFile(homeDir+"Results/MC/MassFit/MC_MassResolution_CB_2016_wksp.root","READ") 
        wMC = f.Get("w")
        f.Close()
        
        alpha_eta_1 = RooRealVar('alpha_eta_1','alpha of CB', wMC.var('alpha_eta_1').getValV(), 0.0, 10.) 
        alpha_eta_1.setConstant(True)
        n_eta_1 = RooRealVar('n_eta_1','n of CB', wMC.var('n_eta_1').getValV(), 0.0, 100.) 
        n_eta_1.setConstant(True)    

        # Fit eta
        cb_etac_1 = BifurcatedCB("cb_etac_1_PT%s_Tz%s"%(nPT,nTz), "Cystal Ball Function", Jpsi_M, RooFit.RooConst(0), sigma_eta_1, alpha_eta_1, n_eta_1, alpha_eta_1, n_eta_1)
        br_wigner = RooRelBreitWigner("br_wigner_Tz%s"%(nTz), "br_wigner",Jpsi_M, mass_eta_sc, gamma_eta, spin_eta,radius_eta,proton_m,proton_m)
            
        bwxg_1 = RooFFTConvPdf("bwxg_1_PT%s_Tz%s"%(nPT,nTz),"breit-wigner (X) gauss", Jpsi_M, br_wigner, cb_etac_1) 
        
        # Fit J/psi
        cb_Jpsi_1 = BifurcatedCB("cb_Jpsi_1_PT%s_Tz%s"%(nPT,nTz), "Cystal Ball Function", Jpsi_M, mass_Jpsi_sc, sigma_Jpsi_1, alpha_eta_1, n_eta_1, alpha_eta_1, n_eta_1)

        modelBkg = RooAddPdf("modelBkg_PT%s_Tz%s"%(nPT,nTz),"background", RooArgList(bkg,pppi0), RooArgList(nBckgr,nPPPi0))
        modelSignal = RooAddPdf("modelSignal_PT%s_Tz%s"%(nPT,nTz),"signal", RooArgList(bwxg_1, cb_Jpsi_1), RooArgList(nEta,nJpsi))
        model = RooAddPdf("model_PT%s_Tz%s"%(nPT,nTz),"signal+bkg", RooArgList(bwxg_1, cb_Jpsi_1, bkg,pppi0), RooArgList(nEta, nJpsi, nBckgr, nPPPi0))
        
        #modelC = RooProdPdf("modelC", "model with constraints",RooArgList(model,fconstraint,fconstrJpsi))
        modelC = RooProdPdf("modelC_PT%s_Tz%s"%(nPT,nTz), "model with constraints",RooArgList(model,fconstraint))
    

    getattr(w,'import')(model, RooFit.RecycleConflictNodes())
    getattr(w,'import')(modelC, RooFit.RecycleConflictNodes())
    getattr(w,'import')(modelBkg, RooFit.RecycleConflictNodes())
    getattr(w,'import')(modelSignal, RooFit.RecycleConflictNodes())

    return model