from ROOT import *
gROOT.LoadMacro("../libRooRelBreitWigner/RooRelBreitWigner.cxx+")
gROOT.LoadMacro("../BifurcatedCB/BifurcatedCB.cxx+")

homeDir = "/users/LHCb/usachov/zhovkovska2018/scripts/"

binningDict = {
    "Jpsi_PT":                      [6500, 8000, 10000, 12000, 14000, 18000],
    "Jpsi_ETA":                     [2.0, 2.45, 2.8, 3.15, 4.5],
    "TMath::Abs(ProtonM_CosTheta)": [0.0, 0.15, 0.3, 0.50, 1.0],
    "nSPDHits":                     [0, 100, 150, 200, 250, 300],
}



# 
minM = 2850
maxM = 3250
nBins = 1000

def getConstPars(nConf, nPT=0):

    f = TFile("../resolutions/resoTzCut_log/MC_MassResolution_Integr_wksp.root","READ") 
    wMC = f.Get("w")
    f.Close()
    
    rNtoW = wMC.var("rNarToW").getValV()
    rEtaToJpsi = wMC.var("rEtaToJpsi").getValV()
    rArea = wMC.var("rG1toG2").getValV()
    bkgOpt = 'Base'
    gamma = 31.8
    effic = 0.057*1.19/2.12
    #gamma = 21.3
    #effic = 0.06
    #ratioNtoW = 0.5
    #ratioEtaToJpsi = 0.88
    #ratioArea = 0.9
    rScSigma = 1.0
    
    if (nConf == 1):
        #rNtoW = wMC.var("rNarToW").getValV()+wMC.var("rNarToW").getError()
        f = TFile(homeDir+"Results/MC/MassFit/parFit%s_GeV.root"%(add),"READ")
        fSigma = f.Get("fSigmaMC")
        f.Close()
        ptL = binningDict["Jpsi_PT"][nPT-1]
        ptR = binningDict["Jpsi_PT"][nPT]
        rScSigma = fSigma.Eval((ptR+ptL)/2000.)/sigma
    elif (nConf == 'rEtaToJpsi'):
        f = TFile("../Results/MC/MassFit/parFit_GeV.root","READ") 
        func = f.Get("fRel")
        f.Close()
        if nPT==0:
            rEtaToJpsi = wMC.var("rEtaToJpsi").getValV()+wMC.var("rEtaToJpsi").getError()
        else:
            ptL = binningDict["Jpsi_PT"][nPT-1]
            ptR = binningDict["Jpsi_PT"][nPT]
            rEtaToJpsi = func.Eval((ptR+ptL)/2000.)    
    elif (nConf == 'CB'):
        rNtoW = 1.0
        rArea = 1.0
    elif (nConf == 'Chebychev3par' or nConf == 'Chebychev4par'):
        bkgOpt = nConf
    elif (nConf == 'Gamma34'):
        gamma = 34.0
    elif (nConf == 'eff_pppi0'):
        effic = 0.057*1.19/2.12*(1.+0.09)
    print ("SETUP ",nConf)

    #if (nPT != 0):
        #nameFile = "results/MC/MassFit/MC_Mass_2016_wksp_PT%d.root"%(nPT)
        #f = TFile(nameFile,"READ") 
        #wMC = f.Get("w;1")
        #f.Close()
        #if (nConf == 1):
            #ratioEtaToJpsi = wMC.var("rEtaToJpsi").getValV()+wMC.var("rEtaToJpsi").getError()
        #else:
            #ratioEtaToJpsi = wMC.var("rEtaToJpsi").getValV()   
    
    return rNtoW,rEtaToJpsi,rArea, bkgOpt, gamma, effic




def fillRelWorkspace(w, nConf, nPT, key):

    
    Jpsi_M = RooRealVar("Jpsi_M","Jpsi_M",2850,3250) 
    #Jpsi_M = w.var("Jpsi_M") 
    Jpsi_M.setBins(1000,"cache")
    
    mDiffEtac = 113.501
    
    ratioNtoW,ratioEtaToJpsi,ratioArea, bkgType, gammaEtac, eff = getConstPars(nConf, nPT)
    
    
    if (nConf=='halfBinShift'):
        Jpsi_M.setRange(2855,3245)
    
    
    rEtaToJpsi = RooRealVar("rEtaToJpsi","rEtaToJpsi", ratioEtaToJpsi)
    rNarToW = RooRealVar("rNarToW","rNarToW",ratioNtoW)
    rG1toG2 = RooRealVar("rG1toG2","rG1toG2",ratioArea)
    eff_pppi0 = RooRealVar("eff_pppi0","eff_pppi0",eff)

    nEta = RooRealVar("nEta_%s"%(key),"num of Etac", 10, 1.e7)
    nJpsi = RooRealVar("nJpsi_%s"%(key),"num of J/Psi", 10, 1.e7)
    nEtacRel = RooRealVar("nEtacRel_%s"%(key),"num of Etac", 0.0, 3.0)
    #nEtacRel = RooRealVar("nEtacRel_%s"%(key),"nEtacRel", "@0/@1",RooArgList(nEta,nJpsi))
    
    #nEta_1 = RooFormulaVar("nEta_1_%s"%(key),"num of Etac","@0*@1*@2",RooArgList(nEtacRel,nJpsi,rG1toG2))
    #nEta_2 = RooFormulaVar("nEta_2_%s"%(key),"num of Etac","@0*@1-@2",RooArgList(nEtacRel,nJpsi,nEta_1))
    nEta_1 = RooFormulaVar("nEta_1_%s"%(key),"num of Etac","@0-@1",RooArgList(nEta,rG1toG2))
    nEta_2 = RooFormulaVar("nEta_2_%s"%(key),"num of Etac","@0-@1",RooArgList(nEta,nEta_1))
    nJpsi_1 = RooFormulaVar("nJpsi_1_%s"%(key),"num of J/Psi","@0*@1",RooArgList(nJpsi,rG1toG2))
    nJpsi_2 = RooFormulaVar("nJpsi_2_%s"%(key),"num of J/Psi","@0-@1",RooArgList(nJpsi,nJpsi_1))
    nBckgr = RooRealVar("nBckgr_%s"%(key),"num of backgr",7e7,10,1.e+9)
    
    
    mass_Jpsi = RooRealVar("mass_Jpsi","mean of gaussian", 3096.9, 3030, 3150) 
    mass_res = RooRealVar("mass_res","mean of gaussian",mDiffEtac, 80, 130) 
    
    if (nConf=='constDM' or nConf==8):  mass_res.setConstant(True)

    mass_eta = RooFormulaVar("mass_eta","mean of gaussian","@0-@1",RooArgList(mass_Jpsi,mass_res)) 
    gamma_eta = RooRealVar("gamma_eta","width of Br-W", gammaEtac, 10., 50.)
    gamma_eta.setConstant()
    spin_eta = RooRealVar("spin_eta","spin_eta", 0. )
    radius_eta = RooRealVar("radius_eta","radius", 1.)
    proton_m = RooRealVar("proton_m","proton mass", 938.3 )
    sigma_eta_1 = RooRealVar("sigma_eta_1_PT%s"%(nPT),"width of gaussian", 9., 5., 50.) 
    sigma_eta_2 = RooFormulaVar("sigma_eta_2_PT%s"%(nPT),"width of gaussian","@0/@1",RooArgList(sigma_eta_1,rNarToW))
    
    sigma_Jpsi_1 = RooFormulaVar("sigma_Jpsi_1_PT%s"%(nPT),"width of gaussian","@0/@1",RooArgList(sigma_eta_1,rEtaToJpsi))
    sigma_Jpsi_2 = RooFormulaVar("sigma_Jpsi_2_PT%s"%(nPT),"width of gaussian","@0/@1",RooArgList(sigma_Jpsi_1,rNarToW))
    
    
    # Fit eta
    gaussEta_1 = RooGaussian("gaussEta_1_PT%s"%(nPT),"gaussEta_1 PDF", Jpsi_M, RooFit.RooConst(0),  sigma_eta_1)
    gaussEta_2 = RooGaussian("gaussEta_2_PT%s"%(nPT),"gaussEta_2 PDF", Jpsi_M, RooFit.RooConst(0),  sigma_eta_2)
    
    br_wigner = RooRelBreitWigner("br_wigner", "br_wigner",Jpsi_M, mass_eta, gamma_eta, spin_eta,radius_eta,proton_m,proton_m)
    
    bwxg_1 = RooFFTConvPdf("bwxg_1_PT%s"%(nPT),"breit-wigner (X) gauss", Jpsi_M, br_wigner, gaussEta_1) 
    bwxg_2 = RooFFTConvPdf("bwxg_2_PT%s"%(nPT),"breit-wigner (X) gauss", Jpsi_M, br_wigner, gaussEta_2) 
    
    # Fit J/psi
    gauss_1 = RooGaussian("gauss_1_PT%s"%(nPT),"gaussian PDF",Jpsi_M, mass_Jpsi, sigma_Jpsi_1) 
    gauss_2 = RooGaussian("gauss_2_PT%s"%(nPT),"gaussian PDF",Jpsi_M, mass_Jpsi, sigma_Jpsi_2) 
    

    if (bkgType == 'Base'):
        a0 = RooRealVar("a0_%s"%(key),"a0",0.4,0.,1.)
    else:
        a0 = RooRealVar("a0_%s"%(key),"a0",0.4,-2.,2.)
        
    a1 = RooRealVar("a1_%s"%(key),"a1",0.05,-1.,1.) 
    a2 = RooRealVar("a2_%s"%(key),"a2",-0.005,-1.,1.) 
    a3 = RooRealVar("a3_%s"%(key),"a3",0.005,-0.1,0.1) 
    a4 = RooRealVar("a4_%s"%(key),"a4",0.005,-0.1,0.1) 
    

    if (bkgType == 'Base'):
        #a0.setRange(0.0, 5.e2)
        a1.setVal(0.04)
        a2.setVal(0.04)
        bkg = RooGenericPdf("bkg_%s"%(key),"background","TMath::Exp(-(@0-3050.)/200.*@1)*(1.+@2*(@0-3050.)/200.+@3*(@0-3050.)*(@0-3050.)/200./200.)",RooArgList(Jpsi_M,a0,a1,a2)) 
        #bkg = RooGenericPdf("bkg","background","TMath::Exp(-(@0-3050.)/200.*@1)*(1.+@2*@0.+@3*(2*@0*@0-1))",RooArgList(Jpsi_M,a0,a1,a2)) 
    elif (bkgType == 'Chebychev3par'):
        bkg = RooChebychev ("bkg_%s"%(key),"Background",Jpsi_M,RooArgList(a0,a1,a2)) 
    elif (bkgType == 'Chebychev4par'):
        bkg = RooChebychev("bkg_%s"%(key),"Background",Jpsi_M,RooArgList(a0,a1,a2,a3))
        
    
    pppi0 = RooGenericPdf("pppi0","Jpsi2pppi0","@0<(2961.92) ? TMath::Sqrt(3096.900-134.977-@0) : 0",RooArgList(Jpsi_M))
    nPPPi0 = RooFormulaVar("nPPPi0_%s"%(key),"nPPPi0","@0*@1",RooArgList(nJpsi,eff_pppi0))
    
    
    modelBkg = RooAddPdf("modelBkg_%s"%(key),"background", RooArgList(bkg,pppi0), RooArgList(nBckgr,nPPPi0))


    if(nConf=='CB'):
        f = TFile("../resolutions/resoTzCut_log/MC_MassResolution_CB_Integr_wksp.root","READ") 
        wMC = f.Get("w")
        f.Close()
        alpha_eta_1 = RooRealVar('alpha_eta_1','alpha of CB', wMC.var('alpha_eta_1').getValV(), 0.0, 10.) 
        alpha_eta_1.setConstant(True)
        n_eta_1 = RooRealVar('n_eta_1','n of CB', wMC.var('n_eta_1').getValV(), 0.0, 100.) 
        n_eta_1.setConstant(True)       
        # Fit eta
        cb_etac_1 = BifurcatedCB("cb_etac_1_PT%s"%(nPT), "Cystal Ball Function", Jpsi_M, RooFit.RooConst(0), sigma_eta_1, alpha_eta_1, n_eta_1, alpha_eta_1, n_eta_1)   
        bwxg_1 = RooFFTConvPdf("bwxg_1_PT%s"%(nPT),"breit-wigner (X) gauss", Jpsi_M, br_wigner, cb_etac_1) 
        # Fit J/psi
        cb_Jpsi_1 = BifurcatedCB("cb_Jpsi_1_PT%s"%(nPT), "Cystal Ball Function", Jpsi_M, mass_Jpsi, sigma_Jpsi_1, alpha_eta_1, n_eta_1, alpha_eta_1, n_eta_1)
        modelSignal = RooAddPdf("modelSignal_%s"%(key),"signal", RooArgList(bwxg_1,cb_Jpsi_1), RooArgList(nEta,nJpsi))
        model = RooAddPdf("model_%s"%(key),"signal+bkg", RooArgList(bwxg_1,cb_Jpsi_1, bkg,pppi0), RooArgList(nEta,nJpsi, nBckgr,nPPPi0))
    else:
        modelSignal = RooAddPdf("modelSignal_%s"%(key),"signal", RooArgList(bwxg_1, bwxg_2,gauss_1,gauss_2), RooArgList(nEta_1,nEta_2,nJpsi_1,nJpsi_2))
        model = RooAddPdf("model_%s"%(key),"signal+bkg", RooArgList(bwxg_1, bwxg_2,gauss_1,gauss_2, bkg,pppi0), RooArgList(nEta_1,nEta_2,nJpsi_1,nJpsi_2, nBckgr,nPPPi0))
    
    
    #modelC = RooProdPdf("modelC", "model with constraints",RooArgList(model,fconstraint,fconstrJpsi))
    #modelC = RooProdPdf("modelC_%s"%(key), "model with constraints",RooArgList(model,fconstraint))

    getattr(w,'import')(model, RooFit.RecycleConflictNodes())
    #getattr(w,'import')(modelC, RooFit.RecycleConflictNodes())
    getattr(w,'import')(modelBkg, RooFit.RecycleConflictNodes())
    getattr(w,'import')(modelSignal, RooFit.RecycleConflictNodes())
