from ROOT import *
from DrawMass import *
gROOT.LoadMacro("../libRooRelBreitWigner/RooRelBreitWigner.cxx+")
gROOT.LoadMacro("../BifurcatedCB/BifurcatedCB.cxx+")



histosDir = "Histos/m_scaled/"
homeDir = "/users/LHCb/zhovkovska/scripts/"

binningDict = {
    "Jpsi_PT":                      [6500, 8000, 10000, 12000, 14000, 18000],
    "Jpsi_ETA":                     [2.0, 2.45, 2.8, 3.15, 4.5],
    "TMath::Abs(ProtonM_CosTheta)": [0.0, 0.15, 0.3, 0.50, 1.0],
    "nSPDHits":                     [0, 100, 150, 200, 250, 300],
    "Jpsi_Tz":                      [-10., -0.125, -0.025, 0.0, 0.20, 2.0, 4.0, 10.0],
    "Jpsi_Tz_Old":                  [-10., -0.025, 0.0, 0.20, 2.0, 10.0],

}


minM = 2850
maxM = 3250
nBins = 1000


def getData_h(w, keyPrompt=True, nPT=0, nTz=0, charm='Etac', shift=False):
       
    
    if keyPrompt: inCutKey = "all_l0TOS"
    else:         inCutKey = "secondary_l0TOS"
    
    if nPT!=0:    
        if nTz!=0: binPT = "Jpsi_PT/"+charm+"/bin%s_"%(nPT)   #!!!!!!!!!!!!!
        else:      binPT = "Jpsi_PT/bin%s_"%(nPT) 
    else:         binPT = "Total/"

    if nPT!=0:    binTz = "Tz%s.root"%(nTz)
    else:         binTz = "Tz%s_5Bins.root"%(nTz)
    
    dirName_2015 = homeDir+"Histos/m_scaled/2015/DiProton/" + inCutKey + "/"
    dirName_2016 = homeDir+"Histos/m_scaled/2016/DiProton/" + inCutKey + "/"
        
    nameFile = dirName_2015 + binPT + binTz
    f_2015 = TFile(nameFile,"read")
    print nameFile
    nameFile = dirName_2016 + binPT + binTz
    f_2016 = TFile(nameFile,"read")
    print nameFile
    
    
    hh = TH1D("hh","hh", nBins, minM, maxM)
    hh = f_2015.Get("Jpsi_M")
    hh.Add(f_2016.Get("Jpsi_M"))
    
    Jpsi_M = w.var("Jpsi_M")
    #Jpsi_M = RooRealVar("Jpsi_M","Jpsi_M",2850,3250) 

    if(shift):
        hh.SetBins(nBins, minM+(maxM-minM)/(2.*float(nBins)),maxM+(maxM-minM)/(2.*float(nBins)))
        Jpsi_M.setRange(minM+(maxM-minM)/(2.*float(nBins)), maxM+(maxM-minM)/(2.*float(nBins))) 

    dh = RooDataHist("dh_PT%s_Tz%s"%(nPT,nTz),"dh_PT%s_Tz%s"%(nPT,nTz), RooArgList(Jpsi_M) ,hh)
    getattr(w,'import')(dh, RooFit.RecycleConflictNodes())
    f_2015.Close()
    f_2016.Close()
    
    #print "DATA\'S READ SUCCESSFULLY"



def getConstPars(nConf, nPT=0, nTz=0):
    
    if nConf==3: add="_CB"
    else:        add=""
    
    f = TFile(homeDir+"Results/MC/MassFit/MC_MassResolution_2016_wksp.root","READ") 
    wMC = f.Get("w")
    f.Close()
    
    rNtoW = wMC.var("rNarToW").getValV()
    rEtaToJpsi = wMC.var("rEtaToJpsi").getValV()
    rArea = wMC.var("rG1toG2").getValV()
    sigma = wMC.var("sigma_eta_1").getValV()
    bkgOpt = 0
    gamma = 31.8    
    #effic = 0.035
    #gamma = 21.3
    effic = 0.057*1.19/2.12
    #ratioNtoW = 0.5
    #ratioEtaToJpsi = 0.88
    #ratioArea = 0.9
    
    if nPT !=0 : 
        f = TFile(homeDir+"Results/MC/MassFit/parFit%s_GeV.root"%(add),"READ")
        fSigma = f.Get("fSigmaMC")
        f.Close()
        ptL = binningDict["Jpsi_PT"][nPT-1]
        ptR = binningDict["Jpsi_PT"][nPT]
        rScSigma = fSigma.Eval((ptR+ptL)/2000.)/sigma
    else:
        rScSigma = 1
    
    #f = TFile("/users/LHCb/zhovkovska/ppbar/param_fits/errMassTzDist_shiftFix_MC.root","READ")
    f = TFile("/users/LHCb/zhovkovska/ppbar/param_fits/errMassTzDist_s_m_scaled_7Bins.root","READ")
    hJpsi = f.Get("h_mass_jpsi")
    #hEtac = f.Get("h_mass_etac")
    hSigma = f.Get("h_sigma_mass_MC")
    #rScEtac = (1+hEtac.GetBinContent(nTz))
    rScJpsi = (1+hJpsi.GetBinContent(nTz)) 
    rScSigma *= hSigma.GetBinContent(nTz)
    f.Close()
    
    
    if (nConf == 1):
        f = TFile(homeDir+"Results/MC/MassFit/parFit_GeV.root","READ")
        fSigma = f.Get("fSigmaMC")
        f.Close()
        ptL = binningDict["Jpsi_PT"][nPT-1]
        ptR = binningDict["Jpsi_PT"][nPT]
        rScSigma = fSigma.Eval((ptR+ptL)/2000.)/sigma
        
        f = TFile("/users/LHCb/zhovkovska/ppbar/param_fits/errMassTzDist_shiftFix_MC.root","READ")
        fSigmaTz = f.Get("fSigmaTz")
        tzL = binningDict["Jpsi_Tz"][nTz-1]
        tzR = binningDict["Jpsi_Tz"][nTz]
        rScSigma *= fSigmaTz.Eval((tzL+tzR)/2.)
        f.Close()
        print ("SETUP 1")
    elif (nConf == 2):
        f = TFile(homeDir+"Results/MC/MassFit/parFit_GeV.root","READ") 
        func = f.Get("fRel")
        f.Close()
        ptL = binningDict["Jpsi_PT"][nPT-1]
        ptR = binningDict["Jpsi_PT"][nPT]
        rEtaToJpsi = func.Eval((ptR+ptL)/2000.)
        #rEtaToJpsi = wMC.var("rEtaToJpsi").getValV()+wMC.var("rEtaToJpsi").getError()
        print ("SETUP 2")      
    elif (nConf == 3):
        rNtoW = 1.0
        rArea = 1.0
        print ("SETUP 3")
    elif (nConf == 4):
        bkgOpt = 1
        print ("SETUP 4")
    elif (nConf == 5):
        bkgOpt = 2
        print ("SETUP 5")
    elif (nConf == 6):
        gamma = 34.0
        print ("SETUP 6")
    elif (nConf == 7):
        f = TFile(homeDir+"Results/MassFit/prompt/Sim/Etac_Wksp_MassFit_PT0_C0_total.root","READ") 
        w = f.Get("w")
        f.Close()
        rScJpsi = w.var("rNormJpsi_Tz%s"%(nTz)).getValV()
        print ("SETUP 7")
    elif (nConf == 8):
        #effic = 0.032
        effic = 0.057*1.19/2.12*(1+0.09)
        print ("SETUP 8")
    elif (nConf == 9):
        #effic = 0.032
        rScJpsi = 1
        print ("SETUP 9")
    elif (nConf == 10):
        #gamma = 19.1
        gamma = 31.8
        print ("SETUP 6")

    #if (nPT != 0):
        #nameFile = "results/MC/MassFit/MC_Mass_2016_wksp_PT%d.root"%(nPT)
        #f = TFile(nameFile,"READ") 
        #wMC = f.Get("w;1")
        #f.Close()
        #if (nConf == 1):
            #ratioEtaToJpsi = wMC.var("rEtaToJpsi").getValV()+wMC.var("rEtaToJpsi").getError()
        #else:
            #ratioEtaToJpsi = wMC.var("rEtaToJpsi").getValV()   
    
    return rNtoW,rEtaToJpsi,rArea, bkgOpt, gamma, effic, rScJpsi, rScSigma
    
    
        

def fillRelWorkspace(w, nConf, nPT, nTz):
    
    Jpsi_M = RooRealVar("Jpsi_M","Jpsi_M",2850,3250) 
    Jpsi_M.setBins(2000,"cache")
    Jpsi_M.setRange("SBLeft", 2850, 2900)
    Jpsi_M.setRange("SBCentral", 3035, 3060)
    Jpsi_M.setRange("SBRight", 3150, 3250)
    Jpsi_M.setRange("SignalEtac", 2900, 3035)
    Jpsi_M.setRange("SignalJpsi", 3060, 3150)
    Jpsi_M.setRange("Total", 2850, 3250)
    
    mDiffEtac = 113.501
    meanJpsi = 3096.9
    meanEtac = 3083.4
    
    ratioNtoW,ratioEtaToJpsi,ratioArea, bkgType, gammaEtac, eff, rScJpsi, rScSigma = getConstPars(nConf, nPT, nTz)
    
    
    if (nConf==10):
        Jpsi_M.setRange(2855,3245)
    
    
    rEtaToJpsi = RooRealVar("rEtaToJpsi_PT%s"%(nPT),"rEtaToJpsi", ratioEtaToJpsi)
    #rNarToW = RooRealVar("rNarToW_PT%s"%(nPT),"rNarToW",ratioNtoW)
    #rG1toG2 = RooRealVar("rG1toG2_PT%s"%(nPT),"rG1toG2",ratioArea)
    #eff_pppi0 = RooRealVar("eff_pppi0_PT%s"%(nPT),"eff_pppi0",eff)
    rNarToW = RooRealVar("rNarToW","rNarToW",ratioNtoW)
    rG1toG2 = RooRealVar("rG1toG2","rG1toG2",ratioArea)
    eff_pppi0 = RooRealVar("eff_pppi0","eff_pppi0",eff)
    nEta = RooRealVar("nEta_PT%s_Tz%s"%(nPT,nTz),"num of Etac", -1e2, 1.e7)       #negative yield
    nJpsi = RooRealVar("nJpsi_PT%s_Tz%s"%(nPT,nTz),"num of J/Psi", -1e2, 1.e7)    #negative yield
    nEtacRel = RooRealVar("nEtacRel_PT%s_Tz%s"%(nPT,nTz),"num of Etac", 0.0, 3.0)
    #nEtacRel = RooRealVar("nEtacRel_PT%s_Tz%s"%(nPT,nTz),"nEtacRel", "@0/@1",RooArgList(nEta,nJpsi))
    
    #nEta_1 = RooFormulaVar("nEta_1_PT%s_Tz%s"%(nPT,nTz),"num of Etac","@0*@1*@2",RooArgList(nEtacRel,nJpsi,rG1toG2))
    #nEta_2 = RooFormulaVar("nEta_2_PT%s_Tz%s"%(nPT,nTz),"num of Etac","@0*@1-@2",RooArgList(nEtacRel,nJpsi,nEta_1))
    nEta_1 = RooFormulaVar("nEta_1_PT%s_Tz%s"%(nPT,nTz),"num of Etac","@0-@1",RooArgList(nEta,rG1toG2))
    nEta_2 = RooFormulaVar("nEta_2_PT%s_Tz%s"%(nPT,nTz),"num of Etac","@0-@1",RooArgList(nEta,nEta_1))
    nJpsi_1 = RooFormulaVar("nJpsi_1_PT%s_Tz%s"%(nPT,nTz),"num of J/Psi","@0*@1",RooArgList(nJpsi,rG1toG2))
    nJpsi_2 = RooFormulaVar("nJpsi_2_PT%s_Tz%s"%(nPT,nTz),"num of J/Psi","@0-@1",RooArgList(nJpsi,nJpsi_1))
    nBckgr = RooRealVar("nBckgr_PT%s_Tz%s"%(nPT,nTz),"num of backgr",7e5,10,1.e+9)
    
    rNormJpsi = RooRealVar("rNormJpsi_Tz%s"%(nTz), "rNormJpsi", rScJpsi, 0.99, 1.01); rNormJpsi.setConstant(True)
    #rNormEtac = RooRealVar("rNormEtac_Tz%s"%(nTz), "rNormEtac", rScEtac); rNormEtac.setConstant()
    rNormSigma = RooRealVar("rNormSigma_PT%s_Tz%s"%(nPT,nTz), "rNormSigma", rScSigma); rNormSigma.setConstant(True)
         
    
    mass_Jpsi = RooRealVar("mass_Jpsi","mean of gaussian",meanJpsi, 3030, 3150) 
    mass_res = RooRealVar("mass_res","mean of gaussian",mDiffEtac, 100, 130) 
    

    mass_eta = RooFormulaVar("mass_eta","mean of gaussian","(@0-@1)",RooArgList(mass_Jpsi,mass_res)) 

    mass_Jpsi_sc = RooFormulaVar("mass_Jpsi_Tz%s"%(nTz),"mean of gaussian", "@0*@1", RooArgList(mass_Jpsi,rNormJpsi)) 
    #mass_res_sc = RooFormulaVar("mass_res_Tz%s"%(nTz),"mean of gaussian", "@0*@1-@2*@3", RooArgList(mass_Jpsi,rNormJpsi, mass_res, rNormEtac)) 
    #mass_eta_sc = RooFormulaVar("mass_eta_Tz%s"%(nTz),"mean of gaussian","(@0-@1)*@2",RooArgList(mass_Jpsi,mass_res, rNormEtac)) 
    mass_eta_sc = RooFormulaVar("mass_eta_Tz%s"%(nTz),"mean of gaussian","(@0-@1)",RooArgList(mass_Jpsi_sc,mass_res)) 

    gamma_eta = RooRealVar("gamma_eta","width of Br-W", gammaEtac, 10., 50.)
    gamma_eta.setConstant()
    spin_eta = RooRealVar("spin_eta","spin_eta", 0. )
    radius_eta = RooRealVar("radius_eta","radius", 1.)
    proton_m = RooRealVar("proton_m","proton mass", 938.3 )
    
    sigma_eta = RooRealVar("sigma_eta_1","width of gaussian", 9., 5., 50.) 
    sigma_eta_1 = RooFormulaVar("sigma_eta_1_PT%s_Tz%s"%(nPT,nTz),"width of gaussian","@0*@1",RooArgList(sigma_eta,rNormSigma))
    sigma_eta_2 = RooFormulaVar("sigma_eta_2_PT%s_Tz%s"%(nPT,nTz),"width of gaussian","@0/@1",RooArgList(sigma_eta_1,rNarToW))
    
    sigma_Jpsi_1 = RooFormulaVar("sigma_Jpsi_1_PT%s_Tz%s"%(nPT,nTz),"width of gaussian","@0/@1",RooArgList(sigma_eta_1,rEtaToJpsi))
    sigma_Jpsi_2 = RooFormulaVar("sigma_Jpsi_2_PT%s_Tz%s"%(nPT,nTz),"width of gaussian","@0/@1",RooArgList(sigma_Jpsi_1,rNarToW))
    
    
    # Fit eta
    gaussEta_1 = RooGaussian("gaussEta_1_PT%s_Tz%s"%(nPT,nTz),"gaussEta_1 PDF", Jpsi_M, RooFit.RooConst(0),  sigma_eta_1)
    gaussEta_2 = RooGaussian("gaussEta_2_PT%s_Tz%s"%(nPT,nTz),"gaussEta_2 PDF", Jpsi_M, RooFit.RooConst(0),  sigma_eta_2)
    
    br_wigner = RooRelBreitWigner("br_wigner_Tz%s"%(nTz), "br_wigner",Jpsi_M, mass_eta_sc, gamma_eta, spin_eta,radius_eta,proton_m,proton_m)
    
    bwxg_1 = RooFFTConvPdf("bwxg_1_PT%s_Tz%s"%(nPT,nTz),"breit-wigner (X) gauss", Jpsi_M, br_wigner, gaussEta_1) 
    bwxg_2 = RooFFTConvPdf("bwxg_2_PT%s_Tz%s"%(nPT,nTz),"breit-wigner (X) gauss", Jpsi_M, br_wigner, gaussEta_2) 
    
    # Fit J/psi
    gauss_1 = RooGaussian("gauss_1_PT%s_Tz%s"%(nPT,nTz),"gaussian PDF",Jpsi_M,mass_Jpsi_sc,sigma_Jpsi_1) 
    gauss_2 = RooGaussian("gauss_2_PT%s_Tz%s"%(nPT,nTz),"gaussian PDF",Jpsi_M,mass_Jpsi_sc,sigma_Jpsi_2) 
    
    
    #Create constraints
    #    RooGaussian constrNEta("constrNEta","constraint Etac",nEta_1,f_1,RooConst(0.0)) 
    fconstraint = RooGaussian("fconstraint","fconstraint",mass_res, RooFit.RooConst(113.5), RooFit.RooConst(0.5))  #PDG 
    fconstrJpsi = RooGaussian("fconstrJpsi","fconstraint",mass_Jpsi, RooFit.RooConst(3096.9),RooFit.RooConst(0.5)) 
    
#    bkg = RooChebychev()
    a0 = RooRealVar("a0_PT%s_Tz%s"%(nPT,nTz),"a0",0.4,-1,2) 
    a1 = RooRealVar("a1_PT%s_Tz%s"%(nPT,nTz),"a1",0.05,-1.5,1.5) 
    a2 = RooRealVar("a2_PT%s_Tz%s"%(nPT,nTz),"a2",-0.005,-1.5,1.5) 
    a3 = RooRealVar("a3_PT%s_Tz%s"%(nPT,nTz),"a3",0.005,-0.1,0.1) 
    a4 = RooRealVar("a4_PT%s_Tz%s"%(nPT,nTz),"a4",0.005,-0.1,0.1) 
    
    if (bkgType == 0):
        #a0.setRange(0.0, 5.e2)
        a1.setVal(0.04)
        a2.setVal(0.04)
        if nTz!=7:
            bkg = RooGenericPdf("bkg_PT%s_Tz%s"%(nPT,nTz),"background","TMath::Exp(-(@0-3050.)/200.*@1)*(1.+@2*(@0-3050.)/200.+@3*(@0-3050.)*(@0-3050.)/200./200.)",RooArgList(Jpsi_M,a0,a1,a2)) 
        else:
            bkg = RooGenericPdf("bkg_PT%s_Tz%s"%(nPT,nTz),"background","TMath::Exp(-(@0-3050.)/200.*@1)*(1.+@2*(@0-3050.)/200)",RooArgList(Jpsi_M,a0,a1)) 
        #bkg = RooGenericPdf("bkg_PT%s_Tz%s"%(nPT,nTz),"background","TMath::Exp(-(@0-3050.)/200.*@1)*(1.+@2*(@0-3050.)/200.+@3*(@0-3050.)*(@0-3050.)/200./200.)",RooArgList(Jpsi_M,a0,a1,a2)) 
        #bkg = RooGenericPdf("bkg","background","TMath::Exp(-(@0-3050.)/200.*@1)*(1.+@2*@0.+@3*(2*@0*@0-1))",RooArgList(Jpsi_M,a0,a1,a2)) 
    elif (bkgType == 1):
        bkg = RooChebychev ("bkg_PT%s_Tz%s"%(nPT,nTz),"Background",Jpsi_M,RooArgList(a0,a1,a2)) 
    elif (bkgType == 2):
        bkg = RooChebychev("bkg_PT%s_Tz%s"%(nPT,nTz),"Background",Jpsi_M,RooArgList(a0,a1,a2,a3))
        
    
    pppi0 = RooGenericPdf("pppi0_PT%s_Tz%s"%(nPT,nTz),"Jpsi.pppi0","@0<(2961.92) ? TMath::Sqrt(3096.900-134.977-@0) : 0",RooArgList(Jpsi_M))
    nPPPi0 = RooFormulaVar("nPPPi0_PT%s_Tz%s"%(nPT,nTz),"nPPPi0","@0*@1",RooArgList(nJpsi,eff_pppi0))
    
    
    if (nConf!=3):

        modelBkg = RooAddPdf("modelBkg_PT%s_Tz%s"%(nPT,nTz),"background", RooArgList(bkg,pppi0), RooArgList(nBckgr,nPPPi0))
        modelSignal = RooAddPdf("modelSignal_PT%s_Tz%s"%(nPT,nTz),"signal", RooArgList(bwxg_1, bwxg_2,gauss_1,gauss_2), RooArgList(nEta_1,nEta_2,nJpsi_1,nJpsi_2))
        model = RooAddPdf("model_PT%s_Tz%s"%(nPT,nTz),"signal+bkg", RooArgList(bwxg_1, bwxg_2,gauss_1,gauss_2, bkg,pppi0), RooArgList(nEta_1,nEta_2,nJpsi_1,nJpsi_2, nBckgr,nPPPi0))
                
        #modelC = RooProdPdf("modelC", "model with constraints",RooArgList(model,fconstraint,fconstrJpsi))
        modelC = RooProdPdf("modelC_PT%s_Tz%s"%(nPT,nTz), "model with constraints",RooArgList(model,fconstraint))
        
    else:
        
        f = TFile(homeDir+"Results/MC/MassFit/MC_MassResolution_CB_2016_wksp.root","READ") 
        wMC = f.Get("w")
        f.Close()
        
        alpha_eta_1 = RooRealVar('alpha_eta_1','alpha of CB', wMC.var('alpha_eta_1').getValV(), 0.0, 10.) 
        alpha_eta_1.setConstant(True)
        n_eta_1 = RooRealVar('n_eta_1','n of CB', wMC.var('n_eta_1').getValV(), 0.0, 100.) 
        n_eta_1.setConstant(True)    

        # Fit eta
        cb_etac_1 = BifurcatedCB("cb_etac_1_PT%s_Tz%s"%(nPT,nTz), "Cystal Ball Function", Jpsi_M, RooFit.RooConst(0), sigma_eta_1, alpha_eta_1, n_eta_1, alpha_eta_1, n_eta_1)
        br_wigner = RooRelBreitWigner("br_wigner_Tz%s"%(nTz), "br_wigner",Jpsi_M, mass_eta_sc, gamma_eta, spin_eta,radius_eta,proton_m,proton_m)
            
        bwxg_1 = RooFFTConvPdf("bwxg_1_PT%s_Tz%s"%(nPT,nTz),"breit-wigner (X) gauss", Jpsi_M, br_wigner, cb_etac_1) 
        
        # Fit J/psi
        cb_Jpsi_1 = BifurcatedCB("cb_Jpsi_1_PT%s_Tz%s"%(nPT,nTz), "Cystal Ball Function", Jpsi_M, mass_Jpsi_sc, sigma_Jpsi_1, alpha_eta_1, n_eta_1, alpha_eta_1, n_eta_1)

        modelBkg = RooAddPdf("modelBkg_PT%s_Tz%s"%(nPT,nTz),"background", RooArgList(bkg,pppi0), RooArgList(nBckgr,nPPPi0))
        modelSignal = RooAddPdf("modelSignal_PT%s_Tz%s"%(nPT,nTz),"signal", RooArgList(bwxg_1, cb_Jpsi_1), RooArgList(nEta,nJpsi))
        model = RooAddPdf("model_PT%s_Tz%s"%(nPT,nTz),"signal+bkg", RooArgList(bwxg_1, cb_Jpsi_1, bkg,pppi0), RooArgList(nEta, nJpsi, nBckgr, nPPPi0))
        
        #modelC = RooProdPdf("modelC", "model with constraints",RooArgList(model,fconstraint,fconstrJpsi))
        modelC = RooProdPdf("modelC_PT%s_Tz%s"%(nPT,nTz), "model with constraints",RooArgList(model,fconstraint))
    

    getattr(w,'import')(model, RooFit.RecycleConflictNodes())
    getattr(w,'import')(modelC, RooFit.RecycleConflictNodes())
    getattr(w,'import')(modelBkg, RooFit.RecycleConflictNodes())
    getattr(w,'import')(modelSignal, RooFit.RecycleConflictNodes())





def fitData( nPTs=[0], nTzs=[0], nConf=0, keyPrompt=True, charm="Etac"):

    #gROOT.Reset()
    
    w = RooWorkspace("w",kTRUE)   
    
    
    #if (nConf!=10):
        #getData_h(w,keyPrompt, nPT, nTz, charm)
    #else:
        #getData_h(w,keyPrompt, nPT, nTz, charm, True)
    
    
    Jpsi_M = w.var("Jpsi_M") 
    
    hists = []    
    nPTbins = len(nPTs)
    nTzbins = len(nTzs)
    
    model = fillRelWorkspace(w,nConf,1, 1)

    sample = RooCategory("sample","sample") 


    Jpsi_M = w.var("Jpsi_M")
    Jpsi_M.setBins(nBins)

    for nPT in nPTs:
        hists.append([])
        for nTz in nTzs:

            model = fillRelWorkspace(w,nConf,nPT, nTz)
            getData_h(w, keyPrompt, nPT, nTz, charm)

            idx = nPTs.index(nPT)
            histo   = w.data("dh_PT%s_Tz%s"%(nPT,nTz))
            hists[idx].append(histo)


    model_pts = []
    modelSignal_pts = []
    samples = []
    #-----------------------------------------------------
    params = []
    for nPT in nPTs:
        model_pts.append([])
        modelSignal_pts.append([])
        samples.append(RooCategory("samplePT","sample") )
        #-----------------------------------------------------
        params.append([])
        for nTz in nTzs:
            idx = nPTs.index(nPT)
            idTz = nTzs.index(nTz)
            samples[idx].defineType("PT%s_Tz%s"%(nPT,nTz)) 
            model_pts[idx].append(w.pdf("model_PT%s_Tz%s"%(nPT,nTz)))
            modelSignal_pts[idx].append(w.pdf("modelSignal_PT%s_Tz%s"%(nPT,nTz)))
            #-----------------------------------------------------
            params[idx].append(w.pdf("modelSignal_PT%s_Tz%s"%(nPT,nTz)).getParameters(RooArgSet(Jpsi_M)))
            w.defineSet("parameters_PT%s_Tz%s"%(nPT,nTz),params[idx][idTz])
        
        sample.defineType("PT%s"%(nPT))



    #combData = RooDataHist("combData","combData",RooArgList(Jpsi_M,sample),RooFit.Index(sample),RooFit.Import("PT1",hists[0]),RooFit.Import("PT2",hists[1]),RooFit.Import("PT3",hists[2]),RooFit.Import("PT4",hists[3]))
    
    combDatas = []
    for nPT in nPTs:
        idx = nPTs.index(nPT)
        if charm == "Etac":
            #combDatas.append( RooDataHist("combData_PT%s"%(nPT), "combData", RooArgList(Jpsi_M,sample), RooFit.Index(sample), RooFit.Import("PT%s_Tz%s"%(nPT,nTzs[0]),hists[idx][0]), RooFit.Import("PT%s_Tz%s"%(nPT,nTzs[1]),hists[idx][1]), RooFit.Import("PT%s_Tz%s"%(nPT,nTzs[2]),hists[idx][2]), RooFit.Import("PT%s_Tz%s"%(nPT,nTzs[3]),hists[idx][3]), RooFit.Import("PT%s_Tz%s"%(nPT,nTzs[4]),hists[idx][4]),RooFit.Import("PT%s_Tz%s"%(nPT,nTzs[5]),hists[idx][5])))
        #else:
            combDatas.append( RooDataHist("combData_PT%s"%(nPT), "combData", RooArgList(Jpsi_M,sample), RooFit.Index(sample), RooFit.Import("PT%s_Tz%s"%(nPT,nTzs[0]),hists[idx][0]), RooFit.Import("PT%s_Tz%s"%(nPT,nTzs[1]),hists[idx][1]), RooFit.Import("PT%s_Tz%s"%(nPT,nTzs[2]),hists[idx][2]), RooFit.Import("PT%s_Tz%s"%(nPT,nTzs[3]),hists[idx][3]), RooFit.Import("PT%s_Tz%s"%(nPT,nTzs[4]),hists[idx][4]),RooFit.Import("PT%s_Tz%s"%(nPT,nTzs[5]),hists[idx][5]),RooFit.Import("PT%s_Tz%s"%(nPT,nTzs[6]),hists[idx][6])))
    
    combData = RooDataHist("combData","combData",RooArgList(Jpsi_M,sample),RooFit.Index(sample),RooFit.Import("PT1",combDatas[0]),RooFit.Import("PT2",combDatas[1]),RooFit.Import("PT3",combDatas[2]),RooFit.Import("PT4",combDatas[3]))
    #combData = RooDataHist("combData","combData",RooArgList(Jpsi_M,sample),RooFit.Index(sample),RooFit.Import("PT0",combDatas[0]))
  
    

    
    #sigma = w.var("sigma_eta_1")
    mass_Jpsi = w.var("mass_Jpsi")
    mass_res = w.var("mass_res")
    gamma_eta = w.var("gamma_eta")


    if keyPrompt:

        #f = TFile(homeDir+"Results/MassFit/fromB/Total/Wksp_MassFit_PT0_Tz0_C%s.root"%(nConf),"READ") 
        f = TFile(homeDir+"Results/MassFit/fromB/Total/Wksp_MassFit_PT0_Tz0_C0.root","READ") 
        wSec = f.Get("w")
        f.Close()
        w.var("mass_Jpsi").setVal(wSec.var("mass_Jpsi").getValV())
        w.var("mass_Jpsi").setConstant(True)
        w.var("mass_res").setVal(wSec.var("mass_res").getValV())        
        #w.var("mass_res").setVal(109.5)  #etac2S2pp
        #w.var("mass_res").setVal(113.5)  #PDG
        w.var("mass_res").setConstant(True)
        w.var("sigma_eta_1").setVal(wSec.var("sigma_eta_1").getValV())
        #w.var("sigma_eta_1_PT%s"%(nPT)).setConstant(True)

    #w.var("rNormJpsi_Tz1").setVal(w.var("rNormJpsi_Tz2").getValV())
    #w.var("rNormJpsi_Tz5").setVal(1.0)
    #w.var("rNormJpsi_Tz5").setConstant(True)

    simPdf = RooSimultaneous("smodel","",sample)
    simPdfSignal = RooSimultaneous("smodelSignal","",sample)
    for nPT in nPTs:
        #w.var("rNormSigma_PT%s_Tz1"%(nPT)).setVal(w.var("rNormSigma_PT%s_Tz2"%(nPT)).getValV())
        #w.var("nBckgr_PT%s_Tz1"%(nPT)).setVal(1e5)
        for nTz in nTzs:
            idxPt = nPTs.index(nPT)
            idxTz = nTzs.index(nTz)
            simPdf.addPdf(model_pts[idxPt][idxTz],"PT%s_Tz%s"%(nPT,nTz))
            simPdfSignal.addPdf(modelSignal_pts[idxPt][idxTz],"PT%s_Tz%s"%(nPT,nTz))

            modelBkg = w.pdf("modelBkg_PT%s_Tz%s"%(nPT,nTz))
            if nTz==1:
                w.var("nJpsi_PT%s_Tz%s"%(nPT,nTz)).setRange(-5e2, 5e4)
                w.var("nEta_PT%s_Tz%s"%(nPT,nTz)).setRange(-1e3, 1e4)
                w.var("nEta_PT%s_Tz%s"%(nPT,nTz)).setVal(100.)
                w.var("nEta_PT%s_Tz%s"%(nPT,nTz)).setConstant(True)
                #w.var("sigma_eta_1").setConstant(True)
                modelBkg.fitTo(hists[idxPt][idxTz], RooFit.Extended(True), RooFit.Save(True))
                w.var("nEta_PT%s_Tz%s"%(nPT,nTz)).setConstant(False)
            
            model_pts[idxPt][idxTz].fitTo(hists[idxPt][idxTz], RooFit.Extended(True), RooFit.Offset(True), RooFit.Save(True), RooFit.NumCPU(8))
            model_pts[idxPt][idxTz].fitTo(hists[idxPt][idxTz], RooFit.Extended(True), RooFit.Offset(True), RooFit.Save(True), RooFit.NumCPU(8))
            model_pts[idxPt][idxTz].fitTo(hists[idxPt][idxTz], RooFit.Offset(True), RooFit.Minos(True), RooFit.Save(True), RooFit.NumCPU(8))
            #a0Val = w.var("a0_PT%s_Tz%s"%(nPT,nTz)).getValV()
            #a0Err = w.var("a0_PT%s_Tz%s"%(nPT,nTz)).getError()
            #w.var("a0_PT%s_Tz%s"%(nPT,nTz)).setRange(a0Val-15*a0Err, a0Val+15*a0Err)
            #a1Val = w.var("a1_PT%s_Tz%s"%(nPT,nTz)).getValV()
            #a1Err = w.var("a1_PT%s_Tz%s"%(nPT,nTz)).getError()
            #w.var("a1_PT%s_Tz%s"%(nPT,nTz)).setRange(a1Val-15*a1Err, a1Val+15*a1Err)
            #a2Val = w.var("a2_PT%s_Tz%s"%(nPT,nTz)).getValV()
            #a2Err = w.var("a2_PT%s_Tz%s"%(nPT,nTz)).getError()
            #w.var("a2_PT%s_Tz%s"%(nPT,nTz)).setRange(a2Val-15*a2Err, a2Val+15*a2Err)
            #w.var("a0_PT%s_Tz%s"%(nPT,nTz)).setConstant(True)
            #w.var("a1_PT%s_Tz%s"%(nPT,nTz)).setConstant(True)
            #w.var("a2_PT%s_Tz%s"%(nPT,nTz)).setConstant(True)
            bgVal = w.var("nBckgr_PT%s_Tz%s"%(nPT,nTz)).getValV()
            bgErr = w.var("nBckgr_PT%s_Tz%s"%(nPT,nTz)).getError()
            #w.var("nBckgr_PT%s_Tz%s"%(nPT,nTz)).setRange(bgVal-10*bgErr, bgVal+20*bgErr)
            w.var("nBckgr_PT%s_Tz%s"%(nPT,nTz)).setConstant(True)
            etaVal = w.var("nEta_PT%s_Tz%s"%(nPT,nTz)).getValV()
            etaErr = w.var("nEta_PT%s_Tz%s"%(nPT,nTz)).getError()
            w.var("nEta_PT%s_Tz%s"%(nPT,nTz)).setRange(etaVal-4*etaErr, etaVal+20*etaErr)
            w.var("nEta_PT%s_Tz%s"%(nPT,nTz)).setConstant(True)
            jpsiVal = w.var("nJpsi_PT%s_Tz%s"%(nPT,nTz)).getValV()
            jpsiErr = w.var("nJpsi_PT%s_Tz%s"%(nPT,nTz)).getError()
            #w.var("nJpsi_PT%s_Tz%s"%(nPT,nTz)).setRange(jpsiVal-15*jpsiErr, jpsiVal+15*jpsiErr)
            w.var("nJpsi_PT%s_Tz%s"%(nPT,nTz)).setConstant(True)
            w.var("sigma_eta_1").setConstant(False)


    getattr(w,'import')(combData)
    getattr(w,'import')(simPdf, RooFit.RecycleConflictNodes())
    getattr(w,'import')(simPdfSignal, RooFit.RecycleConflictNodes())


    ##input (a = "")
    ## NLL with extended term, constrains, and likelihood offset
    nll = simPdf.createNLL(combData, RooFit.Extended(True), RooFit.Offset(True),RooFit.NumCPU(8))

    ## Manual minimization
    minimizer = RooMinimizer(nll)
    minimizer.setVerbose(False)
    minimizer.setMinimizerType("Minuit2")
    minimizer.optimizeConst(True)
    minimizer.setEps(1e-8)
    minimizer.setStrategy(2)
    minimizer.setPrintEvalErrors(1)
    minimizer.setOffsetting(True)
    minimizer.setMaxIterations(2000)
    minimizer.setMaxFunctionCalls(2000)
    minimizer.migrad()
    minimizer.minos()
    #minimizer.improve()
    
    
    #simPdf.fitTo(combData,RooFit.Strategy(2))
    r = simPdf.fitTo(combData)
    w.var("mass_Jpsi").setConstant(False)
    w.var("mass_res").setConstant(False)
    r = simPdf.fitTo(combData,RooFit.Extended(True),RooFit.Save(),RooFit.Offset(True),RooFit.NumCPU(12))
    #r = simPdf.fitTo(combData,RooFit.Extended(True),RooFit.Save(),RooFit.Offset(True),RooFit.NumCPU(2))
    for nPT in nPTs:
        #w.var("nBckgr_PT%s_Tz1"%(nPT)).setRange(1e3, 1e6)
        for nTz in nTzs:

            w.var("nBckgr_PT%s_Tz%s"%(nPT,nTz)).setConstant(False)
            w.var("nEta_PT%s_Tz%s"%(nPT,nTz)).setConstant(False)
            w.var("nJpsi_PT%s_Tz%s"%(nPT,nTz)).setConstant(False)
            #w.var("mass_Jpsi").setConstant(True)
            #w.var("mass_res").setConstant(True)

    r = simPdf.fitTo(combData,RooFit.Extended(True),RooFit.Save(),RooFit.Offset(True),RooFit.NumCPU(12,2))
    #r = simPdf.fitTo(combData,RooFit.Extended(True),RooFit.Save(),RooFit.Offset(True),RooFit.NumCPU(2))
    #r = simPdf.fitTo(combData,RooFit.Extended(True),RooFit.Save(),RooFit.Offset(True),RooFit.NumCPU(2))
    #r = simPdf.fitTo(combData,RooFit.Extended(True),RooFit.Save(),RooFit.Offset(True),RooFit.NumCPU(2))
    #r = simPdf.fitTo(combData,RooFit.Extended(True),RooFit.Save(),RooFit.Offset(True),RooFit.NumCPU(2))
    #r = simPdf.fitTo(combData,RooFit.Extended(True),RooFit.Minos(True),RooFit.Save(True),RooFit.Offset(True),RooFit.NumCPU(2))
    #r = simPdf.fitTo(combData,RooFit.Extended(True),RooFit.Minos(True),RooFit.Save(True),RooFit.Offset(True),RooFit.NumCPU(2))


<<<<<<< HEAD
=======
    
    #RooArgSet* params = (RooArgSet*) model.getParameters(x) ;
    #w.defineSet("parameters",*params) ;
    
    ### Manual minimization
    #minimizer = RooMinimizer(nll)
    #minimizer.setVerbose(True)
    #minimizer.setMinimizerType("Minuit2")
    #minimizer.optimizeConst(True)
    #minimizer.setEps(1e-8)
    #minimizer.setStrategy(2)
    #minimizer.setPrintEvalErrors(1)
    minimizer.setOffsetting(True)
    #minimizer.setMaxIterations(2000)
    #minimizer.setMaxFunctionCalls(2000)
    #minimizer.migrad()
    #minimizer.minos( getattr(w,'set')("poi"))
    ##minimizer.improve()
    for nPT in nPTs:
        for nTz in nTzs:
            minimizer.minos( getattr(w,'set')("parameters_PT%s_Tz%s"%(nPT,nTz)))


>>>>>>> c53a30ee2e5e05985d5dab185783d1bf8f698ad7
    if keyPrompt: inCutKey = "prompt"
    else:         inCutKey = "fromB"
    
    #if nPT!=0:    binPT = "Jpsi_PT/"
    #else:         binPT = "Total/"

    binPT = "Sim/"
    #binTz = "Tz%s.root"%(nTz)
    
    dirName = homeDir+"Results/MassFit/" + inCutKey + "/" + binPT + "m_scaled/"
    
    #if (nPT!=0 and nTz!=0):
        #dirName = dirName + charm + "/"
    
    
    nameTxt  = dirName + charm + "_fitRes_C%d.txt"%(nConf)

    import os, sys 
    save = os.dup( sys.stdout.fileno() ) 
    newout = file(nameTxt, 'w' ) 
    os.dup2( newout.fileno(), sys.stdout.fileno() ) 
    r.Print("v") 
    r.correlationMatrix().Print()
    os.dup2( save, sys.stdout.fileno() ) 
    newout.close()

    #nll = simPdf.createNLL(combData, RooFit.Extended(True), RooFit.Offset(True),RooFit.NumCPU(2))

    paramsSignal = simPdfSignal.getParameters(RooArgSet(Jpsi_M))
    w.defineSet("poiSignal",paramsSignal )
    
    r = simPdf.fitTo(combData,RooFit.Extended(True),RooFit.Minos( getattr(w,'set')("poiSignal")),RooFit.Save(True),RooFit.Offset(True),RooFit.NumCPU(12,2))
    r = simPdf.fitTo(combData,RooFit.Extended(True),RooFit.Minos( getattr(w,'set')("poiSignal")),RooFit.Strategy(2),RooFit.Save(True),RooFit.Offset(True),RooFit.NumCPU(12,2))
    #r = simPdf.fitTo(combData,RooFit.Extended(True),RooFit.Minos(True),RooFit.Strategy(2),RooFit.Save(True),RooFit.Offset(True),RooFit.NumCPU(12,2))
    
    #### Manual minimization
    #minimizer = RooMinimizer(nll)
    ##minimizer.setVerbose(True)
    #minimizer.setMinimizerType("Minuit2")
    ##minimizer.optimizeConst(True)
    ##minimizer.setEps(1e-8)
    #minimizer.setStrategy(2)
    ##minimizer.setPrintEvalErrors(1)
    #minimizer.setOffsetting(True)
    ##minimizer.setMaxIterations(2000)
    ##minimizer.setMaxFunctionCalls(2000)
    #minimizer.migrad()
    ##getattr(w,'set')("poiSignal").Print()
    ##input('Enter your input:') 
    #minimizer.minos( getattr(w,'set')("poiSignal"))
    ###minimizer.improve()
    ##for nPT in nPTs:
        ##for nTz in nTzs:
            ##getattr(w,'set')("parameters_PT%s_Tz%s"%(nPT,nTz)).Print()
            ###input('Enter your input:') 

            ##minimizer.minos( getattr(w,'set')("parameters_PT%s_Tz%s"%(nPT,nTz)))
            ####input('Enter your input:') 

    

    import os, sys 
    save = os.dup( sys.stdout.fileno() ) 
    newout = file(nameTxt, 'a' ) 
    os.dup2( newout.fileno(), sys.stdout.fileno() ) 
    r.Print("v") 
    r.correlationMatrix().Print()
    os.dup2( save, sys.stdout.fileno() ) 
    newout.close()

    Draw(w, combData, sample, simPdf, nPTs, nTzs, nConf, keyPrompt, charm)
    
        
    #w.writeToFile(nameWksp)
    
    #w.Print("v")






def Fit_Mass(list):
    nC = list[0]
    nPTs = [1, 2, 3, 4]
    #nTzs = [1, 2, 3, 4, 5]
    nTzs = [1, 2, 3, 4, 5, 6, 7]

    return  fitData(nPTs,nTzs,nC,True,"Etac")

from multiprocessing import *
from contextlib import closing
def proc():

    #nConfs = [ 1, 2, 3, 4, 6, 7, 8, 9]
    nConfs = [0, 4]
    a = [[nC] for nC in nConfs]
    if __name__ == '__main__':
        with closing(Pool(8)) as p:
            (p.map(Fit_Mass, a))
            p.terminate()

#gROOT.LoadMacro("../lhcbStyle.C")

#nPTs = [0]
#nPTs = [1, 2, 3, 4]
#nTzs = [1, 2, 3, 4, 5, 6]
###nTzs = [1, 2, 3, 4, 5, 6, 7]
##nConfs = [ 1, 2, 3, 4, 6, 7, 8, 9]
nConfs = [0]
#for iC in nConfs:
    #fitData( nPTs, nTzs, iC, True, "Etac")
#fitData( 1, 2, 0, True)
            
Fit_Mass(nConfs)


#proc()