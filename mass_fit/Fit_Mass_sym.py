
from ROOT import *
gROOT.LoadMacro("../libRooRelBreitWigner/RooRelBreitWigner.cxx+")



histosDir = "Histos/"

binningDict = {
    "Jpsi_PT":                      [6500, 8000, 10000, 12000, 14000, 18000],
    "Jpsi_ETA":                     [2.0, 2.45, 2.8, 3.15, 4.5],
    "TMath::Abs(ProtonM_CosTheta)": [0.0, 0.15, 0.3, 0.50, 1.0],
    "nSPDHits":                     [0, 100, 150, 200, 250, 300],
}


minM = 2850
maxM = 3250
nBins = 1000


def getData_h(w, keyPrompt=True, nPT=0, nTz=0, charm='Etac', shift=False):
       
    
    if keyPrompt: inCutKey = "all_l0TOS"
    else:         inCutKey = "secondary_l0TOS"
    
    if nPT!=0:    
        if nTz!=0: binPT = "Jpsi_PT/"+charm+"/bin%s_"%(nPT)
        else:      binPT = "Jpsi_PT/bin%s_"%(nPT) 
    else:         binPT = "Total/"

    binTz = "Tz%s.root"%(nTz)
    
    dirName_2015 = "~/scripts/Histos/2015/DiProton/" + inCutKey + "/"
    dirName_2016 = "~/scripts/Histos/2016/DiProton/" + inCutKey + "/"
        
    nameFile = dirName_2015 + binPT + binTz
    f_2015 = TFile(nameFile,"read")
    print nameFile
    nameFile = dirName_2016 + binPT + binTz
    f_2016 = TFile(nameFile,"read")
    print nameFile
    
    
    hh = TH1D("hh","hh", nBins, minM, maxM)
    hh = f_2015.Get("Jpsi_M")
    hh.Add(f_2016.Get("Jpsi_M"))
    
    Jpsi_M = w.var("Jpsi_M")
    #Jpsi_M = RooRealVar("Jpsi_M","Jpsi_M",2850,3250) 

    if(shift):
        hh.SetBins(nBins, minM+(maxM-minM)/(2.*float(nBins)),maxM+(maxM-minM)/(2.*float(nBins)))
        Jpsi_M.setRange(minM+(maxM-minM)/(2.*float(nBins)), maxM+(maxM-minM)/(2.*float(nBins))) 

    dh = RooDataHist("dh_PT%s"%(nPT),"dh_PT%s"%(nPT), RooArgList(Jpsi_M) ,hh)
    getattr(w,'import')(dh)
    f_2015.Close()
    f_2016.Close()
    
    #print "DATA\'S READ SUCCESSFULLY"



def getConstPars(nConf, nPT=0):

    f = TFile("~/scripts/Results/MC/MassFit/MC_MassResolution_2016_wksp.root","READ") 
    wMC = f.Get("w")
    f.Close()
    
    rNtoW = wMC.var("rNarToW").getValV()
    rEtaToJpsi = wMC.var("rEtaToJpsi").getValV()
    rArea = wMC.var("rG1toG2").getValV()
    bkgOpt = 0
    gamma = 31.8
    effic = 0.035
    #gamma = 21.3
    #effic = 0.06
    #ratioNtoW = 0.5
    #ratioEtaToJpsi = 0.88
    #ratioArea = 0.9
    
    
    if (nConf == 1):
        rNtoW = wMC.var("rNarToW").getValV()+wMC.var("rNarToW").getError()
        print ("SETUP 1")
    elif (nConf == 2):
        rEtaToJpsi = wMC.var("rEtaToJpsi").getValV()+wMC.var("rEtaToJpsi").getError()
        print ("SETUP 2")      
    elif (nConf == 3):
        rArea = wMC.var("rG1toG2").getValV()+wMC.var("rG1toG2").getError()
        print ("SETUP 3")
    elif (nConf == 4):
        bkgOpt = 1
        print ("SETUP 4")
    elif (nConf == 5):
        bkgOpt = 2
        print ("SETUP 5")
    elif (nConf == 6):
        gamma = 32.6
        print ("SETUP 6")
    elif (nConf == 9):
        effic = 0.032
        print ("SETUP 9")

    #if (nPT != 0):
        #nameFile = "results/MC/MassFit/MC_Mass_2016_wksp_PT%d.root"%(nPT)
        #f = TFile(nameFile,"READ") 
        #wMC = f.Get("w;1")
        #f.Close()
        #if (nConf == 1):
            #ratioEtaToJpsi = wMC.var("rEtaToJpsi").getValV()+wMC.var("rEtaToJpsi").getError()
        #else:
            #ratioEtaToJpsi = wMC.var("rEtaToJpsi").getValV()   
    
    return rNtoW,rEtaToJpsi,rArea, bkgOpt, gamma, effic
    
    
        

def fillRelWorkspace(w, nConf, nPT):
    
    Jpsi_M = RooRealVar("Jpsi_M","Jpsi_M",2850,3250) 
    Jpsi_M.setBins(2000,"cache")
    Jpsi_M.setRange("SBLeft", 2850, 2900)
    Jpsi_M.setRange("SBCentral", 3035, 3060)
    Jpsi_M.setRange("SBRight", 3150, 3250)
    Jpsi_M.setRange("SignalEtac", 2900, 3035)
    Jpsi_M.setRange("SignalJpsi", 3060, 3150)
    Jpsi_M.setRange("Total", 2850, 3250)
    
    mDiffEtac = 113.501
    
    ratioNtoW,ratioEtaToJpsi,ratioArea, bkgType, gammaEtac, eff = getConstPars(nConf, nPT)
    
    
    if (nConf==10):
        Jpsi_M.setRange(2855,3245)
    
    
    rEtaToJpsi = RooRealVar("rEtaToJpsi_PT%s"%(nPT),"rEtaToJpsi", ratioEtaToJpsi)
    rNarToW = RooRealVar("rNarToW_PT%s"%(nPT),"rNarToW",ratioNtoW)
    rG1toG2 = RooRealVar("rG1toG2_PT%s"%(nPT),"rG1toG2",ratioArea)
    eff_pppi0 = RooRealVar("eff_pppi0_PT%s"%(nPT),"eff_pppi0",eff)
    nEta = RooRealVar("nEta_PT%s"%(nPT),"num of Etac", 10, 1.e7)
    nJpsi = RooRealVar("nJpsi_PT%s"%(nPT),"num of J/Psi", 10, 1.e7)
    nEtacRel = RooRealVar("nEtacRel_PT%s"%(nPT),"num of Etac", 0.0, 3.0)
    #nEtacRel = RooRealVar("nEtacRel_PT%s"%(nPT),"nEtacRel", "@0/@1",RooArgList(nEta,nJpsi))
    
    nEta_1 = RooFormulaVar("nEta_1_PT%s"%(nPT),"num of Etac","@0*@1*@2",RooArgList(nEtacRel,nJpsi,rG1toG2))
    nEta_2 = RooFormulaVar("nEta_2_PT%s"%(nPT),"num of Etac","@0*@1-@2",RooArgList(nEtacRel,nJpsi,nEta_1))
    #nEta_1 = RooFormulaVar("nEta_1_PT%s"%(nPT),"num of Etac","@0-@1",RooArgList(nEta,rG1toG2))
    #nEta_2 = RooFormulaVar("nEta_2_PT%s"%(nPT),"num of Etac","@0-@1",RooArgList(nEta,nEta_1))
    nJpsi_1 = RooFormulaVar("nJpsi_1_PT%s"%(nPT),"num of J/Psi","@0*@1",RooArgList(nJpsi,rG1toG2))
    nJpsi_2 = RooFormulaVar("nJpsi_2_PT%s"%(nPT),"num of J/Psi","@0-@1",RooArgList(nJpsi,nJpsi_1))
    nBckgr = RooRealVar("nBckgr_PT%s"%(nPT),"num of backgr",7e7,10,1.e+9)
    
    
    mass_Jpsi = RooRealVar("mass_Jpsi","mean of gaussian",3096.9, 3030, 3150) 
    mass_res = RooRealVar("mass_res","mean of gaussian",mDiffEtac, 100, 130) 
    
    if (nConf==7 or nConf==8):  mass_res.setConstant(True)

    mass_eta = RooFormulaVar("mass_eta","mean of gaussian","@0-@1",RooArgList(mass_Jpsi,mass_res)) 
    gamma_eta = RooRealVar("gamma_eta","width of Br-W", gammaEtac, 10., 50.)
    gamma_eta.setConstant()
    spin_eta = RooRealVar("spin_eta","spin_eta", 0. )
    radius_eta = RooRealVar("radius_eta","radius", 1.)
    proton_m = RooRealVar("proton_m","proton mass", 938.3 )
    sigma_eta_1 = RooRealVar("sigma_eta_1_PT%s"%(nPT),"width of gaussian", 9., 5., 50.) 
    sigma_eta_2 = RooFormulaVar("sigma_eta_2_PT%s"%(nPT),"width of gaussian","@0/@1",RooArgList(sigma_eta_1,rNarToW))
    
    sigma_Jpsi_1 = RooFormulaVar("sigma_Jpsi_1_PT%s"%(nPT),"width of gaussian","@0/@1",RooArgList(sigma_eta_1,rEtaToJpsi))
    sigma_Jpsi_2 = RooFormulaVar("sigma_Jpsi_2_PT%s"%(nPT),"width of gaussian","@0/@1",RooArgList(sigma_Jpsi_1,rNarToW))
    
    
    # Fit eta
    gaussEta_1 = RooGaussian("gaussEta_1_PT%s"%(nPT),"gaussEta_1 PDF", Jpsi_M, RooFit.RooConst(0),  sigma_eta_1)
    gaussEta_2 = RooGaussian("gaussEta_2_PT%s"%(nPT),"gaussEta_2 PDF", Jpsi_M, RooFit.RooConst(0),  sigma_eta_2)
    
    br_wigner = RooRelBreitWigner("br_wigner", "br_wigner",Jpsi_M, mass_eta, gamma_eta, spin_eta,radius_eta,proton_m,proton_m)
    
    bwxg_1 = RooFFTConvPdf("bwxg_1_PT%s"%(nPT),"breit-wigner (X) gauss", Jpsi_M, br_wigner, gaussEta_1) 
    bwxg_2 = RooFFTConvPdf("bwxg_2_PT%s"%(nPT),"breit-wigner (X) gauss", Jpsi_M, br_wigner, gaussEta_2) 
    
    # Fit J/psi
    gauss_1 = RooGaussian("gauss_1_PT%s"%(nPT),"gaussian PDF",Jpsi_M,mass_Jpsi,sigma_Jpsi_1) 
    gauss_2 = RooGaussian("gauss_2_PT%s"%(nPT),"gaussian PDF",Jpsi_M,mass_Jpsi,sigma_Jpsi_2) 
    
    
    
    #Connection between parameters
    #    RooFormulaVar f_1("f_1","f_1","@0*9.0",RooArgList(nEta_2))
    
    #Create constraints
    #    RooGaussian constrNEta("constrNEta","constraint Etac",nEta_1,f_1,RooConst(0.0)) 
    fconstraint = RooGaussian("fconstraint","fconstraint",mass_res, RooFit.RooConst(113.5), RooFit.RooConst(0.5))  #PDG 
    fconstrJpsi = RooGaussian("fconstrJpsi","fconstraint",mass_Jpsi, RooFit.RooConst(3096.9),RooFit.RooConst(0.5)) 
    
#    bkg = RooChebychev()
    a0 = RooRealVar("a0_PT%s"%(nPT),"a0",0.4,-1,2) 
    a1 = RooRealVar("a1_PT%s"%(nPT),"a1",0.05,-1.,1.) 
    a2 = RooRealVar("a2_PT%s"%(nPT),"a2",-0.005,-1.5e-1,1.5e-1) 
    a3 = RooRealVar("a3_PT%s"%(nPT),"a3",0.005,-0.1,0.1) 
    a4 = RooRealVar("a4_PT%s"%(nPT),"a4",0.005,-0.1,0.1) 
    
    if (bkgType == 0):
        #a0.setRange(0.0, 5.e2)
        a1.setVal(0.04)
        a2.setVal(0.04)
        bkg = RooGenericPdf("bkg_PT%s"%(nPT),"background","TMath::Exp(-(@0-3050.)/200.*@1)*(1.+@2*(@0-3050.)/200.+@3*(@0-3050.)*(@0-3050.)/200./200.)",RooArgList(Jpsi_M,a0,a1,a2)) 
        #bkg = RooGenericPdf("bkg","background","TMath::Exp(-(@0-3050.)/200.*@1)*(1.+@2*@0.+@3*(2*@0*@0-1))",RooArgList(Jpsi_M,a0,a1,a2)) 
    elif (bkgType == 1):
        bkg = RooChebychev ("bkg_PT%s"%(nPT),"Background",Jpsi_M,RooArgList(a0,a1,a2)) 
    elif (bkgType == 2):
        bkg = RooChebychev("bkg_PT%s"%(nPT),"Background",Jpsi_M,RooArgList(a0,a1,a2,a3))
        
    
    pppi0 = RooGenericPdf("pppi0_PT%s"%(nPT),"Jpsi.pppi0","@0<(2961.92) ? TMath::Sqrt(3096.900-134.977-@0) : 0",RooArgList(Jpsi_M))
    nPPPi0 = RooFormulaVar("nPPPi0_PT%s"%(nPT),"nPPPi0","@0*@1*(1.19/2.12)",RooArgList(nJpsi,eff_pppi0))
    
    
    modelBkg = RooAddPdf("modelBkg_PT%s"%(nPT),"background", RooArgList(bkg,pppi0), RooArgList(nBckgr,nPPPi0))
    modelSignal = RooAddPdf("modelSignal_PT%s"%(nPT),"signal", RooArgList(bwxg_1, bwxg_2,gauss_1,gauss_2), RooArgList(nEta_1,nEta_2,nJpsi_1,nJpsi_2))
    model = RooAddPdf("model","signal+bkg_PT%s"%(nPT), RooArgList(bwxg_1, bwxg_2,gauss_1,gauss_2, bkg,pppi0), RooArgList(nEta_1,nEta_2,nJpsi_1,nJpsi_2, nBckgr,nPPPi0))
    
    
    #modelC = RooProdPdf("modelC", "model with constraints",RooArgList(model,fconstraint,fconstrJpsi))
    modelC = RooProdPdf("modelC_PT%s"%(nPT), "model with constraints",RooArgList(model,fconstraint))

    getattr(w,'import')(model)
    getattr(w,'import')(modelC, RooFit.RecycleConflictNodes())
    getattr(w,'import')(modelBkg, RooFit.RecycleConflictNodes())
    getattr(w,'import')(modelSignal, RooFit.RecycleConflictNodes())



def fitData( nPTs=[0], nTz=0, nConf=0, keyPrompt=True, charm="Etac"):

    #gROOT.Reset()
    
    w = RooWorkspace("w",kTRUE)   
    
    
    if (nConf!=10):
        getData_h(w,keyPrompt, nPT, nTz, charm)
    else:
        getData_h(w,keyPrompt, nPT, nTz, charm, True)
    
    
    Jpsi_M = w.var("Jpsi_M") 
    
    hists[]    
    nPTbins = len(nPTs)

    model = fillRelWorkspace(w,nConf,1)

    sample = RooCategory("sample","sample") 
    #for nPT in nPTs:
        #sample.defineType("PT"+str(nPT)) 


    Jpsi_M = w.var("Jpsi_M")
    Jpsi_M.setBins(nBins)

    for nPT in nPTs:
        model = fillRelWorkspace(w,nConf,nPT)
        getData_h(w, keyPrompt, nPT, nTz, charm)

        histo   = w.data("dh_PT%s"%nPT)
        hists.append(histo)

    
    model_pts = []
    for nPT in nPTs:
        sample.defineType("PT%s"%nPT) 
        model_pts.append(w.pdf("model_PT%s"%nPT))




    combData = RooDataHist("combData","combData",RooArgList(Jpsi_M,sample),RooFit.Index(sample),RooFit.Import("PT1",hists[0]),RooFit.Import("PT2",hists[1]),RooFit.Import("PT3",hists[2]),RooFit.Import("PT4",hists[3]),RooFit.Import("PT5",hists[4]))
    # combData = RooDataHist("combData","combData",RooArgList(Jpsi_M,sample),RooFit.Index(sample),RooFit.Import("PT2",hists[0]),RooFit.Import("PT3",hists[1]),RooFit.Import("PT4",hists[2]),RooFit.Import("PT5",hists[3]))



    simPdf = RooSimultaneous("smodel","",sample)
    for nPT in nPTs:
        idx = nPTs.index(nPT)
        simPdf.addPdf(model_pts[idx],"PT"+str(nPT))

    simPdf.fitTo(combData,RooFit.Strategy(2))
    simPdf.fitTo(combData)
    simPdf.fitTo(combData,RooFit.Minos(True))

    
    
    model = w.pdf("model")
    modelC = w.pdf("modelC")
    modelBkg = w.pdf("modelBkg")
    modelSignal = w.pdf("modelSignal")
        
    fconstraint = w.pdf("fconstraint")    
    
    
    #nEta = w.var("nEta")
    nEta = w.var("nEtacRel")
    nJpsi = w.var("nJpsi")
    nBckgr = w.var("nBckgr")
    
    sigma = w.var("sigma_eta_1")
    mass_Jpsi = w.var("mass_Jpsi")
    mass_res = w.var("mass_res")
    gamma_eta = w.var("gamma_eta")

    #   nEta.setVal(10.)  nEta.setConstant(kTRUE)
    #   nJpsi.setVal(10.)  nJpsi.setConstant(kTRUE)
    #   
    #   modelBkg.fitTo(*hist,Range("SBLeft,SBRight"),Extended(kTRUE),/*Save(),*/NumCPU(48))
    #   nEta.setConstant(False)
    #   nJpsi.setConstant(False)
    
    #   model.fitTo(*hist,Range("SignalEtac,SignalJpsi"),Extended(),/*Save(),*/NumCPU(48))
    
    #   w.var("a1").setConstant(kTRUE)
    #   w.var("a2").setConstant(kTRUE)
    
    if keyPrompt:

        f = TFile("~/scripts/Results/MassFit/fromB/Total/Wksp_MassFit_PT0_Tz0_C%s.root"%(nConf),"READ") 
        wSec = f.Get("w")
        f.Close()
        
        w.var("sigma_eta_1").setVal(wSec.var("sigma_eta_1").getValV())
        w.var("sigma_eta_1").setConstant(True)
        w.var("mass_Jpsi").setVal(wSec.var("mass_Jpsi").getValV())
        w.var("mass_Jpsi").setConstant(True)
        
        if(nConf == 7):
            w.var("mass_res").setVal(wSec.var("mass_res").getValV()+wSec.var("mass_res").getError())
        elif (nConf == 8):
            w.var("mass_res").setVal(wSec.var("mass_res").getValV()-wSec.var("mass_res").getError())
        else:
            w.var("mass_res").setVal(wSec.var("mass_res").getValV())
        
        #w.var("mass_res").setVal(110.2)  #etac2S2pp
        #w.var("mass_res").setVal(113.5)  #PDG
        #w.var("mass_res").setConstant(True)
        
        if (nPT != 0):

            #nameFile = "Results/MassFit/fromB/Jpsi_PT/Wksp_MassFit_PT%s_Tz0_C%s.root"%(nPT, nConf)
            #f = TFile(nameFile,"READ") 
            #wSec = f.Get("w;1")
            #f.Close()
            pt = [7250, 9000, 11000, 13000 , 16000] 
            f= TFile("Results/MC/MassFit/parFit.root","READ")
            fSigma = f.Get("fSigmaDATA")
            f.Close()
            w.var("sigma_eta_1").setVal(fSigma.Eval(pt[nPT-1])), 
        else:
            w.var("sigma_eta_1").setVal(wSec.var("sigma_eta_1").getValV())
        
        w.var("sigma_eta_1").setConstant(kTRUE)
        
        model.fitTo(hist,RooFit.Save(),RooFit.Extended(),RooFit.NumCPU(48),RooFit.ExternalConstraints(RooArgSet(fconstraint)))
        #modelC.fitTo(hist,RooFit.Save(),RooFit.Extended(),RooFit.NumCPU(48), RooFit.Constrain(RooArgSet(Jpsi_M)))
        
        # attempts to stabilize fits in tz bins
         
        #w.var("nBckgr").setConstant(True)
        #model.fitTo(hist,RooFit.Extended(),RooFit.NumCPU(48))
        #w.var("nBckgr").setConstant(False)
        
    #w.var("a1").setConstant(True)
    #w.var("a2").setConstant(True)
    
    
    model.fitTo(hist,RooFit.Extended(True),RooFit.Save(), RooFit.NumCPU(48),RooFit.ExternalConstraints(RooArgSet(fconstraint)))
    #modelC.fitTo(hist,RooFit.Extended(True),RooFit.Save(), RooFit.NumCPU(48), RooFit.Constrain(RooArgSet(Jpsi_M)))
    

    model.fitTo(hist,RooFit.Extended(True),RooFit.Offset(False),RooFit.Save(),RooFit.NumCPU(48),RooFit.ExternalConstraints(RooArgSet(fconstraint)))
    #r = model.fitTo(hist,RooFit.Extended(True),RooFit.Offset(False),RooFit.Save(),RooFit.NumCPU(48))
    #modelC.fitTo(hist,RooFit.Extended(True),RooFit.Offset(True),RooFit.Save(),RooFit.NumCPU(48), RooFit.Constrain(RooArgSet(Jpsi_M)))
    model.fitTo(hist,RooFit.Extended(True),RooFit.Offset(True),RooFit.Minos(True),RooFit.Save(),RooFit.NumCPU(48),RooFit.ExternalConstraints(RooArgSet(fconstraint)))        
    model.fitTo(hist,RooFit.Extended(True),RooFit.Offset(True),RooFit.Minos(True),RooFit.Save(),RooFit.NumCPU(48),RooFit.ExternalConstraints(RooArgSet(fconstraint)))
    model.fitTo(hist,RooFit.Extended(True),RooFit.Offset(True),RooFit.Minos(True),RooFit.Save(),RooFit.NumCPU(48),RooFit.ExternalConstraints(RooArgSet(fconstraint)))
    model.fitTo(hist,RooFit.Extended(True),RooFit.Offset(False),RooFit.Minos(True),RooFit.Save(),RooFit.NumCPU(48),RooFit.ExternalConstraints(RooArgSet(fconstraint)))
    model.fitTo(hist,RooFit.Extended(True),RooFit.Offset(False),RooFit.Minos(True),RooFit.Save(),RooFit.NumCPU(48),RooFit.ExternalConstraints(RooArgSet(fconstraint)))
    model.fitTo(hist,RooFit.Extended(True),RooFit.Offset(False),RooFit.Minos(True),RooFit.Save(),RooFit.NumCPU(48),RooFit.ExternalConstraints(RooArgSet(fconstraint)))
    model.fitTo(hist,RooFit.Extended(True),RooFit.Offset(False),RooFit.Minos(True),RooFit.Save(),RooFit.NumCPU(48),RooFit.ExternalConstraints(RooArgSet(fconstraint)))
    model.fitTo(hist,RooFit.Extended(True),RooFit.Offset(False),RooFit.Minos(True),RooFit.Save(),RooFit.NumCPU(48),RooFit.ExternalConstraints(RooArgSet(fconstraint)))
    model.fitTo(hist,RooFit.Extended(True),RooFit.Offset(False),RooFit.Minos(True),RooFit.Save(),RooFit.NumCPU(48),RooFit.ExternalConstraints(RooArgSet(fconstraint)))
    #model.fitTo(hist,RooFit.Extended(True),RooFit.Minos(True),RooFit.Save(),RooFit.NumCPU(48))
    #model.fitTo(hist,RooFit.Extended(True),RooFit.Minos(True),RooFit.Save(),RooFit.NumCPU(48))
    r = model.fitTo(hist,RooFit.Extended(True),RooFit.Offset(False),RooFit.Minos(True),RooFit.Save(),RooFit.NumCPU(48),RooFit.ExternalConstraints(RooArgSet(fconstraint)))
    #modelC.fitTo(hist,RooFit.Extended(True),RooFit.Minos(True),RooFit.Save(),RooFit.Offset(),RooFit.NumCPU(48), RooFit.Constrain(RooArgSet(Jpsi_M)))
    #r = modelC.fitTo(hist,RooFit.Extended(True),RooFit.Minos(True),RooFit.Save(),RooFit.Offset(),RooFit.NumCPU(48), RooFit.Constrain(RooArgSet(Jpsi_M)))
    
    #w.saveSnapshot("reference_fit",*params,False) 
    
    ## Construct plot frame and draw
    Title = RooFit.Title
    Components = RooFit.Components
    LineColor = RooFit.LineColor
    LineStyle = RooFit.LineStyle
    Normalization = RooFit.Normalization
    
    ## Construct plot frame and draw
    frame = Jpsi_M.frame(Title("J/#psi and #eta_{c} p.d.f.")) 
    hist.plotOn(frame,RooFit.Binning(40,2850,3250), RooFit.Name('histMass'))
    
    bwxg_1 = w.pdf("bwxg_1")
    bwxg_2 = w.pdf("bwxg_2")
    bwxg = RooArgSet(bwxg_1, bwxg_2) 
    
    gauss_1 = w.pdf("gauss_1")
    gauss_2 = w.pdf("gauss_2")
    gauss = RooArgSet(gauss_1, gauss_2) 
    signal = RooArgSet(gauss_1, gauss_2,bwxg_1, bwxg_2) 
    
    bkg = w.pdf("bkg")
    pppi0 = w.pdf("pppi0")
    bkgr = RooArgSet(RooArgList(bkg),pppi0)
#     model.paramOn(frame, Layout(0.68,0.99,0.99))
#     frame.getAttText().SetTextSize(0.027) 
    model.plotOn(frame,Normalization(1.0,RooAbsReal.RelativeExpected), RooFit.Name('pdfMass'))
    chi2 = frame.chiSquare()    
    model.plotOn(frame, Components(gauss), LineColor(2), LineStyle(9), Normalization(1.0, RooAbsReal.RelativeExpected))
    model.plotOn(frame,Components(bwxg),LineColor(7),LineStyle(9),Normalization(1.0,RooAbsReal.RelativeExpected))
    model.plotOn(frame,Components(RooArgSet(pppi0)),LineColor(6),LineStyle(9),Normalization(1.0,RooAbsReal.RelativeExpected))
    model.plotOn(frame,Components(bkgr),LineStyle(9),Normalization(1.0,RooAbsReal.RelativeExpected))
    
    
    hPull = frame.pullHist("histMass","pdfMass",True)    
    frame_pull = Jpsi_M.frame(Title("Pull Distribution"), RooFit.Bins(44)) 
    for ii in range(hPull.GetN()):

        hPull.SetPointEYlow(ii,0)
        hPull.SetPointEYhigh(ii,0)
    
    frame_pull.addPlotable(hPull,"B") 
    
    hResid = frame.residHist()
    frame_res = Jpsi_M.frame(Title("Residual Distribution"), RooFit.Bins(44)) 
    frame_res.addPlotable(hResid,"P") 
    model.plotOn(frame_res, Components(signal),Normalization(1.0,RooAbsReal.RelativeExpected))
    #chi2 = frame.chiSquare()
    model.plotOn(frame_res,Components(RooArgSet(RooArgList(bkg))),LineStyle(9),Normalization(0.0,RooAbsReal.RelativeExpected))
    model.plotOn(frame_res,Components(RooArgSet(pppi0)),LineColor(6),LineStyle(9),Normalization(1.0,RooAbsReal.RelativeExpected))

    c = TCanvas("Masses_Fit","Masses Fit",800,1000)
    c.Divide(1,3)
    c.cd(1).SetPad(.005, .505, .995, .995)
    gPad.SetLeftMargin(0.15),  frame.GetXaxis().SetTitle("J/#psi mass / [MeV/c^{2}]")  
    frame.Draw()
    c.cd(2).SetPad(.005, .255, .995, .495)
    gPad.SetLeftMargin(0.15),  frame_res.GetXaxis().SetTitle("J/#psi mass / [MeV/c^{2}]")  
    frame_res.Draw()
    c.cd(3).SetPad(.005, .005, .995, .245)
    gPad.SetLeftMargin(0.15),  frame_pull.GetXaxis().SetTitle("J/#psi mass / [MeV/c^{2}]")  
    frame_pull.Draw()
    #gPad.SetLeftMargin(0.15)  frame_pull.GetXaxis().SetTitle("J/#psi mass / [MeV/c^{2}]")  
    #frame_pull.Draw()
    
    
        ### Drawing gamma vs sigma_etac contour plot
    ##-----------------------------------------
    
    ##pdg = RooEllipse("PDG 2016",113.5,31.8,0.5, 0.8)
    ##pdg.Draw()

    #nll = model.createNLL(hist,RooFit.NumCPU(48),RooFit.Extended(True),RooFit.Minos(True),RooFit.Offset()) 
    #m = RooMinuit(nll) 
    #fCont = m.contour(mass_res,nEta,1,2)
    ##fCont.addPlotable(pdg/, "same"/)
    
    
    #cCont = TCanvas("ContourPlor","Masses Fit",500,400)
    #cCont.cd()
    #gPad.SetLeftMargin(0.15) 
    #fCont.GetXaxis().SetTitle("#Delta M_{J/#psi,#eta_{c}(1S)}, [MeV/c^{2}]") 
    #fCont.GetYaxis().SetTitle("#N_{#eta_{c}(1S)}/N_{J/#psi}") 
    #fCont.Draw("SAME")
    ##pdg.SetLineColor(8)
    ##pdg.DrawClone("SAME")

    #cCont.SaveAs("ContourPlotMass.pdf")

    
    
    #w.var("a0").setConstant(True)
    #w.var("a1").setConstant(True)
    #w.var("a2").setConstant(True)
    #w.var("mass_res").setConstant(True)
    #w.var("nBckgr").setConstant(True)
    #w.var("nJpsi").setConstant(True)

    
        ## C o n s t r u c t   p l a i n   l i k e l i h o o d
    ## ---------------------------------------------------
        
    # Construct unbinned likelihood
    #nll = model.createNLL(hist,RooFit.NumCPU(48),RooFit.Extended(True),RooFit.Minos(True),RooFit.Offset()) 
    
    ## Minimize likelihood w.r.t all parameters before making plots
    #RooMinuit(nll).minos() 
    
    ## Plot likelihood scan frac 
    #frameRS = mass_res.frame(RooFit.Bins(10),RooFit.Range(100.00,130.00),RooFit.Title("LL and profileLL in #Delta m_{#eta_{c}} ")) 
##     nll.plotOn(frame1,ShiftToZero()) 
        
    ## The profile likelihood estimator on nll for frac will minimize nll w.r.t
    ## all floating parameters except frac for each evaluation
    
    #pll_mass = nll.createProfile(RooArgSet(mass_res)) 
    
    ## Plot the profile likelihood in frac
    #pll_mass.plotOn(frameRS,RooFit.LineColor(2)) 
    
    ## Adjust frame maximum for visual clarity
    #frameRS.SetMinimum(0) 
    
    #cNLL = TCanvas("rf605_profilell","rf605_profilell",800, 400)
    #cNLL.cd(),  gPad.SetLeftMargin(0.15),  frameRS.GetYaxis().SetTitleOffset(1.4),  frameRS.Draw() 

    #cNLL.SaveAs("m_res_NLL.pdf")

    
    #Plot likelihood scan frac 
    #frameY = nEta.frame(RooFit.Bins(10),RooFit.Range(0.00,2.00),RooFit.Title("LL and profileLL in N_{#eta_{c}}/N_{J/#psi} ")) 
##     nll.plotOn(frame1,ShiftToZero()) 
        
    ## The profile likelihood estimator on nll for frac will minimize nll w.r.t
    ## all floating parameters except frac for each evaluation
    
    #pll_eta = nll.createProfile(RooArgSet(nEta)) 
    
    ## Plot the profile likelihood in frac
    #pll_eta.plotOn(frameY,RooFit.LineColor(2)) 
    
    ## Adjust frame maximum for visual clarity
    #frameY.SetMinimum(-10.) 
    
    #cNLL = TCanvas("rf605_profilell","rf605_profilell",800, 400)
    #cNLL.cd(),  gPad.SetLeftMargin(0.15),  frameY.GetYaxis().SetTitleOffset(1.4),  frameY.Draw() 

    #cNLL.SaveAs("eta_NLL.pdf")
    
    
    if keyPrompt: inCutKey = "prompt"
    else:         inCutKey = "fromB"
    
    if nPT!=0:    binPT = "Jpsi_PT/"
    else:         binPT = "Total/"

    #binTz = "Tz%s.root"%(nTz)
    
    dirName = "~/scripts/Results/MassFit/" + inCutKey + "/" + binPT
    
    if (nPT!=0 and nTz!=0):
        dirName = dirName + charm + "/"
        
    nameTxt  = dirName + "fitRes_PT%s_Tz%d_C%d.txt"%(nPT,nTz,nConf)
    nameRoot = dirName + "Jpsi_MassFit_PT%s_Tz%d_C%d.root"%(nPT,nTz,nConf)
    namePic  = dirName + "Jpsi_MassFit_PT%s_Tz%d_C%d.pdf"%(nPT,nTz,nConf)
    nameWksp = dirName + "Wksp_MassFit_PT%s_Tz%d_C%d.root"%(nPT,nTz,nConf)
    

    fo = open(nameTxt, "w")
    
    fo.write( "nConf = %s \n"%(nConf))
    #fo.write( "nTz = " nTz
    fo.write( "edm = %2.2e \n"%(r.edm()))
    fo.write( "chi2/NDF = %3.2f \n \n"%(chi2))
    fo.write( "a0 = %2.2e +/- %2.2e\n"%(w.var("a0").getValV(), w.var("a0").getError()))
    fo.write( "a1 = %2.2e +/- %2.2e\n"%(w.var("a1").getValV(), w.var("a1").getError()))
    fo.write( "a2 = %2.2e +/- %2.2e\n"%(w.var("a2").getValV(), w.var("a2").getError()))
    if(nConf == 5):
        fo.write( "a3 = %2.2e +/- %2.2e\n"%(w.var("a3").getValV(), w.var("a3").getError()))
    #fo.write( "a4 = " w.var("a4").getValV(), w.var("a4").getError()

    if  not(keyPrompt):
        fo.write( "sigmaEtaNar = %2.3f +/- %2.3f \n"%( w.var("sigma_eta_1").getValV(), w.var("sigma_eta_1").getError()))
        fo.write( "mass J/psi = %4.2f +/- %4.2f \n"%( w.var("mass_Jpsi").getValV(), w.var("mass_Jpsi").getError()))

    fo.write( "mass diff = %3.2f +/- %3.2f \n"%( w.var("mass_res").getValV(), w.var("mass_res").getError()))
    fo.write( "nBackground = %5.2f +/- %5.2f \n"%(w.var("nBckgr").getValV(), w.var("nBckgr").getError()))
    fo.write( "nEta = %5.3f +/- %5.3f "%( w.var("nEtacRel").getValV(), w.var("nEtacRel").getError()))
    fo.write( " %3.2f \n"%(w.var("nEtacRel").getValV()/w.var("nEtacRel").getError()))
    #fo.write( "nEta = %5.2f +/- %5.2f "%( w.var("nEta").getValV(), w.var("nEta").getError()))
    #fo.write( " %3.2f \n"%(w.var("nEta").getValV()/w.var("nEta").getError()))
    fo.write( "nJpsi = %5.2f +/- %5.2f "%( w.var("nJpsi").getValV(), w.var("nJpsi").getError()))
    fo.write( " %3.2f \n"%(w.var("nJpsi").getValV()/w.var("nJpsi").getError()))
    fo.write( "n pppi0 = %5.2f \n"%( 0.06*1.19/2.12*w.var("nJpsi").getValV()))

    fo.close()
    
    #w.loadSnapshot("reference_fit")
    w.writeToFile(nameWksp)
    
    fFit = TFile (nameRoot,"RECREATE")
    w.Print("v")
    c.Write("")
    fFit.Close()

    c.SaveAs(namePic)
    #r.correlationMatrix().Print()



def Fit_Mass():

    nPT = 5
    nTzTot = 10
    nTzEtac = 5
    nTzJpsi = 7
    nConf = 11
    iConf = 0
    pr = True
    #    Bool_t pr = False
    #    for (Int_t iConf=0 iConf<10 iConf++)
    #    {
    #        fitData(iConf,0,pr)
    #    }
    #   Int_t iPT=0
    #fitData(0, 0, 0, False, 'Etac')
    #for iPT in range (1, nPT+1):        
            #fitData(iPT, 0, iConf, False, 'Etac')
    #for iTz in range (1, nTzTot+1):        
            #fitData(0, iTz, iConf, pr, 'Etac')
    #for iPT in range (1, nPT+1):        
            #fitData(iPT, 0, iConf, pr, 'Etac')
            
    #for iPT in range(1, nPT+1):
        #for iTz in range (1, nTzJpsi+1):        
            #fitData(iPT, iTz, iConf, pr, 'Jpsi')
    #for iPT in range(1, nPT+1):
        #for iTz in range (1, nTzEtac+1):        
            #fitData(iPT, iTz, iConf, pr, 'Etac')

#w = RooWorkspace("w",kTRUE)   
#fillRelWorkspace(w,0,0)
#getData_h(w, False, 1, 0,"Etac")

#fitData( 0, 0, 4, False)
fitData( 0, 0, 4, True)
fitData( 0, 0, 0, True)
#fitData( 1, 2, 0, True)
            
#Fit_Mass()
