from ROOT import *
from ROOT.RooFit import *
from ROOT import kRed, kBlack, kDashed, kMagenta, kCyan, kViolet, kTRUE
from array import *

gStyle.SetOptStat(000)


import sys
sys.path.insert(0, '../')
from makeConfigDictionary import *

sidebandCut = "(Jpsi_m_scaled>2850 && Jpsi_m_scaled<2920)"

dataChain = TChain("DecayTree")
mcChain = TChain("DecayTree")

dataMCfactor = 0.291*43464/(1314.)


for file in MCTuples["b2etacX"]["SndTopo"]:
    mcChain.Add(file)

dataChain.Add(tuplesSnd2015)
dataChain.Add(tuplesSnd2016)



def getSignif(cut):
    cutFull = cut + " && " + sidebandCut
    nBkg = 100./70.*(float)(dataChain.GetEntries(cutFull))
    nSig = (float)(mcChain.GetEntries(cut))
    return dataMCfactor*nSig/TMath.Sqrt(nBkg+nSig*dataMCfactor)



CutsOptDict = {"Jpsi_PT"              :[4000,4500,5000,5500,6000,6500,7000,7500],
               "proton_PT"            :[1000,1250,1500,1750,2000,2500],
               "proton_IPCHI2"        :[9,16,25,36],
               "Jpsi_FDCHI2_OWNPV"    :[25,30,36,49,64,81,100,121,144],
               "proton_TRACK_CHI2NDOF":[1,2,3,4,5,6]
              }

nJpsiPTbins = len(CutsOptDict["Jpsi_PT"])-1
nProtonPTbins = len(CutsOptDict["proton_PT"])-1
nJpsiFDbins = len(CutsOptDict["Jpsi_FDCHI2_OWNPV"])-1
nProtonIPbins = len(CutsOptDict["proton_IPCHI2"])-1
nProtonTRACKCHI2 = len(CutsOptDict["proton_TRACK_CHI2NDOF"])-1

JpsiPTbinning   = array("d", CutsOptDict["Jpsi_PT"])
ProtonPTbinning = array("d", CutsOptDict["proton_PT"])
JpsiFDbinning   = array("d", CutsOptDict["Jpsi_FDCHI2_OWNPV"])
ProtonIPbinning = array("d", CutsOptDict["proton_IPCHI2"])
ProtonTRACKCHI2binning = array("d", CutsOptDict["proton_TRACK_CHI2NDOF"])

def findOptimalPoint():

    maxSignif = -99999999.
    bestIP = -1
    bestFD = -1
    bestJpsiPT = -1
    bestProtonPT = -1
    bestTrackCHI2 = -1

    for iJpsiPTcut in range(nJpsiPTbins):
      for iProtonPTcut in range(nProtonPTbins):
        for iJpsiFDcut in range(nJpsiFDbins):
          for iProtonIPcut in range(nProtonIPbins):
            for iProtonTRACKCHI2 in range(nProtonIPbins):
              JpsiPTcut = CutsOptDict["Jpsi_PT"][iJpsiPTcut]
              ProtonPTcut = CutsOptDict["proton_PT"][iProtonPTcut]
              JpsiFDcut = CutsOptDict["Jpsi_FDCHI2_OWNPV"][iJpsiFDcut]
              ProtonIPcut = CutsOptDict["proton_IPCHI2"][iProtonIPcut]
              ProtonTRACKCHI2cut = CutsOptDict["proton_TRACK_CHI2NDOF"][iProtonTRACKCHI2]
              cut = "(Jpsi_PT>"+str(JpsiPTcut)+")      && \
                     (ProtonP_PT>"+str(ProtonPTcut)+") && \
                     (ProtonM_PT>"+str(ProtonPTcut)+") && \
                     (Jpsi_FDCHI2_OWNPV>"+str(JpsiFDcut)+") && \
                     (ProtonP_IPCHI2_OWNPV>"+str(ProtonIPcut)+") && \
                     (ProtonM_IPCHI2_OWNPV>"+str(ProtonIPcut)+") && \
                     (ProtonP_TRACK_CHI2NDOF<"+str(ProtonTRACKCHI2cut)+") && \
                     (ProtonM_TRACK_CHI2NDOF<"+str(ProtonTRACKCHI2cut)+")"
              signif = getSignif(cut)
              if (signif > maxSignif):
                maxSignif = signif
                bestIP = iProtonIPcut
                bestFD = iJpsiFDcut
                bestJpsiPT = iJpsiPTcut
                bestProtonPT = iProtonPTcut
                bestTrackCHI2 = iProtonTRACKCHI2

    print "bestJpsiPT (nbin)="    , bestJpsiPT+1,   ",\n \
           bestProtonPT (nbin)="  , bestProtonPT+1, ",\n \
           bestIP (nbin)="        , bestIP+1,       ",\n \
           bestFD (nbin)="        , bestFD+1,       ",\n \
           bestTrackCHI2 (nbin)=" , bestTrackCHI2+1
      
    return bestJpsiPT, bestProtonPT, bestIP, bestFD, bestTrackCHI2



bestJpsiPT, bestProtonPT, bestIP, bestFD, bestTrackCHI2 = findOptimalPoint()

def makePToptimization():

    optHist = TH2F("optHist","optHist",nJpsiPTbins,JpsiPTbinning,nProtonPTbins,ProtonPTbinning)
    optHist.GetXaxis().SetTitle("P_{T}(p#bar{p}), MeV/c^{2}")
    optHist.GetYaxis().SetTitle("P_{T}(p), MeV/c^{2}")
#    optHist.GetZaxis().SetTitle("FoM")
    optHist.GetXaxis().SetTitleSize(0.05)
    optHist.GetYaxis().SetTitleSize(0.05)
    optHist.GetXaxis().SetTitleOffset(0.8)
    optHist.GetYaxis().SetTitleOffset(1.0)
    optHist.GetXaxis().SetLabelSize(0.04)
    optHist.GetYaxis().SetLabelSize(0.04)
    optHist.GetZaxis().SetLabelSize(0.04)
    optHist.GetZaxis().SetLabelSize(0.04)
    for iJpsiPTcut in range(nJpsiPTbins):
      for iProtonPTcut in range(nProtonPTbins):
        JpsiPTcut = CutsOptDict["Jpsi_PT"][iJpsiPTcut]
        ProtonPTcut = CutsOptDict["proton_PT"][iProtonPTcut]

        JpsiFDcut = CutsOptDict["Jpsi_FDCHI2_OWNPV"][bestFD]
        ProtonIPcut = CutsOptDict["proton_IPCHI2"][bestIP]

        cut = "(Jpsi_FDCHI2_OWNPV>"+str(JpsiFDcut)+") && \
               (ProtonP_IPCHI2_OWNPV>"+str(ProtonIPcut)+") && \
               (ProtonM_IPCHI2_OWNPV>"+str(ProtonIPcut)+") && \
               (Jpsi_PT>"+str(JpsiPTcut)+") && \
               (ProtonP_PT>"+str(ProtonPTcut)+") && \
               (ProtonM_PT>"+str(ProtonPTcut)+")"

        signif = getSignif(cut)
        optHist.SetBinContent(iJpsiPTcut+1,iProtonPTcut+1,signif)

    c = TCanvas()
    optHist.Draw("COLZ")

    optHist.SaveAs("OptimizePT.root")
    c.SaveAs("OptimizePT.pdf")



def makeIPoptimization():

    optHist = TH2F("optHist","optHist",nJpsiFDbins,JpsiFDbinning,nProtonIPbins,ProtonIPbinning)
    optHist.GetXaxis().SetTitle("#chi^{2}(FD)")
    optHist.GetYaxis().SetTitle("#chi^{2}_{IP}(p)")
#    optHist.GetZaxis().SetTitle("FoM")
    optHist.GetXaxis().SetTitleSize(0.05)
    optHist.GetYaxis().SetTitleSize(0.05)
    optHist.GetXaxis().SetTitleOffset(0.8)
    optHist.GetYaxis().SetTitleOffset(1.0)
    optHist.GetXaxis().SetLabelSize(0.04)
    optHist.GetYaxis().SetLabelSize(0.04)
    optHist.GetZaxis().SetLabelSize(0.04)
    optHist.GetZaxis().SetLabelSize(0.04)
    for iJpsiFDcut in range(nJpsiFDbins):
      for iProtonIPcut in range(nProtonIPbins):
        JpsiPTcut = CutsOptDict["Jpsi_PT"][bestJpsiPT]
        ProtonPTcut = CutsOptDict["proton_PT"][bestProtonPT]

        JpsiFDcut = CutsOptDict["Jpsi_FDCHI2_OWNPV"][iJpsiFDcut]
        ProtonIPcut = CutsOptDict["proton_IPCHI2"][iProtonIPcut]
        cut = "(Jpsi_FDCHI2_OWNPV>"+str(JpsiFDcut)+") && \
               (ProtonP_IPCHI2_OWNPV>"+str(ProtonIPcut)+") && \
               (ProtonM_IPCHI2_OWNPV>"+str(ProtonIPcut)+") && \
               (Jpsi_PT>"+str(JpsiPTcut)+") && \
               (ProtonP_PT>"+str(ProtonPTcut)+") && \
               (ProtonM_PT>"+str(ProtonPTcut)+")"

        signif = getSignif(cut)
        optHist.SetBinContent(iJpsiFDcut+1,iProtonIPcut+1,signif)

    c = TCanvas()
    optHist.Draw("COLZ")
    optHist.SaveAs("OptimizeIP.root")
    c.SaveAs("OptimizeIP.pdf")



makePToptimization()
makeIPoptimization()


