from ROOT import *
from ROOT.RooFit import *

import sys, os

sys.path.insert(0, '../')
from makeConfigDictionary import *




bold = "\033[1m"
reset = "\033[0;0m"
def makeHistos(nt, year="2016", trigger="DiProton", inCutKey="secondary", var="Jpsi_PT", doBinning=False, doTz=False):
    # gROOT.Reset()
    inCut = cutsDict[inCutKey]

    nMassBins = int((maxMass-minMass)/binWidth)
    Jpsi_M = TH1F("Jpsi_M","Jpsi_M" , nMassBins, minMass, maxMass)
    Jpsi_M.SetLineColor(4)
    Jpsi_M.GetXaxis().SetTitle("J/#psi_Mass / MeV ")
    Jpsi_M.GetYaxis().SetTitle("Entries")
    Jpsi_M.SetMinimum(0.0)

    cutMass = "(Jpsi_m_scaled>" + str(minMass) + ") && (Jpsi_m_scaled<" + str(maxMass) + ")"

    if year=="2015": cutApriori = cutApriori2015_Dict[trigger]
    if year=="2016": cutApriori = cutApriori2016_Dict[trigger]

    if doBinning:
        binningFile = "binning.txt"
# !!!!!!!!
        if doTz:
            nTzBins = len(tzInBinsBinning)-1
            for iTz in range(nTzBins):
                print bold+"  I\'m doing Tz binning. Bin No", (iTz+1), reset
                tzLowEdge = tzInBinsBinning[iTz]
                tzHighEdge = tzInBinsBinning[iTz+1]
                cutTz = "("+tzVar+">"+str(tzLowEdge)+") && ("+tzVar+"<"+str(tzHighEdge)+")"

                varBinning = binningDict[var]
                nVarBins =  len(varBinning)-1
                for iVar in range(nVarBins):
                    print bold+"        Now I\'m doing", var, "binning. Bin No", (iVar+1), reset
                    varLowEdge = varBinning[iVar]
                    varHighEdge = varBinning[iVar+1]
                    cutVar = "("+var+">"+str(varLowEdge)+") && ("+var+"<"+str(varHighEdge)+")"

                    sumCut = cutMass    +" && "+ \
                             cutApriori +" && "+ \
                             inCut      +" && "+ \
                             cutTz      +" && "+ \
                             cutVar

                    dirName = histosDir+year+"/"+trigger+"/"+inCutKey+"/"+var
                    if not os.path.exists(dirName):
                        os.makedirs(dirName)
                    fileName = dirName + "/bin"+str(iVar+1)+"_Tz"+str(iTz+1)+".root"
                    nt.Draw("Jpsi_m_scaled>>Jpsi_M", sumCut)
                    Jpsi_M.SaveAs(fileName)
        else:
            varBinning = binningDict[var]
            nVarBins =  len(varBinning)-1
            print bold+"I DON\'T do Tz binning",reset
            for i in range(nVarBins):
                print bold+"    Now I\'m doing", var, "binning. Bin No", (i+1), ", Tz integrated", reset
                varLowEdge = varBinning[i]
                varHighEdge = varBinning[i+1]
                cutVar = "("+var+">"+str(varLowEdge)+") && ("+var+"<"+str(varHighEdge)+")"

                sumCut = cutMass    +" && "+ \
                         cutApriori +" && "+ \
                         inCut      +" && "+ \
                         cutVar

                dirName = histosDir+year+"/"+trigger+"/"+inCutKey+"/"+var
                if not os.path.exists(dirName):
                    os.makedirs(dirName)
                fileName = dirName + "/bin"+str(i+1)+"_Tz0.root"
                nt.Draw("Jpsi_m_scaled>>Jpsi_M", sumCut)
                Jpsi_M.SaveAs(fileName)
    else:
        print bold+"I\'m doing Total histos"+reset
        if doTz:
            nTzBins = len(tzTotalBinning)-1
            for i in range(nTzBins):
                print bold+"     I\'m doing Tz binning for Total histos. Bin No", (i+1), reset
                tzLowEdge = tzTotalBinning[i]
                tzHighEdge = tzTotalBinning[i+1]
                cutTz = "("+tzVar+">"+str(tzLowEdge)+") && ("+tzVar+"<"+str(tzHighEdge)+")"

                sumCut = cutMass    +" && "+ \
                         cutApriori +" && "+ \
                         inCut      +" && "+ \
                         cutTz

                dirName = histosDir + year + "/" + trigger + "/" + inCutKey + "/Total"
                if not os.path.exists(dirName):
                    os.makedirs(dirName)
                fileName = dirName + "/Tz" + str(i+1) + ".root"
                nt.Draw("Jpsi_m_scaled>>Jpsi_M", sumCut)
                Jpsi_M.SaveAs(fileName)
        else:
            print bold+"     I DON\'T do Tz binning for Total histos"+reset
            sumCut = cutMass    +" && "+ \
                     cutApriori +" && "+ \
                     inCut

            cut = TCut(sumCut)

            dirName = histosDir + year + "/" + trigger + "/" + inCutKey + "/Total"
            if not os.path.exists(dirName):
                os.makedirs(dirName)
            fileName = dirName + "/Tz0.root"
            nt.Draw("Jpsi_m_scaled>>Jpsi_M", cut)
            Jpsi_M.SaveAs(fileName)
    del Jpsi_M




def makeAllHistos():

    inCutKeys= ["secondary_l0TOS","prompt_l0TOS"]#,"all_l0TOS","all_l0TIS",]
    trigger = "DiProton"


    #years = #["2015","2016]"
    years = ["2016"]
    # variables = ["Jpsi_ETA","TMath::Abs(ProtonM_CosTheta)","nSPDHits"]
    variables = ["Jpsi_PT"]
    # variables = ["nSPDHits"]


    for year in years:
      nt = TChain("Jpsi2ppTuple/DecayTree")
      if (year=="2016"):
        nt.Add(tuples2016[0]);nt.Add(tuples2016[1])
      if (year=="2015"): 
        nt.Add(tuples2015[0]);nt.Add(tuples2015[1])
      for var in variables:
        for inCutKey in inCutKeys:
          print "Setup Cuts: ", bold+inCutKey, reset
          print "      year: ", bold+year, reset,"\n"

          makeHistos(nt,year,trigger,inCutKey,var,doBinning=False, doTz=False)
          makeHistos(nt,year,trigger,inCutKey,var,doBinning=True, doTz=False)
          # makeHistos(nt,year,trigger,inCutKey,var,doTz=True)
      del nt

makeAllHistos()


