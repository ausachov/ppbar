from ROOT import *
from ROOT.TMath import Sqrt
gROOT.LoadMacro("../libRooRelBreitWigner/RooRelBreitWigner.cxx+")
gROOT.LoadMacro("../BifurcatedCB/BifurcatedCB.cxx+")


from array import array

jpsiMass = 3096.900
masses = {
  "BES III"                    : { "val": 2984.3, "stat": 0.6, "syst": 0.6},
  "BABAR"                      : { "val": 2984.1, "stat": 1.1, "syst": 2.1},
  "LHCb b#to p#bar{p}X, Run I" : { "val": 2982.2, "stat": 1.5, "syst": 0.6},
  "LHCb b#to#phi#phiX"         : { "val": 2982.8, "stat": 1.0, "syst": 0.5},
  "LHCb B^{+}#to p#bar{p}K^{+}": { "val": 2986.7, "stat": 0.5, "syst": 0.9},
  "PDG 2017"                   : { "val": 2983.4, "stat": 0.5, "syst": 0.},
  "This measurement"           : { "val": 2983.91,"stat": 0.66, "syst": 0.11}
}

keys = ["BABAR",
        "BES III",
        "LHCb b#to p#bar{p}X, Run I",
        "LHCb b#to#phi#phiX",
        "LHCb B^{+}#to p#bar{p}K^{+}",
        "PDG 2017",
        "This measurement"]


c = TCanvas()
c.SetBottomMargin(0.15)
grs = []
grsTot = []

for key in keys:


    val  = masses[key]["val"]
    stat = masses[key]["stat"]
    syst = masses[key]["syst"]
    tot  = Sqrt(stat*stat+syst*syst)
    iD = keys.index(key)
    nID = len(keys)

    gr = TGraphErrors(1,array("d",[-val+jpsiMass]), 
                        array("d",[nID-iD]),
                        array("d",[stat]),
                        array("d",[0.]))
    gr.SetLineWidth(2)
    gr.SetMarkerStyle(8)


    grTot = TGraphErrors(1,array("d",[-val+jpsiMass]), 
                           array("d",[nID-iD]),
                           array("d",[tot]),
                           array("d",[0.]))

    grTot.SetLineColor(2)
    if key=="PDG 2017": 
        grTot.SetLineColor(4)
        gr.SetLineColor(4)
        gr.SetMarkerColor(4)
    if key=="This measurement": 
        gr.SetLineColor(6)
        gr.SetMarkerColor(6)

    grTot.SetTitle("  ")
    grTot.GetYaxis().SetRangeUser(0,nID+1)
    grTot.GetYaxis().SetNdivisions(0)
    grTot.GetXaxis().SetLimits(108.,123.0)

    grTot.GetXaxis().SetTitle("M_{J/#psi}-M_{#eta_{c}}, MeV/c^{2}")
    grTot.GetXaxis().SetLabelSize(0.04)
    grTot.GetXaxis().SetTitleOffset(1.15)
    grTot.GetXaxis().SetTitleSize(0.045)

    grTot.GetYaxis().SetLabelSize(0)

    grTot.SetLineWidth(2)
    grTot.SetMarkerStyle(8)

    grs.append(gr)
    grsTot.append(grTot)

    print -val+jpsiMass

    if iD==0: 
        grsTot[iD].Draw("AP")
    else:     
        grsTot[iD].Draw("Psame")
    grs[iD].Draw("Psame")
    grTot.SaveAs("grTot.root")

c.SaveAs("massMeasurements.pdf")

