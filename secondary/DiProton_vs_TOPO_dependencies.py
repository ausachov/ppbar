from ROOT import *
from ROOT.RooFit import *
from ROOT import kRed, kBlack, kDashed, kMagenta, kCyan, kViolet, kTRUE
import random

promptDataTuple = "/users/LHCb/zhovkovska/scripts/Jpsi_Cuts/Jpsi_M_prompt.root"
frombDataTuple  = "/users/LHCb/zhovkovska/scripts/Jpsi_Cuts/Jpsi_M_fromB.root"

minM = 2900
maxM = 3200
nBins = 120



def getData(w, nPT=0, nTz=0, isPrompt=True, shift=False):

    if(nPT>0):
        frombDataTuple = "/users/LHCb/zhovkovska/scripts/Jpsi_Cuts/Jpsi_M_fromB"+".root"

    if(isPrompt):
        f = TFile(promptDataTuple)
    else:
        f = TFile(frombDataTuple)

    Jpsi_M = w.var("Jpsi_M")

    hh = TH1F("hh","hh",nBins,minM,maxM)
    if(nTz != 0):
        name = "Jpsi_M;"+str(nTz)
        hhh = f.Get(name)
    else:
        hhh = f.Get("Jpsi_M")
        for ii in range(hh.GetNbinsX()):
            median = hh.GetBinCenter(ii+1)
            nbin = hhh.FindBin(median)
            hh.SetBinContent(ii+1,hhh.GetBinContent(nbin))

    if(shift):
        hh.SetBins(276,2705,3395)
        Jpsi_M.setRange(2855,3245)

    dh = RooDataHist("dh","dh",RooArgList(Jpsi_M),hh)
    # frame1 = Jpsi_M.frame()
    # dh.plotOn(frame1,RooFit.Binning(160,2850,3250))
    # c1 = TCanvas("c1")
    # frame1.Draw()
    # c1.SaveAs("nn.pdf")



    # getattr(w,'import')(Jpsi_M)
    getattr(w,'import')(dh)
    f.Close()



def getConstPars(nConf, nPT):

    if(nPT==0) : MCResolutionFile = "results/MC_2016_wksp.root"
    elif(nPT>0): MCResolutionFile = "results/MC_2016_wksp"+".root"

    f = TFile(MCResolutionFile,"READ")
    wMC = f.Get("w;1")
    f.Close()

    ratioNtoW = wMC.var("rNarToW").getValV()  
    ratioEtaToJpsi = wMC.var("rEtaToJpsi").getValV()
    ratioArea = wMC.var("rG1toG2").getValV()


    return ratioNtoW, ratioEtaToJpsi, ratioArea



def fillRelWorkspace(w, nConf, nPT):

    Jpsi_M = RooRealVar("Jpsi_M","Jpsi_M",minM,maxM)

    # Jpsi_M.setBins(160,"cache");
    # Jpsi_M.setRange("SBLeft", 2850, 2900);
    # Jpsi_M.setRange("SBCentral", 3035, 3060);
    # Jpsi_M.setRange("SBRight", 3150, 3250);
    # Jpsi_M.setRange("SignalEtac", 2900, 3035);
    # Jpsi_M.setRange("SignalJpsi", 3060, 3150);
    # Jpsi_M.setRange("Total", 2850, 3250);

    ratioNtoW, ratioEtaToJpsi, ratioArea = getConstPars(nConf, nPT)


    gammaEtac = 31.8;
    mDiffEtac = 113.301;
    eff = 0.035;
    bkgType = 0;


    print "SETUP ", nConf
    if   nConf==0:  bkgType = 0
    elif nConf==4:  bkgType=1
    elif nConf==5:  bkgType=2
    elif nConf==6:  gammaEtac = 32.6
    elif nConf==9:  eff = 0.032
    elif nConf==10: Jpsi_M.setRange(2855,3245)
    else :
       bkgType=0
       print "DEFAULT SETUP"
    


    rEtaToJpsi = RooRealVar("rEtaToJpsi","rEtaToJpsi", ratioEtaToJpsi)
    rNarToW    = RooRealVar("rNarToW","rNarToW",ratioNtoW)
    rG1toG2    = RooRealVar("rG1toG2","rG1toG2",ratioArea)

    eff_pppi0  = RooRealVar("eff_pppi0","eff_pppi0",eff)
    nEta       = RooRealVar("nEta","num of Etac", 10, 1.e7)
    nJpsi      = RooRealVar("nJpsi","num of J/Psi", 10, 1.e7)
    nEtacRel   = RooRealVar("nEtacRel","num of Etac", 0.0, 3.0)

    nEta_1 = RooFormulaVar("nEta_1","num of Etac","@0*@1*@2",RooArgList(nEtacRel,nJpsi,rG1toG2))
    nEta_2 = RooFormulaVar("nEta_2","num of Etac","@0*@1-@2",RooArgList(nEtacRel,nJpsi,nEta_1))

    nJpsi_1 = RooFormulaVar("nJpsi_1","num of J/Psi","@0*@1",RooArgList(nJpsi,rG1toG2))
    nJpsi_2 = RooFormulaVar("nJpsi_2","num of J/Psi","@0-@1",RooArgList(nJpsi,nJpsi_1))
    nBckgr  = RooRealVar("nBckgr","num of backgr",1e7,10,1.e+9)
   
   
    mass_Jpsi = RooRealVar("mass_Jpsi","mean of gaussian",3096.9, 3030, 3150)   
    mass_res  = RooRealVar("mass_res","mean of gaussian",mDiffEtac, 100, 130)
 
    if (nConf==7) or (nConf==8): mass_res.setConstant(True)

    mass_eta    = RooFormulaVar("mass_eta","mean of gaussian","@0-@1",RooArgList(mass_Jpsi,mass_res))
    gamma_eta   = RooRealVar("gamma_eta","width of Br-W", gammaEtac, 5., 80.)
    spin_eta    = RooRealVar("spin_eta","spin_eta", 0.)
    radius_eta  = RooRealVar("radius_eta","radius", 1.)
    proton_m    = RooRealVar("proton_m","proton mass", 938.3 )
    sigma_eta_1 = RooRealVar("sigma_eta_1","width of gaussian", 9., 5., 50.) 
    sigma_eta_2 = RooFormulaVar("sigma_eta_2","width of gaussian","@0/@1",RooArgList(sigma_eta_1,rNarToW))

    sigma_Jpsi_1 = RooFormulaVar("sigma_Jpsi_1","width of gaussian","@0/@1",RooArgList(sigma_eta_1,rEtaToJpsi))
    sigma_Jpsi_2 = RooFormulaVar("sigma_Jpsi_2","width of gaussian","@0/@1",RooArgList(sigma_Jpsi_1,rNarToW))


    # Fit eta
    gaussEta_1 = RooGaussian("gaussEta_1","gaussEta_1 PDF", Jpsi_M, RooFit.RooConst(0),  sigma_eta_1)
    gaussEta_2 = RooGaussian("gaussEta_2","gaussEta_2 PDF", Jpsi_M, RooFit.RooConst(0),  sigma_eta_2)

    br_wigner = RooRelBreitWigner("br_wigner", "br_wigner",Jpsi_M, mass_eta, gamma_eta, spin_eta,radius_eta,proton_m,proton_m)

    bwxg_1 = RooFFTConvPdf("bwxg_1","breit-wigner (X) gauss", Jpsi_M, br_wigner, gaussEta_1)
    bwxg_2 = RooFFTConvPdf("bwxg_2","breit-wigner (X) gauss", Jpsi_M, br_wigner, gaussEta_2)

    # Fit J/psi
    gauss_1 = RooGaussian("gauss_1","gaussian PDF",Jpsi_M,mass_Jpsi,sigma_Jpsi_1)
    gauss_2 = RooGaussian("gauss_2","gaussian PDF",Jpsi_M,mass_Jpsi,sigma_Jpsi_2)

    # RooGaussian fconstraint("fconstraint","fconstraint",mass_res,RooFit.RooConst(113.5),RooFit.RooConst(2.0)) ;
    # RooGaussian fconstrJpsi("fconstrJpsi","fconstraint",mass_Jpsi,RooFit.RooConst(3096.9),RooFit.RooConst(0.5)) ;

    a0 = RooRealVar("a0","a0",-0.4,-1.0,10.8)
    a1 = RooRealVar("a1","a1",0.05,-0.5,0.5)
    a2 = RooRealVar("a2","a2",-0.005,-0.5,0.5)
    a3 = RooRealVar("a3","a3",0.005,-0.1,0.1)
    a4 = RooRealVar("a4","a4",0.005,-0.1,0.1)

    a5 = RooRealVar("a5","a5",-0.0001,-0.01,0)

    if   bkgType==1:
        bkg = RooChebychev("bkg","Background",Jpsi_M,RooArgList(a0,a1,a2,a3,a4))
    elif bkgType==2:
        a2.setVal(0.0)
        a2.setConstant(True)
        bkg = RooGenericPdf("bkg","background","TMath::Exp(-(@0-3050.)/200.*@1)*(1.+@2*(@0-3050.)/200.+@3*(@0-3050.)*(@0-3050.)/200./200.)",RooArgList(Jpsi_M,a0,a1,a2))
    else:
        bkg = RooChebychev("bkg","Background",Jpsi_M,RooArgList(a0,a1,a2))#/*,a3*/))
        # bkg = RooExponential("bkg","Background",Jpsi_M,a5)

    pppi0 = RooGenericPdf("pppi0","Jpsi.pppi0","@0<(2961.92) ? TMath::Sqrt(3096.900-134.977-@0) : 0",RooArgList(Jpsi_M))
    nPPPi0 = RooFormulaVar("nPPPi0","nPPPi0","@0*@1*(1.19/2.12)",RooArgList(nJpsi,eff_pppi0))


    modelBkg = RooAddPdf("modelBkg","background", RooArgList(bkg,pppi0), RooArgList(nBckgr,nPPPi0))
    modelSignal = RooAddPdf("modelSignal","signal", RooArgList(bwxg_1, bwxg_2,gauss_1,gauss_2), RooArgList(nEta_1,nEta_2,nJpsi_1,nJpsi_2))
    model = RooAddPdf("model","signal+bkg", RooArgList(bwxg_1, bwxg_2,gauss_1,gauss_2,bkg,pppi0), RooArgList(nEta_1,nEta_2,nJpsi_1,nJpsi_2,nBckgr,nPPPi0))


    getattr(w,'import')(model,RooFit.RecycleConflictNodes())
    getattr(w,'import')(modelBkg,RooFit.RecycleConflictNodes())
    getattr(w,'import')(modelSignal,RooFit.RecycleConflictNodes())

    return model



def fitData(nConf=0, nPTs=[0],nTz=0, isPrompt=True):

    gROOT.Reset()   
    proof = TProof.Open("")

    w = RooWorkspace("w",True)

    hists = []
    nPTbins = len(nPTs)

    model = fillRelWorkspace(w,nConf,1)

    sample = RooCategory("sample","sample") 
    for nPT in nPTs:
        sample.defineType("PT"+str(nPT)) 


    Jpsi_M = w.var("Jpsi_M")
    Jpsi_M.setBins(nBins)

    for nPT in nPTs:
        model = fillRelWorkspace(w,nConf,nPT)
        getData(w,nPT,nTz,isPrompt)

        histo   = w.data("dh")
        hists.append(histo)

    
    model_pts = []
    for nPT in nPTs:
        sample.defineType("PT"+str(nPT)) 
        model_pts.append(w.pdf("model"))




    combData = RooDataHist("combData","combData",RooArgList(Jpsi_M,sample),RooFit.Index(sample),RooFit.Import("PT1",hists[0]),RooFit.Import("PT2",hists[1]),RooFit.Import("PT3",hists[2]),RooFit.Import("PT4",hists[3]),RooFit.Import("PT5",hists[4]))



    simPdf = RooSimultaneous("smodel","",sample)
    for nPT in nPTs:
        idx = nPTs.index(nPT)
        simPdf.addPdf(model_pts[idx],"PT"+str(nPT))

    simPdf.fitTo(combData,RooFit.Strategy(2))
    simPdf.fitTo(combData)
    simPdf.fitTo(combData,RooFit.Minos(True))



    plot_binWidth = 10.
    plot_binN = int((maxM-minM)/plot_binWidth)

    binning = RooFit.Binning(plot_binN,minM,maxM)
    mrkSize = RooFit.MarkerStyle(7)
    lineWidth = RooFit.LineWidth(2)
    name = RooFit.Name

    frames = []
    pulls = []
    Jpsi_M.setBins(nBins)
    for nPT in nPTs:
        frame = Jpsi_M.frame(RooFit.Title("PT bin "+str(nPT)))
        combData.plotOn(frame,RooFit.Cut("sample==sample::PT"+str(nPT)),binning,mrkSize,name("dataPT"+str(nPT)))
        simPdf.plotOn(frame,RooFit.Slice(sample,"PT"+str(nPT)),RooFit.ProjWData(RooArgSet(sample),combData,True),lineWidth, name("pdfPT"+str(nPT)))
        hpull = frame.pullHist("dataPT"+str(nPT),"pdfPT"+str(nPT),True)
        frame_pull = Jpsi_M.frame()
        for ii in range(hpull.GetN()):
            hpull.SetPointEYlow(ii,0)
            hpull.SetPointEYhigh(ii,0)
        frame_pull.addPlotable(hpull,"B")

        frames.append(frame)
        pulls.append(frame_pull)



    c = TCanvas("c","c",1200,600)
    c.Divide(nPTbins,2)
    for i in range(nPTbins):
        pad = c.cd(i+1)
        xl = pad.GetXlowNDC(); xh = xl + pad.GetWNDC()
        yl = pad.GetYlowNDC(); yh = yl + pad.GetHNDC()
        pad.SetPad(xl,yl-0.1,xh,yh)
        frames[i].Draw()

        pad = c.cd(nPTbins+i+1)
        xl = pad.GetXlowNDC(); xh = xl + pad.GetWNDC()
        yl = pad.GetYlowNDC(); yh = yl + pad.GetHNDC()
        pad.SetPad(xl,yl,xh,yh-0.1)
        pulls[i].Draw()
    c.SaveAs("simFit.pdf")



gROOT.ProcessLine(".L ../RooRelBreitWigner.cxx+")
pr = False
fitData(0,[1,2,3,4,5],0,pr)
