from ROOT import *
from ROOT.RooFit import *
from ROOT import kRed, kBlack, kDashed, kMagenta, kCyan, kViolet, kTRUE
from array import *

gStyle.SetOptStat(000)


import sys
sys.path.insert(0, '../')
from makeConfigDictionary import *


sidebandCut = "((Jpsi_m_scaled>3140 && Jpsi_m_scaled<3185) ||\
                (Jpsi_m_scaled>2875 && Jpsi_m_scaled<2920))"

dataChain = TChain("DecayTree")
mcChain = TChain("DecayTree")

dataMCfactor = 43885/(3013.+75.)


for file in MCTuples["jpsi"]["SndTopo"]:
    mcChain.Add(file)

dataChain.Add(tuplesSnd2015)
dataChain.Add(tuplesSnd2016)



def getSignif(cut):
    cutFull = cut + " && " + sidebandCut
    nBkg = dataChain.GetEntries(cutFull)
    nSig = mcChain.GetEntries(cut)
    return nSig/TMath.Sqrt(nBkg+nSig*dataMCfactor)



CutsOptDict = {"Jpsi_PT"          :[3000,4000,4500,5000,5500,6000,6500,7000,7500,8000],
               "proton_PT"        :[1000,1250,1500,1750,2000,2500],
               "proton_IPCHI2"    :[9,16,25,36],
               "Jpsi_FDCHI2_OWNPV":[25,30,36,45,49,64,81,100,121,144]
              }


def findOptimalPoint():
    nJpsiPTbins = len(CutsOptDict["Jpsi_PT"])-1
    nProtonPTbins = len(CutsOptDict["proton_PT"])-1
    nJpsiFDbins = len(CutsOptDict["Jpsi_FDCHI2_OWNPV"])-1
    nProtonIPbins = len(CutsOptDict["proton_IPCHI2"])-1

    JpsiPTbinning   = array("d", CutsOptDict["Jpsi_PT"])
    ProtonPTbinning = array("d", CutsOptDict["proton_PT"])
    JpsiFDbinning   = array("d", CutsOptDict["Jpsi_FDCHI2_OWNPV"])
    ProtonIPbinning = array("d", CutsOptDict["proton_IPCHI2"])

    maxSignif = -99999999

    for iJpsiPTcut in range(nJpsiPTbins):
      for iProtonPTcut in range(nProtonPTbins):
        for iJpsiFDcut in range(nJpsiFDbins):
          for iProtonIPcut in range(nProtonIPbins):
            JpsiPTcut = CutsOptDict["Jpsi_PT"][iJpsiPTcut]
            ProtonPTcut = CutsOptDict["proton_PT"][iProtonPTcut]
            JpsiFDcut = CutsOptDict["Jpsi_FDCHI2_OWNPV"][iJpsiFDcut]
            ProtonIPcut = CutsOptDict["proton_IPCHI2"][iProtonIPcut]
            cut = "(Jpsi_PT>"+str(JpsiPTcut)+")      && \
                   (ProtonP_PT>"+str(ProtonPTcut)+") && \
                   (ProtonM_PT>"+str(ProtonPTcut)+") && \
                   (Jpsi_FDCHI2_OWNPV>"+str(JpsiFDcut)+") && \
                   (ProtonP_IPCHI2_OWNPV>"+str(ProtonIPcut)+") && \
                   (ProtonM_IPCHI2_OWNPV>"+str(ProtonIPcut)+")"
            signif = getSignif(cut)
            if(signif>maxSignif):
              maxSignif = signif
              bestIP = iProtonIPcut
              bestFD = iJpsiFDcut
              bestJpsiPT = iJpsiPTcut
              bestProtonPT = iProtonIPcut

      print "bestJpsiPT (nbin)="  , bestJpsiPT+1,   ",\n \
             bestProtonPT (nbin)=", bestProtonPT+1, ",\n \
             bestIP (nbin)="      , bestIP+1,       ",\n \
             bestFD (nbin)="      , bestFD+1
      
      return bestJpsiPT, bestProtonPT, bestIP, bestFD



def makePToptimization():
    nJpsiPTbins = len(CutsOptDict["Jpsi_PT"])-1
    nProtonPTbins = len(CutsOptDict["proton_PT"])-1

    JpsiPTbinning   = array("d", CutsOptDict["Jpsi_PT"])
    ProtonPTbinning = array("d", CutsOptDict["proton_PT"])


    bestJpsiPT, bestProtonPT, bestIP, bestFD = findOptimalPoint()

    optHist = TH2F("optHist","optHist",nJpsiPTbins,JpsiPTbinning,nProtonPTbins,ProtonPTbinning)
    optHist.GetXaxis().SetTitle("Jpsi_PT")
    optHist.GetYaxis().SetTitle("proton_PT")
    for iJpsiPTcut in range(nJpsiPTbins):
        for iProtonPTcut in range(nProtonPTbins):
            JpsiPTcut = CutsOptDict["Jpsi_PT"][iJpsiPTcut]
            ProtonPTcut = CutsOptDict["proton_PT"][iProtonPTcut]

            JpsiFDcut = CutsOptDict["Jpsi_FDCHI2_OWNPV"][bestFD]
            ProtonIPcut = CutsOptDict["proton_IPCHI2"][bestIP]

            cut = "(Jpsi_FDCHI2_OWNPV>"+str(JpsiFDcut)+") && \
                   (ProtonP_IPCHI2_OWNPV>"+str(ProtonIPcut)+") && \
                   (ProtonM_IPCHI2_OWNPV>"+str(ProtonIPcut)+") && \
                   (Jpsi_PT>"+str(JpsiPTcut)+") && \
                   (ProtonP_PT>"+str(ProtonPTcut)+") && \
                   (ProtonM_PT>"+str(ProtonPTcut)+")"

            signif = getSignif(cut)
            optHist.SetBinContent(iJpsiPTcut+1,iProtonPTcut+1,signif)


    c = TCanvas()
    optHist.Draw("COLZ")

    c.SaveAs("OptimizePT.pdf")



def makeIPoptimization():
    nJpsiFDbins = len(CutsOptDict["Jpsi_FDCHI2_OWNPV"])-1
    nProtonIPbins = len(CutsOptDict["proton_IPCHI2"])-1

    JpsiFDbinning   = array("d", CutsOptDict["Jpsi_FDCHI2_OWNPV"])
    ProtonIPbinning = array("d", CutsOptDict["proton_IPCHI2"])
    
    bestJpsiPT, bestProtonPT, bestIP, bestFD = findOptimalPoint()

    optHist = TH2F("optHist","optHist",nJpsiFDbins,JpsiFDbinning,nProtonIPbins,ProtonIPbinning)
    optHist.GetXaxis().SetTitle("Jpsi_FDCHI2_OWNPV")
    optHist.GetYaxis().SetTitle("proton_IPCHI2")
    for iJpsiFDcut in range(nJpsiFDbins):
        for iProtonIPcut in range(nProtonIPbins):
            JpsiPTcut = CutsOptDict["Jpsi_PT"][bestJpsiPT]
            ProtonPTcut = CutsOptDict["proton_PT"][bestProtonPT]

            JpsiFDcut = CutsOptDict["Jpsi_FDCHI2_OWNPV"][iJpsiFDcut]
            ProtonIPcut = CutsOptDict["proton_IPCHI2"][iProtonIPcut]
            cut = "(Jpsi_FDCHI2_OWNPV>"+str(JpsiFDcut)+") && \
                   (ProtonP_IPCHI2_OWNPV>"+str(ProtonIPcut)+") && \
                   (ProtonM_IPCHI2_OWNPV>"+str(ProtonIPcut)+") && \
                   (Jpsi_PT>"+str(JpsiPTcut)+") && \
                   (ProtonP_PT>"+str(ProtonPTcut)+") && \
                   (ProtonM_PT>"+str(ProtonPTcut)+")"

            signif = getSignif(cut)
            optHist.SetBinContent(iJpsiFDcut+1,iProtonIPcut+1,signif)


    c = TCanvas()
    optHist.Draw("COLZ")

    c.SaveAs("OptimizeIP.pdf")



makePToptimization()
makeIPoptimization()


