
  RooFitResult: minimized FCN value: -4069.03, estimated distance to minimum: 2.12746e-05
                covariance matrix quality: Full, accurate covariance matrix
                Status : MIGRAD=0 HESSE=0 

    Floating Parameter    FinalValue +/-  Error   
  --------------------  --------------------------
             mean_Etac    9.0634e-01 +/-  2.41e-01
             mean_Jpsi    8.8290e-01 +/-  2.52e-01
                 nEtac    1.1690e+03 +/-  3.42e+01
                 nJpsi    1.1710e+03 +/-  3.42e+01
        rEtaToJpsi_pt3    9.6218e-01 +/-  3.36e-02
           rG1toG2_pt3    9.4498e-01 +/-  1.09e-02
           rNarToW_pt3    2.5940e-01 +/-  2.34e-02
           sigma_eta_1    7.6793e+00 +/-  2.10e-01

