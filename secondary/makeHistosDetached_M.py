from ROOT import *
from ROOT.RooFit import *
import sys, os

sys.path.insert(0, '../')
from makeConfigDictionary import *

binningDict["Jpsi_PT"]=[5500,6500,8000,10000,12000,14000,18000,30000]


bold = "\033[1m"
reset = "\033[0;0m"
def makeHistos(nt, year="2016", inCutKey="secondary", var="Jpsi_PT", doBinning=False):
    inCut = cutsDict[inCutKey]

    nMassBins = int((maxMass-minMass)/binWidthSnd)
    Jpsi_M = TH1F("Jpsi_M","Jpsi_M" , nMassBins, minMass, maxMass)
    Jpsi_M.SetLineColor(4)
    Jpsi_M.GetXaxis().SetTitle("J/#psi_Mass / MeV ")
    Jpsi_M.GetYaxis().SetTitle("Entries")
    Jpsi_M.SetMinimum(0.0)

    cutMass = "(Jpsi_M>" + str(minMass) + ") && (Jpsi_M<" + str(maxMass) + ")"


    if year=="2015": cutApriori = cutsAprioriDetached
    if year=="2016": cutApriori = cutsAprioriDetached

    if doBinning:
        binningFile = "binning.txt"
# !!!!!!!!
        varBinning = binningDict[var]
        nVarBins =  len(varBinning)-1
        for i in range(nVarBins):
            print bold+"    Now I\'m doing", var, "binning. Bin No", (i+1), ", Tz integrated", reset
            varLowEdge = varBinning[i]
            varHighEdge = varBinning[i+1]
            cutVar = "("+var+">"+str(varLowEdge)+") && ("+var+"<"+str(varHighEdge)+")"

            sumCut = cutMass    +" && "+ \
                     cutApriori +" && "+ \
                     inCut      +" && "+ \
                     cutVar

            dirName = histosDir+year+"/SndTopo_M/"+inCutKey+"/"+var
            if not os.path.exists(dirName):
                os.makedirs(dirName)
            fileName = dirName + "/bin"+str(i+1)+"_Tz0.root"
            nt.Draw("Jpsi_M>>Jpsi_M", sumCut)
            Jpsi_M.SaveAs(fileName)
    else:
        print bold+"I\'m doing Total histos"+reset
        sumCut = cutMass    +" && "+ \
                 cutApriori +" && "+ \
                 inCut

        cut = TCut(sumCut)

        dirName = histosDir + year + "/SndTopo_M/" + inCutKey + "/Total"
        if not os.path.exists(dirName):
            os.makedirs(dirName)
        fileName = dirName + "/Tz0.root"
        nt.Draw("Jpsi_M>>Jpsi_M", cut)
        Jpsi_M.SaveAs(fileName)
    del Jpsi_M



def makeAllHistos():
    years = ["2015","2016"]
    variables = ["Jpsi_PT"]
    inCutKeys = ["secondaryTopoOpt"]

    for year in years:
        nt = TChain("Jpsi2ppTuple/DecayTree")
        if (year=="2016"): 
            nt.Add(tuplesSnd2016[0]);nt.Add(tuplesSnd2016[1])
        if (year=="2015"): 
            nt.Add(tuplesSnd2015[0]);nt.Add(tuplesSnd2015[1])
        for var in variables:
            for inCutKey in inCutKeys:
               print "Setup Cuts: ", bold+inCutKey, reset
               print "      year: ", bold+year, reset,"\n"

               makeHistos(nt,year,inCutKey,var,doBinning=False)
               makeHistos(nt,year,inCutKey,var,doBinning=True)
        del nt

makeAllHistos()

