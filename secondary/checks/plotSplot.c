#include <stdio.h>
#include <stdlib.h>

using namespace RooFit ;
using namespace RooStats ;


void plotSplot()
{
    TFile * f;
    f = new TFile("splot/splotDiProton.root","read");
    
    RooWorkspace* w2 = (RooWorkspace*)f->Get("w2");

    RooRealVar *Jpsi_M = w2->var("Jpsi_M") ;
    
    RooRealVar *Jpsi_Hlt1DiProtonDecision_TOS = w2->var("Jpsi_Hlt1DiProtonDecision_TOS");
    RooRealVar *Jpsi_L0Global_TIS = w2->var("Jpsi_L0Global_TIS") ;
    RooRealVar *Jpsi_L0HadronDecision_TOS = w2->var("Jpsi_L0HadronDecision_TOS") ;

    
    RooRealVar *Jpsi_P = w2->var("Jpsi_P") ;
    RooRealVar *Jpsi_PT = w2->var("Jpsi_PT") ;

    RooRealVar *ProtonP_P = w2->var("ProtonP_P");
    RooRealVar *ProtonM_P = w2->var("ProtonM_P");
    
    RooRealVar *ProtonP_PT = w2->var("ProtonP_PT");
    RooRealVar *ProtonM_PT = w2->var("ProtonM_PT");
    
    RooRealVar *ProtonP_PIDp = w2->var("ProtonP_PIDp");
    RooRealVar *ProtonM_PIDp = w2->var("ProtonM_PIDp");
    
    RooRealVar *ProtonP_PIDe = w2->var("ProtonP_PIDe");
    RooRealVar *ProtonM_PIDe = w2->var("ProtonM_PIDe");

    
    RooRealVar *nSPDHits = w2->var("nSPDHits");
    RooRealVar *totCandidates = w2->var("totCandidates");
    
    RooDataSet* dsetEtac = (RooDataSet*)w2->data("dsetEtac");
    RooDataSet* dsetJpsi = (RooDataSet*)w2->data("dsetJpsi");
    RooDataSet* dsetBkg = (RooDataSet*)w2->data("dsetBkg");
    
    
    RooRealVar *Var = Jpsi_PT;
    
    TCanvas* c = new TCanvas("splot","splot",1100,500);
    c->Divide(3,1);
    RooPlot* frameEtac = Var->frame(Title("Etac"));
    RooPlot* frameJpsi = Var->frame(Title("Jpsi"));
    RooPlot* frameBkg  = Var->frame(Title("Bkg"));
    
    
    int nBins = 10;
    Double_t low = 6500;
    Double_t high = 18000.;
    
    dsetEtac->plotOn(frameEtac, Binning(nBins, low, high),DataError(RooAbsData::SumW2));
    dsetJpsi->plotOn(frameJpsi, Binning(nBins, low, high),DataError(RooAbsData::SumW2));
    dsetBkg->plotOn(frameBkg, Binning(nBins, low, high),DataError(RooAbsData::SumW2));
    
    frameEtac->GetXaxis()->SetLimits(low,high);
    frameJpsi->GetXaxis()->SetLimits(low,high);
    frameBkg->GetXaxis()->SetLimits(low,high);
    
    c->cd(1);
    frameEtac->Draw();
    c->cd(2);
    frameJpsi->Draw();
    c->cd(3);
    frameBkg->Draw();

}


