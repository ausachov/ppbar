

using namespace RooFit ;


void Interference()
{
//    Double_t lowM = 1700;
//    Double_t highM = 4700;
    Double_t lowM = 2850;
    Double_t highM = 3250;
    Int_t nPlotBins = 50;
    
    RooRealVar Jpsi_M("Jpsi_M","Jpsi_M",lowM,highM) ;
    Jpsi_M.setBins(1000,"cache");
    
    RooRealVar p1("p1","p1", 0.5) ;//?
    RooRealVar frac_VA("frac_VA","frac_VA",0.);//?
    
    
//    RooRealVar phase("phase","phase",-0.6287);
    RooRealVar phase("phase","phase",-0.5);
//    RooRealVar scale("scale","scale", 0.317);
    RooRealVar scale("scale","scale", 0.05);
    
    RooRealVar mass_etac("mass_etac","mass_etac",2983.6);
    RooRealVar gamma_etac("gamma_etac","gamma_etac",32.);
    RooRealVar eslope("eslope","eslope",0.00);
    
    RooInterModel pdf("inter","inter", Jpsi_M, p1, frac_VA, mass_etac, gamma_etac, phase, scale);
    RooInterModel pdf2("inter2","inter2", Jpsi_M, p1, frac_VA, mass_etac, gamma_etac, RooConst(3*3.14/2.), scale);
    
    
    RooRealVar spin_eta("spin_eta","spin_eta", 0.);
    RooRealVar radius_eta("radius_eta","radius", 0.5);
    RooRealVar proton_m("proton_m","proton mass", 938.3 );
    RooRelBreitWigner br_wigner("br_wigner", "br_wigner",Jpsi_M, mass_etac, gamma_etac, spin_eta,radius_eta,proton_m,proton_m);
    
    
//    RooInterModel pdf("inter","inter", Jpsi_M, p1, frac_VA, mass_etac, gamma_etac, phase, scale);
//    RooInterModel pdf2("inter2","inter2", Jpsi_M, p1, frac_VA, mass_etac, gamma_etac, RooConst(3*3.14/2.), scale);
    RooExponential exp("exp","exp",Jpsi_M,eslope);
    
    
//    RooInterModel pdf2("inter2","inter2", Jpsi_M, p1, frac_VA, mass_etac, RooConst(21.), RooConst(3*3.14/2.), scale);
    
    Double_t bgRatio = 0.0001;
    Double_t intRatio = 10.;
    
    RooRealVar intFraction("intW","intW",intRatio/(1.+bgRatio+intRatio));
    RooRealVar bgFraction("bgW","bgW",bgRatio/(1.+bgRatio+intRatio));

    
    RooAddPdf pdfSum("sum","sum", RooArgList(exp,pdf,pdf2),RooArgList(bgFraction,intFraction));
    RooAddPdf clean("clean","clean",RooArgList(exp,pdf,pdf2),RooArgList(bgFraction,RooConst(0.)));

    RooPlot *frame = Jpsi_M.frame(Title("ppbar mass")) ;
    clean.plotOn(frame);
    pdfSum.plotOn(frame,LineColor(kRed));
    
    TCanvas* c = new TCanvas("Masses_Fit","Masses Fit",700,700);
    frame->Draw();
    
    
    
}