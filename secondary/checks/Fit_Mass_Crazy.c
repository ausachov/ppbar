#include <stdio.h>
#include <stdlib.h>

using namespace RooFit ;


Double_t lowM = 2850;
Double_t highM = 3250;
Int_t nPlotBins = 100;

void getData_h(RooWorkspace* w)
{
    TFile * f;
    f = new TFile("currentCut.root","read");
//    f = new TFile("FD4_IP16.root","read");
//    f = new TFile("FD100_IP4.root","read");
//    f = new TFile("FD49_IP4.root","read");
//    f = new TFile("FD25_IP9.root","read");
//    f = new TFile("PT6000.root","read");
//    f = new TFile("ID_as_prompt.root","read");
//    f = new TFile("P_Pt2000.root","read");
//    f = new TFile("P_P10000.root","read");
//    f = new TFile("SPD300.root","read");
//    f = new TFile("SPD200.root","read");
//    
//
    TH1D *hh = new TH1D();
//    RooRealVar Jpsi_M("Jpsi_M","Jpsi_M",2850,3250) ;
    RooRealVar Jpsi_M("Jpsi_M","Jpsi_M",lowM,highM) ;


    hh = (TH1D*)f->Get("h");

    RooDataHist dh("dh","dh",RooArgSet(Jpsi_M),hh);
    w->import(dh);
    delete hh;
    f->Close();
    delete f;
}


void fillRelWorkspace(RooWorkspace *w)
{
    
    RooRealVar Jpsi_M("Jpsi_M","Jpsi_M",lowM,highM) ;
    Jpsi_M.setBins(1000,"cache");
    Jpsi_M.setRange("SBLeft", lowM, 2900);
    Jpsi_M.setRange("SBCentral", 3035, 3060);
    Jpsi_M.setRange("SBRight", 3150, highM);
    Jpsi_M.setRange("SignalEtac", 2900, 3035);
    Jpsi_M.setRange("SignalJpsi", 3060, 3150);
    Jpsi_M.setRange("Total", lowM, highM);
    

    Double_t gammaEtac = 31.8;
    Double_t mDiffEtac = 113.301;
    Int_t bkgType = 0;
    std::cout<<"SETUP 0"<<std::endl;

    
    Double_t ratioNtoW = 0.25;
    Double_t ratioEtaToJpsi = 0.97;
    Double_t ratioArea = 0.95;
    

    

    RooRealVar rEtaToJpsi("rEtaToJpsi","rEtaToJpsi", ratioEtaToJpsi);
    RooRealVar rNarToW("rNarToW","rNarToW",ratioNtoW);
    RooRealVar rG1toG2("rG1toG2","rG1toG2",ratioArea);
    RooRealVar nEta("nEta","num of Etac", 10, 1.e7);
    RooRealVar nJpsi("nJpsi","num of J/Psi", 10, 1.e7);
    RooRealVar nEtacRel("nEtacRel","num of Etac", 0.0, 3.0);
    RooRealVar nCrazy("nCrazy","nCrazy", 0.0, 1e7);
    //    RooFormulaVar nEtacRel("nEtacRel","nEtacRel", "@0/@1",RooArgList(nEta,nJpsi));
    

    RooRealVar nBckgr("nBckgr","num of backgr",1e7,10,1.e+9);
    
    
    RooRealVar mass_Jpsi("mass_Jpsi","mean of gaussian",3096.9, 3030, 3150) ;
    RooRealVar mass_res("mass_res","mean of gaussian",mDiffEtac, 100, 130) ;

    RooFormulaVar mass_eta("mass_eta","mean of gaussian","@0-@1",RooArgSet(mass_Jpsi,mass_res)) ;
    RooRealVar gamma_eta("gamma_eta","width of Br-W", gammaEtac, 5., 80.);
    RooRealVar spin_eta("spin_eta","spin_eta", 0. );
    RooRealVar radius_eta("radius_eta","radius", 1.);
    RooRealVar proton_m("proton_m","proton mass", 938.3 );
    
    
    RooRealVar sigma_eta_1("sigma_eta_1","width of gaussian", 9., 5., 50.) ;
    RooFormulaVar sigma_eta_2("sigma_eta_2","width of gaussian","@0/@1",RooArgSet(sigma_eta_1,rNarToW));
    
    RooFormulaVar sigma_Jpsi_1("sigma_Jpsi_1","width of gaussian","@0/@1",RooArgList(sigma_eta_1,rEtaToJpsi));
    RooFormulaVar sigma_Jpsi_2("sigma_Jpsi_2","width of gaussian","@0/@1",RooArgList(sigma_Jpsi_1,rNarToW));

    RooFormulaVar sigma_Crazy_1("sigma_Jpsi_1","width of gaussian","@0/@1",RooArgList(sigma_eta_1,RooConst(0.25)));
    RooFormulaVar sigma_Crazy_2("sigma_Jpsi_2","width of gaussian","@0/@1",RooArgList(sigma_Jpsi_1,rNarToW));
    
    // Fit eta
    RooGaussian gaussEta_1("gaussEta_1","gaussEta_1 PDF", Jpsi_M, RooConst(0),  sigma_eta_1);
    RooGaussian gaussEta_2("gaussEta_2","gaussEta_2 PDF", Jpsi_M, RooConst(0),  sigma_eta_2);
    
    RooRelBreitWigner br_wigner("br_wigner", "br_wigner",Jpsi_M, mass_eta, gamma_eta, spin_eta,radius_eta,proton_m,proton_m);
    
    
    RooFFTConvPdf bwxg_1("bwxg_1","breit-wigner (X) gauss", Jpsi_M, br_wigner, gaussEta_1) ;
    RooFFTConvPdf bwxg_2("bwxg_2","breit-wigner (X) gauss", Jpsi_M, br_wigner, gaussEta_2) ;
    
    // Fit J/psi
    RooGaussian gauss_1("gauss_1","gaussian PDF",Jpsi_M,mass_Jpsi,sigma_Jpsi_1) ;
    RooGaussian gauss_2("gauss_2","gaussian PDF",Jpsi_M,mass_Jpsi,sigma_Jpsi_2) ;
    
    
    RooGaussian Crazy_1("Crazy_1","Crazy_1 PDF",Jpsi_M,mass_eta,sigma_Crazy_1) ;
    RooGaussian Crazy_2("Crazy_2","Crazy_2 PDF",Jpsi_M,mass_eta,sigma_Crazy_2) ;
    RooAddPdf modelCrazy("modelCrazy","modelCrazy",RooArgList(Crazy_1,Crazy_2),RooArgList(rG1toG2));
    
    
    RooAddPdf modelEtac("modelEtac","modelEtac",RooArgList(bwxg_1,bwxg_2),RooArgList(rG1toG2));
    RooAddPdf modelJpsi("modelJpsi","modelJpsi",RooArgList(gauss_1,gauss_2),RooArgList(rG1toG2));
    
    RooFormulaVar nEtac("nEtac","nEtac","@0*@1",RooArgSet(nEtacRel,nJpsi));
    RooAddPdf modelSignal("modelSignal","signal", RooArgList(modelEtac,modelJpsi,modelCrazy), RooArgList(nEtac,nJpsi,nCrazy));


    RooAbsPdf *bkg;
    RooRealVar a0("a0","a0",0,-1.,1.) ;
    //RooRealVar a0("a0","a0",0.2,0,20) ;
    RooRealVar a1("a1","a1",-3e-2,-1,1) ;
    RooRealVar a2("a2","a2",0.) ;
//    RooRealVar a2("a2","a2",0.,-0.5,0.5) ;
    RooRealVar a3("a3","a3",0.,-1.,1.) ;
    RooRealVar a4("a4","a4",0.) ;
    switch (bkgType)
    {
        case 1:
            bkg = new RooChebychev("bkg","Background",Jpsi_M,RooArgSet(a0,a1,a2,a3,a4)) ;
            std::cout<<"Cheb2: OK"<<std::endl;
            break;
        case 3:
            //a0.setRange(0,1e-2); a0.setVal(5e-3);
//            a1.setRange(-1.,1.);
//            a2.setRange(-1.,1.);
            bkg = new RooGenericPdf("bkg","background","TMath::Exp(-(@0-3050.)/200.*@1)*(1.+@2*(@0-3050.)/200.+@3*(@0-3050.)*(@0-3050.)/200./200.)",RooArgList(Jpsi_M,a0,a1,a2)) ;
            std::cout<<"Expo: OK"<<std::endl;
            break;
        default:
            bkg = new RooChebychev ("bkg","Background",Jpsi_M,RooArgSet(a0,a1,a2,a3)) ;
            std::cout<<"Cheb3: OK"<<std::endl;
            break;
    }
    
    RooGenericPdf pppi0("pppi0","Jpsi->pppi0","(@0<2961.92) ? TMath::Sqrt(3096.900-134.977-@0) : 0",RooArgList(Jpsi_M));
    RooFormulaVar nPPPi0("nPPPi0","nPPPi0","@0*0.06*(1.19/2.12)",RooArgSet(nJpsi));
    
    
    RooAddPdf modelBkg("modelBkg","background", RooArgList(*bkg,pppi0), RooArgList(nBckgr,nPPPi0));
    RooAddPdf model("model","signal+bkg", RooArgList(modelEtac,modelJpsi,modelCrazy,*bkg,pppi0), RooArgList(nEtac,nJpsi,nCrazy,nBckgr,nPPPi0));
    
    
    w->import(model);
    w->import(modelBkg,RecycleConflictNodes());
    w->import(modelSignal,RecycleConflictNodes());
}



void Fit_Mass(Double_t* gamma = 0, Double_t* gammaerror=0)
{
    gROOT->Reset();
    
    
    TProof *proof = TProof::Open("");
    RooWorkspace *w = new RooWorkspace("w",kTRUE);
    
    fillRelWorkspace(w);
    getData_h(w);

    
    RooAbsPdf *model = w->pdf("model");
    RooAbsPdf *modelBkg = w->pdf("modelBkg");
    RooAbsPdf *modelSignal = w->pdf("modelSignal");
    RooRealVar *Jpsi_M = w->var("Jpsi_M");
    
    Jpsi_M->setBins(10000);
    
    RooDataHist *hist = (RooDataHist*) w->data("dh");
    
    RooRealVar *sigma = w->var("sigma_eta_1");
    RooRealVar *mass_Jpsi = w->var("mass_Jpsi");
    RooRealVar *mass_res = w->var("mass_res");
    RooRealVar *gamma_eta = w->var("gamma_eta");
    
    //    w->var("nEtac")->setVal(0.);
    //    w->var("nEtac")->setConstant(kTRUE);
    w->var("nEtacRel")->setVal(0.);
    w->var("nEtacRel")->setConstant(kTRUE);
    w->var("nJpsi")->setVal(0.);
    w->var("nJpsi")->setConstant(kTRUE);
    
    mass_res->setConstant(kTRUE);
    gamma_eta->setConstant(kTRUE);
    mass_Jpsi->setConstant(kTRUE);
    sigma->setConstant(kTRUE);
    
    modelBkg->fitTo(*hist,Range("SBLeft,SBRight"),Extended(kTRUE),NumCPU(48));
    
    //    w->var("nEtac")->setConstant(kFALSE);
    w->var("nEtacRel")->setConstant(kFALSE);
    w->var("nJpsi")->setConstant(kFALSE);
//    mass_res->setConstant(kFALSE);
//    sigma->setConstant(kFALSE);
//    gamma_eta->setConstant(kFALSE);
//    mass_Jpsi->setConstant(kFALSE);
    
    
    model->fitTo(*hist,Extended(kTRUE),Offset(kTRUE),NumCPU(48));
    mass_res->setConstant(kFALSE);
    sigma->setConstant(kFALSE);
    //gamma_eta->setConstant(kFALSE);
    mass_Jpsi->setConstant(kFALSE);
    
    model->fitTo(*hist,Extended(kTRUE),Minos(kTRUE),Save(),/*Constrain(*Jpsi_M),*/NumCPU(48));
    RooFitResult *r = model->fitTo(*hist,Extended(kTRUE),Minos(kTRUE),Save(),/*Constrain(*Jpsi_M),*/NumCPU(48),PrintLevel(2));

    RooPlot *frame = Jpsi_M->frame(Title("J/#psi and #eta_{c} p.d.f.")) ;
    hist->plotOn(frame,Binning(nPlotBins,lowM,highM));
    

    
    RooAbsPdf *bwxg_1 = w->pdf("bwxg_1");
    RooAbsPdf *bwxg_2 = w->pdf("bwxg_2");
    RooAbsPdf *gauss_1 = w->pdf("gauss_1");
    RooAbsPdf *gauss_2 = w->pdf("gauss_2");
    RooAbsPdf *bkg = w->pdf("bkg");
    RooAbsPdf *pppi0 = w->pdf("pppi0");
    model->paramOn(frame,Layout(0.70, 0.99, 0.99));
    frame->getAttText()->SetTextSize(0.025) ;
    
    
    model->plotOn(frame,Components(RooArgSet(*gauss_1,gauss_2)),LineColor(kRed),LineStyle(kDashed),Normalization(1.0,RooAbsReal::RelativeExpected));
    model->plotOn(frame,Components(RooArgSet(*bwxg_1,bwxg_2)),LineColor(kCyan),LineStyle(kDashed),Normalization(1.0,RooAbsReal::RelativeExpected));
    model->plotOn(frame,Components(RooArgSet(*pppi0)),LineColor(kMagenta),LineStyle(kDashed),Normalization(1.0,RooAbsReal::RelativeExpected));
    model->plotOn(frame,Components(RooArgSet(*bkg,*pppi0)),LineStyle(kDashed),Normalization(1.0,RooAbsReal::RelativeExpected));
    model->plotOn(frame,Normalization(1.0,RooAbsReal::RelativeExpected));
    
    
    Double_t chi2=0;
//    RooHist *hResid = frame->residHist();
//    RooPlot *frame_res = Jpsi_M->frame(Title("Residual Distribution"));
//    frame_res->addPlotable(hResid,"P") ;
    
    
    RooHist *hpull = frame->pullHist(0,0,1);
    RooPlot *frame_pull = Jpsi_M->frame(Title("Pull Distribution"));
    for(int ii=0; ii<hpull->GetN(); ii++)
    {
        hpull->SetPointEYlow(ii,0);
        hpull->SetPointEYhigh(ii,0);
    }
    frame_pull->addPlotable(hpull,"B") ;
    
    
    
    
//    model->plotOn(frame_res, Components(RooArgSet(*bwxg_1,*gauss_1,*bwxg_2,*gauss_2)),Normalization(1.0,RooAbsReal::RelativeExpected));
//    chi2 = frame->chiSquare();
//    model->plotOn(frame_res,Components(*bkg),LineStyle(kDashed),Normalization(0.0,RooAbsReal::RelativeExpected));
//    model->plotOn(frame_res,Components(*pppi0),LineColor(kViolet),LineStyle(kDashed),Normalization(1.0,RooAbsReal::RelativeExpected));
    TCanvas* c = new TCanvas("Masses_Fit","Masses Fit",700,700);
    c->Divide(1,2);
    c->cd(1)->SetPad(.005, .305, .995, .995);
    gPad->SetLeftMargin(0.15) ; frame->GetXaxis()->SetTitle("J/#psi mass / [MeV/c^{2}]") ; frame->Draw();
//    c->cd(2)->SetPad(.005, .205, .995, .395);
//    gPad->SetLeftMargin(0.15) ; frame_res->GetXaxis()->SetTitle("") ; frame_res->Draw();
    c->cd(2)->SetPad(.005, .005, .995, .295);
    gPad->SetLeftMargin(0.15) ; frame_pull->GetXaxis()->SetTitle("") ; frame_pull->Draw();
    
    if(gamma)*gamma =gamma_eta->getVal();
    if(gammaerror)*gammaerror =gamma_eta->getError();

//    if(gamma)*gamma =mass_res->getVal();
//    if(gammaerror)*gammaerror =mass_res->getError();
    
}

void runSPD()
{
    TChain* ch = new TChain("DecayTree");
    ch->Add("Data2015/Snd/*.root");
    ch->Add("Data2016/Snd/*.root");
    
    
    TH1D* h = new TH1D("h","h",1000,lowM,highM);
   
    Double_t gammas [1000];
    Double_t ergammas [1000];
    Double_t spds [1000];
    Double_t erspds [1000];
    Int_t nSteps = 1;
    Int_t step = 300;
    Int_t start = 0;
    Int_t cutVal = 0;
    Int_t cutValOld = 0;
    
    char cut [3000];

    for(int i=0;i<nSteps;i++)
    {
        cutValOld = start + i*step;
        cutVal = start + (i+1)*step;
//        sprintf(cut,"nSPDHits>%i && nSPDHits<%i && ProtonP_P>10e3 && ProtonM_P>10e3 && ProtonP_PT>2000 && ProtonM_PT>2000 && ProtonP_PIDp>20 && ProtonM_PIDp>20 && (ProtonP_PIDp-ProtonP_PIDK)>15 && (ProtonM_PIDp-ProtonM_PIDK)>15 && Jpsi_PT>6500",cutValOld,cutVal);
        
        sprintf(cut,"nSPDHits>%i && nSPDHits<%i && (Jpsi_Hlt1TrackMVADecision_TOS || Jpsi_Hlt1TwoTrackMVADecision_TOS) && (Jpsi_Hlt2Topo2BodyDecision_TOS || Jpsi_Hlt2Topo3BodyDecision_TOS || Jpsi_Hlt2Topo4BodyDecision_TOS)",cutValOld,cutVal);
        
        ch->Draw("Jpsi_MM>>h",cut,"E");
        h->SaveAs("currentCut.root");
        
        spds[i] = Double_t(cutValOld)+Double_t(step)/Double_t(2);
        erspds[i] = Double_t(step)/Double_t(2);
        Fit_Mass(&gammas[i],&ergammas[i]);
    }
    
    TGraphErrors* gamma_vs_SPD = new TGraphErrors(nSteps,spds,gammas,erspds,ergammas);
    TCanvas* c = new TCanvas("gamma_vs_SPD","gamma_vs_SPD",800,600);
    gamma_vs_SPD->Draw("AP");
}

