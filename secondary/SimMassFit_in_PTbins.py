from ROOT import *
from ROOT.RooFit import *
from ROOT import kRed, kBlack, kDashed, kMagenta, kCyan, kViolet, kTRUE
import random

import sys
sys.path.insert(0, '../')
from makeConfigDictionary import *

minM = minMassSnd
maxM = maxMass
nBins = int((maxM-minM)/binWidthSnd)

from setupPlot import *




def getData(w=0, nPT=0, SystType='Base'):

    if SystType=='nonScaled':
        frombDataHist = ["../Histos/2015/SndTopo/secondaryTopoOpt/Total/Tz0.root",
                         "../Histos/2016/SndTopo/secondaryTopoOpt/Total/Tz0.root"]
        if(nPT>0):
            dataHist =  ["../Histos/2016/SndTopo/secondaryTopoOpt/Jpsi_PT/bin"+str(nPT)+"_Tz0.root",
                         "../Histos/2015/SndTopo/secondaryTopoOpt/Jpsi_PT/bin"+str(nPT)+"_Tz0.root"]
        else:
            dataHist = frombDataHist
    else:
        frombDataHist = ["../Histos/2015/SndTopo_M/secondaryTopoOpt/Total/Tz0.root",
                         "../Histos/2016/SndTopo_M/secondaryTopoOpt/Total/Tz0.root"]
        if(nPT>0):
            dataHist =  ["../Histos/2016/SndTopo_M/secondaryTopoOpt/Jpsi_PT/bin"+str(nPT)+"_Tz0.root",
                         "../Histos/2015/SndTopo_M/secondaryTopoOpt/Jpsi_PT/bin"+str(nPT)+"_Tz0.root"]
        else:
            dataHist = frombDataHist


    f0 = TFile(dataHist[0])
    f1 = TFile(dataHist[1])

    Jpsi_M = w.var("Jpsi_M")

    hh = TH1F("hh","hh",nBins,minM,maxM)

    hhh = f0.Get("Jpsi_M")
    hhh.Add(f1.Get("Jpsi_M"))
    for ii in range(hh.GetNbinsX()):
        median = hh.GetBinCenter(ii+1)
        nbin = hhh.FindBin(median)
        hh.SetBinContent(ii+1,hhh.GetBinContent(nbin))

    # if(shift):
    #     hh.SetBins(276,2705,3395)
    #     Jpsi_M.setRange(2855,3245)

    dh = RooDataHist("dh_pt"+str(nPT),"dh_pt"+str(nPT),RooArgList(Jpsi_M),hh)
    getattr(w,'import')(dh)
    
    f0.Close()
    f1.Close()



def getConstPars(SystType='Base', nPT=0):

    if(nPT==0) : MCResolutionFile = "../resolutions/resoSndTopo_log/MC_MassResolution_fromB__SndTopoOpt_wksp.root"
    elif(nPT>0): MCResolutionFile = "../resolutions/resoSndTopo_log/MC_MassResolution_fromB__SndTopoOpt_wksp_PT"+str(nPT)+".root"

    f = TFile(MCResolutionFile,"READ")
    wMC = RooWorkspace()
    wMC = f.Get("w")

    ratioNtoW = wMC.var("rNarToW").getValV()  

    if SystType=='rEtaToJpsi':
        ratioEtaToJpsi = wMC.var("rEtaToJpsi").getValV()+wMC.var("rEtaToJpsi").getError()
    else:
        ratioEtaToJpsi = wMC.var("rEtaToJpsi").getValV()
    ratioArea = wMC.var("rG1toG2").getValV()

    f.Close()

    return ratioNtoW, ratioEtaToJpsi, ratioArea



def fillRelWorkspace(w=0, SystType='Base', nPT=0):

    Jpsi_M = RooRealVar("Jpsi_M","Jpsi_M",minM,maxM)
    ratioNtoW, ratioEtaToJpsi, ratioArea = getConstPars(SystType=SystType, nPT=nPT)

    gammaEtac = 31.8
    mDiffEtac = 113.301
    eff = 0.035

    print "SETUP ", SystType
    if SystType=='eff_pppi0':  eff = 0.032
    # elif SystType=='halfBinShift':: Jpsi_M.setRange(2855,3245)
    

    rEtaToJpsi = RooRealVar("rEtaToJpsi_pt"+str(nPT),"rEtaToJpsi_pt"+str(nPT), ratioEtaToJpsi)
    rNarToW    = RooRealVar("rNarToW_pt"+str(nPT),"rNarToW_pt"+str(nPT),ratioNtoW)
    rG1toG2    = RooRealVar("rG1toG2_pt"+str(nPT),"rG1toG2_pt"+str(nPT),ratioArea)

    eff_pppi0  = RooRealVar("eff_pppi0","eff_pppi0",eff)
    nEta       = RooRealVar("nEta_pt"+str(nPT),"num of Etac_pt"+str(nPT), 10, 1.e7)
    nJpsi      = RooRealVar("nJpsi_pt"+str(nPT),"num of J/Psi", 10, 1.e7)
    nEtacRel   = RooRealVar("nEtacRel_pt"+str(nPT),"num of Etac", 0.0, 3.0)

    nEta_1 = RooFormulaVar("nEta_1_pt"+str(nPT),"num of Etac","@0*@1*@2",RooArgList(nEtacRel,nJpsi,rG1toG2))
    nEta_2 = RooFormulaVar("nEta_2_pt"+str(nPT),"num of Etac","@0*@1-@2",RooArgList(nEtacRel,nJpsi,nEta_1))

    nJpsi_1 = RooFormulaVar("nJpsi_1_pt"+str(nPT),"num of J/Psi","@0*@1",RooArgList(nJpsi,rG1toG2))
    nJpsi_2 = RooFormulaVar("nJpsi_2_pt"+str(nPT),"num of J/Psi","@0-@1",RooArgList(nJpsi,nJpsi_1))
    nBckgr  = RooRealVar("nBckgr_pt"+str(nPT),"num of backgr",1e7,10,1.e+9)
   
   
    mass_Jpsi = RooRealVar("mass_Jpsi","mean of gaussian",3096.9, 3030, 3150)   
    mass_res  = RooRealVar("mass_res","mean of gaussian",mDiffEtac, 100, 130)
 
    mass_eta    = RooFormulaVar("mass_eta","mean of gaussian","@0-@1",RooArgList(mass_Jpsi,mass_res))
    
    if SystType=='FreeGamma': 
        gamma_eta = RooRealVar("gamma_eta","width of Br-W", gammaEtac, 1., 80.)
    elif SystType=='Gamma34': 
        gamma_eta = RooRealVar("gamma_eta","width of Br-W", 34.)
    else:
        gamma_eta = RooRealVar("gamma_eta","width of Br-W", gammaEtac)

    
    spin_eta    = RooRealVar("spin_eta","spin_eta", 0.)
    radius_eta  = RooRealVar("radius_eta","radius", 1.)
    proton_m    = RooRealVar("proton_m","proton mass", 938.3 )
    sigma_eta_1 = RooRealVar("sigma_eta_1_pt"+str(nPT),"width of gaussian", 9., 5., 50.) 
    sigma_eta_2 = RooFormulaVar("sigma_eta_2_pt"+str(nPT),"width of gaussian","@0/@1",RooArgList(sigma_eta_1,rNarToW))

    sigma_Jpsi_1 = RooFormulaVar("sigma_Jpsi_1_pt"+str(nPT),"width of gaussian","@0/@1",RooArgList(sigma_eta_1,rEtaToJpsi))
    sigma_Jpsi_2 = RooFormulaVar("sigma_Jpsi_2_pt"+str(nPT),"width of gaussian","@0/@1",RooArgList(sigma_Jpsi_1,rNarToW))


    # Fit eta
    gaussEta_1 = RooGaussian("gaussEta_1_pt"+str(nPT),"gaussEta_1 PDF", Jpsi_M, RooFit.RooConst(0),  sigma_eta_1)
    gaussEta_2 = RooGaussian("gaussEta_2_pt"+str(nPT),"gaussEta_2 PDF", Jpsi_M, RooFit.RooConst(0),  sigma_eta_2)

    br_wigner = RooRelBreitWigner("br_wigner", "br_wigner",Jpsi_M, mass_eta, gamma_eta, spin_eta,radius_eta,proton_m,proton_m)

    bwxg_1 = RooFFTConvPdf("bwxg_1_pt"+str(nPT),"breit-wigner (X) gauss", Jpsi_M, br_wigner, gaussEta_1)
    bwxg_2 = RooFFTConvPdf("bwxg_2_pt"+str(nPT),"breit-wigner (X) gauss", Jpsi_M, br_wigner, gaussEta_2)

    # Fit J/psi
    gauss_1 = RooGaussian("gauss_1_pt"+str(nPT),"gaussian PDF",Jpsi_M,mass_Jpsi,sigma_Jpsi_1)
    gauss_2 = RooGaussian("gauss_2_pt"+str(nPT),"gaussian PDF",Jpsi_M,mass_Jpsi,sigma_Jpsi_2)



    if SystType!='Chebychev':
        a0 = RooRealVar("a0_pt"+str(nPT),"a0",-0.001,-1.0,0)
        a1 = RooRealVar("a1_pt"+str(nPT),"a1", 0.,-1,1)
        a2 = RooRealVar("a2_pt"+str(nPT),"a2",-0,-1,1)
    else:
        a0 = RooRealVar("a0_pt"+str(nPT),"a0",-0.4,-1.0,10.8)
        a1 = RooRealVar("a1_pt"+str(nPT),"a1",0.05,-1.,1.)
        a2 = RooRealVar("a2_pt"+str(nPT),"a2",-0.005,-1.,1.)
    a3 = RooRealVar("a3_pt"+str(nPT),"a3",0.005,-0.1,0.1)
    a4 = RooRealVar("a4_pt"+str(nPT),"a4",0.005,-0.1,0.1)
    a5 = RooRealVar("a5_pt"+str(nPT),"a5",-0.0001,-0.01,0)


    if SystType!='Chebychev':
        bkg = RooChebychev("bkg_pt"+str(nPT),"Background",Jpsi_M,RooArgList(a0,a1,a2))
    else:
        bkg = RooGenericPdf("bkg_pt"+str(nPT),"background","TMath::Exp(-(@0-3050.)/200.*@1)*(1.+@2*(@0-3050.)/200.+@3*(@0-3050.)*(@0-3050.)/200./200.)",RooArgList(Jpsi_M,a0,a1,a2))

    pppi0 = RooGenericPdf("pppi0_pt"+str(nPT),"Jpsi.pppi0","@0<(2961.92) ? TMath::Sqrt(3096.900-134.977-@0) : 0",RooArgList(Jpsi_M))
    nPPPi0 = RooFormulaVar("nPPPi0_pt"+str(nPT),"nPPPi0","@0*@1*(1.19/2.12)",RooArgList(nJpsi,eff_pppi0))


    modelBkg = RooAddPdf("modelBkg_pt"+str(nPT),"background", RooArgList(bkg,pppi0), RooArgList(nBckgr,nPPPi0))

    if(SystType=='CB'):

        if(nPT==0) : MCResolutionFile = "../resolutions/resoSndTopo_log/MC_MassResolution_fromB__CB_SndTopoOpt_wksp.root"
        elif(nPT>0): MCResolutionFile = "../resolutions/resoSndTopo_log/MC_MassResolution_fromB__CB_SndTopoOpt_PT"+str(nPT)+".root"
        f = TFile(MCResolutionFile,"READ") 

        wMC = f.Get("w")
        f.Close()
        alpha_eta_1 = RooRealVar('alpha_eta_1','alpha of CB', wMC.var('alpha_eta_1').getValV(), 0.0, 10.) 
        alpha_eta_1.setConstant(True)
        n_eta_1 = RooRealVar('n_eta_1','n of CB', wMC.var('n_eta_1').getValV(), 0.0, 100.) 
        n_eta_1.setConstant(True)       

        cb_etac_1 = BifurcatedCB("cb_etac_1_PT%s"%(nPT), "Cystal Ball Function", Jpsi_M, RooFit.RooConst(0), sigma_eta_1, alpha_eta_1, n_eta_1, alpha_eta_1, n_eta_1)   
        bwxg_1 = RooFFTConvPdf("bwxg_1_PT%s"%(nPT),"breit-wigner (X) gauss", Jpsi_M, br_wigner, cb_etac_1) 

        cb_Jpsi_1 = BifurcatedCB("cb_Jpsi_1_PT%s"%(nPT), "Cystal Ball Function", Jpsi_M, mass_Jpsi, sigma_Jpsi_1, alpha_eta_1, n_eta_1, alpha_eta_1, n_eta_1)
        modelSignal = RooAddPdf("modelSignal_pt"+str(nPT),"signal", RooArgList(bwxg_1,cb_Jpsi_1), RooArgList(nEta,nJpsi))
        model = RooAddPdf("model_pt"+str(nPT),"signal+bkg", RooArgList(bwxg_1,cb_Jpsi_1, bkg,pppi0), RooArgList(nEta,nJpsi, nBckgr,nPPPi0))
    else:
        modelSignal = RooAddPdf("modelSignal_pt"+str(nPT),"signal", RooArgList(bwxg_1, bwxg_2,gauss_1,gauss_2), RooArgList(nEta_1,nEta_2,nJpsi_1,nJpsi_2))
        model = RooAddPdf("model_pt"+str(nPT),"signal+bkg", RooArgList(bwxg_1, bwxg_2,gauss_1,gauss_2,bkg,pppi0), RooArgList(nEta_1,nEta_2,nJpsi_1,nJpsi_2,nBckgr,nPPPi0))


    getattr(w,'import')(model,RooFit.RecycleConflictNodes())
    getattr(w,'import')(modelBkg,RooFit.RecycleConflictNodes())
    getattr(w,'import')(modelSignal,RooFit.RecycleConflictNodes())

    return model



def fitData(SystType='Base', nPTs=[0]):

    gROOT.Reset()  
    gROOT.LoadMacro("../libRooRelBreitWigner/RooRelBreitWigner.cxx+") 
    gROOT.LoadMacro("../BifurcatedCB/BifurcatedCB.cxx+")
    proof = TProof.Open("")

    w = RooWorkspace("w",True)

    hists = []
    nPTbins = len(nPTs)

    model = fillRelWorkspace(w=w,SystType=SystType,nPT=0)

    sample = RooCategory("sample","sample") 

    for npt in nPTs:
        nPT = npt
        sample.defineType("PT"+str(nPT)) 


    Jpsi_M = w.var("Jpsi_M")
    Jpsi_M.setBins(nBins)

    for npt in nPTs:
        nPT = npt
        model = fillRelWorkspace(w=w,SystType='Base',nPT=nPT)
        getData(w,nPT=nPT,SystType=SystType)
        histo   = w.data("dh_pt"+str(nPT))
        hists.append(histo)

    
    model_pts = []
    for nPT in nPTs:
        model_pts.append(w.pdf("model_pt"+str(nPT)))



    if nPTs==[0]:
        combData = RooDataHist("combData","combData",RooArgList(Jpsi_M,sample),RooFit.Index(sample),RooFit.Import("PT0",hists[0]))
    else:
        # combData = RooDataHist("combData","combData",RooArgList(Jpsi_M,sample),RooFit.Index(sample),RooFit.Import("PT1",hists[0]),RooFit.Import("PT2",hists[1]),RooFit.Import("PT3",hists[2]),RooFit.Import("PT4",hists[3]),RooFit.Import("PT5",hists[4]))
        combData = RooDataHist("combData","combData",RooArgList(Jpsi_M,sample),RooFit.Index(sample),RooFit.Import("PT1",hists[0]),RooFit.Import("PT2",hists[1]),RooFit.Import("PT3",hists[2]),RooFit.Import("PT4",hists[3]),RooFit.Import("PT5",hists[4]),RooFit.Import("PT6",hists[5]),RooFit.Import("PT7",hists[6]))


    simPdf = RooSimultaneous("smodel","",sample)
    for nPT in nPTs:
        idx = nPTs.index(nPT)
        simPdf.addPdf(model_pts[idx],"PT"+str(nPT))

    mass_res = w.var("mass_res")

    simPdf.fitTo(combData,RooFit.Strategy(2))
    simPdf.fitTo(combData)
    r = simPdf.fitTo(combData,RooFit.Minos(True),RooFit.Save(True))


    frames = []
    pulls  = []
    names  = []
    for nPT in nPTs:
        pdf   = w.pdf("model_pt"+str(nPT))
        histo = w.data("dh_pt"+str(nPT))
        frame, pull, chi2 = setupFrame(Jpsi_M, "_pt_"+str(nPT), histo, pdf)
        frames.append(frame)
        pulls.append(pull)
        names.append("")

    c = setupCanvas(names, frames, pulls)
    c.SaveAs("fitResults/plots/simFit_{}bins_{}.pdf".format(nPTbins,SystType))

    save = os.dup( sys.stdout.fileno() ) 
    fileOut = file("fitResults/parsPrint/simFit_{}bins_{}.txt".format(nPTbins,SystType), 'w' ) 
    os.dup2( fileOut.fileno(), sys.stdout.fileno() ) 
    r.Print("v") 
    os.dup2( save, sys.stdout.fileno() ) 
    fileOut.close()

    return mass_res.getVal(), mass_res.getError() 



resultDict = {}
systs = ["nonScaled","FreeGamma","eff_pppi0","Gamma34","Chebychev","CB","7_PtBins","rEtaToJpsi"]

mDiffBase, mDiffBaseErr = fitData(SystType='Base',nPTs=[0])
resultDict['Base'] = "{}  +-  {}".format(mDiffBase, mDiffBaseErr)

for syst in systs:
    mDiff, mDiffErr = fitData(SystType=syst,nPTs=[0])
    resultDict[syst] = "{}  +-  {}".format(mDiff-mDiffBase, mDiffErr)

mDiff, mDiffErr = fitData(SystType="7_PtBins",nPTs=[1,2,3,4,5,6,7])
resultDict["7_PtBins"] = "{}  +-  {}".format(mDiff-mDiffBase, mDiffErr)


import pprint
pprint.pprint(resultDict)

