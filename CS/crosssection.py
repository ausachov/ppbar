from extrapolateJpsi import *


from ROOT import *
from ROOT.TMath import *
from ROOT.TMath import Sq, Sqrt
from array import array


homeDir = "../"
nPTBins = 4

L   = 2.0
eff = 0.99
effErr  = 0.02
PTBins  = array("d", [7.25, 9.0, 11.0, 13.0, 16.0])
PTBinHW = array("d", [0.75, 1.0, 1.0, 1.0, 2.0])
ptBinning = array("d", [6.5, 8.0, 10.0, 12.0, 14.0])

BREtacToPP    = 1.41e-3
BREtacToPPErr = 0.17e-3

BRJpsiToPP    = 2.17e-3
BRJpsiToPPErr = 0.07e-3

errBRrel = ((BRJpsiToPPErr/BRJpsiToPP)**2 + (BREtacToPPErr/BREtacToPP)**2)**0.5

rangeL, rangeR  = 6.5, 14.

mcDir = "../MC/tuples/selected/"

class uncPT():
    def __init__(self):
        self.d = {}
        for key in ["val","stat","uncorr","corr","systTot","SS","norm","tot"]:
            self.d[key] = array("d", nPTBins*[0])

    def calculateErrors(self,iB):
        Val    = self.d["val"][iB]
        Stat   = self.d["stat"][iB]
        Uncorr = self.d["uncorr"][iB]
        Corr   = self.d["corr"][iB]
        Norm   = self.d["norm"][iB]

        self.d["systTot"][iB]    = (Uncorr**2 + Corr**2)**0.5
        self.d["SS"][iB]         = (Uncorr**2 + Corr**2 + Stat**2)**0.5
        self.d["tot"][iB]        = (Uncorr**2 + Corr**2 + Stat**2 + Norm**2)**0.5

    def copyRelUncts(self,origin,iB,val):
        oldVal  = origin.d["val"][iB]
        self.d["val"][iB] = val
        for key in ["stat","stat","uncorr","corr","systTot","SS","norm"]:
            self.d[key][iB] = val*origin.d[key][iB]/oldVal


    def addRelUnct(self,key,iB,valRel):
        if key=="uncorr" or key=="corr":
            otherKeys = ["systTot","SS","tot"]
        if key=="norm" :
            otherKeys = ["tot"]

        otherKeys.append(key)
        for kkey in otherKeys:
            oldUnc = self.d[kkey][iB]
            val    = self.d["val"][iB]
            self.d[kkey][iB] = val*((oldUnc/val)**2 + valRel**2)**0.5


    def calculateTotalCS(self):
        totCS      = 0
        stat       = 0
        systUncorr = 0
        systCorr   = 0
        syst       = 0
        norm       = 0
        for iB in range(nPTBins):
            valPT = self.d["val"][iB]*PTBinHW[iB]*2
            totCS      += valPT
            stat       += self.d["stat"][iB]*PTBinHW[iB]*2
            systUncorr += self.d["uncorr"][iB]*PTBinHW[iB]*2

            relCorr     = self.d["corr"][iB]*PTBinHW[iB]*2/valPT
            systCorr   += relCorr/float(nPTBins)

            norm = self.d["norm"][iB]*PTBinHW[iB]*2/valPT

        systCorr = systCorr*totCS
        norm     = norm*totCS

        systTot = (systUncorr**2 + systCorr**2)**0.5

        self.IntegralXsec = {"val"       :totCS,
                             "stat"      :stat,
                             "uncorr"    :systUncorr,
                             "corr"      :systCorr,
                             "systTot"   :systTot,
                             "norm"      :norm}

def createHisto(hName = "", vals=[], errors=[]):
    hist = TH1F(hName,hName,nPTBins,ptBinning)
    for ii in range(nPTBins):
        hist.SetBinContent(ii+1, vals[ii])
        hist.SetBinError(ii+1, errors[ii])
    return hist


def crosssection(CSfilename="Results/rel_PT_sim.txt"):

    NJpsiPrDict  = uncPT()
    NJpsiSecDict = uncPT()

    NEtacPrDict  = uncPT()
    NEtacSecDict = uncPT()


    fi = open(homeDir+CSfilename,"r")

    keys = ["val","stat","uncorr","corr"]
    for iPT in range(nPTBins):
        for key in keys:
            line = fi.readline()
            NJpsiPrDict.d[key][iPT], NJpsiSecDict.d[key][iPT], NEtacPrDict.d[key][iPT], NEtacSecDict.d[key][iPT] = float(line.split()[0]), float(line.split()[1]), float(line.split()[2]), float(line.split()[3])
            NJpsiPrDict.calculateErrors(iPT)
            NJpsiSecDict.calculateErrors(iPT)
            NEtacPrDict.calculateErrors(iPT)
            NEtacPrDict.calculateErrors(iPT)
        line = fi.readline()
    fi.close()

    CSRelPrDict  = uncPT()
    CSRelSecDict = uncPT()

    CSEtacPrDict  = uncPT()
    CSEtacSecDict = uncPT()

    CSJpsiPrDict  = uncPT()
    CSJpsiSecDict = uncPT()


    fiS = open(homeDir+"Results/CSJpsi_new.txt","r")
    for iPT in range(nPTBins):
        i=0
        line = fiS.readline()
        for key in ["val","stat","uncorr","corr","norm"]:
            CSJpsiPrDict.d[key][iPT] = float(line.split()[i])/(2*PTBinHW[iPT])
            i+=1
        CSJpsiPrDict.calculateErrors(iPT)

    line = fiS.readline()
    for iPT in range(nPTBins):
        i=0
        line = fiS.readline()
        for key in ["val","stat","uncorr","corr","norm"]:
            CSJpsiSecDict.d[key][iPT] = float(line.split()[i])/(2*PTBinHW[iPT])
            i+=1
        CSJpsiSecDict.calculateErrors(iPT)
    fiS.close()


    for iB in range(nPTBins):

        valRelPrXsec = NEtacPrDict.d["val"][iB]*BRJpsiToPP/BREtacToPP/eff
        CSRelPrDict.copyRelUncts(NEtacPrDict,iB,valRelPrXsec)
        CSRelPrDict.addRelUnct("uncorr",iB,effErr/eff)
        CSRelPrDict.addRelUnct("norm",  iB,errBRrel)
        CSRelPrDict.calculateErrors(iB)
        

        valRelSecXsec = NEtacSecDict.d["val"][iB]*BRJpsiToPP/BREtacToPP/eff
        CSRelSecDict.copyRelUncts(NEtacSecDict,iB,valRelSecXsec)
        CSRelSecDict.addRelUnct("uncorr",iB,effErr/eff)
        CSRelSecDict.addRelUnct("norm",  iB,errBRrel)
        CSRelSecDict.calculateErrors(iB)


        valEtacPrXsec = CSRelPrDict.d["val"][iB]*CSJpsiPrDict.d["val"][iB]
        CSEtacPrDict.copyRelUncts(CSRelPrDict,iB,valEtacPrXsec)
        ErrJpsiProdRel = CSJpsiPrDict.d["tot"][iB]/CSJpsiPrDict.d["val"][iB]
        CSEtacPrDict.addRelUnct("norm",iB,ErrJpsiProdRel)
        CSEtacPrDict.calculateErrors(iB)
        

        valEtacSecXsec = CSRelSecDict.d["val"][iB]*CSJpsiSecDict.d["val"][iB]
        CSEtacSecDict.copyRelUncts(CSRelSecDict,iB,valEtacSecXsec)
        ErrJpsiProdRel = CSJpsiSecDict.d["tot"][iB]/CSJpsiSecDict.d["val"][iB]
        CSEtacSecDict.addRelUnct("norm",iB,ErrJpsiProdRel)
        CSEtacSecDict.calculateErrors(iB)

    CSJpsiPrDict.calculateTotalCS()
    CSJpsiSecDict.calculateTotalCS()


    CSEtacPrDict.calculateTotalCS()
    CSEtacSecDict.calculateTotalCS()


    CSEtacPrTot = CSEtacPrDict.IntegralXsec
    print "prompt eta_c  ", CSEtacPrTot["val"], \
                            CSEtacPrTot["stat"], \
                            CSEtacPrTot["systTot"], \
                            CSEtacPrTot["norm"], \
                            "\n"

    CSEtacSecTot = CSEtacSecDict.IntegralXsec
    print "fromB eta_c  ",  CSEtacSecTot["val"], \
                            CSEtacSecTot["stat"], \
                            CSEtacSecTot["systTot"], \
                            CSEtacSecTot["norm"], \
                            "\n"

    CSJpsiPrTot = CSJpsiPrDict.IntegralXsec
    print "prompt J/psi  ", CSJpsiPrTot["val"], \
                            CSJpsiPrTot["stat"], \
                            CSJpsiPrTot["systTot"],\
                            CSJpsiPrTot["norm"], \
                            "\n"

    CSJpsiSecTot = CSJpsiSecDict.IntegralXsec
    print "fromB J/psi  ",  CSJpsiSecTot["val"], \
                            CSJpsiSecTot["stat"], \
                            CSJpsiSecTot["systTot"],\
                            CSJpsiSecTot["norm"], \
                            "\n"

    return  CSRelPrDict,  CSRelSecDict,\
            CSEtacPrDict, CSEtacSecDict,\
            CSJpsiPrDict, CSJpsiSecDict,\
            NEtacPrDict, NEtacSecDict



def makeCSPlots(empty,TypePrefix, CSRelPrDict,CSRelSecDict, CSEtacPrDict, CSEtacSecDict, CSJpsiPrDict, CSJpsiSecDict, NEtacPrDict, NEtacSecDict):


    fitNRelPr    = TF1("firNRelPr_"+TypePrefix,  "[0]+[1]*x", 6.5, 14.0)
    fitNRelSec   = TF1("firNRelSec_"+TypePrefix, "[0]+[1]*x", 6.5, 14.0)

    fitCSRelPr   = TF1("fitCSJpsiPr_"+TypePrefix, "[0]+[1]*x", 6.5, 14.0)
    fitCSRelPr.SetParNames("Const","LinSlope")
    fitCSRelPr.SetParameters(1e6,0.2)

    fitCSJpsiPr  = TF1("fitCSJpsiPr_"+TypePrefix,  "[0]*exp(-[1]*x)", 6.5, 14.0)
    fitCSJpsiPr.SetParNames("AmpJpsiPr","ExpJpsiPr")
    fitCSJpsiPr.SetParameters(1e6,0.2)
    fitCSJpsiPr.SetLineColor(4)

    fitCSJpsiSec = TF1("fitCSJpsiSec_"+TypePrefix, "[0]*exp(-[1]*x)", 6.5, 14.0)

    fitCSEtacPr  = TF1("fitCSEtacPr_"+TypePrefix,   "[0]*exp(-[1]*x)", 6.5, 14.0)
    fitCSEtacPr.SetParNames("AmpEtacPr","ExpEtacPr")
    fitCSEtacPr.SetParameters(1e6,0.2)
    fitCSEtacPr.SetLineColor(2)

    fitCSEtacSec = TF1("fitCSEtacSec_"+TypePrefix, "[0]*exp(-[1]*x)",  6.5, 14.0)


    if empty:
        gStyle.SetOptFit(0)
        gStyle.SetOptStat(0)
        gStyle.SetOptTitle(0)
    else:
        gStyle.SetOptFit(1)
        gStyle.SetOptStat(1)
        gStyle.SetOptTitle(0)        
    
    
    texCS = TLatex()
    texCS.SetNDC()
    
    canv_1 = TCanvas("canv_prRel_"+TypePrefix, "1",55,55,550,400)
    canv_2 = TCanvas("canv_secRel_"+TypePrefix,"2",55,55,550,400)

    canv_1.cd()

    sigmaPrRel     = createHisto("sigmaPrRel",CSRelPrDict.d["val"],CSRelPrDict.d["tot"])
    sigmaPrRelStat = createHisto("sigmaPrRelStat",CSRelPrDict.d["val"],CSRelPrDict.d["stat"])
    sigmaPrRelSS   = createHisto("sigmaPrRelSS",CSRelPrDict.d["val"],CSRelPrDict.d["SS"])
    
    gPad.SetLeftMargin(0.20)
    #gPad.SetBottomMargin(0.125)

    sigmaPrRelSS.Fit(fitCSRelPr,"IMER")
    # fitCSRelPr.SetLineColor(2)

    sigmaPrRel.Draw("E1")
    sigmaPrRelSS.Draw("E1 SAME")
    sigmaPrRelStat.Draw("E1 SAME")
 
    sigmaPrRel.SetMinimum(0.0)
    sigmaPrRel.SetTitle("#frac{d#sigma_{#eta_{c}}/dp_{T}}{d#sigma_{J/#psi}/dp_{T}}")
    sigmaPrRel.GetYaxis().SetTitle("#frac{d#sigma_{#eta_{c}}/dp_{T}}{d#sigma_{J/#psi}/dp_{T}}")
    sigmaPrRel.GetYaxis().SetTitleOffset(1.1)   


    texCS.DrawLatex(0.3, 0.8, "LHCb preliminary")
    texCS.DrawLatex(0.3, 0.75, "#sqrt{s} = 13 TeV")
    texCS.DrawLatex(0.3, 0.7, "2.0 < y < 4.5")

    canv_2.cd()
    #gPad.SetBottomMargin(0.125)
    
    sigmaSecRel     = createHisto("sigmaSecRel",CSRelSecDict.d["val"],CSRelSecDict.d["tot"])
    sigmaSecRelStat = createHisto("sigmaSecRelStat",CSRelSecDict.d["val"],CSRelSecDict.d["stat"])
    sigmaSecRelSS   = createHisto("sigmaSecRelSS",CSRelSecDict.d["val"],CSRelSecDict.d["SS"])

    gPad.SetLeftMargin(0.20)


    sigmaSecRel.Draw("E1")
    sigmaSecRelStat.Draw("E1 SAME")
    sigmaSecRelSS.Draw("E1 SAME")

    sigmaSecRel.SetMinimum(0.0)
    sigmaSecRel.SetTitle("#frac{d#sigma_{b#rightarrow#eta_{c}X}/dp_{T}}{d#sigma_{b#rightarrowJ/#psiX}/dp_{T}}")
    sigmaSecRel.GetYaxis().SetTitle("#frac{d#sigma_{b#rightarrow#eta_{c}X}/dp_{T}}{d#sigma_{b#rightarrowJ/#psiX}/dp_{T}}")
    sigmaSecRel.GetYaxis().SetTitleOffset(1.1)

    
    texCS.DrawLatex(0.3, 0.35, "LHCb preliminary")
    texCS.DrawLatex(0.3, 0.30, "#sqrt{s} = 13 TeV")
    texCS.DrawLatex(0.3, 0.25, "2.0 < y < 4.5")

    canv_pr  = TCanvas("canv_pr_"+TypePrefix,"J/Psi_Mass",55,55,500,400)
    canv_sec = TCanvas("canv_sec_"+TypePrefix,"J/Psi_Mass",55,55,500,400)
    canv_pr.cd()

    sigmaPr     = createHisto("sigmaPr",CSEtacPrDict.d["val"],CSEtacPrDict.d["tot"])
    sigmaPrStat = createHisto("sigmaPrStat",CSEtacPrDict.d["val"],CSEtacPrDict.d["stat"])
    sigmaPrSS   = createHisto("sigmaPrSS",CSEtacPrDict.d["val"],CSEtacPrDict.d["SS"])
    sigmaJpsiPr = createHisto("sigmaJpsiPr",CSJpsiPrDict.d["val"],CSJpsiPrDict.d["tot"])

    canv_pr.SetLogy()
    gPad.SetLeftMargin(0.20)
    #gPad.SetBottomMargin(0.125)

    sigmaPrSS.Fit(fitCSEtacPr,"IMER")
    fitCSEtacPr.SetLineColor(2)

    sigmaJpsiPr.Fit(fitCSJpsiPr,"IMER")


    sigmaPr.Draw("E1")
    sigmaPrSS.Draw("E1 SAME")
    sigmaPrStat.Draw("E1 SAME")

    sigmaJpsiPr.Draw("E1 SAME")
    sigmaPr.SetTitle("d#sigma_{p}/dp_{t}")
    sigmaPr.SetMinimum(1.0)
    sigmaPr.SetMaximum(1.e3)
    sigmaPr.GetYaxis().SetTitle("d#sigma/dp_{T} [nb/ GeV/c]")
    sigmaPr.GetYaxis().SetLimits(1, 500)
    sigmaJpsiPr.SetLineWidth(2)
    sigmaJpsiPr.SetLineColor(4)

    fitCSJpsiPr.SetLineColor(4)
    sigmaJpsiPr.SetMarkerStyle(4)
    sigmaJpsiPr.SetMarkerColor(4)
    #fitCSEtacPr.SetLineColor(kBlack)
    #fitCSEtacPr.SetLineStyle(2)
    #fitCSJpsiPr.SetLineStyle(2)


    leg1 = TLegend(0.25,0.47,0.35,0.65)
    leg1.SetBorderSize(0)
    leg1.AddEntry(sigmaPr,"#eta_{c}","lep")
    leg1.AddEntry(sigmaJpsiPr,"J/#psi","lep")
    leg1.Draw()

    texCS.DrawLatex(0.25, 0.36, "LHCb preliminary")
    texCS.DrawLatex(0.25, 0.3, "#sqrt{s} = 13 TeV")
    texCS.DrawLatex(0.25, 0.24, "2.0 < y < 4.5")


    canv_sec.cd()
    gPad.SetLeftMargin(0.20)
    #gPad.SetBottomMargin(0.125)
    
    sigmaSec     = createHisto("sigmaSec",CSEtacSecDict.d["val"],CSEtacSecDict.d["tot"])
    sigmaSecStat = createHisto("sigmaSecStat",CSEtacSecDict.d["val"],CSEtacSecDict.d["stat"])
    sigmaSecSS   = createHisto("sigmaSecSS",CSEtacSecDict.d["val"],CSEtacSecDict.d["SS"])
    sigmaJpsiSec = createHisto("sigmaJpsiSec",CSJpsiSecDict.d["val"],CSJpsiSecDict.d["tot"])

    canv_sec.SetLogy()
    #sigmaSec.Fit(fitCSEtacSec,"I")
    #sigmaJpsiSec.Fit(fitCSJpsiSec,"I")


    sigmaSec.Draw("E1")
    sigmaSecStat.Draw("E1 SAME")
    sigmaSecSS.Draw("E1 SAME")
    sigmaJpsiSec.Draw("E1 SAME")
    sigmaSec.SetTitle("d#sigma_{s}/dp_{t}")
    sigmaSec.SetLineWidth(2)

    sigmaSec.SetMinimum(1.0)
    sigmaSec.SetMaximum(1.0e2)
    sigmaSec.GetYaxis().SetLimits(0.3, 100)
    sigmaSec.GetYaxis().SetTitle("d#sigma_{b#rightarrow#eta_{c}X}/dp_{T} [nb/ Gev/c]")
    
    sigmaJpsiSec.SetLineWidth(2)
    sigmaJpsiSec.SetMarkerStyle(4)
    sigmaJpsiSec.SetMarkerColor(4)
    sigmaJpsiSec.SetLineColor(kBlue)

    texCS.DrawLatex(0.25, 0.36, "LHCb preliminary")
    texCS.DrawLatex(0.25, 0.3, "#sqrt{s} = 13 TeV")
    texCS.DrawLatex(0.25, 0.24, "2.0 < y < 4.5")
    leg1.Draw()



    for graph in [sigmaPrRel,sigmaPrRelStat,sigmaPrRelSS, \
              sigmaSecRel,sigmaSecRelStat,sigmaSecRelSS, \
              sigmaPr, sigmaPrStat, sigmaPrSS, \
              sigmaSec, sigmaSecStat, sigmaSecSS]:
        graph.SetLineColor(2)
        graph.SetMarkerStyle(4)
        graph.SetMarkerColor(2)
        graph.GetXaxis().SetLimits(rangeL,rangeR)
        graph.GetXaxis().SetTitle("p_{t}, GeV/c")


    canv_1.SaveAs(homeDir+"Results/CS/CSRelPrompt_"+TypePrefix+".pdf")
    canv_2.SaveAs(homeDir+"Results/CS/CSRelFromB_"+TypePrefix+".pdf")

    canv_pr.SaveAs(homeDir+"Results/CS/CSEtacPrompt_"+TypePrefix+".pdf")
    canv_sec.SaveAs(homeDir+"Results/CS/CSEtacSec_"+TypePrefix+".pdf")




    canv_Pr2  = TCanvas("canv_Pr2_"+TypePrefix,"N Rel",55,55,550,400)
    canv_Sec2 = TCanvas("canv_Sec2_"+TypePrefix,"N Rel",55,55,550,400)

    canv_Pr2.cd()
    gPad.SetLeftMargin(0.20)
    #gPad.SetBottomMargin(0.125)

    nRelPr     = createHisto("nRelPr",NEtacPrDict.d["val"],NEtacPrDict.d["tot"])
    nRelPrStat = createHisto("nRelPrStat",NEtacPrDict.d["val"],NEtacPrDict.d["stat"])

    # nRelPr.Fit(fitNRelPr,"IMER")
    nRelPr.Draw("E1")
    nRelPrStat.Draw("E1 SAME")
    # fitNRelPr.Draw("E1 SAME")
    nRelPr.SetTitle("prompt")
    nRelPr.SetLineColor(6)
    nRelPr.SetMinimum(0.0)
    nRelPr.SetMaximum(3.0)
    nRelPr.GetYaxis().SetTitle("#frac{#sigma_{#eta_{c}} #times BR(#eta_{c} #rightarrow p#bar{p})}{#sigma_{J/#psi} #times BR(J/#psi #rightarrow p#bar{p})}")
    nRelPr.GetXaxis().SetTitleOffset(0.90)
    nRelPr.GetYaxis().SetTitleOffset(1.10)
    nRelPrStat.SetLineColor(4)
    nRelPrStat.SetMarkerStyle(23)


    texCS.DrawLatex(0.25, 0.76, "LHCb preliminary")
    texCS.DrawLatex(0.25, 0.70, "#sqrt{s} = 13 TeV")
    texCS.DrawLatex(0.25, 0.64, "2.0 < y < 4.5")

    canv_Sec2.cd()
    gPad.SetLeftMargin(0.20)
    #gPad.SetBottomMargin(0.125)

    nRelSec     = createHisto("nRelSec",NEtacSecDict.d["val"],NEtacSecDict.d["tot"])
    nRelSecStat = createHisto("nRelSecStat",NEtacSecDict.d["val"],NEtacSecDict.d["stat"])

    # nRelSec.Fit(fitNRelSec, "IMER")
    nRelSec.Draw("E1")
    nRelSecStat.Draw("E1 SAME")
    # fitNRelSec.Draw("E1 SAME")
    nRelSec.SetTitle("from-b")
    nRelSec.SetLineColor(6)
    nRelSec.SetMinimum(0.0)
    nRelSec.SetMaximum(0.8)
    nRelSec.GetYaxis().SetTitle("#frac{#sigma_{b#rightarrow#eta_{c}X} #times BR(#eta_{c} #rightarrow p#bar{p})}{#sigma_{b#rightarrowJ/#psiX} #times BR(J/#psi #rightarrow p#bar{p})}")
    nRelSec.GetXaxis().SetTitleOffset(0.90)
    nRelSec.GetYaxis().SetTitleOffset(1.10)
    nRelSecStat.SetLineColor(4)

    texCS.DrawLatex(0.25, 0.76, "LHCb preliminary")
    texCS.DrawLatex(0.25, 0.7, "#sqrt{s} = 13 TeV")
    texCS.DrawLatex(0.25, 0.64, "2.0 < y < 4.5")

    for graph in [nRelPrStat,nRelPr,nRelSecStat,nRelSec]:
        graph.GetXaxis().SetLimits(rangeL,rangeR)
        graph.GetXaxis().SetTitle("p_{t}, GeV/c")
        graph.SetLineWidth(2)
        graph.SetMarkerStyle(23)


    canv_Pr2.SaveAs(homeDir+"Results/CS/NRelPrompt_"+TypePrefix+".pdf")
    canv_Sec2.SaveAs(homeDir+"Results/CS/NRelSec_"+TypePrefix+".pdf")


    fo = open(homeDir+"Results/CS/CSTotal_new_"+TypePrefix+".txt","w")

    fo.write("Etac Prompt dSigma/dPT [nb/ GeV/c] \n")
    for iBin in range(nPTBins):
        fo.write( "%5.2f$\\pm$%5.2f$\\pm$%5.2f$\\pm$%5.2f$\\pm$%5.2f \n"%(CSEtacPrDict.d["val"][iBin], CSEtacPrDict.d["stat"][iBin], CSEtacPrDict.d["uncorr"][iBin], CSEtacPrDict.d["corr"][iBin], CSEtacPrDict.d["norm"][iBin]))

    fo.write("\n Jpsi Prompt dSigma/dPT [nb/ GeV/c] (ANA 2015) \n")
    for iBin in range(nPTBins):
        fo.write( "%5.2f$\\pm$%5.2f$\\pm$%5.2f$\\pm$%5.2f \n"%(CSJpsiPrDict.d["val"][iBin], CSJpsiPrDict.d["stat"][iBin], CSJpsiPrDict.d["uncorr"][iBin], CSJpsiPrDict.d["corr"][iBin]))

    fo.write("\n Etac from-b dSigma/dPT [nb/ GeV/c] \n")
    for iBin in range(nPTBins):
        fo.write( "%5.2f$\\pm$%5.2f$\\pm$%5.2f$\\pm$%5.2f$\\pm$%5.2f \n"%(CSEtacSecDict.d["val"][iBin], CSEtacSecDict.d["stat"][iBin], CSEtacSecDict.d["uncorr"][iBin], CSEtacSecDict.d["corr"][iBin], CSEtacSecDict.d["norm"][iBin]))

    fo.write("\n Jpsi from-b dSigma/dPT [nb/ GeV/c] (ANA 2015) \n")
    for iBin in range(nPTBins):
        fo.write( "%5.2f$\\pm$%5.2f$\\pm$%5.2f$\\pm$%5.2f \n"%(CSJpsiSecDict.d["val"][iBin], CSJpsiSecDict.d["stat"][iBin], CSJpsiSecDict.d["uncorr"][iBin], CSJpsiSecDict.d["corr"][iBin]))

    fo.write("Etac Rel Prompt dSigma/dPT [nb/ GeV/c] \n")
    for iBin in range(nPTBins):
        fo.write( "%5.2f$\\pm$%5.2f$\\pm$%5.2f$\\pm$%5.2f$\\pm$%5.2f \n"%(CSRelPrDict.d["val"][iBin], CSRelPrDict.d["stat"][iBin], CSRelPrDict.d["uncorr"][iBin], CSRelPrDict.d["corr"][iBin], CSRelPrDict.d["norm"][iBin]))

    fo.write("\n Etac Rel from-b dSigma/dPT [nb/ GeV/c] \n")
    for iBin in range(nPTBins):
        fo.write( "%5.2f$\\pm$%5.2f$\\pm$%5.2f$\\pm$%5.2f$\\pm$%5.2f \n"%(CSRelSecDict.d["val"][iBin], CSRelSecDict.d["stat"][iBin], CSRelSecDict.d["uncorr"][iBin], CSRelSecDict.d["corr"][iBin], CSRelSecDict.d["norm"][iBin]))
    fo.close()
        

def makePlotsTzFit_vs_TzCut(empty, CSRelPrDict_tzFit, CSRelSecDict_tzFit, CSRelPrDict_tzCut,CSRelSecDict_tzCut):

    if empty:
        gStyle.SetOptFit(0)
        gStyle.SetOptStat(0)
        gStyle.SetOptTitle(0)
    else:
        gStyle.SetOptFit(1)
        gStyle.SetOptStat(1)
        gStyle.SetOptTitle(0)        
    
    
    texCS = TLatex()
    texCS.SetNDC()
    
    canv_1 = TCanvas("canv_prRel_comp", "1",55,55,550,400)
    canv_2 = TCanvas("canv_secRel_comp","2",55,55,550,400)

    canv_1.cd()

    sigmaPrRel_tzFit     = TGraphErrors(4,PTBins,CSRelPrDict_tzFit.d["val"],PTBinHW,CSRelPrDict_tzFit.d["tot"])
    sigmaPrRelStat_tzFit = TGraphErrors(4,PTBins,CSRelPrDict_tzFit.d["val"],PTBinHW,CSRelPrDict_tzFit.d["stat"])
    sigmaPrRelSS_tzFit   = TGraphErrors(4,PTBins,CSRelPrDict_tzFit.d["val"],PTBinHW,CSRelPrDict_tzFit.d["SS"])


    sigmaPrRel_tzCut     = TGraphErrors(4,PTBins,CSRelPrDict_tzCut.d["val"],PTBinHW,CSRelPrDict_tzCut.d["tot"])
    sigmaPrRelStat_tzCut = TGraphErrors(4,PTBins,CSRelPrDict_tzCut.d["val"],PTBinHW,CSRelPrDict_tzCut.d["stat"])
    sigmaPrRelSS_tzCut   = TGraphErrors(4,PTBins,CSRelPrDict_tzCut.d["val"],PTBinHW,CSRelPrDict_tzCut.d["SS"])
    
    gPad.SetLeftMargin(0.25)
    #gPad.SetBottomMargin(0.125)

    
    sigmaPrRel_tzCut.Draw("AE2")
    sigmaPrRelSS_tzCut.Draw("E2 same")
    sigmaPrRelStat_tzCut.Draw("E2 same")

    sigmaPrRel_tzFit.Draw("SAME P")
    sigmaPrRelSS_tzFit.Draw("SAME P")
    sigmaPrRelStat_tzFit.Draw("SAME P")


    texCS.DrawLatex(0.3, 0.8, "LHCb preliminary")
    texCS.DrawLatex(0.3, 0.75, "#sqrt{s} = 13 TeV")
    texCS.DrawLatex(0.3, 0.7, "2.0 < y < 4.5")

    canv_2.cd()
    #gPad.SetBottomMargin(0.125)
    
    sigmaSecRel_tzFit     = TGraphErrors( 4,PTBins,CSRelSecDict_tzFit.d["val"],PTBinHW,CSRelSecDict_tzFit.d["tot"])
    sigmaSecRelStat_tzFit = TGraphErrors( 4,PTBins,CSRelSecDict_tzFit.d["val"],PTBinHW,CSRelSecDict_tzFit.d["stat"])
    sigmaSecRelSS_tzFit   = TGraphErrors( 4,PTBins,CSRelSecDict_tzFit.d["val"],PTBinHW,CSRelSecDict_tzFit.d["SS"])


    sigmaSecRel_tzCut     = TGraphErrors( 4,PTBins,CSRelSecDict_tzCut.d["val"],PTBinHW,CSRelSecDict_tzCut.d["tot"])
    sigmaSecRelStat_tzCut = TGraphErrors( 4,PTBins,CSRelSecDict_tzCut.d["val"],PTBinHW,CSRelSecDict_tzCut.d["stat"])
    sigmaSecRelSS_tzCut   = TGraphErrors( 4,PTBins,CSRelSecDict_tzCut.d["val"],PTBinHW,CSRelSecDict_tzCut.d["SS"])

    gPad.SetLeftMargin(0.25)

    sigmaSecRel_tzCut.Draw("AE2")
    sigmaSecRelStat_tzCut.Draw("E2 same")
    sigmaSecRelSS_tzCut.Draw("E2 same")

    sigmaSecRel_tzFit.Draw("SAME P")
    sigmaSecRelStat_tzFit.Draw("SAME P")
    sigmaSecRelSS_tzFit.Draw("SAME P")

    
    texCS.DrawLatex(0.3, 0.35, "LHCb preliminary")
    texCS.DrawLatex(0.3, 0.30, "#sqrt{s} = 13 TeV")
    texCS.DrawLatex(0.3, 0.25, "2.0 < y < 4.5")


    

    for graph in [sigmaPrRel_tzFit,sigmaPrRelStat_tzFit,sigmaPrRelSS_tzFit, \
              sigmaSecRel_tzFit,sigmaSecRelStat_tzFit,sigmaSecRelSS_tzFit]:
        graph.SetMinimum(0.0)
        graph.GetYaxis().SetTitleOffset(1.1)
        graph.SetLineColor(1)
        graph.SetMarkerStyle(8)
        graph.SetMarkerColor(1)
        graph.GetXaxis().SetLimits(rangeL,rangeR)
        graph.GetXaxis().SetTitle("p_{t}, GeV/c")

    for graph in [sigmaPrRel_tzCut,sigmaPrRelStat_tzCut,sigmaPrRelSS_tzCut, \
              sigmaSecRel_tzCut,sigmaSecRelStat_tzCut,sigmaSecRelSS_tzCut]:
        graph.SetMinimum(0.0)
        graph.GetYaxis().SetTitleOffset(1.1)
        # graph.SetLineColor(2)
        # graph.SetMarkerStyle(4)
        # graph.SetMarkerColor(2)
        graph.SetFillColor(2)
        graph.SetFillStyle(3002)
        graph.GetXaxis().SetLimits(rangeL,rangeR)
        graph.GetXaxis().SetTitle("p_{t}, GeV/c")

    for graph in [sigmaPrRel_tzFit,sigmaPrRelStat_tzFit,sigmaPrRelSS_tzFit,\
                  sigmaPrRel_tzCut,sigmaPrRelStat_tzCut,sigmaPrRelSS_tzCut]:
        graph.GetYaxis().SetTitle("#frac{d#sigma_{#eta_{c}}/dp_{T}}{d#sigma_{J/#psi}/dp_{T}}")

    for graph in [sigmaSecRel_tzCut,sigmaSecRelStat_tzCut,sigmaSecRelSS_tzCut,\
                  sigmaSecRel_tzFit,sigmaSecRelStat_tzFit,sigmaSecRelSS_tzFit]:
        graph.GetYaxis().SetTitle("#frac{d#sigma_{b#rightarrow#eta_{c}X}/dp_{T}}{d#sigma_{b#rightarrowJ/#psiX}/dp_{T}}")



    canv_1.SaveAs(homeDir+"Results/CS/CSRelPrompt_comp.pdf")
    canv_2.SaveAs(homeDir+"Results/CS/CSRelFromB_comp.pdf")


    




CSRelPrDict_tzFit,  CSRelSecDict_tzFit, \
CSEtacPrDict_tzFit, CSEtacSecDict_tzFit, \
CSJpsiPrDict_tzFit, CSJpsiSecDict_tzFit, \
NEtacPrDict_tzFit,  NEtacSecDict_tzFit = crosssection(CSfilename="Results/rel_PT_sim.txt")


CSRelPrDict_tzCut,  CSRelSecDict_tzCut, \
CSEtacPrDict_tzCut, CSEtacSecDict_tzCut, \
CSJpsiPrDict_tzCut, CSJpsiSecDict_tzCut, \
NEtacPrDict_tzCut,  NEtacSecDict_tzCut = crosssection(CSfilename="Results/rel_PT_RunIMethod.txt")


if __name__=='__main__':
    gROOT.LoadMacro("../lhcbStyle.C")
    makeCSPlots(True, "sim", \
                CSRelPrDict_tzFit,  CSRelSecDict_tzFit, \
                CSEtacPrDict_tzFit, CSEtacSecDict_tzFit, \
                CSJpsiPrDict_tzFit, CSJpsiSecDict_tzFit, \
                NEtacPrDict_tzFit, NEtacSecDict_tzFit)

    makeCSPlots(True, "RunIMethod", \
                CSRelPrDict_tzCut,  CSRelSecDict_tzCut, \
                CSEtacPrDict_tzCut, CSEtacSecDict_tzCut, \
                CSJpsiPrDict_tzCut, CSJpsiSecDict_tzCut, \
                NEtacPrDict_tzCut, NEtacSecDict_tzCut)

    makePlotsTzFit_vs_TzCut(True,CSRelPrDict_tzFit, CSRelSecDict_tzFit, \
                            CSRelPrDict_tzCut,CSRelSecDict_tzCut)

