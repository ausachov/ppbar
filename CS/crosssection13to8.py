from ROOT import *
from ROOT.TMath import *
from ROOT.TMath import Sq, Sqrt
from array import array

homeDir = "../"
nPTBins = 4

PTBins  = array("d", [7.25, 9.0, 11.0, 13.0, 16.0])
PTBinHW = array("d", [0.75, 1.0, 1.0, 1.0, 2.0])

nBinsJpsi = 8
PTBinsJpsi   = array("d",[6.5,7.5,8.5,9.5,10.5,11.5,12.5,13.5])
PTBinsJpsiHW = array("d",[0.5,0.5,0.5,0.5,0.5, 0.5, 0.5, 0.5])

rangeL, rangeR  = 0.0, 16.0

def crosssection13to8(empty = False):

    cSRelPr_13          = array("d", nPTBins*[0])
    cSRelSec_13         = array("d", nPTBins*[0])
    cSRelPr_13_Err      = array("d", nPTBins*[0])
    cSRelSec_13_Err     = array("d", nPTBins*[0])
    cSRelPr_13_ErrStat  = array("d", nPTBins*[0])
    cSRelSec_13_ErrStat = array("d", nPTBins*[0])
    cSRelPr_13_ErrSyst  = array("d", nPTBins*[0])
    cSRelSec_13_ErrSyst = array("d", nPTBins*[0])

    cSRelPr_8      = array("d", [1.43, 1.31, 1.78, 1.99])
    cSRelSec_8     = array("d", [0.419, 0.455, 0.417, 0.401])
    cSRelPr_8_Err  = array("d", [1.12, 0.45, 0.55, 0.67])  # without BR contribution
    cSRelSec_8_Err = array("d", [0.069, 0.049, 0.063, 0.070])   # without BR contribution
    cSRelPr_8_ErrStat  = array("d", [1.10, 0.40, 0.47, 0.59 ])
    cSRelSec_8_ErrStat = array("d", [0.066, 0.043, 0.059, 0.067])
    cSRelPr_8_ErrSyst  = array("d", [0.22, 0.20, 0.28, 0.31])
    cSRelSec_8_ErrSyst = array("d", [0.021, 0.023, 0.021, 0.020 ])


    JpsiPr_13to8    = array("d",[1.64,1.64,1.71,1.80,1.83,1.92,1.91,1.98])
    JpsiPrErr_13to8 = array("d",[0.10,0.10,0.11,0.12,0.13,0.14,0.15,0.17])

    JpsiSec_13to8    = array("d",[2.08,2.06,2.06,2.30,2.38,2.57,2.29,2.60])
    JpsiSecErr_13to8 = array("d",[0.13,0.14,0.14,0.16,0.18,0.21,0.20,0.25])

    
    cSJpsiPr_13          = array("d", nPTBins*[0])
    cSJpsiSec_13         = array("d", nPTBins*[0])
    cSJpsiPr_13_Err      = array("d", nPTBins*[0])
    cSJpsiSec_13_Err     = array("d", nPTBins*[0])
    cSJpsiPr_13_ErrStat  = array("d", nPTBins*[0])
    cSJpsiSec_13_ErrStat = array("d", nPTBins*[0])
    cSJpsiPr_13_ErrSyst  = array("d", nPTBins*[0])
    cSJpsiSec_13_ErrSyst = array("d", nPTBins*[0])




    fi = open(homeDir+"Results/CS/CSRel.txt","r")

    for iPT in range(nPTBins):        
        line = fi.readline()
        cSJpsiPr_13[iPT], cSJpsiPr_13_ErrStat[iPT], cSJpsiPr_13_ErrSyst[iPT]  = float(line.split()[0]), float(line.split()[1]), float(line.split()[2])
        cSJpsiPr_13_Err[iPT] = (Sq(cSJpsiPr_13_ErrStat[iPT]) + Sq(cSJpsiPr_13_ErrSyst[iPT]))**0.5
    
    line = fi.readline()

    for iPT in range(nPTBins):        
        line = fi.readline()
        cSJpsiSec_13[iPT], cSJpsiSec_13_ErrStat[iPT], cSJpsiSec_13_ErrSyst[iPT]  = float(line.split()[0]), float(line.split()[1]), float(line.split()[2])
        cSJpsiSec_13_Err[iPT] = (Sq(cSJpsiSec_13_ErrStat[iPT]) + Sq(cSJpsiSec_13_ErrSyst[iPT]))**0.5
    
    line = fi.readline()

    for iPT in range(nPTBins):        
        line = fi.readline()
        cSRelPr_13[iPT], cSRelPr_13_ErrStat[iPT], cSRelPr_13_ErrSyst[iPT] = float(line.split()[0]), float(line.split()[1]), float(line.split()[2])
        cSRelPr_13_Err[iPT] = (Sq(cSRelPr_13_ErrStat[iPT]) + Sq(cSRelPr_13_ErrSyst[iPT]))**0.5
    
    line = fi.readline()

    for iPT in range(nPTBins):        
        line = fi.readline()
        cSRelSec_13[iPT], cSRelSec_13_ErrStat[iPT], cSRelSec_13_ErrSyst[iPT] = float(line.split()[0]), float(line.split()[1]), float(line.split()[2])
        cSRelSec_13_Err[iPT] = (Sq(cSRelSec_13_ErrStat[iPT]) + Sq(cSRelSec_13_ErrSyst[iPT]))**0.5

    fi.close()


    cSRelPr          = array("d", nPTBins*[0])
    cSRelSec         = array("d", nPTBins*[0])
    cSRelPr_Err      = array("d", nPTBins*[0])
    cSRelSec_Err     = array("d", nPTBins*[0])
    cSRelPr_ErrStat  = array("d", nPTBins*[0])
    cSRelSec_ErrStat = array("d", nPTBins*[0])
    cSRelPr_ErrSyst  = array("d", nPTBins*[0])
    cSRelSec_ErrSyst = array("d", nPTBins*[0])
    cSRelPr_ErrTot   = array("d", nPTBins*[0])   #with Jpsi error
    cSRelSec_ErrTot  = array("d", nPTBins*[0])   #with Jpsi error

    cSRelJpsiPr          = array("d", nPTBins*[0])
    cSRelJpsiSec         = array("d", nPTBins*[0])
    cSRelJpsiPr_Err      = array("d", nPTBins*[0])
    cSRelJpsiSec_Err     = array("d", nPTBins*[0])
    cSRelJpsiPr_ErrStat  = array("d", nPTBins*[0])
    cSRelJpsiSec_ErrStat = array("d", nPTBins*[0])
    cSRelJpsiPr_ErrSyst  = array("d", nPTBins*[0])
    cSRelJpsiSec_ErrSyst = array("d", nPTBins*[0])

    #fitNRelPr = TF1("firNRelPr", "pol1(0)", 6.0, 15.0)
    #fitNRelSec = TF1("firNRelSec", "pol1(0)", 6.0, 15.0)

    #fitCSRelPr = TF1("fitCSJpsiPr", "pol1(0)", 6.0, 15.0)
    #fitCSRelPrRunI = TF1("fitCSJpsiPrRunI", "pol1(0)", 6.0, 15.0)

    fi = open(homeDir+"Results/CS/CSJpsi_8.txt","r")

    for iPT in range(nPTBins):        
        line = fi.readline()
        cSRelJpsiPr[iPT], cSRelJpsiPr_ErrStat[iPT], cSRelJpsiPr_ErrSyst[iPT]  = float(line.split()[0]), float(line.split()[1]), 0
        cSRelJpsiPr_Err[iPT] = (Sq(cSRelJpsiPr_ErrStat[iPT]) + Sq(cSRelJpsiPr_ErrSyst[iPT]))**0.5
    
    line = fi.readline()

    for iPT in range(nPTBins):        
        line = fi.readline()
        cSRelJpsiSec[iPT], cSRelJpsiSec_ErrStat[iPT], cSRelJpsiSec_ErrSyst[iPT]  = float(line.split()[0]), float(line.split()[1]), 0
        cSRelJpsiSec_Err[iPT] = (Sq(cSRelJpsiSec_ErrStat[iPT]) + Sq(cSRelJpsiSec_ErrSyst[iPT]))**0.5

    fi.close()



    for iPT in range(nPTBins):        
        
        cSRelPr[iPT] = cSRelPr_13[iPT]/cSRelPr_8[iPT] * cSRelJpsiPr[iPT]
        cSRelPr_ErrStat[iPT] = cSRelPr[iPT]*((cSRelPr_13_ErrStat[iPT]/cSRelPr_13[iPT])**2 +(cSRelPr_8_ErrStat[iPT]/cSRelPr_8[iPT])**2)**0.5
        cSRelPr_ErrSyst[iPT] = cSRelPr[iPT]*((cSRelPr_13_ErrSyst[iPT]/cSRelPr_13[iPT])**2 +(cSRelPr_8_ErrSyst[iPT]/cSRelPr_8[iPT])**2)**0.5
        cSRelPr_Err[iPT] = (cSRelPr_ErrStat[iPT]**2 + cSRelPr_ErrSyst[iPT]**2)**0.5
                
        cSRelPr_ErrTot[iPT] =  (cSRelPr_Err[iPT]**2 +  (cSRelPr[iPT]*cSRelJpsiPr_Err[iPT]/cSRelJpsiPr[iPT])**2)**0.5
        #cSRelPr_Err[iPT] = ((cSRelPr_13_Err[iPT]/cSRelPr_13[iPT])**2 +(cSRelPr_8_Err[iPT]/cSRelPr_8[iPT])**2)**0.5
        print (cSRelPr_13_Err[iPT]/cSRelPr_13[iPT]), (cSRelPr_8_Err[iPT]/cSRelPr_8[iPT]), (cSRelJpsiPr_Err[iPT]/cSRelJpsiPr[iPT])
        print ((cSRelPr_13_Err[iPT]/cSRelPr_13[iPT])**2 +(cSRelPr_8_Err[iPT]/cSRelPr_8[iPT])**2 + (cSRelJpsiPr_Err[iPT]/cSRelJpsiPr[iPT])**2)**0.5

        cSRelSec[iPT] = cSRelSec_13[iPT]/cSRelSec_8[iPT] * cSRelJpsiSec[iPT]
        cSRelSec_ErrStat[iPT] = cSRelSec[iPT]*((cSRelSec_13_ErrStat[iPT]/cSRelSec_13[iPT])**2 +(cSRelSec_8_ErrStat[iPT]/cSRelSec_8[iPT])**2)**0.5
        cSRelSec_ErrSyst[iPT] = cSRelSec[iPT]*((cSRelSec_13_ErrSyst[iPT]/cSRelSec_13[iPT])**2 +(cSRelSec_8_ErrSyst[iPT]/cSRelSec_8[iPT])**2)**0.5
        cSRelSec_Err[iPT] = (cSRelSec_ErrStat[iPT]**2 + cSRelSec_ErrSyst[iPT]**2)**0.5
                
        cSRelSec_ErrTot[iPT] =  (cSRelSec_Err[iPT]**2 +  (cSRelSec[iPT]*cSRelJpsiSec_Err[iPT]/cSRelJpsiSec[iPT])**2)**0.5
        #cSRelSec_Err[iPT] = ((cSRelSec_13_Err[iPT]/cSRelSec_13[iPT])**2 +(cSRelSec_8_Err[iPT]/cSRelSec_8[iPT])**2)**0.5


    if empty:
        gStyle.SetOptFit(0)
        gStyle.SetOptStat(0)
        gStyle.SetOptTitle(0)
    else:
        gStyle.SetOptFit(1)
        gStyle.SetOptStat(1)
        gStyle.SetOptTitle(0)        
    
    
    texCS = TLatex()
    texCS.SetNDC()
    
    canv_1 = TCanvas("canv_prRel","1",55,55,550,400)
    canv_2 = TCanvas("canv_secRel","2",55,55,550,400)
#    canv_1.Divide(1,2)

    canv_1.cd()

    sigmaPrRel = TGraphErrors(4,PTBins,cSRelPr,PTBinHW,cSRelPr_ErrTot)
    sigmaPrRelStat = TGraphErrors(4,PTBins,cSRelPr,PTBinHW,cSRelPr_ErrStat)
    sigmaPrRelSS = TGraphErrors(4,PTBins,cSRelPr,PTBinHW,cSRelPr_Err)
    sigmaPrJpsi = TGraphErrors(nBinsJpsi,PTBinsJpsi,JpsiPr_13to8,PTBinsJpsiHW,JpsiPrErr_13to8)

    
    #canv_1.SetLogy()
    gPad.SetLeftMargin(0.25)
    #gPad.SetBottomMargin(0.125)

    #sigmaPrRelSS.Fit(fitCSRelPr,"I")
    
    sigmaPrRel.Draw("AP")
    sigmaPrRelSS.Draw("SAME P")
    sigmaPrRelStat.Draw("SAME P")
    sigmaPrJpsi.Draw("SAME P")
    sigmaPrJpsi.SetLineColor(4)
    sigmaPrJpsi.SetMarkerColor(4)
    sigmaPrRel.SetMarkerStyle(4)
    #sigmaPrRel.SetMarkerColor(2)
    sigmaPrRel.SetMinimum(0.0)
    #sigmaPrRel.SetMaximum(1.e3)
    sigmaPrRel.GetXaxis().SetTitle("p_{t}, GeV/c")
    sigmaPrRel.GetYaxis().SetTitle("R_{13/8}(d#sigma_{#eta_{c}}/dp_{T})")
    sigmaPrRel.GetXaxis().SetLimits(rangeL,rangeR)
    sigmaPrRel.GetYaxis().SetTitleOffset(1.1)
    #sigmaPrRel.GetYaxis().SetLimits(1, 500)
    
   
    texCS.DrawLatex(0.3, 0.85, "LHCb preliminary")
    texCS.DrawLatex(0.3, 0.80, "#sqrt{s} = 13 TeV")
    texCS.DrawLatex(0.3, 0.75, "2.0 < y < 4.5")

    legPr = TLegend(0.3, 0.25, 0.5, 0.4)
    legPr.AddEntry(sigmaPrJpsi,"J/#psi")
    legPr.AddEntry(sigmaPrRelSS,"#eta_{c}")
    legPr.Draw()


    canv_2.cd()
    #gPad.SetBottomMargin(0.125)
    
    sigmaSecRel = TGraphErrors( 4,PTBins,cSRelSec,PTBinHW,cSRelSec_ErrTot)
    sigmaSecRelStat = TGraphErrors( 4,PTBins,cSRelSec,PTBinHW,cSRelSec_ErrStat)
    sigmaSecRelSS = TGraphErrors( 4,PTBins,cSRelSec,PTBinHW,cSRelSec_Err)
    sigmaSecJpsi = TGraphErrors(nBinsJpsi,PTBinsJpsi,JpsiSec_13to8,PTBinsJpsiHW,JpsiSecErr_13to8)


    #canv_2.SetLogy()
    gPad.SetLeftMargin(0.25)

    sigmaSecRel.Draw("AP")
    sigmaSecRelStat.Draw("SAME P")
    sigmaSecRelSS.Draw("SAME P")
    sigmaSecJpsi.Draw("SAME P")
    sigmaSecJpsi.SetLineColor(4)
    sigmaSecJpsi.SetMarkerColor(4)
    sigmaSecRel.SetMarkerStyle(4)
    #sigmaSecRel.SetMarkerColor(2)
    sigmaSecRel.SetMinimum(0.0)
    #sigmaSecRel.SetMaximum(2.5e2)
    sigmaSecRel.GetXaxis().SetLimits(rangeL,rangeR)
    #sigmaSecRelStat.GetXaxis().SetLimits(rangeL,rangeR)
    #sigmaSecRel.GetYaxis().SetLimits(0.3, 100)
    sigmaSecRel.GetXaxis().SetTitle("p_{t}, GeV/c")
    sigmaSecRel.GetYaxis().SetTitle("R_{13/8}(d#sigma_{#eta_{c}}/dp_{T})")
    sigmaSecRel.GetYaxis().SetTitleOffset(1.1)
    
    texCS.DrawLatex(0.3, 0.85, "LHCb preliminary")
    texCS.DrawLatex(0.3, 0.80, "#sqrt{s} = 13 TeV")
    texCS.DrawLatex(0.3, 0.75, "2.0 < y < 4.5")

    legSec = TLegend(0.3, 0.25, 0.5, 0.4)
    legSec.AddEntry(sigmaSecJpsi,"J/#psi")
    legSec.AddEntry(sigmaSecRelSS,"#eta_{c}")
    legSec.Draw()

    canv_1.SaveAs(homeDir+"Results/CS/CSRelPrompt_13vs8.pdf")
    canv_2.SaveAs(homeDir+"Results/CS/CSRelFromB_13vs8.pdf")
    

gROOT.LoadMacro("../lhcbStyle.C")
crosssection13to8(True)