from ROOT import *
from ROOT.TMath import *
from ROOT.TMath import Sq, Sqrt
from array import array

homeDir = "../"
nPTBins = 4

#calculation of J/psi CS in bin of PT (6.5-8 GeV)
def extrapol(sqrtS=13):

    gStyle.SetOptFit(0)
    gStyle.SetOptStat(0)

    PTBins1 = array("d",[4.5, 5.5, 6.5, 7.5, 8.5, 9.5, 10.5, 11.5, 12.5, 13.5])
    PTBins2 = array("d",[4.5, 5.5, 6.5, 7.5, 8.5, 9.5, 10.5, 11.5, 12.5, 13.5])

    if sqrtS==13:
        cSJpsiPr = array("d",[ 1273, 703.7, 376.8, 299.7, 113.8, 66.5, 39.9, 25.1, 15.4, 10.1])
        cSJpsiSec = array("d",[ 242.7, 146.3, 94.6, 57.7, 35.8, 25.5, 17.0, 12.1, 8.1, 5.9])
        cSJpsiPrErrSystUncorr = array("d",[ 9, 6, 3.7, 2.4, 1.6, 1.2, 0.9, 0.7, 0.5, 0.4])
        cSJpsiSecErrSystUncorr  = array("d",[  1.9, 1.3, 0.9, 0.7, 0.5, 0.5, 0.4, 0.3, 0.3, 0.2])
        cSJpsiPrErrSystCorr = array("d",[ 72, 38.9, 20.5, 10.8, 6.1, 3.6, 2.1, 1.3, 0.8, 0.5])
        cSJpsiSecErrSystCorr = array("d",[  13.8, 8.1, 5.1, 3.1, 1.9, 1.4, 0.9, 0.6, 0.4, 0.3])
        cSJpsiPrErrStat = array("d",[ 5, 3.8, 2.6, 1.7, 1.2, 0.9, 0.7, 0.6, 0.4, 0.3 ])
        cSJpsiSecErrStat = array("d",[ 2.5, 1.8, 1.3, 1.0, 0.7, 0.6, 0.5, 0.4, 0.3, 0.3])
    if sqrtS==8:
        cSJpsiPr = array("d",[ 1273, 703.7, 376.8, 299.7, 113.8, 66.5, 39.9, 25.1, 15.4, 10.1])
        cSJpsiSec = array("d",[ 242.7, 146.3, 94.6, 57.7, 35.8, 25.5, 17.0, 12.1, 8.1, 5.9])
        cSJpsiPrErrSystUncorr = array("d",[ 9, 6, 3.7, 2.4, 1.6, 1.2, 0.9, 0.7, 0.5, 0.4])
        cSJpsiSecErrSystUncorr  = array("d",[  1.9, 1.3, 0.9, 0.7, 0.5, 0.5, 0.4, 0.3, 0.3, 0.2])
        cSJpsiPrErrSystCorr = array("d",[ 72, 38.9, 20.5, 10.8, 6.1, 3.6, 2.1, 1.3, 0.8, 0.5])
        cSJpsiSecErrSystCorr = array("d",[  13.8, 8.1, 5.1, 3.1, 1.9, 1.4, 0.9, 0.6, 0.4, 0.3])
        cSJpsiPrErrStat = array("d",[ 5, 3.8, 2.6, 1.7, 1.2, 0.9, 0.7, 0.6, 0.4, 0.3 ])
        cSJpsiSecErrStat = array("d",[ 2.5, 1.8, 1.3, 1.0, 0.7, 0.6, 0.5, 0.4, 0.3, 0.3])

    hJpsiPr = TH1D("hJpsiPr", "hJpsiPr", 6, 4.0, 10.0)
    hJpsiSec = TH1D("hJpsiSec", "hJpsiSec", 6, 4.0, 10.0)

    binValPr = 0.
    binValSec = 0.
    binValPrErrStat = 0.
    binValPrErrSystUncorr = 0.
    binValPrErrSystCorr = 0.
    binValSecErrStat = 0.
    binValSecErrSystUncorr = 0.
    binValSecErrSystCorr = 0.

    yD1 = []
    yD2 = []

    for iB in range(6):

      hJpsiPr.SetBinContent(iB+1,cSJpsiPr[iB])
      hJpsiPr.SetBinError(iB+1, Sqrt(cSJpsiPrErrSystCorr[iB]*cSJpsiPrErrSystCorr[iB] + cSJpsiPrErrSystUncorr[iB]*cSJpsiPrErrSystUncorr[iB] + cSJpsiPrErrStat[iB]*cSJpsiPrErrStat[iB]))
      hJpsiSec.SetBinContent(iB+1,cSJpsiSec[iB])
      hJpsiSec.SetBinError(iB+1,Sqrt(cSJpsiSecErrSystCorr[iB]*cSJpsiSecErrSystCorr[iB] + cSJpsiSecErrSystUncorr[iB]*cSJpsiSecErrSystUncorr[iB] + cSJpsiSecErrStat[iB]*cSJpsiSecErrStat[iB]))

      yD1.append(cSJpsiPr[iB])
      yD2.append(cSJpsiSec[iB])

    x1 = array("f",30*[0])
    y1 = array("f",30*[0])
    x2 = array("f",30*[0])
    y2 = array("f",30*[0])



    inter1 = Math.Interpolator(6, Math.Interpolation.kPOLYNOMIAL)
    inter2 = Math.Interpolator(6, Math.Interpolation.kPOLYNOMIAL)
    inter1.SetData(6, PTBins1, cSJpsiPr)
    inter2.SetData(6, PTBins2, cSJpsiSec)

    nInt = 30
    for i in range(nInt):

      x1[i] = ( i*(9.5-4.5)/(nInt-1.) + 4.5)
      y1[i] = (inter1.Eval(x1[i]))
      #print(y1[i] + "\n")
      x2[i] = (i*(9.5-4.5)/(nInt-1.) + 4.5)
      y2[i] = (inter2.Eval(x2[i]))


    fitFunc1 = TF1("fitFunc1", "expo(0)", 4.0, 10.0)
    fitFunc2 = TF1("fitFunc2", "expo(0)", 4.0, 10.0)

    rPr = hJpsiPr.Fit(fitFunc1,"S")
    binValPr = fitFunc1.Integral(6.5, 7.0)
    binValPrErrStat = (binValPr/fitFunc1.Integral(6.0, 7.0))*cSJpsiPrErrStat[2]
    binValPrErrSystUncorr = (binValPr/fitFunc1.Integral(6.0, 7.0))*cSJpsiPrErrSystUncorr[2]
    binValPrErrSystCorr = (binValPr/fitFunc1.Integral(6.0, 7.0))*cSJpsiPrErrSystCorr[2]
    #   binValPrErr = fitFunc1.IntegralError(6.5,7.0, rPr.GetParams(), rPr.GetCovarianceMatrix().GetMatrixArray())


    rSec = hJpsiSec.Fit(fitFunc2,"S")
    binValSec = fitFunc2.Integral(6.5, 7.0)
    binValSecErrStat = (binValSec/fitFunc2.Integral(6.0, 7.0))*cSJpsiSecErrStat[2]
    binValSecErrSystUncorr = (binValSec/fitFunc2.Integral(6.0, 7.0))*cSJpsiSecErrSystUncorr[2]
    binValSecErrSystCorr = (binValSec/fitFunc2.Integral(6.0, 7.0))*cSJpsiSecErrSystCorr[2]
    #    binValSecErr = fitFunc2.IntegralError(6.5,7.0, rSec.GetParams(), rSec.GetCovarianceMatrix().GetMatrixArray())

    canv = TCanvas("canv","J/Psi cross-section",55,55,600,800)
    canv.Divide(1,2)

    canv.cd(1)
    hJpsiPr.Draw()
    hJpsiPr.SetTitle("J/#psi prompt cross-section")
    hJpsiPr.GetXaxis().SetTitle("p_{T} [GeV]")
    hJpsiPr.GetYaxis().SetTitle("#sigma [nb/ GeV/c]")
    fitFunc1.Draw("SAME")

    gi1 = TGraph(30, x1, y1)
    gi1.SetLineColor(kBlue)
    gi1.SetLineWidth(2)
    gi1.SetTitle("Integral")
    gi1.Draw("SAME L")

    leg = TLegend(0.55, 0.7, 0.85, 0.85)
    leg.AddEntry(hJpsiPr,"J/#psi cross-section","lep")
    leg.AddEntry(fitFunc1,"e^{ax+b} fit","l")
    leg.AddEntry(gi1,"pol3 interpolation","l")
    leg.SetBorderSize(0)
    leg.Draw()

    canv.cd(2)
    hJpsiSec.Draw()
    hJpsiSec.SetTitle("J/#psi from-b cross-section")
    hJpsiSec.GetXaxis().SetTitle("p_{T}, GeV/c")
    hJpsiSec.GetYaxis().SetTitle("#sigma [nb/ GeV/c]")
    fitFunc2.Draw("SAME")

    gi2 = TGraph(30, x2, y2)
    gi2.SetLineColor(kBlue)
    gi2.SetLineWidth(2)
    gi2.SetTitle("Integral")
    gi2.Draw("SAME L")


    leg.Draw()


    canv.SaveAs(homeDir+"Results/CS/CSfit.pdf")

    if sqrtS==13:
        fo = open(homeDir+"Results/CS/CSJpsi13_new.txt","w")
    if sqrtS==8:
        fo = open(homeDir+"Results/CS/CSJpsi8_new.txt","w")

    fo.write( str(binValPr+cSJpsiPr[3]) + " ")
    fo.write( str(Sqrt(Sq(binValPrErrStat)+Sq(cSJpsiPrErrStat[3]))) + "  ")
    fo.write( str(Sqrt(Sq(binValPrErrSystUncorr)+Sq(cSJpsiPrErrSystUncorr[3]))) + "  ")
    fo.write( str(Sqrt(Sq(binValPrErrSystCorr)+Sq(cSJpsiPrErrSystCorr[3]))) + "  ")
    fo.write( str(Sqrt(Sq(binValPrErrStat)+Sq(cSJpsiPrErrStat[3]) + Sq(binValPrErrSystUncorr)+Sq(cSJpsiPrErrSystUncorr[3]) + Sq(binValPrErrSystCorr)+Sq(cSJpsiPrErrSystCorr[3]))) + "\n")
    for i in range(2, 5):
        fo.write( str(cSJpsiPr[2*i]+cSJpsiPr[2*i+1]) + "  ")
        fo.write( str(Sqrt(Sq(cSJpsiPrErrStat[2*i])+Sq(cSJpsiPrErrStat[2*i+1]))) + "  ")
        fo.write( str(Sqrt(Sq(cSJpsiPrErrSystUncorr[2*i])+Sq(cSJpsiPrErrSystUncorr[2*i+1]))) + "  ")
        fo.write( str(Sqrt(Sq(cSJpsiPrErrSystCorr[2*i])+Sq(cSJpsiPrErrSystCorr[2*i+1]))) + "  ")
        fo.write( str(Sqrt(Sq(cSJpsiPrErrStat[2*i])+Sq(cSJpsiPrErrStat[2*i+1])+Sq(cSJpsiPrErrSystUncorr[2*i])+Sq(cSJpsiPrErrSystUncorr[2*i+1]) + +Sq(cSJpsiPrErrSystCorr[2*i])+Sq(cSJpsiPrErrSystCorr[2*i+1]))) +  "\n ")

    fo.write("\n")
    
    fo.write( str(binValSec+cSJpsiSec[3]) + " ")
    fo.write( str(Sqrt(Sq(binValSecErrStat)+Sq(cSJpsiSecErrStat[3]))) + "  ")
    fo.write( str(Sqrt(Sq(binValSecErrSystUncorr)+Sq(cSJpsiSecErrSystUncorr[3]))) + "  ")
    fo.write( str(Sqrt(Sq(binValSecErrSystCorr)+Sq(cSJpsiSecErrSystCorr[3]))) + "  ")
    fo.write( str(Sqrt(Sq(binValSecErrStat)+Sq(cSJpsiSecErrStat[3]) + Sq(binValSecErrSystUncorr)+Sq(cSJpsiSecErrSystUncorr[3]) + Sq(binValSecErrSystCorr)+Sq(cSJpsiSecErrSystCorr[3]))) +  "\n")
    for i in range(2, 5):
        fo.write( str(cSJpsiSec[2*i]+cSJpsiSec[2*i+1]) + "  ")
        fo.write( str(Sqrt(Sq(cSJpsiSecErrStat[2*i])+Sq(cSJpsiSecErrStat[2*i+1]))) + "  ")
        fo.write( str(Sqrt(Sq(cSJpsiSecErrSystUncorr[2*i])+Sq(cSJpsiSecErrSystUncorr[2*i+1]))) + "  ")
        fo.write( str(Sqrt(Sq(cSJpsiSecErrSystCorr[2*i])+Sq(cSJpsiSecErrSystCorr[2*i+1]))) + "  ")
        fo.write( str(Sqrt(Sq(cSJpsiSecErrStat[2*i])+Sq(cSJpsiSecErrStat[2*i+1])+Sq(cSJpsiSecErrSystUncorr[2*i])+Sq(cSJpsiSecErrSystUncorr[2*i+1]) +Sq(cSJpsiSecErrSystCorr[2*i])+Sq(cSJpsiSecErrSystCorr[2*i+1]))) + "\n ")

    fo.write("\n")
    fo.close()





def extrapolRel():

    gStyle.SetOptFit(0)
    gStyle.SetOptStat(0)

    PTBins1 = array("d",[0.5, 1.5, 2.5, 3.5, 4.5, 5.5, 6.5, 7.5, 8.5, 9.5, 10.5, 11.5, 12.5, 13.5])
    PTBins2 = array("d",[0.5, 1.5, 2.5, 3.5, 4.5, 5.5, 6.5, 7.5, 8.5, 9.5, 10.5, 11.5, 12.5, 13.5])

    cSJpsiPr = array("d", 14*[0])
    cSJpsiSec = array("d", 14*[0])
    cSJpsiPrErr = array("d", 14*[0])
    cSJpsiSecErr = array("d", 14*[0])

    fi = open(homeDir+"Results/CSJpsi_Run1.txt","r")

    for iPT in range(14):
        
        l = fi.readline()
        cSJpsiPr[iPT], cSJpsiPrErr[iPT] =  float(l.split()[0]), float(l.split()[1])

    l = fi.readline()
    
    for iPT in range(14):
        
        l = fi.readline()
        cSJpsiSec[iPT], cSJpsiSecErr[iPT] =  float(l.split()[0]), float(l.split()[1])

    fi.close()

    hJpsiPr = TH1D("hJpsiPr", "hJpsiPr", 14, 0.0, 14.0)
    hJpsiSec = TH1D("hJpsiSec", "hJpsiSec", 14, 0.0, 14.0)

    binValPr = 0.
    binValSec = 0.
    binValPrErrStat = 0.
    binValPrErrSystUncorr = 0.
    binValPrErrSystCorr = 0.
    binValSecErrStat = 0.
    binValSecErrSystUncorr = 0.
    binValSecErrSystCorr = 0.

    yD1 = []
    yD2 = []

    for iB in range(14):

      hJpsiPr.SetBinContent(iB+1,cSJpsiPr[iB])
      hJpsiPr.SetBinError(iB+1, cSJpsiPrErr[iB])
      hJpsiSec.SetBinContent(iB+1,cSJpsiSec[iB])
      hJpsiSec.SetBinError(iB+1, cSJpsiSecErr[iB])

      yD1.append(cSJpsiPr[iB])
      yD2.append(cSJpsiSec[iB])

    x1 = array("f",30*[0])
    y1 = array("f",30*[0])
    x2 = array("f",30*[0])
    y2 = array("f",30*[0])



    inter1 = Math.Interpolator(14, Math.Interpolation.kPOLYNOMIAL)
    inter2 = Math.Interpolator(14, Math.Interpolation.kPOLYNOMIAL)
    inter1.SetData(14, PTBins1, cSJpsiPr)
    inter2.SetData(14, PTBins2, cSJpsiSec)

    nInt = 10
    for i in range(nInt):

      x1[i] = ( i*(13.5-0.5)/(nInt-1.) + 0.5)
      y1[i] = (inter1.Eval(x1[i]))
      #print(y1[i] + "\n")
      x2[i] = (i*(13.5-0.5)/(nInt-1.) + 0.5)
      y2[i] = (inter2.Eval(x2[i]))


    fitFunc1 = TF1("fitFunc1", "expo(0)", 0.0, 14.0)
    fitFunc2 = TF1("fitFunc2", "expo(0)", 0.0, 14.0)

    rPr = hJpsiPr.Fit(fitFunc1,"S")
    binValPr = fitFunc1.Integral(6.5, 7.0)
    binValPrErr = (binValPr/fitFunc1.Integral(6.0, 7.0))*cSJpsiPrErr[6]
    #binValPrErrStat = (binValPr/fitFunc1.Integral(6.0, 7.0))*cSJpsiPrErrStat[2]
    #binValPrErrSystUncorr = (binValPr/fitFunc1.Integral(6.0, 7.0))*cSJpsiPrErrSystUncorr[2]
    #binValPrErrSystCorr = (binValPr/fitFunc1.Integral(6.0, 7.0))*cSJpsiPrErrSystCorr[2]
    #   binValPrErr = fitFunc1.IntegralError(6.5,7.0, rPr.GetParams(), rPr.GetCovarianceMatrix().GetMatrixArray())


    rSec = hJpsiSec.Fit(fitFunc2,"S")
    binValSec = fitFunc2.Integral(6.5, 7.0)
    binValSecErr = (binValSec/fitFunc2.Integral(6.0, 7.0))*cSJpsiSecErr[6]
    #binValSecErrStat = (binValSec/fitFunc2.Integral(6.0, 7.0))*cSJpsiSecErrStat[2]
    #binValSecErrSystUncorr = (binValSec/fitFunc2.Integral(6.0, 7.0))*cSJpsiSecErrSystUncorr[2]
    #binValSecErrSystCorr = (binValSec/fitFunc2.Integral(6.0, 7.0))*cSJpsiSecErrSystCorr[2]
    #    binValSecErr = fitFunc2.IntegralError(6.5,7.0, rSec.GetParams(), rSec.GetCovarianceMatrix().GetMatrixArray())

    canv = TCanvas("canv","J/Psi cross-section",55,55,600,800)
    canv.Divide(1,2)

    canv.cd(1)
    hJpsiPr.Draw()
    hJpsiPr.SetTitle("J/#psi prompt cross-section")
    hJpsiPr.GetXaxis().SetTitle("p_{T} [GeV]")
    hJpsiPr.GetYaxis().SetTitle("#sigma [nb/ GeV/c]")
    fitFunc1.Draw("SAME")

    gi1 = TGraph(nInt, x1, y1)
    gi1.SetLineColor(kBlue)
    gi1.SetLineWidth(2)
    gi1.SetTitle("Integral")
    gi1.Draw("SAME L")

    leg = TLegend(0.55, 0.7, 0.85, 0.85)
    leg.AddEntry(hJpsiPr,"J/#psi cross-section","lep")
    leg.AddEntry(fitFunc1,"e^{ax+b} fit","l")
    leg.AddEntry(gi1,"pol3 interpolation","l")
    leg.SetBorderSize(0)
    leg.Draw()

    canv.cd(2)
    hJpsiSec.Draw()
    hJpsiSec.SetTitle("J/#psi from-b cross-section")
    hJpsiSec.GetXaxis().SetTitle("p_{T}, GeV/c")
    hJpsiSec.GetYaxis().SetTitle("#sigma [nb/ GeV/c]")
    fitFunc2.Draw("SAME")

    gi2 = TGraph(nInt, x2, y2)
    gi2.SetLineColor(kBlue)
    gi2.SetLineWidth(2)
    gi2.SetTitle("Integral")
    gi2.Draw("SAME L")


    leg.Draw()


    canv.SaveAs(homeDir+"Results/CS/CSJpsiRelFit.pdf")

    fo = open(homeDir+"Results/CS/CSJpsi_8.txt","w")

    ptBins = {7.25, 9.0, 11.0, 13.0}
    for i in range(3, 7):
        
        fo.write( "%4.3f  "%((cSJpsiPr[2*i]/cSJpsiPrErr[2*i]**2 + cSJpsiPr[2*i+1]/cSJpsiPrErr[2*i+1]**2)/(1/cSJpsiPrErr[2*i]**2+1/cSJpsiPrErr[2*i+1]**2)))
        fo.write( "%4.3f  \n"%(1/(1/cSJpsiPrErr[2*i]**2+1/cSJpsiPrErr[2*i+1]**2)**0.5))

    fo.write("\n")
    
    for i in range(3, 7):

        fo.write( "%4.3f  "%((cSJpsiSec[2*i]/cSJpsiSecErr[2*i]**2 + cSJpsiSec[2*i+1]/cSJpsiSecErr[2*i+1]**2)/(1/cSJpsiSecErr[2*i]**2+1/cSJpsiSecErr[2*i+1]**2)))
        fo.write( "%4.3f  \n"%(1/(1/cSJpsiSecErr[2*i]**2+1/cSJpsiSecErr[2*i+1]**2)**0.5))

    fo.write("\n")

    fo.close()