from ROOT import *
from ROOT.TMath import Sq, Sqrt
from array import array

energyJpsi    = array("d", [2.76, 7., 8., 13.])
energyJpsiErr = array("d", 4*[0])
energy        = array("d", [7., 8., 13.])
energyErr     = array("d", 3*[0])

homeDir = "../"


def enCS():

    CSJpsi          = array("d", [0.0393, 0.2839, 0.3714, 0.749])
    CSJpsiErrStat   = array("d", [0.0039, 0.0017, 0.0014, 0.0238])
    CSJpsiErrSyst   = array("d", [0.0049, 0.0153, 0.0272, 0.0188])
    CSJpsiErr       = array("d", 4*[0])


    CSEtacRel          = array("d", [1.74, 1.60, 1.88])
    CSEtacRelErrStat   = array("d", [0.29, 0.29, 0.16])
    CSEtacRelErrSyst   = array("d", [0.28, 0.25, 0.14])
    CSEtacRelErrBR     = array("d", [0.18, 0.17, 0.21])
    CSEtacRelErr       = array("d", 3*[0])
    CSEtacRelErrTot    = array("d", 3*[0])

    CSEtac             = array("d", [0.52, 0.59, 1.41])
    CSEtacErrStat      = array("d", [0.09, 0.11, 0.12])
    CSEtacErrSyst      = array("d", [0.08, 0.09, 0.10])
    CSEtacErrBR        = array("d", [0.06, 0.08, 0.16])
    CSEtacErr          = array("d", 3*[0])
    CSEtacErrTot       = array("d", 3*[0])



    for i in range(3):

        CSEtacErr[i]    = ((CSEtacErrStat[i])**2 + (CSEtacErrSyst[i])**2)**0.5
        CSEtacRelErr[i] = ((CSEtacRelErrStat[i])**2 + (CSEtacRelErrSyst[i])**2)**0.5

        CSJpsiErr[i]       = ((CSJpsiErrStat[i])**2 + (CSJpsiErrSyst[i])**2)**0.5

        CSEtacErrTot[i]    = ((CSEtacErr[i])**2    + (CSEtacErrBR[i])**2)**0.5
        CSEtacRelErrTot[i] = ((CSEtacRelErr[i])**2 +(CSEtacRelErrBR[i])**2)**0.5

    sigma          = TGraphErrors(3,energy,CSEtac,energyErr,CSEtacErr)
    sigmaStat      = TGraphErrors(3,energy,CSEtac,energyErr,CSEtacErrStat)
    sigmaErrTot    = TGraphErrors(3,energy,CSEtac,energyErr,CSEtacErrTot)

    sigmaJpsiStat  = TGraphErrors(4,energyJpsi,CSJpsi,energyJpsiErr,CSJpsiErrStat)
    sigmaJpsi      = TGraphErrors(4,energyJpsi,CSJpsi,energyJpsiErr,CSJpsiErr)
    

    sigmaRel       = TGraphErrors(3,energy,CSEtacRel,energyErr,CSEtacRelErr)
    sigmaRelStat   = TGraphErrors(3,energy,CSEtacRel,energyErr,CSEtacRelErrStat)
    sigmaRelErrTot = TGraphErrors(3,energy,CSEtacRel,energyErr,CSEtacRelErrTot)


    texCS = TLatex()
    texCS.SetNDC()
    
    leg1 = TLegend(0.30,0.53,0.4,0.67)
    leg1.SetBorderSize(0)
    leg1.AddEntry(sigma,"#eta_{c}","lep")
    leg1.AddEntry(sigmaJpsi,"J/#psi","lep")

    canv = TCanvas("canv","CS",55,55,500,400)
    gPad.SetLeftMargin(0.2)
    
    sigma.Draw("AP")
    sigmaStat.Draw("SAME P")
    sigmaErrTot.Draw("SAME P")
    sigmaJpsi.Draw("SAME P")
    sigmaJpsiStat.Draw("SAME P")
    #sigma.SetMarkerStyle(2)
    sigma.GetYaxis().SetRangeUser(0.0, 1.8)
    sigma.GetXaxis().SetLimits(0., 14)
    #sigma.GetYaxis().SetLimits(0.0, 2.0)
    sigma.GetXaxis().SetTitle("#sqrt{s}, TeV")
    sigma.GetYaxis().SetTitle("#sigma_{#eta_{c}}, #mub")
    sigmaJpsi.SetMarkerStyle(4)
    sigmaJpsi.SetMarkerColor(4)
    sigmaJpsi.SetMarkerSize(1)
    sigmaJpsi.SetLineColor(4)
    sigmaJpsiStat.SetMarkerStyle(4)
    sigmaJpsiStat.SetMarkerSize(1)
    sigmaJpsiStat.SetMarkerColor(4)
    sigmaJpsiStat.SetLineColor(4)
    leg1.Draw()

    texCS.DrawLatex(0.3, 0.81,"LHCb preliminary")
    texCS.DrawLatex(0.3, 0.75,"6.5 < p_{T} < 14 GeV/c")
    texCS.DrawLatex(0.3, 0.69,"2.0 < y < 4.5")
    
    canv.SaveAs(homeDir+"Results/CS/CSEnergyDist.pdf")

    canvRel = TCanvas("canvRel","CS",55,55,500,400)
    gPad.SetLeftMargin(0.2)
    
    sigmaRel.Draw("AP")
    sigmaRelStat.Draw("SAME P")
    sigmaRelErrTot.Draw("SAME P")
    sigmaRel.SetMarkerStyle(2)
    sigmaRel.GetYaxis().SetRangeUser(0.0, 2.5)
    sigmaRel.GetXaxis().SetLimits(0., 14)
    #sigma.GetYaxis().SetLimits(0.0, 2.0)
    sigmaRel.GetXaxis().SetTitle("#sqrt{s}, TeV")
    sigmaRel.GetYaxis().SetTitle("#sigma_{#eta_{c}}/#sigma_{J/#psi}")
    texCS.DrawLatex(0.3, 0.41,"LHCb preliminary")
    texCS.DrawLatex(0.3, 0.35,"6.5 < p_{T} < 14 GeV/c")
    texCS.DrawLatex(0.3, 0.29,"2.0 < y < 4.5")
    
    canvRel.SaveAs(homeDir+"Results/CS/CSEnergyRelDist.pdf")

gROOT.LoadMacro("../lhcbStyle.C")
enCS()