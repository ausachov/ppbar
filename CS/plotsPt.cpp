/*
 * plotsPT.cpp
 *
 *  Created on: Oct 17, 2013
 *      Author: maksym
 */
using namespace RooFit;

void plotsPt(){

    gROOT->ProcessLine(".x ../lhcbStyle.C");
// pT prompt:

	Float_t sft = 0.05;

	double x_pmtEtac2011[3]    = {8.225+sft, 11+sft, 13+sft};
	double xErr_pmtEtac2011[3]    = {1.75, 1, 1};
	double x_pmtEtac2012[4]    = {7.25+sft, 9+sft, 11+sft, 13+sft};
	double xErr_pmtEtac2012[4] = {0.75, 1, 1, 1};

	double x_pmtJpsi2011[3]    = {8.225-sft, 11-sft, 13-sft};
	double xErr_pmtJpsi2011[3]    = {1.75, 1, 1};
	double x_pmtJpsi2012[4]    = {7.25-sft, 9-sft, 11-sft, 13-sft};
	double xErr_pmtJpsi2012[4] = {0.75, 1, 1, 1};

	double y_pmtEtac2011[3]    = {129.3, 17.2, 8.8};
	double yErr1_pmtEtac2011[3]    = {36.2, 9.4, 4.9};
	double yErr2_pmtEtac2011[3]    = {36.9, 9.4, 5.0};
	double y_pmtEtac2012[4]    = {209.6, 67.9, 31.2, 13.1};
	double yErr1_pmtEtac2012[4] = {165.54, 24.26, 10.09, 4.65};
	double yErr2_pmtEtac2012[4] = {166.3, 24.7, 10.3, 4.7};

	double y_pmtJpsi2011[3]    = {71.4, 12.7, 4.5};
	double yErr_pmtJpsi2011[3]    = {4.3, 0.9, 0.4};
	double y_pmtJpsi2012[4]    = {146.3, 51.9, 17.5, 6.6};
	double yErr_pmtJpsi2012[4] = {11.3, 3.7, 1.3, 0.5};

// pT secondary:

	double x_sndEtac[4]    = {7.25+sft, 9+sft, 11+sft, 13+sft};
	double xErr_sndEtac[4] = {0.75, 1, 1, 1};
	double x_sndJpsi[4]    = {7.25-sft, 9-sft, 11-sft, 13-sft};
	double xErr_sndJpsi[4] = {0.75, 1, 1, 1};

	double y_sndEtac2011[4]    = {8.70, 3.08, 1.91, 0.57};
	double yErr1_sndEtac2011[4]    = {2.31, 0.95, 0.40, 0.18};
	double yErr2_sndEtac2011[4]    = {2.62, 1.04, 0.46, 0.19};
	double y_sndEtac2012[4]    = {10.69, 4.99, 1.80, 0.78};
	double yErr1_sndEtac2012[4] = {2.08, 0.76, 0.33, 0.16};
	double yErr2_sndEtac2012[4] = {2.24, 0.84, 0.36, 0.17};

	double y_sndJpsi2011[4]    = {21.42, 9.12, 3.49, 1.34};
	double yErr_sndJpsi2011[4]    = {3.04, 1.21, 0.42, 0.18};
	double y_sndJpsi2012[4]    = {25.51, 10.97, 4.31, 1.95};
	double yErr_sndJpsi2012[4] = {1.97, 0.79, 0.31, 0.14};

// pT ratio:

	double y_pmtR2011[3] = {1.73, 1.30, 1.95};
	double y_pmtR2012[4] = {1.43, 1.31, 1.78, 1.99};
	double y_sndR2011[4] = {0.406, 0.337, 0.547, 0.422};
	double y_sndR2012[4] = {0.419, 0.455, 0.417, 0.401};

	double yErr_pmtR2011[3] = {0.48, 0.71, 1.10};
	double yErr_pmtR2012[4] = {1.13, 0.47, 0.58, 0.70};
	double yErr_sndR2011[4] = {0.108, 0.105, 0.113, 0.133};
	double yErr_sndR2012[4] = {0.082, 0.069, 0.077, 0.082};

// energy:

	double x_energyEtac[2] = {7, 8};
	double x_energyJpsi[3] = {2.76, 7, 8};
	double xErr_energy[3] = {0, 0, 0};

	double y_ratio[2]    = {1.75, 1.60};
	double yErr1_ratio[2]    = {0.30, 0.29};
	double yErr2_ratio[2]    = {0.41, 0.38};
	double yErr3_ratio[2]    = {0.45, 0.42};

	double y_etac[2]    = {0.496, 0.595};
	double yErr1_etac[2]    = {0.084, 0.108};
	double yErr2_etac[2]    = {0.115, 0.143};
	double yErr3_etac[2]    = {0.130, 0.162};

	double y_jpsi[3]    = {0.0393, 0.2839, 0.3714};
	double yErr1_jpsi[3]    = {0.0039, 0.0017, 0.0014};
	double yErr2_jpsi[3]    = {0.0049, 0.0153, 0.0272};



// drawing:

	TCanvas* canvPtAbs = new TCanvas("canvPtAbs", "canvPtAbs", 1000, 800);
	canvPtAbs->Divide(2,2);

	canvPtAbs->cd(1);
	TH1F* frame = gPad->DrawFrame(0, 2, 16, 500);
	gPad->SetLogy();
	frame->GetXaxis()->SetTitle("p_{T} [GeV/#font[12]{c}]");
	frame->GetXaxis()->SetTitleSize(0.05);
	frame->GetYaxis()->SetTitle("#sigma  nb/(GeV/#font[12]{c})");
	frame->GetYaxis()->SetTitleSize(0.05);

	TGraphErrors* grErr1_pmtJpsi2011 = new TGraphErrors(3, x_pmtJpsi2011, y_pmtJpsi2011, xErr_pmtJpsi2011, yErr_pmtJpsi2011);
	TGraphErrors* grErr1_pmtEtac2011 = new TGraphErrors(3, x_pmtEtac2011, y_pmtEtac2011, xErr_pmtEtac2011, yErr1_pmtEtac2011);
	TGraphErrors* grErr2_pmtEtac2011 = new TGraphErrors(3, x_pmtEtac2011, y_pmtEtac2011, xErr_pmtEtac2011, yErr2_pmtEtac2011);

	grErr1_pmtJpsi2011->SetMarkerStyle(4);
	grErr1_pmtEtac2011->SetMarkerStyle(20);

	grErr1_pmtJpsi2011->SetMarkerColor(kBlue);
	grErr1_pmtEtac2011->SetMarkerColor(kRed);

	grErr1_pmtJpsi2011->SetLineColor(kBlue);
	grErr1_pmtEtac2011->SetLineColor(kRed);
	grErr2_pmtEtac2011->SetLineColor(kRed);

	grErr1_pmtJpsi2011->Draw("pe0");
	grErr1_pmtEtac2011->Draw("pe0");
	grErr2_pmtEtac2011->Draw("pe0");

	TLegend* legend = new TLegend(.2, 0.6, .3, 0.8);
	legend->AddEntry(grErr1_pmtEtac2011, "  #eta_{c}", "lep");
	legend->AddEntry(grErr1_pmtJpsi2011, "  J/#psi", "lep");
	legend->SetFillStyle(0);
	legend->SetLineColor(0);
	legend->Draw();

	canvPtAbs->cd(3);
	frame = gPad->DrawFrame(0, 2, 16, 500);
	gPad->SetLogy();
	frame->GetXaxis()->SetTitle("p_{T} [GeV/#font[12]{c}]");
	frame->GetXaxis()->SetTitleSize(0.05);
	frame->GetYaxis()->SetTitle("#sigma  nb/(GeV/#font[12]{c}");
	frame->GetYaxis()->SetTitleSize(0.05);

	TGraphErrors* grErr1_pmtJpsi2012 = new TGraphErrors(4, x_pmtJpsi2012, y_pmtJpsi2012, xErr_pmtJpsi2012, yErr_pmtJpsi2012);
	TGraphErrors* grErr1_pmtEtac2012 = new TGraphErrors(4, x_pmtEtac2012, y_pmtEtac2012, xErr_pmtEtac2012, yErr1_pmtEtac2012);
	TGraphErrors* grErr2_pmtEtac2012 = new TGraphErrors(4, x_pmtEtac2012, y_pmtEtac2012, xErr_pmtEtac2012, yErr2_pmtEtac2012);

	grErr1_pmtJpsi2012->SetMarkerStyle(4);
	grErr1_pmtEtac2012->SetMarkerStyle(20);

	grErr1_pmtJpsi2012->SetMarkerColor(kBlue);
	grErr1_pmtEtac2012->SetMarkerColor(kRed);

	grErr1_pmtJpsi2012->SetLineColor(kBlue);
	grErr1_pmtEtac2012->SetLineColor(kRed);
	grErr2_pmtEtac2012->SetLineColor(kRed);

	grErr1_pmtJpsi2012->Draw("pe0");
	grErr1_pmtEtac2012->Draw("pe0");
	grErr2_pmtEtac2012->Draw("pe0");

	canvPtAbs->cd(2);
	frame = gPad->DrawFrame(0, 0.3, 16, 40);
	gPad->SetLogy();
	frame->GetXaxis()->SetTitle("p_{T} [GeV/#font[12]{c}]");
	frame->GetXaxis()->SetTitleSize(0.05);
	frame->GetYaxis()->SetTitle("#sigma  nb/(GeV/#font[12]{c})");
	frame->GetYaxis()->SetTitleSize(0.05);

	TGraphErrors* grErr1_sndJpsi2011 = new TGraphErrors(4, x_sndJpsi, y_sndJpsi2011, xErr_sndJpsi, yErr_sndJpsi2011);
	TGraphErrors* grErr1_sndEtac2011 = new TGraphErrors(4, x_sndEtac, y_sndEtac2011, xErr_sndEtac, yErr1_sndEtac2011);
	TGraphErrors* grErr2_sndEtac2011 = new TGraphErrors(4, x_sndEtac, y_sndEtac2011, xErr_sndEtac, yErr2_sndEtac2011);

	grErr1_sndJpsi2011->SetMarkerStyle(4);
	grErr1_sndEtac2011->SetMarkerStyle(20);

	grErr1_sndJpsi2011->SetMarkerColor(kBlue);
	grErr1_sndEtac2011->SetMarkerColor(kRed);

	grErr1_sndJpsi2011->SetLineColor(kBlue);
	grErr1_sndEtac2011->SetLineColor(kRed);
	grErr2_sndEtac2011->SetLineColor(kRed);

	grErr1_sndJpsi2011->Draw("pe0");
	grErr1_sndEtac2011->Draw("pe0");
	grErr2_sndEtac2011->Draw("pe0");

	canvPtAbs->cd(4);
	frame = gPad->DrawFrame(0, 0.3, 16, 40);
	gPad->SetLogy();
	frame->GetXaxis()->SetTitle("p_{T} [GeV/#font[12]{c}]");
	frame->GetXaxis()->SetTitleSize(0.05);
	frame->GetYaxis()->SetTitle("#sigma  nb/(GeV/#font[12]{c})");
	frame->GetYaxis()->SetTitleSize(0.05);

	TGraphErrors* grErr1_sndJpsi2012 = new TGraphErrors(4, x_sndJpsi, y_sndJpsi2012, xErr_sndJpsi, yErr_sndJpsi2012);
	TGraphErrors* grErr1_sndEtac2012 = new TGraphErrors(4, x_sndEtac, y_sndEtac2012, xErr_sndEtac, yErr1_sndEtac2012);
	TGraphErrors* grErr2_sndEtac2012 = new TGraphErrors(4, x_sndEtac, y_sndEtac2012, xErr_sndEtac, yErr2_sndEtac2012);

	grErr1_sndJpsi2012->SetMarkerStyle(4);
	grErr1_sndEtac2012->SetMarkerStyle(20);

	grErr1_sndJpsi2012->SetMarkerColor(kBlue);
	grErr1_sndEtac2012->SetMarkerColor(kRed);

	grErr1_sndJpsi2012->SetLineColor(kBlue);
	grErr1_sndEtac2012->SetLineColor(kRed);
	grErr2_sndEtac2012->SetLineColor(kRed);

	grErr1_sndJpsi2012->Draw("pe0");
	grErr1_sndEtac2012->Draw("pe0");
	grErr2_sndEtac2012->Draw("pe0");

// pT ratio:

	TCanvas* canvPtRatio = new TCanvas("canvPtRatio", "canvPtRatio", 1000, 400);
	canvPtRatio->Divide(2,1);

	canvPtRatio->cd(1);
	frame = gPad->DrawFrame(0, 0, 16, 3.2);
//	frame->SetTitle("prompt");
//	frame->GetXaxis()->SetTitle("p_{T} GeV");
//	frame->GetYaxis()->SetTitle("N_{#eta_{c}}/N_{J/#psi}");

	TGraphErrors* grRatio_pmt2011 = new TGraphErrors(3, x_pmtJpsi2011, y_pmtR2011, xErr_pmtJpsi2011, yErr_pmtR2011);
	TGraphErrors* grRatio_pmt2012 = new TGraphErrors(4, x_pmtEtac2012, y_pmtR2012, xErr_pmtEtac2012, yErr_pmtR2012);

	grRatio_pmt2011->SetMarkerStyle(22);
	grRatio_pmt2012->SetMarkerStyle(32);

	grRatio_pmt2011->SetMarkerColor(kMagenta+2);
	grRatio_pmt2012->SetMarkerColor(kCyan+2);
	grRatio_pmt2011->SetLineColor(kMagenta+2);
	grRatio_pmt2012->SetLineColor(kCyan+2);

	grRatio_pmt2011->Draw("pe0");
	grRatio_pmt2012->Draw("pe0");

	TLegend* legend2 = new TLegend(.2, 0.6, .35, 0.8);
	legend2->AddEntry(grRatio_pmt2011, "2011", "lep");
	legend2->AddEntry(grRatio_pmt2012, "2012", "lep");
	legend2->SetFillStyle(0);
	legend2->SetLineColor(0);
	legend2->Draw();


	canvPtRatio->cd(2);
	frame = gPad->DrawFrame(0, 0, 16, 0.8);
//	frame->SetTitle("secondary");
//	frame->GetXaxis()->SetTitle("p_{T} GeV");
//	frame->GetYaxis()->SetTitle("N_{#eta_{c}}/N_{J/#psi}");

	TGraphErrors* grRatio_snd2011 = new TGraphErrors(4, x_sndJpsi, y_sndR2011, xErr_sndJpsi, yErr_sndR2011);
	TGraphErrors* grRatio_snd2012 = new TGraphErrors(4, x_sndEtac, y_sndR2012, xErr_sndEtac, yErr_sndR2012);

	grRatio_snd2011->SetMarkerStyle(22);
	grRatio_snd2012->SetMarkerStyle(32);

	grRatio_snd2011->SetMarkerColor(kMagenta+2);
	grRatio_snd2012->SetMarkerColor(kCyan+2);
	grRatio_snd2011->SetLineColor(kMagenta+2);
	grRatio_snd2012->SetLineColor(kCyan+2);

	grRatio_snd2011->Draw("pe0");
	grRatio_snd2012->Draw("pe0");

// energy:

	TCanvas* canvEnergyRatio = new TCanvas("canvEnergyRatio", "canvEnergyRatio", 500, 400);

	frame = gPad->DrawFrame(0, 0, 10, 2.5);
//	frame->GetXaxis()->SetTitle("#sqrt{s}, TeV");
//	frame->GetYaxis()->SetTitle("#sigma_{#eta_{c}}/#sigma_{J/#psi}");

	TGraphErrors* grErr1_EnergyRatio = new TGraphErrors(2, x_energyEtac, y_ratio, xErr_energy, yErr1_ratio);
	TGraphErrors* grErr2_EnergyRatio = new TGraphErrors(2, x_energyEtac, y_ratio, xErr_energy, yErr2_ratio);
	TGraphErrors* grErr3_EnergyRatio = new TGraphErrors(2, x_energyEtac, y_ratio, xErr_energy, yErr3_ratio);

	grErr1_EnergyRatio->SetMarkerStyle(20);
	grErr1_EnergyRatio->Draw("pe0");
	grErr2_EnergyRatio->Draw("pe0");
	grErr3_EnergyRatio->Draw("pe0");

	TCanvas* canvEnergyAbs = new TCanvas("canvEnergyAbs", "canvEnergyAbs", 500, 400);

	frame = gPad->DrawFrame(0, 0, 10, 0.8);
//	frame->GetXaxis()->SetTitle("#sqrt{s}, TeV");
//	frame->GetYaxis()->SetTitle("#sigma_{#eta_{c}}, #mub");

	TGraphErrors* grErr1_EtacEnergyAbs = new TGraphErrors(2, x_energyEtac, y_etac, xErr_energy, yErr1_etac);
	TGraphErrors* grErr2_EtacEnergyAbs = new TGraphErrors(2, x_energyEtac, y_etac, xErr_energy, yErr2_etac);
	TGraphErrors* grErr3_EtacEnergyAbs = new TGraphErrors(2, x_energyEtac, y_etac, xErr_energy, yErr3_etac);

	TGraphErrors* grErr1_JpsiEnergyAbs = new TGraphErrors(3, x_energyJpsi, y_jpsi, xErr_energy, yErr2_jpsi);

	grErr1_EtacEnergyAbs->SetMarkerStyle(20);
//	grErr1_EtacEnergyAbs->SetMarkerColor(kRed);
//	grErr1_EtacEnergyAbs->SetLineColor(kRed);
//	grErr2_EtacEnergyAbs->SetLineColor(kRed);
//	grErr3_EtacEnergyAbs->SetLineColor(kRed);

	grErr2_EtacEnergyAbs->Draw("pe0");
	grErr3_EtacEnergyAbs->Draw("pe0");
	grErr1_EtacEnergyAbs->Draw("pe0");

	grErr1_JpsiEnergyAbs->SetMarkerStyle(4);
	grErr1_JpsiEnergyAbs->SetMarkerColor(kBlue);
	grErr1_JpsiEnergyAbs->SetLineColor(kBlue);
	grErr1_JpsiEnergyAbs->Draw("pe0");

	TLegend* legend3 = new TLegend(.2, 0.6, .3, 0.8);
	legend3->AddEntry(grErr1_EtacEnergyAbs, "  #eta_{c}", "lep");
	legend3->AddEntry(grErr1_JpsiEnergyAbs, "  J/#psi", "lep");
	legend3->SetFillStyle(0);
	legend3->SetLineColor(0);
	legend3->Draw();










}


