from ROOT import *
from ROOT.TMath import Sq, Sqrt
from array import array

BREtacToPP = 1.50e-3
BREtacToPPErr = 0.16e-3

BRJpsiToPP = 2.12e-3
BRJpsiToPPErr = 0.029e-3

brB2Jpsi    = 1.16*0.01
brB2JpsiErr = 0.10*0.01

eff = 0.99
effErr = 0.02
PTBins  = array("d", [7.25, 9.0, 11.0, 13.0, 16.0])
PTBinHW = array("d", [0.75, 1.0, 1.0, 1.0, 2.0])


def totCS(NEtacSec, NEtacPr, NEtacSecErrStat, NEtacPrErrStat, NEtacSecErrSyst, NEtacPrErrSyst):
    
    NEtacPrErr  = Sqrt(Sq(NEtacPrErrStat)+Sq(NEtacPrErrSyst))
    NEtacSecErr = Sqrt(Sq(NEtacSecErrStat)+Sq(NEtacSecErrSyst))
    
    cSJpsiPr, cSJpsiSec               = 748.985, 203.996
    cSJpsiPrErrStat, cSJpsiSecErrStat = 14.686,  6.182
    cSJpsiPrErrSyst, cSJpsiSecErrSyst = 18.807,  5.254
    cSJpsiPrErr  = Sqrt(Sq(cSJpsiPrErrStat)+Sq(cSJpsiPrErrSyst))
    cSJpsiSecErr = Sqrt(Sq(cSJpsiSecErrStat)+Sq(cSJpsiSecErrSyst))




    cSEtacPrRel  = NEtacPr*BRJpsiToPP/BREtacToPP/eff
    cSEtacSecRel = NEtacSec*BRJpsiToPP/BREtacToPP/eff
    
    cSEtacPrRelErrStat  = cSEtacPrRel* (NEtacPrErrStat)
    cSEtacSecRelErrStat = cSEtacSecRel * (NEtacSecErrStat)
    
    cSEtacPrRelErrSyst  = cSEtacPrRel * (NEtacPrErrSyst)
    cSEtacSecRelErrSyst = cSEtacSecRel * (NEtacSecErrSyst)
    
    
    cSEtacPrRelErrNorm = cSEtacPrRel * Sqrt(Sq(BRJpsiToPPErr/BRJpsiToPP) + Sq (BREtacToPPErr/BREtacToPP) + Sq(effErr/eff))
    cSEtacSecRelErrNorm = cSEtacSecRel * Sqrt(Sq(BRJpsiToPPErr/BRJpsiToPP) + Sq (BREtacToPPErr/BREtacToPP) + Sq(effErr/eff))
    
    cSEtacPrRelErr = Sqrt( Sq(cSEtacPrRelErrStat) + Sq(cSEtacPrRelErrSyst) + Sq(cSEtacPrRelErrNorm))
    cSEtacSecRelErr = Sqrt(Sq(cSEtacSecRelErrStat) + Sq(cSEtacSecRelErrSyst) + Sq(cSEtacSecRelErrNorm))
    
    cSEtacPrRelErrSS = Sqrt(Sq(cSEtacPrRelErrStat)+Sq(cSEtacPrRelErrSyst))
    cSEtacSecRelErrSS = Sqrt(Sq(cSEtacSecRelErrStat)+Sq(cSEtacSecRelErrSyst))
    
    print "prompt Etac Rel", cSEtacPrRel, cSEtacPrRelErrStat, cSEtacPrRelErrSyst, cSEtacPrRelErrNorm
    print "fromB Etac  Rel", cSEtacSecRel, cSEtacSecRelErrStat, cSEtacSecRelErrSyst, cSEtacSecRelErrNorm, "\n"





        
    cSEtacPr = NEtacPr*cSJpsiPr*BRJpsiToPP/BREtacToPP/eff
    cSEtacSec= NEtacSec*cSJpsiSec*BRJpsiToPP/BREtacToPP/eff
    
    cSEtacPrErrStat= cSEtacPr* (NEtacPrErrStat)
    cSEtacSecErrStat = cSEtacSec * (NEtacSecErrStat)
    
    cSEtacPrErrSyst = cSEtacPr * (NEtacPrErrSyst)
    cSEtacSecErrSyst = cSEtacSec * (NEtacSecErrSyst)
    
    
    cSEtacPrErrNorm = cSEtacPr * Sqrt(Sq(cSJpsiPrErr/cSJpsiPr) + Sq(BRJpsiToPPErr/BRJpsiToPP) + Sq (BREtacToPPErr/BREtacToPP) + Sq(effErr/eff))
    cSEtacSecErrNorm = cSEtacSec * Sqrt(Sq(cSJpsiSecErr/cSJpsiSec) + Sq(BRJpsiToPPErr/BRJpsiToPP) + Sq (BREtacToPPErr/BREtacToPP) + Sq(effErr/eff))
    
    cSEtacPrErr = Sqrt( Sq(cSEtacPrErrStat) + Sq(cSEtacPrErrSyst) + Sq(cSEtacPrErrNorm))
    cSEtacSecErr = Sqrt(Sq(cSEtacSecErrStat) + Sq(cSEtacSecErrSyst) + Sq(cSEtacSecErrNorm))
    
    cSEtacPrErrSS = Sqrt(Sq(cSEtacPrErrStat)+Sq(cSEtacPrErrSyst))
    cSEtacSecErrSS = Sqrt(Sq(cSEtacSecErrStat)+Sq(cSEtacSecErrSyst))
    
    print "prompt Etac ", cSEtacPr, cSEtacPrErrStat, cSEtacPrErrSyst, cSEtacPrErrNorm
    print "fromB  Etac  ", cSEtacSec, cSEtacSecErrStat, cSEtacSecErrSyst, cSEtacSecErrNorm, "\n"


    brB2etac= NEtacSec*brB2Jpsi*BRJpsiToPP/BREtacToPP/eff
    brB2etacErrStat = brB2etac * (NEtacSecErrStat)
    brB2etacErrSyst = brB2etac * (NEtacSecErrSyst)
    
    brB2etacErrNorm = brB2etac * Sqrt(Sq(brB2JpsiErr/brB2Jpsi) + Sq(BRJpsiToPPErr/BRJpsiToPP) + Sq (BREtacToPPErr/BREtacToPP) + Sq(effErr/eff))
    brB2etacErr = Sqrt(Sq(brB2etacErrStat) + Sq(brB2etacErrSyst) + Sq(brB2etacErrNorm))
    brB2etacErrSS = Sqrt(Sq(brB2etacErrStat)+Sq(brB2etacErrSyst))
    
    print "fromB  Etac  BR", brB2etac, brB2etacErrStat, brB2etacErrSyst, brB2etacErrNorm, "\n"


print "tzCut: "
NEtacSec_tzCut,        NEtacPr_tzCut        = 0.337, 1.166 
NEtacSecErrStat_tzCut, NEtacPrErrStat_tzCut = 0.053, 0.085 
NEtacSecErrSyst_tzCut, NEtacPrErrSyst_tzCut = 0.052, 0.060 
totCS(NEtacSec_tzCut,        NEtacPr_tzCut,\
    NEtacSecErrStat_tzCut, NEtacPrErrStat_tzCut,\
    NEtacSecErrSyst_tzCut, NEtacPrErrSyst_tzCut)

print "\n\n tzFit: "
NEtacSec_tzFit,        NEtacPr_tzFit        = 0.331,  1.316
NEtacSecErrStat_tzFit, NEtacPrErrStat_tzFit = 0.092,  0.086
NEtacSecErrSyst_tzFit, NEtacPrErrSyst_tzFit = 0.066,  0.074
totCS(NEtacSec_tzFit,        NEtacPr_tzFit,\
    NEtacSecErrStat_tzFit, NEtacPrErrStat_tzFit,\
    NEtacSecErrSyst_tzFit, NEtacPrErrSyst_tzFit) 
