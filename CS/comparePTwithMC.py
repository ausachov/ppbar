from ROOT import *
from ROOT.TMath import Sq,Sqrt
from array import array

from crosssection import *

def comparePTwithMC(NEtacPrDict,NEtacSecDict):

    relFromBMC, relPromptMC, relFromBMCErr, relPromptMCErr = array("d", nPTBins*[0]), array("d", nPTBins*[0]), array("d", nPTBins*[0]), array("d", nPTBins*[0])
    
    
    normPmtDataMCFactor, normSecDataMCFactor = 1., 1.
    totMCPmt,   totMCSec                     = 0., 0.
    totDataPmt, totDataSec                   = 0., 0.

    ntEtac=TChain("MCDecayTree")
    ntJpsi=TChain("MCDecayTree")
    
    ntEtac.Add(mcDir+"Etac_allMCDecayTree_AddBr.root")
    ntJpsi.Add(mcDir+"Jpsi_allMCDecayTree_AddBr.root")

    for iPT in range(nPTBins):

        cutPT = "Jpsi_TRUEPT>"+str(1000*ptBinning[iPT])+" && Jpsi_TRUEPT<"+str(1000*ptBinning[iPT+1])

        treeEtac = ntEtac.CopyTree("Jpsi_prompt &&" + cutPT)
        treeJpsi = ntJpsi.CopyTree("Jpsi_prompt &&" + cutPT)    
        
        NEtac = float(treeEtac.GetEntries(""))
        NJpsi = float(treeJpsi.GetEntries(""))


        relPromptMC[iPT]    = NEtac/NJpsi
        relPromptMCErr[iPT] = relPromptMC[iPT]*Sqrt(1./NEtac+1./NJpsi)
        totMCPmt   += NEtac/NJpsi*PTBinHW[iPT]*2.
        totDataPmt += NEtacPrDict.d["val"][iPT]*PTBinHW[iPT]*2.


        treeEtac = ntEtac.CopyTree("Jpsi_sec && "+cutPT+" && Jpsi_TRUEM < 3050.0") 
        treeJpsi = ntJpsi.CopyTree("Jpsi_sec && "+cutPT)    
        NEtac = float(treeEtac.GetEntries(""))
        NJpsi = float(treeJpsi.GetEntries(""))

        relFromBMC[iPT]    = normSecDataMCFactor*NEtac/NJpsi
        relFromBMCErr[iPT] = relFromBMC[iPT]*Sqrt(1./NEtac+1./NJpsi)
        totMCSec   += NEtac/NJpsi*PTBinHW[iPT]*2.
        totDataSec += NEtacSecDict.d["val"][iPT]*PTBinHW[iPT]*2.

    for iPT in range(nPTBins):
        relPromptMC[iPT]    = relPromptMC[iPT]*totDataPmt/totMCPmt
        relPromptMCErr[iPT] = relPromptMCErr[iPT]*totDataPmt/totMCPmt

        relFromBMC[iPT]    = relFromBMC[iPT]*totDataSec/totMCSec
        relFromBMCErr[iPT] = relFromBMCErr[iPT]*totDataSec/totMCSec




        
    canv_Pr_vs_MC  = TCanvas("canv_Pr_vs_MC","N Rel",55,55,550,400)
    canv_Sec_vs_MC = TCanvas("canv_Sec2_vs_MC","N Rel",55,55,550,400)

    canv_Pr_vs_MC.cd()
    gPad.SetLeftMargin(0.20)
    #gPad.SetBottomMargin(0.125)

    nRelPr     = createHisto("nRelPr",NEtacPrDict.d["val"],NEtacPrDict.d["SS"])
    nRelPrStat = createHisto("nRelPrStat",NEtacPrDict.d["val"],NEtacPrDict.d["stat"])
    nRelPrMC   = createHisto("nRelPrMC",relPromptMC,relPromptMCErr)

    nRelPr.Draw("E1")
    nRelPrStat.Draw("E1 SAME")
    nRelPrMC.Draw("SAME")
    nRelPr.SetTitle("prompt")
    nRelPr.SetLineColor(6)
    nRelPr.SetMinimum(0.0)
    nRelPr.SetMaximum(3.0)
    nRelPr.GetYaxis().SetTitle("#frac{#sigma_{#eta_{c}} #times BR(#eta_{c} #rightarrow p#bar{p})}{#sigma_{J/#psi} #times BR(J/#psi #rightarrow p#bar{p})}")
    nRelPr.GetXaxis().SetTitleOffset(0.90)
    nRelPr.GetYaxis().SetTitleOffset(1.10)
    nRelPrStat.SetLineColor(4)
    nRelPrStat.SetMarkerStyle(23)

    texCS = TLatex()
    texCS.SetNDC()


    texCS.DrawLatex(0.25, 0.76, "LHCb preliminary")
    texCS.DrawLatex(0.25, 0.70, "#sqrt{s} = 13 TeV")
    texCS.DrawLatex(0.25, 0.64, "2.0 < y < 4.5")

    legPmt = TLegend(0.75,0.20,0.85,0.40)
    legPmt.SetBorderSize(0)
    legPmt.AddEntry(nRelPrStat,"Data","lep")
    legPmt.AddEntry(nRelPrMC,"MC","lep")
    legPmt.Draw()

    canv_Sec_vs_MC.cd()
    gPad.SetLeftMargin(0.20)

    nRelSec     = createHisto("nRelSec",NEtacSecDict.d["val"],NEtacSecDict.d["SS"])
    nRelSecStat = createHisto("nRelSecStat",NEtacSecDict.d["val"],NEtacSecDict.d["stat"])
    nRelSecMC   = createHisto("nRelSecMC",relFromBMC,relFromBMCErr)

    nRelSec.Draw("E1")
    nRelSecStat.Draw("E1 SAME")
    nRelSecMC.Draw("SAME")
    nRelSec.SetTitle("from-b")
    nRelSec.SetLineColor(6)
    nRelSec.SetMinimum(0.0)
    nRelSec.SetMaximum(0.8)
    nRelSec.GetYaxis().SetTitle("#frac{#sigma_{b#rightarrow#eta_{c}X} #times BR(#eta_{c} #rightarrow p#bar{p})}{#sigma_{b#rightarrowJ/#psiX} #times BR(J/#psi #rightarrow p#bar{p})}")
    nRelSec.GetXaxis().SetTitleOffset(0.90)
    nRelSec.GetYaxis().SetTitleOffset(1.10)
    nRelSecStat.SetLineColor(4)

    texCS.DrawLatex(0.25, 0.76, "LHCb preliminary")
    texCS.DrawLatex(0.25, 0.7, "#sqrt{s} = 13 TeV")
    texCS.DrawLatex(0.25, 0.64, "2.0 < y < 4.5")

    legSec = TLegend(0.75,0.20,0.85,0.40)
    legSec.SetBorderSize(0)
    legSec.AddEntry(nRelSecStat,"Data","lep")
    legSec.AddEntry(nRelSecMC,"MC","lep")
    legSec.Draw()

    for graph in [nRelPrStat,nRelPr,nRelSecStat,nRelSec]:
        graph.GetXaxis().SetLimits(rangeL,rangeR)
        graph.GetXaxis().SetTitle("p_{t}, GeV/c")
        graph.SetLineWidth(2)
        graph.SetMarkerStyle(23)


    canv_Pr_vs_MC.SaveAs(homeDir+"Results/CS/NRelPrompt_vs_MC.pdf")
    canv_Sec_vs_MC.SaveAs(homeDir+"Results/CS/NRelSec_vs_MC.pdf")


    
CSRelPrDict_tzFit,  CSRelSecDict_tzFit, \
CSEtacPrDict_tzFit, CSEtacSecDict_tzFit, \
CSJpsiPrDict_tzFit, CSJpsiSecDict_tzFit, \
NEtacPrDict_tzFit,  NEtacSecDict_tzFit = crosssection(CSfilename="Results/rel_PT_sim.txt")


CSRelPrDict_tzCut,  CSRelSecDict_tzCut, \
CSEtacPrDict_tzCut, CSEtacSecDict_tzCut, \
CSJpsiPrDict_tzCut, CSJpsiSecDict_tzCut, \
NEtacPrDict_tzCut,  NEtacSecDict_tzCut = crosssection(CSfilename="Results/rel_PT_RunIMethod.txt")


gROOT.LoadMacro("../lhcbStyle.C")
comparePTwithMC(NEtacPrDict_tzFit,NEtacSecDict_tzCut)
