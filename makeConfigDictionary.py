# import subprocess
# pwd = subprocess.check_output("pwd", shell=True)

# MC part
mcDir   = "/sps/lhcb/zhovkovska/etacToPpbar/MC/"
mcMyDir = "/sps/lhcb/usachov/ppbar/ppbar/MC/"
MCTuples = {
    "etac"   :{ "Reco"        :[mcDir+"EtacDiProton_MC_NoStripping_2015.root ",
                                mcDir+"EtacDiProton_MC_NoStripping_2016.root "],
                "promptSel"   :[mcDir+"EtacDiProton_MC_2015.root ",
                                mcDir+"EtacDiProton_MC_2016.root "],
                "secondarySel": "../MC/Etac/*",
                "MCDecayTree" : "../MC/Etac/*",
                "NoGenCut"    : "../MC/Etac/*",
                "GenLevelEff" : 0,
                "SndTopo"     : [mcMyDir+"Etac/2015/Stripping/EtacDiProton_MC_Detached_2015_AddBr.root",
                                 mcMyDir+"Etac/2016/Stripping/EtacDiProton_MC_Detached_2016_AddBr.root"]
                },
    "jpsi"   :{ "Reco"        :[mcDir+"JpsiDiProton_MC_NoStripping_2015.root ",
                                mcDir+"JpsiDiProton_MC_NoStripping_2016.root "],
                "promptSel"   :[mcDir+"JpsiDiProton_MC_2015.root ",
                                mcDir+"JpsiDiProton_MC_2016.root "],
                "secondarySel": "",
                "MCDecayTree" : "",
                "NoGenCut"    : "",
                "GenLevelEff" : 0.,
                "SndTopo"     : [mcMyDir+"Jpsi/2015/Stripping/JpsiDiProton_MC_Detached_2015_AddBr.root",
                                 mcMyDir+"Jpsi/2016/Stripping/JpsiDiProton_MC_Detached_2016_AddBr.root"]
                },
    "b2etacX":{ "Reco"        :[mcDir+"EtacDiProton_MC_NoStripping_2015_incl_b.root ",
                                mcDir+"EtacDiProton_MC_NoStripping_2016_incl_b.root "],
                "promptSel"   :[mcDir+"EtacDiProton_MC_2015_incl_b.root ",
                                mcDir+"EtacDiProton_MC_2016_incl_b.root "],
                "secondarySel": "",
                "MCDecayTree" : "",
                "NoGenCut"    : "",
                "GenLevelEff" : 0,
                "SndTopo"     :[mcMyDir+"incl_b/2015/Stripping/EtacDiProton_MC_2015_Detached_incl_b_AddBr.root",
                                mcMyDir+"incl_b/2016/Stripping/EtacDiProton_MC_2016_Detached_incl_b_AddBr.root"]
                }
}


# Data DiProton Triggers

import os
pwd = os.path.dirname(os.path.realpath(__file__))
histosDir = pwd.rstrip()+"/Histos/"


cutApriori2015_Dict = {"DiProton":"Jpsi_Hlt1DiProtonDecision_TOS && Jpsi_Hlt2DiProtonDecision_TOS && \
                                  ProtonP_P>12.5e3 && ProtonM_P>12.5e3 && \
                                  ProtonP_PT>2000 && ProtonM_PT>2000 && \
                                  ProtonP_PT/ProtonP_P>0.0366 && ProtonM_PT/ProtonM_P>0.0366 && \
                                  ProtonP_PIDp>20 && ProtonM_PIDp>20 && \
                                  (ProtonP_PIDp-ProtonP_PIDK)>15 && (ProtonM_PIDp-ProtonM_PIDK)>15 && \
                                  ProtonP_TRACK_CHI2NDOF<2.5 && ProtonM_TRACK_CHI2NDOF<2.5 && \
                                  Jpsi_ENDVERTEX_CHI2<3.5 && \
                                  Jpsi_Y>2 && Jpsi_Y<4.5 && \
                                  Jpsi_PT>6500 && \
                                  ProtonP_TRACK_GhostProb<0.2 && ProtonP_TRACK_GhostProb<0.2 && \
                                  ProtonP_TRACK_CloneDist<0 && ProtonM_TRACK_CloneDist<0"
                      }

cutApriori2016_Dict = {"DiProton":"Jpsi_Hlt1DiProtonDecision_TOS && Jpsi_Hlt2CcDiHadronDiProtonDecision_TOS && \
                                  ProtonP_P>12.5e3 && ProtonM_P>12.5e3 && \
                                  ProtonP_PT>2000 && ProtonM_PT>2000 && \
                                  ProtonP_PT/ProtonP_P>0.0366 && ProtonM_PT/ProtonM_P>0.0366 && \
                                  ProtonP_PIDp>20 && ProtonM_PIDp>20 && \
                                  (ProtonP_PIDp-ProtonP_PIDK)>15 && (ProtonM_PIDp-ProtonM_PIDK)>15 && \
                                  ProtonP_TRACK_CHI2NDOF<2.5 && ProtonM_TRACK_CHI2NDOF<2.5 && \
                                  Jpsi_ENDVERTEX_CHI2<3.5 && \
                                  Jpsi_Y>2 && Jpsi_Y<4.5 && \
                                  Jpsi_PT>6500 && \
                                  ProtonP_TRACK_GhostProb<0.2 && ProtonP_TRACK_GhostProb<0.2 && \
                                  ProtonP_TRACK_CloneDist<0 && ProtonM_TRACK_CloneDist<0"
                      }


tzVar = "(3.3)*(Jpsi_ENDVERTEX_Z-Jpsi_OWNPV_Z)*Jpsi_MM/Jpsi_PZ"



binningDict = {
    "Jpsi_PT":                      [6500, 8000, 10000, 12000, 14000, 18000],
    "Jpsi_ETA":                     [2.0, 2.45, 2.8, 3.15, 4.5],
    "TMath::Abs(ProtonM_CosTheta)": [0.0, 0.15, 0.3, 0.50, 1.0],
    "nSPDHits":                     [0, 100, 150, 200, 250, 300],
}

charmDict = {
    "Jpsi":     0,
    "Etac":     1,
}

tzTotalBinning =  [-10., -0.125, -0.025, 0.0, 0.025, 0.2, 1.0, 1.5, 2.0, 3.0, 4.0, 10]
#tzInBinsBinning = [[-10., -0.025, 0., 0.025, 0.1, 1., 4., 10.],
                       #[-10., -0.025, 0.,        0.200,   2., 10.]]
tzInBinsBinning = [-10., -0.125, -0.025, 0.0, 0.20, 2.0, 4.0, 10.0]



minMass = 2850.
maxMass = 3250.
binWidth = 0.4

minMassSnd = 2850.
maxMassSnd = 4000.
binWidthSnd = 0.1



tuples2015 = ["/eos/user/a/ausachov/DataRunII_ppbar/569/mergedTuple*.root",
              "/eos/user/a/ausachov/DataRunII_ppbar/570/mergedTuple*.root"]

tuples2016 = ["/eos/user/a/ausachov/DataRunII_ppbar/567/mergedTuple*.root",
              "/eos/user/a/ausachov/DataRunII_ppbar/568/mergedTuple*.root"]



tuplesSnd2016 = ["/eos/user/a/ausachov/DataRunII_ppbar/643/mergedTuple*.root",
                 "/eos/user/a/ausachov/DataRunII_ppbar/644/mergedTuple*.root"]

tuplesSnd2015 = ["/eos/user/a/ausachov/DataRunII_ppbar/933/mergedTuple*.root",
                 "/eos/user/a/ausachov/DataRunII_ppbar/934/mergedTuple*.root"]


cutsAprioriDetached = "Jpsi_L0HadronDecision_TOS && \
                      (Jpsi_Hlt1TrackMVADecision_TOS || Jpsi_Hlt1TwoTrackMVADecision_TOS) && \
                      (Jpsi_Hlt2Topo2BodyDecision_TOS || Jpsi_Hlt2Topo3BodyDecision_TOS || Jpsi_Hlt2Topo4BodyDecision_TOS) && \
                      ProtonP_P>-2e3 && ProtonM_P>-2e3 && \
                      ProtonP_PT>1000 && ProtonM_PT>1000 && \
                      ProtonP_PIDp>15 && ProtonM_PIDp>15 && \
                      (ProtonP_PIDp-ProtonP_PIDK)>10 && (ProtonM_PIDp-ProtonM_PIDK)>10 && \
                      ProtonP_TRACK_CHI2NDOF<5 && ProtonM_TRACK_CHI2NDOF<5 && \
                      ProtonP_IPCHI2_OWNPV>9 && ProtonM_IPCHI2_OWNPV>9 && \
                      Jpsi_ENDVERTEX_CHI2<9 && \
                      Jpsi_Y>2 && Jpsi_Y<4.5 && \
                      Jpsi_FDCHI2_OWNPV>25 && \
                      nSPDHits<600"


cutsAprioriDetachedNoPID = "Jpsi_L0HadronDecision_TOS && \
                      (Jpsi_Hlt1TrackMVADecision_TOS || Jpsi_Hlt1TwoTrackMVADecision_TOS) && \
                      (Jpsi_Hlt2Topo2BodyDecision_TOS || Jpsi_Hlt2Topo3BodyDecision_TOS || Jpsi_Hlt2Topo4BodyDecision_TOS) && \
                      ProtonP_P>-2e3 && ProtonM_P>-2e3 && \
                      ProtonP_PT>1000 && ProtonM_PT>1000 && \
                      ProtonP_TRACK_CHI2NDOF<5 && ProtonM_TRACK_CHI2NDOF<5 && \
                      ProtonP_IPCHI2_OWNPV>9 && ProtonM_IPCHI2_OWNPV>9 && \
                      Jpsi_ENDVERTEX_CHI2<9 && \
                      Jpsi_Y>2 && Jpsi_Y<4.5 && \
                      Jpsi_FDCHI2_OWNPV>25 && \
                      nSPDHits<600"


cutsAprioriVeryLooseNoPID = "ProtonP_PT>1000 && \
                             ProtonP_ETA>2 && ProtonP_ETA<4.5 && \
                             ProtonP_P>12500 && ProtonP_P<80000"


frombDataHist  = ["../Histos/2015/SndTopo/secondary_l0TOS/Total/Tz0.root",
                  "../Histos/2016/SndTopo/secondary_l0TOS/Total/Tz0.root"]



cutsDict = {
    "all"       : "1",
    "all_l0TOS" : "Jpsi_L0HadronDecision_TOS",
    "all_l0TIS" : "Jpsi_L0HadronDecision_TIS",
    "secondary" : "("+tzVar + ">0.08) && (ProtonP_IPCHI2_OWNPV > 16) && (ProtonM_IPCHI2_OWNPV > 16)",
    "secondary_l0TOS" : "("+tzVar+">0.08) && (Jpsi_L0HadronDecision_TOS) && (ProtonP_IPCHI2_OWNPV > 16) && (ProtonM_IPCHI2_OWNPV > 16)",
    "secondaryTopoOpt" : "(Jpsi_PT>5500) && \
                          (ProtonP_PT>1000) && (ProtonM_PT>1000) && \
                          (Jpsi_FDCHI2_OWNPV>81) && \
                          ProtonP_TRACK_CHI2NDOF<5 && ProtonM_TRACK_CHI2NDOF<5 && \
                          (ProtonP_IPCHI2_OWNPV>9) && (ProtonM_IPCHI2_OWNPV>9)",
    "secondaryTopoOptTzCut" : "(Jpsi_PT>5500) && \
                          (ProtonP_PT>1000) && (ProtonM_PT>1000) && \
                          (Jpsi_FDCHI2_OWNPV>81) && \
                          ProtonP_TRACK_CHI2NDOF<5 && ProtonM_TRACK_CHI2NDOF<5 && \
                          (ProtonP_IPCHI2_OWNPV>9) && (ProtonM_IPCHI2_OWNPV>9) && \
                          ("+tzVar + ">1.5)",
    "prompt"    : tzVar + "<0.08",
    "prompt_l0TOS"  : "("+tzVar+"<0.08) && (Jpsi_L0HadronDecision_TOS)",
    "Detached"      : cutsAprioriDetached,
    "DetachedNoPID" : cutsAprioriDetachedNoPID
    
    }

