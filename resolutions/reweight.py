from ROOT import *
from array import array
homeDir = "/users/LHCb/zhovkovska/scripts/"

def reweight():

    gROOT.Reset()
    gROOT.SetStyle("Plain")

    jpsiMass = 3096.90
    ptBins = array("f", [6500., 8000., 10000., 12000., 14000.])

    ntJpsiMC = TChain("DecayTree")
    ntJpsiMC.Add(homeDir+'MC/newMC/Jpsi/JpsiDiProton_MC_2016_AddBr.root')
    ntJpsiMC.Add(homeDir+'MC/newMC/Jpsi/JpsiDiProton_MC_2015_AddBr.root')

    #Float_t jpsiGenN = 644997 + 629999
    #cout << jpsiGenN << " jpsi were generated" << endl


#====================


    minMass = jpsiMass-70.
    maxMass = jpsiMass+70.
    binWidth = 100.
    binN = int((maxMass-minMass)/binWidth)

    entries = ntJpsiMC.GetEntries()
    print "entries = %2.2f\n"%entries

    cutReweight = "(1./(1-0.1/3.))*(1-0.1*ProtonM_CosTheta**2)"

    histJpsiMC = TH1D("histJpsiMC", "Invariant mass of p#bar{p}", binN, minMass, maxMass)
    ntJpsiMC.Project("histJpsiMC", "Jpsi_MM", "")

    histProtonPCos = TH1D("histProtonPCos", "histProtonPCos", 100, -1, 1)
    ntJpsiMC.Project("histProtonPCos", "ProtonM_CosTheta", "")
    histProtonPCosw = TH1D("histProtonPCosw", "histProtonPCosw", 100, -1, 1)
    ntJpsiMC.Project("histProtonPCosw", "ProtonM_CosTheta", cutReweight)

    histJpsiPT = TH1D("histJpsiPT", "histJpsiPT", binN, 0, 2e4)
    ntJpsiMC.Project("histJpsiPT", "Jpsi_PT", "")
    histJpsiPTw = TH1D("histJpsiPTw", "histJpsiPTw", binN, 0, 2e4)
    ntJpsiMC.Project("histJpsiPTw", "Jpsi_PT", cutReweight)

    canvA = TCanvas("canvA", "canvA", 1000, 800)

    canvA.Divide(1,2)
    canvA.cd(1)
    histProtonPCosw.SetLineColor(2)
    histProtonPCosw.Draw()
    histProtonPCos.Draw("SAME")

    canvA.cd(2)
#    gPad.SetLogy(1)
    histJpsiPTw.SetLineColor(1)
    histJpsiPTw.Draw()
    histJpsiPT.Draw("SAME")

    #ntJpsiMC.Draw("Jpsi_PT:ProtonM_CosTheta", "")
    canvA.SaveAs("reweight_MC.pdf")

    print "unweighted %i\n"%histProtonPCos.GetSumOfWeights()
    print "weighted %i\n"%histProtonPCosw.GetSumOfWeights()

    histEff = TH1D("histEff","histEff", 4, ptBins)

    for iPT in range(4):
        NW = ntJpsiMC.GetEntries(cutReweight+"&& Jpsi_PT>%s && Jpsi_PT<%s"%(ptBins[iPT], ptBins[iPT+1]))
        N = ntJpsiMC.GetEntries("Jpsi_PT>%s && Jpsi_PT<%s"%(ptBins[iPT], ptBins[iPT+1]))
        histEff.SetBinContent( iPT+1,1-N/float(NW))
    
    canv = TCanvas("canv", "canv", 1000, 800)
    canv.cd(1)
    histEff.Draw()
    canv.SaveAs("eff.pdf")
    
reweight()