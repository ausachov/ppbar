#simultaneous fit to MC eta_c and J/psi tz

from ROOT import *
homeDir = "/users/LHCb/zhovkovska/scripts/"

def getData_s(w, iPT=0):
  
    nPTBins = 5
    pt = (['6500', '8000'], ['8000', '10000'], ['10000', '12000'], ['12000', '14000'], ['14000', '18000'])
    
    ntEtac_Prompt =  TChain("DecayTree")
    ntEtac_FromB =  TChain("DecayTree")
    ntJpsi_Prompt =  TChain("DecayTree")
    ntJpsi_FromB =  TChain("DecayTree")

    #-----------old MC--------------------------------------------------------    
    #ntEtac_Prompt.Add(homeDir+'MC/newMC/Etac/EtacDiProton_MC_2016_AddBr.root')
    #ntEtac_Prompt.Add(homeDir+'MC/newMC/Etac/EtacDiProton_MC_2015_AddBr.root')
    #ntJpsi_Prompt.Add(homeDir+'MC/newMC/Jpsi/JpsiDiProton_MC_2016_AddBr.root')
    #ntJpsi_Prompt.Add(homeDir+'MC/newMC/Jpsi/JpsiDiProton_MC_2015_AddBr.root')
    #ntEtac_FromB.Add(homeDir+'MC/newMC/incl_b/EtacDiProton_MC_2015_incl_b_AddBr.root')
    #ntEtac_FromB.Add(homeDir+'MC/newMC/incl_b/EtacDiProton_MC_2016_incl_b_AddBr.root')
    #ntJpsi_FromB.Add(homeDir+'MC/newMC/Jpsi/JpsiDiProton_MC_2016_AddBr.root')
    #ntJpsi_FromB.Add(homeDir+'MC/newMC/Jpsi/JpsiDiProton_MC_2015_AddBr.root')


    #-----------new MC--------------------------------------------------------    
    ntEtac_Prompt.Add(homeDir+'MC/newMC_spring/Etac_Pr_all_l0TOS_allSelected_AddBr.root')
    ntJpsi_Prompt.Add(homeDir+'MC/newMC_spring/Jpsi_Pr_all_l0TOS_allSelected_AddBr.root')
    ntEtac_FromB.Add(homeDir+'MC/newMC_spring/incl_b_Pr_all_l0TOS_allSelected_AddBr.root')
    ntJpsi_FromB.Add(homeDir+'MC/newMC_spring/Jpsi_Pr_all_l0TOS_allSelected_AddBr.root')

    
    treeEtac_Prompt = TTree()
    treeJpsi_Prompt = TTree()
    treeEtac_FromB = TTree()
    treeJpsi_FromB = TTree()
        
    varChi2 = "( abs(Jpsi_TRUEPV_X-PVX)/(PVXERR) + abs(Jpsi_TRUEPV_Y-PVY)/(PVYERR) + abs(Jpsi_TRUEPV_Z-PVZ)/(PVZERR))" 
    
    if(iPT == 0):

        #cutJpsi_Dist_CHI2 = 'Jpsi_Dist_CHI2 < 7'
        cutJpsi_Dist_CHI2 = varChi2 + ' < 7'

        #treeEtac = ntEtac.CopyTree("prompt and TMath.Abs(Jpsi_MC_MOTHER_ID) != 4 and Jpsi_MC_MOTHER_ID != 0")
        treeEtac_Prompt = ntEtac_Prompt.CopyTree("prompt" + '&&' + cutJpsi_Dist_CHI2)
        treeJpsi_Prompt = ntJpsi_Prompt.CopyTree("prompt" + '&&' + cutJpsi_Dist_CHI2)
        treeEtac_FromB = ntEtac_FromB.CopyTree("sec" + '&&' + cutJpsi_Dist_CHI2)
        treeJpsi_FromB = ntJpsi_FromB.CopyTree("sec" + '&&' + cutJpsi_Dist_CHI2)

    elif ( iPT <= nPTBins ):
        
        cutJpsiPtL = 'Jpsi_PT > {}'.format(pt[iPT-1][0])
        cutJpsiPtR = 'Jpsi_PT < {}'.format(pt[iPT-1][1])
        #cutJpsi_Dist_CHI2 = 'Jpsi_Dist_CHI2 < 7'
        cutJpsi_Dist_CHI2 = varChi2 + ' < 7'
        
        treeEtac_Prompt = ntEtac_Prompt.CopyTree("prompt"  + '&&' +  cutJpsiPtL  + '&&' +  cutJpsiPtR + '&&' + cutJpsi_Dist_CHI2)
        treeJpsi_Prompt = ntJpsi_Prompt.CopyTree("prompt"  + '&&' +  cutJpsiPtL  + '&&' +  cutJpsiPtR + '&&' + cutJpsi_Dist_CHI2)
        treeEtac_FromB = ntEtac_FromB.CopyTree('sec' + '&&' +  cutJpsiPtL  + '&&' +  cutJpsiPtR + '&&' + cutJpsi_Dist_CHI2)
        treeJpsi_FromB = ntJpsi_FromB.CopyTree('sec' + '&&' +  cutJpsiPtL  + '&&' +  cutJpsiPtR + '&&' + cutJpsi_Dist_CHI2)
        
    else: 
        print ("Incorrect number of PT bin {}".format(iPT))

    
    Jpsi_Tz_res = RooRealVar("Jpsi_Tz_res","Jpsi_Tz_res",-0.45,0.45) 
    dsEtac_Prompt = RooDataSet("dsEtac_Prompt","dsEtac_Prompt",treeEtac_Prompt,RooArgSet(Jpsi_Tz_res))
    dsJpsi_Prompt = RooDataSet("dsJpsi_Prompt","dsJpsi_Prompt",treeJpsi_Prompt,RooArgSet(Jpsi_Tz_res))
    dsEtac_FromB = RooDataSet("dsEtac_FromB","dsEtac_FromB",treeEtac_FromB,RooArgSet(Jpsi_Tz_res))
    dsJpsi_FromB = RooDataSet("dsJpsi_FromB","dsJpsi_FromB",treeJpsi_FromB,RooArgSet(Jpsi_Tz_res))
    getattr(w,'import')(dsEtac_Prompt)
    getattr(w,'import')(dsJpsi_Prompt)
    getattr(w,'import')(dsEtac_FromB)
    getattr(w,'import')(dsJpsi_FromB)

    print ("DATAGET COMPLETED")


def getTail_s(w):

    ntEtac = TChain("DecayTree")
    ntJpsi= TChain("DecayTree")
    ntEtac.Add(homeDir+"MC/Etac_MC_Tz_Tail.root")
    ntJpsi.Add(homeDir+"MC/Jpsi_MC_Tz_Tail.root")
    
    
    treeJpsi = ntJpsi.CopyTree("Jpsi_Tz >-100.0")
    treeEtac = ntEtac.CopyTree("Jpsi_Tz >-100.0")
    
    Jpsi_Tz_res = RooRealVar ("Jpsi_Tz_res","Jpsi_Tz",-1.5,1.5) 
    dsTailJpsi = RooDataSet("dsTailJpsi","dsTailJpsi",treeJpsi,RooArgSet(Jpsi_Tz_res))
    dsTailEtac = RooDataSet("dsTailEtac","dsTailEtac",treeEtac,RooArgSet(Jpsi_Tz_res))
  
    getattr(w,'import')(dsTailEtac)
    getattr(w,'import')(dsTailJpsi)

    print ("TAILGET COMPLETED")



#def getData_s(w):

    #nt = TChain("tree")
    #nt.Add("Jpsi_Cuts/Tz_MC_cuted.root")

    #Jpsi_Tz_res = RooRealVar("Jpsi_Tz_res","Jpsi_Tz_res",-10.,10.) 

    #tree = nt.CopyTree("prompt")
    #dsJpsi = RooDataSet("dsJpsi","dsJpsi",tree,RooArgSet(Jpsi_Tz_res))
    #getattr(w,'import')(dsJpsi)
    #tree = nt.CopyTree("sec")
    #dsEtac = RooDataSet("dsEtac","dsEtac",tree,RooArgSet(Jpsi_Tz_res))
    #getattr(w,'import')(dsEtac)




def fillWorkspace(w):

    Jpsi_Tz_res = w.var("Jpsi_Tz_res")
#     Jpsi_Tz_res.setBins(400)
#    Jpsi_Tz_res.setRange("SB1",-40.,-3.)
#    Jpsi_Tz_res.setRange("SB2", 3., 40.)

    # Resolution function

    beta = RooRealVar("beta","Gaussian's fraction", 0.77, 0., 1.)
#    beta.setVal(1.0)    beta.setConstant(True)
#    RooRealVar beta2("beta2","Gaussian's fraction", 0.1, 0., 1.)
    S1 = RooRealVar("S1","Scale factor 1", 5.36, 0.1, 1.e3)
#    S1.setConstant(True)
    rS = RooRealVar("rS","rS", 3.52, 1.2, 5.e1)
    #rS.setConstant(True)
    S2 = RooFormulaVar("S2","Scale factor 2", "@0*@1",RooArgList(S1,rS))
    mu = RooRealVar("mu","bias of Tz", -1.0e-3, -5.e-2, 5.e-2)
    sigma = RooRealVar("sigma","error of Tz", 0.01)


    sigmaS1 = RooFormulaVar("sigmaS1","S1*sigma","@0*@1",RooArgList(S1,sigma))
    sigmaS2 = RooFormulaVar("sigmaS2","S2*sigma","@0*@1",RooArgList(S2,sigma))

    
    # Signal
    
    g_1 = RooGaussian("g_1","gauss_1 PDF", Jpsi_Tz_res, mu,  sigmaS1)
    g_2 = RooGaussian("g_2","gauss_2 PDF", Jpsi_Tz_res, mu,  sigmaS2)

    resolutionM = RooAddModel("resolutionM","b*g1+(1-b)*g2",RooArgList(g_1,g_2),RooArgList(beta))

    #prompt = RooAddModel("prompt","b*g1+(1-b)*g2",RooArgList(g_1,g_2),RooArgList(beta))

    
    
    gM_1 = RooGaussModel("gM_1","gauss_1 PDF", Jpsi_Tz_res, mu,  sigmaS1)
    gM_2 = RooGaussModel("gM_2","gauss_2 PDF", Jpsi_Tz_res, mu,  sigmaS2)

    fromB = RooAddModel("fromB","b*g1+(1-b)*g2",RooArgList(gM_1,gM_2),RooArgList(beta))
    
    #tauB = RooRealVar("tauB","Expo index", 1.343, 1.0, 10.)
    #fromB = RooDecay("fromB","Jpsi secondary" ,Jpsi_Tz_res, tauB, resolutionM,RooDecay.SingleSided) 


    # Tail

    getTail_s(w)
    
    dataTailJpsi = w.data("dsTailJpsi")
    dataTailEtac = w.data("dsTailEtac")
    tailJpsi = RooKeysPdf("tailJpsi","kestPdf",Jpsi_Tz_res,RooDataSet(dataTailJpsi),RooKeysPdf.MirrorBoth)
    tailEtac = RooKeysPdf("tailEtac","kestPdf",Jpsi_Tz_res,RooDataSet(dataTailEtac),RooKeysPdf.MirrorBoth)
    #tailJpsi.Print ("v")
    #tailEtac.Print ("v")
    #input('')
    


#     NJpsi = RooRealVar("NJpsi","num of Jpsi",81994/*,7.9e4,8.5e4*/)
    NJpsiPr = RooRealVar("NJpsiPr","num of Jpsi prompt",1e5,1e1,1.e+6)
    NJpsiB = RooRealVar("NJpsiB","num of Jpsi from-b",1e4,10,1.e+6)
    NJpsiTPr = RooRealVar("NJpsiTPr","Jpsi tail",5e1,0,1.e+3)
    NJpsiTB = RooRealVar("NJpsiTB","Jpsi tail",5e1,0,1.e+3)

    NEtacPr = RooRealVar("NEtacPr","num of etac prompt",1e5,1e1,1.e+6)
    NEtacB = RooRealVar("NEtacB","num of etac from-b",1e4,10,1.e+6)
    NEtacTPr = RooRealVar("NEtacTPr","etac tail",5e1,0,1.e+3)
    NEtacTB = RooRealVar("NEtacTB","etac tail",5e1,0,1.e+3)

#    Np = RooFormulaVar("NJpsi","num of Jpsi","@0-@1",RooArgList(NJpsi,Nb))
#    JpsiConstr = RooGaussian("JpsiConstr","JpsiConstr",NJpsi,RooConst(81994),RooConst(2691))


    #modelSignalJpsi_Prompt = RooAddPdf("modelSignalJpsi_Prompt","signal", RooArgList( resolutionM, tailJpsi), RooArgList( NJpsiPr, NJpsiTPr))
    #modelSignalEtac_Prompt = RooAddPdf("modelSignalEtac_Prompt","signal", RooArgList( resolutionM, tailEtac), RooArgList( NEtacPr, NEtacTPr))
    #modelSignalJpsi_FromB = RooAddPdf("modelSignalJpsi_FromB","signal", RooArgList( resolutionM, tailJpsi), RooArgList( NJpsiB, NJpsiTB))
    #modelSignalEtac_FromB = RooAddPdf("modelSignalEtac_FromB","signal", RooArgList( resolutionM, tailEtac), RooArgList( NEtacB, NEtacTB))

    modelSignalJpsi_Prompt = RooAddPdf("modelSignalJpsi_Prompt","signal", RooArgList( resolutionM), RooArgList( NJpsiPr))
    modelSignalEtac_Prompt = RooAddPdf("modelSignalEtac_Prompt","signal", RooArgList( resolutionM), RooArgList( NEtacPr))
    modelSignalJpsi_FromB = RooAddPdf("modelSignalJpsi_FromB","signal", RooArgList( resolutionM), RooArgList( NJpsiB))
    modelSignalEtac_FromB = RooAddPdf("modelSignalEtac_FromB","signal", RooArgList( resolutionM), RooArgList( NEtacB))

    sample = RooCategory("sample","sample") 
    sample.defineType("Jpsi_Prompt") 
    sample.defineType("Etac_Prompt") 
    sample.defineType("Jpsi_FromB") 
    sample.defineType("Etac_FromB") 


    dataJpsi_Prompt = w.data("dsJpsi_Prompt")
    dataEtac_Prompt = w.data("dsEtac_Prompt")
    dataJpsi_FromB = w.data("dsJpsi_FromB")
    dataEtac_FromB = w.data("dsEtac_FromB")

    
    # Construct combined dataset in (x,sample)
    combData = RooDataSet("combData","combined data",RooArgSet(Jpsi_Tz_res), RooFit.Index(sample), RooFit.Import("Jpsi_Prompt",dataJpsi_Prompt), RooFit.Import("Etac_Prompt",dataEtac_Prompt), RooFit.Import("Jpsi_FromB",dataJpsi_FromB), RooFit.Import("Etac_FromB",dataEtac_FromB)) 


    simPdf = RooSimultaneous("simPdf","simultaneous pdf",sample) 

    # Associate model with the physics state and model_ctl with the control state
    simPdf.addPdf(modelSignalJpsi_Prompt,"Jpsi_Prompt")     
    simPdf.addPdf(modelSignalEtac_Prompt,"Etac_Prompt") 
    simPdf.addPdf(modelSignalJpsi_FromB,"Jpsi_FromB")     
    simPdf.addPdf(modelSignalEtac_FromB,"Etac_FromB") 


    getattr(w,'import')(combData, RooFit.RecycleConflictNodes())
    getattr(w,'import')(simPdf, RooFit.RecycleConflictNodes())




def fitData(iPT, empty=False):

    gROOT.Reset()

    w =  RooWorkspace("w",True)

#    getData_h(w)
    getData_s(w,iPT)
    fillWorkspace(w)


    Jpsi_Tz_res = w.var("Jpsi_Tz_res")

    modelJpsi_Prompt = w.pdf("modelSignalJpsi_Prompt")
    modelEtac_Prompt = w.pdf("modelSignalEtac_Prompt")
    modelJpsi_FromB = w.pdf("modelSignalJpsi_FromB")
    modelEtac_FromB = w.pdf("modelSignalEtac_FromB")

    sample = w.cat("sample")
    simPdf = w.pdf("simPdf")
    combData = w.data("combData")


    dataJpsi_Prompt = w.data("dsJpsi_Prompt")
    dataEtac_Prompt = w.data("dsEtac_Prompt")
    dataJpsi_FromB = w.data("dsJpsi_FromB")
    dataEtac_FromB = w.data("dsEtac_FromB")


    params = simPdf.getParameters(RooArgSet(Jpsi_Tz_res)) 
    w.defineSet("parameters",params) 
    w.defineSet("observables",RooArgSet(Jpsi_Tz_res)) 
    params.setAttribAll("StoreError",True)


    #if (iPT != 0):

      #f =  TFile(homeDir+"Results/MC/TzFit/MC_TzRes_2016_wksp.root","READ") 
      #wMC = f.Get("w")
      #f.Close()
      
      #w.var("beta").setVal(wMC.var("beta").getValV())
      #w.var("rS").setVal(wMC.var("rS").getValV())
      
      #w.var("beta").setConstant(True)
      #w.var("rS").setConstant(True)

    
 
    simPdf.fitTo(combData, RooFit.Extended(), RooFit.Minos(True), RooFit.Save(True))     
    r = simPdf.fitTo(combData, RooFit.Extended(), RooFit.Minos(True), RooFit.Save(True)) 
    

    ## Construct plot frame
    name = RooFit.Name

    frame = []
    for i in range(4):
        frame.append(Jpsi_Tz_res.frame(RooFit.Title('')))

    #frame[0] = Jpsi_Tz_res.frame(RooFit.Title("J/#psi t_{z} prompt")) 
    #frame[1] = Jpsi_Tz_res.frame(RooFit.Title("#eta_{c}(1S) t_{z} prompt")) 
    #frame[2] = Jpsi_Tz_res.frame(RooFit.Title("J/#psi t_{z} from-b")) 
    #frame[3] = Jpsi_Tz_res.frame(RooFit.Title("#eta_{c}(1S) t_{z} from-b")) 


    dataJpsi_Prompt.plotOn(frame[0], name('histJpsi_Prompt'))
    dataEtac_Prompt.plotOn(frame[1], name('histEtac_Prompt'))
    dataJpsi_FromB.plotOn(frame[2], name('histJpsi_FromB'))
    dataEtac_FromB.plotOn(frame[3], name('histEtac_FromB'))


    chi2Jpsi_Prompt = 0.
    chi2Etac_Prompt = 0.
    chi2Jpsi_FromB = 0.
    chi2Etac_FromB = 0.
    
    resolutionM = w.pdf("resolutionM")
    #prompt = w.pdf("prompt")
    #fromB = w.pdf("fromB")
    #tailJpsi = w.pdf("tailJpsi")
    #tailEtac = w.pdf("tailEtac")

    gROOT.ProcessLine('gStyle->SetOptTitle(0)')
    if empty:
        gROOT.ProcessLine('gStyle->SetOptStat(0)')

    #modelJpsi_Prompt.paramOn(frame[0],RooFit.Layout(0.68,0.99,0.99))
    #frame[0].getAttText().SetTextSize(0.027)
    modelJpsi_Prompt.plotOn( frame[0], RooFit.Normalization(1.0,RooAbsReal.RelativeExpected), name('pdfJpsi_Prompt'))
    chi2Jpsi_Prompt = frame[0].chiSquare()
    #modelJpsi_Prompt.plotOn(frame[0],RooFit.Components(tailJpsi), RooFit.FillStyle(3005), RooFit.FillColor(kMagenta), RooFit.DrawOption("F"), RooFit.Normalization(1.0,RooAbsReal.RelativeExpected))
    
    #modelEtac_Prompt.paramOn(frame[1],RooFit.Layout(0.68,0.99,0.99))
    #frame[1].getAttText().SetTextSize(0.027)
    modelEtac_Prompt.plotOn(frame[1], RooFit.Normalization(1.0,RooAbsReal.RelativeExpected), name('pdfEtac_Prompt'))
    chi2Etac_Prompt = frame[1].chiSquare()
    #modelEtac.plotOn(frame[1], RooFit.Components(tailEtac), RooFit.FillStyle(3005), RooFit.FillColor(kMagenta), RooFit.DrawOption("F"), RooFit.Normalization(1.0,RooAbsReal.RelativeExpected))
    
    #modelJpsi_FromB.paramOn(frame[2],RooFit.Layout(0.68,0.99,0.99))
    #frame[2].getAttText().SetTextSize(0.027)
    modelJpsi_FromB.plotOn(frame[2], RooFit.Normalization(1.0,RooAbsReal.RelativeExpected), name('pdfJpsi_FromB'))
    chi2Jpsi_FromB = frame[2].chiSquare()
    #modelJpsi.plotOn(frame[0],RooFit.Components(tailJpsi), RooFit.FillStyle(3005), RooFit.FillColor(kMagenta), RooFit.DrawOption("F"), RooFit.Normalization(1.0,RooAbsReal.RelativeExpected))
    
    #modelEtac_FromB.paramOn(frame[3],RooFit.Layout(0.68,0.99,0.99))    
    #frame[3].getAttText().SetTextSize(0.027)
    modelEtac_FromB.plotOn(frame[3], RooFit.Normalization(1.0,RooAbsReal.RelativeExpected), name('pdfEtac_FromB'))
    chi2Etac_FromB = frame[3].chiSquare()
    #modelEtac.plotOn(frame[1], RooFit.Components(tailEtac), RooFit.FillStyle(3005), RooFit.FillColor(kMagenta), RooFit.DrawOption("F"), RooFit.Normalization(1.0,RooAbsReal.RelativeExpected))



    hPull_Jpsi_Prompt = frame[0].pullHist('histJpsi_Prompt','pdfJpsi_Prompt',True)    
    frame_pull_1 = Jpsi_Tz_res.frame(RooFit.Title("Pull Distribution"), RooFit.Bins(200)) 
    for ii in range(hPull_Jpsi_Prompt.GetN()):
        
        hPull_Jpsi_Prompt.SetPointEYlow(ii,0)
        hPull_Jpsi_Prompt.SetPointEYhigh(ii,0)

    frame_pull_1.addPlotable(hPull_Jpsi_Prompt,"B") 

    hPull_Etac_Prompt = frame[1].pullHist('histEtac_Prompt','pdfEtac_Prompt',True)    
    frame_pull_2 = Jpsi_Tz_res.frame(RooFit.Title("Pull Distribution"), RooFit.Bins(200)) 
    for ii in range(hPull_Etac_Prompt.GetN()):
        
        hPull_Etac_Prompt.SetPointEYlow(ii,0)
        hPull_Etac_Prompt.SetPointEYhigh(ii,0)

    frame_pull_2.addPlotable(hPull_Etac_Prompt,"B") 

    hPull_Jpsi_FromB = frame[2].pullHist('histJpsi_FromB','pdfJpsi_FromB',True)    
    frame_pull_3 = Jpsi_Tz_res.frame(RooFit.Title("Pull Distribution"), RooFit.Bins(200)) 
    for ii in range(hPull_Jpsi_FromB.GetN()):
        
        hPull_Jpsi_FromB.SetPointEYlow(ii,0)
        hPull_Jpsi_FromB.SetPointEYhigh(ii,0)

    frame_pull_3.addPlotable(hPull_Jpsi_FromB,"B") 

    hPull_Etac_FromB = frame[3].pullHist('histEtac_FromB','pdfEtac_FromB',True)    
    frame_pull_4 = Jpsi_Tz_res.frame(RooFit.Title("Pull Distribution"), RooFit.Bins(200)) 
    for ii in range(hPull_Etac_FromB.GetN()):
        
        hPull_Etac_FromB.SetPointEYlow(ii,0)
        hPull_Etac_FromB.SetPointEYhigh(ii,0)

    frame_pull_4.addPlotable(hPull_Etac_FromB,"B") 


    #gROOT.ProcessLine('gStyle->SetOptStat(000000000)')
    #gROOT.ProcessLine('gStyle->SetOptTitle(0)')


    c = TCanvas('Tz_Fit','Tz Fit',1200,800)
    c.Divide(2,2)
    #c.cd(1).SetPad(.005, .505, .495, .995)
    ##gPad.SetLeftMargin(0.15),  frame[0].GetXaxis().SetTitle('(Tz - TRUE_Tz)_{p #bar{p}} / [fs]'),  
    #frame[0].Draw()
    #frame[0].SetMinimum(1.)
    #gPad.SetLogy()
    #c.cd(2).SetPad(.005, .005, .495, .495)
    ##gPad.SetLeftMargin(0.15),  frame[1].GetXaxis().SetTitle('(Tz - TRUE_Tz)_{p #bar{p}} / [fs]'),  
    #frame[1].Draw()
    #frame[1].SetMinimum(1.)
    #gPad.SetLogy()
    #c.cd(3).SetPad(.495, .505, .995, .995)
    ##gPad.SetLeftMargin(0.15),  frame[2].GetXaxis().SetTitle('(Tz - TRUE_Tz)_{p #bar{p}} / [fs]'),  
    #frame[2].Draw()
    #frame[2].SetMinimum(1.)
    #gPad.SetLogy()
    #c.cd(4).SetPad(.495, .005, .995, .495)
    ##gPad.SetLeftMargin(0.15),  frame[3].GetXaxis().SetTitle('(Tz - TRUE_Tz)_{p #bar{p}} / [fs]'),  
    #frame[3].Draw()
    #frame[3].SetMinimum(1.)
    #gPad.SetLogy()
    
    names = ["#eta_{c} prompt", "J/#psi prompt", "#eta_{c} from-b", "J/#psi from-b"]
    texMC = TLatex()
    texMC.SetNDC()
    
    for iC in range(4):
        pad = c.cd(iC+1)
        xl = pad.GetXlowNDC(); xh = xl + pad.GetWNDC()
        yl = pad.GetYlowNDC(); yh = yl + pad.GetHNDC()
        pad.SetPad(xl+0.005,yl+0.005,xh-0.005,yh-0.005)
        pad.SetLeftMargin(0.15);  pad.SetBottomMargin(0.15);  frame[iC].GetXaxis().SetTitle('(t_{z} - t^{TRUE}_{z})_{p#bar{p}}, ps')
        frame[iC].GetXaxis().SetTitleSize(0.06)
        frame[iC].GetYaxis().SetTitleSize(0.06)
        frame[iC].GetXaxis().SetTitleOffset(1.10)
        frame[iC].GetYaxis().SetTitleOffset(1.10)
        frame[iC].GetXaxis().SetTitleFont(12)
        frame[iC].GetYaxis().SetTitleFont(12)
        frame[iC].GetXaxis().SetLabelSize(0.05)
        frame[iC].GetYaxis().SetLabelSize(0.05)
        frame[iC].GetXaxis().SetLabelFont(62)
        frame[iC].GetYaxis().SetLabelFont(62)
        frame[iC].Draw()
        #frame[iC].SetMaximum(5.e3)
        frame[iC].SetMinimum(0.1)
        texMC.DrawLatex(0.6, 0.80, "LHCb simulation")
        texMC.DrawLatex(0.6, 0.75, "#sqrt{s}=13 TeV")
        texMC.DrawLatex(0.25, 0.75, names[iC])
        #pad.SetLogy()



    cPull = TCanvas('Tz_Fit_Pulls','Tz Fit',1200,600)
    cPull.Divide(2,2)
    cPull.cd(1).SetPad(.005, .505, .495, .995)
    #gPad.SetLeftMargin(0.15),  frame_pull_1.GetXaxis().SetTitle('(Tz - TRUE_Tz)_{p #bar{p}} / [fs]'),  
    frame_pull_1.Draw()
    frame_pull_1.SetMinimum(-7.)
    frame_pull_1.SetMaximum(+7.)
    #gPad.SetLogy()
    cPull.cd(2).SetPad(.005, .005, .495, .495)
    #gPad.SetLeftMargin(0.15),  frame_pull_2.GetXaxis().SetTitle('(Tz - TRUE_Tz)_{p #bar{p}} / [fs]'),  
    frame_pull_2.Draw()
    frame_pull_2.SetMinimum(-7.)
    frame_pull_2.SetMaximum(+7.)
    #gPad.SetLogy()
    cPull.cd(3).SetPad(.495, .505, .995, .995)
    #gPad.SetLeftMargin(0.15),  frame_pull_3.GetXaxis().SetTitle('(Tz - TRUE_Tz)_{p #bar{p}} / [fs]'),  
    frame_pull_3.Draw()
    frame_pull_3.SetMinimum(-7.)
    frame_pull_3.SetMaximum(+7.)
    #gPad.SetLogy()
    cPull.cd(4).SetPad(.495, .005, .995, .495)
    #gPad.SetLeftMargin(0.15),  frame_pull_4.GetXaxis().SetTitle('(Tz - TRUE_Tz)_{p #bar{p}} / [fs]'),  
    frame_pull_4.Draw()
    frame_pull_4.SetMinimum(-7.)
    frame_pull_4.SetMaximum(+7.)
    #gPad.SetLogy()


    ## C o n s t r u c t   p l a i n   l i k e l i h o o d
    ## ---------------------------------------------------
    
    #rS = w.var("rS")
    
    ## Construct unbinned likelihood
    #nll = simPdf.createNLL(combData,RooFit.NumCPU(48)) 
    
    ## Minimize likelihood w.r.t all parameters before making plots
    #RooMinuit(nll).migrad() 
    
    ## Plot likelihood scan frac 
    #frameRS = rS.frame(RooFit.Bins(10),RooFit.Range(1.00,30.00),RooFit.Title("LL and profileLL in r_{S} ")) 
##     nll.plotOn(frame[0],ShiftToZero()) 
        
    ## The profile likelihood estimator on nll for frac will minimize nll w.r.t
    ## all floating parameters except frac for each evaluation
    
    #pll_gamma = nll.createProfile(RooArgSet(rS)) 
    
    ## Plot the profile likelihood in frac
    #pll_gamma.plotOn(frameRS,RooFit.LineColor(2)) 
    
    ## Adjust frame maximum for visual clarity
    #frameRS.SetMinimum(0) 
    
    #cNLL = TCanvas("rf605_profilell","rf605_profilell",800, 400)
    #cNLL.cd(),  gPad.SetLeftMargin(0.15),  frameRS.GetYaxis().SetTitleOffset(1.4),  frameRS.Draw() 

    #cNLL.SaveAs("rS_NLL_PT%s.pdf"%iPT)

    nameTxt = ''
    nameRoot = ''
    nameWksp = ''
    namePic1 = ''
    namePic2 = ''
    add = "_newMC"
    
    if(iPT == 0):

        nameTxt = homeDir+"Results/MC/TzFit/fit_TzRes"+add+".txt"
        nameWksp = homeDir+"Results/MC/TzFit/MC_TzRes"+add+"_2016_wksp.root"
        nameRoot = homeDir+"Results/MC/TzFit/MC_TzRes"+add+"_Fit_plot.root"
        namePic  = homeDir+"Results/MC/TzFit/MC_TzRes"+add+".pdf"
        namePicP = homeDir+"Results/MC/TzFit/MC_TzRes"+add+"_Pulls.pdf"
        #namePic1 = homeDir+"Results/MC/TzFit/MC_TzRes_Jpsi.pdf"
        #namePic2 = homeDir+"Results/MC/TzFit/MC_TzRes_Etac.pdf"

    else:

        nameTxt = homeDir+"Results/MC/TzFit/fit_TzRes"+add+"_PT{}.txt".format(iPT)
        nameWksp = homeDir+"Results/MC/TzFit/MC_TzRes"+add+"_2016_wksp_PT{}.root".format(iPT)
        nameRoot = homeDir+"Results/MC/TzFit/MC_TzRes"+add+"_Fit_plot_PT{}.root".format(iPT)
        namePic = homeDir+"Results/MC/TzFit/MC_TzRes"+add+"_PT{}.pdf".format(iPT)
        namePicP = homeDir+"Results/MC/TzFit/MC_TzRes"+add+"_Pulls_PT{}.pdf".format(iPT)
        #namePic1 = homeDir+"Results/MC/TzFit/MC_TzRes_Jpsi_PT{}.pdf".format(iPT)
        #namePic2 = homeDir+"Results/MC/TzFit/MC_TzRes_Etac_PT{}.pdf".format(iPT)

    
    import os, sys 
    save = os.dup( sys.stdout.fileno() ) 
    newout = file(nameTxt, 'w' ) 
    os.dup2( newout.fileno(), sys.stdout.fileno() ) 
    print " chi2Jpsi_Prompt =  %5.4f,    chi2Etac_Prompt =  %5.4f \n"%( chi2Jpsi_Prompt,  chi2Etac_Prompt)
    print " chi2Jpsi_FromB =  %5.4f,     chi2Etac_FromB =  %5.4f \n\n"%( chi2Jpsi_FromB,  chi2Etac_FromB)
    r.Print("v") 
    r.correlationMatrix().Print()
    os.dup2( save, sys.stdout.fileno() ) 
    newout.close()

    
    w.writeToFile(nameWksp)
    fFit =  TFile (nameRoot,"RECREATE")
    #fFit =  TFile (nameRoot,"UPDATE")
    c.Write("")
    cPull.Write("")
    #c1.Write("")
    #c2.Write("")
    fFit.Close()
    
    c.SaveAs(namePic)
    cPull.SaveAs(namePicP)
    #c1.SaveAs(namePic1)
    #c2.SaveAs(namePic2)
        
    w.Print("v")
    r.floatParsFinal().Print()
    r.correlationMatrix().Print()
    #     std.cin.get()
    
    

def MC_TzRes_Fit():

    nPTBins = 5
    fitData(0)
    for iPT in range(1, nPTBins+1):
        fitData(iPT)


#w =  RooWorkspace("w",True)
#getData_s(w, 0)
#getTail_s(w)
MC_TzRes_Fit()
#fitData(4)