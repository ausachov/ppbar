#simultaneous fit to MC eta_c and J/psi tz from-b
#to extract tau_B

from ROOT import *

homeDir = "/users/LHCb/zhovkovska/"

def getData_s(w, iPT=0):
  
    nPTBins = 7
    #pt = (['0', '4000'], ['4000', '7000'], ['7000', '10000'], ['10000', '13000'], ['13000', '16000'],  ['16000', '20000'], ['20000', '30000'])
    pt = ( ['6500', '8000'], ['8000', '10000'], ['10000', '12000'], ['12000', '14000'], ['14000', '16000'], ['16000', '20000'], ['20000', '30000'])
    
    #ntEtac =  TChain("MCDecayTree")
    #ntJpsi =  TChain("MCDecayTree")
    
    #ntEtac.Add('~/scripts/MC/newMC/incl_b/EtacDiProton_MC_Generator_2015_incl_b_AddBr.root')
    #ntEtac.Add('~/scripts/MC/newMC/incl_b/EtacDiProton_MC_Generator_2016_incl_b_AddBr.root')
    #ntJpsi.Add('~/scripts/MC/newMC/Jpsi/JpsiDiProton_MC_Generator_2016_AddBr.root')
    #ntJpsi.Add('~/scripts/MC/newMC/Jpsi/JpsiDiProton_MC_Generator_2015_AddBr.root')

    ntEtac =  TChain("DecayTree")
    ntJpsi =  TChain("DecayTree")

    #-----------old MC--------------------------------------------------------    
    #ntEtac.Add('~/scripts/MC/newMC/incl_b/EtacDiProton_MC_2015_incl_b_AddBr.root')
    #ntEtac.Add('~/scripts/MC/newMC/incl_b/EtacDiProton_MC_2016_incl_b_AddBr.root')
    #ntJpsi.Add('~/scripts/MC/newMC/Jpsi/JpsiDiProton_MC_2016_AddBr.root')
    #ntJpsi.Add('~/scripts/MC/newMC/Jpsi/JpsiDiProton_MC_2015_AddBr.root')


    #-----------new MC--------------------------------------------------------    
    ntEtac.Add('~/scripts/MC/newMC_spring/incl_b_Pr_all_l0TOS_allSelected_AddBr.root')
    ntJpsi.Add('~/scripts/MC/newMC_spring/Jpsi_Pr_all_l0TOS_allSelected_AddBr.root')
    
    treeEtac = TTree()
    treeJpsi = TTree()
    
    
    cutApriori = "       TMath::Sqrt(ProtonP_TRUEP_X*ProtonP_TRUEP_X+ProtonP_TRUEP_Y*ProtonP_TRUEP_Y+ProtonP_TRUEP_Z*ProtonP_TRUEP_Z)>12.5e3 && TMath::Sqrt(ProtonM_TRUEP_X*ProtonM_TRUEP_X+ProtonM_TRUEP_Y*ProtonM_TRUEP_Y+ProtonM_TRUEP_Z*ProtonM_TRUEP_Z)>12.5e3 && \
                         ProtonP_TRUEPT>2000 && ProtonM_TRUEPT>2000 && \
                         ProtonP_TRUEPT/TMath::Sqrt(ProtonP_TRUEP_X*ProtonP_TRUEP_X+ProtonP_TRUEP_Y*ProtonP_TRUEP_Y+ProtonP_TRUEP_Z*ProtonP_TRUEP_Z)>0.0366 && ProtonM_TRUEPT/TMath::Sqrt(ProtonM_TRUEP_X*ProtonM_TRUEP_X+ProtonM_TRUEP_Y*ProtonM_TRUEP_Y+ProtonM_TRUEP_Z*ProtonM_TRUEP_Z)>0.0366 && \
                         Jpsi_TRUEETA>2 && Jpsi_TRUEETA<4.5"

    if(iPT == 0):

        cutJpsi_Dist_CHI2 = 'Jpsi_Dist_CHI2 < 7'

        #treeEtac = ntEtac.CopyTree("prompt and TMath.Abs(Jpsi_MC_MOTHER_ID) != 4 and Jpsi_MC_MOTHER_ID != 0")
        treeEtac = ntEtac.CopyTree("sec" + '&&' + cutJpsi_Dist_CHI2)
        treeJpsi = ntJpsi.CopyTree("sec" + '&&' + cutJpsi_Dist_CHI2)

    elif ( iPT <= nPTBins ):
        
        cutJpsiFromB = 'sec'
        cutJpsiPtL = '(Jpsi_TRUEPT > {})'.format(pt[iPT-1][0])
        cutJpsiPtR = '(Jpsi_TRUEPT < {})'.format(pt[iPT-1][1])
        cutJpsi_Dist_CHI2 = 'Jpsi_Dist_CHI2 < 7'
        
        treeEtac = ntEtac.CopyTree(cutJpsiPtL + '&&' + cutJpsiPtR + '&&' + cutJpsi_Dist_CHI2 + '&&' + cutApriori )
        treeJpsi = ntJpsi.CopyTree(cutJpsiFromB + '&&' + cutJpsiPtL +'&&'+cutJpsiPtR + '&&' + cutJpsi_Dist_CHI2 + '&&' + cutApriori)    
        
    else: 
        print ("Incorrect number of PT bin {}".format(iPT))

    
    Jpsi_TRUE_Tz = RooRealVar("Jpsi_TRUE_Tz","Jpsi_TRUE_Tz",-10.0,10.0) 
    dsEtac = RooDataSet("dsEtac","dsEtac",treeEtac,RooArgSet(Jpsi_TRUE_Tz))
    dsJpsi = RooDataSet("dsJpsi","dsJpsi",treeJpsi,RooArgSet(Jpsi_TRUE_Tz))
    getattr(w,'import')(dsEtac)
    getattr(w,'import')(dsJpsi)

    print ("DATAGET COMPLETED")
  


def getTail_s(w):

    ntEtac = TChain("DecayTree")
    ntJpsi = TChain("DecayTree")
    ntEtac.Add(homeDir+"scripts/MC/Etac_MC_Tz_Tail.root")
    ntJpsi.Add(homeDir+"scripts/MC/Jpsi_MC_Tz_Tail.root")
    
    
    treeJpsi = ntJpsi.CopyTree("Jpsi_Tz >-100.0")
    treeEtac = ntEtac.CopyTree("Jpsi_Tz >-100.0")
    
    Jpsi_TRUE_Tz = RooRealVar ("Jpsi_TRUE_Tz","Jpsi_Tz",-10.,10.) 
    dsTailJpsi = RooDataSet("dsTailJpsi","dsTailJpsi",treeJpsi,RooArgSet(Jpsi_TRUE_Tz))
    dsTailEtac = RooDataSet("dsTailEtac","dsTailEtac",treeEtac,RooArgSet(Jpsi_TRUE_Tz))
  
    getattr(w,'import')(dsTailEtac)
    getattr(w,'import')(dsTailJpsi)

    print ("TAILGET COMPLETED")



def fillWorkspace(w):

    Jpsi_TRUE_Tz = w.var("Jpsi_TRUE_Tz")
#     Jpsi_TRUE_Tz.setBins(400)

    # Resolution function

    beta = RooRealVar("beta","Gaussian's fraction", 0.77, 0., 1.)
#    beta.setVal(1.0)    beta.setConstant(True)
#    RooRealVar beta2("beta2","Gaussian's fraction", 0.1, 0., 1.)
    S1 = RooRealVar("S1","Scale factor 1", 5.36, 0.1, 1.e3)
#    S1.setConstant(True)
    rS = RooRealVar("rS","rS", 2.52, 1.0, 5.e1)
#    rS.setConstant(True)
    S2 = RooFormulaVar("S2","Scale factor 2", "@0*@1",RooArgList(S1,rS))
    mu = RooRealVar("mu","bias of Tz", 0.0)
    sigma = RooRealVar("sigma","error of Tz", 0.01)


    sigmaS1 = RooFormulaVar("sigmaS1","S1*sigma","@0*@1",RooArgList(S1,sigma))
    sigmaS2 = RooFormulaVar("sigmaS2","S2*sigma","@0*@1",RooArgList(S2,sigma))

    
    # Signal
    
    gM_1 = RooGaussModel("gM_1","gauss_1 PDF", Jpsi_TRUE_Tz, mu,  sigmaS1)
    gM_2 = RooGaussModel("gM_2","gauss_2 PDF", Jpsi_TRUE_Tz, mu,  sigmaS2)

    resolutionM = RooAddModel("resolutionM","b*g1+(1-b)*g2",RooArgList(gM_1,gM_2),RooArgList(beta))
    tm = RooTruthModel("tm","truth model", Jpsi_TRUE_Tz) ;
  
    tauB = RooRealVar("tauB","Expo index", 1.343, 0.8, 10.)
    fromB = RooDecay("fromB","Jpsi secondary" ,Jpsi_TRUE_Tz, tauB, tm, RooDecay.SingleSided) 


    # Tail

    getTail_s(w)
    
    dataTailJpsi = w.data("dsTailJpsi")
    dataTailEtac = w.data("dsTailEtac")
    
    
    tailJpsi = RooKeysPdf("tailJpsi","kestPdf",Jpsi_TRUE_Tz,dataTailJpsi,RooKeysPdf.MirrorBoth)
    tailEtac = RooKeysPdf("tailEtac","kestPdf",Jpsi_TRUE_Tz,dataTailEtac,RooKeysPdf.MirrorBoth)
    


#     NJpsi = RooRealVar("NJpsi","num of Jpsi",81994/*,7.9e4,8.5e4*/)
    NJpsiB = RooRealVar("NJpsiB","num of Jpsi from-b",1e4,10,1.e+6)
    NJpsiT = RooRealVar("NJpsiT","Jpsi tail",5e3,0,1.e+6)

    NEtacB = RooRealVar("NEtacB","num of etac from-b",1e4,10,1.e+6)
    NEtacT = RooRealVar("NEtacT","etac tail",5e2,0,1.e+6)

#    Np = RooFormulaVar("NJpsi","num of Jpsi","@0-@1",RooArgList(NJpsi,Nb))
#    JpsiConstr = RooGaussian("JpsiConstr","JpsiConstr",NJpsi,RooConst(81994),RooConst(2691))


    #modelSignalJpsi = RooAddPdf("modelSignalJpsi","signal", RooArgList(  fromB, tailJpsi), RooArgList( NJpsiB, NJpsiT))
    #modelSignalEtac = RooAddPdf("modelSignalEtac","signal", RooArgList(  fromB, tailEtac), RooArgList( NEtacB, NEtacT))

    modelSignalJpsi = RooAddPdf("modelSignalJpsi","signal", RooArgList(  fromB), RooArgList( NJpsiB))
    modelSignalEtac = RooAddPdf("modelSignalEtac","signal", RooArgList(  fromB), RooArgList( NEtacB))

    sample = RooCategory("sample","sample") 
    sample.defineType("Jpsi") 
    sample.defineType("Etac") 


    dataJpsi = w.data("dsJpsi")
    dataEtac = w.data("dsEtac")


    # Construct combined dataset in (x,sample)
    combData = RooDataSet("combData","combined data", RooArgSet(Jpsi_TRUE_Tz),RooFit.Index(sample),RooFit.Import("Jpsi",dataJpsi),RooFit.Import("Etac",dataEtac)) 


    simPdf = RooSimultaneous("simPdf","simultaneous pdf",sample) 

    # Associate model with the physics state and model_ctl with the control state
    simPdf.addPdf(modelSignalJpsi,"Jpsi")     
    simPdf.addPdf(modelSignalEtac,"Etac") 

    Jpsi_TRUE_Tz.setRange("fitRange_Jpsi", 0. , 10.)
    Jpsi_TRUE_Tz.setRange("fitRange_Etac", 1.5, 10.)

    getattr(w,'import')(combData, RooFit.RecycleConflictNodes())
    getattr(w,'import')(simPdf, RooFit.RecycleConflictNodes())




def fitData(iPT):

    gROOT.Reset()

    w =  RooWorkspace("w",True)

#    getData_h(w)
    getData_s(w,iPT)
    fillWorkspace(w)


    Jpsi_TRUE_Tz = w.var("Jpsi_TRUE_Tz")

    modelJpsi = w.pdf("modelSignalJpsi")
    modelEtac = w.pdf("modelSignalEtac")

    sample = w.cat("sample")
    simPdf = w.pdf("simPdf")
    combData = w.data("combData")


    dataJpsi = w.data("dsJpsi")
    dataEtac = w.data("dsEtac")

    params = simPdf.getParameters(RooArgSet(Jpsi_TRUE_Tz)) 
    w.defineSet("parameters",params) 
    w.defineSet("observables",RooArgSet(Jpsi_TRUE_Tz)) 
    params.setAttribAll("StoreError",True)

    wMC =  RooWorkspace("w",True)    

    #if (iPT != 0):

      #f =  TFile(homeDir+"scripts/Results/MC/TzFit/MC_TzRes_2016_wksp_PT{}.root".format(iPT),"READ") 
      #wMC = f.Get("w")
      #f.Close()

    #else:

      #f =  TFile(homeDir+"scripts/Results/MC/TzFit/MC_TzRes_2016_wksp.root","READ") 
      #wMC = f.Get("w")
      #f.Close()
        
    ##w.var("mu").setVal(wMC.var("mu").getValV())
    #w.var("beta").setVal(wMC.var("beta").getValV())
    #w.var("rS").setVal(wMC.var("rS").getValV())
    #w.var("S1").setVal(wMC.var("S1").getValV())

    ##w.var("mu").setConstant(True)
    #w.var("beta").setConstant(True)
    #w.var("rS").setConstant(True)
    #w.var("S1").setConstant(True)

    
    
#    w.var("mu").setConstant(True)
#     w.var("beta").setConstant(True)
#     w.var("S1").setConstant(True)
#    w.var("tauB").setConstant(True)
#    w.var("rS").setConstant(True)
#    w.var("NEtac").setConstant(True)
#    w.var("NJpsi").setConstant(True)

#    simPdf.fitTo(*combData,Extended(), Hesse(True)) 


#    w.var("mu").setConstant(kFALSE)
#     w.var("beta").setConstant(kFALSE)
#     w.var("S1").setConstant(kFALSE)
#    w.var("tauB").setConstant(kFALSE)
#    w.var("rS").setConstant(kFALSE)
#    w.var("NEtac").setConstant(kFALSE)
#    w.var("NJpsi").setConstant(kFALSE)
    simPdf.fitTo(combData, RooFit.Extended(), RooFit.Minos(True), RooFit.Range("fitRange"), RooFit.SplitRange(True)) 
    r = simPdf.fitTo(combData, RooFit.Extended(), RooFit.Minos(True), RooFit.Save(True), RooFit.Range("fitRange"), RooFit.SplitRange(True)) 
    
    #input('')
    ## Construct plot frame

    #frame[0] = Jpsi_TRUE_Tz.frame(RooFit.Title("J/#psi t_{z}")) 
    #frame[1] = Jpsi_TRUE_Tz.frame(RooFit.Title("#eta_{c}(1S) t_{z}")) 

    frame = []
    frame.append(Jpsi_TRUE_Tz.frame())
    frame.append(Jpsi_TRUE_Tz.frame())
    
    dataJpsi.plotOn(frame[0])
    dataEtac.plotOn(frame[1])


    chi2Jpsi = 0.
    chi2Etac = 0.
    fromB = w.pdf("fromB")
    tailJpsi = w.pdf("tailJpsi")
    tailEtac = w.pdf("tailEtac")

    #modelJpsi.paramOn(frame[0],RooFit.Layout(0.68,0.99,0.99))
    #frame[0].getAttText().SetTextSize(0.027)
    modelJpsi.plotOn(frame[0],RooFit.Normalization(1.0,RooAbsReal.RelativeExpected))
    chi2Jpsi = frame[0].chiSquare()
    
    #modelEtac.paramOn(frame[1],RooFit.Layout(0.68,0.99,0.99))
    #frame[1].getAttText().SetTextSize(0.027)
    modelEtac.plotOn(frame[1],  RooFit.Range('fitRange_Etac') )
    chi2Etac = frame[1].chiSquare()
    
    gROOT.ProcessLine('gStyle->SetOptStat(000000000)')
    gROOT.ProcessLine('gStyle->SetOptTitle(0)')

    
    c =  TCanvas("TRUE_Tz","Signal Fit",1200,500)
    c.Divide(2,1,0.001, 0.001)
    #c.cd(1) #.SetPad(.005, .505, .495, .995)
    ##gPad.SetLeftMargin(0.15),  frame[0].GetXaxis().SetTitle('(Tz - TRUE_Tz)_{p #bar{p}} / [fs]'),  
    #frame[0].Draw()
    #frame[0].SetMinimum(1.)
    #gPad.SetLogy()
    #c.cd(2) #.SetPad(.005, .005, .495, .495)
    ##gPad.SetLeftMargin(0.15),  frame[1].GetXaxis().SetTitle('(Tz - TRUE_Tz)_{p #bar{p}} / [fs]'),  
    #frame[1].Draw()
    #frame[1].SetMinimum(1.)
    #gPad.SetLogy()

    names = ["J/#psi from-b","#eta_{c} from-b"]
    leg = []
    texMC = TLatex()
    texMC.SetNDC()
    
    for iC in range(2):
        pad = c.cd(iC+1)
        xl = pad.GetXlowNDC(); xh = xl + pad.GetWNDC()
        yl = pad.GetYlowNDC(); yh = yl + pad.GetHNDC()
        pad.SetPad(xl+0.005,yl+0.005,xh-0.005,yh-0.005)
        pad.SetLeftMargin(0.15);  pad.SetBottomMargin(0.15);  frame[iC].GetXaxis().SetTitle('t^{TRUE}_{z   p#bar{p}} / [ps]')
        frame[iC].GetXaxis().SetTitleSize(0.06)
        frame[iC].GetYaxis().SetTitleSize(0.06)
        frame[iC].GetXaxis().SetTitleOffset(1.10)
        frame[iC].GetYaxis().SetTitleOffset(1.10)
        frame[iC].GetXaxis().SetTitleFont(12)
        frame[iC].GetYaxis().SetTitleFont(12)
        frame[iC].GetXaxis().SetLabelSize(0.05)
        frame[iC].GetYaxis().SetLabelSize(0.05)
        frame[iC].GetXaxis().SetLabelFont(62)
        frame[iC].GetYaxis().SetLabelFont(62)
        frame[iC].Draw()
        #frame[iC].SetMaximum(1.e3)
        frame[iC].SetMinimum(1.0)
        texMC.DrawLatex(0.2, 0.80, "LHCb simulation")
        texMC.DrawLatex(0.2, 0.75, "#sqrt{s}=13 TeV")
        texMC.DrawLatex(0.2, 0.7, names[iC])
        pad.SetLogy()

    
    nameTxt = ''
    nameRoot = ''
    nameWksp = ''
    namePic1 = ''
    namePic2 = ''
    add = "_newMC"
    
    if(iPT == 0):

        nameTxt = homeDir+"scripts/Results/MC/TzFit/tauFit/fit_TRUE"+add+"_Tz.txt"
        nameWksp = homeDir+"scripts/Results/MC/TzFit/tauFit/MC_TRUE"+add+"_Tz_2016_wksp.root"
        nameRoot = homeDir+"scripts/Results/MC/TzFit/tauFit/MC_TRUE"+add+"_Tz_Fit_plot.root"
        namePic1 = homeDir+"scripts/Results/MC/TzFit/tauFit/MC_TRUE"+add+"_Tz.pdf"
        #namePic1 = homeDir+"scripts/Results/MC/TzFit/MC_TRUE_Tz_Jpsi.pdf"
        #namePic2 = homeDir+"scripts/Results/MC/TzFit/MC_TRUE_Tz_Etac.pdf"

    else:

        nameTxt = homeDir+"scripts/Results/MC/TzFit/tauFit/fit_TRUE_Tz"+add+"_PT{}.txt".format(iPT)
        nameWksp = homeDir+"scripts/Results/MC/TzFit/tauFit/MC_TRUE_Tz"+add+"_2016_wksp_PT{}.root".format(iPT)
        nameRoot = homeDir+"scripts/Results/MC/TzFit/tauFit/MC_TRUE_Tz"+add+"_Fit_plot_PT{}.root".format(iPT)
        namePic1 = homeDir+"scripts/Results/MC/TzFit/tauFit/MC_TRUE_Tz"+add+"_PT{}.pdf".format(iPT)
        #namePic1 = homeDir+"scripts/Results/MC/TzFit/MC_TRUE_Tz_Jpsi_TRUEPT{}.pdf".format(iPT)
        #namePic2 = homeDir+"scripts/Results/MC/TzFit/MC_TRUE_Tz_Etac_PT{}.pdf".format(iPT)

    
    w.writeToFile(nameWksp)
    fFit =  TFile (nameRoot,"RECREATE")
    #fFit =  TFile (nameRoot,"UPDATE")
    c.Write("")
    #c1.Write("")
    #c2.Write("")
    fFit.Close()
    
    c.SaveAs(namePic1)
    #c1.SaveAs(namePic1)
    #c2.SaveAs(namePic2)
    
    fo = open(nameTxt,"w")
    fo.write(" chi2Jpsi =  %5.4f,    chi2Etac =  %5.4f \n\n"%( chi2Jpsi,  chi2Etac)) 
    fo.write(" edm =  %5.3e \n\n"%(r.edm()))
    fo.write(" tauB = %5.4f +/- %5.4f \n"%( w.var("tauB").getValV(),  w.var("tauB").getError()))
    #fo.write(" mu = %5.4e +/- %5.4e \n"%(   w.var("mu").getValV(),    w.var("mu").getError()))
    #fo.write(" beta = %5.4f +/- %5.4f \n"%( w.var("beta").getValV(),  w.var("beta").getError())) 
    #fo.write(" S1 = %5.4f +/- %5.4f \n"%(   w.var("S1").getValV(),    w.var("S1").getError()))
    #fo.write(" rS = %5.4f +/- %5.4f \n"%(   w.var("rS").getValV(),    w.var("rS").getError()))
    fo.write(" NJpsi_B = %5.4f +/- %5.4f,  %5.4f \n"%( w.var("NJpsiB").getValV(), w.var("NJpsiB").getError(), w.var("NJpsiB").getValV()/w.var("NJpsiB").getError()))
    fo.write(" NEtac_B = %5.4f +/- %5.4f,  %5.4f \n"%( w.var("NEtacB").getValV() , w.var("NEtacB").getError(), w.var("NEtacB").getValV()/w.var("NEtacB").getError()))
    #fo.write(" NJpsi_T = %5.4f +/- %5.4f,  %5.4f \n"%( w.var("NJpsiT").getValV(), w.var("NJpsiT").getError(), w.var("NJpsiT").getValV()/w.var("NJpsiT").getError()))
    #fo.write(" NEtac_T = %5.4f +/- %5.4f,  %5.4f \n"%( w.var("NEtacT").getValV() , w.var("NEtacT").getError(), w.var("NEtacT").getValV()/w.var("NEtacT").getError()))
    fo.write("\n")
    
    #r.printValue(fo)
    #r.printMultiline(fo, 6, True)
    fo.close()
    
    #w.Print("v")
    #r.floatParsFinal().Print()
    r.correlationMatrix().Print()
    #     std.cin.get()
    
    

def MC_TRUE_Tz_Fit():

    nPTBins = 7
    fitData(0)
    for iPT in range(1, nPTBins+1):
        fitData(iPT)


#w =  RooWorkspace("w",True)
#getData_s(w, 0)

MC_TRUE_Tz_Fit()

#fitData(0)
