
//#ifndef __CINT__
//#include "RooGlobalFunc.h"
//#endif
//#include "RooRealVar.h"
//#include "RooDataSet.h"
//#include "RooGaussian.h"
//#include "RooConstVar.h"
//#include "RooProdPdf.h"
//#include "RooAddPdf.h"
//#include "RooPolynomial.h"
//#include "TCanvas.h"
//#include "TAxis.h"
//#include "RooPlot.h"
//#include "RooFitResult.h"
//#include "RooRelBreitWigner.h"
#include <stdio.h>
#include <stdlib.h>
using namespace RooFit ;

void getData_d(RooWorkspace* w)
{
  
  const Int_t nPTBins = 5;
  const char *pt[nPTBins+1] = {"6500", "8000", "10000", "12000", "14000", "18000"};
  
  TChain * ntEtac=new TChain("DecayTree");
  TChain * ntJpsi=new TChain("DecayTree");
  
   
  ntEtac->Add("../MC/Etac/2016/EtacDiProton_MC_2016_AddBr.root");
  ntEtac->Add("../MC/Etac/2015/EtacDiProton_MC_2015_AddBr.root");
  ntJpsi->Add("../MC/Jpsi/2016/JpsiDiProton_MC_2016_AddBr.root");
  ntJpsi->Add("../MC/Jpsi/2015/JpsiDiProton_MC_2015_AddBr.root");
  
  TTree *treeEtac;
  TTree *treeJpsi;
  
  treeEtac = ntEtac->CopyTree("Jpsi_sec && (ProtonP_PT>6000) && (ProtonM_PT>6000)");
  treeJpsi = ntJpsi->CopyTree("Jpsi_sec && (ProtonP_PT>6000) && (ProtonM_PT>6000)");
    
    
  
  RooRealVar Jpsi_M("Jpsi_M","Jpsi_M",2800.,3200.) ;
  RooDataSet dsEtac("dsEtac","dsEtac",treeEtac,RooArgSet(Jpsi_M));
  RooDataSet dsJpsi("dsJpsi","dsJpsi",treeJpsi,RooArgSet(Jpsi_M));
  w->import(dsEtac);
//  w->import(dsJpsi,RecycleConflictNodes());
  w->import(dsJpsi);
  
  
  delete ntEtac;
  delete ntJpsi;
}

void fillRelWorkspace(RooWorkspace *w)
{
  
  RooRealVar* Jpsi_M = w->var("Jpsi_M");
//   Jpsi_M->setBins(1000,"cache");
  
  Double_t ratioNtoW = 0.50;
  Double_t ratioEtaToJpsi = 0.88;
  Double_t ratioArea = 0.70;
  Double_t gammaEtac = 31.8;
  
  
  
  RooRealVar rEtaToJpsi("rEtaToJpsi","rEtaToJpsi", 0.95);
  RooRealVar rNarToW("rNarToW","rNarToW",0.245);
  RooRealVar rG1toG2("rG1toG2","rG1toG2",0.948);
  RooRealVar nEtac("nEtac","num of Etac", 1e3, 10, 1.e4);
  RooRealVar nJpsiEtac("nJpsiEtac","num of nJpsiEtac", 1e3, 10, 1.e4);
  RooRealVar nJpsi("nJpsi","num of J/Psi", 2e3, 10, 1.e4);
  RooRealVar nEtacRel("nEtacRel","num of Etac", 0.0, 3.0);
  
//   RooFormulaVar nEtac_1("nEtac_1","num of Etac","@0*@1*@2",RooArgSet(nEtacRel,nJpsi,rG1toG2));
//   RooFormulaVar nEtac_2("nEtac_2","num of Etac","@0*@1-@2",RooArgSet(nEtacRel,nJpsi,nEtac_1));
  RooFormulaVar nEtac_1("nEtac_1","num of Etac","@0*@1",RooArgSet(nEtac,rG1toG2));
  RooFormulaVar nEtac_2("nEtac_2","num of Etac","@0-@1",RooArgSet(nEtac,nEtac_1));
    
  RooFormulaVar nJpsiEtac_1("nJpsiEtac_1","nJpsiEtac_1","@0*@1",RooArgSet(nJpsiEtac,rG1toG2));
  RooFormulaVar nJpsiEtac_2("nJpsiEtac_2","nJpsiEtac_2","@0-@1",RooArgSet(nJpsiEtac,nJpsiEtac_1));
    
  RooFormulaVar nJpsi_1("nJpsi_1","num of J/Psi","@0*@1",RooArgSet(nJpsi,rG1toG2));
  RooFormulaVar nJpsi_2("nJpsi_2","num of J/Psi","@0-@1",RooArgSet(nJpsi,nJpsi_1));

  
//  RooRealVar mean_Jpsi("mean_Jpsi","mean of gaussian", 0.0, -50.0, 50.0) ;  
//  RooRealVar mean_Etac("mean_Etac","mean of gaussian", 0.0, -50.0, 50.0) ;
    
  RooRealVar mean_Jpsi("mean_Jpsi","mean of gaussian", 3100., 3050,3150) ;
  RooRealVar mean_Etac("mean_Etac","mean of gaussian", 2986., 2970,3000) ;
    
  RooRealVar gamma_eta("gamma_eta","width of Br-W", gammaEtac, 10., 50. );
  RooRealVar spin_eta("spin_eta","spin_eta", 0. );
  RooRealVar radius_eta("radius_eta","radius", 1.);
  RooRealVar proton_m("proton_m","proton mass", 938.3 );

  RooRealVar sigma_eta_1("sigma_eta_1","width of gaussian", 9., 0.1, 50.) ;
  RooFormulaVar sigma_eta_2("sigma_eta_2","width of gaussian","@0/@1",RooArgSet(sigma_eta_1,rNarToW));
  
  RooFormulaVar sigma_Jpsi_1("sigma_Jpsi_1","width of gaussian","@0/@1",RooArgList(sigma_eta_1,rEtaToJpsi));
  RooFormulaVar sigma_Jpsi_2("sigma_Jpsi_2","width of gaussian","@0/@1",RooArgList(sigma_Jpsi_1,rNarToW));
  
  
  
  // Fit eta
  RooGaussian gaussEta_1("gaussEta_1","gaussEta_1 PDF", *Jpsi_M, RooConst(0.),  sigma_eta_1);
  RooGaussian gaussEta_2("gaussEta_2","gaussEta_2 PDF", *Jpsi_M, RooConst(0.),  sigma_eta_2);
  
  RooRelBreitWigner br_wigner("br_wigner", "br_wigner",*Jpsi_M, mean_Etac, gamma_eta, spin_eta,radius_eta,proton_m,proton_m);
  
  RooFFTConvPdf bwxg_1("bwxg_1","breit-wigner (X) gauss", *Jpsi_M, br_wigner, gaussEta_1) ;
  RooFFTConvPdf bwxg_2("bwxg_2","breit-wigner (X) gauss", *Jpsi_M, br_wigner, gaussEta_2) ;
  
    
  RooGaussian pdfJpsi_in_Etac1("pdfJpsi_in_Etac1","pdfJpsi_in_Etac1",*Jpsi_M, mean_Jpsi, sigma_Jpsi_1) ;
  RooGaussian pdfJpsi_in_Etac2("pdfJpsi_in_Etac2","pdfJpsi_in_Etac2",*Jpsi_M, mean_Jpsi, sigma_Jpsi_2) ;
    

  // Fit J/psi
  RooGaussian gauss_1("gauss_1","gauss_1",*Jpsi_M, mean_Jpsi, sigma_Jpsi_1) ;
  RooGaussian gauss_2("gauss_2","gauss_2",*Jpsi_M, mean_Jpsi, sigma_Jpsi_2) ;
  
  

  
    
  RooAddPdf modelEtac("modelEtac","Etac signal", RooArgList(bwxg_1, bwxg_2, pdfJpsi_in_Etac1, pdfJpsi_in_Etac2), RooArgList(nEtac_1, nEtac_2, nJpsiEtac_1, nJpsiEtac_2));
  RooAddPdf modelJpsi("modelJpsi","Jpsi signal", RooArgList(gauss_1, gauss_2), RooArgList(nJpsi_1, nJpsi_2));
    
  
  RooCategory sample("sample","sample") ;
  sample.defineType("Etac") ;
  sample.defineType("Jpsi") ;
  
  
  RooDataSet *dataEtac = (RooDataSet*) w->data("dsEtac");
  RooDataSet *dataJpsi = (RooDataSet*) w->data("dsJpsi");
  
  
  // Construct combined dataset in (Jpsi_M,sample)
  RooDataSet combData("combData","combined data",*Jpsi_M,Index(sample),Import("Etac",*dataEtac),Import("Jpsi",*dataJpsi)) ;
  
  
  // Associate model with the physics state and model_ctl with the control state
  RooSimultaneous simPdf("simPdf","simultaneous signal pdf",sample) ;
  simPdf.addPdf(modelEtac,"Etac") ;
  simPdf.addPdf(modelJpsi,"Jpsi") ;
  
  
//   w->import(model,RecycleConflictNodes());
  w->import(combData,RecycleConflictNodes());
  w->import(simPdf,RecycleConflictNodes());
  
}



void fitData()
{
  
  gROOT->Reset();
  
  TProof *proof = TProof::Open("");
  
  
  RooWorkspace *w = new RooWorkspace("w",kTRUE);   
  
  getData_d(w);
  fillRelWorkspace(w);
  
  RooRealVar *Jpsi_M = w->var("Jpsi_M");


  RooAbsPdf *modelEtac = w->pdf("modelEtac");
  RooAbsPdf *modelJpsi = w->pdf("modelJpsi");
  
  RooCategory *sample = w->cat("sample");
  RooAbsPdf *simPdf = w->pdf("simPdf");
  RooDataSet *combData = (RooDataSet*) w->data("combData");
  
  
  RooDataSet *dataEtac = (RooDataSet*) w->data("dsEtac");
  RooDataSet *dataJpsi = (RooDataSet*) w->data("dsJpsi");
  
//1
  RooRealVar *sigma = w->var("sigma_eta_1");
  RooRealVar *mean_Jpsi = w->var("mean_Jpsi");
  RooRealVar *mean_Etac = w->var("mean_Etac");
  RooRealVar *gamma_eta = w->var("gamma_eta");
    
  
//   mean_Etac->setConstant(kTRUE);
//   mean_Jpsi->setConstant(kTRUE);
//   simPdf->fitTo(*combData,/*Extended(),Minos(kTRUE),*//*Offset(kTRUE),*//*Integrate(kTRUE),*/Save(kTRUE)) ;
//   RooFitResult *r2 = modelJpsi->fitTo(*dataJpsi,/*Extended(),Minos(kTRUE),*//*Offset(kTRUE),*//*Integrate(kTRUE),*/Save(kTRUE)) ;
//   RooFitResult *r1 = modelEtac->fitTo(*dataEtac,/*Extended(),Minos(kTRUE),*//*Offset(kTRUE),*//*Integrate(kTRUE),*/Save(kTRUE)) ;
//   mean_Etac->setConstant(kFALSE);
//   mean_Jpsi->setConstant(kFALSE);
  RooFitResult *r = simPdf->fitTo(*combData,/*Extended(),Minos(kTRUE),*//*Offset(kTRUE),*//*Integrate(kTRUE),*/Save(kTRUE)) ;
//   model->fitTo(*dataEtac,/*Extended(),*//*Minos(kTRUE),*/Offset(kTRUE),/*Integrate(kTRUE),*/Save(kTRUE)) ;
//   RooFitResult *r = model->fitTo(*dataEtac,/*Extended(),*//*Minos(kTRUE),*//*Offset(kTRUE),*//*Integrate(kTRUE),*/Save(kTRUE)) ;
  
  
    
  RooPlot *frame1 = Jpsi_M->frame(Title("#eta_{c} to p #bar{p}")) ;
  RooPlot *frame2 = Jpsi_M->frame(Title("J/#psi to p #bar{p}")) ;
    
  dataEtac->plotOn(frame1/*,Binning(Jpsi_Tz->getBinning("Bins"))*/);
  dataJpsi->plotOn(frame2/*,Binning(Jpsi_Tz->getBinning("Bins"))*/);
  
  
//2
  Double_t chi2Etac=0;
  Double_t chi2Jpsi=0;
  RooAbsPdf *gaussEta_1 = w->pdf("gaussEta_1");
  RooAbsPdf *gaussEta_2 = w->pdf("gaussEta_2");
  RooAbsPdf *gauss_1 = w->pdf("gauss_1");
  RooAbsPdf *gauss_2 = w->pdf("gauss_2");
  
  
  
  modelEtac->paramOn(frame1);
  modelEtac->plotOn(frame1,Normalization(1.0,RooAbsReal::RelativeExpected));
  chi2Etac = frame1->chiSquare();
  modelEtac->plotOn(frame1,Components(RooArgSet(*gaussEta_1,gaussEta_2)), FillStyle(3005), FillColor(kMagenta), DrawOption("F"),Normalization(1.0,RooAbsReal::RelativeExpected));
  
  modelJpsi->paramOn(frame2);
  modelJpsi->plotOn(frame2,Normalization(1.0,RooAbsReal::RelativeExpected));
  chi2Jpsi = frame2->chiSquare();
  modelJpsi->plotOn(frame2,Components(RooArgSet(*gauss_1,gauss_2)),FillStyle(3005),FillColor(kMagenta),DrawOption("F"), Normalization(1.0,RooAbsReal::RelativeExpected));
    
    
  TCanvas* c = new TCanvas("Masses_Fit","Masses Fit",800,900);
  c->Divide(1,2);
  c->cd(1)->SetPad(.005, .505, .995, .995);
  gPad->SetLeftMargin(0.15) ; frame1->GetXaxis()->SetTitle("M(p #bar{p}) / [MeV/c^{2}]") ; frame1->Draw();
  frame1->SetMinimum(1.);
  //gPad->SetLogy();
  c->cd(2)->SetPad(.005, .005, .995, .495);
  gPad->SetLeftMargin(0.15) ; frame2->GetXaxis()->SetTitle("M(p #bar{p}) / [MeV/c^{2}]") ; frame2->Draw();
  frame2->SetMinimum(1.);
  //gPad->SetLogy();
  
  
  
  
//   r->correlationMatrix().Print("v");
//   std::cin.get();
}


void MC_Resolution_Fit()
{
  fitData();
//  for (Int_t iPT=1; iPT<=5; iPT++)
//    fitData(iPT);  
}