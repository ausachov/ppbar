from ROOT import *

gROOT.LoadMacro("../libRooRelBreitWigner/RooRelBreitWigner.cxx+");
gROOT.LoadMacro("../BifurcatedCB/BifurcatedCB.cxx+");
homeDir = "/users/LHCb/zhovkovska/scripts/"

def getData_d(w, iPT=0):
    
    nPTBins = 5
    pt = (['6500', '8000'], ['8000', '10000'], ['10000', '12000'], ['12000', '14000'], ['14000', '18000'])
    #pt = (['6500', '7000'], ['7000', '8000'], ['8000', '9000'], ['9000', '10000'] ,['10000', '11000'], ['11000', '12000'],['12000', '13000'], ['13000', '14000'],['14000', '16000'],['16000', '18000'])
    
    ntEtac_Prompt = TChain('DecayTree')
    ntJpsi_Prompt = TChain('DecayTree')
    ntEtac_FromB = TChain('DecayTree')
    ntJpsi_FromB = TChain('DecayTree')
    
    ntEtac_Prompt.Add(homeDir+'MC/newMC/Etac/EtacDiProton_MC_2016_AddBr.root')
    ntEtac_Prompt.Add(homeDir+'MC/newMC/Etac/EtacDiProton_MC_2015_AddBr.root')
    ntJpsi_Prompt.Add(homeDir+'MC/newMC/Jpsi/JpsiDiProton_MC_2016_AddBr.root')
    ntJpsi_Prompt.Add(homeDir+'MC/newMC/Jpsi/JpsiDiProton_MC_2015_AddBr.root')
    ntEtac_FromB.Add(homeDir+'MC/newMC/incl_b/EtacDiProton_MC_2015_incl_b_AddBr.root')
    ntEtac_FromB.Add(homeDir+'MC/newMC/incl_b/EtacDiProton_MC_2016_incl_b_AddBr.root')
    ntJpsi_FromB.Add(homeDir+'MC/newMC/Jpsi/JpsiDiProton_MC_2016_AddBr.root')
    ntJpsi_FromB.Add(homeDir+'MC/newMC/Jpsi/JpsiDiProton_MC_2015_AddBr.root')
    
    treeEtac_Prompt = TTree()
    treeJpsi_Prompt = TTree()
    treeEtac_FromB = TTree()
    treeJpsi_FromB = TTree()
    
    if(iPT == 0):
        
        treeEtac_Prompt = ntEtac_Prompt.CopyTree("prompt")
        treeJpsi_Prompt = ntJpsi_Prompt.CopyTree("prompt")
        treeEtac_FromB = ntEtac_FromB.CopyTree("sec")
        treeJpsi_FromB = ntJpsi_FromB.CopyTree("sec")

    elif ( iPT <= nPTBins ):
        cutPrompt = 'prompt'
        cutFromB = 'sec'
        cutJpsiPtL = 'Jpsi_PT > %s'%(pt[iPT-1][0])
        cutJpsiPtR = 'Jpsi_PT < %s'%(pt[iPT-1][1])
        
        treeEtac_Prompt = ntEtac_Prompt.CopyTree(cutPrompt + '&&' + cutJpsiPtL + '&&' + cutJpsiPtR)
        treeJpsi_Prompt = ntJpsi_Prompt.CopyTree(cutPrompt + '&&' + cutJpsiPtL + '&&' + cutJpsiPtR)
        treeEtac_FromB = ntEtac_FromB.CopyTree(cutFromB + '&&' + cutJpsiPtL + '&&' + cutJpsiPtR)
        treeJpsi_FromB = ntJpsi_FromB.CopyTree(cutFromB + '&&' + cutJpsiPtL + '&&' + cutJpsiPtR)
      
    else:
        print ('Incorrect number of PT bin %s'%(iPT))
    
    
    Jpsi_M_res = RooRealVar ('Jpsi_M_res','Jpsi_M_res',-50.0,50.0)  #!!!!!!!!!!!!!!!!!! change from (-100, 100)
    dsEtac_Prompt = RooDataSet('dsEtac_Prompt','dsEtac_Prompt',treeEtac_Prompt,RooArgSet(Jpsi_M_res))
    dsJpsi_Prompt = RooDataSet('dsJpsi_Prompt','dsJpsi_Prompt',treeJpsi_Prompt,RooArgSet(Jpsi_M_res))
    dsEtac_FromB = RooDataSet('dsEtac_FromB','dsEtac_FromB',treeEtac_FromB,RooArgSet(Jpsi_M_res))
    dsJpsi_FromB = RooDataSet('dsJpsi_FromB','dsJpsi_FromB',treeJpsi_FromB,RooArgSet(Jpsi_M_res))
    getattr(w,'import')(dsEtac_Prompt)
    getattr(w,'import')(dsJpsi_Prompt)
    getattr(w,'import')(dsEtac_FromB)
    getattr(w,'import')(dsJpsi_FromB)
    
    #  dsEtac.Draw('')
    #  dsJpsi.Draw('')
    




def fitData(iPT, empty=False):

    
    gROOT.Reset()
    #TProof *proof = TProof.Open('')
    
    
    w = RooWorkspace('w',True)   
    
    getData_d(w, iPT)
    
    
    Jpsi_M_res = w.var('Jpsi_M_res')
    
    if (iPT==0):
        f = TFile(homeDir+"Results/MC/MassFit/MC_MassResolution_2016_wksp.root","READ")
    else:
        f = TFile(homeDir+"Results/MC/MassFit/MC_MassResolution_2016_wksp_PT%s.root"%(iPT),"READ")
    wTwo = f.Get("w")
    f.Close()
    
    #if (iPT==0):
        #f = TFile(homeDir+"Results/MC/MassFit/OneRes/MC_MassResolution_2016_wksp.root","READ")
    #else:
        #f = TFile(homeDir+"Results/MC/MassFit/OneRes/MC_MassResolution_2016_wksp_PT%s.root"%(iPT),"READ")
    if (iPT==0):
        f = TFile(homeDir+"Results/MC/MassFit/MC_MassResolution_CB_2016_wksp.root","READ")
    else:
        f = TFile(homeDir+"Results/MC/MassFit/MC_MassResolution_CB_2016_wksp_PT%s.root"%(iPT),"READ")
    wOne = f.Get("w")
    f.Close()
    
    modelEtac_Prompt = wTwo.pdf('modelEtac_Prompt')
    modelJpsi_Prompt = wTwo.pdf('modelJpsi_Prompt')
    modelEtac_FromB = wTwo.pdf('modelEtac_FromB')
    modelJpsi_FromB = wTwo.pdf('modelJpsi_FromB')
    modelEtac_Prompt.SetName("modelEtac_PromptTwo")
    modelJpsi_Prompt.SetName("modelEtac_FromBTwo")
    modelEtac_FromB.SetName("modelJpsi_PromptTwo")
    modelJpsi_FromB.SetName("modelJpsi_FromBTwo")
    
    modelEtac_PromptOne = wOne.pdf('modelEtac_Prompt')
    modelJpsi_PromptOne = wOne.pdf('modelJpsi_Prompt')
    modelEtac_FromBOne = wOne.pdf('modelEtac_FromB')
    modelJpsi_FromBOne = wOne.pdf('modelJpsi_FromB')

    
    dataEtac_Prompt = w.data('dsEtac_Prompt')
    dataJpsi_Prompt = w.data('dsJpsi_Prompt')
    dataEtac_FromB = w.data('dsEtac_FromB')
    dataJpsi_FromB = w.data('dsJpsi_FromB')
    
        
    
    gROOT.ProcessLine('gStyle->SetOptTitle(0)')
    if empty:
        gROOT.ProcessLine('gStyle->SetOptStat(000000000)')

    frame = []
    for i in range(4):
        frame.append(Jpsi_M_res.frame(RooFit.Title('')))

    dataEtac_Prompt.plotOn(frame[0])
    dataJpsi_Prompt.plotOn(frame[1])
    dataEtac_FromB.plotOn(frame[2])
    dataJpsi_FromB.plotOn(frame[3])
    
    
    
    #chi2Etac = 0.
    #chi2Jpsi = 0.
    #chi2Etac = 0.
    #chi2Jpsi = 0.
    
    
    #modelEtac_Prompt.paramOn(frame[0],RooFit.Layout(0.68,0.99,0.99))
    #frame[0].getAttText().SetTextSize(0.027) 
    modelEtac_Prompt.plotOn(frame[0],RooFit.Normalization(1.0,RooAbsReal.RelativeExpected))
    modelEtac_PromptOne.plotOn(frame[0], RooFit.LineColor(2), RooFit.Normalization(1.0,RooAbsReal.RelativeExpected))
    chi2Etac_Prompt = frame[0].chiSquare()
    #modelEtac_Prompt.plotOn(frame[0],RooFit.Components(RooArgSet(gaussEta_1,gaussEta_2)), RooFit.FillStyle(3005), RooFit.FillColor(kMagenta), RooFit.DrawOption('F'), RooFit.Normalization(1.0,RooAbsReal.RelativeExpected))
    print 'chi2Etac_Prompt = ', chi2Etac_Prompt

    #modelJpsi_Prompt.paramOn(frame[1],RooFit.Layout(0.68,0.99,0.99))
    #frame[1].getAttText().SetTextSize(0.027) 
    modelJpsi_Prompt.plotOn(frame[1],RooFit.Normalization(1.0,RooAbsReal.RelativeExpected))
    modelJpsi_PromptOne.plotOn(frame[1],RooFit.LineColor(2), RooFit.Normalization(1.0,RooAbsReal.RelativeExpected))
    chi2Jpsi_Prompt = frame[1].chiSquare()
    #modelJpsi_Prompt.plotOn(frame[1],RooFit.Components(RooArgSet(gauss_1,gauss_2)),RooFit.FillStyle(3005),RooFit.FillColor(kMagenta),RooFit.DrawOption('F'), RooFit.Normalization(1.0,RooAbsReal.RelativeExpected))
    print 'chi2Jpsi_Prompt = ', chi2Jpsi_Prompt
    
    #modelEtac_FromB.paramOn(frame[2],RooFit.Layout(0.68,0.99,0.99))
    #frame[2].getAttText().SetTextSize(0.027) 
    modelEtac_FromB.plotOn(frame[2],RooFit.Normalization(1.0,RooAbsReal.RelativeExpected))
    modelEtac_FromBOne.plotOn(frame[2], RooFit.LineColor(2), RooFit.Normalization(1.0,RooAbsReal.RelativeExpected))
    chi2Etac_FromB = frame[2].chiSquare()
    #modelEtac_FromB.plotOn(frame[2],RooFit.Components(RooArgSet(gaussEta_1,gaussEta_2)), RooFit.FillStyle(3005), RooFit.FillColor(kMagenta), RooFit.DrawOption('F'),RooFit.Normalization(1.0,RooAbsReal.RelativeExpected))
    print 'chi2Etac_FromB = ', chi2Etac_FromB
    
    #modelJpsi_FromB.paramOn(frame[3],RooFit.Layout(0.68,0.99,0.99))
    #frame[3].getAttText().SetTextSize(0.027)
    modelJpsi_FromB.plotOn(frame[3],RooFit.Normalization(1.0,RooAbsReal.RelativeExpected))
    modelJpsi_FromBOne.plotOn(frame[3], RooFit.LineColor(2), RooFit.Normalization(1.0,RooAbsReal.RelativeExpected))
    chi2Jpsi_FromB = frame[3].chiSquare()
    #modelJpsi_FromB.plotOn(frame[3], RooFit.Components(RooArgSet(gauss_1,gauss_2)), RooFit.FillStyle(3005), RooFit.FillColor(kMagenta), RooFit.DrawOption('F'), RooFit.Normalization(1.0,RooAbsReal.RelativeExpected))
    print 'chi2Jpsi_FromB = ', chi2Jpsi_FromB
    
    c = TCanvas('Masses_Fit','Masses Fit',1200,800)
    c.Divide(2,2)
    names = ["#eta_{c} prompt", "J/#psi prompt", "#eta_{c} from-b", "J/#psi from-b"]
    texMC = TLatex()
    texMC.SetNDC()

    for iC in range(4):
        pad = c.cd(iC+1)
        xl = pad.GetXlowNDC(); xh = xl + pad.GetWNDC()
        yl = pad.GetYlowNDC(); yh = yl + pad.GetHNDC()
        pad.SetPad(xl+0.005,yl+0.005,xh-0.005,yh-0.005)
        pad.SetLeftMargin(0.15);  pad.SetBottomMargin(0.15);  frame[iC].GetXaxis().SetTitle('M_{p#bar{p}} - M^{TRUE}_{p#bar{p}} / [MeV/c^2]')
        frame[iC].GetXaxis().SetTitleSize(0.06)
        frame[iC].GetYaxis().SetTitleSize(0.06)
        frame[iC].GetXaxis().SetTitleOffset(0.90)
        frame[iC].GetYaxis().SetTitleOffset(0.90)
        frame[iC].GetXaxis().SetTitleFont(12)
        frame[iC].GetYaxis().SetTitleFont(12)
        frame[iC].GetXaxis().SetLabelSize(0.05)
        frame[iC].GetYaxis().SetLabelSize(0.05)
        frame[iC].GetXaxis().SetLabelFont(62)
        frame[iC].GetYaxis().SetLabelFont(62)
        frame[iC].Draw()
        #frame[iC].SetMaximum(6.e2)
        frame[iC].SetMinimum(0.1)
        texMC.DrawLatex(0.6, 0.80, "LHCb simulation")
        texMC.DrawLatex(0.6, 0.75, "#sqrt{s}=13 TeV")
        texMC.DrawLatex(0.25, 0.75, names[iC])
        #pad.SetLogy()

    
    
    nameTxt = ''
    nameRoot = ''
    nameWksp = ''
    namePic = ''
    
    if(iPT == 0):
    
        nameRoot = homeDir+'Results/MC/MassFit/MC_MassRes_Fit_plot_CB_comp.root'
        namePic = homeDir+'Results/MC/MassFit/MC_MassRes_CB_comp.pdf'    
    
    else:
    
        nameRoot = homeDir+'Results/MC/MassFit/MC_MassRes_Fit_plot_CB_comp_PT%s.root'%(iPT)
        namePic = homeDir+'Results/MC/MassFit/MC_MassRes_CB_comp_PT%s.pdf'%(iPT)
    

    fFit = TFile (nameRoot,'RECREATE')
    
    
    c.Write('')
    fFit.Write()
    fFit.Close()
    
    c.SaveAs(namePic)
    





def MC_Resolution_Fit():

    #fitData(0)
    nPTBins = 5 
    for iPT in range(nPTBins):
        fitData(iPT)  
    
#gROOT.LoadMacro("../lhcbStyle.C")
MC_Resolution_Fit()
#fitData(0)



