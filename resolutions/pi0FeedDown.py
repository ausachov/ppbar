from ROOT import *

gROOT.LoadMacro("../libRooRelBreitWigner/RooRelBreitWigner.cxx+");
homeDir = "/users/LHCb/zhovkovska/scripts/"

def getData_d(w, iTz=0):
    
    nPTBins = 5
    nTzBins = 10
    pt = (['6500', '8000'], ['8000', '10000'], ['10000', '12000'], ['12000', '14000'], ['14000', '18000'])
    
    ntJpsi= TChain('DecayTree')

    #-----------old MC--------------------------------------------------------    
    #ntJpsi.Add(homeDir+'MC/newMC/Jpsi2pppi0/JpsiDiProtonPi0_MC_2016_AddBr.root')
    #ntJpsi.Add(homeDir+'MC/newMC/Jpsi2pppi0/JpsiDiProtonPi0_MC_2015_AddBr.root')

    #-----------new MC--------------------------------------------------------    
    ntJpsi.Add(homeDir+'MC/newMC_spring/Jpsi2pppi0_Pr_all_l0TOS_allSelected_AddBr.root')
    
    treeJpsi = TTree()
        
    
    if(iTz == 0):        
        treeJpsi = ntJpsi.CopyTree("")
    elif (iTz != 0):
        cutJpsiTzL = 'Jpsi_Tz > %f'%(tz[iTz-1])
        cutJpsiTzR = 'Jpsi_Tz < %f'%(tz[iTz])

        treeJpsi = ntJpsi.CopyTree( cutJpsiTzL + '&&' + cutJpsiTzR)
      
    else:
        print ('Incorrect number of Tz bin %s'%(iTz))
    
    
    Jpsi_M = RooRealVar ('Jpsi_M','Jpsi_M',2850.,3100.0) 
    dsJpsi = RooDataSet('dsJpsi','dsJpsi',treeJpsi,RooArgSet(Jpsi_M))
    getattr(w,'import')(dsJpsi)
    
    #dsEtac_Prompt.Draw('')
    #dsJpsi_Prompt.Draw('')
    

def getConstPars():

    f = TFile(homeDir+"Results/MC/MassFit/MC_MassResolution_2016_wksp.root","READ") 
    wMC = f.Get("w")
    f.Close()
    
    rNtoW = wMC.var("rNarToW").getValV()
    rEtaToJpsi = wMC.var("rEtaToJpsi").getValV()
    rArea = wMC.var("rG1toG2").getValV()
    gamma = 31.8
    effic = 0.035
    #gamma = 21.3
    #effic = 0.06
    #ratioNtoW = 0.5
    #ratioEtaToJpsi = 0.88
    #ratioArea = 0.9
    
    
    return rNtoW,rEtaToJpsi,rArea, gamma, effic

def fillRelWorkspace(w):

    
    Jpsi_M = w.var('Jpsi_M')
    #Jpsi_M.setBins(1000,'cache')
    Jpsi_M.setRange("Signal", 2850, 2970)
    Jpsi_M.setRange("Draw", 2850, 3100)

    jpsiMass = 3096.90
    piMass = 134.977
    thresMass = jpsiMass-piMass

    ratioNtoW,ratioEtaToJpsi,ratioArea, gammaEtac, eff = getConstPars()

    rEtaToJpsi = RooRealVar('rEtaToJpsi','rEtaToJpsi', ratioEtaToJpsi)
    rNarToW = RooRealVar('rNarToW','rNarToW',ratioNtoW)
    rG1toG2 = RooRealVar('rG1toG2','rG1toG2',ratioArea)

    mean_Jpsi = RooRealVar('mean_Jpsi','mean of gaussian', 0.0, -50.0, 50.0)   
    sigma_Jpsi_1 = RooRealVar('sigma_Jpsi_1','width of gaussian', 9., 0.1, 50.) 
    sigma_Jpsi_2 = RooFormulaVar('sigma_Jpsi_2','width of gaussian','@0/@1',RooArgList(sigma_Jpsi_1,rNarToW))    

    mThres = RooRealVar("M_{thres}", "varThres", thresMass);
        #Fit J/psi
    gauss_1 = RooGaussian('gauss_1','gaussian PDF',Jpsi_M, RooFit.RooConst(0.), sigma_Jpsi_1) 
    gauss_2 = RooGaussian('gauss_2','gaussian PDF',Jpsi_M, RooFit.RooConst(0.), sigma_Jpsi_2) 

    #nPPPi0 = RooFormulaVar("nPPPi0_PT%s_Tz%s"%(nPT,nTz),"nPPPi0","@0*@1*(1.19/2.12)",RooArgList(nJpsi,eff_pppi0))

    modelRes = RooAddPdf('modelRes','resolution', RooArgList(gauss_1, gauss_2), RooArgList(rG1toG2))
    modelRoot = RooGenericPdf("modelRoot", "modelRoot", "(sqrt(@1 - @0))*(@0<@1)", RooArgList(Jpsi_M, mThres));
    #model = RooFFTConvPdf("model", "pdfFD", Jpsi_M, modelRoot, modelRes);
    model = RooGenericPdf("model", "model", "(sqrt(@1 - @0))*(@0<@1)", RooArgList(Jpsi_M, mThres));

    #nPPPi0 = RooRealVar('nPPPi0','num of J/Psi', 1e3, 10, 1.e4)

    #pppi0 = RooGenericPdf("pppi0","Jpsi.pppi0","@0<(2961.92) ? TMath::Sqrt(3096.900-134.977-@0) : 0",RooArgList(Jpsi_M))
    ##nPPPi0 = RooFormulaVar("nPPPi0","nPPPi0","@0*@1*(1.19/2.12)",RooArgList(nJpsi,eff_pppi0))
    
    #model = RooAddPdf("model","background", RooArgList(pppi0), RooArgList(nPPPi0))

    getattr(w,'import')(model)
    




def fitData(iTz):

    
    gROOT.Reset()
    #TProof *proof = TProof.Open('')
    
    
    w = RooWorkspace('w',True)   
    
    getData_d(w, iTz)
    
    fillRelWorkspace(w)
    
    Jpsi_M = w.var('Jpsi_M')
        
    model= w.pdf('model')
    dataJpsi = w.data('dsJpsi')
            
    gROOT.ProcessLine('gStyle->SetOptStat(000000000)')
    gROOT.ProcessLine('gStyle->SetOptTitle(0)')

    frame = Jpsi_M.frame(RooFit.Title('J/#psi to p#bar{p}#pi^{0}')) 
    dataJpsi.plotOn(frame,RooFit.Binning(100,2850,3100))
    
    #model.paramOn(frame,RooFit.Layout(0.68,0.99,0.99))
    #frame.getAttText().SetTextSize(0.027) 
    #n = w.var("nPPPi0").getValV()
    model.plotOn(frame) #,RooFit.Normalization(1.0,RooAbsReal.RelativeExpected))
    chi2Jpsi = frame.chiSquare()
    texJpsiPiMC1 = TLatex(0.7, 0.8, "#chi^{2} = %4.2f"%chi2Jpsi);
    texJpsiPiMC1.SetNDC();
    frame.addObject(texJpsiPiMC1);
    #model.plotOn(frame,RooFit.Components(RooArgSet(gauss_1,gauss_2)),RooFit.FillStyle(3005),RooFit.FillColor(kMagenta),RooFit.DrawOption('F'), RooFit.Normalization(1.0,RooAbsReal.RelativeExpected))
    print 'chi2Jpsi = ', chi2Jpsi
        
    c = TCanvas('Masses_Fit','Masses Fit',600,500)
    c.cd(1)
    #gPad.SetLeftMargin(0.15)
    frame.GetXaxis().SetTitle('M_{p#bar{p}}, MeV/c^{2}'),  
    frame.Draw()
    frame.SetMaximum(7.e1)
    frame.SetMinimum(0.1)
    #gPad.SetLogy()
    
    
    
    nameTxt = ''
    nameRoot = ''
    nameWksp = ''
    namePic = ''
    
    if(iTz == 0):
    
        nameTex = homeDir+'Results/MC/MassFit/fit_Mass_pppi0_newMC.txt'
        nameWksp = homeDir+'Results/MC/MassFit/MC_Mass_pppi0_newMC_2016_wksp.root'
        nameRoot = homeDir+'Results/MC/MassFit/MC_Mass_pppi0_newMC_Fit_plot.root'
        namePic = homeDir+'Results/MC/MassFit/MC_Mass_pppi0_newMC.pdf'    
    
    else:
    
        nameTex = homeDir+'Results/MC/MassFit/fit_Mass_pppi0_Tz%s.txt'%(iTz)
        nameWksp = homeDir+'Results/MC/MassFit/MC_Mass_pppi0_wksp_Tz%s.root'%(iTz)
        nameRoot = homeDir+'Results/MC/MassFit/MC_Mass_pppi0_Fit_plot_Tz%s.root'%(iTz)
        namePic = homeDir+'Results/MC/MassFit/MC_Mass_pppi0_Tz%s.pdf'%(iTz)
    
    #import os
    #os = open(nameTxt,'w')
    #fo = open(nameTxt,'w')

    #for var in w.allVars():
        #line = string(var.getTitle()+' '+var.getValV()+' '+var.getError() )
        #fo.write(line)

    #fo.close()

    #params = simPdf.getParameters(Jpsi_M) ;
    #params = w.allVars()
    #params.printLatex(RooFit.OutputFile(nameTex))

    #fo = open(nameTex,'w')
    #fo.write('$chi^2 eta_c$ %6.4f \n'  %(chi2Etac))
    #fo.write('$chi^2 J/\psi$ %6.4f \n' %(chi2Jpsi))

    #fo.write("edm = %6.6f \n\n"%(r.edm()))

    #fo.write("mean J/psi = %2.3f +/- %2.3f \n"  %( w.var("mass_Jpsi").getValV(),  w.var("mass_Jpsi").getError()))
    ##fo.write("mean eta(c) = %2.3f +/- %2.3f \n" %( w.var("mass_res").getValV(), w.var("mass_res").getError()))
    ##fo.write("nEtac = %6.1f +/- %6.1f, %3.2f \n"   %( w.var("nEtac").getValV(),  w.var("nEtac").getError(), w.var("nEtac").getValV()/w.var("nEtac").getError()))
    #fo.write("nJpsi = %6.1f +/- %6.1f, %3.2f \n"   %( w.var("nJpsi").getValV(),  w.var("nJpsi").getError(), w.var("nJpsi").getValV()/w.var("nJpsi").getError())) 

    ##fo.write("rEtaToJpsi = %1.4f +/- %1.4f \n"  %( w.var("rEtaToJpsi").getValV(),  w.var("rEtaToJpsi").getError()))
    ##fo.write("rNarToW = %1.4f +/- %1.4F \n"     %( w.var("rNarToW").getValV(), w.var("rNarToW").getError()))
    ##fo.write("rG1toG2 = %1.4f +/- %1.4f \n"     %( w.var("rG1toG2").getValV(),  w.var("rG1toG2").getError()))

    #fo.write("sigmaEtaNar = %6.4f +/- %6.4f \n" %( w.var("sigma_eta_1").getValV(),  w.var("sigma_eta_1").getError()))



    #fo.close()
    
    w.writeToFile(nameWksp)
    fFit = TFile (nameRoot,'RECREATE')
    
    
    c.Write('')
    fFit.Write()
    fFit.Close()
    
    c.SaveAs(namePic)
    #fo.write(r)
    #r.correlationMatrix().Print('v')
    #r.globalCorr().Print('v')
    





def MC_Resolution_Fit():

    #fitData(1)
    nTzBins = 10 
    for iTz in range(1,nTzBins+1):
        fitData(iTz)  
    

#MC_Resolution_Fit()
gROOT.LoadMacro("../lhcbStyle.C")
fitData(0)



