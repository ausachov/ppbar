from ROOT import *

gROOT.LoadMacro("../libRooRelBreitWigner/RooRelBreitWigner.cxx+");
homeDir = "/users/LHCb/zhovkovska/scripts/"

def getData_d(w, key = '', iPT=0):
    
    nPTBins = 5
    pt = (['6500', '8000'], ['8000', '10000'], ['10000', '12000'], ['12000', '14000'], ['14000', '18000'])
    #pt = (['6500', '7000'], ['7000', '8000'], ['8000', '9000'], ['9000', '10000'] ,['10000', '11000'], ['11000', '12000'],['12000', '13000'], ['13000', '14000'],['14000', '16000'],['16000', '18000'])
    
    ntEtac_FromB = TChain('DecayTree')
    ntJpsi_FromB = TChain('DecayTree')
    
    ntEtac_FromB.Add(homeDir+'MC/newMC/incl_b/EtacDiProton_MC_2015_incl_b_AddBr.root')
    ntEtac_FromB.Add(homeDir+'MC/newMC/incl_b/EtacDiProton_MC_2016_incl_b_AddBr.root')
    ntJpsi_FromB.Add(homeDir+'MC/newMC/Jpsi/JpsiDiProton_MC_2016_AddBr.root')
    ntJpsi_FromB.Add(homeDir+'MC/newMC/Jpsi/JpsiDiProton_MC_2015_AddBr.root')
    
    treeEtac_FromB = TTree()
    treeJpsi_FromB = TTree()
    
    if(iPT == 0):
        if (key == ''):
            treeEtac_FromB = ntEtac_FromB.CopyTree('sec')
            treeJpsi_FromB = ntJpsi_FromB.CopyTree('sec')
        else:
            cutJpsi_Tz = 'Jpsi_Tz > 0.08'
            cutJpsi_IP_CHI2 = '((ProtonP_IPCHI2_OWNPV > 16) && (ProtonM_IPCHI2_OWNPV > 16))'
        
            treeEtac_FromB = ntEtac_FromB.CopyTree('sec' + '&&' + cutJpsi_IP_CHI2 + '&&' + cutJpsi_Tz)
            treeJpsi_FromB = ntJpsi_FromB.CopyTree('sec' + '&&' + cutJpsi_IP_CHI2 + '&&' + cutJpsi_Tz)
        
    elif ( iPT <= nPTBins ):
        cutFromB = 'sec'
        cutJpsiPtL = 'Jpsi_PT > %s'%(pt[iPT-1][0])
        cutJpsiPtR = 'Jpsi_PT < %s'%(pt[iPT-1][1])
        if (key == ''):
            treeEtac_FromB = ntEtac_FromB.CopyTree(cutFromB + '&&' + cutJpsiPtL + '&&' + cutJpsiPtR)
            treeJpsi_FromB = ntJpsi_FromB.CopyTree(cutFromB + '&&' + cutJpsiPtL + '&&' + cutJpsiPtR)
        else:
            cutJpsi_Tz = 'Jpsi_Tz > 0.08'
            cutJpsi_IP_CHI2 = '((ProtonP_IPCHI2_OWNPV > 16) && (ProtonM_IPCHI2_OWNPV > 16))'
            treeEtac_FromB = ntEtac_FromB.CopyTree(cutFromB + '&&' + cutJpsiPtL + '&&' + cutJpsiPtR + '&&' + cutJpsi_IP_CHI2 + '&&' + cutJpsi_Tz)
            treeJpsi_FromB = ntJpsi_FromB.CopyTree(cutFromB + '&&' + cutJpsiPtL + '&&' + cutJpsiPtR + '&&' + cutJpsi_IP_CHI2 + '&&' + cutJpsi_Tz)
      
    else:
        print ('Incorrect number of PT bin %s'%(iPT))
    
    
    Jpsi_M = RooRealVar ('Jpsi_M','Jpsi_M',2850.,3250) 
    dsEtac_FromB = RooDataSet('dsEtac_FromB','dsEtac_FromB',treeEtac_FromB,RooArgSet(Jpsi_M))
    dsJpsi_FromB = RooDataSet('dsJpsi_FromB','dsJpsi_FromB',treeJpsi_FromB,RooArgSet(Jpsi_M))
    getattr(w,'import')(dsEtac_FromB)
    getattr(w,'import')(dsJpsi_FromB)
    


def fillRelWorkspace(w):

    
    Jpsi_M = w.var('Jpsi_M')
    #Jpsi_M.setBins(1000,'cache')
    Jpsi_M.setRange("SignalEtac", 2900, 3035)
    Jpsi_M.setRange("SignalJpsi", 3060, 3150)

    ratioNtoW = 0.20
    ratioEtaToJpsi = 0.97
    ratioArea = 0.95
    gammaEtac = 31.8
    #gammaEtac = 29.7
    
    
    
    #rEtaToJpsi = RooRealVar('rEtaToJpsi','rEtaToJpsi', ratioEtaToJpsi, 0.01, 5.0)
    #rNarToW = RooRealVar('rNarToW','rNarToW',ratioNtoW, 0.01, 1.0)
    #rG1toG2 = RooRealVar('rG1toG2','rG1toG2',ratioArea, 0.01, 1.0)
    rEtaToJpsi = RooRealVar('rEtaToJpsi','rEtaToJpsi', ratioEtaToJpsi)
    rNarToW = RooRealVar('rNarToW','rNarToW',ratioNtoW)
    rG1toG2 = RooRealVar('rG1toG2','rG1toG2',ratioArea)
    nEtac_FromB = RooRealVar('nEtac_FromB','num of Etac', 1e3, 10, 1.e4)
    nJpsi_FromB = RooRealVar('nJpsi_FromB','num of J/Psi', 2e3, 10, 1.e4)
    nEtacRel_FromB = RooRealVar('nEtacRel','num of Etac', 0.0, 3.0)
    
    nEtac_FromB_1 = RooFormulaVar('nEtac_FromB_1','num of Etac','@0*@1',RooArgList(nEtac_FromB,rG1toG2))
    nEtac_FromB_2 = RooFormulaVar('nEtac_FromB_2','num of Etac','@0-@1',RooArgList(nEtac_FromB,nEtac_FromB_1))
    nJpsi_FromB_1 = RooFormulaVar('nJpsi_FromB_1','num of J/Psi','@0*@1',RooArgList(nJpsi_FromB,rG1toG2))
    nJpsi_FromB_2 = RooFormulaVar('nJpsi_FromB_2','num of J/Psi','@0-@1',RooArgList(nJpsi_FromB,nJpsi_FromB_1))

    
    mean_Jpsi = RooRealVar('mean_Jpsi','mean of gaussian', 2984.3, 2950, 3020)   
    mean_Etac = RooRealVar('mean_Etac','mean of gaussian', 3096.9, 3050, 3120) 
    gamma_eta = RooRealVar('gamma_eta','width of Br-W', gammaEtac, 10., 50. )
    gamma_eta.setConstant(True)
    spin_eta = RooRealVar('spin_eta','spin_eta', 0. )
    radius_eta = RooRealVar('radius_eta','radius', 1.)
    proton_m = RooRealVar('proton_m','proton mass', 938.3 )
    
    sigma_eta_1 = RooRealVar('sigma_eta_1','width of gaussian', 9., 0.1, 50.) 
    sigma_eta_2 = RooFormulaVar('sigma_eta_2','width of gaussian','@0/@1',RooArgList(sigma_eta_1,rNarToW))
    
    sigma_Jpsi_1 = RooFormulaVar('sigma_Jpsi_1','width of gaussian','@0/@1',RooArgList(sigma_eta_1,rEtaToJpsi))
    sigma_Jpsi_2 = RooFormulaVar('sigma_Jpsi_2','width of gaussian','@0/@1',RooArgList(sigma_Jpsi_1,rNarToW))    

    #Fit eta
    br_wigner = RooRelBreitWigner('br_wigner', 'br_wigner',Jpsi_M, mean_Etac, gamma_eta, spin_eta,radius_eta,proton_m,proton_m)
    
    gaussEta_1 = RooGaussian('gaussEta_1','gaussEta_1 PDF', Jpsi_M, RooFit.RooConst(0.0),  sigma_eta_1) #mean_Etac -> mean_Jpsi
    gaussEta_2 = RooGaussian('gaussEta_2','gaussEta_2 PDF', Jpsi_M, RooFit.RooConst(0.0),  sigma_eta_2) #mean_Etac -> mean_Jpsi
    
    bwxg_1 = RooFFTConvPdf('bwxg_1','breit-wigner (X) gauss', Jpsi_M, br_wigner, gaussEta_1) 
    bwxg_2 = RooFFTConvPdf('bwxg_2','breit-wigner (X) gauss', Jpsi_M, br_wigner, gaussEta_2) 
    
    #Fit J/psi
    gauss_1 = RooGaussian('gauss_1','gaussian PDF',Jpsi_M, mean_Jpsi, sigma_Jpsi_1) 
    gauss_2 = RooGaussian('gauss_2','gaussian PDF',Jpsi_M, mean_Jpsi, sigma_Jpsi_2) 
    
    
    ##Connection between parameters
    ##   RooFormulaVar f_1('f_1','f_1','@0*9.0',RooArgList(nEtac_2))
    
    ## Create constraints
    ##   RooGaussian constrNEta('constrNEta','constraint Etac',nEtac_1,f_1,RooConst(0.0)) 
    
    
    ##   RooAddPdf model('model','signal', RooArgList(bwxg_1, bwxg_2,gauss_1,gauss_2), RooArgList(nEtac_1,nEtac_2,nJpsi_1,nJpsi_2))  
    ##   RooAddPdf modelEtac('modelEtac','Etac signal', RooArgList(bwxg_1, bwxg_2), RooArgList(nEtac_1, nEtac_2))
    
    modelEtac_FromB = RooAddPdf('modelEtac_FromB','Etac signal', RooArgList(bwxg_1, bwxg_2), RooArgList(nEtac_FromB_1, nEtac_FromB_2))
    modelJpsi_FromB = RooAddPdf('modelJpsi_FromB','Jpsi signal', RooArgList(gauss_1, gauss_2), RooArgList(nJpsi_FromB_1, nJpsi_FromB_2))
    
    sample = RooCategory('sample','sample') 
    sample.defineType('Etac_FromB') 
    sample.defineType('Jpsi_FromB') 
    
    
    dataEtac_FromB = w.data('dsEtac_FromB')
    dataJpsi_FromB = w.data('dsJpsi_FromB')
    
    
    # Construct combined dataset in (Jpsi_M,sample)
    combData = RooDataSet('combData', 'combined data', RooArgSet(Jpsi_M), RooFit.Index(sample), RooFit.Import('Etac_FromB',dataEtac_FromB), RooFit.Import('Jpsi_FromB',dataJpsi_FromB)) 
    
    
    # Associate model with the physics state and model_ctl with the control state
    simPdf = RooSimultaneous('simPdf','simultaneous signal pdf',sample) 
    simPdf.addPdf(modelEtac_FromB,'Etac_FromB') 
    simPdf.addPdf(modelJpsi_FromB,'Jpsi_FromB') 
    
    
    #   getattr(w,'import')(model,RecycleConflictNodes())
    getattr(w,'import')(combData)
    getattr(w,'import')(simPdf)
    




def fitData(iPT, key):

    
    gROOT.Reset()    
    
    w = RooWorkspace('w',True)   
    
    getData_d(w, key, iPT)
    
    fillRelWorkspace(w)
    
    Jpsi_M = w.var('Jpsi_M')
    
    
    modelEtac_FromB = w.pdf('modelEtac_FromB')
    modelJpsi_FromB = w.pdf('modelJpsi_FromB')
    
    sample = w.cat('sample')
    simPdf = w.pdf('simPdf')
    combData = w.data('combData')
    
    
    dataEtac_FromB = w.data('dsEtac_FromB')
    dataJpsi_FromB = w.data('dsJpsi_FromB')
    
    
    sigma = w.var('sigma_eta_1')
    mean_Jpsi = w.var('mean_Jpsi')
    mean_Etac = w.var('mean_Etac')
    gamma_eta = w.var('gamma_eta')
    
    
    #if (iPT != 0):
    
        #f = TFile(homeDir+'Results/MC/MassFit/MC_Mass_fromB%s_2016_wksp.root'%(key),'READ') 
        #wMC = f.Get('w')
        #f.Close()
        
        #w.var('rNarToW').setVal(wMC.var('rNarToW').getValV())
        ##     w.var('rEtaToJpsi').setVal(wMC.var('rEtaToJpsi').getValV())
        #w.var('rG1toG2').setVal(wMC.var('rG1toG2').getValV())
        #w.var('mean_Jpsi').setVal(wMC.var('mean_Jpsi').getValV())
        
        #w.var('rNarToW').setConstant(True)
        ##     w.var('rEtaToJpsi').setConstant(True)
        #w.var('rG1toG2').setConstant(True)
        #w.var('mean_Jpsi').setConstant(True)
    
    r = simPdf.fitTo(combData,RooFit.Save(True)) 
    r = simPdf.fitTo(combData,RooFit.Minos(True), RooFit.Save(True)) 
    modelJpsi_FromB.fitTo(dataJpsi_FromB,RooFit.Minos(True), RooFit.Save(True))
    r = modelEtac_FromB.fitTo(dataEtac_FromB,RooFit.Minos(True), RooFit.Save(True))
    
    gROOT.ProcessLine('gStyle->SetOptStat(000000000)')
    gROOT.ProcessLine('gStyle->SetOptTitle(0)')

    frame1 = Jpsi_M.frame(RooFit.Title('#eta_c to p #bar{p} from_b')) 
    frame2 = Jpsi_M.frame(RooFit.Title('J/#psi to p #bar{p} from_b')) 
    
    dataEtac_FromB.plotOn(frame1)
    dataJpsi_FromB.plotOn(frame2)
    
    
    
    #chi2Etac = 0.
    #chi2Jpsi = 0.
    #chi2Etac = 0.
    #chi2Jpsi = 0.
    gaussEta_1 = w.pdf('gaussEta_1')
    gaussEta_2 = w.pdf('gaussEta_2')
    gauss_1 = w.pdf('gauss_1')
    gauss_2 = w.pdf('gauss_2')
    
        
    #modelEtac_FromB.paramOn(frame1,RooFit.Layout(0.68,0.99,0.99))
    #frame1.getAttText().SetTextSize(0.027) 
    modelEtac_FromB.plotOn(frame1,RooFit.Normalization(1.0,RooAbsReal.RelativeExpected))
    chi2Etac_FromB = frame1.chiSquare()
    #modelEtac_FromB.plotOn(frame1,RooFit.Components(RooArgSet(gaussEta_1,gaussEta_2)), RooFit.FillStyle(3005), RooFit.FillColor(kMagenta), RooFit.DrawOption('F'),RooFit.Normalization(1.0,RooAbsReal.RelativeExpected))
    print 'chi2Etac_FromB = ', chi2Etac_FromB
    
    #modelJpsi_FromB.paramOn(frame2,RooFit.Layout(0.68,0.99,0.99))
    #frame2.getAttText().SetTextSize(0.027)
    modelJpsi_FromB.plotOn(frame2,RooFit.Normalization(1.0,RooAbsReal.RelativeExpected))
    chi2Jpsi_FromB = frame2.chiSquare()
    #modelJpsi_FromB.plotOn(frame2, RooFit.Components(RooArgSet(gauss_1,gauss_2)), RooFit.FillStyle(3005), RooFit.FillColor(kMagenta), RooFit.DrawOption('F'), RooFit.Normalization(1.0,RooAbsReal.RelativeExpected))
    print 'chi2Jpsi_FromB = ', chi2Jpsi_FromB
    
    c = TCanvas('Masses_Fit','Masses Fit',600,800)
    c.Divide(1,2)
    c.cd(1).SetPad(.005, .505, .995, .995)
    #gPad.SetLeftMargin(0.15),  frame1.GetXaxis().SetTitle('(M - TRUEM)_{p #bar{p}} / [MeV/c^2]'),  
    frame1.Draw()
    frame1.SetMaximum(1.e3)
    frame1.SetMinimum(0.1)
    frame1.GetXaxis().SetTitleSize(0.14)
    frame1.GetXaxis().SetTitleSize(0.14)
    gPad.SetLogy()
    c.cd(2).SetPad(.005, .005, .995, .495)
    #gPad.SetLeftMargin(0.15),  frame2.GetXaxis().SetTitle('(M - TRUEM)_{p #bar{p}} / [MeV/c^2]'),  
    frame2.Draw()
    frame2.SetMaximum(1.e3)
    frame2.SetMinimum(0.1)
    frame2.GetXaxis().SetTitleSize(0.14)
    frame2.GetXaxis().SetTitleSize(0.14)
    gPad.SetLogy()
    
    
    
    nameTxt = ''
    nameRoot = ''
    nameWksp = ''
    namePic = ''
    
    if(iPT == 0):
    
        nameTex = homeDir+'Results/MC/MassFit/fit_Mass_fromB%s.txt'%(key)
        nameWksp = homeDir+'Results/MC/MassFit/MC_Mass_fromB%s_2016_wksp.root'%(key)
        nameRoot = homeDir+'Results/MC/MassFit/MC_Mass_fromB%s_Fit_plot.root'%(key)
        namePic = homeDir+'Results/MC/MassFit/MC_Mass_fromB%s_empty.pdf'%(key)    
    
    else:
    
        nameTex = homeDir+'Results/MC/MassFit/fit_Mass_fromB%s_PT%s.txt'%(key,iPT)
        nameWksp = homeDir+'Results/MC/MassFit/MC_Mass_fromB%s_2016_wksp_PT%s.root'%(key,iPT)
        nameRoot = homeDir+'Results/MC/MassFit/MC_Mass_fromB%s_Fit_plot_PT%s.root'%(key,iPT)
        namePic = homeDir+'Results/MC/MassFit/MC_Mass_fromB%s_PT%s.pdf'%(key,iPT)
    

    fo = open(nameTex,'w')
    fo.write('$chi^2 eta_c from-b$ %6.4f \n'  %(chi2Etac_FromB))
    fo.write('$chi^2 J/\psi from-b$ %6.4f \n\n' %(chi2Jpsi_FromB))

    fo.write("edm = %6.6f \n\n"%(r.edm()))

    fo.write("mean J/psi = %2.3f +/- %2.3f \n"  %( w.var("mean_Jpsi").getValV(),  w.var("mean_Jpsi").getError()))
    fo.write("mean eta(c) = %2.3f +/- %2.3f \n" %( w.var("mean_Etac").getValV(), w.var("mean_Etac").getError()))
    fo.write("nEtac_FromB = %6.1f +/- %6.1f, %3.2f \n"   %( w.var("nEtac_FromB").getValV(),  w.var("nEtac_FromB").getError(), w.var("nEtac_FromB").getValV()/w.var("nEtac_FromB").getError()))
    fo.write("nJpsi_FromB = %6.1f +/- %6.1f, %3.2f \n"   %( w.var("nJpsi_FromB").getValV(),  w.var("nJpsi_FromB").getError(), w.var("nJpsi_FromB").getValV()/w.var("nJpsi_FromB").getError())) 

    fo.write("rEtaToJpsi = %1.4f +/- %1.4f \n"  %( w.var("rEtaToJpsi").getValV(),  w.var("rEtaToJpsi").getError()))
    fo.write("rNarToW = %1.4f +/- %1.4F \n"     %( w.var("rNarToW").getValV(), w.var("rNarToW").getError()))
    fo.write("rG1toG2 = %1.4f +/- %1.4f \n"     %( w.var("rG1toG2").getValV(),  w.var("rG1toG2").getError()))

    fo.write("sigmaEtaNar = %6.4f +/- %6.4f \n" %( w.var("sigma_eta_1").getValV(),  w.var("sigma_eta_1").getError()))



    fo.close()
    
    w.writeToFile(nameWksp)
    fFit = TFile (nameRoot,'RECREATE')
    
    
    c.Write('')
    fFit.Write()
    fFit.Close()
    
    c.SaveAs(namePic)
    r.correlationMatrix().Print('v')
    r.globalCorr().Print('v')



def MC__Fit():

    key = '_cuted'
    #key = ''
    fitData(0, key)
    fitData(0, '')
    #nPTBins = 5 
    #for iPT in range(nPTBins):
        #fitData(iPT+1, key)  
    

#MC__Fit()
fitData(0, '')
