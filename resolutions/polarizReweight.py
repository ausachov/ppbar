from ROOT import *
from array import array
gROOT.LoadMacro("../libRooRelBreitWigner/RooRelBreitWigner.cxx+");
gROOT.LoadMacro("../BifurcatedCB/BifurcatedCB.cxx+");
homeDir = "/users/LHCb/zhovkovska/scripts/"

def polarizReweight():
    
    gStyle.SetOptStat(0)

    ptBins = array("f", [6500., 8000., 10000., 12000., 14000.])

    ntJpsiR = TChain("DecayTree")
    ntJpsiR.Add(homeDir+'MC/newMC/Jpsi/JpsiDiProton_MC_2016_AddBr.root')
    ntJpsiR.Add(homeDir+'MC/newMC/Jpsi/JpsiDiProton_MC_2015_AddBr.root')

    ntJpsi = ntJpsiR.CopyTree("")

    nEntries = ntJpsi.GetEntries()
    Jpsi_PT = array("d",[0])
    ProtonM_CosTheta = array("d",[0])

    ntJpsiR.SetBranchAddress("Jpsi_PT",Jpsi_PT)
    ntJpsiR.SetBranchAddress("ProtonM_CosTheta",ProtonM_CosTheta)

    hEff = TH1D("Eff","Eff", 4, ptBins)
    #hPTW = TH1D("hPTW","CosTheta", nEntries, -1., 1.)
    #hPT = TH1D("hPT","CosTheta", nEntries, -1., 1.)
    hPTW = TH1D("hPTW","PT", 50, 6500, 14000.)
    hPT = TH1D("hPT","PT", 50, 6500, 14000.)
    for iEn in range(nEntries):

        ntJpsiR.GetEntry(iEn+1)
        hPTW.Fill(Jpsi_PT[0], (1./(1+0.1/3.))*(1+0.1*ProtonM_CosTheta[0]**2))
        hPT.Fill(Jpsi_PT[0])


    c = TCanvas("TailEval","Tail Evaluation",800,500)
#    c.cd()

    hPT.SetLineColor(2)
    hPT.GetXaxis().SetTitle("p_{T} / [GeV/c]")
    hPT.Draw()
    hPTW.Draw("same")
    
    c.SaveAs("reweight_MC.pdf")
    
    cutReweight = "(1./(1-0.1/3.))*(1-0.1*ProtonM_CosTheta**2)"

    histEff = TH1D("histEff","histEff", 4, ptBins)
    histEffTot = TH1D("histEffTot","histEff", 1, 6500., 14000.)

    NW = hPTW.Integral(hPTW.FindBin(6500.), hPTW.FindBin(14000))
    N = hPT.Integral(hPT.FindBin(6500.), hPT.FindBin(14000.))
    histEffTot.SetBinContent( 1,1-N/float(NW))

    for iPT in range(4):
        #NW = ntJpsiR.GetEntries(cutReweight+"&& Jpsi_PT>%s && Jpsi_PT<%s"%(ptBins[iPT], ptBins[iPT+1]))
        #N = ntJpsiR.GetEntries("Jpsi_PT>%s && Jpsi_PT<%s"%(ptBins[iPT], ptBins[iPT+1]))
        NW = hPTW.Integral(hPTW.FindBin(ptBins[iPT]), hPTW.FindBin(ptBins[iPT+1]))
        N = hPT.Integral(hPT.FindBin(ptBins[iPT]), hPT.FindBin(ptBins[iPT+1]))
        histEff.SetBinContent( iPT+1,1-N/float(NW))
    
    canv = TCanvas("canv", "canv", 1000, 800)
    canv.cd(1)
    histEff.Draw()
    canv.SaveAs("eff.pdf")

    f = TFile(homeDir+"Results/effPolarization.root","RECREATE")
    histEff.Write()
    histEffTot.Write()
    f.Close()
    
polarizReweight()