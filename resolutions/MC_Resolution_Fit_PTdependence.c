
//#ifndef __CINT__
//#include "RooGlobalFunc.h"
//#endif
//#include "RooRealVar.h"
//#include "RooDataSet.h"
//#include "RooGaussian.h"
//#include "RooConstVar.h"
//#include "RooProdPdf.h"
//#include "RooAddPdf.h"
//#include "RooPolynomial.h"
//#include "TCanvas.h"
//#include "TAxis.h"
//#include "RooPlot.h"
//#include "RooFitResult.h"
//#include "RooRelBreitWigner.h"
#include <stdio.h>
#include <stdlib.h>
using namespace RooFit ;

void getData_d(RooWorkspace* w, Int_t iPT=0)
{
  
  const Int_t nPTBins = 5;
  const char *pt[nPTBins+1] = {"6500", "8000", "10000", "12000", "14000", "18000"};
  
  TChain * ntEtac=new TChain("DecayTree");
  TChain * ntJpsi=new TChain("DecayTree");
  
    
//  ntEtac->Add("../MC/Etac/2016/EtacDiProton_Detached_MC_2016_AddBr.root");
//  ntJpsi->Add("../MC/Jpsi/2016/JpsiDiProton_Detached_MC_2016_AddBr.root");
   
  ntEtac->Add("../MC/Etac/2016/EtacDiProton_MC_2016_AddBr.root");
  ntEtac->Add("../MC/Etac/2015/EtacDiProton_MC_2015_AddBr.root");
  ntJpsi->Add("../MC/Jpsi/2016/JpsiDiProton_MC_2016_AddBr.root");
  ntJpsi->Add("../MC/Jpsi/2015/JpsiDiProton_MC_2015_AddBr.root");
  
  TTree *treeEtac;
  TTree *treeJpsi;
  
  if(iPT == 0)
  {
    treeEtac = ntEtac->CopyTree("prompt");
    treeJpsi = ntJpsi->CopyTree("prompt");
  }
  else if ( iPT <= nPTBins )
  {  
    char LPT[20] = "Jpsi_PT > ";
    char RPT[20] = "Jpsi_PT < ";
    strcat(LPT,pt[iPT-1]);
    strcat(RPT,pt[iPT]);
    TCut cutJpsiPtL (LPT);
    TCut cutJpsiPtR (RPT);

    treeEtac = ntEtac->CopyTree("prompt" && cutJpsiPtL && cutJpsiPtR);
    treeJpsi = ntJpsi->CopyTree("prompt" && cutJpsiPtL && cutJpsiPtR);    
  }  
  else 
  {
    std::cout<<"Incorrect number of PT bin "<<iPT<<std::endl;
    std::cin.get();
  }
  
  RooRealVar Jpsi_M_res("Jpsi_M_res","Jpsi_M_res",-100.0,100.0) ;
  RooRealVar Jpsi_PT("Jpsi_PT","Jpsi_PT",6500.,18000.) ;
  RooDataSet dsEtac("dsEtac","dsEtac",treeEtac,RooArgSet(Jpsi_M_res,Jpsi_PT));
  RooDataSet dsJpsi("dsJpsi","dsJpsi",treeJpsi,RooArgSet(Jpsi_M_res,Jpsi_PT));
  w->import(dsEtac,RecycleConflictNodes());
  w->import(dsJpsi,RecycleConflictNodes());
  
//   dsEtac.Draw("");
//   dsJpsi.Draw("");
  
  delete ntEtac;
  delete ntJpsi;
}

void fillRelWorkspace(RooWorkspace *w)
{
  
  RooRealVar* Jpsi_M_res = w->var("Jpsi_M_res");
  RooRealVar* Jpsi_PT    = w->var("Jpsi_PT");

//   Jpsi_M_res->setBins(1000,"cache");
  
  Double_t ratioNtoW = 0.50;
  Double_t ratioEtaToJpsi = 0.88;
  Double_t ratioArea = 0.70;
  Double_t gammaEtac = 31.8;
  
  
  
  RooRealVar rEtaToJpsi("rEtaToJpsi","rEtaToJpsi", 0.01,5.);
  RooRealVar rNarToW("rNarToW","rNarToW",0.01,1.);
  RooRealVar rG1toG2("rG1toG2","rG1toG2",0.01,1.);
    
//  RooRealVar rEtaToJpsislope("rEtaToJpsislope","rEtaToJpsislope", -0,-1e2,1e2);
//  RooRealVar rNarToWslope("rNarToWslope","rNarToWslope",-0,-1e2,1e2);
//  RooRealVar rG1toG2slope("rG1toG2slope","rG1toG2slope",-0,-1e2,1e2);
    
//    RooRealVar rEtaToJpsislope("rEtaToJpsislope","rEtaToJpsislope", -0);
//    RooRealVar rNarToWslope("rNarToWslope","rNarToWslope",-0);
//    RooRealVar rG1toG2slope("rG1toG2slope","rG1toG2slope",-0);
//    
//  RooRealVar rEtaToJpsiBase("rEtaToJpsiBase","rEtaToJpsiBase", ratioEtaToJpsi, 0.01, 5.0);
//  RooRealVar rNarToWBase("rNarToWBase","rNarToWBase",ratioNtoW, 0.01, 1.0);
//  RooRealVar rG1toG2Base("rG1toG2Base","rG1toG2Base",ratioArea, 0.01, 1.0);
    
    
    
//  RooFormulaVar rEtaToJpsi("rEtaToJpsi","rEtaToJpsi","@0*(1+(@1-6500.)*@2/1000.)", RooArgSet(rEtaToJpsiBase, *Jpsi_PT, rEtaToJpsislope));
//  RooFormulaVar rNarToW("rNarToW","rNarToW","@0*(1+(@1-6500.)*@2/1000.)",RooArgSet(rNarToWBase, *Jpsi_PT, rNarToWslope));
//  RooFormulaVar rG1toG2("rG1toG2","rG1toG2","@0*(1+(@1-6500.)*@2/1000.)",RooArgSet(rG1toG2Base, *Jpsi_PT, rG1toG2slope));
    
    
    
    
  RooRealVar nEtac("nEtac","num of Etac", 1e3, 10, 1.e4);
  RooRealVar nJpsi("nJpsi","num of J/Psi", 2e3, 10, 1.e4);
  RooRealVar nEtacRel("nEtacRel","num of Etac", 0.0, 3.0);
  
//   RooFormulaVar nEtac_1("nEtac_1","num of Etac","@0*@1*@2",RooArgSet(nEtacRel,nJpsi,rG1toG2));
//   RooFormulaVar nEtac_2("nEtac_2","num of Etac","@0*@1-@2",RooArgSet(nEtacRel,nJpsi,nEtac_1));
  RooFormulaVar nEtac_1("nEtac_1","num of Etac","@0*@1",RooArgSet(nEtac,rG1toG2));
  RooFormulaVar nEtac_2("nEtac_2","num of Etac","@0-@1",RooArgSet(nEtac,nEtac_1));
  RooFormulaVar nJpsi_1("nJpsi_1","num of J/Psi","@0*@1",RooArgSet(nJpsi,rG1toG2));
  RooFormulaVar nJpsi_2("nJpsi_2","num of J/Psi","@0-@1",RooArgSet(nJpsi,nJpsi_1));

  
  RooRealVar mean_Jpsi("mean_Jpsi","mean of gaussian", 0.0, -50.0, 50.0) ;  
  RooRealVar mean_Etac("mean_Etac","mean of gaussian", 0.0, -50.0, 50.0) ;
  RooRealVar gamma_eta("gamma_eta","width of Br-W", gammaEtac, 10., 50. );
  RooRealVar spin_eta("spin_eta","spin_eta", 0. );
  RooRealVar radius_eta("radius_eta","radius", 1.);
  RooRealVar proton_m("proton_m","proton mass", 938.3 );

  RooRealVar sigma_eta_1("sigma_eta_1","width of gaussian", 9., 0.1, 50.) ;
  RooFormulaVar sigma_eta_2("sigma_eta_2","width of gaussian","@0/@1",RooArgSet(sigma_eta_1,rNarToW));
  
  RooFormulaVar sigma_Jpsi_1("sigma_Jpsi_1","width of gaussian","@0/@1",RooArgList(sigma_eta_1,rEtaToJpsi));
  RooFormulaVar sigma_Jpsi_2("sigma_Jpsi_2","width of gaussian","@0/@1",RooArgList(sigma_Jpsi_1,rNarToW));
  
  
  
  // Fit eta
  RooGaussian gaussEta_1("gaussEta_1","gaussEta_1 PDF", *Jpsi_M_res, /*RooConst(0)*/mean_Etac,  sigma_eta_1);
  RooGaussian gaussEta_2("gaussEta_2","gaussEta_2 PDF", *Jpsi_M_res, /*RooConst(0)*/mean_Etac,  sigma_eta_2);
  
  RooRelBreitWigner br_wigner("br_wigner", "br_wigner",*Jpsi_M_res, mean_Etac, gamma_eta, spin_eta,radius_eta,proton_m,proton_m);
  
  RooFFTConvPdf bwxg_1("bwxg_1","breit-wigner (X) gauss", *Jpsi_M_res, br_wigner, gaussEta_1) ;
  RooFFTConvPdf bwxg_2("bwxg_2","breit-wigner (X) gauss", *Jpsi_M_res, br_wigner, gaussEta_2) ;
  
  // Fit J/psi
  RooGaussian gauss_1("gauss_1","gaussian PDF",*Jpsi_M_res, mean_Jpsi, sigma_Jpsi_1) ;
  RooGaussian gauss_2("gauss_2","gaussian PDF",*Jpsi_M_res, mean_Jpsi, sigma_Jpsi_2) ;
  
  
  
  // Connection between parameters
//   RooFormulaVar f_1("f_1","f_1","@0*9.0",RooArgList(nEtac_2));
  
  // Create constraints
//   RooGaussian constrNEta("constrNEta","constraint Etac",nEtac_1,f_1,RooConst(0.0)) ;
  
  
//   RooAddPdf model("model","signal", RooArgList(bwxg_1, bwxg_2,gauss_1,gauss_2), RooArgList(nEtac_1,nEtac_2,nJpsi_1,nJpsi_2));  
//   RooAddPdf modelEtac("modelEtac","Etac signal", RooArgList(bwxg_1, bwxg_2), RooArgList(nEtac_1, nEtac_2));
  
  RooAddPdf modelEtac("modelEtac","Etac signal", RooArgList(gaussEta_1, gaussEta_2), RooArgList(nEtac_1, nEtac_2));
  RooAddPdf modelJpsi("modelJpsi","Jpsi signal", RooArgList(gauss_1, gauss_2), RooArgList(nJpsi_1, nJpsi_2));
 
  
  RooCategory sample("sample","sample") ;
  sample.defineType("Etac") ;
  sample.defineType("Jpsi") ;
  
  
  RooDataSet *dataEtac = (RooDataSet*) w->data("dsEtac");
  RooDataSet *dataJpsi = (RooDataSet*) w->data("dsJpsi");
  
  
  // Construct combined dataset in (Jpsi_M_res,sample)
  RooDataSet combData("combData","combined data",RooArgSet(*Jpsi_M_res,*Jpsi_PT),Index(sample),Import("Etac",*dataEtac),Import("Jpsi",*dataJpsi)) ;
  
  
  // Associate model with the physics state and model_ctl with the control state
  RooSimultaneous simPdf("simPdf","simultaneous signal pdf",sample) ;
  simPdf.addPdf(modelEtac,"Etac") ;
  simPdf.addPdf(modelJpsi,"Jpsi") ;
  
  
//   w->import(model,RecycleConflictNodes());
  w->import(combData,RecycleConflictNodes());
  w->import(simPdf,RecycleConflictNodes());
  
}



void fitData(Int_t iPT)
{
  
  gROOT->Reset();
  
  TProof *proof = TProof::Open("");
  
  
  RooWorkspace *w = new RooWorkspace("w",kTRUE);   
  
  getData_d(w, iPT);
  
  fillRelWorkspace(w);
  
//   RooAbsPdf *model = w->pdf("model");
  RooRealVar *Jpsi_M_res = w->var("Jpsi_M_res");
  RooRealVar *Jpsi_PT = w->var("Jpsi_PT");

  RooAbsPdf *modelEtac = w->pdf("modelEtac");
  RooAbsPdf *modelJpsi = w->pdf("modelJpsi");
  
  RooCategory *sample = w->cat("sample");
  RooAbsPdf *simPdf = w->pdf("simPdf");
  RooDataSet *combData = (RooDataSet*) w->data("combData");
    
  RooDataSet *dataEtac = (RooDataSet*) w->data("dsEtac");
  RooDataSet *dataJpsi = (RooDataSet*) w->data("dsJpsi");
    
  RooRealVar *sigma = w->var("sigma_eta_1");
  RooRealVar *mean_Jpsi = w->var("mean_Jpsi");
  RooRealVar *mean_Etac = w->var("mean_Etac");
  RooRealVar *gamma_eta = w->var("gamma_eta");
    
  
//   mean_Etac->setConstant(kTRUE);
//   mean_Jpsi->setConstant(kTRUE);
//   simPdf->fitTo(*combData,/*Extended(),Minos(kTRUE),*//*Offset(kTRUE),*//*Integrate(kTRUE),*/Save(kTRUE)) ;
//   RooFitResult *r2 = modelJpsi->fitTo(*dataJpsi,/*Extended(),Minos(kTRUE),*//*Offset(kTRUE),*//*Integrate(kTRUE),*/Save(kTRUE)) ;
//   RooFitResult *r1 = modelEtac->fitTo(*dataEtac,/*Extended(),Minos(kTRUE),*//*Offset(kTRUE),*//*Integrate(kTRUE),*/Save(kTRUE)) ;
//   mean_Etac->setConstant(kFALSE);
//   mean_Jpsi->setConstant(kFALSE);
  RooFitResult *r = simPdf->fitTo(*combData,/*Extended(),Minos(kTRUE),*//*Offset(kTRUE),*//*Integrate(kTRUE),*/Save(kTRUE)) ;
//   model->fitTo(*dataEtac,/*Extended(),*//*Minos(kTRUE),*/Offset(kTRUE),/*Integrate(kTRUE),*/Save(kTRUE)) ;
//   RooFitResult *r = model->fitTo(*dataEtac,/*Extended(),*//*Minos(kTRUE),*//*Offset(kTRUE),*//*Integrate(kTRUE),*/Save(kTRUE)) ;
  
  
    
  RooPlot *frame1 = Jpsi_M_res->frame(Title("#eta_{c} to p #bar{p}")) ;
  RooPlot *frame2 = Jpsi_M_res->frame(Title("J/#psi to p #bar{p}")) ;
      
  dataEtac->plotOn(frame1/*,Binning(Jpsi_Tz->getBinning("Bins"))*/);
  dataJpsi->plotOn(frame2/*,Binning(Jpsi_Tz->getBinning("Bins"))*/);
  
  
  
  Double_t chi2Etac=0;
  Double_t chi2Jpsi=0;
  RooAbsPdf *gaussEta_1 = w->pdf("gaussEta_1");
  RooAbsPdf *gaussEta_2 = w->pdf("gaussEta_2");
  RooAbsPdf *gauss_1 = w->pdf("gauss_1");
  RooAbsPdf *gauss_2 = w->pdf("gauss_2");
  
  
  
  modelEtac->paramOn(frame1);
  modelEtac->plotOn(frame1,Normalization(1.0,RooAbsReal::RelativeExpected));
  chi2Etac = frame1->chiSquare();
  modelEtac->plotOn(frame1,Components(RooArgSet(*gaussEta_1,gaussEta_2)), FillStyle(3005), FillColor(kMagenta), DrawOption("F"),Normalization(1.0,RooAbsReal::RelativeExpected));
  
  modelJpsi->paramOn(frame2);
  modelJpsi->plotOn(frame2,Normalization(1.0,RooAbsReal::RelativeExpected));
  chi2Jpsi = frame2->chiSquare();
  modelJpsi->plotOn(frame2,Components(RooArgSet(*gauss_1,gauss_2)),FillStyle(3005),FillColor(kMagenta),DrawOption("F"), Normalization(1.0,RooAbsReal::RelativeExpected));
      
    
  TCanvas* c = new TCanvas("Masses_Fit","Masses Fit",800,900);
  c->Divide(1,2);
  c->cd(1)->SetPad(.005, .505, .995, .995);
  gPad->SetLeftMargin(0.15) ; frame1->GetXaxis()->SetTitle("M(p #bar{p}) / [MeV/c^{2}]") ; frame1->Draw();
  frame1->SetMinimum(1.);
  gPad->SetLogy();
  c->cd(2)->SetPad(.005, .005, .995, .495);
  gPad->SetLeftMargin(0.15) ; frame2->GetXaxis()->SetTitle("M(p #bar{p}) / [MeV/c^{2}]") ; frame2->Draw();
  frame2->SetMinimum(1.);
  gPad->SetLogy();
  
  
  
  char nameTxt[50];
  char nameRoot[50];
  char nameWksp[80];
  
  if(iPT == 0)
  {
    sprintf(nameTxt,  "results/MC/MassFit/fitRes.txt");
    sprintf(nameWksp, "results/MC/MassFit/MC_2016_wksp.root");
    sprintf(nameRoot, "results/MC/MassFit/MC_Mass_Fit_plot.root");
  }
  else
  {
    sprintf(nameTxt,  "results/MC/MassFit/fitRes_PT%d.txt", iPT);
    sprintf(nameWksp, "results/MC/MassFit/MC_2016_wksp_PT%d.root", iPT);
    sprintf(nameRoot, "results/MC/MassFit/MC_Mass_Fit_plot_PT%d.root", iPT);
  }
  
  std::fstream fo;
  TFile *fFit;
  fo.open(nameTxt,std::ios::out);
  w->writeToFile(nameWksp);
  fFit = new TFile (nameRoot,"RECREATE");
  
  
  c->Write("");
  fFit->Write();
  fFit->Close();
//   fo<<"edm = "<<r->edm()<<std::endl<<std::endl;
//   fo<<"chi2 Etac = "<<chi2Etac<<std::endl;
//   fo<<"chi2 Jpsi = "<<chi2Jpsi<<std::endl;
//   fo<<"mean J/psi = "<<w->var("mean_Jpsi")->getValV()<<" err = "<<w->var("mean_Jpsi")->getError()<<std::endl;
//   fo<<"mean eta(c) = "<<w->var("mean_Etac")->getValV()<<" err = "<<w->var("mean_Etac")->getError()<<std::endl;
//   fo<<"nEtac = "<<w->var("nEtac")->getValV()<<" err = "<<w->var("nEtac")->getError();
//   fo<<"   "<<w->var("nEtac")->getValV()/w->var("nEtac")->getError()<<std::endl;
//   fo<<"nJpsi = "<<w->var("nJpsi")->getValV()<<" err = "<<w->var("nJpsi")->getError();
//   fo<<"   "<<w->var("nJpsi")->getValV()/w->var("nJpsi")->getError()<<std::endl;
//   fo<<"sigmaEtaNar = "<<w->var("sigma_eta_1")->getValV()<<" err = "<<w->var("sigma_eta_1")->getError()<<std::endl<<std::endl<<std::endl;
  r->printMultiline(fo, 8);
  fo.close();
  
//   r->correlationMatrix().Print("v");
//   std::cin.get();
}


void MC_Resolution_Fit()
{
  fitData(0);
  for (Int_t iPT=1; iPT<=5; iPT++)
    fitData(iPT);  
}