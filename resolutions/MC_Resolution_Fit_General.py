from ROOT import *

gROOT.LoadMacro("../libRooRelBreitWigner/RooRelBreitWigner.cxx+");
gROOT.LoadMacro("../BifurcatedCB/BifurcatedCB.cxx+");
homeDir = "/users/LHCb/zhovkovska/scripts/"

cutApriori_Dict = {"DiProton":"Jpsi_Hlt1DiProtonDecision_TOS && \
                                  ProtonP_P>12.5e3 && ProtonM_P>12.5e3 && \
                                  ProtonP_PT>2000 && ProtonM_PT>2000 && \
                                  ProtonP_PT/ProtonP_P>0.0366 && ProtonM_PT/ProtonM_P>0.0366 && \
                                  ProtonP_PIDp>20 && ProtonM_PIDp>20 && \
                                  (ProtonP_PIDp-ProtonP_PIDK)>15 && (ProtonM_PIDp-ProtonM_PIDK)>15 && \
                                  ProtonP_TRACK_CHI2NDOF<2.5 && ProtonM_TRACK_CHI2NDOF<2.5 && \
                                  Jpsi_ENDVERTEX_CHI2<3.5 && \
                                  Jpsi_Y>2 && Jpsi_Y<4.5 && \
                                  Jpsi_PT>6500 && \
                                  ProtonP_TRACK_GhostProb<0.2 && ProtonP_TRACK_GhostProb<0.2 && \
                                  ProtonP_TRACK_CloneDist<0 && ProtonM_TRACK_CloneDist<0",
}


def getData_d(w, iPT=0):
    
    nPTBins = 5
    pt = (['6500', '8000'], ['8000', '10000'], ['10000', '12000'], ['12000', '14000'], ['14000', '18000'])
    #pt = (['6500', '7000'], ['7000', '8000'], ['8000', '9000'], ['9000', '10000'] ,['10000', '11000'], ['11000', '12000'],['12000', '13000'], ['13000', '14000'],['14000', '16000'],['16000', '18000'])
    
    ntEtac_Prompt = TChain('DecayTree')
    ntJpsi_Prompt = TChain('DecayTree')
    ntEtac_FromB = TChain('DecayTree')
    ntJpsi_FromB = TChain('DecayTree')

    #-----------old MC--------------------------------------------------------    
    #ntEtac_Prompt.Add(homeDir+'MC/newMC/Etac/EtacDiProton_MC_2016_AddBr.root')
    #ntEtac_Prompt.Add(homeDir+'MC/newMC/Etac/EtacDiProton_MC_2015_AddBr.root')
    #ntJpsi_Prompt.Add(homeDir+'MC/newMC/Jpsi/JpsiDiProton_MC_2016_AddBr.root')
    #ntJpsi_Prompt.Add(homeDir+'MC/newMC/Jpsi/JpsiDiProton_MC_2015_AddBr.root')
    #ntEtac_FromB.Add(homeDir+'MC/newMC/incl_b/EtacDiProton_MC_2015_incl_b_AddBr.root')
    #ntEtac_FromB.Add(homeDir+'MC/newMC/incl_b/EtacDiProton_MC_2016_incl_b_AddBr.root')
    #ntJpsi_FromB.Add(homeDir+'MC/newMC/Jpsi/JpsiDiProton_MC_2016_AddBr.root')
    #ntJpsi_FromB.Add(homeDir+'MC/newMC/Jpsi/JpsiDiProton_MC_2015_AddBr.root')


    #-----------new MC--------------------------------------------------------    
    ntEtac_Prompt.Add(homeDir+'MC/newMC_spring/Etac_Pr_all_l0TOS_allSelected_AddBr.root')
    ntJpsi_Prompt.Add(homeDir+'MC/newMC_spring/Jpsi_Pr_all_l0TOS_allSelected_AddBr.root')
    ntEtac_FromB.Add(homeDir+'MC/newMC_spring/incl_b_Pr_all_l0TOS_allSelected_AddBr.root')
    ntJpsi_FromB.Add(homeDir+'MC/newMC_spring/Jpsi_Pr_all_l0TOS_allSelected_AddBr.root')
    
    treeEtac_Prompt = TTree()
    treeJpsi_Prompt = TTree()
    treeEtac_FromB = TTree()
    treeJpsi_FromB = TTree()
    
    cut = cutApriori_Dict["DiProton"] 
    if(iPT == 0):
        
        treeEtac_Prompt = ntEtac_Prompt.CopyTree("prompt" + "&&" + cut)
        treeJpsi_Prompt = ntJpsi_Prompt.CopyTree("prompt" + "&&" + cut)
        treeEtac_FromB = ntEtac_FromB.CopyTree("sec" + "&&" + cut)
        treeJpsi_FromB = ntJpsi_FromB.CopyTree("sec" + "&&" + cut)

    elif ( iPT <= nPTBins ):
        cutPrompt = 'prompt'
        cutFromB = 'sec'
        cutJpsiPtL = 'Jpsi_PT > %s'%(pt[iPT-1][0])
        cutJpsiPtR = 'Jpsi_PT < %s'%(pt[iPT-1][1])
        
        treeEtac_Prompt = ntEtac_Prompt.CopyTree(cutPrompt + '&&' + cutJpsiPtL + '&&' + cutJpsiPtR  + "&&" + cut)
        treeJpsi_Prompt = ntJpsi_Prompt.CopyTree(cutPrompt + '&&' + cutJpsiPtL + '&&' + cutJpsiPtR  + "&&" + cut)
        treeEtac_FromB = ntEtac_FromB.CopyTree(cutFromB + '&&' + cutJpsiPtL + '&&' + cutJpsiPtR  + "&&" + cut)
        treeJpsi_FromB = ntJpsi_FromB.CopyTree(cutFromB + '&&' + cutJpsiPtL + '&&' + cutJpsiPtR  + "&&" + cut)
      
    else:
        print ('Incorrect number of PT bin %s'%(iPT))
    
    
    Jpsi_M_res = RooRealVar ('Jpsi_M_res','Jpsi_M_res',-100.0,100.0)  #!!!!!!!!!!!!!!!!!! change from (-100, 100)
    dsEtac_Prompt = RooDataSet('dsEtac_Prompt','dsEtac_Prompt',treeEtac_Prompt,RooArgSet(Jpsi_M_res))
    dsJpsi_Prompt = RooDataSet('dsJpsi_Prompt','dsJpsi_Prompt',treeJpsi_Prompt,RooArgSet(Jpsi_M_res))
    dsEtac_FromB = RooDataSet('dsEtac_FromB','dsEtac_FromB',treeEtac_FromB,RooArgSet(Jpsi_M_res))
    dsJpsi_FromB = RooDataSet('dsJpsi_FromB','dsJpsi_FromB',treeJpsi_FromB,RooArgSet(Jpsi_M_res))
    getattr(w,'import')(dsEtac_Prompt)
    getattr(w,'import')(dsJpsi_Prompt)
    getattr(w,'import')(dsEtac_FromB)
    getattr(w,'import')(dsJpsi_FromB)
    
    #  dsEtac.Draw('')
    #  dsJpsi.Draw('')
    


def fillRelWorkspace(w):

    
    Jpsi_M_res = w.var('Jpsi_M_res')
    #Jpsi_M_res.setBins(1000,'cache')
    
    ratioNtoW = 0.50
    ratioEtaToJpsi = 0.88
    ratioArea = 0.70
    #gammaEtac = 31.8
    gammaEtac = 29.7
    
    
    
    rEtaToJpsi = RooRealVar('rEtaToJpsi','rEtaToJpsi', ratioEtaToJpsi, 0.01, 5.0)
    rNarToW = RooRealVar('rNarToW','rNarToW',ratioNtoW, 0.01, 1.0)
    rG1toG2 = RooRealVar('rG1toG2','rG1toG2',ratioArea, 0.01, 1.0)
    #rEtaToJpsi = RooRealVar('rEtaToJpsi','rEtaToJpsi', ratioEtaToJpsi, 0.01, 5.0)
    #rNarToW = RooRealVar('rNarToW','rNarToW', 1.0)
    #rG1toG2 = RooRealVar('rG1toG2','rG1toG2', 1.0)
    nEtac_Prompt = RooRealVar('nEtac_Prompt','num of Etac Prompt', 1e3, 10, 5.e4)
    nJpsi_Prompt = RooRealVar('nJpsi_Prompt','num of J/Psi Prompt', 2e3, 10, 5.e4)
    nEtacRel_Prompt = RooRealVar('nEtacRel_Prompt','num of Etac Prompt', 0.0, 3.0)
    nEtac_FromB = RooRealVar('nEtac_FromB','num of Etac', 1e3, 10, 1.e4)
    nJpsi_FromB = RooRealVar('nJpsi_FromB','num of J/Psi', 2e3, 10, 1.e4)
    nEtacRel_FromB = RooRealVar('nEtacRel','num of Etac', 0.0, 3.0)
    
    #  nEtac_1 = RooFormulaVar('nEtac_1','num of Etac','@0*@1*@2',RooArgSet(nEtacRel,nJpsi,rG1toG2))
    #  nEtac_2 = RooFormulaVar('nEtac_2','num of Etac','@0*@1-@2',RooArgSet(nEtacRel,nJpsi,nEtac_1))
    nEtac_Prompt_1 = RooFormulaVar('nEtac_Prompt_1','num of Etac','@0*@1',RooArgList(nEtac_Prompt,rG1toG2))
    nEtac_Prompt_2 = RooFormulaVar('nEtac_Prompt_2','num of Etac','@0-@1',RooArgList(nEtac_Prompt,nEtac_Prompt_1))
    nJpsi_Prompt_1 = RooFormulaVar('nJpsi_Prompt_1','num of J/Psi','@0*@1',RooArgList(nJpsi_Prompt,rG1toG2))
    nJpsi_Prompt_2 = RooFormulaVar('nJpsi_Prompt_2','num of J/Psi','@0-@1',RooArgList(nJpsi_Prompt,nJpsi_Prompt_1))
    
    nEtac_FromB_1 = RooFormulaVar('nEtac_FromB_1','num of Etac','@0*@1',RooArgList(nEtac_FromB,rG1toG2))
    nEtac_FromB_2 = RooFormulaVar('nEtac_FromB_2','num of Etac','@0-@1',RooArgList(nEtac_FromB,nEtac_FromB_1))
    nJpsi_FromB_1 = RooFormulaVar('nJpsi_FromB_1','num of J/Psi','@0*@1',RooArgList(nJpsi_FromB,rG1toG2))
    nJpsi_FromB_2 = RooFormulaVar('nJpsi_FromB_2','num of J/Psi','@0-@1',RooArgList(nJpsi_FromB,nJpsi_FromB_1))

    
    mean_Jpsi = RooRealVar('mean_Jpsi','mean of gaussian', 0.0, -50.0, 50.0)   
    mean_Etac = RooRealVar('mean_Etac','mean of gaussian', 0.0, -50.0, 50.0) 
    gamma_eta = RooRealVar('gamma_eta','width of Br-W', gammaEtac, 10., 50. )
    spin_eta = RooRealVar('spin_eta','spin_eta', 0. )
    radius_eta = RooRealVar('radius_eta','radius', 1.)
    proton_m = RooRealVar('proton_m','proton mass', 938.3 )
    
    sigma_eta_1 = RooRealVar('sigma_eta_1','width of gaussian', 9., 0.1, 50.) 
    sigma_eta_2 = RooFormulaVar('sigma_eta_2','width of gaussian','@0/@1',RooArgList(sigma_eta_1,rNarToW))
    
    sigma_Jpsi_1 = RooFormulaVar('sigma_Jpsi_1','width of gaussian','@0/@1',RooArgList(sigma_eta_1,rEtaToJpsi))
    sigma_Jpsi_2 = RooFormulaVar('sigma_Jpsi_2','width of gaussian','@0/@1',RooArgList(sigma_Jpsi_1,rNarToW))    

    #Prompt    
    #Fit eta
    br_wigner = RooRelBreitWigner('br_wigner', 'br_wigner',Jpsi_M_res, mean_Etac, gamma_eta, spin_eta,radius_eta,proton_m,proton_m)
    
    gaussEta_1 = RooGaussian('gaussEta_1','gaussEta_1 PDF', Jpsi_M_res, mean_Etac,  sigma_eta_1) #mean_Etac -> mean_Jpsi
    gaussEta_2 = RooGaussian('gaussEta_2','gaussEta_2 PDF', Jpsi_M_res, mean_Etac,  sigma_eta_2) #mean_Etac -> mean_Jpsi
    
    bwxg_1 = RooFFTConvPdf('bwxg_1','breit-wigner (X) gauss', Jpsi_M_res, br_wigner, gaussEta_1) 
    bwxg_2 = RooFFTConvPdf('bwxg_2','breit-wigner (X) gauss', Jpsi_M_res, br_wigner, gaussEta_2) 
    
    #Fit J/psi
    gauss_1 = RooGaussian('gauss_1','gaussian PDF',Jpsi_M_res, mean_Jpsi, sigma_Jpsi_1) 
    gauss_2 = RooGaussian('gauss_2','gaussian PDF',Jpsi_M_res, mean_Jpsi, sigma_Jpsi_2) 
    
    
    ##Connection between parameters
    ##   RooFormulaVar f_1('f_1','f_1','@0*9.0',RooArgList(nEtac_2))
    
    ## Create constraints
    ##   RooGaussian constrNEta('constrNEta','constraint Etac',nEtac_1,f_1,RooConst(0.0)) 
    
    
    ##   RooAddPdf model('model','signal', RooArgList(bwxg_1, bwxg_2,gauss_1,gauss_2), RooArgList(nEtac_1,nEtac_2,nJpsi_1,nJpsi_2))  
    ##   RooAddPdf modelEtac('modelEtac','Etac signal', RooArgList(bwxg_1, bwxg_2), RooArgList(nEtac_1, nEtac_2))
    
    modelEtac_Prompt = RooAddPdf('modelEtac_Prompt','Etac signal', RooArgList(gaussEta_1, gaussEta_2), RooArgList(nEtac_Prompt_1, nEtac_Prompt_2))
    modelJpsi_Prompt = RooAddPdf('modelJpsi_Prompt','Jpsi signal', RooArgList(gauss_1, gauss_2), RooArgList(nJpsi_Prompt_1, nJpsi_Prompt_2))
    #modelEtac_Prompt = RooAddPdf('modelEtac_Prompt','Etac signal', RooArgList(gaussEta_1), RooArgList(nEtac_Prompt_1))
    #modelJpsi_Prompt = RooAddPdf('modelJpsi_Prompt','Jpsi signal', RooArgList(gauss_1), RooArgList(nJpsi_Prompt_1))
    
    modelEtac_FromB = RooAddPdf('modelEtac_FromB','Etac signal', RooArgList(gaussEta_1, gaussEta_2), RooArgList(nEtac_FromB_1, nEtac_FromB_2))
    modelJpsi_FromB = RooAddPdf('modelJpsi_FromB','Jpsi signal', RooArgList(gauss_1, gauss_2), RooArgList(nJpsi_FromB_1, nJpsi_FromB_2))
    #modelEtac_FromB = RooAddPdf('modelEtac_FromB','Etac signal', RooArgList(gaussEta_1), RooArgList(nEtac_FromB_1))
    #modelJpsi_FromB = RooAddPdf('modelJpsi_FromB','Jpsi signal', RooArgList(gauss_1), RooArgList(nJpsi_FromB_1))
    
    sample = RooCategory('sample','sample') 
    sample.defineType('Etac_Prompt') 
    sample.defineType('Jpsi_Prompt') 
    sample.defineType('Etac_FromB') 
    sample.defineType('Jpsi_FromB') 
    
    
    #dataEtac = (RooDataSet) w.data('dsEtac')
    #dataJpsi = (RooDataSet) w.data('dsJpsi')
    dataEtac_Prompt = w.data('dsEtac_Prompt')
    dataJpsi_Prompt = w.data('dsJpsi_Prompt')
    dataEtac_FromB = w.data('dsEtac_FromB')
    dataJpsi_FromB = w.data('dsJpsi_FromB')
    
    
    # Construct combined dataset in (Jpsi_M_res,sample)
    combData = RooDataSet('combData', 'combined data', RooArgSet(Jpsi_M_res), RooFit.Index(sample), RooFit.Import('Etac_Prompt',dataEtac_Prompt), RooFit.Import('Jpsi_Prompt',dataJpsi_Prompt), RooFit.Import('Etac_FromB',dataEtac_FromB), RooFit.Import('Jpsi_FromB',dataJpsi_FromB)) 
    
    
    # Associate model with the physics state and model_ctl with the control state
    simPdf = RooSimultaneous('simPdf','simultaneous signal pdf',sample) 
    simPdf.addPdf(modelEtac_Prompt,'Etac_Prompt') 
    simPdf.addPdf(modelJpsi_Prompt,'Jpsi_Prompt') 
    simPdf.addPdf(modelEtac_FromB,'Etac_FromB') 
    simPdf.addPdf(modelJpsi_FromB,'Jpsi_FromB') 
    
    
    #   getattr(w,'import')(model,RecycleConflictNodes())
    getattr(w,'import')(combData)
    getattr(w,'import')(simPdf)
    


def fillRelWorkspaceCB(w):

    
    Jpsi_M_res = w.var('Jpsi_M_res')
    #Jpsi_M_res.setBins(1000,'cache')
    
    ratioNtoW = 0.50
    ratioEtaToJpsi = 0.88
    ratioArea = 0.70
    #gammaEtac = 31.8
    gammaEtac = 29.7
    
    
    
    rEtaToJpsi = RooRealVar('rEtaToJpsi','rEtaToJpsi', ratioEtaToJpsi, 0.01, 5.0)
    rNarToW = RooRealVar('rNarToW','rNarToW',ratioNtoW, 0.01, 1.0)
    rG1toG2 = RooRealVar('rG1toG2','rG1toG2',ratioArea, 0.01, 1.0)
    #rEtaToJpsi = RooRealVar('rEtaToJpsi','rEtaToJpsi', ratioEtaToJpsi, 0.01, 5.0)
    #rNarToW = RooRealVar('rNarToW','rNarToW', 1.0)
    #rG1toG2 = RooRealVar('rG1toG2','rG1toG2', 1.0)
    nEtac_Prompt = RooRealVar('nEtac_Prompt','num of Etac Prompt', 1e3, 10, 5.e4)
    nJpsi_Prompt = RooRealVar('nJpsi_Prompt','num of J/Psi Prompt', 2e3, 10, 5.e4)
    nEtacRel_Prompt = RooRealVar('nEtacRel_Prompt','num of Etac Prompt', 0.0, 3.0)
    nEtac_FromB = RooRealVar('nEtac_FromB','num of Etac', 1e3, 10, 1.e4)
    nJpsi_FromB = RooRealVar('nJpsi_FromB','num of J/Psi', 2e3, 10, 1.e4)
    nEtacRel_FromB = RooRealVar('nEtacRel','num of Etac', 0.0, 3.0)
    
    #  nEtac_1 = RooFormulaVar('nEtac_1','num of Etac','@0*@1*@2',RooArgSet(nEtacRel,nJpsi,rG1toG2))
    #  nEtac_2 = RooFormulaVar('nEtac_2','num of Etac','@0*@1-@2',RooArgSet(nEtacRel,nJpsi,nEtac_1))
    nEtac_Prompt_1 = RooFormulaVar('nEtac_Prompt_1','num of Etac','@0*@1',RooArgList(nEtac_Prompt,rG1toG2))
    nEtac_Prompt_2 = RooFormulaVar('nEtac_Prompt_2','num of Etac','@0-@1',RooArgList(nEtac_Prompt,nEtac_Prompt_1))
    nJpsi_Prompt_1 = RooFormulaVar('nJpsi_Prompt_1','num of J/Psi','@0*@1',RooArgList(nJpsi_Prompt,rG1toG2))
    nJpsi_Prompt_2 = RooFormulaVar('nJpsi_Prompt_2','num of J/Psi','@0-@1',RooArgList(nJpsi_Prompt,nJpsi_Prompt_1))
    
    nEtac_FromB_1 = RooFormulaVar('nEtac_FromB_1','num of Etac','@0*@1',RooArgList(nEtac_FromB,rG1toG2))
    nEtac_FromB_2 = RooFormulaVar('nEtac_FromB_2','num of Etac','@0-@1',RooArgList(nEtac_FromB,nEtac_FromB_1))
    nJpsi_FromB_1 = RooFormulaVar('nJpsi_FromB_1','num of J/Psi','@0*@1',RooArgList(nJpsi_FromB,rG1toG2))
    nJpsi_FromB_2 = RooFormulaVar('nJpsi_FromB_2','num of J/Psi','@0-@1',RooArgList(nJpsi_FromB,nJpsi_FromB_1))

    
    mean_Jpsi = RooRealVar('mean_Jpsi','mean of gaussian', 0.0, -50.0, 50.0)   
    mean_Etac = RooRealVar('mean_Etac','mean of gaussian', 0.0, -50.0, 50.0) 
    gamma_eta = RooRealVar('gamma_eta','width of Br-W', gammaEtac, 10., 50. )
    spin_eta = RooRealVar('spin_eta','spin_eta', 0. )
    radius_eta = RooRealVar('radius_eta','radius', 1.)
    proton_m = RooRealVar('proton_m','proton mass', 938.3 )
    

    alpha_eta_1 = RooRealVar('alpha_eta_1','alpha of CB', 1., 0.0, 10.) 
    alpha_eta_2 = RooRealVar('alpha_eta_2','alpha of CB', 1., 0.0, 10.) 
    n_eta_1 = RooRealVar('n_eta_1','n of CB', 1., 0.0, 100.) 
    n_eta_2 = RooRealVar('n_eta_2','n of CB', 1., 0.0, 100.) 

    sigma_eta_1 = RooRealVar('sigma_eta_1','width of gaussian', 9., 0.1, 50.) 
    sigma_eta_2 = RooFormulaVar('sigma_eta_2','width of gaussian','@0/@1',RooArgList(sigma_eta_1,rNarToW))
    
    sigma_Jpsi_1 = RooFormulaVar('sigma_Jpsi_1','width of gaussian','@0/@1',RooArgList(sigma_eta_1,rEtaToJpsi))
    sigma_Jpsi_2 = RooFormulaVar('sigma_Jpsi_2','width of gaussian','@0/@1',RooArgList(sigma_Jpsi_1,rNarToW))    

    #Prompt    
    #Fit eta
    br_wigner = RooRelBreitWigner('br_wigner', 'br_wigner',Jpsi_M_res, mean_Etac, gamma_eta, spin_eta,radius_eta,proton_m,proton_m)
    
    cb_etac_1 = BifurcatedCB("cb_etac_1", "Cystal Ball Function", Jpsi_M_res, mean_Etac, sigma_eta_1, alpha_eta_1, n_eta_1, alpha_eta_1, n_eta_1)
    #cb_etac_2 = BifurcatedCB("cb_etac_2", "Cystal Ball Function", Jpsi_M_res, mean_Etac, sigma_eta_1, alpha_eta_2, n_eta_2);

    bwxg_1 = RooFFTConvPdf('bwxg_1','breit-wigner (X) gauss', Jpsi_M_res, br_wigner, cb_etac_1) 
    #bwxg_2 = RooFFTConvPdf('bwxg_2','breit-wigner (X) gauss', Jpsi_M_res, br_wigner, cb_etac_2) 
    
    #Fit J/psi
    cb_Jpsi_1 = BifurcatedCB("cb_Jpsi_1", "Cystal Ball Function", Jpsi_M_res, mean_Jpsi, sigma_Jpsi_1, alpha_eta_1, n_eta_1, alpha_eta_1, n_eta_1)
    #cb_Jpsi_2 = BifurcatedCB("cb_Jpsi_2", "Cystal Ball Function", Jpsi_M_res, mean_Jpsi, sigma_Jpsi_1, alpha_eta_2, n_eta_2);
    
    
    ##Connection between parameters
    ##   RooFormulaVar f_1('f_1','f_1','@0*9.0',RooArgList(nEtac_2))
    
    ## Create constraints
    ##   RooGaussian constrNEta('constrNEta','constraint Etac',nEtac_1,f_1,RooConst(0.0)) 
    
    
    ##   RooAddPdf model('model','signal', RooArgList(bwxg_1, bwxg_2,gauss_1,gauss_2), RooArgList(nEtac_1,nEtac_2,nJpsi_1,nJpsi_2))  
    ##   RooAddPdf modelEtac('modelEtac','Etac signal', RooArgList(bwxg_1, bwxg_2), RooArgList(nEtac_1, nEtac_2))
    
    modelEtac_Prompt = RooAddPdf('modelEtac_Prompt','Etac signal', RooArgList(cb_etac_1), RooArgList(nEtac_Prompt))
    modelJpsi_Prompt = RooAddPdf('modelJpsi_Prompt','Jpsi signal', RooArgList(cb_Jpsi_1), RooArgList(nJpsi_Prompt))
    #modelEtac_Prompt = RooAddPdf('modelEtac_Prompt','Etac signal', RooArgList(cb_etac_1,cb_etac_2), RooArgList(nEtac_Prompt_1,nEtac_Prompt_2))
    #modelJpsi_Prompt = RooAddPdf('modelJpsi_Prompt','Jpsi signal', RooArgList(cb_Jpsi_1,cb_Jpsi_2), RooArgList(nJpsi_Prompt_1,nJpsi_Prompt_2))
    
    modelEtac_FromB = RooAddPdf('modelEtac_FromB','Etac signal', RooArgList(cb_etac_1), RooArgList(nEtac_FromB))
    modelJpsi_FromB = RooAddPdf('modelJpsi_FromB','Jpsi signal', RooArgList(cb_Jpsi_1), RooArgList(nJpsi_FromB))
    #modelEtac_FromB = RooAddPdf('modelEtac_FromB','Etac signal', RooArgList(cb_etac_1,cb_etac_2), RooArgList(nEtac_FromB_1,nEtac_FromB_2))
    #modelJpsi_FromB = RooAddPdf('modelJpsi_FromB','Jpsi signal', RooArgList(cb_Jpsi_1,cb_Jpsi_2), RooArgList(nJpsi_FromB_1,nJpsi_FromB_2))
    
    sample = RooCategory('sample','sample') 
    sample.defineType('Etac_Prompt') 
    sample.defineType('Jpsi_Prompt') 
    sample.defineType('Etac_FromB') 
    sample.defineType('Jpsi_FromB') 
    
    
    #dataEtac = (RooDataSet) w.data('dsEtac')
    #dataJpsi = (RooDataSet) w.data('dsJpsi')
    dataEtac_Prompt = w.data('dsEtac_Prompt')
    dataJpsi_Prompt = w.data('dsJpsi_Prompt')
    dataEtac_FromB = w.data('dsEtac_FromB')
    dataJpsi_FromB = w.data('dsJpsi_FromB')
    
    
    # Construct combined dataset in (Jpsi_M_res,sample)
    combData = RooDataSet('combData', 'combined data', RooArgSet(Jpsi_M_res), RooFit.Index(sample), RooFit.Import('Etac_Prompt',dataEtac_Prompt), RooFit.Import('Jpsi_Prompt',dataJpsi_Prompt), RooFit.Import('Etac_FromB',dataEtac_FromB), RooFit.Import('Jpsi_FromB',dataJpsi_FromB)) 
    
    
    # Associate model with the physics state and model_ctl with the control state
    simPdf = RooSimultaneous('simPdf','simultaneous signal pdf',sample) 
    simPdf.addPdf(modelEtac_Prompt,'Etac_Prompt') 
    simPdf.addPdf(modelJpsi_Prompt,'Jpsi_Prompt') 
    simPdf.addPdf(modelEtac_FromB,'Etac_FromB') 
    simPdf.addPdf(modelJpsi_FromB,'Jpsi_FromB') 
    
    
    #   getattr(w,'import')(model,RecycleConflictNodes())
    getattr(w,'import')(combData)
    getattr(w,'import')(simPdf)
    


def fitData(iPT, gauss=True, empty=False):

    
    gROOT.Reset()
    #TProof *proof = TProof.Open('')
    
    if gauss: add = ""
    else: add="_CB"

    w = RooWorkspace('w',True)   
    
    getData_d(w, iPT)
    
    if gauss:
        fillRelWorkspace(w)
    else:
        fillRelWorkspaceCB(w)
        
    
    Jpsi_M_res = w.var('Jpsi_M_res')
    
    
    modelEtac_Prompt = w.pdf('modelEtac_Prompt')
    modelJpsi_Prompt = w.pdf('modelJpsi_Prompt')
    modelEtac_FromB = w.pdf('modelEtac_FromB')
    modelJpsi_FromB = w.pdf('modelJpsi_FromB')
    
    sample = w.cat('sample')
    simPdf = w.pdf('simPdf')
    combData = w.data('combData')
    
    
    dataEtac_Prompt = w.data('dsEtac_Prompt')
    dataJpsi_Prompt = w.data('dsJpsi_Prompt')
    dataEtac_FromB = w.data('dsEtac_FromB')
    dataJpsi_FromB = w.data('dsJpsi_FromB')
    
    
    #sigma = w.var('sigma_eta_1')
    #mean_Jpsi = w.var('mean_Jpsi')
    #mean_Etac = w.var('mean_Etac')
    #gamma_eta = w.var('gamma_eta')
    
    
    if (iPT != 0):
    
        f = TFile(homeDir+"Results/MC/MassFit/MC_MassResolution%s_2016_wksp.root"%(add),"READ") 
        wMC = f.Get("w")
        f.Close()
        if gauss:        
            w.var('rNarToW').setVal(wMC.var('rNarToW').getValV())
            #     w.var('rEtaToJpsi').setVal(wMC.var('rEtaToJpsi').getValV())
            w.var('rG1toG2').setVal(wMC.var('rG1toG2').getValV())
            w.var('mean_Jpsi').setVal(wMC.var('mean_Jpsi').getValV())
            
            #w.var('rNarToW').setConstant(True)
            #w.var('rEtaToJpsi').setConstant(True)
            #w.var('rG1toG2').setConstant(True)
            #w.var('mean_Jpsi').setConstant(True)
        else:
            w.var('rEtaToJpsi').setVal(wMC.var('rEtaToJpsi').getValV())
            w.var('alpha_eta_1').setVal(wMC.var('alpha_eta_1').getValV())
            w.var('n_eta_1').setVal(wMC.var('n_eta_1').getValV())
            w.var('mean_Jpsi').setVal(wMC.var('mean_Jpsi').getValV())
            
            #w.var('rEtaToJpsi').setConstant(True)
            w.var('alpha_eta_1').setConstant(True)
            w.var('n_eta_1').setConstant(True)
            #w.var('mean_Jpsi').setConstant(True)
    

    r = simPdf.fitTo(combData,RooFit.Save(True)) 
    r = simPdf.fitTo(combData,RooFit.Minos(True),RooFit.Save(True)) 
    
    
    gROOT.ProcessLine('gStyle->SetOptTitle(0)')
    if empty:
        gROOT.ProcessLine('gStyle->SetOptStat(000000000)')

    #frame[0] = Jpsi_M_res.frame(RooFit.Title('#eta_c prompt')) 
    #frame[1] = Jpsi_M_res.frame(RooFit.Title('J/#psi prompt')) 
    #frame[2] = Jpsi_M_res.frame(RooFit.Title('#eta_c from-b')) 
    #frame[3] = Jpsi_M_res.frame(RooFit.Title('J/#psi from-b')) 
    frame = []
    for i in range(4):
        frame.append(Jpsi_M_res.frame(RooFit.Title('')))

    dataEtac_Prompt.plotOn(frame[0])
    dataJpsi_Prompt.plotOn(frame[1])
    dataEtac_FromB.plotOn(frame[2])
    dataJpsi_FromB.plotOn(frame[3])
    
    
    
    #gaussEta_1 = w.pdf('gaussEta_1')
    #gaussEta_2 = w.pdf('gaussEta_2')
    #gauss_1 = w.pdf('gauss_1')
    #gauss_2 = w.pdf('gauss_2')
    
    
    #modelEtac_Prompt.paramOn(frame[0],RooFit.Layout(0.68,0.99,0.99))
    #frame[0].getAttText().SetTextSize(0.027) 
    modelEtac_Prompt.plotOn(frame[0],RooFit.Normalization(1.0,RooAbsReal.RelativeExpected))
    chi2Etac_Prompt = frame[0].chiSquare()
    #modelEtac_Prompt.plotOn(frame[0],RooFit.Components(RooArgSet(gaussEta_1,gaussEta_2)), RooFit.FillStyle(3005), RooFit.FillColor(kMagenta), RooFit.DrawOption('F'), RooFit.Normalization(1.0,RooAbsReal.RelativeExpected))
    print 'chi2Etac_Prompt = ', chi2Etac_Prompt

    #modelJpsi_Prompt.paramOn(frame[1],RooFit.Layout(0.68,0.99,0.99))
    #frame[1].getAttText().SetTextSize(0.027) 
    modelJpsi_Prompt.plotOn(frame[1],RooFit.Normalization(1.0,RooAbsReal.RelativeExpected))
    chi2Jpsi_Prompt = frame[1].chiSquare()
    #modelJpsi_Prompt.plotOn(frame[1],RooFit.Components(RooArgSet(gauss_1,gauss_2)),RooFit.FillStyle(3005),RooFit.FillColor(kMagenta),RooFit.DrawOption('F'), RooFit.Normalization(1.0,RooAbsReal.RelativeExpected))
    print 'chi2Jpsi_Prompt = ', chi2Jpsi_Prompt
    
    #modelEtac_FromB.paramOn(frame[2],RooFit.Layout(0.68,0.99,0.99))
    #frame[2].getAttText().SetTextSize(0.027) 
    modelEtac_FromB.plotOn(frame[2],RooFit.Normalization(1.0,RooAbsReal.RelativeExpected))
    chi2Etac_FromB = frame[2].chiSquare()
    #modelEtac_FromB.plotOn(frame[2],RooFit.Components(RooArgSet(gaussEta_1,gaussEta_2)), RooFit.FillStyle(3005), RooFit.FillColor(kMagenta), RooFit.DrawOption('F'),RooFit.Normalization(1.0,RooAbsReal.RelativeExpected))
    print 'chi2Etac_FromB = ', chi2Etac_FromB
    
    #modelJpsi_FromB.paramOn(frame[3],RooFit.Layout(0.68,0.99,0.99))
    #frame[3].getAttText().SetTextSize(0.027)
    modelJpsi_FromB.plotOn(frame[3],RooFit.Normalization(1.0,RooAbsReal.RelativeExpected))
    chi2Jpsi_FromB = frame[3].chiSquare()
    #modelJpsi_FromB.plotOn(frame[3], RooFit.Components(RooArgSet(gauss_1,gauss_2)), RooFit.FillStyle(3005), RooFit.FillColor(kMagenta), RooFit.DrawOption('F'), RooFit.Normalization(1.0,RooAbsReal.RelativeExpected))
    print 'chi2Jpsi_FromB = ', chi2Jpsi_FromB
    
    c = TCanvas('Masses_Fit','Masses Fit',1200,800)
    c.Divide(2,2)
    names = ["#eta_{c} prompt", "J/#psi prompt", "#eta_{c} from-b", "J/#psi from-b"]
    texMC = TLatex()
    texMC.SetNDC()

    for iC in range(4):
        pad = c.cd(iC+1)
        xl = pad.GetXlowNDC(); xh = xl + pad.GetWNDC()
        yl = pad.GetYlowNDC(); yh = yl + pad.GetHNDC()
        pad.SetPad(xl+0.005,yl+0.005,xh-0.005,yh-0.005)
        pad.SetLeftMargin(0.15);  pad.SetBottomMargin(0.15);  frame[iC].GetXaxis().SetTitle('M_{p#bar{p}} - M^{TRUE}_{p#bar{p}},  MeV/c^{2}')
        frame[iC].GetXaxis().SetTitleSize(0.06)
        frame[iC].GetYaxis().SetTitleSize(0.06)
        frame[iC].GetXaxis().SetTitleOffset(0.90)
        frame[iC].GetYaxis().SetTitleOffset(0.90)
        frame[iC].GetXaxis().SetTitleFont(12)
        frame[iC].GetYaxis().SetTitleFont(12)
        frame[iC].GetXaxis().SetLabelSize(0.05)
        frame[iC].GetYaxis().SetLabelSize(0.05)
        frame[iC].GetXaxis().SetLabelFont(62)
        frame[iC].GetYaxis().SetLabelFont(62)
        frame[iC].Draw()
        #frame[iC].SetMaximum(6.e2)
        frame[iC].SetMinimum(0.1)
        texMC.DrawLatex(0.6, 0.80, "LHCb simulation")
        texMC.DrawLatex(0.6, 0.75, "#sqrt{s}=13 TeV")
        texMC.DrawLatex(0.25, 0.75, names[iC])
        #pad.SetLogy()

    
    
    nameTxt = ''
    nameRoot = ''
    nameWksp = ''
    namePic = ''
    
    if(iPT == 0):
    
        nameTxt = homeDir+'Results/MC/MassFit/fit_MassRes'+add+'.txt'
        nameWksp = homeDir+'Results/MC/MassFit/MC_MassResolution'+add+'_2016_wksp.root'
        nameRoot = homeDir+'Results/MC/MassFit/MC_MassResolution'+add+'_Fit_plot.root'
        namePic = homeDir+'Results/MC/MassFit/MC_MassResolution'+add+'.pdf'    
    
    else:
    
        nameTxt = homeDir+'Results/MC/MassFit/fit_MassRes'+add+'_PT%s.txt'%(iPT)
        nameWksp = homeDir+'Results/MC/MassFit/MC_MassResolution'+add+'_2016_wksp_PT%s.root'%(iPT)
        nameRoot = homeDir+'Results/MC/MassFit/MC_MassResolution'+add+'_Fit_plot_PT%s.root'%(iPT)
        namePic = homeDir+'Results/MC/MassFit/MC_MassResolution'+add+'_PT%s.pdf'%(iPT)
    
    #import os
    #os = open(nameTxt,'w')
    #fo = open(nameTxt,'w')

    #for var in w.allVars():
        #line = string(var.getTitle()+' '+var.getValV()+' '+var.getError() )
        #fo.write(line)

    #fo.close()

    #params = simPdf.getParameters(Jpsi_M_res) ;
    #params = w.allVars()
    #params.printLatex(RooFit.OutputFile(nameTex))

    import os, sys 
    save = os.dup( sys.stdout.fileno() ) 
    newout = file(nameTxt, 'w' ) 
    os.dup2( newout.fileno(), sys.stdout.fileno() ) 
    r.Print("v") 
    print "chi2 eta_c prompt %6.4f \n"%(chi2Etac_Prompt)
    print "chi2 eta_c fromb %6.4f \n"%(chi2Etac_FromB)
    print "chi2 Jpsi prompt %6.4f \n"%(chi2Jpsi_Prompt)
    print "chi2 Jpsi fromb %6.4f \n"%(chi2Jpsi_FromB)
    r.correlationMatrix().Print()
    os.dup2( save, sys.stdout.fileno() ) 
    newout.close()



    
    w.writeToFile(nameWksp)
    fFit = TFile (nameRoot,'RECREATE')
    
    
    c.Write('')
    fFit.Write()
    fFit.Close()
    
    c.SaveAs(namePic)
    #fo.write(r)
    r.correlationMatrix().Print('v')
    r.globalCorr().Print('v')
    





def MC_Resolution_Fit():

    fitData(0)
    nPTBins = 5 
    for iPT in range(nPTBins):
        fitData(iPT)  
    
#gROOT.LoadMacro("../lhcbStyle.C")
MC_Resolution_Fit()
#fitData(0)
#fitData(0,False)



