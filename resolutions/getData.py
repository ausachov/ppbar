from ROOT import *

gROOT.LoadMacro("../libRooRelBreitWigner/RooRelBreitWigner.cxx+");
gROOT.LoadMacro("../BifurcatedCB/BifurcatedCB.cxx+");
homeDir = "/users/LHCb/zhovkovska/scripts/"

binningDict = {
    "Jpsi_PT":                      [6500, 8000, 10000, 12000, 14000, 18000],
    "Jpsi_ETA":                     [2.0, 2.45, 2.8, 3.15, 4.5],
    "TMath::Abs(ProtonM_CosTheta)": [0.0, 0.15, 0.3, 0.50, 1.0],
    "nSPDHits":                     [0, 100, 150, 200, 250, 300],
    "Jpsi_Tz":                      [-10., -0.125, -0.025, 0.0, 0.20, 2.0, 4.0, 10.0],
    "Jpsi_Tz_Old":                  [-10., -0.025, 0.0, 0.20, 2.0, 10.0],

}



cutApriori_Dict = {"DiProton":"Jpsi_Hlt1DiProtonDecision_TOS && \
                                  ProtonP_P>12.5e3 && ProtonM_P>12.5e3 && \
                                  ProtonP_PT>2000 && ProtonM_PT>2000 && \
                                  ProtonP_PT/ProtonP_P>0.0366 && ProtonM_PT/ProtonM_P>0.0366 && \
                                  ProtonP_PIDp>20 && ProtonM_PIDp>20 && \
                                  (ProtonP_PIDp-ProtonP_PIDK)>15 && (ProtonM_PIDp-ProtonM_PIDK)>15 && \
                                  ProtonP_TRACK_CHI2NDOF<2.5 && ProtonM_TRACK_CHI2NDOF<2.5 && \
                                  Jpsi_ENDVERTEX_CHI2<3.5 && \
                                  Jpsi_Y>2 && Jpsi_Y<4.5 && \
                                  Jpsi_PT>6500 && \
                                  ProtonP_TRACK_GhostProb<0.2 && ProtonP_TRACK_GhostProb<0.2 && \
                                  ProtonP_TRACK_CloneDist<0 && ProtonM_TRACK_CloneDist<0",
}

def getData_d(w, iPT=0, iTz=0):
    
    nPTBins = 5
    pt = (['6500', '8000'], ['8000', '10000'], ['10000', '12000'], ['12000', '14000'], ['14000', '18000'])
    #pt = (['6500', '7000'], ['7000', '8000'], ['8000', '9000'], ['9000', '10000'] ,['10000', '11000'], ['11000', '12000'],['12000', '13000'], ['13000', '14000'],['14000', '16000'],['16000', '18000'])
    pt = binningDict["Jpsi_PT"]
    tz = binningDict["Jpsi_Tz"]
    
    ntEtac_Prompt = TChain('DecayTree')
    ntJpsi_Prompt = TChain('DecayTree')
    ntEtac_FromB = TChain('DecayTree')
    ntJpsi_FromB = TChain('DecayTree')

    #-----------old MC--------------------------------------------------------    
    #ntEtac_Prompt.Add(homeDir+'MC/newMC/Etac/EtacDiProton_MC_2016_AddBr.root')
    #ntEtac_Prompt.Add(homeDir+'MC/newMC/Etac/EtacDiProton_MC_2015_AddBr.root')
    #ntJpsi_Prompt.Add(homeDir+'MC/newMC/Jpsi/JpsiDiProton_MC_2016_AddBr.root')
    #ntJpsi_Prompt.Add(homeDir+'MC/newMC/Jpsi/JpsiDiProton_MC_2015_AddBr.root')
    #ntEtac_FromB.Add(homeDir+'MC/newMC/incl_b/EtacDiProton_MC_2015_incl_b_AddBr.root')
    #ntEtac_FromB.Add(homeDir+'MC/newMC/incl_b/EtacDiProton_MC_2016_incl_b_AddBr.root')
    #ntJpsi_FromB.Add(homeDir+'MC/newMC/Jpsi/JpsiDiProton_MC_2016_AddBr.root')
    #ntJpsi_FromB.Add(homeDir+'MC/newMC/Jpsi/JpsiDiProton_MC_2015_AddBr.root')


    #-----------new MC--------------------------------------------------------    
    ntEtac_Prompt.Add(homeDir+'MC/newMC_spring/Etac_Pr_all_l0TOS_allSelected_AddBr.root')
    ntJpsi_Prompt.Add(homeDir+'MC/newMC_spring/Jpsi_Pr_all_l0TOS_allSelected_AddBr.root')
    ntEtac_FromB.Add(homeDir+'MC/newMC_spring/incl_b_Pr_all_l0TOS_allSelected_AddBr.root')
    ntJpsi_FromB.Add(homeDir+'MC/newMC_spring/Jpsi_Pr_all_l0TOS_allSelected_AddBr.root')
    
    treeEtac_Prompt = TTree()
    treeJpsi_Prompt = TTree()
    treeEtac_FromB = TTree()
    treeJpsi_FromB = TTree()
    
    cut = cutApriori_Dict["DiProton"] 
    
    if (iPT == 0):
        cutJpsiPtL = 'Jpsi_PT > %s'%(pt[0])
        cutJpsiPtR = 'Jpsi_PT < %s'%(pt[-2])
    else:
        cutJpsiPtL = 'Jpsi_PT > %s'%(pt[iPT-1])
        cutJpsiPtR = 'Jpsi_PT < %s'%(pt[iPT])
        
    if (iTz == 0):
        cutJpsiTzL = 'Jpsi_Tz > %s'%(tz[0])
        cutJpsiTzR = 'Jpsi_Tz < %s'%(tz[-1])
    else:
        cutJpsiTzL = 'Jpsi_Tz > %s'%(tz[iTz-1])
        cutJpsiTzR = 'Jpsi_Tz < %s'%(tz[iTz])
        

    cutPrompt = 'prompt'
    cutFromB = 'sec'
        
    treeEtac_Prompt = ntEtac_Prompt.CopyTree(cutPrompt + '&&' + cutJpsiPtL + '&&' + cutJpsiPtR  + '&&' + cutJpsiTzL + '&&' + cutJpsiTzR  + "&&" + cut)
    treeJpsi_Prompt = ntJpsi_Prompt.CopyTree(cutPrompt + '&&' + cutJpsiPtL + '&&' + cutJpsiPtR  + "&&" + cutJpsiTzL + '&&' + cutJpsiTzR  + "&&" + cut)
    treeEtac_FromB = ntEtac_FromB.CopyTree(cutFromB + '&&' + cutJpsiPtL + '&&' + cutJpsiPtR  + "&&" + cutJpsiTzL + '&&' + cutJpsiTzR  + "&&" + cut)
    treeJpsi_FromB = ntJpsi_FromB.CopyTree(cutFromB + '&&' + cutJpsiPtL + '&&' + cutJpsiPtR  + "&&" + cutJpsiTzL + '&&' + cutJpsiTzR  + "&&" + cut)
          
    
    Jpsi_M_res = RooRealVar ('Jpsi_M_res','Jpsi_M_res',-100.0,100.0)  #!!!!!!!!!!!!!!!!!! change from (-100, 100)
    dsEtac_Prompt = RooDataSet('dsEtac_Prompt','dsEtac_Prompt',treeEtac_Prompt,RooArgSet(Jpsi_M_res))
    dsJpsi_Prompt = RooDataSet('dsJpsi_Prompt','dsJpsi_Prompt',treeJpsi_Prompt,RooArgSet(Jpsi_M_res))
    dsEtac_FromB = RooDataSet('dsEtac_FromB','dsEtac_FromB',treeEtac_FromB,RooArgSet(Jpsi_M_res))
    dsJpsi_FromB = RooDataSet('dsJpsi_FromB','dsJpsi_FromB',treeJpsi_FromB,RooArgSet(Jpsi_M_res))
    getattr(w,'import')(dsEtac_Prompt)
    getattr(w,'import')(dsJpsi_Prompt)
    getattr(w,'import')(dsEtac_FromB)
    getattr(w,'import')(dsJpsi_FromB)
    
    #  dsEtac.Draw('')
    #  dsJpsi.Draw('')
    
