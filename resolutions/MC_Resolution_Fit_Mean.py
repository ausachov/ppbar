from ROOT import *

gROOT.LoadMacro("../libRooRelBreitWigner/RooRelBreitWigner.cxx+");
gROOT.LoadMacro("../BifurcatedCB/BifurcatedCB.cxx+");
homeDir = "/users/LHCb/zhovkovska/scripts/"

def getData_d(w, iTz=0):
    
    nPTBins = 5
    #pt = (['6500', '8000'], ['8000', '10000'], ['10000', '12000'], ['12000', '14000'], ['14000', '18000'])
    pt = [6500, 8000, 10000, 12000, 14000]
    #tz = [-10., -0.15, -0.025, 0.,        0.200,   2., 4., 10.]
    #tz = [-10., -0.125, -0.025, 0.,        0.200,   2., 4., 10.]
    tz = [-10.0, -0.125, -0.025, 0.0, 0.025, 0.2, 1.0, 1.5, 2.0, 3.0, 4.0, 10]
    nTzBins = len(tz)-1
    #nTzBins = 6
    #tz = [-10.0, -0.15, -0.05, 0.,        0.100,   4., 10.]
    #pt = (['6500', '7000'], ['7000', '8000'], ['8000', '9000'], ['9000', '10000'] ,['10000', '11000'], ['11000', '12000'],['12000', '13000'], ['13000', '14000'],['14000', '16000'],['16000', '18000'])
    
    ntEtac = TChain('DecayTree')
    ntEtac_FromB = TChain('DecayTree')
    ntJpsi= TChain('DecayTree')
    #ntEtac = TChain('DecayTree')
    
    #-----------old MC--------------------------------------------------------    
    #ntEtac.Add(homeDir+'MC/newMC/Etac/EtacDiProton_MC_2016_AddBr.root')
    #ntEtac.Add(homeDir+'MC/newMC/Etac/EtacDiProton_MC_2015_AddBr.root')
    ##ntEtac = ntEtac.CopyTree("prompt")
    #ntEtac_FromB.Add(homeDir+'MC/newMC/incl_b/EtacDiProton_MC_2015_incl_b_AddBr.root')
    #ntEtac_FromB.Add(homeDir+'MC/newMC/incl_b/EtacDiProton_MC_2016_incl_b_AddBr.root')
    #ntJpsi.Add(homeDir+'MC/newMC/Jpsi/JpsiDiProton_MC_2016_AddBr.root')
    #ntJpsi.Add(homeDir+'MC/newMC/Jpsi/JpsiDiProton_MC_2015_AddBr.root')
    
    
    #-----------new MC--------------------------------------------------------    
    ntEtac.Add(homeDir+'MC/newMC_spring/Etac_Pr_all_l0TOS_allSelected_AddBr.root')
    ntEtac_FromB.Add(homeDir+'MC/newMC_spring/incl_b_Pr_all_l0TOS_allSelected_AddBr.root')
    ntJpsi.Add(homeDir+'MC/newMC_spring/Jpsi_Pr_all_l0TOS_allSelected_AddBr.root')

    
    treeJpsi = TTree()
    treeEtac = TTree()
        
    
    if(iTz == 0):
        
        #cutJpsiTzL = '(3.3)*(JpsiERTEX_Z-Jpsi_OWNPV_Z)*Jpsi_MM/Jpsi_PZ > %f'%(tz[iTz-1])
        #cutJpsiTzR = '(3.3)*(JpsiERTEX_Z-Jpsi_OWNPV_Z)*Jpsi_MM/Jpsi_PZ < %f'%(tz[iTz])
        
        treeEtac = ntEtac.CopyTree("prompt")
        treeEtac_FromB = ntEtac_FromB.CopyTree("sec")
        list0 = TList()
        list0.Add(treeEtac) 
        list0.Add(treeEtac_FromB) 
        
        treeEtac = TTree.MergeTrees(list0)
        treeJpsi = ntJpsi.CopyTree("")

    elif ( iTz <= nTzBins ):
    #if (iTz != 0):
        print iTz
        cutJpsiTzL = 'Jpsi_Tz > %f'%(tz[iTz-1])
        cutJpsiTzR = 'Jpsi_Tz < %f'%(tz[iTz])
        #cutJpsiTzL = '(3.3)*(JpsiERTEX_Z-Jpsi_OWNPV_Z)*Jpsi_MM/Jpsi_PZ > %f'%(tz[iTz-1])
        cutJpsiPTL = 'Jpsi_PT > %f'%(6500)
        cutJpsiPTR = 'Jpsi_PT < %f'%(14000)
        #cutJpsiTzR = '(3.3)*(JpsiERTEX_Z-Jpsi_OWNPV_Z)*Jpsi_MM/Jpsi_PZ < %f'%(tz[iTz])
        
        treeEtac = ntEtac.CopyTree("prompt" + '&&' + cutJpsiTzL + '&&' + cutJpsiTzR + '&&' + cutJpsiPTL + '&&' + cutJpsiPTR)
        treeEtac_FromB = ntEtac_FromB.CopyTree("sec" + '&&' + cutJpsiTzL + '&&' + cutJpsiTzR + '&&' + cutJpsiPTL + '&&' + cutJpsiPTR)
        list0 = TList()
        list0.Add(treeEtac) 
        list0.Add(treeEtac_FromB) 
        
        treeEtac = TTree.MergeTrees(list0)
        #treeEtac = ntEtac.CopyTree( "prompt &&" + cutJpsiTzL + '&&' + cutJpsiTzR)
        treeJpsi = ntJpsi.CopyTree(cutJpsiTzL + '&&' + cutJpsiTzR + '&&' + cutJpsiPTL + '&&' + cutJpsiPTR)
      
    else:
        print ('Incorrect number of Tz bin %s'%(iTz))
    
    
    Jpsi_M_res = RooRealVar ('Jpsi_M_res','Jpsi_M_res',-100.,100.0) 
    dsEtac = RooDataSet('dsEtac','dsEtac',treeEtac,RooArgSet(Jpsi_M_res))
    dsJpsi = RooDataSet('dsJpsi','dsJpsi',treeJpsi,RooArgSet(Jpsi_M_res))
    getattr(w,'import')(dsEtac)
    getattr(w,'import')(dsJpsi)
    
    #dsEtac.Draw('')
    #dsJpsi.Draw('')
    

def getConstPars():

    f = TFile(homeDir+"Results/MC/MassFit/MC_MassResolution_2016_wksp.root","READ") 
    wMC = f.Get("w")
    f.Close()
    
    rNtoW = wMC.var("rNarToW").getValV()
    rEtaToJpsi = wMC.var("rEtaToJpsi").getValV()
    rArea = wMC.var("rG1toG2").getValV()
    gamma = 31.8
    effic = 0.035
    #gamma = 21.3
    #effic = 0.06
    #ratioNtoW = 0.5
    #ratioEtaToJpsi = 0.88
    #ratioArea = 0.9
    
    
    return rNtoW,rEtaToJpsi,rArea, gamma, effic
    

def fillRelWorkspace(w):

    
    Jpsi_M_res = w.var('Jpsi_M_res')
    #Jpsi_M_res.setBins(1000,'cache')
    
    ratioNtoW = 0.50
    ratioEtaToJpsi = 0.88
    ratioArea = 0.70
    #gammaEtac = 31.8
    gammaEtac = 29.7
    
    ratioNtoW,ratioEtaToJpsi,ratioArea, gammaEtac, eff = getConstPars()
    
    
    
    rEtaToJpsi = RooRealVar('rEtaToJpsi','rEtaToJpsi', ratioEtaToJpsi)
    rNarToW = RooRealVar('rNarToW','rNarToW',ratioNtoW)
    rG1toG2 = RooRealVar('rG1toG2','rG1toG2',ratioArea)
    nEtac = RooRealVar('nEtac','num of Etac', 1e3, 10, 1.e4)
    nJpsi = RooRealVar('nJpsi','num of J/Psi', 2e3, 10, 1.e5)
    nEtacRel = RooRealVar('nEtacRel','num of Etac', 0.0, 3.0)
    
    nEtac_1 = RooFormulaVar('nEtac_1','num of Etac','@0*@1',RooArgList(nEtac,rG1toG2))
    nEtac_2 = RooFormulaVar('nEtac_2','num of Etac','@0-@1',RooArgList(nEtac,nEtac_1))
    nJpsi_1 = RooFormulaVar('nJpsi_1','num of J/Psi','@0*@1',RooArgList(nJpsi,rG1toG2))
    nJpsi_2 = RooFormulaVar('nJpsi_2','num of J/Psi','@0-@1',RooArgList(nJpsi,nJpsi_1))

    
    mean_Jpsi = RooRealVar('mean_Jpsi','mean of gaussian', 0.0, -50.0, 50.0)   
    mean_Etac = RooRealVar('mean_Etac','mean of gaussian', 0.0, -50.0, 50.0) 
    gamma_eta = RooRealVar('gamma_eta','width of Br-W', gammaEtac, 10., 50. )
    spin_eta = RooRealVar('spin_eta','spin_eta', 0. )
    radius_eta = RooRealVar('radius_eta','radius', 1.)
    proton_m = RooRealVar('proton_m','proton mass', 938.3 )
    
    sigma_eta_1 = RooRealVar('sigma_eta_1','width of gaussian', 9., 0.1, 50.) 
    sigma_eta_2 = RooFormulaVar('sigma_eta_2','width of gaussian','@0/@1',RooArgList(sigma_eta_1,rNarToW))
    
    sigma_Jpsi_1 = RooFormulaVar('sigma_Jpsi_1','width of gaussian','@0/@1',RooArgList(sigma_eta_1,rEtaToJpsi))
    #sigma_Jpsi_1 = RooRealVar('sigma_Jpsi_1','width of gaussian', 9., 0.1, 50.) 
    #sigma_Jpsi_1.setVal(6.5)
    #sigma_Jpsi_1.setConstant(True)
    sigma_Jpsi_2 = RooFormulaVar('sigma_Jpsi_2','width of gaussian','@0/@1',RooArgList(sigma_Jpsi_1,rNarToW))    

    #Fit eta
    br_wigner = RooRelBreitWigner('br_wigner', 'br_wigner',Jpsi_M_res, mean_Etac, gamma_eta, spin_eta,radius_eta,proton_m,proton_m)
    
    #gaussEta_1 = RooGaussian('gaussEta_1','gaussEta_1 PDF', Jpsi_M_res, mean_Etac,  sigma_eta_1) 
    #gaussEta_2 = RooGaussian('gaussEta_2','gaussEta_2 PDF', Jpsi_M_res, mean_Etac,  sigma_eta_2) 
    gaussEta_1 = RooGaussian('gaussEta_1','gaussEta_1 PDF', Jpsi_M_res, mean_Jpsi,  sigma_eta_1) #mean_Etac -> mean_Jpsi
    gaussEta_2 = RooGaussian('gaussEta_2','gaussEta_2 PDF', Jpsi_M_res, mean_Jpsi,  sigma_eta_2) #mean_Etac -> mean_Jpsi
    
    bwxg_1 = RooFFTConvPdf('bwxg_1','breit-wigner (X) gauss', Jpsi_M_res, br_wigner, gaussEta_1) 
    bwxg_2 = RooFFTConvPdf('bwxg_2','breit-wigner (X) gauss', Jpsi_M_res, br_wigner, gaussEta_2) 
    
    #Fit J/psi
    gauss_1 = RooGaussian('gauss_1','gaussian PDF',Jpsi_M_res, mean_Jpsi, sigma_Jpsi_1) 
    gauss_2 = RooGaussian('gauss_2','gaussian PDF',Jpsi_M_res, mean_Jpsi, sigma_Jpsi_2) 
    
    
    ##Connection between parameters
    ##   RooFormulaVar f_1('f_1','f_1','@0*9.0',RooArgList(nEtac_2))
    
    ## Create constraints
    ##   RooGaussian constrNEta('constrNEta','constraint Etac',nEtac_1,f_1,RooConst(0.0)) 
    
    
    ##   RooAddPdf model('model','signal', RooArgList(bwxg_1, bwxg_2,gauss_1,gauss_2), RooArgList(nEtac_1,nEtac_2,nJpsi_1,nJpsi_2))  
    ##   RooAddPdf modelEtac('modelEtac','Etac signal', RooArgList(bwxg_1, bwxg_2), RooArgList(nEtac_1, nEtac_2))
    
    modelEtac = RooAddPdf('modelEtac','Etac signal', RooArgList(gaussEta_1, gaussEta_2), RooArgList(nEtac_1, nEtac_2))
    modelJpsi = RooAddPdf('modelJpsi','Jpsi signal', RooArgList(gauss_1, gauss_2), RooArgList(nJpsi_1, nJpsi_2))
    
    sample = RooCategory('sample','sample') 
    sample.defineType('Etac') 
    sample.defineType('Jpsi') 
    
    
    dataEtac = w.data('dsEtac')
    dataJpsi = w.data('dsJpsi')
    
    
    # Construct combined dataset in (Jpsi_M_res,sample)
    combData = RooDataSet('combData', 'combined data', RooArgSet(Jpsi_M_res), RooFit.Index(sample), RooFit.Import('Etac',dataEtac), RooFit.Import('Jpsi',dataJpsi)) 
    
    
    # Associate model with the physics state and model_ctl with the control state
    simPdf = RooSimultaneous('simPdf','simultaneous signal pdf',sample) 
    simPdf.addPdf(modelEtac,'Etac') 
    simPdf.addPdf(modelJpsi,'Jpsi') 
    
    
    #   getattr(w,'import')(model,RecycleConflictNodes())
    getattr(w,'import')(modelEtac)
    getattr(w,'import')(modelJpsi, RooFit.RecycleConflictNodes())
    getattr(w,'import')(combData, RooFit.RecycleConflictNodes())
    getattr(w,'import')(simPdf, RooFit.RecycleConflictNodes())
    

def fillRelWorkspaceCB(w):

    
    Jpsi_M_res = w.var('Jpsi_M_res')
    #Jpsi_M_res.setBins(1000,'cache')
    
    ratioNtoW = 0.50
    ratioEtaToJpsi = 0.88
    ratioArea = 0.70
    #gammaEtac = 31.8
    gammaEtac = 29.7
    
    
    
    rEtaToJpsi = RooRealVar('rEtaToJpsi','rEtaToJpsi', ratioEtaToJpsi, 0.01, 5.0)
    rNarToW = RooRealVar('rNarToW','rNarToW',ratioNtoW, 0.01, 1.0)
    rG1toG2 = RooRealVar('rG1toG2','rG1toG2',ratioArea, 0.01, 1.0)
    #rEtaToJpsi = RooRealVar('rEtaToJpsi','rEtaToJpsi', ratioEtaToJpsi, 0.01, 5.0)
    #rNarToW = RooRealVar('rNarToW','rNarToW', 1.0)
    #rG1toG2 = RooRealVar('rG1toG2','rG1toG2', 1.0)
    nEtac = RooRealVar('nEtac','num of Etac Prompt', 1e3, 10, 5.e4)
    nJpsi = RooRealVar('nJpsi','num of J/Psi Prompt', 2e3, 10, 5.e4)
    nEtacRel = RooRealVar('nEtacRel','num of Etac Prompt', 0.0, 3.0)
    
    #  nEtac_1 = RooFormulaVar('nEtac_1','num of Etac','@0*@1*@2',RooArgSet(nEtacRel,nJpsi,rG1toG2))
    #  nEtac_2 = RooFormulaVar('nEtac_2','num of Etac','@0*@1-@2',RooArgSet(nEtacRel,nJpsi,nEtac_1))
    nEtac_1 = RooFormulaVar('nEtac_1','num of Etac','@0*@1',RooArgList(nEtac,rG1toG2))
    nEtac_2 = RooFormulaVar('nEtac_2','num of Etac','@0-@1',RooArgList(nEtac,nEtac_1))
    nJpsi_1 = RooFormulaVar('nJpsi_1','num of J/Psi','@0*@1',RooArgList(nJpsi,rG1toG2))
    nJpsi_2 = RooFormulaVar('nJpsi_2','num of J/Psi','@0-@1',RooArgList(nJpsi,nJpsi_1))
    
    
    mean_Jpsi = RooRealVar('mean_Jpsi','mean of gaussian', 0.0, -50.0, 50.0)   
    mean_Etac = RooRealVar('mean_Etac','mean of gaussian', 0.0, -50.0, 50.0) 
    gamma_eta = RooRealVar('gamma_eta','width of Br-W', gammaEtac, 10., 50. )
    spin_eta = RooRealVar('spin_eta','spin_eta', 0. )
    radius_eta = RooRealVar('radius_eta','radius', 1.)
    proton_m = RooRealVar('proton_m','proton mass', 938.3 )
    

    alpha_eta_1 = RooRealVar('alpha_eta_1','alpha of CB', 1., 0.0, 10.) 
    alpha_eta_2 = RooRealVar('alpha_eta_2','alpha of CB', 1., 0.0, 10.) 
    n_eta_1 = RooRealVar('n_eta_1','n of CB', 1., 0.0, 100.) 
    n_eta_2 = RooRealVar('n_eta_2','n of CB', 1., 0.0, 100.) 

    sigma_eta_1 = RooRealVar('sigma_eta_1','width of gaussian', 9., 0.1, 50.) 
    sigma_eta_2 = RooFormulaVar('sigma_eta_2','width of gaussian','@0/@1',RooArgList(sigma_eta_1,rNarToW))
    
    sigma_Jpsi_1 = RooFormulaVar('sigma_Jpsi_1','width of gaussian','@0/@1',RooArgList(sigma_eta_1,rEtaToJpsi))
    sigma_Jpsi_2 = RooFormulaVar('sigma_Jpsi_2','width of gaussian','@0/@1',RooArgList(sigma_Jpsi_1,rNarToW))    

    #Prompt    
    #Fit eta
    br_wigner = RooRelBreitWigner('br_wigner', 'br_wigner',Jpsi_M_res, mean_Etac, gamma_eta, spin_eta,radius_eta,proton_m,proton_m)
    
    cb_etac_1 = BifurcatedCB("cb_etac_1", "Cystal Ball Function", Jpsi_M_res, mean_Jpsi, sigma_eta_1, alpha_eta_1, n_eta_1, alpha_eta_1, n_eta_1)
    #cb_etac_2 = BifurcatedCB("cb_etac_2", "Cystal Ball Function", Jpsi_M_res, mean_Etac, sigma_eta_1, alpha_eta_2, n_eta_2);

    bwxg_1 = RooFFTConvPdf('bwxg_1','breit-wigner (X) gauss', Jpsi_M_res, br_wigner, cb_etac_1) 
    #bwxg_2 = RooFFTConvPdf('bwxg_2','breit-wigner (X) gauss', Jpsi_M_res, br_wigner, cb_etac_2) 
    
    #Fit J/psi
    cb_Jpsi_1 = BifurcatedCB("cb_Jpsi_1", "Cystal Ball Function", Jpsi_M_res, mean_Jpsi, sigma_Jpsi_1, alpha_eta_1, n_eta_1, alpha_eta_1, n_eta_1)
    #cb_Jpsi_2 = BifurcatedCB("cb_Jpsi_2", "Cystal Ball Function", Jpsi_M_res, mean_Jpsi, sigma_Jpsi_1, alpha_eta_2, n_eta_2);
    
    
    ##Connection between parameters
    ##   RooFormulaVar f_1('f_1','f_1','@0*9.0',RooArgList(nEtac_2))
    
    ## Create constraints
    ##   RooGaussian constrNEta('constrNEta','constraint Etac',nEtac_1,f_1,RooConst(0.0)) 
    
    
    ##   RooAddPdf model('model','signal', RooArgList(bwxg_1, bwxg_2,gauss_1,gauss_2), RooArgList(nEtac_1,nEtac_2,nJpsi_1,nJpsi_2))  
    ##   RooAddPdf modelEtac('modelEtac','Etac signal', RooArgList(bwxg_1, bwxg_2), RooArgList(nEtac_1, nEtac_2))
    
    modelEtac = RooAddPdf('modelEtac','Etac signal', RooArgList(cb_etac_1), RooArgList(nEtac))
    modelJpsi = RooAddPdf('modelJpsi','Jpsi signal', RooArgList(cb_Jpsi_1), RooArgList(nJpsi))
    #modelEtac = RooAddPdf('modelEtac','Etac signal', RooArgList(cb_etac_1,cb_etac_2), RooArgList(nEtac_1,nEtac_2))
    #modelJpsi = RooAddPdf('modelJpsi','Jpsi signal', RooArgList(cb_Jpsi_1,cb_Jpsi_2), RooArgList(nJpsi_1,nJpsi_2))
    
    
    sample = RooCategory('sample','sample') 
    sample.defineType('Etac') 
    sample.defineType('Jpsi') 
    
    
    dataEtac = w.data('dsEtac')
    dataJpsi = w.data('dsJpsi')
    
    
    # Construct combined dataset in (Jpsi_M_res,sample)
    combData = RooDataSet('combData', 'combined data', RooArgSet(Jpsi_M_res), RooFit.Index(sample), RooFit.Import('Etac',dataEtac), RooFit.Import('Jpsi',dataJpsi)) 
    
    
    # Associate model with the physics state and model_ctl with the control state
    simPdf = RooSimultaneous('simPdf','simultaneous signal pdf',sample) 
    simPdf.addPdf(modelEtac,'Etac') 
    simPdf.addPdf(modelJpsi,'Jpsi') 
    
    
    #   getattr(w,'import')(model,RecycleConflictNodes())
    getattr(w,'import')(combData)
    getattr(w,'import')(simPdf)
    
    
def fitData(iTz, gauss=True):

    
    gROOT.Reset()    
    
    
    w = RooWorkspace('w',True)   
    
    getData_d(w, iTz)
    
    if gauss: 
        add=""
        fillRelWorkspace(w)
    else: 
        add="_CB"
        fillRelWorkspaceCB(w)
    
    Jpsi_M_res = w.var('Jpsi_M_res')
    
    
    modelEtac = w.pdf('modelEtac')
    modelJpsi = w.pdf('modelJpsi')
    
    sample = w.cat('sample')
    simPdf = w.pdf('simPdf')
    combData = w.data('combData')
    
    
    dataEtac = w.data('dsEtac')
    dataJpsi = w.data('dsJpsi')
    
    
    #sigma = w.var('sigma_eta_1')
    #sigmaJpsi = w.var('sigma_Jpsi_1')
    #mean_Jpsi = w.var('mean_Jpsi')
    #mean_Etac = w.var('mean_Etac')
    #gamma_eta = w.var('gamma_eta')
    
    
    if (iTz != 0):
    
        f = TFile(homeDir+"Results/MC/MassFit/MC_MassResolution%s_2016_wksp.root"%(add),"READ") 
        wMC = f.Get("w")
        f.Close()
        if gauss:        
            w.var('rNarToW').setVal(wMC.var('rNarToW').getValV())
            #     w.var('rEtaToJpsi').setVal(wMC.var('rEtaToJpsi').getValV())
            w.var('rG1toG2').setVal(wMC.var('rG1toG2').getValV())
            w.var('mean_Jpsi').setVal(wMC.var('mean_Jpsi').getValV())
            
            w.var('rNarToW').setConstant(True)
            #     w.var('rEtaToJpsi').setConstant(True)
            w.var('rG1toG2').setConstant(True)
            #w.var('mean_Jpsi').setConstant(True)
        else:
            w.var('rEtaToJpsi').setVal(wMC.var('rEtaToJpsi').getValV())
            w.var('alpha_eta_1').setVal(wMC.var('alpha_eta_1').getValV())
            w.var('n_eta_1').setVal(wMC.var('n_eta_1').getValV())
            w.var('mean_Jpsi').setVal(wMC.var('mean_Jpsi').getValV())
            
            w.var('rEtaToJpsi').setConstant(True)
            w.var('alpha_eta_1').setConstant(True)
            w.var('n_eta_1').setConstant(True)
            #w.var('mean_Jpsi').setConstant(True)
            
            
    r = simPdf.fitTo(combData,RooFit.Save(True)) 
    r = simPdf.fitTo(combData, RooFit.Extended(True), RooFit.Minos(True), RooFit.Save(True)) 
    r = simPdf.fitTo(combData, RooFit.Minos(True), RooFit.Save(True)) 
    
    #modelEtac.fitTo(dataEtac, RooFit.Minos(True), RooFit.Save(True))
    #r = modelJpsi.fitTo(dataJpsi, RooFit.Extended(True), RooFit.Minos(True), RooFit.Save(True))
    #r = modelJpsi.fitTo(dataJpsi, RooFit.Minos(True), RooFit.Save(True))
    
    gROOT.ProcessLine('gStyle->SetOptStat(000000000)')
    gROOT.ProcessLine('gStyle->SetOptTitle(0)')

    frame1 = Jpsi_M_res.frame(RooFit.Title('#eta_c to p #bar{p} from_b')) 
    frame2 = Jpsi_M_res.frame(RooFit.Title('J/#psi to p #bar{p} from_b')) 
    
    dataEtac.plotOn(frame1)
    dataJpsi.plotOn(frame2)
    
    
    
    #chi2Etac = 0.
    #chi2Jpsi = 0.
    #chi2Etac = 0.
    #chi2Jpsi = 0.
    gaussEta_1 = w.pdf('gaussEta_1')
    gaussEta_2 = w.pdf('gaussEta_2')
    gauss_1 = w.pdf('gauss_1')
    gauss_2 = w.pdf('gauss_2')
    
        
    #modelEtac.paramOn(frame1,RooFit.Layout(0.68,0.99,0.99))
    #frame1.getAttText().SetTextSize(0.027) 
    modelEtac.plotOn(frame1,RooFit.Normalization(1.0,RooAbsReal.RelativeExpected))
    chi2Etac = frame1.chiSquare()
    #modelEtac.plotOn(frame1,RooFit.Components(RooArgSet(gaussEta_1,gaussEta_2)), RooFit.FillStyle(3005), RooFit.FillColor(kMagenta), RooFit.DrawOption('F'),RooFit.Normalization(1.0,RooAbsReal.RelativeExpected))
    print 'chi2Etac = ', chi2Etac
    
    #modelJpsi.paramOn(frame2,RooFit.Layout(0.68,0.99,0.99))
    #frame2.getAttText().SetTextSize(0.027)
    modelJpsi.plotOn(frame2,RooFit.Normalization(1.0,RooAbsReal.RelativeExpected))
    chi2Jpsi = frame2.chiSquare()
    #modelJpsi.plotOn(frame2, RooFit.Components(RooArgSet(gauss_1,gauss_2)), RooFit.FillStyle(3005), RooFit.FillColor(kMagenta), RooFit.DrawOption('F'), RooFit.Normalization(1.0,RooAbsReal.RelativeExpected))
    print 'chi2Jpsi = ', chi2Jpsi
    
    print   dataEtac.numEntries()

    texMC = TLatex();
    texMC.SetNDC();

    c = TCanvas('Masses_Fit','Masses Fit',600,800)
    c.Divide(1,2)
    c.cd(1).SetPad(.005, .505, .995, .995)
    #gPad.SetLeftMargin(0.15),  frame1.GetXaxis().SetTitle('(M - TRUEM)_{p #bar{p}} / [MeV/c^2]'),  
    frame1.Draw()
    #frame1.SetMaximum(1.e3)
    frame1.SetMinimum(0.1)
    frame1.GetXaxis().SetTitleSize(0.14)
    frame1.GetXaxis().SetTitleSize(0.14)
    #gPad.SetLogy()
    texMC.DrawLatex(0.7, 0.8, "#chi^{2}_{#eta_{c}} = %4.2f"%chi2Etac)
    #frame.addObject(texJpsiPiMC1);
    c.cd(2).SetPad(.005, .005, .995, .495)
    #gPad.SetLeftMargin(0.15),  frame2.GetXaxis().SetTitle('(M - TRUEM)_{p #bar{p}} / [MeV/c^2]'),  
    frame2.Draw()
    #frame2.SetMaximum(1.e3)
    frame2.SetMinimum(0.1)
    frame2.GetXaxis().SetTitleSize(0.14)
    frame2.GetXaxis().SetTitleSize(0.14)
    #gPad.SetLogy()
    texMC.DrawLatex(0.7, 0.8, "#chi^{2}_{J/#psi} = %4.2f"%chi2Jpsi)
    
    nameTxt = ''
    nameRoot = ''
    nameWksp = ''
    namePic = ''
    
    if(iTz == 0):
    
        nameTxt = homeDir+'Results/MC/MassFit/fit_MassRes%s.txt'%(add)
        nameWksp = homeDir+'Results/MC/MassFit/MC_MassResolution%s_2016_wksp.root'%(add)
        nameRoot = homeDir+'Results/MC/MassFit/MC_MassResolution%s_Fit_plot.root'%(add)
        namePic = homeDir+'Results/MC/MassFit/MC_MassResolution%s_empty.pdf'%(add)    
    
    else:
    
        nameTxt = homeDir+'Results/MC/MassFit/Tz11Bins/fit_MassRes%s_shiftFix_Tz%s.txt'%(add,iTz)
        nameWksp = homeDir+'Results/MC/MassFit/Tz11Bins/MC_MassResolution%s_shiftFix_2016_wksp_Tz%s.root'%(add,iTz)
        nameRoot = homeDir+'Results/MC/MassFit/Tz11Bins/MC_MassResolution%s_shiftFix_Fit_plot_Tz%s.root'%(add,iTz)
        namePic = homeDir+'Results/MC/MassFit/Tz11Bins/MC_MassResolution%s_shiftFix_Tz%s.pdf'%(add,iTz)
    
    
    
    import os, sys 
    save = os.dup( sys.stdout.fileno() ) 
    newout = file(nameTxt, 'w' ) 
    os.dup2( newout.fileno(), sys.stdout.fileno() ) 
    r.Print("v") 
    print "chi2 eta_c %6.4f \n"%(chi2Etac)
    print "chi2 Jpsi %6.4f \n"%(chi2Jpsi)
    r.correlationMatrix().Print()
    os.dup2( save, sys.stdout.fileno() ) 
    newout.close()
    
    w.writeToFile(nameWksp)
    fFit = TFile (nameRoot,'RECREATE')
        
    
    c.Write('')
    fFit.Write()
    fFit.Close()
    
    c.SaveAs(namePic)
    r.correlationMatrix().Print('v')
    r.globalCorr().Print('v')



def MC_Resolution_Fit():

    #fitData(0)
    nTzBins = 11
    for iTz in range(nTzBins):
        fitData(iTz+1)  
    

MC_Resolution_Fit()
#fitData(1)
#fitData(2)
